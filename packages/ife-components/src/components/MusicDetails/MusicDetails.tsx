import {Button, Divider, Grid, Hidden, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import CloseIcon from '@material-ui/icons/Close';
import PauseIcon from '@material-ui/icons/Pause';
import PlayIcon from '@material-ui/icons/PlayArrow';
import * as React from 'react';
import {AudioGroup, AudioItem} from '../../store/audio/types';
import {MediaThumbnailWithSize} from '../common/MediaThumbnail/MediathumbnailWithSize';
import {ImageVariantsType} from '../types';
import * as defaultProps from './defaultProps.json';
import {MusicDetailsProps} from './types';
import {attachImagesToAudioItem} from './utils';

const MusicDetails: React.FC<MusicDetailsProps> = (props: MusicDetailsProps) => {
  const generateCurrentClass = (track: AudioItem) => (props.currentPlayingUid === track.uid ? 'current-item' : '');
  const isPauseButton = (track: AudioItem) => props.currentPlayingUid === track.uid && !props.isPlayerPaused;
  const isAddIcon = (track: AudioItem) => !props.playlistIdsMap[track.uid];
  const isBuffering = (track: AudioItem) => props.currentPlayingUid === track.uid && props.isBuffering;
  const removeFromPlaylist = (track: AudioItem, props: MusicDetailsProps) => (e: any) => {
    props.onRemoveFromPlaylist(track.uid);
    e.stopPropagation();
  };

  const addToPlaylist = (tracks: AudioItem[], props: MusicDetailsProps) => (e: any) => {
    props.addToPlaylist(tracks);
    e.stopPropagation();
  };
  return (
    <Grid container className="music-container">
      <Grid item xs={12} md={12} className="header">
        <Button aria-label="Go back" className="back-button" onClick={props.onClickBack} children={<ArrowBackIcon />} />
        <h1 className="title">
          <div className="album-title">{props.item.title}</div>
          <div className="subtitle artist" children={props.item.artist} />
        </h1>
      </Grid>
      <Grid item xs={12} md={6}>
        <div className="poster-container">
          <div className="poster">
            {props.item.titleImages && (
              <MediaThumbnailWithSize
                item={props.item}
                className="poster-image"
                imageVariants={props.item.titleImages[0]}
                variantType={ImageVariantsType.audio}
                clickable={false}
              />
            )}
          </div>
        </div>
      </Grid>
      <Grid item xs={12} md={6} className="details">
        <div className="title">
          <div className="album-title">{props.item.title}</div>
          <div className="subtitle artist" children={props.item.artist} />
        </div>
        <List component="nav" className="audio-list">
          <ListItem onClick={_ => props.item && props.item.items && props.playAll(props.item.items, props.item)}>
            <Hidden smDown children={<ListItemIcon children={<div className="track-number" children={''} />} />} />
            <div className="top-row-items">
              {props.item.items && (
                <Button aria-label="Play all" className="play-all-button">
                  <PlayIcon />
                  <span className="play-label">Play All</span>
                </Button>
              )}
              <div className="track-count">
                <span>{props.item && props.item.items ? props.item.items.length : '0'} tracks</span>
              </div>
            </div>
          </ListItem>
          <Divider />
          {props.item.items &&
            props.item.items.map((track, index, array) => (
              <div key={index}>
                <ListItem
                  className={generateCurrentClass(track)}
                  button
                  onClick={props.handleListItemClick(track, isPauseButton(track), props.item)}
                >
                  <Hidden
                    smDown
                    children={<ListItemIcon children={<div className="track-number" children={`${index + 1}.`} />} />}
                  />
                  <ListItemIcon
                    className={'play-pause-action'}
                    children={
                      isBuffering(track) ? (
                        <CircularProgress size={24} />
                      ) : isPauseButton(track) ? (
                        <PauseIcon />
                      ) : (
                        <PlayIcon />
                      )
                    }
                  />
                  <ListItemText className={'title-artist'} primary={track.title} secondary={track.artist} />
                  <ListItemIcon
                    className={'playlist-action'}
                    children={
                      isAddIcon(track) ? (
                        <AddIcon
                          onClick={addToPlaylist(
                            [
                              {
                                ...attachImagesToAudioItem(track, props.item as AudioGroup),
                                parentId: props.item.uid
                              }
                            ],
                            props
                          )}
                        />
                      ) : (
                        <CloseIcon onClick={removeFromPlaylist(track, props)} />
                      )
                    }
                  />
                </ListItem>
                {array.length !== index + 1 ? <Divider /> : null}
              </div>
            ))}
        </List>
      </Grid>
    </Grid>
  );
};

MusicDetails.defaultProps = defaultProps as any;

export default MusicDetails;
