import Modal from '@material-ui/core/Modal';
import CloseIcon from '@material-ui/icons/Close';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import {PortalComponent} from 'component-utils';
import * as React from 'react';
import {AudioItem} from '../../store/audio/types';
import {VideoItem} from '../../store/videos/types';
import * as defaultProps from './defaultProps.json';
import * as defaults from './defaultProps.json';
import ListItem from './ListItem';
import {ListButtonContainer, ResponsiveContainer, StyledContent, StyledItemCount} from './styledComponents';
import {PlaylistProps} from './types';
import {Button} from '@material-ui/core';

const Playlist: React.FC<PlaylistProps> = (layoutProps: PlaylistProps) => {
  const {componentThemeDefault, ...propsDefault} = defaults as any;
  const props = {...propsDefault, ...layoutProps} as PlaylistProps;
  return (
    <PortalComponent componentTheme={props.componentTheme}>
      <ListButtonContainer>
        {props.feature.showItemCountBadge && props.items.length ? (
          <StyledItemCount onClick={props.toggleModalVisibility}>{props.items.length}</StyledItemCount>
        ) : null}
        <Button
          aria-label="Playlist"
          className="list-button"
          onClick={props.toggleModalVisibility}
          children={<FormatListBulletedIcon />}
        />
      </ListButtonContainer>
      <Modal
        container={props.mountPoint}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.isModalOpen}
        onClose={props.toggleModalVisibility}
      >
        <ResponsiveContainer>
          <StyledContent>
            <div className="modal-header">
              <div className="title">
                <span>Playlist</span>
              </div>
              <Button
                aria-label="Close"
                className="close-button"
                onClick={props.toggleModalVisibility}
                children={<CloseIcon />}
              />
            </div>
            <div className="modal-content">
              {!props.items.length ? (
                <div children={'Your Playlist Is Empty'} />
              ) : (
                props.items.map((item: VideoItem | AudioItem, index) => (
                  <ListItem
                    isPlaying={props.isPlaying}
                    currentPlayingItem={props.currentItem}
                    key={index}
                    item={item}
                    onRemoveItem={props.onRemoveItem}
                    onItemPlay={() => (props as any).onItemPlay(item)}
                    onItemPause={props.onItemPause}
                  />
                ))
              )}
            </div>
            <div className="modal-footer">
              {props.items.length ? (
                <Button
                  aria-label="Clear"
                  variant="contained"
                  className="cta clear"
                  children={'Clear'}
                  onClick={props.onClearPlaylist}
                />
              ) : (
                <div />
              )}
              <Button
                aria-label="Close"
                variant="contained"
                className="cta close"
                children={'Close'}
                onClick={props.toggleModalVisibility}
              />
            </div>
          </StyledContent>
        </ResponsiveContainer>
      </Modal>
    </PortalComponent>
  );
};

Playlist.defaultProps = defaultProps as any;

export default Playlist;
