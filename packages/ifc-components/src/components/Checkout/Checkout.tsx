import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Checkout as ExternalInterface} from '../common/ComponentsInterface';
import Voucher from './Voucher';
import Purchase from './Purchase';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  premiumTiers?: any;
  WifiServiceOptions?: any;
  connected?: boolean;
  onVoucherSubmit?: any;
  errorDict?: any;
  messagingTiers?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.62);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.62);
  display: flex;
  flex-direction: column;
`;

class Checkout extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    premiumTiers: [],
    messagingTiers: []
  };

  constructor(props: any) {
    super(props);
  }

  public render() {
    const {WifiServiceOptions} = this.props;
    const voucherSelected =
      WifiServiceOptions && WifiServiceOptions.selected && WifiServiceOptions.selected.method == 'voucher';
    const messagingSelected =
      WifiServiceOptions && WifiServiceOptions.selected && WifiServiceOptions.selected.method == 'messaging';
    const selectedTier = this.props.messagingTiers[0];

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-checkout-component">
          {!voucherSelected && !messagingSelected ? (
            <Purchase {...this.props} />
          ) : voucherSelected && !messagingSelected ? (
            <Voucher {...this.props} />
          ) : messagingSelected && !WifiServiceOptions.signup ? (
            this.props.signupWithMessaging(selectedTier.id)
          ) : (
            <p>Please wait....</p>
          )}
        </Container>
      </PortalComponent>
    );
  }
}

export default Checkout;
