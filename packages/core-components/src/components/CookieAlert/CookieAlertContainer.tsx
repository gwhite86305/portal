import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {CookieAlert as ExternalInterface} from '../common/ComponentsInterface';
import CookieAlert, {InternalInterface} from './CookieAlert';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {getUserResponse, saveUserResponse} from '../../store/CookieAlert/actions';

export type PageProps = ExternalInterface & InternalInterface;
export interface DispatchProps {
  onDismiss(): any;
  onLoad(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => ({
    dismissed: state.cookieAlert.dismissed
  }),
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onDismiss: () => {
      dispatch(saveUserResponse(true, ownProps));
    },
    onLoad: () => {
      dispatch(getUserResponse());
    }
  })
)
class CookieAlertContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
    this.props.onLoad();
  }
  onClose = () => {
    this.props.onDismiss();
  };
  public render() {
    return this.props.dismissed ? null : <CookieAlert {...this.props} onClose={this.onClose} />;
  }
}

export default withLocaleProps(CookieAlertContainer, [
  {closeButtonAltText: 'core.CookieAlert.closeIconAltText'}
]) as any;
