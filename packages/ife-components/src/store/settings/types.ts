export interface AgeRatingsClass {
  code: string;
  label: string;
}

export interface ISettingsState {
  readonly ageRatingPassword: string;
  readonly ageRatingClasses: AgeRatingsClass[];
  readonly ageRatingClassSelected: string;
}

export interface UserSettings {
  ageRatingClassSelected: string;
  ageRatingPassword: string;
}
