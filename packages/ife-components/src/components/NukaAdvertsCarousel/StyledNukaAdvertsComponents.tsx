import * as React from 'react';
import {
  renderThemeProperty,
  applyMediaQueriesForStyle
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {pathOr} from 'ramda';
import {AdItemBackground} from '../AdvertsCardGrid/StyledAdvertsGrid';

const getImageHeight = (firstItemImages: any, breakpoint: string, itemCount: number) => {
  const baseHeight = pathOr(264, [breakpoint, 'height'], firstItemImages);
  const scrollbarHeight = itemCount > 1 ? 32 : 0;
  return `${baseHeight + scrollbarHeight}px`;
};
export const StyledCarousel = styled.div`
  max-width: 100vw;
  @media screen and (max-width: 600px) {
    ${({itemCount, firstItemImages}: any) => `height: ${getImageHeight(firstItemImages, 'small', itemCount)}`};
    ${({firstItemImages}: any) => `width: ${pathOr(335, ['small', 'width'], firstItemImages)}px`};
  }
  @media screen and (min-width: 601px) and (max-width: 1280px) {
    ${({itemCount, firstItemImages}: any) => `height: ${getImageHeight(firstItemImages, 'medium', itemCount)}`};
    ${({firstItemImages}: any) => `width: ${pathOr(728, ['medium', 'width'], firstItemImages)}px`};
  }
  @media screen and (min-width: 1281px) {
    ${({itemCount, firstItemImages}: any) => `height: ${getImageHeight(firstItemImages, 'large', itemCount)}`};
    ${({firstItemImages}: any) => `width: ${pathOr(1010, ['large', 'width'], firstItemImages)}px`};
  }
  .carousel-item {
    height: 100%;
    background-size: contain;
    background-repeat: no-repeat;
    background-position-x: center;
  }
  .slider,
  .slider-list,
  .slider-frame,
  .slider-slide {
    height: 100% !important;
  }

  button {
    -webkit-appearance: none;
    background: none;
    border-style: outset;
    border: none;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
  }

  /* LEFT - RIGHT arrow style */
  .slider-control-centerleft,
  .slider-control-centerright {
    ${(props: any) => applyMediaQueriesForStyle('top', props.theme.componentTheme.arrowTopPosition, props.theme, true)};
    z-index: 100;
  }

  .control-container {
    align-items: center;
    background-color: unset;
    cursor: pointer;
    display: flex;
    justify-content: center;
  }

  .arrow {
    ${renderThemeProperty('arrowsHeight', 'padding')};
    ${renderThemeProperty('arrowsMargin', 'margin')};
    ${renderThemeProperty('arrowsColor', 'border-color')};
    ${renderThemeProperty('arrowsThikness', 'border-right-width')};
    ${renderThemeProperty('arrowsThikness', 'border-bottom-width')};
    border-right-style: solid;
    border-bottom-style: solid;
    border-top-width: 0px;
    border-left-width: 0px;
    cursor: pointer;
    display: inline-block;
    &--left {
      transform: rotate(135deg) translate(-2.5px, -2.5px);
      -webkit-transform: rotate(135deg) translate(-2.5px, -2.5px);
    }
    &--right {
      transform: rotate(-45deg) translate(-2.5px, -2.5px);
      -webkit-transform: rotate(-45deg) translate(-2.5px, -2.5px);
    }
  }

  /* Bullet buttons  style */
  .slider-control-bottomcenter {
    z-index: 99;
    align-items: center;
    bottom: 0px;
    display: flex;
    justify-content: center;
    left: unset;
    position: absolute;
    transform: unset;
    width: 100%;
    ${renderThemeProperty('bulletButtonBarColor', 'background-color')};
  }

  .bottom-center-controls {
    display: flex;
    flex-direction: row;
    margin: 0;
    padding: 0;
    overflow: hidden;

    .bullet-button {
      border-radius: 50%;
      border-style: solid;
      ${renderThemeProperty('bulletButtonsBorderColorThikness', 'border-width')};
      ${renderThemeProperty('bulletButtonsBorderColor', 'border-color')};
      ${renderThemeProperty('bulletButtonsColor', 'background-color')};
      ${renderThemeProperty('bulletButtonsSize', 'padding')};
      ${renderThemeProperty('bulletButtonsMargin', 'margin')};
    }
    .selected {
      ${renderThemeProperty('bulletButtonsSelectedColor', 'background-color')};
    }
  }
`;

export const StyledAdItemBackground = styled(AdItemBackground)`
  max-width: 100vw;
  @media screen and (max-width: 600px) {
    ${({images}: any) => `height: ${pathOr(264, ['small', 'height'], images)}px`};
    ${({images}: any) => `width: ${pathOr(335, ['small', 'width'], images)}px`};
  }
  @media screen and (min-width: 601px) and (max-width: 1280px) {
    ${({images}: any) => `height: ${pathOr(264, ['medium', 'height'], images)}px`};
    ${({images}: any) => `width: ${pathOr(728, ['medium', 'width'], images)}px`};
  }
  @media screen and (min-width: 1281px) {
    ${({images}: any) => `height: ${pathOr(264, ['large', 'height'], images)}px`};
    ${({images}: any) => `width: ${pathOr(1010, ['large', 'width'], images)}px`};
  }
`;

export const StyledInfoContainer = styled.div`
  height: 77px;
  background: rgba(32, 46, 57, 0.9);
  display: flex;
  justify-content: space-between;
`;

export const StyledInfoSubSesction = styled.div`
  display: flex;
  align-self: center;
`;

export const StyledIcon = styled.div`
  height: 77px;
  display: flex;
  margin: 0 20px;
  justify-content: space-between;
  img {
    margin: auto;
    height: 50px;
    width: 50px;
    border-radius: 10px;
    box-shadow: 1px 1px 2px 0px black;
  }
`;

export const StyledIconMobile = styled(StyledIcon)`
  margin: 0px 10px;
`;

export const StyledTitle = styled.div`
  color: white;
  display: flex;
  line-height: 19px;
  align-items: center;
  justify-content: space-between;
  font-size: 27px;
  font-weight: 600;
`;

export const StyledTitleMobile = styled(StyledTitle)`
  height: fit-content;
  padding-bottom: 5px;
  font-size: 21px;
`;

export const StyledSubtitle = styled.div`
  color: #ffffff;
  font-size: 18px;
  align-self: center;
`;

export const StyledTitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: fit-content;
  margin: auto 0 auto 0;
`;

export const StyledCta = styled.div`
  background-color: #347ac9;
  color: #ffffff;
  border-radius: 3px;
  width: 100px;
  height: 36px;
  font-size: 16px;
  padding: 0px 3px;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  cursor: pointer;
  margin: 22px;
`;

export const StyledCtaMobile = styled(StyledCta)`
  width: 90px;
  height: 30px;
  margin: auto 10px auto 5px;
  font-size: 14px;
`;
