import '@storybook/addon-knobs/register';
import '@storybook/addon-actions/register';
import '@storybook/addon-a11y/register';
import registerScissors from 'storybook-addon-scissors';
import devices from './devices.json';

registerScissors(devices);
