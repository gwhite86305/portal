#! /bin/sh
BINDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR=$BINDIR
echo $DIR
echo $DIR | grep "amen-mock-server" -
if [ $? -eq 1 ]; then
    DIR="node_modules/amen-mock-server"
else
    DIR=$DIR/..
fi
case "$1" in
    start)
        node $DIR/index.js &> $BINDIR/../.amen.server.log &
        echo $! > $BINDIR/../.amen.server.pid
        sleep 10
        curl http://localhost:4000 &> /dev/null
        if [ $? -eq 0 ]; then
            echo -e "\e[92mAmen server started\e[0m"
            exit 0
        else
            echo -e "\e[91mAmen server could not be started\e[0m"
            kill $(cat ./.server.pid)
            exit 1

        fi
        ;;
    stop)
        kill $(cat $BINDIR/../.amen.server.pid)
        if [ $? -eq 0 ]; then
            echo -e "\e[92mAmen server stopped\e[0m"
            exit 0
        else
            echo -e "\e[91mAmen server is unstoppable\e[0m"
            exit 0
        fi
        ;;
    *)
        echo $"Usage: $) {start|stop}"
        ;;
esac


