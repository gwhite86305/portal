enum PortalToBuilderMessageType {
  componentSelected = 'componentSelected',
  componentDeleteRequest = 'componentDeleteRequest',
  componentInsertRequest = 'componentInsertRequest',
  portalReady = 'portalReady',
  portalLocaleChange = 'portalLocaleChange',
  keyPress = 'keyPress'
}

export default PortalToBuilderMessageType;

export interface PortalToBuilderMessageData {
  type: PortalToBuilderMessageType;
  data: any;
}
