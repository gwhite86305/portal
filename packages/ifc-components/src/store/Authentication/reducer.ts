import {Action, ActionCreators} from './actions';

export type State = {
  readonly errorMessage: string | null;
};

export const initialState: State = {
  errorMessage: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;
  switch (action.type) {
    case ActionCreators.aaAuthStarted.type:
    case ActionCreators.aaAuthSuccess.type:
    case ActionCreators.aaAuthError.type:
    case ActionCreators.clearMessageAA.type:
    case ActionCreators.logoutAA.type:
    case ActionCreators.aaJwtRetrived.type:
    case ActionCreators.aaCheckSubscriptionStarted.type:
    case ActionCreators.aaCheckSubscriptionSuccess.type:
    case ActionCreators.aaCheckSubscriptionError.type:
    case ActionCreators.aaFetchCardStarted.type:
    case ActionCreators.aaFetchCardSucccess.type:
    case ActionCreators.aaFetchCardFailed.type:
    case ActionCreators.gogoPurchaseSubscriptionSuccess.type:
    case ActionCreators.gogoPurchaseSubscriptionError.type:
      partialState = {...action.payload};
      break;
    default:
      return state;
  }

  return {...state, ...partialState};
}
