import * as React from 'react';
import {
  
  renderThemeProperty
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {Button} from '@material-ui/core';
import {ButtonProps} from '@material-ui/core/Button';

export default styled(Button)`
  cursor: pointer;
  padding: 1rem 2rem;
  text-transform: none;
  ${renderThemeProperty('buttonBackground', 'background')};
  position: relative;
  ${renderThemeProperty('buttonFontSize', 'font-size')};
  ${renderThemeProperty('buttonColor', 'color')};
`;
