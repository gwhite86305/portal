import * as React from 'react';
import {pathOr} from 'ramda';
import {connect} from 'react-redux';
import {injectIntl} from 'react-intl';

const DEFAULT_CONTENT_PROPNAME = 'content';

export interface ContentKeyPropName {
  contentKey: string;
  contentPropName?: string;
}

export function withContent(WrappedComponent: any, contentKeyPropMap: ContentKeyPropName[]) {
  const ComponentWithContent = class extends React.Component<any, any> {
    constructor(props: any) {
      super(props);
    }

    public render() {
      const {stateContent, intl, ...originalProps} = this.props;
      const contentProps: any = {};
      contentKeyPropMap.forEach((entry: ContentKeyPropName) => {
        const contentPropName = entry.contentPropName || DEFAULT_CONTENT_PROPNAME;
        const defaultProps = this.props[contentPropName];

        contentProps[contentPropName] =
          pathOr(undefined, [this.props.componentUid, intl.locale, entry.contentKey], stateContent) || defaultProps; // use default "content" prop that should be provided by component

        if (defaultProps) {
          // make sure anything left out in the user entered content object has defaults
          contentProps[contentPropName] = {...defaultProps, ...contentProps[contentPropName]};
        }
      });

      return <WrappedComponent {...originalProps} {...contentProps} />;
    }
  };

  const ComponentWithContentIntl = connect(
    (state: any, ownProps: any) => ({
      stateContent: state.contentAssets.content
    }),
    () => {}
  )(injectIntl(ComponentWithContent));

  ComponentWithContentIntl.defaultProps = WrappedComponent.defaultProps;
  return ComponentWithContentIntl;
}
