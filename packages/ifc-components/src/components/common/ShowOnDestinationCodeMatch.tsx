import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {ShowOnDestinationCodeMatch as ExternalInterface} from './ComponentsInterface';

// How to use: If you add a "destinationCode" to a field, itll check to see if it matches the current device destinationCode. If omitted from the layout value it does no comparison and returns child normally.

export interface InternalInterface {
  actualDestinationCode: string;
}

@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, actualDestinationCode: null};
  if (state.device) {
    props.actualDestinationCode = state.device.flightDestination;
  }
  return props;
})
export default class ShowOnDestinationCodeMatch extends React.Component<ExternalInterface & InternalInterface, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.feature.destinationCode && this.props.actualDestinationCode
      ? new RegExp(this.props.feature.destinationCode).test(this.props.actualDestinationCode)
        ? this.props.children
        : null
      : null;
  }
}
