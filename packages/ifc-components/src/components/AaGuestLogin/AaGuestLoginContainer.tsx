import * as React from 'react';
import {utils} from 'component-utils';
import {AaGuestLogin as ExternalInterface} from '../common/ComponentsInterface';
import AaGuestLogin, {InternalInterface} from './AaGuestLogin';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {HideModal, ShowModal} from 'core-components/dist/store/Modal/actions';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
const connectWrapper = utils.connectWrapper.default;
import {registerGuestUser} from '../../store/User/actions';
import {getTierById} from '../../utils/ConnectionUtils';

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onGuestLoginSubmit(email: string): any;
  onReturnClick(): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    let props = {...ownProps};
    return props;
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onGuestLoginSubmit: email => {
        dispatch(registerGuestUser(email));
        dispatch(HideModal());
        if (ownProps.feature.openPageOnContinue) {
          ownProps.history.push('/' + ownProps.feature.openPageOnContinue);
        } else if (ownProps.feature.openModalOnContinue) {
          dispatch(ShowModal(ownProps.feature.openModalOnContinue));
        }
      },
      onReturnClick: () => {
        dispatch(HideModal());
        if (ownProps.feature.openPageOnSkip) {
          ownProps.history.push('/' + ownProps.feature.openPageOnSkip);
        } else if (ownProps.feature.openModalOnSkip) {
          dispatch(ShowModal(ownProps.feature.openModalOnSkip));
        }
      }
    };
  }
)
class AaGuestLoginContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
    // This state is to make sure what we don't clear the state on unmount if
    // the user is continuing through the purchase path.
    this.state = {
      continuingPurchasePath: false
    };
  }

  public render() {
    const {intl, componentUid} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.AaGuestLogin.header'},
      {note: 'ifc.AaGuestLogin.note'},
      {emailInputLabel: 'ifc.AaGuestLogin.emailInputLabel'},
      {returnButtonText: 'ifc.AaGuestLogin.returnButtonText'},
      {continueButtonText: 'ifc.AaGuestLogin.continueButtonText'},
      {errorMessageInvalidEmail: 'ifc.AaGuestLogin.errorMessageInvalidEmail'}
    ]);
    let errorDict = {
      errorMessageInvalidEmail: localizedProps.errorMessageInvalidEmail,
      generic: localizedProps.errorMessageGeneric
    };
    let onContinueClick = () => {
      this.setState({
        continuingPurchasePath: true
      });
      this.props.onContinue();
    };
    return <AaGuestLogin {...this.props} errorDict={errorDict} {...localizedProps} onContinueClick={onContinueClick} />;
  }
}

export default injectIntl(withRouter(AaGuestLoginContainer));
