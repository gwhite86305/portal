import {IAudioState} from './reducer';
import {filter} from 'lodash';
import {AudioCategory} from './types';

export const selectAudioByCategories = (audioState: IAudioState): AudioCategory[] => {
  const {categories, items} = audioState;
  return categories.allIds
    .map(uid => ({
      parentCategory: categories.byId[uid].category,
      category: categories.byId[uid].title,
      uid,
      items: filter(items.byId, item => !!item.categories.find(cat => cat === uid))
    }))
    .concat({
      category: 'Others',
      parentCategory: 'none',
      uid: 'others',
      items: filter(items.byId, item => item.categories.length === 1)
    })
    .filter(category => category.items.length);
};
