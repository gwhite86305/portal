import styled from 'styled-components';

export interface StyledGridImageProps {
  imgSrc: string;
}

const StyledGridImage = styled.div<StyledGridImageProps>`
  position: relative;
  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center center;
  ${({imgSrc}: any) => `background-image: url(${imgSrc})`}
`;

export default StyledGridImage as any;
