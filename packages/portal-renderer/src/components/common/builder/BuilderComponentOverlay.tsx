import styled from 'styled-components';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {GridItemComponentName} from '../../GridItem/GridItem';
import {GridContainerComponentName} from '../../GridContainer/GridContainer';
import StyledComponentOverlay, {InsertCenterOverlay, InsertOverlay, InsertOverlayTitle} from './StyledComponentOverlay';
import {Component} from 'component-utils/dist/types/Component';
import {pathOr} from 'ramda';

const LABEL_HEIGHT = 20;

export enum InsertPosition {
  before = 'before',
  after = 'after',
  inside = 'inside'
}

export interface OverlayProps {
  boundingRect: ClientRect | null;
  yScrollPosition: number;
  componentUid?: string;
  component?: Component;
  hasInnerOverlay?: boolean;
  onComponentInsert(position: InsertPosition): void;
}

class BuilderComponentOverlay extends React.Component<OverlayProps> {
  private el: HTMLElement;

  constructor(props: OverlayProps) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    document.body.appendChild(this.el);
  }

  componentWillUnmount() {
    document.body.removeChild(this.el);
  }

  private _isDomElementFixed = function() {
    const {componentUid} = this.props;

    if (componentUid) {
      const overlayDomElement = document.querySelectorAll("[data-component-guid='" + this.props.componentUid + "']");

      let elFound = overlayDomElement[0] || overlayDomElement;
      while (typeof elFound === 'object' && elFound.nodeName && elFound.nodeName.toLowerCase() !== 'body') {
        if (
          window
            .getComputedStyle(elFound)
            .getPropertyValue('position')
            .toLowerCase() === 'fixed'
        ) {
          return true;
        }
        elFound = elFound.parentElement as any;
      }
      return false;
    } else {
      return false;
    }
  };

  public render() {
    const {boundingRect, component, hasInnerOverlay, onComponentInsert, yScrollPosition} = this.props;
    if (!boundingRect) {
      return null;
    }
    // if DOM element we overlay is fixed relative to the page we have to adjust our calculations
    const isDomElementFixed = this._isDomElementFixed();
    const yScrollOffset = isDomElementFixed ? yScrollPosition : 0;
    let labelYPosition = -LABEL_HEIGHT;
    const topPosition = boundingRect.top + yScrollPosition;
    labelYPosition = (topPosition < LABEL_HEIGHT ? boundingRect.height + topPosition : labelYPosition) - yScrollOffset;

    // if no component or explicitly grid item component, show + symbols on l/r, as opposed to t/b
    const isGridOverlay =
      component &&
      (component.componentName === GridItemComponentName || component.componentName === GridContainerComponentName);
    const isGridItemOverlay = component && component.componentName === GridItemComponentName;

    const overlayPosition = {
      top: topPosition - yScrollOffset,
      bottom: boundingRect.bottom - yScrollOffset,
      left: boundingRect.left + 2,
      right: boundingRect.right,
      width: boundingRect.width - 4,
      height: boundingRect.height
    };

    const overlayInternalComponents: React.ReactNode[] = component
      ? [
          <InsertOverlayTitle
            key="insert-title"
            style={{top: labelYPosition + 'px'}}
            children={component.componentName}
          />
        ]
      : [];

    overlayInternalComponents.push(
      isGridItemOverlay
        ? [
            <InsertOverlay
              className="left"
              key="insert-l"
              children="+"
              onClickCapture={e => {
                e.stopPropagation();
                onComponentInsert(InsertPosition.before);
              }}
            />,
            <InsertOverlay
              className="right"
              key="insert-r"
              children="+"
              onClick={() => onComponentInsert(InsertPosition.after)}
            />
          ]
        : [
            <InsertOverlay
              className="top"
              key="insert-t"
              children="+"
              onClick={() => onComponentInsert(InsertPosition.before)}
            />,
            <InsertOverlay
              className="bottom"
              key="insert-b"
              children="+"
              onClick={() => onComponentInsert(InsertPosition.after)}
            />
          ]
    );

    const componentChildren = pathOr([], ['properties', 'children'], component);

    const overlayComponent = (
      <StyledComponentOverlay
        hasInnerOverlay={hasInnerOverlay}
        isInnerOverlay={!component}
        isFixedComponent={isDomElementFixed}
        style={overlayPosition}
        data-builder-component-overlay={true}
      >
        {component ? overlayInternalComponents : null}
        {isGridOverlay && componentChildren.length === 0 ? (
          <InsertCenterOverlay onClick={() => onComponentInsert(InsertPosition.inside)}>&oplus;</InsertCenterOverlay>
        ) : null}
      </StyledComponentOverlay>
    );

    return ReactDOM.createPortal(overlayComponent, this.el);
  }
}

export default BuilderComponentOverlay;
