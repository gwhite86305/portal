import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {BillBoard as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import Icon from '../Icon/Icon';
import NavButton from '../common/NavButton';
import {sanitize} from 'component-utils';
import styled from 'styled-components';

const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  position: relative;
  ${renderThemeProperty('backgroundColor', 'background-color')};
  display: flex;
  flex-direction: column;
  height: 100%;
  div {
    display: flex;
    flex-direction: column;
  }
  a {
    text-decoration: none;
    display: flex;
    flex-direction: column;
  }
  .billBoard-textregion {
    flex-wrap: wrap;
    align-self: flex-end;
    flex-grow: 2;
    width: 100%;
  }
  .header-arrow {
    padding-top: 5px;
    ${renderThemeProperty('headerArrowFloat', 'float')};
    ${(props: any) => {
      return applyMediaQueriesForStyle('margin', props.theme.componentTheme.arrowMargin, props.theme);
    }};
  }
  .billBoard-header {
    ${renderThemeProperty('billboardHeaderFontWeight', 'font-weight')};
    font-size: ${(props: any) => props.theme.tertiaryFontSize};
    margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    ${(props: any) => {
      return applyMediaQueriesForStyle('text-align', props.theme.componentTheme.headertextAlign, props.theme);
    }};
    word-wrap: break-word;
    ${renderThemeProperty('headerTextColor', 'color')};
    div,
    p {
      display: inline-block;
    }
  }
  .subHeader-text {
    margin-top: calc(${(props: any) => props.theme.gutter} * 0.625);
    ${(props: any) => {
      return applyMediaQueriesForStyle('text-align', props.theme.componentTheme.subheadertextAlign, props.theme);
    }};
    word-wrap: break-word;
    line-height: 1.25;
    ${renderThemeProperty('subHeaderTextColor', 'color')};
  }
  p {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;
    margin: 0;
  }
  .button-wrap {
    justify-content: center;
    ${(props: any) => {
      return applyMediaQueriesForStyle('align-items', props.theme.componentTheme.buttonAlign, props.theme);
    }};
    padding-top: ${(props: any) => props.theme.gutter};
  }
  .button-div {
    flex-grow: 1;
  }
  .add-cursor {
    cursor: pointer;
  }
`;

const HeaderBody = (props: any) => {
  const {headerText, displayArrowIcon, navIcon, spanArrow} = props;
  let headerArrowSpan = '';
  if (spanArrow && displayArrowIcon) {
    headerArrowSpan =
      headerText + "<span><img class='header-arrow' src=" + props.navIcon + " alt='billboard icon'></span>";
  }

  return spanArrow && displayArrowIcon ? (
    <div>
      <p dangerouslySetInnerHTML={sanitize(headerArrowSpan)} />
    </div>
  ) : displayArrowIcon ? (
    <div>
      <p dangerouslySetInnerHTML={sanitize(headerText)} />
      <img className="header-arrow" src={navIcon} alt="billboard icon" />
    </div>
  ) : (
    <div>
      <p dangerouslySetInnerHTML={sanitize(headerText)} />
    </div>
  );
};

const HeaderText = (props: any) => {
  const {headerText, displayArrowIcon, navIcon, spanArrow} = props;
  return (
    <div className="billBoard-header">
      <HeaderBody headerText={headerText} displayArrowIcon={displayArrowIcon} navIcon={navIcon} spanArrow={spanArrow} />
    </div>
  );
};

const SubHeaderText = (props: any) => {
  const {subHeaderText} = props;
  return (
    <div className="subHeader-text">
      <p dangerouslySetInnerHTML={sanitize(subHeaderText)} />
    </div>
  );
};

const BillBoardTextRegion = (props: any) => {
  const {headerText, openPageOnClick, displayArrowIcon, subHeaderText, navIcon, spanArrow} = props;
  return (
    <div className="billBoard-textregion">
      {headerText ? (
        <HeaderText
          headerText={headerText}
          spanArrow={spanArrow}
          openPageOnClick={openPageOnClick}
          displayArrowIcon={displayArrowIcon}
          navIcon={navIcon}
        />
      ) : null}
      {subHeaderText ? <SubHeaderText subHeaderText={subHeaderText} /> : null}
    </div>
  );
};

class BillBoard extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {altTextForImage, subHeaderText, headerText, feature, buttonText, componentTheme} = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <Container className={!feature.includeButton ? 'add-cursor' : ''}>
          {!feature.includeButton ? (
            feature.openPageOnClick == '#' || feature.openPageOnClick == '' ? (
              <React.Fragment>
                <ResponsiveImage
                  altText={altTextForImage}
                  image={feature.image}
                  imageWidth={componentTheme.imageWidth}
                  imageMargin={componentTheme.imageMargin}
                />
                <BillBoardTextRegion
                  subHeaderText={subHeaderText}
                  headerText={headerText}
                  spanArrow={feature.spanArrow}
                  openPageOnClick={feature.openPageOnClick}
                  navIcon={feature.navIcon}
                  displayArrowIcon={feature.displayArrowIcon}
                />
              </React.Fragment>
            ) : (
              <a href={feature.openPageOnClick} target="_blank" title="(opens in new tab/window)">
                <React.Fragment>
                  <ResponsiveImage
                    altText={altTextForImage}
                    image={feature.image}
                    imageWidth={componentTheme.imageWidth}
                    imageMargin={componentTheme.imageMargin}
                  />
                  <BillBoardTextRegion
                    subHeaderText={subHeaderText}
                    headerText={headerText}
                    spanArrow={feature.spanArrow}
                    openPageOnClick={feature.openPageOnClick}
                    navIcon={feature.navIcon}
                    displayArrowIcon={feature.displayArrowIcon}
                  />
                </React.Fragment>
              </a>
            )
          ) : (
            <div className="button-div">
              <ResponsiveImage
                altText={altTextForImage}
                image={feature.image}
                imageWidth={componentTheme.imageWidth}
                imageMargin={componentTheme.imageMargin}
              />
              <BillBoardTextRegion
                subHeaderText={subHeaderText}
                headerText={headerText}
                spanArrow={feature.spanArrow}
                openPageOnClick={feature.openPageOnClick}
                navIcon={feature.navIcon}
                displayArrowIcon={feature.displayArrowIcon}
              />
              <div className="button-wrap">
                <NavButton isPrimary={true} to={feature.openPageOnClick} buttonText={buttonText} />
              </div>
            </div>
          )}
        </Container>
      </PortalComponent>
    );
  }
}

export default BillBoard;
