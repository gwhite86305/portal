import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withNotes} from '@storybook/addon-notes';
import {withKnobs, text, object, boolean, number, select} from '@storybook/addon-knobs/react';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';
import WifiServiceOptions from '../dist/components/WifiServiceOptions/WifiServiceOptions.js';

const defaultLocaleProps = {
  header: '',
  body: '',
  voucherTitle: 'Use a',
  voucherText: 'VOUCHER',
  voucherDescripiton: 'The most convienient option to get connected!',
  voucherButtonText: 'CONNECT NOW'
};
const defaultProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor',

    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',

    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',

    optionTitleColor: 'theme.secondaryTextColor',
    optionTitleFontSize: 'theme.secondaryFontSize',

    optionCostColor: 'theme.primaryTextColor',
    optionCostFontSize: 'theme.quaternaryFontSize',

    optionDescriptionColor: 'theme.primaryTextColor',
    optionDescriptionFontSize: 'theme.primaryFontSize'
  },
  feature: {
    voucherAvailable: true,
    purchaseOptionSortBy: 'duration',
    purchaseOptionOrderBy: 'ascending',
    showOptionDescription: true,
    openPageOnSelection: ''
  }
};

const demoHomeLocaleProps = {
  header: '',
  body: '',
  voucherTitle: 'Use a',
  voucherText: 'VOUCHER',
  voucherDescripiton: 'The most convienient option to get connected!',
  voucherButtonText: 'CONNECT NOW'
};
const propsDemoHome = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor',

    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',

    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',

    optionTitleColor: 'theme.secondaryTextColor',
    optionTitleFontSize: 'theme.secondaryFontSize',

    optionCostColor: 'theme.primaryTextColor',
    optionCostFontSize: 'theme.quaternaryFontSize',

    optionDescriptionColor: 'theme.primaryTextColor',
    optionDescriptionFontSize: 'theme.primaryFontSize'
  },
  feature: {
    voucherAvailable: true,
    purchaseOptionSortBy: 'duration',
    purchaseOptionOrderBy: 'ascending',
    showOptionDescription: false,
    openPageOnSelection: ''
  }
};

const demoServicePageLocaleProps = {
  header: 'Wi-Fi Services',
  body:
    'Pick one of the Wi-Fi sessions below. You can purchase Wi-Fi with a debit/credit card, PayPal, or a voucher code.',
  voucherTitle: 'Pre-purchased',
  voucherText: 'VOUCHER',
  voucherDescripiton: 'The most convienient option to get connected!',
  voucherButtonText: 'CONNECT NOW'
};
const propsDemoServicePage = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor',

    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',

    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',

    optionTitleColor: 'theme.secondaryTextColor',
    optionTitleFontSize: 'theme.secondaryFontSize',

    optionCostColor: 'theme.primaryTextColor',
    optionCostFontSize: 'theme.quaternaryFontSize',

    optionDescriptionColor: 'theme.primaryTextColor',
    optionDescriptionFontSize: 'theme.primaryFontSize'
  },
  feature: {
    voucherAvailable: true,
    purchaseOptionSortBy: 'duration',
    purchaseOptionOrderBy: 'ascending',
    showOptionDescription: true,
    openPageOnSelection: ''
  }
};

const neosHomePageLocaleProps = {
  header: '',
  body: '',
  voucherTitle: 'Utilizza il',
  voucherText: 'Voucher',
  voucherDescripiton: 'The most convienient option to get connected!',
  voucherButtonText: 'CONNECTI'
};
const propsNeosHome = {
  componentTheme: {
    margin: {
      xs: '0px 0px 70px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    dividerColor: '#BDBDBD',

    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',

    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',

    optionTitleColor: '#1A1919',
    optionTitleFontSize: 'theme.secondaryFontSize',

    optionCostColor: '#0073CB',
    optionCostFontSize: 'theme.quaternaryFontSize',

    optionDescriptionColor: 'theme.primaryTextColor',
    optionDescriptionFontSize: 'theme.primaryFontSize'
  },
  feature: {
    voucherAvailable: true,
    purchaseOptionSortBy: 'duration',
    purchaseOptionOrderBy: 'ascending',
    showOptionDescription: false,
    openPageOnSelection: ''
  }
};

const neosServicePageLocaleProps = {
  header: 'Attiva la tua connessione',
  body: 'Wi-Fi service description',
  voucherTitle: 'Utilizza il',
  voucherText: 'Voucher',
  voucherDescripiton: 'The most convienient option to get connected!',
  voucherButtonText: 'CONNECTI',
  openPageOnSelection: 'Checkout_page'
};

const propsNeosServicePage = {
  componentTheme: {
    margin: {
      xs: '0px 0px 70px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    dividerColor: '#BDBDBD',

    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',

    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',

    optionTitleColor: '#1A1919',
    optionTitleFontSize: 'theme.secondaryFontSize',

    optionCostColor: '#0073CB',
    optionCostFontSize: 'theme.quaternaryFontSize',

    optionDescriptionColor: 'theme.primaryTextColor',
    optionDescriptionFontSize: 'theme.primaryFontSize'
  },
  feature: {
    voucherAvailable: true,
    purchaseOptionSortBy: 'duration',
    purchaseOptionOrderBy: 'ascending',
    showOptionDescription: true,
    openPageOnSelection: ''
  }
};
const premiumTiers = [
  {
    description: '1-Hour Pass',
    customerId: 5,
    active: true,
    deviceTypeMask: 0,
    deviceCategoryId: 'PED',
    id: 72,
    cost: 12.0,
    name: 'Aa PED LH Premium Plan (1-hour)',
    npiCodeId: 'AaPedPremium',
    autoConnect: false,
    alternateCost: null,
    metadata: {
      assetKeySuffix: 'Hour',
      description: 'Only need Wi-Fi for a short time? Purchase our One-Hour Access plan for a quick dip in the clouds.'
    },
    duration: 3600
  },
  {
    description: 'Flight Pass',
    customerId: 5,
    active: true,
    deviceTypeMask: 0,
    deviceCategoryId: 'PED',
    id: 73,
    cost: 16.0,
    name: 'Aa PED LH Premium Plan (full-flight)',
    npiCodeId: 'AaPedPremium',
    autoConnect: false,
    alternateCost: null,
    metadata: {
      assetKeySuffix: 'Full',
      description: 'Have a long flight? Purchase our Flight-Time Package to stay connected the entire flight.'
    },
    duration: 84600
  }
];
const stories = storiesOf('WifiServiceOptions (CMOB-19579)', module);
stories.addDecorator(withKnobs);

stories
  .add('Default', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <WifiServiceOptions {...defaultProps} {...defaultLocaleProps} premiumTiers={premiumTiers} />
    </PortalComponentWrapper>
  ))
  .add('Demo portal (Home screen)', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <WifiServiceOptions {...propsDemoHome} {...demoHomeLocaleProps} premiumTiers={premiumTiers} />
    </PortalComponentWrapper>
  ))
  .add('Demo portal (WiFi Services page)', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <WifiServiceOptions {...propsDemoServicePage} {...demoServicePageLocaleProps} premiumTiers={premiumTiers} />
    </PortalComponentWrapper>
  ))
  .add('Neos portal (Home page)', () => (
    <PortalComponentWrapper mockTheme={mockThemeNeos}>
      <WifiServiceOptions {...propsNeosHome} {...neosHomePageLocaleProps} premiumTiers={premiumTiers} />
    </PortalComponentWrapper>
  ))
  .add('Neos portal (WiFi Service page)', () => (
    <PortalComponentWrapper mockTheme={mockThemeNeos}>
      <WifiServiceOptions {...propsNeosServicePage} {...neosServicePageLocaleProps} premiumTiers={premiumTiers} />
    </PortalComponentWrapper>
  ))
  .add('with Knob ', () => (
    <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
      <WifiServiceOptions
        {...defaultProps}
        componentTheme={object('componentTheme', defaultProps.componentTheme)}
        header={text('header', defaultLocaleProps.header)}
        body={text('body', defaultLocaleProps.body)}
        voucherTitle={text('voucherTitle', defaultLocaleProps.voucherTitle)}
        voucherText={text('voucherText', defaultLocaleProps.voucherText)}
        voucherDescripiton={text('voucherDescripiton', defaultLocaleProps.voucherDescripiton)}
        voucherButtonText={text('voucherButtonText', defaultLocaleProps.voucherButtonText)}
        feature={object('feature', defaultProps.feature)}
        premiumTiers={object('internal-premiumTiers', premiumTiers)}
      />
    </PortalComponentWrapper>
  ));
