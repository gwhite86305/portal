import {AxiosInstance} from 'axios';

export interface IAnalyticsPayload {
  url: string;
  method: 'get' | 'post' | 'put';
  data: any;
  headers?: any;
}

export interface IPFEAnalyticsPayload {
  msgId: string;
  version: string;
  time: number;
  message: string;
}

export interface IAnalyticsActionMeta {
  analytics: IAnalyticsPayload;
  customAxiosInstance?: AxiosInstance;
}

export interface ILogger {
  log: (log: any) => void;
  handleUnload: () => void;
  endpoint: string;
}

export interface IAnalyticsLogger {
  logger: ILogger;

  // These should be private (and therefore absent from this interface)
  // but typescript can't handle a singleton pattern with private methods for javascript reasons.
  metafyState: (target: any) => object;
  retrieveQueryParams: () => object;
  getClickData: (event: any) => any;

  // Public methods
  UserAgent: (userAgent: string) => void;
  TimingMetrics: (timingMetrics: object) => void;
  StateChange: (action: object) => void;
  PageView: (path: string, language: string) => void;
  ClickThrough: (clickEvent: object) => void;
  DeviceType: (deviceType: number) => void;
  MediaImpression: (mediaFileName: string) => void;
  AdImpression: (adUid: string) => void;
  AdClickThrough: (adUid: string) => void;
}
