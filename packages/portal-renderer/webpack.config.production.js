const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const gitRevisionPlugin = new GitRevisionPlugin();

module.exports = {
  name: 'production-app',
  mode: 'production',
  entry: {
    main: ['intersection-observer', 'babel-polyfill', './src/index.tsx']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[contenthash].js',
    chunkFilename: '[name].[contenthash].js',
    publicPath: '/'
  },
  optimization: {
    namedModules: true,
    noEmitOnErrors: true,
    concatenateModules: true,
    splitChunks: {
      chunks: 'async',
      cacheGroups: {
        vendors: false,
        default: false
      }
    },
    minimizer: [
      new UglifyJsPlugin({
        parallel: true,
        uglifyOptions: {
          compress: true,
          mangle: true,
          output: {
            comments: false
          },
          exclude: [/\.min\.js$/gi] // skip pre-minified libs
        }
      })
    ]
  },
  plugins: [
    new BundleAnalyzerPlugin({analyzerMode: 'disabled', generateStatsFile: true}),
    new CopyWebpackPlugin([{from: 'mocks/', to: 'mocks/'}], {debug: 'warning'}),
    new webpack.DefinePlugin({
      'process.env': {
        API_BUILDER_URL: JSON.stringify(process.env.API_BUILDER_URL),
        DEVICE_URL: JSON.stringify(process.env.DEVICE_URL),
        HASH_ROUTES: process.env.HASH_ROUTES,
        NODE_ENV: JSON.stringify(process.env.ENV),
        PCMS_ENV_BUILDER: JSON.stringify(process.env.PCMS_ENV_BUILDER),
        PCMS_PORTAL_BUILDER_API_ENDPOINT: JSON.stringify(process.env.PCMS_PORTAL_BUILDER_API_ENDPOINT),
        PROFILE_URL: JSON.stringify(process.env.PROFILE_URL),
        UXBUILDER_URL: JSON.stringify(process.env.UXBUILDER_URL),
        BUILD_DATETIME: JSON.stringify(new Date().toISOString()),
        GIT_VERSION: JSON.stringify(gitRevisionPlugin.version()),
        GIT_COMMIT_HASH: JSON.stringify(gitRevisionPlugin.commithash()),
        GIT_BRANCH: JSON.stringify(gitRevisionPlugin.branch())
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    }),
    new ExtractTextPlugin({
      filename: '[name].[hash:8].css',
      allChunks: true
    }),
    new ManifestPlugin(),
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /./,
      threshold: 10240,
      minRatio: 0.8
    })
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      react: path.resolve('../../node_modules/react'), // necessary due to npm being not greate a de-duping
      'react-dom': path.resolve('../../node_modules/react-dom'),
      '@material-ui': path.resolve('../../node_modules/@material-ui'),
      'core-js': path.resolve('../../node_modules/core-js'),
      'styled-components': path.resolve('../../node_modules/styled-components'),
      jss: path.resolve('../../node_modules/jss'),
      axios: path.resolve('../../node_modules/axios'),
      ramda: path.resolve('../../node_modules/ramda'),
      'react-intl': path.resolve('../../node_modules/react-intl'),
      'react-redux': path.resolve('../../node_modules/react-redux'),
      'react-router': path.resolve('../../node_modules/react-router'),
      'react-router-dom': path.resolve('../../node_modules/react-router-dom'),
      'react-router-redux': path.resolve('../../node_modules/react-router-redux'),
      redux: path.resolve('../../node_modules/redux'),
      'redux-actions': path.resolve('../../node_modules/redux-actions'),
      'redux-dynamic-reducer': path.resolve('../../node_modules/redux-dynamic-reducer'),
      'redux-thunk': path.resolve('../../node_modules/redux-thunk'),
      history: path.resolve('../../node_modules/history'),
      intl: path.resolve('../../node_modules/intl'),
      'intl-locales-supported': path.resolve('../../node_modules/intl-locales-supported'),
      soundmanager2: path.resolve('../../node_modules/soundmanager2/script/soundmanager2-nodebug-jsmin.js'),
      "lodash-es": path.resolve("../../node_modules/lodash-es"),
    }
  },
  module: {
    rules: [
      {
        // The loader that handles ts and tsx files.  These are compiled
        // with the ts-loader and the output is then passed through to the
        // babel-loader.  The babel-loader uses the es2015 and react presets
        // in order that jsx and es6 are processed.
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=es2015&presets[]=react!ts-loader'
      },
      {test: /\.css$/, loader: 'style-loader!css-loader'},

      {
        test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      },
      {
        test: /locale-default-message.json/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]'
            }
          }
        ]
      }
    ]
  }
};
