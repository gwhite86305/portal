import {IPFEAnalyticsPayload, IAnalyticsPayload} from './types';

/**
 * Compose Portal Analytics logger message objects for submission
 */
namespace Compose {
  const VERSION = 'v1';
  var ID3Count = 0;

  let messageTemplate = {
    msgId: 'None',
    version: VERSION,
    message: 'No Message'
  };

  /**
   * ID1: User Agent String
   * @param userAgent Contains device and browser information
   */
  export function ID1(userAgent: string) {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID1',
      message: userAgent
    };
  }

  /**
   * ID2: Timing metrics
   * @param timingMetrics Contains load-time information for the site
   */
  export function ID2(timingMetrics: string) {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID2',
      message: timingMetrics
    };
  }

  /**
   * ID3: State machine changes
   * Shipping all state changes here will allow us to reconstruct all actions on a page by a user.
   * @param action A State action, usually captured in middleware
   */
  export function ID3(action: object): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID3',
      message: JSON.stringify({
        index: ID3Count++,
        state: action
      })
    };
  }

  /**
   * ID4: Page View
   * @param pathname The name of page navigation
   * @param language The language of the page.  Defaults to 'en'
   */
  export function ID4(pathname: string, language: string = 'en'): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID4',
      message: JSON.stringify(arguments)
    };
  }

  /**
   * ID5 Clickthrough
   * @param clickthrough Path to log, generally the URL or xpath of a page click
   */
  export function ID5(clickThrough: string): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID5',
      message: JSON.stringify(arguments)
    };
  }

  /**
   * ID6 Detected Device
   * @param detectedDeviceTypeId Device type number.
   */
  export function ID6(detectedDeviceTypeId: number): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID6',
      message: JSON.stringify(arguments)
    };
  }

  /**
   * ID7: Impresions
   * @param mediaFileName File name of the image
   * @param mediaId Unsure of what this should be in the new portal
   * @param mediaHash SHA hash of the media file
   */
  export function ID7(mediaFileName: string, mediaId: number = 0, mediaHash: string = ''): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID7',
      message: JSON.stringify(arguments)
    };
  }

  /**
   * ID11: Advert Impresions
   * @param adUid ID of the viewed Ad
   */
  export function ID11(adUid: string): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID11',
      message: adUid
    };
  }

  /**
   * ID12: Advert click throughs
   * @param adUid ID of the viewed Ad
   */
  export function ID12(adUid: string): IPFEAnalyticsPayload {
    return {
      ...messageTemplate,
      time: Date.now(),
      msgId: 'ID12',
      message: adUid
    };
  }
}

export default Compose;
