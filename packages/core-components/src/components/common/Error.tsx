import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {ErrorAPIMessage} from 'component-utils/dist/utils/APIError';
import Localize from 'component-utils/dist/components/Localize';
import Button from '../Button/Button';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface Props {
  fullHeight?: boolean;
  message: ErrorAPIMessage;
}

const ErrorContainer = styled.div`
  color: ${(props: any) => props.theme.primaryTextColor};
  background-color: ${(props: any) => props.theme.primaryBackgroundColor};
  display: flex;

  .error-container-inner {
    flex: 0 1 auto;
    max-width: 270px;
    margin: 0 auto;
    align-self: center;
  }

  h1 {
    font-size: 1.6rem;
    margin-top: 2rem;
  }

  .error-container-status {
    text-align: right;
    font-size: 0.8rem;
    opacity: 0.5;
    margin: 1rem 0 0 0;
  }

  .error-container-message {
    word-wrap: break-word;
  }

  .error-container-internal-message {
    opacity: 0.5;
    font-style: italic;
    word-wrap: break-word;
    margin: 0.5rem 0 0 0;
  }
`;

const Error: React.FC<Props> = props => {
  const {message} = props;
  const {internalError} = message;

  return (
    <ErrorContainer
      className="error-container"
      style={
        props.fullHeight
          ? {
              height: '100vh',
              position: 'absolute',
              margin: '0 auto',
              left: '0',
              right: '0'
            }
          : {}
      }
    >
      <div className="error-container-inner">
        <h1>
          <Localize messageId="core.Error.header.title" />
        </h1>
        <div className="error-container-message">
          {message.friendlyMessage}
          {internalError ? <div className="error-container-internal-message">{internalError.message}</div> : null}
        </div>
        <div className="error-container-status">
          [{message.status} {internalError ? '- ' + internalError.code : null}]
        </div>

        <Button
          className="error-container-home-btn"
          onClick={() => {
            location.href = '/';
          }}
          isPrimary={true}
        >
          <Localize messageId="core.Error.button.home" />
        </Button>
      </div>
    </ErrorContainer>
  );
};

export default Error;
