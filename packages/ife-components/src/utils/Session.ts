const Session = {
  _token: '',
  headerKey: 'x-auth-token',
  getToken() {
    if (this._token) {
      return this._token;
    }
    try {
      this._token = localStorage.getItem(this.headerKey) || '';
    } catch {}
    return this._token;
  },
  setToken(tokenArg: string) {
    this._token = tokenArg;
    try {
      localStorage.setItem(this.headerKey, this._token);
    } catch {}
  }
};

export default Session;
