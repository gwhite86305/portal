import React from 'react';
import cloneDeep from 'lodash/cloneDeep';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {array, boolean, number, object, text, withKnobs} from '@storybook/addon-knobs/react';
import UserAgreement from '../dist/components/UserAgreement/UserAgreement.js';
import Accordion from '../../core-components/dist/components/Accordion/Accordion.js';
import PortalComponentWrapper, {
  mockTheme,
  mockThemeNeos,
  mockThemeIcelandair
} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    motifTextColor: 'theme.primaryTextColor',
    motifTextFontSize: 'theme.quinaryFontSize',
    motifTextFontWeight: 'normal',
    motifTextFontFamily: 'theme.secondaryFontFamily',
    motifTextMargin: {
      xs: '16px auto auto auto',
      md: '20px auto auto auto'
    },
    noticeHeaderTextColor: 'theme.secondaryTextColor',
    noticeHeaderFontSize: 'theme.tertiaryFontSize',
    noticeHeaderFontWeight: 'bold',
    noticeHeaderMargin: {xs: '40px auto auto auto'},
    noticeTextColor: 'theme.secondaryTextColor',
    noticeTextFontSize: 'theme.primaryFontSize',
    noticeTextFontWeight: 'normal',
    noticeTextMargin: {xs: '20px auto auto auto'},
    effectiveDateTextColor: 'theme.primaryTextColor',
    effectiveDateTextFontSize: 'theme.primaryFontSize',
    effectiveDateMargin: {xs: '40px auto 100px auto'},
    consentDeclarationTextColor: 'theme.primaryTextColor',
    consentDeclarationTextFontSize: 'theme.primaryFontSize',
    consentDeclarationHeaderFontSize: '24px',
    checkBoxWidth: '18px',
    checkBoxHeight: '18px',
    checkBoxBorderColor: 'theme.primaryBackgroundColor',
    checkBoxBorderRadius: '3px',
    checkedBackgroundColor: 'theme.primaryBackgroundColor',
    uncheckedBackgroundColor: 'theme.secondaryBackgroundColor',
    footerTextColor: 'theme.primaryBackgroundColor',
    footerBackgroundColor: 'theme.secondaryBackgroundColor',
    buttonWidth: '184px',
    buttonFontWeight: 'bold',
    lineHeight: 'normal',
    footerTextSpanPaddingLeft: '14px'
  },
  feature: {
    headerImage: '',
    openPageOnAccept: '/',
    includeAddtionalCheckbox: false,
    useMaterial: false,
    showPrivacyPolicy: true,
    showConsentDeclaration: false
  },
  content: {
    header: {
      motifText: 'User Agreement',
      noticeHeader: 'IMPORTANT',
      noticeText: 'PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION.'
    },
    effectiveDate: {
      effectiveDateText: 'Effective Date: May 7, 2018'
    },
    agreements: {
      agreementCheckOneText:
        'By checking this box and selecting Agree you accept Viasat’s Terms of Service Agreement and Consent Declaration, and you also accept Viasat’s Privacy Policy and Acceptable Use Policy.',
      agreementCheckTwoText:
        'By checking this box and selecting Agree you provide consent for Viasat to collect certain information for troubleshooting / debug purposes.',
      buttonText: 'AGREE TO TERMS'
    },
    terms: {
      termsHeader: 'Terms of service',
      termsSections: [
        {
          title: 'Section title',
          isOpen: false,
          paragraphs: ['Paragraph 1', 'Paragraph 2']
        }
      ]
    },
    privacy: {
      privacyHeader: 'Privacy Policy',
      privacySections: [
        {
          title: 'Section title',
          isOpen: false,
          paragraphs: ['Paragraph 1', 'Paragraph 2']
        }
      ]
    },
    consentDeclaration: {
      consentDeclarationHeader: 'Consent Declaration',
      consentDeclarationContent: '<p>paragraph 1</p><p>paragraph 2</p>'
    }
  }
};

const accordionDefaultProps = {
  componentTheme: {
    textColor: 'theme.secondaryTextColor',
    headerTextColor: 'theme.secondaryTextColor',
    headerFontSize: 'theme.primaryFontSize',
    seperatorColor: '#1694CA',
    sectionTextColor: 'theme.secondaryTextColor',
    sectionFontSize: 'theme.primaryFontSize',
    toggleOpenIconColor: 'theme.primaryTextColor',
    toggleClosedIconColor: 'theme.secondaryTextColor'
  }
};

const sampleProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor'
  },
  feature: {
    headerImage: '',
    openPageOnAccept: '/',
    includeAddtionalCheckbox: true,
    useMaterial: false
  },
  content: {
    header: {
      motifText: 'User Agreement',
      noticeHeader: 'IMPORTANT',
      noticeText: 'PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION.'
    },
    effectiveDate: {
      effectiveDateText: 'Effective Date: May 7, 2018'
    },
    agreements: {
      agreementCheckOneText:
        'By checking this box and selecting Agree you accept Viasat’s Terms of Service Agreement and Consent Declaration, and you also accept Viasat’s Privacy Policy and Acceptable Use Policy.',
      agreementCheckTwoText:
        'By checking this box and selecting Agree you provide consent for Viasat to collect certain information for troubleshooting / debug purposes.',
      buttonText: 'AGREE TO TERMS'
    },
    terms: {
      termsHeader: 'Terms of Service',
      termsSections: [
        {
          title: 'Introduction.',
          isOpen: false,
          paragraphs: [
            'This Terms of Service Agreement (the \u201CAgreement\u201D) is between you and Viasat, Inc. (\u201CViasat,\u201D \u201CService Provider,\u201D \u201Cus,\u201D or \u201Cwe\u201D), and governs your use of WiFi and internet services provided by Viasat (the \u201DService\u201D) to which you are connecting on the airline operator\'s aircraft from which you are connecting to such Service (the \u201CAirline\u201D) (collectively the "Service"). Your acceptance below and continued use of the Service represents your agreement to the terms set forth in this Agreement. If you do not agree with the terms set forth in this Agreement, immediately cease using the Service. If you would like to contact Service Provider, you may write to:',
            'Viasat, Inc.<br/>2908 Finfeather Road<br/>Bryan, TX 77801<br/>USA'
          ]
        },
        {
          title: 'Using The Service.',
          isOpen: false,
          paragraphs: [
            'In exchange for access to and use of the Service, you: (a) agree to provide Service Provider with accurate and complete registration information, if requested, and to notify Service Provider of changes to your registration information; (b) agree to protect the password, username and security information you use to access the Service and to notify Service Provider immediately of any unauthorized use of your account that you become aware of; (c) agree to comply with applicable laws and regulations, including but not limited to copyright and intellectual property rights laws; and (d) represent that you are at least 18 years of age.'
          ]
        },
        {
          title: 'Billing Terms and Payment.',
          isOpen: false,
          paragraphs: [
            'General Billing Terms.  A billing period generally starts on the day and time you log in following the completion of the registration process (the \u201CBilling Commencement Date\u201D). The Billing Commencement Date will be the day and time you log in. You will be logged off of the Service when (i) you click the \u201CLogout\u201D button (\u201CIndividual Logout\u201D); or (ii) the system automatically logs you off because your session time has expired, your device has been powered off or has been inactive for an extended period of time, or Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination (\u201CAutomatic Logout\u201D).  The billing period ends on upon the earliest to occur of an Individual Logout or Automatic Logout.',
            'Single Session Pay Per Use Plan.  At the start of each session, we will charge all fees related to your use of the Service, including taxes, surcharges or other assessments applicable to the Service (\u201CService Fees\u201D) to your credit card, debit card or other payment method (a \u201CCard Payment\u201D).',
            'Pay Per Flight Plan.  Each pay per flight session begins on the Billing Commencement Date and ends at the point in time when Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination. At the start of each session, we will charge all Service Fees related to your use of the Service to your Card Payment.',
            'Roaming Fees.  If you are a subscriber of another service provider that has a contractual relationship allowing that service provider\u2019s subscribers to roam on Viasat\u2019s Wi-Fi network, your service provider may charge you a roaming fee for access to Viasat\u2019s Wi-Fi network.',
            'Payment Terms.  You agree to pay all Service Fees in accordance with the provisions of the Service plan you selected. You authorize Service Provider to charge your Card Payment for payment of all, or any portion of your Service fees, until such amounts are paid in full. Your card issuer agreement governs use of your Card Payment in connection with this Service; please refer to that agreement for your rights and liabilities as a cardholder. If we do not receive payment from your credit or debit card issuer or its agent, you agree to pay us all amounts due upon demand by us. You agree that we will not be responsible for any expenses that you may incur resulting from overdrawing your bank account or exceeding your credit limit as a result of an automatic charge made under this Agreement. ',
            'Billing Errors and Collections.  If you think a charge is incorrect or you need more information on any charges applied to your account, you should contact us through inflight.Viasat.com or by calling +(00) 1 888-649-6711 within 60 days of receiving the statement on which the error or problem appeared. We will not pay you interest on any overcharged amounts later refunded or credited to you.  If we choose to use a collection agency or attorney to collect money that you owe us or to assert any other right that we may have against you, you agree to pay the reasonable costs of collection or other action including, without limitation, collection agency fees, reasonable attorneys\u2019 fees, and court costs. '
          ]
        }
      ]
    },
    privacy: {
      privacyHeader: 'Privacy Policy',
      privacySections: [
        {
          title: 'Introduction.',
          isOpen: false,
          paragraphs: [
            'Viasat, Inc., 2908 Finfeather Road, Bryan, TX 77801, UNITED STATES and its subsidiaries and affiliates, a list of which can be found at Viasat.com/legal, (collectively, <strong>\u201CViasat,\u201D \u201Cwe,\u201D \u201Cus,\u201D or \u201Cour\u201D </strong>) respect and value your privacy.  This Privacy Policy (<strong>\u201CPolicy\u201D</strong>) provides you with notice regarding how we collect data about you in connection with the Services (defined below), and how we use and share it, including the choices we offer with respect to that information, and applies to your use of any of our services posting a copy of this Policy, and in particular the Viasat inflight Wi-Fi Internet service and related products and services provided by us to which you are connecting on the airline operator\u2019s aircraft from which you are connecting to such service (<strong>\u201CAirline\u201D</strong>) (collectively the <strong>\u201CServices\u201D</strong>), regardless of how you access or use it, whether via computer, mobile device or otherwise.  It does not apply to Viasat\u2019s data collection activities offline or otherwise outside of the Services (unless otherwise stated below or at the time of collection).  This Policy may be supplemented by additional privacy notices posted on the Services, including addenda applicable to Services provided outside of the United States (in each such instance, and collectively the <strong>\u201CAdditional Privacy Terms\u201D</strong>'
          ]
        }
      ]
    }
  }
};

const icelandProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    motifTextColor: 'theme.secondaryTextColor',
    motifTextFontSize: '32px',
    motifTextFontWeight: '500',
    motifTextMargin: {xs: '20px auto auto auto'},
    motifTextFontFamily: 'theme.appFontFamily',
    noticeHeaderTextColor: 'theme.primaryTextColor',
    noticeHeaderFontSize: '24px',
    noticeHeaderFontWeight: 'bold',
    noticeHeaderMargin: {xs: '40px auto 20px auto'},
    noticeTextColor: 'theme.secondaryTextColor',
    noticeTextFontSize: 'theme.primaryFontSize',
    noticeTextFontWeight: 'normal',
    noticeTextMargin: {xs: '20px auto 0px auto'},
    effectiveDateTextColor: 'theme.secondaryTextColor',
    effectiveDateTextFontSize: 'theme.primaryFontSize',
    effectiveDateMargin: {xs: '40px auto 100px auto'},
    checkBoxWidth: '21px',
    checkBoxHeight: '20px',
    checkBoxBorderColor: '#444444',
    checkBoxBorderRadius: '0px',
    checkBoxLeft: {xs: '28px'},
    checkedBackgroundColor: 'theme.secondaryBackgroundColor',
    uncheckedBackgroundColor: 'theme.secondaryBackgroundColor',
    footerTextColor: 'theme.secondaryTextColor',
    footerTextSpanPaddingLeft: '25px',
    buttonWidth: '200px',
    buttonFontWeight: 'normal',
    lineHeight: '1.55'
  },
  feature: {
    headerImage: {
      xs: './images/cloudsUA_Mobile.svg',
      sm: './images/cloudsUA_Tablet.svg',
      lg: './images/cloudsUA_Laptop.svg'
    },
    openPageOnAccept: '',
    includeAddtionalCheckbox: true,
    useMaterial: true
  }
};
icelandProps.content = cloneDeep(sampleProps.content);
icelandProps.content.agreements.buttonText = 'Continue';
icelandProps.content.effectiveDate.effectiveDateText = '';

const icelandAccordionProps = {
  componentTheme: {
    textColor: 'theme.secondaryTextColor',
    headerTextColor: 'theme.secondaryTextColor',
    headerFontSize: '24px',
    seperatorColor: 'theme.primaryDividerColor',
    sectionTextColor: 'theme.secondaryTextColor',
    sectionFontSize: 'theme.primaryFontSize',
    toggleOpenIconColor: 'theme.secondaryTextColor',
    toggleClosedIconColor: 'theme.secondaryTextColor'
  }
};

mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles = "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';

mockThemeNeos.fontIconFontFamily = "'FontAwesome'";
mockThemeNeos.fontIconFiles = "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2');";
mockThemeNeos.fontIconCss = './fonts/font-awesome/css/font-awesome.css';

mockThemeIcelandair.fontIconFontFamily = "'FontAwesome'";
mockThemeIcelandair.fontIconFiles = "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2');";
mockThemeIcelandair.fontIconCss = './fonts/font-awesome/css/font-awesome.css';

const stories = storiesOf('User Agreement', module);
stories.addDecorator(withKnobs);

const disablePrivacyPolicy = cloneDeep(defaultProps);
disablePrivacyPolicy.feature.showPrivacyPolicy = false;

stories
  .add('[CMOB-16522] Default Props', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <UserAgreement {...defaultProps}>
        {[
          <div>
            <Accordion {...accordionDefaultProps} />
          </div>
        ]}
      </UserAgreement>
    </PortalComponentWrapper>
  ))
  .add('[CMOB-16522] Without effective date', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.effectiveDate.effectiveDateText = '';

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] Without notice', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.header.noticeHeader = '';
    testProps.content.header.noticeText = '';

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] Without terms', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.terms.termsHeader = '';
    testProps.content.terms.termsSections = [];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] Without privacy policy', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.privacy.privacyHeader = '';
    testProps.content.privacy.privacySections = [];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] With Header image', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.feature.headerImage = {
      xs: './images/ua-background.svg'
    };

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] With Header image and three terms sections', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.feature.headerImage = {
      xs: './images/ua-background.svg'
    };
    testProps.content.terms.termsSections = [
      {
        title: 'Introduction.',
        isOpen: false,
        paragraphs: [
          'This Terms of Service Agreement (the \u201CAgreement\u201D) is between you and Viasat, Inc. (\u201CViasat,\u201D \u201CService Provider,\u201D \u201Cus,\u201D or \u201Cwe\u201D), and governs your use of WiFi and internet services provided by Viasat (the \u201DService\u201D) to which you are connecting on the airline operator\'s aircraft from which you are connecting to such Service (the \u201CAirline\u201D) (collectively the "Service"). Your acceptance below and continued use of the Service represents your agreement to the terms set forth in this Agreement. If you do not agree with the terms set forth in this Agreement, immediately cease using the Service. If you would like to contact Service Provider, you may write to:',
          'Viasat, Inc.<br/>2908 Finfeather Road<br/>Bryan, TX 77801<br/>USA'
        ]
      },
      {
        title: 'Using The Service.',
        isOpen: false,
        paragraphs: [
          'In exchange for access to and use of the Service, you: (a) agree to provide Service Provider with accurate and complete registration information, if requested, and to notify Service Provider of changes to your registration information; (b) agree to protect the password, username and security information you use to access the Service and to notify Service Provider immediately of any unauthorized use of your account that you become aware of; (c) agree to comply with applicable laws and regulations, including but not limited to copyright and intellectual property rights laws; and (d) represent that you are at least 18 years of age.'
        ]
      },
      {
        title: 'Billing Terms and Payment.',
        isOpen: false,
        paragraphs: [
          'General Billing Terms.  A billing period generally starts on the day and time you log in following the completion of the registration process (the \u201CBilling Commencement Date\u201D). The Billing Commencement Date will be the day and time you log in. You will be logged off of the Service when (i) you click the \u201CLogout\u201D button (\u201CIndividual Logout\u201D); or (ii) the system automatically logs you off because your session time has expired, your device has been powered off or has been inactive for an extended period of time, or Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination (\u201CAutomatic Logout\u201D).  The billing period ends on upon the earliest to occur of an Individual Logout or Automatic Logout.',
          'Single Session Pay Per Use Plan.  At the start of each session, we will charge all fees related to your use of the Service, including taxes, surcharges or other assessments applicable to the Service (\u201CService Fees\u201D) to your credit card, debit card or other payment method (a \u201CCard Payment\u201D).',
          'Pay Per Flight Plan.  Each pay per flight session begins on the Billing Commencement Date and ends at the point in time when Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination. At the start of each session, we will charge all Service Fees related to your use of the Service to your Card Payment.',
          'Roaming Fees.  If you are a subscriber of another service provider that has a contractual relationship allowing that service provider\u2019s subscribers to roam on Viasat\u2019s Wi-Fi network, your service provider may charge you a roaming fee for access to Viasat\u2019s Wi-Fi network.',
          'Payment Terms.  You agree to pay all Service Fees in accordance with the provisions of the Service plan you selected. You authorize Service Provider to charge your Card Payment for payment of all, or any portion of your Service fees, until such amounts are paid in full. Your card issuer agreement governs use of your Card Payment in connection with this Service; please refer to that agreement for your rights and liabilities as a cardholder. If we do not receive payment from your credit or debit card issuer or its agent, you agree to pay us all amounts due upon demand by us. You agree that we will not be responsible for any expenses that you may incur resulting from overdrawing your bank account or exceeding your credit limit as a result of an automatic charge made under this Agreement. ',
          'Billing Errors and Collections.  If you think a charge is incorrect or you need more information on any charges applied to your account, you should contact us through inflight.Viasat.com or by calling +(00) 1 888-649-6711 within 60 days of receiving the statement on which the error or problem appeared. We will not pay you interest on any overcharged amounts later refunded or credited to you.  If we choose to use a collection agency or attorney to collect money that you owe us or to assert any other right that we may have against you, you agree to pay the reasonable costs of collection or other action including, without limitation, collection agency fees, reasonable attorneys\u2019 fees, and court costs. '
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] External link and bold styled text in the introduction paragraph', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.feature.headerImage = {
      xs: './images/ua-background.svg'
    };
    testProps.content.terms.termsSections = [
      {
        title: 'Introduction.',
        isOpen: false,
        paragraphs: [
          'I am an <a href="https://www.google.com/">external link</a>. Looks like we can make <strong> text</strong> & link <strong>  <a href="https://www.google.com/">strong</a></strong> '
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] Sanitize bad text in the introduction paragraph', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.feature.headerImage = {
      xs: './images/ua-background.svg'
    };
    testProps.content.terms.termsSections = [
      {
        title: 'Introduction.',
        isOpen: false,
        paragraphs: [
          'There is a script tag with alert here( <script>alert();</script> ). If nothing is showing up inside the bracket then the html sanitizer is working properly'
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] User Agreement - Demo Portal theme', () => {
    sampleProps.componentTheme = defaultProps.componentTheme;
    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <UserAgreement {...sampleProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] User Agreement - Neos Portal theme', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockThemeNeos)}>
        <UserAgreement
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          feature={object('feature', sampleProps.feature)}
          content={object('content', sampleProps.content)}
        >
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('[CMOB-16522] User Agreement - Icelandair Portal theme', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockThemeIcelandair)}>
        <UserAgreement {...icelandProps}>
          {[
            <div>
              <Accordion {...icelandAccordionProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('CMOB-16522: With Knob', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <UserAgreement
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          feature={object('feature', sampleProps.feature)}
          content={object('content', sampleProps.content)}
        >
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('Disabled Privacy Policy', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <UserAgreement {...disablePrivacyPolicy}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  })
  .add('Enabled Consent Declaration', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.feature.showConsentDeclaration = true;
    testProps.content.consentDeclaration.consentDeclarationContent =
      "<p>Pursuant to the European Union General Data Protection Regulation 2016/679 (<strong>\u201CEU GDPR\u201D</strong>), Viasat (the <strong>\u201CCompany\u201D</strong>), in its capacity as a data controller under the EU GDPR, is requesting your explicit, affirmative consent to certain processing activities involving your personal data.</p><p>By checking <strong>\u201CI have read and accept this Agreement\u201D</strong> below, you consent to the collection, processing, and use of personal data by Viasat, Inc.,  3902 South Traditions Drive, College Station, TX 77845, UNITED STATES (<strong>\u201CViasat\u201D</strong>), as described in the Viasat Inflight WiFi Privacy Policy (the <strong>\u201CPrivacy Policy\u201D</strong>), including a transfer and storage within Viasat\u2019s database(s), for the purposes of providing the Viasat in-flight WiFi internet service and related products and services (the <strong>\u201CPurpose\u201D</strong>) (collectively the <strong>\u201CServices\u201D</strong>).</p><p>Generally, \u201Cpersonal data\u201D (or \u201Cpersonal information\u201D as the term is used in the Privacy Policy) means information about an identified or identifiable natural person. The types of personal data we may process about you include, but is not limited to, your first and last name, e-mail address, IP addresses, and/or zip code.</p><p>Viasat may process your personal data as required to provide you the Services. For example, Viasat will use your personal information to process your payment for the Service and to identify your personal device to provide you with the Service.</p><p>Viasat will also share your personal data with third parties in order to provide you the Services. Third parties that Viasat may share your personal data with may include partners, vendors, or others who perform functions on our behalf related to the Services. Viasat will require any third party that processes your personal data to comply with the terms and conditions of this Consent Declaration.</p><p>Viasat may also process your personal data as necessary to comply with applicable laws, to protect your vital interests, and to fulfil goals related to the Viasat’s legitimate interests in providing you the Services (to the extent that such interests are not outweighed by your interests in protecting your personal data). Unless otherwise required by law, personal data will be handled and processed only by the persons who are responsible for the necessary activities for the Purpose.</p><p>Viasat is committed to ensuring the security of your information. We have put in place reasonable physical, technical, and administrative safeguards designed to prevent unauthorized access to your information.</p><p>Furthermore, you acknowledge and confirm that your personal data will be shared with Viasat offices in California, India, and in the United States, and Icelandair, ehf., Amazon Web Services, Inc., Salesforce.com, Inc., CMC Americas, Inc., and Eutelsat SA, which may be located outside your country of residence and may have a different level of data protection than your country of residence, all in accordance with the Privacy Policy.</p><p>You have the right to withdraw your consent to the collection and processing of personal data at any time.  If you would like to withdraw consent, please contact via e-mail <a href='mailto:privacy@Viasat.com'>privacy@Viasat.com</a>.</p>";
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <UserAgreement {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </UserAgreement>
      </PortalComponentWrapper>
    );
  });
