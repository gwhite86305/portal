import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty,
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export default styled.div`
  ${() => applyMediaQueriesForStyle('width', {xs: '90%', sm: '90%', md: '600px'}, {})};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalPadding, props.theme)};
  ${renderThemeProperty('categoryTitleColor', 'color')};
  ${renderThemeProperty('itemTitleFontSize', 'font-size')};

  margin: 40px auto;
  text-align: center;
`;
