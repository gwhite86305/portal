import * as React from 'react';
import {renderThemeProperty, applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';

import styled from 'styled-components';

const StyledCarousel = styled.div`
  height: 100%;

  .slider,
  .slider-list,
  .slider-frame,
  .slider-slide {
    height: 100% !important;
  }

  button {
    -webkit-appearance: none;
    background: none;
    border-style: outset;
    border: none;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
  }

  /* LEFT - RIGHT arrow style */
  .slider-control-centerleft,
  .slider-control-centerright {
    ${(props: any) => applyMediaQueriesForStyle('top', props.theme.componentTheme.arrowTopPosition, props.theme, true)};
    z-index: 100;
  }

  .control-container {
    align-items: center;
    background-color: unset;
    cursor: pointer;
    display: flex;
    justify-content: center;
  }

  .arrow {
    ${renderThemeProperty('arrowsHeight', 'padding')} ${renderThemeProperty(
  'arrowsMargin',
  'margin'
)} ${renderThemeProperty('arrowsColor', 'border-color')} ${renderThemeProperty('arrowsThikness', 'border-right-width')};
    ${renderThemeProperty('arrowsThikness', 'border-bottom-width')} border-right-style: solid;
    border-bottom-style: solid;
    border-top-width: 0px;
    border-left-width: 0px;
    cursor: pointer;
    display: inline-block;
    &--left {
      transform: rotate(135deg) translate(-2.5px, -2.5px);
      -webkit-transform: rotate(135deg) translate(-2.5px, -2.5px);
    }
    &--right {
      transform: rotate(-45deg) translate(-2.5px, -2.5px);
      -webkit-transform: rotate(-45deg) translate(-2.5px, -2.5px);
    }
  }

  /* Bullet buttons  style */
  .slider-control-bottomcenter {
    z-index: 99;
    align-items: center;
    bottom: 0px;
    display: flex;
    justify-content: center;
    left: unset;
    position: absolute;
    transform: unset;
    width: 100%;
    ${renderThemeProperty('bulletButtonBarColor', 'background-color')};
  }

  .bottom-center-controls {
    display: flex;
    flex-direction: row;
    margin: 0;
    padding: 0;
    overflow: hidden;

    .bullet-button {
      border-radius: 50%;
      border-style: solid;
      ${renderThemeProperty('bulletButtonsBorderColorThikness', 'border-width')};
      ${renderThemeProperty('bulletButtonsBorderColor', 'border-color')};
      ${renderThemeProperty('bulletButtonsColor', 'background-color')};
      ${renderThemeProperty('bulletButtonsSize', 'padding')} ${renderThemeProperty('bulletButtonsMargin', 'margin')};
    }
    .selected {
      ${renderThemeProperty('bulletButtonsSelectedColor', 'background-color')};
    }
  }
`;

export default StyledCarousel;
