import LocalMovies from '@material-ui/icons/LocalMovies';
import MusicNote from '@material-ui/icons/MusicNote';
import {path, pathOr} from 'ramda';
import {AudioItem} from '../../store/audio/types';
import {VideoItem, TYPES} from '../../store/videos/types';
import EpisodeFormatter from './EpisodeFormatter';

import * as React from 'react';

const DisplayIcon = (props: any) => {
  if (props.item.type === TYPES.VIDEO_ITEM) {
    return <LocalMovies />;
  } else {
    return <MusicNote />;
  }
};

const PrimaryRow = (props: any) => {
  const primaryString = (item: AudioItem | VideoItem) => {
    let outputText: any = <span />;
    if (item.type === TYPES.VIDEO_ITEM && !(item.episode > 0)) {
      outputText = <span>{item.title}</span>;
    } else if (item.type === TYPES.VIDEO_ITEM && item.episode > 0) {
      outputText = (
        <span>
          <EpisodeFormatter episode={item} /> {item.title}
        </span>
      );
    } else {
      outputText = (
        <span>
          {item.title} by <span className="artist">{pathOr('', ['artist'], item)}</span>
        </span>
      );
    }
    return outputText;
  };
  return <div className="title primary-info">{primaryString(props.item)}</div>;
};

const SecondaryRow = (props: any) => {
  const secondaryString = (item: AudioItem | VideoItem) => {
    let outputText: any = <span />;
    if (item.type === TYPES.VIDEO_ITEM && item.episode > 0) {
      outputText = <span>{item.subtitle}</span>;
    } else if (item.group) {
      outputText = <span>{item.group.title}</span>;
    } else {
      outputText = <span />;
    }
    return outputText;
  };
  return (
    <div className="description-type-artist secondary-info">
      <DisplayIcon item={props.item} />
      {secondaryString(props.item)}
    </div>
  );
};

const ListItemText = (props: any) => {
  let item = props.item;
  return (
    <div className="description-body">
      <PrimaryRow item={item} />
      <SecondaryRow item={item} />
    </div>
  );
};

export default ListItemText;
