import * as React from 'react';
import {utils} from 'component-utils';
import {DeviceSwap as ExternalInterface} from '../common/ComponentsInterface';
import DeviceSwap, {InternalInterface} from './DeviceSwap';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {requestDeviceSwap, deviceSwapped, resetSwapDevice} from '../../store/WifiServiceOptions/actions';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
const querySearch = require('stringquery');
const connectWrapper = utils.connectWrapper.default;

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onSwapDeviceSubmit(email: string, tierId: number): any;
  onCancelButtonClick(): any;
  onDeviceSwap(): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const defaultTier =
      (state.profile &&
        state.profile.serviceTiers &&
        state.profile.serviceTiers.filter(
          (tier: any) => tier.npiCodeId && ~tier.npiCodeId.toLowerCase().indexOf('default')
        )[0]) ||
      {};
    let premiumTiers =
      (state.profile &&
        state.profile.serviceTiers &&
        state.profile.serviceTiers.filter((tier: any) => tier.cost > 0)) ||
      [];
    const connected =
      state.device &&
      defaultTier &&
      Object.keys(defaultTier).length > 0 &&
      state.device.serviceTierId &&
      state.device.serviceTierId != defaultTier.id;
    const WifiServiceOptions = state.WifiServiceOptions;

    return {premiumTiers, connected, WifiServiceOptions};
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onSwapDeviceSubmit: email => {
        dispatch(requestDeviceSwap(email));
      },
      onCancelButtonClick: () => {
        if (ownProps.feature.modal) {
          ownProps.history.push({pathname: ownProps.location.pathname, search: ''});
        } else {
          ownProps.history.push({pathname: '/', search: ''});
        }
        dispatch(resetSwapDevice());
      },
      onDeviceSwap: () => {
        dispatch(deviceSwapped());
      }
    };
  }
)
class DeviceSwapContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }

  public componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
    const prevConnected = prevProps.connected;
    const {connected} = this.props;

    if (this.props.WifiServiceOptions && this.props.WifiServiceOptions.deviceSwap && !prevConnected && connected) {
      this.props.onDeviceSwap();
      this.props.history.push({pathname: '/' + this.props.feature.openPageWhenConnected, search: ''});
    }
  }

  public render() {
    const {intl, componentUid, feature} = this.props;
    const queryParams = querySearch(this.props.location.search);
    const display = !feature.modal || (feature.modal && queryParams.swapmodal && queryParams.swapmodal == 'open');

    const localizedProps = localizeToProperties(intl, componentUid, [
      {headerText: 'ifc.DeviceSwap.headerText'},
      {bodyText: 'ifc.DeviceSwap.bodyText'},
      {emailLabel: 'ifc.DeviceSwap.emailLabel'},
      {emailErrorHelpText: 'ifc.DeviceSwap.emailErrorHelpText'},
      {submitButtonText: 'ifc.DeviceSwap.submitButtonText'},
      {cancelButtonText: 'ifc.DeviceSwap.cancelButtonText'},
      {connectedDeviceHeaderText: 'ifc.DeviceSwap.connectedDeviceHeaderText'},
      {connectedDevicebodyText: 'ifc.DeviceSwap.connectedDevicebodyText'},
      {connectedDeviceAlertButtonText: 'ifc.DeviceSwap.connectedDeviceAlertButtonText'},
      {errorMessageOnFormHeader: 'ifc.DeviceSwap.errorMessageOnFormHeader'},
      {errorMessageInvalidEmail: 'ifc.DeviceSwap.errorMessageInvalidEmail'},
      {errorMessageGeneric: 'ifc.DeviceSwap.errorMessageGeneric'},
      {errorIcon: 'ifc.DeviceSwap.errorIcon'}
    ]);
    return display ? <DeviceSwap {...this.props} {...localizedProps} /> : null;
  }
}

export default injectIntl(withRouter(DeviceSwapContainer));
