import * as React from 'react';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {FlightTimeRemaining as ExternalInterface} from '../common/ComponentsInterface';
import { renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {Plane, ComponentWrapper, TimeToDestination} from './common_utils';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  flightOrigin?: string;
  flightDestination?: string;
  flightMinutesRemaining?: number;
  percentageOfFlightComplete?: number;
}

const FlightDestination = (props: any) => {
  const {percentageOfFlightComplete, flightDestination} = props;
  const Container = styled.div`
    ${renderThemeProperty('locationLabelFontSize', 'font-size')};
    ${renderThemeProperty('locationLabelFontWeight', 'font-weight')};
    ${renderThemeProperty('locationLabelColor', 'color')};
    text-align: center;
    justify-content: space-evenly;
  `;
  return (
    <Container>
      <span>{flightDestination || '---'}</span>
    </Container>
  );
};

const FlightOrigin = (props: any) => {
  const Container = styled.div`
    ${renderThemeProperty('locationLabelFontSize', 'font-size')};
    ${renderThemeProperty('locationLabelFontWeight', 'font-weight')};
    ${renderThemeProperty('locationLabelColor', 'color')};
    text-align: center;
    justify-content: space-evenly;
    ${renderThemeProperty('originMarginRight', 'margin-right')};
  `;
  const {flightOrigin} = props;
  return (
    <Container>
      <span>{flightOrigin || '---'}</span>
    </Container>
  );
};

const ProgressBar = (props: any) => {
  const Container = styled.div`
    flex-grow: 1;
    display: flex;
    align-items: center;
    ${renderThemeProperty('progressMarginRight', 'margin-right')};

    .progress-bar {
      ${renderThemeProperty('progressBarBackgroundColor', 'background')};
      position: relative;
      ${renderThemeProperty('progressBarHeight', 'height')};
      ${renderThemeProperty('progressBarBorderRadius', 'border-radius')};
      width: 100%;
    }
    .progress-filler {
      ${renderThemeProperty('progressBarForegroundColor', 'background')};
      ${renderThemeProperty('progressBarBorderRadius', 'border-radius')};
      height: 100%;
      float: left;
      position: relative;
    }
  `;

  const Filler = (props: any) => {
    const styles = {
      width: `${props.percentageOfFlightComplete}%`,
      marginRight: `${props.componentTheme.progressBarFlexFillerMarginRight}`
    };
    return <div className="progress-filler" style={styles} />;
  };
  const {flightCutoffPercentage} = props.feature;
  return (
    <Container>
      <div className="progress-bar">
        <Filler
          percentageOfFlightComplete={
            props.percentageOfFlightComplete < flightCutoffPercentage ? props.percentageOfFlightComplete : 100
          }
          {...props}
        />
        <Plane {...props} />
      </div>
    </Container>
  );
};

class FlightTimeRemainingFlex extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    flightOrigin: '---',
    flightDestination: '---',
    flightMinutesRemaining: 0,
    percentageOfFlightComplete: 0
  };

  public render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <ComponentWrapper>
          <div className="ifc-flighttimeremaining-component ">
            <div className="ifc-flighttimeremaining-progress">
              <FlightOrigin {...this.props} />
              <ProgressBar {...this.props} />
              <TimeToDestination {...this.props} />
              <FlightDestination {...this.props} />
            </div>
          </div>
        </ComponentWrapper>
      </PortalComponent>
    );
  }
}

export default FlightTimeRemainingFlex;
