import * as React from 'react';
import { PortalComponent, utils } from 'component-utils';
import { Modal as ExternalInterface } from '../common/ComponentsInterface';
import ReactModal = require('react-modal');
import * as defaults from './defaultProps.json';
import styled from 'styled-components';
const { CommonComponentThemeWrapper } = utils.ComponentUtils;
const { renderThemeProperty, applyMediaQueriesForStyle } = utils.styledComponents;
export interface InternalInterface {
  className?: string;
  onModalClose?: any;
}
const Container = styled(CommonComponentThemeWrapper)`
 ${(props: any) => applyMediaQueriesForStyle('display', props.theme.componentTheme.display, props.theme)};
  :focus{
    outline:none !important;
  }
  div.overlay {
    width: 100%;
    ${renderThemeProperty('overlayColor', 'background-color')};
    ${renderThemeProperty('overlayOpacity', 'opacity')};
    align-items: center;
    justify-content: center;
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow: scroll;
  }
`;

class Modal extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  constructor(props: any) {
    super(props);
    this.state = {
      showModal: props.feature.visibleOnLoad
    };

    this.handleonModalClose = this.handleonModalClose.bind(this);
  }
  handleonModalClose(event: any) {
    this.props.onModalClose();
    this.setState({ showModal: false });
  }
  public componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
    if (this.props.feature.dynamic) {
      if (!this.state.showModal && this.props.feature.id === this.props.modal.displayedModalId) {
        this.setState({ showModal: true });
      }
      // Hide the modal when the state's displayedModalId value is cleared or changed
      else if (this.state.showModal && this.props.feature.id !== this.props.modal.displayedModalId) {
        this.setState({ showModal: false });
      }
    }
  }
  public render() {
    const { componentTheme, feature } = this.props;
    let zIndex = componentTheme && componentTheme.zIndex
    const customStyles = {
      overlay: {
        backgroundColor: 'none',
        zIndex: zIndex + 1,
      },
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        border: 'none',
        backgroundColor: 'transparent',
        borderRadius: componentTheme && componentTheme.modalRadius,
        width: feature.fullscreen ? "100vw" : "auto",
        height: feature.fullscreen ? "100vh" : "auto",
        padding: '0px'
      }
    };

    
    const contentWidth = feature.fullscreen ? "100%" : "auto";
    const contentHeight =  feature.fullscreen ? "100%" : "auto";

    return <PortalComponent componentTheme={this.props.componentTheme}>
      <Container className="core-modal">
        {this.state.showModal ? <div className="overlay" style={{ zIndex: zIndex }} /> : null}
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          style={customStyles}
          shouldCloseOnOverlayClick={feature.shouldCloseOnOverlayClick}
          shouldCloseOnEsc={feature.shouldCloseOnOverlayClick}
          onRequestClose={this.handleonModalClose}
        >
          <div style={{ zIndex: zIndex+1, width: contentWidth, height: contentHeight }}>{this.state.showModal ? this.props.children : null}</div>
        </ReactModal>
      </Container>
    </PortalComponent>
  }
}

export default Modal;
