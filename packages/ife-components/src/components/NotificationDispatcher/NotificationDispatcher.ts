import {utils} from 'component-utils';
import * as React from 'react';
import {SystemMessage} from 'component-utils';
import {Props} from './types';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import * as defaultProps from './defaultProps.json';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class NotificationDispatcher extends React.Component<Props, any> {
  public static defaultProps = defaultProps;

  componentDidMount() {
    this.props.actions.initConnection();
  }

  componentDidUpdate(prevProps: Props) {
    this.props.isNotificationInProgress === prevProps.isNotificationInProgress
      ? null
      : this.props.isNotificationInProgress
      ? this.props.actions.showMessage({
          title: 'Please pay attention to cabin crew announcement',
          message: 'Media playback will resume after the announcement',
          isError: false
        } as SystemMessage)
      : this.props.actions.hideMessage();
  }

  render() {
    return null;
  }
}
