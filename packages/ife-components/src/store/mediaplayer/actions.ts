import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {AudioItem} from '../audio/types';
import {VideoItem} from '../videos/types';
import {IDrmServers, PlaylistItem} from './types';
import {Normalized} from '../../utils/Norm';

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

/* prettier-ignore */
export const ActionCreators = {
  addItemsToPlaylist: new ActionCreator<'addItemsToPlaylist', Normalized<string, PlaylistItem>>('addItemsToPlaylist'),
  clearPlayerInfo: new ActionCreator<'clearPlayerInfo', any>('clearPlayerInfo'),
  decreaseCurrentPlaylistIndex: new ActionCreator<'decreaseCurrentPlaylistIndex', any>('decreaseCurrentPlaylistIndex'),
  getAudioPlaybackError: new ActionCreator<'getAudioPlaybackError', any>('getAudioPlaybackError'),
  getAudioPlaybackStart: new ActionCreator<'getAudioPlaybackStart', {audioId: string; fileId: string}>('getAudioPlaybackStart'),
  getAudioPlaybackSuccess: new ActionCreator<'getAudioPlaybackSuccess', {src: string}>('getAudioPlaybackSuccess'),
  getDrmServersError: new ActionCreator<'getDrmServersError', any>('getDrmServersError'),
  getDrmServersStart: new ActionCreator<'getDrmServersStart', any>('getDrmServersStart'),
  getDrmServersSuccess: new ActionCreator<'getDrmServersSuccess', {data: IDrmServers}>('getDrmServersSuccess'),
  getVideoPlaybackError: new ActionCreator<'getVideoPlaybackError', any>('getVideoPlaybackError'),
  getVideoPlaybackStart: new ActionCreator<'getVideoPlaybackStart', {videoId: string}>('getVideoPlaybackStart'),
  getVideoPlaybackSuccess: new ActionCreator<'getVideoPlaybackSuccess', {src: string; drmToken: string}>('getVideoPlaybackSuccess'),
  increaseCurrentPlaylistIndex: new ActionCreator<'increaseCurrentPlaylistIndex', any>('increaseCurrentPlaylistIndex'),
  loadPlaylistFromStorageStart: new ActionCreator<'loadPlaylistStateStart', any>('loadPlaylistStateStart'),
  loadPlaylistFromStorageSuccess: new ActionCreator<'loadPlaylistStateSuccess', any>('loadPlaylistStateSuccess'),
  loadPlaylistFromStorageFailure: new ActionCreator<'loadPlaylistFromStorageFailure', any>('loadPlaylistFromStorageFailure'),
  onItemEnded: new ActionCreator<'onItemEnded', undefined>('onItemEnded'),
  removeAllItemFromPlaylist: new ActionCreator<'removeAllItemFromPlaylist', any>('removeAllItemFromPlaylist'),
  removeItemFromPlaylist: new ActionCreator<'removeItemFromPlaylist', any>('removeItemFromPlaylist'),
  resetPlaylistIndex: new ActionCreator<'resetPlaylistIndex', any>('resetPlaylistIndex'),
  savePlaylistState: new ActionCreator<'savePlaylistState', any>('savePlaylistState'),
  seekToRange: new ActionCreator<'seekToRange', {target: number}>('seekToRange'),
  setAudioLanguages: new ActionCreator<'setAudioLanguages', {langs: string[]}>('setAudioLanguages'),
  setBuffering: new ActionCreator<'setBuffering', {isBuffering: boolean}>('setBuffering'),
  setCurrentAudioTrack: new ActionCreator<'setCurrentAudioTrack', {langs: string[]}>('setCurrentAudioTrack'),
  setCurrentItem: new ActionCreator<'setCurrentItem', Partial<AudioItem | VideoItem> & { uid: string}>('setCurrentItem'),
  setError: new ActionCreator<'setError', any>('setError'),
  setFullscreen: new ActionCreator<'setFullscreen', {isFullscreen: boolean}>('setFullscreen'),
  setPlayerAsPaused: new ActionCreator<'setPlayerAsPaused', any>('setPlayerAsPaused'),
  setPlayerAsPlay: new ActionCreator<'setPlayerAsPlay', any>('setPlayerAsPlay'),
  setPlayerAsPlaying: new ActionCreator<'setPlayerAsPlaying', any>('setPlayerAsPlaying'),
  setPlayerPage: new ActionCreator<'setPlayerPage', string>('setPlayerPage'),
  setSubtitles: new ActionCreator<'setSubtitles', {langs: string[]}>('setSubtitles'),
  updateVolume: new ActionCreator<'updateVolume', {value: number}>('updateVolume'),
  mobilePlayVideo: new ActionCreator<'mobile_playVideo', {uid: string}>('mobile_playVideo'),
  mobileAddItemsToPlaylist: new ActionCreator<'mobile_addItemsToPlaylist', {ids: string[], type: string}>('mobile_addItemsToPlaylist'),
  loadPlayListStart: new ActionCreator<'loadPlayListStart', any>('loadPlayListStart'),
  loadPlayListSuccess: new ActionCreator<'loadPlayListSuccess', any>('loadPlayListSuccess'),
};
/* prettier-ignore-end */
