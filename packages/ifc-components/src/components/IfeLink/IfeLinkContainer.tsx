import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {IfeLink as ExternalInterface} from '../common/ComponentsInterface';
import IfeLink, {InternalInterface} from './IfeLink';
import {injectIntl} from 'react-intl';
import {resolveMenuItemsLocale} from 'component-utils/dist/components/Localize';

const connectWrapper = utils.connectWrapper.default;

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {}

@connectWrapper((state: any) => {
  if (state.device) {
    return {isAccessible: state.device.ifeState == 'Available' || state.device.ifeState == 'Down'};
  }
})
class IFELinkContainer extends React.Component<PageProps, any> {
  public render() {
    return (
      <IfeLink
        {...this.props}
        localizedMenuItems={resolveMenuItemsLocale(this.props.intl, this.props.feature.menuItems)}
      />
    );
  }
}

export default injectIntl(IFELinkContainer as any);
