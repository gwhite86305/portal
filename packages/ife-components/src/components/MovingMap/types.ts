export interface Coordinate {
  latitude: number;
  longitude: number;
}

export type Point = number[];

export interface BoundsChangeEvent {
  center: number[];
  zoom: number;
  bounds: Bounds;
  initial: boolean;
}

export interface Bounds {
  ne: number[];
  sw: number[];
}
