import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {Banner as ExternalInterface} from '../common/ComponentsInterface';
import Banner, {InternalInterface} from './Banner';
import {withRouter} from 'react-router';

export type PageProps = ExternalInterface & InternalInterface & {actions: any};

class BannerContainer extends React.Component<PageProps> {
  public render() {
    return <Banner {...this.props} />;
  }
}

export default withRouter(
  withLocaleProps(BannerContainer, [
    {altTextForImage: 'core.Banner.image.altText'},
    {subHeaderText: 'core.Banner.subHeader.text'},
    {headerText: 'core.Banner.header.text'},
    {buttonText: 'core.Banner.button.text'}
  ])
) as any;
