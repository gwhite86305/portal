const enum LocalStorageDataType {
  ifeAgeUnlockPassword = 'ifeAgeUnlockPassword',
  ifeContentRestriction = 'ifeContentRestriction',
  ifePlaylist = 'ifePlaylist'
}

export default LocalStorageDataType;
