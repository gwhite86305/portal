import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {LoginLogoutButton as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  onLoginClick?: any;
  onLogoutClick?: any;
}

class LoginLogoutButton extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const childrens = this.props.children as any;
    const loginButton = (childrens.length > 0 && childrens[0]) || null;
    const logoutButton = (childrens.length > 1 && childrens[1]) || null;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <div className="LoginLogoutButton-component">
          {this.props.isLoggedIn ? (
            <div onClick={this.props.onLogoutClick}>{logoutButton}</div>
          ) : (
            <div onClick={this.props.onLoginClick}> {loginButton}</div>
          )}
        </div>
      </PortalComponent>
    );
  }
}

export default LoginLogoutButton;
