import {IntlProvider} from 'react-intl';
import * as localeMessages from '../locale/error-messages.json';

export enum ErrorCode {
  INVALID_TOTAL_PRICE_OF_ORDER_ITEMS = 40011,
  VOUCHER_EXPIRED = 40012,
  VOUCHER_ALREADY_USED_IN_DIFFERENT_SESSION = 40013,
  NOT_ENOUGH_VOUCHER_CREDITS = 40014,
  VOUCHER_CANNOT_BE_USED_WITH_CURRENCY = 40015,
  FREE_CONTENT = 40016,
  NO_PRICE_FOR_CURRENCY = 40017,
  VOUCHER_ALREADY_USED_ON_DIFFERENT_FLIGHT = 40018,
  REQUESTED_PAID_CONTENT_ITEM = 403100,
  CONTENT_ITEM_NOT_FOUND = 40400,
  VOUCHER_NOT_FOUND = 40406,
  UNSUPPORTED_VOUCHER_TYPE = 50002,
  ORDER_PAID_BUT_NOT_COMPLETED = 50003
}

/*
 * Service for transforming Errors based on their code.
*/
export class ErrorCodeService {
  private intlProvider: IntlProvider;

  constructor() {
    // Todo detect current local and choose apropriatly
    this.intlProvider = new IntlProvider({locale: 'en', messages: localeMessages}, {});
  }

  // localise errors by code
  public localiseErrors(errors: {code: number; message: string}[]) {
    return errors.map(({code, message}) => ({code, message: this.localiseMessage({code, message})}));
  }

  // localise message by code using IntlProvider
  public localiseMessage(error: {code: number; message: string}): string {
    const {intl: {formatMessage}} = this.intlProvider.getChildContext();
    const errorCodeKey = ErrorCode[error.code];
    try {
      const errorCodeMessage = formatMessage({id: errorCodeKey});
      return errorCodeMessage;
    } catch (err) {
      return error.message;
    }
  }
}

export const errorCodeService = new ErrorCodeService();
