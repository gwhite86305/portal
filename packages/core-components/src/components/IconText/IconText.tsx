import * as React from 'react';
import {utils, PortalComponent, ResponsiveImage} from 'component-utils';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';
import {IconText as ExternalInterface} from '../common/ComponentsInterface';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}
const Container: any = styled(CommonComponentThemeWrapper)`
  > * {
    display: inline-block;
  }
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  position: relative;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  align-items: center;
  ${renderThemeProperty('align', 'justify-content')};
  font-family: ${(props: any) => props.theme.appFontFamily};
  .text-container {
    word-wrap: break-word;
    ${(props: any) => {
      return applyMediaQueriesForStyle('font-size', props.theme.componentTheme.textFontSize, props.theme);
    }};
    font-weight: ${(props: any) => props.theme.componentTheme.textFontWeight};
    ${renderThemeProperty('textColor', 'color')};
    ${(props: any) => {
      return applyMediaQueriesForStyle('vertical-align', props.theme.componentTheme.verticalTextAlign, props.theme);
    }};
  }
  .img-align {
    ${(props: any) => {
      return applyMediaQueriesForStyle('vertical-align', props.theme.componentTheme.verticalImageAlign, props.theme);
    }};
  }
  display: flex;
  flex-direction: ${(props: any) => props.theme.componentTheme.imagePosition};
`;

class IconText extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {localizedText, feature, componentTheme} = this.props;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          <div className="text-container">
            <span>{localizedText}</span>
          </div>
          <ResponsiveImage
            altText={feature.altText}
            image={feature.image}
            imageMargin={componentTheme.imageMargin}
            imageWidth={componentTheme.imageWidth}
            className={'img-align'}
          />
        </Container>
      </PortalComponent>
    );
  }
}

export default IconText;
