import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import Button from '../dist/components/Button/Button';
import PortalComponentWrapper from 'component-utils/dist/utils/PortalComponentWrapper';
import {checkA11y} from '@storybook/addon-a11y';

const stories = storiesOf('Button', module);

stories.addDecorator(checkA11y);

stories
  .add('with text', () => (
    <PortalComponentWrapper>
      <Button onClick={action('clicked')}>Hello Button</Button>
    </PortalComponentWrapper>
  ))
  .add('with some emoji', () => (
    <PortalComponentWrapper>
      <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
    </PortalComponentWrapper>
  ));
