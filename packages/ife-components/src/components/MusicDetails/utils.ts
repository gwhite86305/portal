import {capitalize} from 'lodash-es';
import {join, keys, length, map, pathOr, pipe, prop, zipObj} from 'ramda';
import {trackPlay, trackView} from '../../store/analytics/actions';
import {getAudioById} from '../../store/audio/actions';
import {pauseItem, playItem} from '../../store/mediaplayer/actions.player';
import {addItemsToPlaylist, removeItemFromPlaylist} from '../../store/mediaplayer/actions.playlist';
import {selectPlayListItems, selectPlaylistLoading} from '../../store/mediaplayer/selector';
import {MEDIA_PLAYER_STATUS} from '../../store/mediaplayer/types';
import {IRootState} from './../../index';
import {AudioGroup, AudioItem} from './../../store/audio/types';
import {DispatchProps, StoreProps} from './types';
import {values} from 'lodash-es';

export const mapStoreToProps = (state: IRootState): StoreProps => {
  const {items} = state.ifeAudio;
  return {
    playlistLoading: selectPlaylistLoading(state.ifeMediaPlayer),
    storeState: {
      audioGroups: values(items.byId),
      playlistItems: selectPlayListItems(state),
      playlistIdsMap: pipe(
        map(prop('uid')),
        (list: any) => zipObj(list, list)
      )(selectPlayListItems(state)) as any,
      currentPlayingItem: state.ifeMediaPlayer.currentItem,
      isBuffering: state.ifeMediaPlayer.isBuffering,
      isPlayerPaused: state.ifeMediaPlayer.status === MEDIA_PLAYER_STATUS.PAUSED,
      isLoading: state.ifeAudio.isLoadingSynopsis
    }
  };
};
export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    addToPlaylist: items => dispatch(addItemsToPlaylist(items)),
    getAudioById: uid => dispatch(getAudioById(uid)),
    pauseItem: item => dispatch(pauseItem(item)),
    playItem: (item, playerPage) => dispatch(playItem(item, playerPage)),
    removeFromPlaylist: id => dispatch(removeItemFromPlaylist(id)),
    trackView: item => dispatch(trackView(item)),
    trackPlay: item => dispatch(trackPlay(item))
  }
});

export const capitaliseAndJoin = pipe(
  value => (Array.isArray(value) ? value : [value]),
  map(capitalize),
  join(', ')
);
export const imgPlaceholder = 'http://via.placeholder.com/' + 600 + 'pxx' + 420 + 'px';
export const imagesExist = (propName: string) =>
  pipe(
    pathOr({}, [propName, 0]),
    keys,
    length
  );
export const attachImagesToAudioItem = (item: AudioItem, group: AudioGroup): AudioItem => ({
  ...item,
  group,
  galleryImages: imagesExist('galleryImages')(item) ? item.galleryImages : group.galleryImages,
  titleImages: imagesExist('titleImages')(item) ? item.titleImages : group.titleImages
});
