//
// HTMLBundle requires Parent GridItem in Portal Builder to have height set
//
import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  ${() => applyMediaQueriesForStyle('width', {xs: '100%', sm: '100%', md: '100%'}, {})};

  ${renderThemeProperty('backgroundColor', 'background')};
  ${renderThemeProperty('modalTextColor', 'color')};
  ${renderThemeProperty('modalRadius', 'border-radius')};
  ${renderThemeProperty('modalTextSize', 'font-size')};
  height: 100%;
  iframe {
    border: 0;
    width: 100%;
    height: 100%;
    background: white;
  }
`;

export default StyledContainer;
