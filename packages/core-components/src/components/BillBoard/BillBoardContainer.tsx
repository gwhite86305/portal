import * as React from 'react';
import BillBoard from './BillBoard';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

const BillBoardContainer: React.FC<any> = (props: any) => {
  return <BillBoard {...props} />;
};

export default withLocaleProps(BillBoardContainer, [
  {altTextForImage: 'core.BillBoard.image.altText'},
  {subHeaderText: 'core.BillBoard.subHeader.text'},
  {headerText: 'core.BillBoard.header.text'},
  {buttonText: 'core.BillBoard.button.text'}
]) as any;
