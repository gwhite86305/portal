import * as React from 'react';
import AdvertisementCarousel from './AdvertsCarousel';
import styled from 'styled-components';
import ThumbSliderContainer from '../common/CarouselView/ThumbSliderContainer';
import {AdvertItem} from '../../store/adverts/types';
import {AdvertsCarousel as ExternalInterface} from '../common/ComponentsInterface';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import {PortalComponent, utils} from 'component-utils';
import {Props} from './types';
const connectWrapper = utils.connectWrapper.default;

const StyledThumbSliderContainer = styled(ThumbSliderContainer)`
  width: 100%;
  padding: 0;
  .ThumbSliderContainer {
    padding: 0px !important;
  }
`;
@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class AdvertsCarouselContainer extends React.Component<Props & ExternalInterface> {
  constructor(props: Props & ExternalInterface) {
    super(props);
    this.openAd = this.openAd.bind(this);
  }
  componentDidMount() {
    this.onUpdate(this.props);
  }

  render() {
    const {items, actions, isLoading, isFetchCompleted, ...rest} = this.props;
    const isLoaded = !isLoading && isFetchCompleted && items.length;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <StyledThumbSliderContainer className="StyledThumbSliderContainer">
          {isLoaded && (
            <AdvertisementCarousel
              onItemInViewPort={actions.trackImpression}
              onClick={this.openAd}
              items={items}
              {...{actions, isLoading, isFetchCompleted, ...rest}}
            />
          )}
        </StyledThumbSliderContainer>
      </PortalComponent>
    );
  }

  private shouldFetch = (nextProps: Props) => !nextProps.isFetchCompleted && !nextProps.isLoading;
  private onUpdate = (nextProps: Props) => this.shouldFetch(nextProps) && this.props.actions.getAdverts();
  private openAd = (itemId: string) => {
    const adItem = this.props.items.find((ad: AdvertItem) => ad.uid === itemId);
    this.props.actions.trackView(adItem!);
    window.open(adItem!.targetUrl, '_blank');
  };
}
