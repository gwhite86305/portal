import {Action, ActionCreators, ClearPendingRequestsCounter} from './actions';

export type State = {
  readonly PendingRequestsCounter: number;
  readonly TimeoutTimestamp?: number | null;
};

export const initialState: State = {
  PendingRequestsCounter: 0,
  TimeoutTimestamp: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;

  switch (action.type) {
    case ActionCreators.IncrementPendingRequestsCounter.type:
      partialState = {
        PendingRequestsCounter: state.PendingRequestsCounter + 1,
        TimeoutTimestamp: state.TimeoutTimestamp || action.payload
      };
      break;
    case ActionCreators.DecrementPendingRequestsCounter.type:
      partialState = {
        PendingRequestsCounter: state.PendingRequestsCounter > 0 ? state.PendingRequestsCounter - 1 : 0,
        TimeoutTimestamp: state.PendingRequestsCounter === 1 ? null : state.TimeoutTimestamp
      };
      break;
    case ActionCreators.ClearPendingRequestsCounter.type:
      partialState = {
        PendingRequestsCounter: 0,
        TimeoutTimestamp: null
      };
      break;
    default:
      return state;
  }

  return {...state, ...partialState};
}
