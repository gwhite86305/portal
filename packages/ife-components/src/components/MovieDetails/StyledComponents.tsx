import * as React from 'react';
import {Grid} from '@material-ui/core';
import PulsateContainer from '../common/Pulsate';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const showTitleHeaderCSS = '.header .title {display: flex;} .details-container .title {display: none;}';
const showTitlePosterCSS = '.header .title {display: none;} .details-container .title {display: flex;}';

export interface StyledMovieDetailsContainer {
  showTitleHeader?: boolean;
  showBackButton?: boolean;
}

export const StyledContainer = styled.div<StyledMovieDetailsContainer>`
  .movie-details-container {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    flex-grow: 1;
  }
  ${({showTitleHeader}) => (showTitleHeader ? showTitleHeaderCSS : showTitlePosterCSS)};
  .header {
    ${renderThemeProperty('headerBackground', 'background-color')};
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingHeader, theme)};
    display: ${({showBackButton, showTitleHeader}) => (showBackButton || showTitleHeader ? 'flex' : 'none')};
    align-content: center;
    justify-content: space-between;
    align-items: center;
    .place-holder {
      visibility: hidden;
    }
    .back-button,
    .place-holder {
      display: ${({showBackButton}) => (showBackButton ? 'flex' : 'none')};
    }
    .back-button {
      min-width: auto;
      min-height: auto;
      border-radius: 50%;
      padding: 1rem;
    }
    .back-button svg {
      display: ${({showBackButton}) => (showBackButton ? 'flex' : 'none')};
      ${renderThemeProperty('backButtonColor', 'fill')};
      ${renderThemeProperty('backButtonColorHover', 'font-weight')};
    }
  }
  .title {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingTitle, theme)};
    ${({theme}) => applyMediaQueriesForStyle('font-size', theme.componentTheme.titleTextSize, theme)};
    ${renderThemeProperty('titleTextColor', 'color')};
    ${renderThemeProperty('titleTextWeight', 'font-weight')};
    display: flex;
    flex: 1;
    justify-content: center;
  }
  .poster-container {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingPoster, theme)};
    .poster {
      ${() => applyMediaQueriesForStyle('height', {xs: '300px', sm: '508px', md: '600px'}, {})};
      ${() => applyMediaQueriesForStyle('width', {xs: '255px', sm: '357px', md: '421px'}, {})};
    }
    display: flex;
    flex-direction: column;
    flex: 1;
    justify-content: flex-start;
    align-items: center;
  }
  .details-container {
    .info-panel {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingInfoPanel, theme)};
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      height: 100%;
    }

    .details {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingDetails, theme)};
      min-height: 2rem;
    }
    .info {
      min-height: 2rem;
    }
  }
  .description {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingDescription, theme)};
    ${renderThemeProperty('descriptionTextColor', 'color')};
    ${renderThemeProperty('descriptionTextSize', 'font-size')};
    ${renderThemeProperty('descriptionTextWeight', 'font-weight')};
  }
  .info {
    display: flex;
    justify-content: flex-start;
    align-content: center;
    ${renderThemeProperty('detailsInfoMarginBottom', 'margin-bottom')} :last-child {
      margin-bottom: 0;
    }

    .label {
      ${renderThemeProperty('detailsLabelTextColor', 'color')};
      ${renderThemeProperty('detailsLabelTextSize', 'font-size')};
      ${renderThemeProperty('detailsLabelTextWeight', 'font-weight')};
      width: 50%;
      min-width: 50%;
    }
    .value {
      ${renderThemeProperty('detailsValueTextColor', 'color')};
      ${renderThemeProperty('detailsValueTextSize', 'font-size')};
      ${renderThemeProperty('detailsValueTextWeight', 'font-weight')};
    }
    .star {
      ${renderThemeProperty('detailsValueTextSize', 'height')};
      ${renderThemeProperty('detailsValueTextSize', 'width')};
    }
    .star--empty {
      ${renderThemeProperty('reviewEmptyColor', 'fill')};
    }
    .star--full {
      ${renderThemeProperty('reviewFullColor', 'fill')};
    }
  }

  .cta-container {
    display: flex;
    flex-direction: column;
    .cta {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingButtons, theme)};
      ${({theme}) => applyMediaQueriesForStyle('margin', theme.componentTheme.marginButtons, theme)};
      ${renderThemeProperty('buttonRadius', 'border-radius')};
      display: flex;
      justify-content: center;
      align-content: center;
      cursor: pointer;
    }
    .play {
      ${renderThemeProperty('playButtonBackgroundColor', 'background')};
      ${renderThemeProperty('playButtonTextColor', 'color')};
      ${renderThemeProperty('playButtonTextSize', 'font-size')};
      ${renderThemeProperty('playButtonTextWeight', 'font-weight')};
    }
    .playlist {
      ${renderThemeProperty('playlistButtonBackgroundColor', 'background')};
      ${renderThemeProperty('playlistButtonTextColor', 'color')};
      ${renderThemeProperty('playlistButtonTextSize', 'font-size')};
      ${renderThemeProperty('playlistButtonTextWeight', 'font-weight')};
    }
    .price-label-icon {
      margin: 0 1rem;
      ${renderThemeProperty('playButtonTextSize', 'font-size')};
    }
  }
`;

export const Loader: React.FC<any> = props => {
  return (
    <PulsateContainer>
      <Grid container spacing={24} className="movie-details-container">
        <Grid item xs={12} md={8}>
          <div className="poster-container">
            <div className="poster pulsate" />
          </div>
        </Grid>
        <Grid item xs={12} md={4} className="details-container">
          {Array.from(new Array(3)).map((_, index) => (
            <div key={index}>
              <div className="details pulsate" />
            </div>
          ))}
        </Grid>
      </Grid>
    </PulsateContainer>
  );
};
