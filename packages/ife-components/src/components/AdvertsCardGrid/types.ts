import {AdvertItem} from './../../store/adverts/types';
import {AdvertsCarousel as ExternalInterface} from '../common/ComponentsInterface';
import {trackImpression, trackView} from '../../store/analytics/actions';

export interface StateProps {
  items: AdvertItem[];
  isLoading: boolean;
  isFetchCompleted: boolean;
}

export interface DispatchProps {
  actions: {
    getAdverts: () => void;
    trackImpression: typeof trackImpression;
    trackView: typeof trackView;
  };
}

export interface Props extends StateProps, DispatchProps, ExternalInterface {}
export interface AdvertsCardGridProps {
  items: AdvertItem[];
  onClick: (adItem: AdvertItem) => void;
  onItemInViewPort: any;
}
