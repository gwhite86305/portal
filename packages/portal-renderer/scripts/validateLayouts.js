#!/usr/bin/env node
/*
 * ITERATE THRU HANDBUILT LAYOUTS, AND VALIDATE THEIR STRUCTURE AND CONTENTS
 * THIS EXISTS UNTIL THE PORTAL BUILDER IS RESPONSIBLE FOR LAYOUT JSON GENERATION
 */

const path = require('path');
const fs = require('fs');
const R = require('ramda');
const lstatSync = fs.lstatSync;
const readdirSync = fs.readdirSync;
const join = require('path').join;

const MOCKS_DIR = 'mocks/api/layout';
const PACKAGES_DIR = '..';

let validationCount = 0;
const validationErrors = {};

/* ****************************************
  VALIDATORS
*/

// this could be better by navigating component tree as per portal-renderer, but this covers most real world orphans in layouts
const validateOrhpanComponents = (layoutOrg, customLayout, layoutChildComponentUuids) => {
  const {components} = customLayout.layout;
  layoutChildComponentUuids.forEach(childComponentUuid => {
    if (!components[childComponentUuid] && childComponentUuid !== '*') {
      recordError(layoutOrg, `orphan child component does not exist in the layout: ${childComponentUuid}`);
    }
  });
};

// TODO should be driven by schema, but will do for now
const validateLayoutProps = (layoutOrg, customLayout) => {
  if (customLayout.theme === undefined || customLayout.settings === undefined || customLayout.layout === undefined) {
    recordError(layoutOrg, `missing mandatory theme, settings, layout objects`);
  }

  if (customLayout.layout && (customLayout.layout.app === undefined || customLayout.layout.components === undefined)) {
    recordError(layoutOrg, 'missing mandatory layout.app, layout.components objects');
  }
};

// TODO should be driven by schema, but will do for now
const validateComponentProps = (layoutOrg, customLayout) => {
  const {components} = customLayout.layout;
  const componentUuids = Object.keys(components);

  componentUuids.forEach(componentUuid => {
    const component = components[componentUuid];
    const {properties} = component;

    if (
      component.componentName === undefined ||
      component.componentLib === undefined ||
      component.properties === undefined
    ) {
      recordError(layoutOrg, `missing mandatory properties on component ${componentUuid}`);
    }

    if (properties) {
      const allowedProps = ['componentTheme', 'feature', 'children', 'config', 'icons'];
      // TODO: remove 'icons' from this list. It was added because this is currently how we handle weather icons, but it shouldn't be.
      Object.keys(properties).forEach(propKey => {
        if (allowedProps.indexOf(propKey) === -1) {
          recordError(layoutOrg, `invalid property on component ${componentUuid}`);
        }
      });
    }
  });
};

// navigate entire hierarchy ensure prop key contain no "." as this breaks mongodb in builder persistance
const validatePropKeys = (layoutOrg, customLayout) => {
  if (customLayout !== null && typeof customLayout == 'object') {
    Object.entries(customLayout).forEach(([key, value]) => {
      if (key.indexOf('.') !== -1) {
        recordError(layoutOrg, `key has invalid period character: ${key}`);
      }
      validatePropKeys(layoutOrg, value);
    });
  }
};

// TODO should be driven by schema parser, but will do for now
const validateComponentSchema = (layoutOrg, customLayout) => {
  const {components} = customLayout.layout;

  const componentUuids = Object.keys(components);

  componentUuids.forEach(componentUid => {
    const component = components[componentUid];
    const componentDir =
      component.componentLib === 'system' ? '/portal-renderer' : `/${component.componentLib}-components`;
    const componentLibPath = PACKAGES_DIR + `${componentDir}/src/schema/components-schema.json`;

    if (fs.existsSync(componentLibPath)) {
      try {
        const componentSchema = JSON.parse(fs.readFileSync(componentLibPath));

        if (!componentSchema.properties[component.componentName]) {
          recordError(
            layoutOrg,
            `no schema definition in ${component.componentLib} lib for ${component.componentName} component`
          );
        } else {
          const schemaFeaturePropKeys = R.compose(
            R.keys,
            R.pathOr({}, ['properties', component.componentName, 'properties', 'feature', 'properties'])
          )(componentSchema);

          const schemaThemePropKeys = R.compose(
            R.keys,
            R.pathOr({}, ['properties', component.componentName, 'properties', 'componentTheme', 'properties'])
          )(componentSchema);

          const featurePropKeys = R.compose(R.keys, R.pathOr({}, ['properties', 'feature']))(component);

          const themePropKeys = R.compose(R.keys, R.pathOr({}, ['properties', 'componentTheme']))(component);

          const featurePropsDiff = R.difference(featurePropKeys, schemaFeaturePropKeys);
          const themePropsDiff = R.difference(themePropKeys, schemaThemePropKeys);

          if (featurePropsDiff.length) {
            recordError(
              layoutOrg,
              `unexpected *feature* props for component ${component.componentName}: ${featurePropsDiff}`
            );
          }

          if (themePropsDiff.length) {
            recordError(
              layoutOrg,
              `unexpected *componentTheme* props for component ${component.componentName}: ${themePropsDiff}`
            );
          }
        }
      } catch (e) {
        console.error(e);
      }
    }
  });
};

// a component should never be used more than once as a child, a component instance can only === 1 in a layout
const validateComponentUuidUniqueness = (layoutOrg, customLayout, layoutChildComponentUuids) => {
  const {components} = customLayout.layout;

  const componentUuids = Object.keys(components);

  componentUuids.forEach(componentUid => {
    const usedAsChildCount = layoutChildComponentUuids.filter(i => i === componentUid).length;
    if (usedAsChildCount > 1) {
      recordError(layoutOrg, `non-template component used child >=1: ${componentUid}`);
    }
  });
};

/* ****************************************
  RUN THE VALIDATIOM
*/

const isDirectory = source => lstatSync(source).isDirectory();
const getDirectories = source =>
  readdirSync(source)
    .map(name => join(source, name))
    .filter(isDirectory);

const parseLayout = folderName => {
  const layoutJSONPath = folderName + '/GET.json';
  if (fs.existsSync(layoutJSONPath)) {
    validationCount++;
    const layoutOrg = folderName.split(path.sep).pop();
    let customLayout;

    try {
      customLayout = JSON.parse(fs.readFileSync(layoutJSONPath));
    } catch (e) {
      recordError(layoutOrg, 'could not parse layout');
    }

    const {components} = customLayout.layout;
    const layoutChildComponentUuids = R.compose(
      R.flatten,
      R.map(componentUid => R.pathOr([], [componentUid, 'properties', 'children'], components)),
      R.keys
    )(components);

    validateLayoutProps(layoutOrg, customLayout);
    validateComponentProps(layoutOrg, customLayout);
    validatePropKeys(layoutOrg, customLayout);
    validateComponentUuidUniqueness(layoutOrg, customLayout, layoutChildComponentUuids);
    validateOrhpanComponents(layoutOrg, customLayout, layoutChildComponentUuids);
    validateComponentSchema(layoutOrg, customLayout);
  }
};

const recordError = (org, message) => {
  if (!validationErrors[org]) {
    validationErrors[org] = [];
  }
  validationErrors[org].push(message);
};

getDirectories(MOCKS_DIR).forEach(parseLayout);

const orgErrs = Object.keys(validationErrors);

if (orgErrs.length === 0) {
  console.info(`Successfully validated *${validationCount}* layouts - nice work - carry on :)`);
} else {
  console.info(`==========================================================`);

  let aggregateErrorCount = 0;
  orgErrs.forEach(org => {
    validationErrors[org].forEach(message => {
      aggregateErrorCount++;
      console.error(`Error: ${org}/GET.json - ${message}`);
    });
  });

  console.info(`==========================================================`);
  console.info(`Attempted to validate ${validationCount} layouts with ${aggregateErrorCount} failure(s)`);
  orgErrs.forEach(org => {
    if (validationErrors[org].length > 0) {
      console.info(`- ${org} - ${validationErrors[org].length} failure(s)`);
    }
  });

  // TODO: undo when LAYOUTS ARE ALL FIXED ADD THIS:
  // process.exitCode = 1;

  // IN THE MEANTIME PREVENT IT GETTING WORSE THAN IT IS, FAIL WHEN > CURRENT FAILURES

  if (aggregateErrorCount > 360) {
    console.info(`${aggregateErrorCount} failures exceeded the maximum`);
    console.info(`Please fix some and update scripts/validateLayout.js with the new maximum`)
    process.exitCode = 1;
  }
}

/*
This is the current state of play: we can't get worse than this :)

==========================================================
Attempted to validate 15 layouts with 355 failure(s)
- aa - 101 failure(s)
- amx - 20 failure(s)
- amxpaid - 58 failure(s)
- demofree - 3 failure(s)
- demopaid - 5 failure(s)
- djt - 26 failure(s)
- djtsimple - 21 failure(s)
- iceland - 21 failure(s)
- ife-aa - 10 failure(s)
- ife-el-al - 12 failure(s)
- ife-qantas - 14 failure(s)
- ife-tiger - 13 failure(s)
- jbu - 40 failure(s)
- neos - 11 failure(s)


*/
