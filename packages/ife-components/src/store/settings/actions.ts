import {AgeRatingsClass, UserSettings} from './types';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {axiosWithBaseUrl as axios} from '../../utils/utils';
import {userSettingsService, UserSettingsService} from '../../services/UserSettingsService';
import {NO_AGE_RESTRICTION} from './reducer';

export const ActionCreators = {
  ageRatingsClassesStart: new ActionCreator<'ageRatingsClassesStart', any>('ageRatingsClassesStart'),
  ageRatingsClassesSuccess: new ActionCreator<'ageRatingsClassesSuccess', AgeRatingsClass[]>(
    'ageRatingsClassesSuccess'
  ),
  ageRatingsClassesError: new ActionCreator<'ageRatingsClassesError', any>('ageRatingsClassesError'),
  selectAgeRatingClass: new ActionCreator<'selectAgeRatingClass', string>('selectAgeRatingClass'),
  loadSettings: new ActionCreator<'loadSettings', UserSettings>('loadSettings'),
  saveSettings: new ActionCreator<'saveSettings', UserSettings>('saveSettings')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

const STORAGE_SETTING_KEY = 'ife-settings-key';

export function getAgeRatingsClasses() {
  return async (dispatch: any) => {
    dispatch(ActionCreators.ageRatingsClassesStart.create({}));
    try {
      const {data: {ageRatings}} = await axios.get(`/meta/ageratings`);
      dispatch(ActionCreators.ageRatingsClassesSuccess.create(ageRatings as AgeRatingsClass[]));
      // TODO: if loaded classes do not contain current setting, invalidated settings
    } catch {
      dispatch(ActionCreators.ageRatingsClassesError.create({}));
    }
  };
}

export function selectAgeRatingClass(ageRating: string) {
  return (dispatch: any) => dispatch(ActionCreators.selectAgeRatingClass.create(ageRating));
}

export function loadSettings() {
  return (dispatch: any) => {
    const ifeSettings = userSettingsService.getSettings();
    const ageRatingPassword = ifeSettings ? ifeSettings.ageRatingPassword : '';
    const ageRatingClassSelected = ifeSettings ? ifeSettings.ageRatingClassSelected : NO_AGE_RESTRICTION;
    dispatch(ActionCreators.loadSettings.create({ageRatingPassword, ageRatingClassSelected}));
  };
}

export function saveSettings(payload: UserSettings) {
  return (dispatch: any) => {
    userSettingsService.saveSettings(payload);
    dispatch(ActionCreators.saveSettings.create(payload));
    window.location.reload();
  };
}

export default {getAgeRatingsClasses, selectAgeRatingClass, loadSettings, saveSettings};
