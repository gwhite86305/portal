import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, object, boolean, number} from '@storybook/addon-knobs/react';
import WifiNotificationScroll from '../dist/components/WifiNotificationScroll/WifiNotificationScroll.js';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.tertiaryBackgroundColor',
    textColor: 'theme.primaryTextColor',
    position: 'static',
    margin: {
      xs: '0 0 0 0'
    }
  },
  text: 'Connected - Inflight Wi-Fi is currently avaliable. Service is up'
};

const stories = storiesOf('WifiNotificationScroll - Demo Portal Theme', module);

stories.addDecorator(withKnobs);

stories.add('With Knob', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <WifiNotificationScroll
      componentTheme={object('theme', defaultProps.componentTheme)}
      text={text('text', defaultProps.text)}
    />
  </PortalComponentWrapper>
));
