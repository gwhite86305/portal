import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {WaitSplash as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  localizedHeaderText: string;
  localizedSubHeaderText: string;
  localizedMessageText: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  .wait-splash-overlay-page {
    margin-bottom: 0;
    margin-top: 0;
    padding-right: 0;
    padding-left: 0;
    background-image: ${(props: any) =>
      `linear-gradient(
	      ${props.theme.componentTheme.backgroundGradientAngle},
	      ${props.theme.componentTheme.backgroundGradientStartColor},
	      ${props.theme.componentTheme.backgroundGradientEndColor})
	    `};
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9002;
  }
`;

const HeaderText = (props: any) => {
  const Container = styled.div`
    .headerText {
      ${renderThemeProperty('headerFontSize', 'font-size')};
      ${renderThemeProperty('headerTextColor', 'color')};
      ${renderThemeProperty('headerFontWeight', 'font-weight')};
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.headerMargin, props.theme)};
      text-align: center;
    }
  `;
  const {localizedHeaderText, feature} = props;
  return (
    <Container>
      {localizedHeaderText && feature && feature.showHeaderText ? (
        <h2 className="headerText">{localizedHeaderText}</h2>
      ) : null}
    </Container>
  );
};

const SubHeaderText = (props: any) => {
  const Container = styled.div`
    .subHeaderText {
      ${renderThemeProperty('subHeaderFontSize', 'font-size')};
      ${renderThemeProperty('subHeaderTextColor', 'color')};
      ${renderThemeProperty('subHeaderFontWeight', 'font-weight')};
      ${props => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.subHeaderMaxWidth, props.theme)};
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.subHeaderMargin, props.theme)};
      text-align: center;
    }
  `;
  const {localizedSubHeaderText, feature} = props;
  return (
    <Container>
      {localizedSubHeaderText && feature && feature.showSubHeaderText ? (
        <h3 className="subHeaderText">{localizedSubHeaderText}</h3>
      ) : null}
    </Container>
  );
};

const MessageText = (props: any) => {
  const Container = styled.div`
    .messageText {
      ${renderThemeProperty('messageFontSize', 'font-size')};
      ${renderThemeProperty('messageTextColor', 'color')};
      ${renderThemeProperty('messageFontWeight', 'font-weight')};
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.messageMargin, props.theme)};
      font-style: normal;
      font-stretch: normal;
      word-break: break-word;
      text-align: center;
    }
  `;
  const {localizedMessageText, feature} = props;
  return (
    <Container>
      {localizedMessageText && feature && feature.showMessageText ? (
        <p className="messageText">{localizedMessageText}</p>
      ) : null}
    </Container>
  );
};

const CenterIcon = (props: any) => {
  const Container = styled.div`
    .center-container {
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.centerIconMargin, props.theme)};
    }
    .center-icon {
      display: block;
      margin-left: auto;
      margin-right: auto;
    }
  `;
  const {feature} = props;
  return (
    <Container>
      {feature && feature.centerIconPath && feature.showCenterIcon ? (
        <div className="center-container">
          <img
            src={feature.centerIconPath}
            alt={feature.centerIconAltText ? feature.centerIconAltText : ' '}
            className="center-icon"
          />
        </div>
      ) : null}
    </Container>
  );
};

const Footer = (props: any) => {
  const Container = styled.div`
    .footer {
      bottom: 0;
      position: absolute;
      width: 100%;
      ${(props: any) =>
        applyMediaQueriesForStyle('max-height', props.theme.componentTheme.footerMaxHeight, props.theme)};
    }
  `;
  const {feature} = props;
  return (
    <Container>
      <div className="footer">
        {feature && feature.footerImage && feature.showFooterImage ? (
          <ResponsiveImage image={feature.footerImage} />
        ) : null}
      </div>
    </Container>
  );
};

class WaitSplash extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }

  public render() {
    const {componentTheme} = this.props;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <Container>
          <div className="wait-splash-overlay-page">
            <HeaderText {...this.props} />
            <CenterIcon {...this.props} />
            <SubHeaderText {...this.props} />
            <MessageText {...this.props} />
            <Footer {...this.props} />
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default WaitSplash;
