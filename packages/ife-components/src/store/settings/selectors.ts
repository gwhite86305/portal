import {UserSettings} from './types';
import {NO_AGE_RESTRICTION} from './reducer';

export const getAgeRestrictionCode = ({ageRatingClassSelected}: UserSettings) =>
  !!ageRatingClassSelected && ageRatingClassSelected !== NO_AGE_RESTRICTION ? ageRatingClassSelected : undefined;
