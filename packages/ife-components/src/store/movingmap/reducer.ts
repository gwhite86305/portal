import {Action, ActionCreators} from './actions';
import {IMovingMapState} from './types';

export const initialState: IMovingMapState = {
  error: false,
  loading: false,
  firstLoad: true,
  followingPlane: true,
  flightInfo: {
    originPoint: {
      latitude: 0,
      longitude: 0
    },
    destinationPoint: {
      latitude: 0,
      longitude: 0
    },
    aircraftId: '',
    originName: '',
    destinationName: ''
  },
  flightProgress: {
    trackedRoutePoints: [],
    position: {
      latitude: 0,
      longitude: 0
    },
    altitude: 0,
    airSpeed: 0,
    progress: 0,
    takeOffTime: 0,
    timeToDestInMinutes: 0
  },
  map: {
    minZoom: 0,
    maxZoom: 0,
    mapUrl: ''
  }
};

const removeZipExtension = (fileUrl: string): string => fileUrl.slice(undefined, -4);

export default function reducer(state: IMovingMapState = initialState, action: Action): IMovingMapState {
  const {type, payload} = action;
  switch (type) {
    case ActionCreators.loadMovingMap.type:
      return {...state, loading: true, error: false};
    case ActionCreators.loadMovingMapSuccess.type:
      const mapItem = payload.maps[0];
      return {
        ...state,
        ...payload,
        map: {
          minZoom: mapItem.attributes.minZoom,
          maxZoom: mapItem.attributes.maxZoom,
          mapUrl: removeZipExtension(mapItem.files[0])
        },
        firstLoad: false,
        loading: false,
        error: false
      };
    case ActionCreators.loadMovingMapError.type:
      return {...state, loading: false, error: true};
    case ActionCreators.followPlane.type:
      return {...state, followingPlane: true};
    case ActionCreators.unFollowPlane.type:
      return {...state, followingPlane: false};
    default:
      return state;
  }
}
