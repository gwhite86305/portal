import {AnalyticEventType, EventPayload, reportEvent} from '../../store/analytics/actions';
import {setCurrentAudioTime} from '../../store/audio/actions';
import {
  clearPlayerInfo,
  getDrmServers,
  getVideoPlayback,
  seekToRange,
  setAudioLanguages,
  setBuffering,
  setCurrentItem,
  setFullscreen,
  setPlayerAsPaused,
  setPlayerAsPlay,
  setPlayerAsPlaying,
  setSubtitles,
  updateVolume,
  setErrorMovie
} from '../../store/mediaplayer/actions.player';
import {playNextInPlaylist} from '../../store/mediaplayer/actions.playlist';
import {IDrmServers, MEDIA_PLAYER_STATUS, MEDIA_PLAYER_TYPE} from '../../store/mediaplayer/types';
import {
  getPreRollAds,
  getVideoById,
  loadVideosConfig,
  setAudioLang,
  setSubtitle,
  updateVideoConfig
} from '../../store/videos/actions';
import {PrerollAd, VideosConfigItem} from '../../store/videos/types';
import {MediaPlayer as ExternalInterface} from './../common/ComponentsInterface.d';
import {AnalyticsError} from '../../store/analytics/utils';
import {Subject} from 'rxjs';

export interface ContainerState {
  adsPlaylist: any;
  currentAdIndex: number;
  isTrailer: boolean;
  videoId: string;
}

export interface MediaPlayerState {
  showControls: boolean;
  isPausedByUser: boolean;
  duration: number;
  currentTime: number;
}

export interface MediaPlayerProps {
  audioLanguages: string[];
  currentTime: number;
  drmToken: string;
  duration: number;
  fileManifest: string;
  prevfileManifest: string;
  isBuffering: boolean;
  isFullScreen: boolean;
  status: MEDIA_PLAYER_STATUS;
  subtitles: string[];
  volume: number;
  subtitleLanguage: string;
  audioLanguage: string;
  advertId: string;
  isPausedForNotification: boolean;
  isTrailer: boolean;
  onSubtitlesLoaded: (subtitles: string[]) => any;
  fairplayCertificateUrl?: string;
  fairplayLicenseUrl?: string;
  widevineLicenseUrl?: string;
  playReadyLicenseUrl?: string;
  setFullscreen: (isFullscreen: boolean) => any;
  seekToRange: number;
  onPlay: () => void;
  onPlaying: () => void;
  onPause: () => void;
  onVideoEnded: (videoPlayerPage?: string) => any;
  onBuffering: (isBuffering: boolean) => void;
  onAudioLanguagesLoaded: (languages: string[]) => any;
  onError: (error: any, playbackSessionId: string) => any;
  updateVideoConfig: typeof updateVideoConfig;
  blockFastForward?: boolean;
  menusBackgroundColor: string;
  menusTextColor: string;
  menusTextPadding: string;
  menusTextSize: string;
  onClickBack: () => any;
  onSeekToRange: typeof seekToRange;
  onUpdateChange: (value: number) => any;
  progressBackgroundColor: string;
  progressColor: string;
  progressTextSize: string;
  skippableAfter?: number;
  spinnerSize: string;
  spinnerThickness: string;
  videoId: string;
  reportEvent: (event: AnalyticEventType, payload?: EventPayload) => void;
  playbackSessionId: string;
}

export interface MediaContainerProps extends ExternalInterface {
  videoUid: string;
  audioLanguages: string[];
  currentPlaylistItemIndex: number;
  currentTime: number | boolean;
  drmServerUrl: IDrmServers | undefined;
  drmToken: string;
  duration: number;
  fileManifest: string;
  fileType: MEDIA_PLAYER_TYPE;
  isBuffering: boolean;
  isFullScreen: boolean;
  isLoadingDrmServer: boolean;
  isLoadingToken: boolean;
  playerPage: string | undefined;
  seekToRange: number;
  status: MEDIA_PLAYER_STATUS;
  subtitles: string[];
  volume: number;
  subtitleLanguage: string;
  audioLanguage: string;
  preRollAds: {[videoId: string]: PrerollAd[]};
  isPausedForNotification: boolean;
  trailerVideoUrl: string;
  clearPlayerInfo: typeof clearPlayerInfo;
  getDrmServers: typeof getDrmServers;
  getPreRollAds: typeof getPreRollAds;
  getVideoById: typeof getVideoById;
  getVideoPlayback: typeof getVideoPlayback;
  playNextInPlaylist: typeof playNextInPlaylist;
  setAudioLanguages: typeof setAudioLanguages;
  setBuffering: typeof setBuffering;
  setCurrentItem: typeof setCurrentItem;
  setError: typeof setErrorMovie;
  setFullscreen: typeof setFullscreen;
  setPlayerAsPaused: typeof setPlayerAsPaused;
  setPlayerAsPlay: typeof setPlayerAsPlay;
  setPlayerAsPlaying: typeof setPlayerAsPlaying;
  setSubtitles: typeof setSubtitles;
  updateVolume: typeof updateVolume;
  onSeekToRange: typeof seekToRange;
  setCurrentAudioTime: typeof setCurrentAudioTime;
  setSubtitle: typeof setSubtitle;
  setAudioLang: typeof setAudioLang;
  updateVideoConfig: typeof updateVideoConfig;
  loadVideosConfig: typeof loadVideosConfig;
  reportEvent: typeof reportEvent;
}

export interface PlayerProps {
  fileManifest: string;
  prevfileManifest: string;
  drmToken: string;
  volume: number;
  audioLanguage: string;
  subtitleLanguage: string;
  advertId: string;
  isPausedForNotification: boolean;
  fairplayCertificateUrl: string;
  fairplayLicenseUrl: string;
  currentTime: number;
  widevineLicenseUrl: string;
  playReadyLicenseUrl: string;
  videoId: string;
  seekToRange: number;
  isTrailer: boolean;
  onTimeUpdate: (uid: string, currentTime: number, duration: number) => any;
  setFullscreen: (isFullscreen: boolean) => any;
  onPlay: () => void;
  onPlaying: () => void;
  onPause: () => void;
  onVideoEnded: (videoPlayerPage?: string) => any;
  updateVideoConfig: (watchTime: VideosConfigItem) => any;
  onBuffering: (isBuffering: boolean) => void;
  onAudioLanguagesLoaded: (languages: string[]) => any;
  onSubtitlesLoaded: (subtitles: string[]) => any;
  onError: (error: AnalyticsError, playbackSessionId: string) => any;
  reportEvent: (event: AnalyticEventType, payload: EventPayload) => void;
  playbackSessionId: string;
  stopMovie$: Subject<boolean>;
  updateSubtitleLanguage: (language: {uid: string; lang: string}) => {};
  updateAudioLanguage: (language: {uid: string; lang: string}) => {};
}

export interface PlayerState {
  fileManifest: string;
  prevfileManifest: string;
  currentTime: number;
  duration: number;
}

export interface IPlayerState {
  fileManifest: string;
  prevfileManifest: string;
}
