import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty,
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export default styled.div`
  ${renderThemeProperty('backgroundColor', 'background')};
  ${renderThemeProperty('modalTextColor', 'color')};
  ${renderThemeProperty('modalRadius', 'border-radius')};
  ${renderThemeProperty('modalTextSize', 'font-size')};
  ${() => applyMediaQueriesForStyle('width', {xs: '90%', sm: '90%', md: '600px'}, {})};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalPadding, props.theme)};
  box-sizing: border-box;
  padding 1rem;
  position: relative;
  .modal-loader {
    visibility: hidden;
    opacity: 0;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    ${renderThemeProperty('modalLoadingBackgroundColor', 'background')};
    display: flex;
    justify-content: center;
    align-items: center;
    transition: visibility 500ms, opacity 500ms ease-out;
    &.loading {
      visibility: visible;
      opacity: 1;
      transition: visibility 0ms, opacity 0ms ease-out;
    }
  }

  .progress-indicator {
    ${renderThemeProperty('modalButtonTextColor', 'color')};
  }


  .seat-no-input, .voucher-input {
    & > * {
      ${renderThemeProperty('modalTextColor', 'color')};
      ${renderThemeProperty('modalTextSize', 'font-size')};
    }

    &.error > p {
      ${renderThemeProperty('formErrorColor', 'color')};
    }
  }

  .close-button {
    ${renderThemeProperty('modalTextColor', 'color')};
    min-width: auto;
    min-height: auto;
    border-radius: 50%;
    padding: 1rem;
  }

  .cta {
    cursor: pointer;
    padding: 1rem 2rem;
    text-transform: none;
    ${renderThemeProperty('secondaryButtonColor', 'background')};
    position: relative;
    ${renderThemeProperty('modalButtonFontSize', 'font-size')};
    ${renderThemeProperty('modalButtonTextColor', 'color')};
    &.primary {
      ${renderThemeProperty('primaryButtonColor', 'background')};
      > span {
        > .progress-indicator {
          ${renderThemeProperty('modalButtonTextColor', 'color')};
          position: absolute;
          fill: white;
          font-size: 12px;
        }
      }
      &:disabled {
        ${renderThemeProperty('primaryButtonDisabledColor', 'background')};
      }
    }
  }

  .modal-header {
    display: flex;
    justify-content: space-between;
  }

  .modal-header svg {
    cursor: pointer;
  }

  .modal-header {
    align-content: center;
    padding: 1rem 0;

    .title {
      display: flex;
      align-items: center;
    }
  }

  .btn-wrapper {
    display: flex;
    justify-content: space-between;
    padding: 1rem 0 0 0;
    .continue {
      margin-left: auto;
    }
  }

  .modal-content {
    padding: 1rem 0;
    max-height: 350px;
    display: flex;
    flex-direction: column;
    > .form { 
      display: flex;
      flex-direction: column;
      > * {
        ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.itemsPadding, props.theme)};
        display: flex;
        min-height: 20px;
        margin-bottom: 1px;
      }
    }
  }
`;
