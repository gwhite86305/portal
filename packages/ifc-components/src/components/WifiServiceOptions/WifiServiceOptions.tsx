import * as React from 'react';
import {PortalComponent, utils, Currency} from 'component-utils';
import {WifiServiceOptions as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {Button} from 'core-components';
import {pathOr} from 'ramda';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  premiumTiers?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.62);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.62);
  display: flex;
  flex-direction: column;
  div.header {
    flex-direction: column;
    border-width: 0;
    border-style: solid;
    ${renderThemeProperty('dividerColor', 'border-color')};
    h1.header-text {
      margin: 0px;
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'margin-bottom',
          {xs: `calc(${props.theme.gutter}*1.25)`, lg: `calc(${props.theme.gutter}*2.5)`},
          props.theme
        )};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.5);
      font-family: ${(props: any) => props.theme.secondaryFontFamily};
      ${renderThemeProperty('headerColor', 'color')};
      ${renderThemeProperty('headerFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.04;
    }
    h3.body-text {
      margin: 0px;
      ${renderThemeProperty('bodyTextColor', 'color')};
      ${renderThemeProperty('bodyTextFontSize', 'font-size')};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.062);
      font-weight: normal;
      line-height: 1.187;
    }
  }
  div.wifi-options {
    display: flex;
    ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
    div.option {
      border-width: 0;
      border-style: solid;
      ${renderThemeProperty('dividerColor', 'border-color')};
      ${(props: any) => applyMediaQueriesForStyle('border-top-width', {xs: '1px', md: '0px'}, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('border-right-width', {xs: '0px', md: '1px'}, props.theme)};
      flex-grow: 1;
      flex-basis: 0;
      flex-direction: column;
      padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
      div.top {
        ${(props: any) =>
          applyMediaQueriesForStyle(
            'flex-direction',
            props.theme.componentTheme.optionChildFlexDirection,
            props.theme
          )};
        ${(props: any) =>
          applyMediaQueriesForStyle('text-align', props.theme.componentTheme.optionChildTextAlign, props.theme)};
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        div.title-cost {
          display: flex;
          flex-direction: column;
          ${(props: any) =>
            applyMediaQueriesForStyle('max-width', props.theme.componentTheme.optionChildMaxWidth, props.theme)};
          h4.title {
            ${renderThemeProperty('optionTitleColor', 'color')};
            ${renderThemeProperty('optionTitleFontSize', 'font-size')};
            ${renderThemeProperty('optionTitleFontWeight', 'font-weight')};
            word-wrap: break-word;
            margin: 0px;
            line-height: 1.25;
            font-weight: 600;
          }
          h5.cost {
            margin: 0px;
            ${renderThemeProperty('optionCostFontSize', 'font-size')};
            ${renderThemeProperty('optionCostColor', 'color')};
            line-height: 1.28;
            word-wrap: break-word;
            ${renderThemeProperty('optionCostFontWeight', 'font-weight')};
          }
        }
        div.button-wrap {
          ${(props: any) =>
            applyMediaQueriesForStyle(
              'margin-top',
              {xs: `calc(${props.theme.gutter}*.75)`, md: `calc(${props.theme.gutter}*1.25)`},
              props.theme
            )};
          button {
            ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.buttonWidth, props.theme)};
            height: 41px;
            margin: 0 auto;
            border: 1px solid #d8d8d8;
            white-space: nowrap;
            justify-content: center;
            font-size: ${(props: any) => props.theme.primaryFontSize};
            ${renderThemeProperty('buttonBorderRadius', 'border-radius')};
            ${renderThemeProperty('buttonBorderColor', 'border-color')};
            ${renderThemeProperty('buttonBackgroundColor', 'background-color')};
            ${renderThemeProperty('buttonTextColor', 'color')};
            ${renderThemeProperty('buttonTextFontSize', 'font-size')};
            ${renderThemeProperty('buttonTextFontWeight', 'font-weight')};
            ${renderThemeProperty('buttonHeight', 'height')};
            ${renderThemeProperty('buttonWidth', 'width')};
          }
        }
      }
      div.bottom {
        align-items: center;
        line-height: 1.25;
        margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
        span.description {
          display: inline-block;
          ${renderThemeProperty('optionDescriptionFontSize', 'font-size')};
          ${renderThemeProperty('optionDescriptionColor', 'color')};
          ${(props: any) =>
            applyMediaQueriesForStyle('margin-left', props.theme.componentTheme.optionDescriptionGutter, props.theme)};
          ${(props: any) =>
            applyMediaQueriesForStyle('margin-right', props.theme.componentTheme.optionDescriptionGutter, props.theme)};
        }
      }
    }
    div.option:last-child {
      border-right-width: 0px;
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'padding-bottom',
          {xs: `calc(${props.theme.gutter}*.625)`, md: `calc(${props.theme.gutter}*1.25)`},
          props.theme
        )};
    }
    div.option:first-child {
      border-top-width: 0px;
    }
  }
`;

const HeaderSection = (props: any) => {
  const {header, body} = props;
  return (
    <div className="header">
      {header ? <h1 className="header-text">{header}</h1> : null}
      {body ? <h3 className="body-text">{body}</h3> : null}
    </div>
  );
};

const PurchaseOption = (props: any) => {
  const {
    tier,
    showOptionDescription,
    onClick,
    currencyCode,
    currencySuffix,
    locale,
    minimumFractionDigits,
    maximumFractionDigits
  } = props;
  return (
    <div className="option">
      <div className="top">
        <div className="title-cost">
          <h4 className="title">{tier.description}</h4>
          <h5 className="cost">
            <Currency
              currency={currencyCode}
              value={tier.cost}
              locale={locale}
              minimumFractionDigits={minimumFractionDigits}
              maximumFractionDigits={maximumFractionDigits}
              currencySuffix={currencySuffix}
            />
          </h5>
        </div>
        <div className="button-wrap">
          <Button
            aria-label={`${tier.description}, ${pathOr('BUY NOW', ['metadata', 'button'], tier)}`}
            isPrimary={true}
            onClick={(event: any) => {
              onClick(tier.id);
            }}
          >
            {pathOr('BUY NOW', ['metadata', 'button'], tier)}
          </Button>
        </div>
      </div>
      {showOptionDescription && pathOr(false, ['metadata', 'description'], tier) ? (
        <div className="bottom">
          <span className="description">{tier.metadata.description}</span>
        </div>
      ) : null}
    </div>
  );
};
const VoucherSection = (props: any) => {
  const {voucherTitle, voucherText, voucherButtonText, showOptionDescription, voucherDescription, onClick} = props;
  return (
    <div className="option">
      <div className="top">
        <div className="title-cost">
          <h4 className="title">{voucherTitle}</h4>
          <h5 className="cost">{voucherText}</h5>
        </div>
        <div className="button-wrap">
          <Button isPrimary={true} onClick={onClick} aria-label={voucherText}>
            {voucherButtonText}
          </Button>
        </div>
      </div>
      {showOptionDescription && voucherDescription ? (
        <div className="bottom">
          <span className="description">{voucherDescription}</span>
        </div>
      ) : null}
    </div>
  );
};
const MessagingSection = (props: any) => {
  const {
    messagingTitle,
    messagingText,
    messagingButtonText,
    showOptionDescription,
    messagingDescripiton,
    onClick
  } = props;
  return (
    <div className="option">
      <div className="top">
        <div className="title-cost">
          <h4 className="title">{messagingTitle}</h4>
          <h5 className="cost">{messagingText}</h5>
        </div>
        <div className="button-wrap">
          <Button isPrimary={true} onClick={onClick} aria-label={messagingText}>
            {messagingButtonText}
          </Button>
        </div>
      </div>
      {showOptionDescription && messagingDescripiton ? (
        <div className="bottom">
          <span className="description">{messagingDescripiton}</span>
        </div>
      ) : null}
    </div>
  );
};
class WifiServiceOptions extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    premiumTiers: []
  };
  public render() {
    let {
      messagingAvailable,
      premiumTiers,
      subscriptions,
      onPurchaseClick,
      onVoucherClick,
      header,
      body,
      voucherTitle,
      voucherText,
      voucherDescription,
      voucherButtonText,
      afterCostText,
      currencyCode,
      currencySuffix,
      locale,
      minimumFractionDigits,
      maximumFractionDigits,
      messagingTitle,
      messagingText,
      messagingDescripiton,
      messagingButtonText,
      onMessagingClick
    } = this.props;
    let {purchaseOptionSortBy, purchaseOptionOrderBy, voucherAvailable, showOptionDescription} = this.props.feature;
    if (purchaseOptionSortBy == 'cost') {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.cost - tier2.cost);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.cost - tier1.cost);
      }
    } else if (purchaseOptionSortBy == 'duration') {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.duration - tier2.duration);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.duration - tier1.duration);
      }
    } else {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.name - tier2.name);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.name - tier1.name);
      }
    }
    if (subscriptions && subscriptions.length) {
      premiumTiers = premiumTiers.concat(subscriptions);
    }
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-WifiServiceOptions-component">
          {header || body ? <HeaderSection header={header} body={body} /> : null}
          <div className="wifi-options">
            {messagingAvailable ? (
              <MessagingSection
                messagingTitle={messagingTitle}
                messagingText={messagingText}
                messagingDescripiton={messagingDescripiton}
                messagingButtonText={messagingButtonText}
                onClick={onMessagingClick}
                showOptionDescription={showOptionDescription}
                currencyCode={currencyCode}
                locale={locale}
                key="1001"
              />
            ) : null}
            {premiumTiers.map((tier: any, i: number) => (
              <PurchaseOption
                key={i}
                tier={tier}
                showOptionDescription={showOptionDescription}
                onClick={onPurchaseClick}
                afterCostText={afterCostText}
                currencyCode={currencyCode}
                locale={locale}
                minimumFractionDigits={minimumFractionDigits}
                maximumFractionDigits={maximumFractionDigits}
                currencySuffix={currencySuffix}
              />
            ))}
            {voucherAvailable ? (
              <VoucherSection
                voucherTitle={voucherTitle}
                voucherText={voucherText}
                voucherDescription={voucherDescription}
                voucherButtonText={voucherButtonText}
                onClick={onVoucherClick}
                showOptionDescription={showOptionDescription}
                currencyCode={currencyCode}
                locale={locale}
              />
            ) : null}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}
export default WifiServiceOptions;
