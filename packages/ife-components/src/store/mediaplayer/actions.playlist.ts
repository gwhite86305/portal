import * as localForage from 'localforage';
import {goBack, push} from 'connected-react-router';
import {IRootState} from './../../index';
import {ActionCreators} from './actions';
import {clearPlayerInfo, pauseItem, playItem} from './actions.player';
import {PLAYLIST_STORAGE_KEY, PlaylistItem, SavedPlaylist} from './types';
import {TYPES as VIDEO_TYPES} from '../videos/types';
import {VideoItem} from '../videos/types';
import {AudioItem, AUDIO_TYPE, CATEGORIES} from '../audio/types';
import {selectPlayListItems} from './selector';
import localStorageService from '../../services/LocalStorageService';
import Norm, {Normalized} from '../../utils/Norm';
import {trackPlay} from '../analytics/actions';
import {utils} from 'component-utils';
import {getMovies} from '../movies/actions';
import {getAudio} from '../audio/actions';
import {getSeries} from '../videos/actions';

const mapAudioItemToPlaylistItem = ({parentId, title, artist, type, uid}: AudioItem): PlaylistItem => ({
  uid,
  parentId,
  title,
  artist,
  type
});

const mapVideoItemToPlaylistItem = ({parentId, title, season, episode, type, uid}: VideoItem): PlaylistItem => ({
  uid,
  parentId,
  title,
  season,
  episode,
  type
});

export const addItemsToPlaylist = (items: (AudioItem | VideoItem)[]) => {
  return (dispatch: any) => {
    if (utils.mobileSdk.isUsingMobileSdk() && items.length) {
      dispatch(
        ActionCreators.mobileAddItemsToPlaylist.create({
          ids: items.map(item => item.uid),
          type: items[0].type
        })
      );
    }

    const playListItems = items.map((item: AudioItem | VideoItem) =>
      item.type === AUDIO_TYPE.ITEM ? mapAudioItemToPlaylistItem(item) : mapVideoItemToPlaylistItem(item)
    );
    const normalizedPlaylistItems = Norm.toNormalized(playListItems);
    dispatch(ActionCreators.addItemsToPlaylist.create(normalizedPlaylistItems));
    dispatch(savePlaylistState());

    localStorageService
      .readData<Normalized<string, Partial<PlaylistItem>>>('playlist')
      .then(data =>
        localStorageService.writeData(
          'playlist',
          data ? Norm.merge(data, normalizedPlaylistItems) : normalizedPlaylistItems
        )
      );
  };
};

export function decreaseCurrentPlaylistIndex() {
  return (dispatch: any) => dispatch(ActionCreators.decreaseCurrentPlaylistIndex.create(undefined));
}

export function increaseCurrentPlaylistIndex() {
  return (dispatch: any) => dispatch(ActionCreators.increaseCurrentPlaylistIndex.create(undefined));
}

export function resetPlaylistIndex() {
  return (dispatch: any) => dispatch(ActionCreators.resetPlaylistIndex.create(undefined));
}

export const loadPlaylistFromStorage = () => async (dispatch: any) => {
  dispatch(ActionCreators.loadPlaylistFromStorageStart.create({}));
  const playlist = await localStorageService.readData<Normalized<string, PlaylistItem>>('playlist');

  if (playlist) {
    localStorageService.writeData('playlist', playlist);
    return dispatch(ActionCreators.loadPlaylistFromStorageSuccess.create(playlist));
  }
  return dispatch(ActionCreators.loadPlaylistFromStorageFailure.create({}));
};

export function removeAllItemFromPlaylist() {
  return (dispatch: any) => {
    dispatch(ActionCreators.removeAllItemFromPlaylist.create(undefined));
    localStorageService.writeData('playlist', {
      byId: {},
      allIds: []
    });
    dispatch(savePlaylistState());
  };
}

export function removeItemFromPlaylist(uid: string) {
  return (dispatch: any) => {
    dispatch(ActionCreators.removeItemFromPlaylist.create(uid));
    dispatch(savePlaylistState());

    localStorageService.readData('playlist').then((data: SavedPlaylist) =>
      // TODO - we can't execute delete operation in two places (reducer and here). we can't maintain two logics.
      localStorageService.writeData('playlist', Norm.deleteId(data, uid))
    );
  };
}

export function savePlaylistState() {
  return (dispatch: any, getState: () => IRootState) => {
    const {playlistItems} = getState().ifeMediaPlayer;
    dispatch(ActionCreators.savePlaylistState.create(playlistItems));
    localForage.setItem(PLAYLIST_STORAGE_KEY, playlistItems);
  };
}

export function playNextInPlaylist(videoPlayerPage?: string) {
  return (dispatch: any, getState: () => IRootState) => {
    dispatch(ActionCreators.onItemEnded.create(undefined));
    const {currentPlaylistItemIndex} = getState().ifeMediaPlayer;
    const playlistItems = selectPlayListItems(getState());
    const nextItem = playlistItems[currentPlaylistItemIndex + 1];
    // CASE 1, we are not in the playlist flow: go back
    if (currentPlaylistItemIndex === -1) {
      dispatch(goBack());
      dispatch(clearPlayerInfo());
      return;
    }
    // CASE 2, we are at the end of the playlist: go home
    if (currentPlaylistItemIndex >= playlistItems.length - 1) {
      if (playlistItems[0].type === VIDEO_TYPES.VIDEO_ITEM) {
        dispatch(push('/'));
      }
      dispatch(resetPlaylistIndex());
      dispatch(clearPlayerInfo());
      return;
    }
    dispatch(increaseCurrentPlaylistIndex());
    dispatch(trackPlay(nextItem));
    dispatch(playItem(nextItem, videoPlayerPage));
  };
}

export function playPreviousInPlaylist(videoPlayerPage?: string) {
  return (dispatch: any, getState: () => IRootState) => {
    const {currentPlaylistItemIndex} = getState().ifeMediaPlayer;
    const playlistItems = selectPlayListItems(getState());
    const prevItem = playlistItems[currentPlaylistItemIndex - 1];

    if (prevItem === undefined) {
      return;
    }
    dispatch(increaseCurrentPlaylistIndex());
    dispatch(playItem(prevItem, videoPlayerPage));
  };
}

export function playCurrentInPlaylist(videoPlayerPage?: string) {
  return (dispatch: any, getState: () => IRootState) => {
    let {currentPlaylistItemIndex, currentItem} = getState().ifeMediaPlayer;
    const playlistItems = selectPlayListItems(getState());
    // Playlist paused
    if (currentItem) {
      return dispatch(playItem(currentItem, videoPlayerPage));
    }

    // Exit if invalid state
    if (!playlistItems || playlistItems.length === 0) {
      return;
    }

    // Playlist is at init stage
    if (currentPlaylistItemIndex == -1) {
      dispatch(increaseCurrentPlaylistIndex());
    }
    currentPlaylistItemIndex = getState().ifeMediaPlayer.currentPlaylistItemIndex;
    const nextItem = playlistItems[currentPlaylistItemIndex];
    dispatch(playItem(nextItem, videoPlayerPage));
  };
}

export function pauseCurrentInPlaylist() {
  return (dispatch: any, getState: () => IRootState) => {
    const {currentItem} = getState().ifeMediaPlayer;
    dispatch(pauseItem(currentItem));
  };
}

export const loadPlaylist = () => async (dispatch: any) => {
  dispatch(ActionCreators.loadPlayListStart.create({}));
  await Promise.all(
    [
      dispatch(await getMovies()),
      dispatch(await getAudio(CATEGORIES.ALBUMS)),
      dispatch(await getAudio(CATEGORIES.PODCASTS)),
      dispatch(await getAudio(CATEGORIES.AUDIOBOOKS)),
      dispatch(await getSeries()),
      dispatch(await loadPlaylistFromStorage())      
    ]
  );

  dispatch(ActionCreators.loadPlayListSuccess.create({}));
};
