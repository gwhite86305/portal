//!/usr/bin/env groovy

/*
 * Filename: Jenkinsfile
 * Classification: UNCLASSIFIED
 *
 * Copyright (C) 2018 Viasat, Inc.
 * All rights reserved.
 * The information in this software is subject to change without notice and
 * should not be construed as a commitment by Viasat, Inc.
 *
 * Viasat Proprietary
 * The Proprietary Information provided herein is proprietary to Viasat and
 * must be protected from further distribution and use.  Disclosure to others,
 * use or copying without express written authorization of Viasat, is strictly
 * prohibited.
 */

import groovy.transform.Field

@Field
def gitCredentials = 'jenkins_git_key'
def current_slave
def common_util
def portal_util
def pi_util

Object lib_agg
Object lib_aws_conf
Object aws_conf
String fail_email
String promote_email
String db_suffix_prefix = env.BRANCH_NAME.replaceAll('/','').replaceAll('-','')
println(db_suffix_prefix)
Boolean test_mode = false
Boolean is_mainline_branch = true

@Field static final Integer PROMOTE_MILESTONE = 1
@Field static final String S3_REGION = "us-west-2"
@Field static final String S3_ENV = "dev"

stage ("Setup") {
    node("general-centos7") {
        deleteDir()

        checkout(scm)

        git(changelog: false,
            poll: false,
            credentialsId: "gitorg_cmob_automation_owner",
            url: "https://git.viasat.com/Mobility-Engineering/cmob-jenkins-jobs.git")

        lib_agg = load("src/main/lib/library_aggregator.groovy")
        lib_agg.addLibraries(lib_agg.DEFAULT_LIBS +
                             ["src/main/rbo/lib/rbo_misc_funcs.groovy",
                              "src/main/rbo/resources/aws_stack_config.groovy"])

        lib_rbo_misc_funcs = lib_agg.libs.rbo_misc_funcs
        lib_aws_conf = lib_agg.libs.aws_stack_config

        is_mainline_branch = lib_rbo_misc_funcs.isMainLineBranch(env.BRANCH_NAME)
        aws_conf = lib_aws_conf.newAWSStackConfig(S3_REGION, S3_ENV)

        if (test_mode || !is_mainline_branch) {
            fail_email = lib_agg.libs.email.getBuildUserEmail(this)
            promote_email = lib_agg.libs.email.getBuildUserEmail(this)
        }
        else {
            fail_email = "joshua.masterson@viasat.com matthew.k.smith@viasat.com Arpit.Saxena@viasat.com"
            promote_email = "matthew.k.smith@viasat.com"
        }
    }
}

node("rbo_pi") {
    current_slave = env.NODE_NAME
    stage("Robot Test") {
        try {
            println 'Checkout portal-integration repo'
            checkout([
                $class: 'GitSCM',
                userRemoteConfigs: [[url: 'git@git.viasat.com:VWS/portal-integration.git', credentialsId: gitCredentials]],
                branches: [[name: '*/master']],
                doGenerateSubmoduleConfigurations: false,
                poll: false,
                extensions: [
                    [$class: 'RelativeTargetDirectory', relativeTargetDir: 'portal-integration']
                ],
                submoduleCfg: [],
            ])
            dir('portal-integration'){\
                Object lib_portal = lib_agg.libs.portal_lib
                withCredentials([usernamePassword(credentialsId: aws_conf.getDBPassword(),
                                    passwordVariable: "DB_PASSWORD",
                                    usernameVariable: "DB_USER")]) {
                    withEnv(["DB_SUFFIX=${db_suffix_prefix}_${env.BUILD_NUMBER}"]) {
                        try {
                            sh("docker-compose -p docker up -d")
                            sh("make wait-pbe-available")
                            sleep(10)
                            sh("docker restart docker_pbe_1")
                        }
                        finally {
                            sh("make copy-logs")
                            sh("sudo chown -R jenkins:jenkins *")
                            archiveArtifacts(artifacts: "**/*.log")
                            sh('docker-compose -p docker kill')
                            sh('docker-compose -p docker rm -f')
                        }
                    }
                }
            }
        }
        finally {
        }
    }
}


/* Build and CAT stage must run on the same node, since CAT tests depend on build
    artifacts in the repo */
node("vws_build") {
    stage("Build") {
        checkout(scm)

        sh("make build")
    }

    
    Object lib_portal = lib_agg.libs.portal_lib
    withCredentials([usernamePassword(credentialsId: aws_conf.getDBPassword(),
                                    passwordVariable: "DB_PASSWORD",
                                    usernameVariable: "DB_USER")]) {
        withEnv(["DB_SUFFIX=${db_suffix_prefix}_${env.BUILD_NUMBER}","DEPLOY_DOCKER_NETWORK_NAME=host"]) {
            stage("Test") {
                try {
                        sh("make test")
                        sh("make deploy")
                    }
                finally {
                    Boolean archive_diffs = sh(
                        script: 'ls packages/portal-renderer/test/screenshot/**/* | grep _diff &> /dev/null',
                        returnStatus: true) == 0
                    if (archive_diffs) {
                        archiveArtifacts(artifacts: "packages/portal-renderer/test/screenshot/**/*_diff.png")
                    }
                }
            }
        }
    }

    // Do not promote image if this is a feature/fix branch
    if (!is_mainline_branch) {
        deleteDir()
        return
    }

    
    stage("Promote") {
        milestone(PROMOTE_MILESTONE)
        println("Promote canonical react portals to artifactory")
        sh("make publish-all")
        println("Promote components to S3")
        println("Currently rebuilding after the inspection builds")
        sh("make build-integration")
        withCredentials([aws_conf.getAWSPipelineCreds()]) {
            sh("python promote_components.py pcms-portal-components")
            dir("packages/schema-server") {
                sh("docker build -t artifactory.viasat.com:8091/twin_peaks/pcms-portal-schema .")
            }
            // TODO version the image with a version thing
            dir("packages/portal-renderer") {
                sh("docker build -t artifactory.viasat.com:8091/twin_peaks/pcms-portal-renderer --build-arg CACHEBUST=\$(date +%s) --build-arg AWS_SECRET_ACCESS_KEY=${env.AWS_SECRET_ACCESS_KEY} --build-arg AWS_ACCESS_KEY_ID=${env.AWS_ACCESS_KEY_ID} .")
            }
            withCredentials([usernamePassword(
                credentialsId: "scm_user",
                passwordVariable: "pword",
                usernameVariable: "uname")]) {

                sh("docker login -u ${env.uname} -p ${env.pword} artifactory.viasat.com:8091")
                sh("docker push artifactory.viasat.com:8091/twin_peaks/pcms-portal-renderer")
                sh("docker push artifactory.viasat.com:8091/twin_peaks/pcms-portal-schema")
            }
        }
    }

    deleteDir()
}
