import {trackView, trackPlay} from '../../store/analytics/actions';
import {getAudioById} from '../../store/audio/actions';
import {pauseItem, playItem} from '../../store/mediaplayer/actions.player';
import {addItemsToPlaylist, removeItemFromPlaylist} from '../../store/mediaplayer/actions.playlist';
import {MusicDetails as ExternalInterface} from '../common/ComponentsInterface';
import {AudioGroup, AudioItem} from './../../store/audio/types';
import {VideoItem} from './../../store/videos/types';

// Container
export interface StoreProps {
  playlistLoading: boolean;
  storeState: {
    audioGroups: AudioGroup[];
    currentPlayingItem: VideoItem | AudioItem | undefined;
    isBuffering: boolean;
    isPlayerPaused: boolean;
    playlistIdsMap: {[key: string]: string};
    playlistItems: (AudioItem | VideoItem)[];
    isLoading: boolean;
  };
}
export interface DispatchProps {
  actions: {
    addToPlaylist: typeof addItemsToPlaylist;
    getAudioById: typeof getAudioById;
    pauseItem: typeof pauseItem;
    playItem: typeof playItem;
    removeFromPlaylist: typeof removeItemFromPlaylist;
    trackView: typeof trackView;
    trackPlay: typeof trackPlay;
  };
}
export interface DetailsContainerProps extends StoreProps, DispatchProps, ExternalInterface {}
export interface DetailsContainerState {
  item: AudioGroup | undefined;
  contentId: string;
}

// Component
export interface MusicDetailsProps {
  currentPlayingUid?: string;
  imgPlaceholder: string;
  isBuffering: boolean;
  isPlayerPaused: boolean;
  item: AudioGroup;
  onClickBack(): any;
  onClickPause: (e: any) => any;
  onClickPlay: (e: any) => any;
  onRemoveFromPlaylist: (e: any) => any;
  playlistIdsMap: any;
  addToPlaylist: (e: any) => any;
  playAll: (tracks: AudioItem[], albumItem: AudioGroup) => void;
  handleListItemClick: (track: AudioItem, isPauseButton: boolean, albumItem: AudioGroup) => (e: any) => void;
}
