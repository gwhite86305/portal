import Device from '../types/Device';
import Profile from '../types/Profile';

const _distance = (latA: number, lonA: number, latB: number, lonB: number) => {
  const toRad = (x: number) => (x * Math.PI) / 180;
  // Implements Havershine approximation, in miles
  let dLat = toRad(latB - latA);
  let dLon = toRad(lonB - lonA);
  const earth_radius = 3959;

  let a =
    Math.pow(Math.sin(dLat / 2), 2) + Math.cos(toRad(latA)) * Math.cos(toRad(latB)) * Math.pow(Math.sin(dLon / 2), 2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return c * earth_radius;
};

const _getRemainingDistance = (device: any, profile: any) => {
  const origLat = profile.locations.originLatitude;
  const origLon = profile.locations.originLongitude;

  const destLat = profile.locations.destinationLatitude;
  const destLon = profile.locations.destinationLongitude;

  let totalDistance = _distance(origLat, origLon, destLat, destLon);
  let remainingDistance = _distance(device.latitude, device.longitude, destLat, destLon);

  return {remainingDistance, totalDistance};
};

export const getFlightRemaining = (device: any, profile: any) => {
  if (device && profile && profile.locations) {
    let {remainingDistance, totalDistance} = _getRemainingDistance(device, profile);

    // Safe constant commercial airspeed for estimation, in mph
    const groundSpeed = 480;
    let minutesRemaining = Math.ceil((remainingDistance / groundSpeed) * 60);

    let percentageOfFlightComplete = totalDistance > 0 ? 1 - remainingDistance / totalDistance : 0;
    percentageOfFlightComplete = percentageOfFlightComplete < 0 ? 0 : Math.ceil(100 * percentageOfFlightComplete);
    return {minutesRemaining, percentageOfFlightComplete};
  }
  return {minutesRemaining: 0, percentageOfFlightComplete: 0};
};

export const getFlightStatus = (device: any, profile: any) => {
  let status = 'in_progress'; // default
  if (device && profile) {
    let {minutesRemaining, percentageOfFlightComplete} = getFlightRemaining(device, profile);
    if (percentageOfFlightComplete < 5 && (!device || !device.altitude || device.altitude < 10000)) {
      status = 'starting';
    } else if (percentageOfFlightComplete > 95 && (!device || !device.altitude || device.altitude < 10000)) {
      status = 'complete';
    }
  }
  return status;
};
