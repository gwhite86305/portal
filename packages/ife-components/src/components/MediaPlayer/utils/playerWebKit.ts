import {once} from 'ramda';
import axios from 'axios';
import {fromEvent} from 'rxjs';
// tslint:disable:no-bitwise
// tslint:disable:one-variable-per-declaration
// tslint:disable:no-bitwise
// tslint:disable:no-unused-expression

interface WebKitMediaKeySession {
  update: (key: Uint8Array) => any;
}
interface WebKitMediaKeysEvent {
  target: WebKitMediaKeySession;
  message: Uint8Array;
}

const utils = {
  arrayToString: (array: any) => {
    const uint16array = new Uint16Array(array.buffer);
    return String.fromCharCode.apply(null, uint16array);
  },
  attachLicenseToSeession: (session: WebKitMediaKeySession, licenseData: any) => {
    const key = new Uint8Array(licenseData);
    session.update(key);
  },
  attachManifestToVideoTag: (videoRef: HTMLVideoElement, manifestUrl: string) => {
    return new Promise((resolve, reject) => {
      videoRef.addEventListener('webkitneedkey', resolve, {once: true});
      videoRef.addEventListener('error', reject, false);
      videoRef.src = manifestUrl;
    });
  },
  base64EncodeUint8Array: (input: any) => {
    const keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    let output = '';
    let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    let i = 0;
    while (i < input.length) {
      chr1 = input[i++];
      chr2 = i < input.length ? input[i++] : Number.NaN;
      chr3 = i < input.length ? input[i++] : Number.NaN;
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      output += keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
    }
    return output;
  },
  certificateRequest: (urlParam: string, token: string) => {
    return axios.get(urlParam, {
      responseType: 'arraybuffer',
      headers: {'x-vudrm-token': token}
    });
  },
  concatInitDataIdAndCertificate: (initData: any, id: string, cert: Uint8Array) => {
    const localId = typeof id === 'string' ? utils.stringToArray(id) : id;
    let offset = 0;
    const buffer = new ArrayBuffer(initData.byteLength + 4 + localId.byteLength + 4 + cert.byteLength);
    const dataView = new DataView(buffer);
    const initDataArray = new Uint8Array(buffer, offset, initData.byteLength);
    initDataArray.set(initData);
    offset += initData.byteLength;
    dataView.setUint32(offset, localId.byteLength, true);
    offset += 4;
    const idArray = new Uint16Array(buffer, offset, localId.length);
    idArray.set(localId);
    offset += idArray.byteLength;
    dataView.setUint32(offset, cert.byteLength, true);
    offset += 4;
    const certArray = new Uint8Array(buffer, offset, cert.byteLength);
    certArray.set(cert);
    return new Uint8Array(buffer, 0, buffer.byteLength);
  },
  destroy: (videoRef: HTMLVideoElement) => {
    videoRef.pause();
    videoRef.src = '';
  },
  extractContentId: (initData: any) => {
    const contentId = utils.arrayToString(initData);
    return contentId.slice(contentId.lastIndexOf('/') + 1);
  },
  extractHostName: (initData: any) => {
    const contentId = utils.arrayToString(initData);
    const link = document.createElement('a');
    link.href = contentId;
    return link.hostname;
  },
  licenseRequest: (url: string, token: string, webKitMessageEvent: WebKitMediaKeysEvent, contentId: string) => {
    const data = {
      token,
      contentId,
      payload: utils.base64EncodeUint8Array(webKitMessageEvent.message)
    };
    return axios.post(url, data, {
      responseType: 'arraybuffer',
      headers: {'Content-type': 'application/json'}
    });
  },
  onKeyNeed: (event: any, certificate: any): Promise<{contentId: string; mediaKeysEvent: WebKitMediaKeysEvent}> => {
    return new Promise((resolve, reject) => {
      const video = event.target;
      let initData = event.initData;
      const hostname = utils.extractHostName(initData); // TODO: check this
      const contentId = utils.extractContentId(initData);
      initData = utils.concatInitDataIdAndCertificate(initData, hostname, certificate);
      if (!video.webkitKeys) {
        video.webkitSetMediaKeys(new WebKitMediaKeys(utils.selectKeySystem()));
      }
      if (!video.webkitKeys) {
        throw new Error('Could not create MediaKeys');
      }
      const keySession = video.webkitKeys.createSession('video/mp4', initData);
      if (!keySession) {
        throw new Error('Could not create key session');
      }
      keySession.contentId = hostname; // TODO: check this
      keySession.addEventListener(
        'webkitkeymessage',
        (mediaKeysEvent: WebKitMediaKeysEvent) => resolve({mediaKeysEvent, contentId}),
        {once: true}
      );
      keySession.addEventListener('webkitkeyerror', reject, false);
    });
  },
  onDataLoaded: (videoRef: HTMLVideoElement): Promise<any> => {
    return new Promise(resolve => {
      videoRef.addEventListener('loadeddata', resolve, {once: true});
    });
  },
  selectKeySystem: () => {
    if (WebKitMediaKeys.isTypeSupported('com.apple.fps.1_0', 'video/mp4')) {
      return 'com.apple.fps.1_0';
    } else {
      throw new Error('Key System not supported');
    }
  },
  stringToArray: (string: string) => {
    const buffer = new ArrayBuffer(string.length * 2);
    const array = new Uint16Array(buffer);
    for (let i = 0, strLen = string.length; i < strLen; i++) {
      array[i] = string.charCodeAt(i);
    }
    return array;
  },
  transformCertificate: (apiResponse: any) => {
    return new Uint8Array(apiResponse);
  }
};

export async function initWebKitPlayer(
  videoRef: HTMLVideoElement,
  manifestUrl: string,
  token: string,
  fairPlayLicenseServer: string,
  fairPlayCertificateServer: string
) {
  utils.destroy(videoRef);
  const certificateResponse = await utils.certificateRequest(fairPlayCertificateServer, token);
  const certificate = utils.transformCertificate(certificateResponse.data);
  const mediaKeyNeededEvent = await utils.attachManifestToVideoTag(videoRef, manifestUrl);
  const {mediaKeysEvent, contentId} = await utils.onKeyNeed(mediaKeyNeededEvent, certificate);
  const webKitMediaSession = mediaKeysEvent.target;
  const licenseResponse = await utils.licenseRequest(fairPlayLicenseServer, token, mediaKeysEvent, contentId);
  utils.attachLicenseToSeession(webKitMediaSession, licenseResponse.data);
  videoRef.poster = '';
  await utils.onDataLoaded(videoRef);
}
