import {VideoGroup, LocalizedCategory, VideoCategory} from '../../store/videos/types';
import {trackImpression} from '../../store/analytics/actions';
import {Series as ExternalInterface} from '../common/ComponentsInterface';

export interface SeriesProps {
  seriesCategories: VideoCategory[];
  onClick(id: string): any;
  onItemInViewPort: typeof trackImpression;
}

export interface StateProps {
  storeState: {
    items: VideoGroup[];
    seriesFetchCompleted: boolean;
    seriesCategories: LocalizedCategory[];
    isLoadingSeries: boolean;
  };
}
export interface DispatchProps {
  actions: {
    getSeries: () => any;
    getVideoCategories: () => any;
    trackImpression: typeof trackImpression;
  };
}
export interface Props extends StateProps, DispatchProps, ExternalInterface {}
