import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {path} from 'ramda';
import {IRootState} from '../..';
import {audioPlayer, pauseAudioPlayer, playAudioPlayer} from '../../utils/audioPlayer';
import {extractCategories, flattenCategories} from '../../utils/categories';
import Norm from '../../utils/Norm';
import {axiosWithBaseUrl as axios} from '../../utils/utils';
import {
  clearPlayerInfo,
  getAudioPlayback,
  setBuffering,
  setCurrentItem,
  setErrorAudio,
  setPlayerAsPaused,
  setPlayerAsPlay,
  setPlayerAsPlaying
} from '../mediaplayer/actions.player';
import {playNextInPlaylist, removeItemFromPlaylist} from '../mediaplayer/actions.playlist';
import {MediaPlayerState} from '../mediaplayer/types';
import {INotificationDispatcher} from '../notificationDispatcher/types';
import {getAgeRestrictionCode} from '../settings/selectors';
import {IAudioState} from './reducer';
import {AudioGroup, AudioItem, AUDIO_TYPE} from './types';
import {utils} from 'component-utils';

export const ActionCreators = {
  getAudioStart: new ActionCreator<'getAudioStart', {category: string}>('getAudioStart'),
  getAudioSuccess: new ActionCreator<'getAudioSuccess', {items: any; categories: any}>('getAudioSuccess'),
  getAudioError: new ActionCreator<'getAudioError', {category: string}>('getAudioError'),
  getAudioByIdError: new ActionCreator<'getAudioByIdError', any>('getAudioByIdError'),
  getAudioByIdStart: new ActionCreator<'getAudioByIdStart', any>('getAudioByIdStart'),
  getAudioByIdSuccess: new ActionCreator<'getAudioByIdSuccess', AudioGroup>('getAudioByIdSuccess'),
  setCurrentAudioTime: new ActionCreator<'setCurrentAudioTime', {uid: string; duration: number; currentTime: number}>(
    'setCurrentAudioTime'
  ),
  mobilePlayAudio: new ActionCreator<'mobile_playAudio', {uid: string; groupId: string}>('mobile_playAudio'),
  mobilePauseAudio: new ActionCreator<'mobile_pauseAudio', {}>('mobile_pauseAudio')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

const fetchItems = async (path: string, queryParams?: object) => {
  const {
    data: {items}
  } = await axios.get(path, queryParams || {});
  return items || [];
};

const fetchData = async (path: string, queryParams?: object) => {
  const {data} = await axios.get(path, queryParams || {});
  return data || {};
};

export const getAudio = (parentCategory: string) => {
  return async (dispatch: any, getState: () => IRootState) => {
    dispatch(ActionCreators.getAudioStart.create({category: parentCategory}));
    try {
      const ageRating = getAgeRestrictionCode(getState().ifeSettings);
      const [allCategories, audioItems] = await Promise.all([
        fetchData('/content/audio/categories'),
        fetchItems('/content/audio', {ageRating, categoryUid: parentCategory})
      ]);

      const categories = flattenCategories(extractCategories(allCategories, parentCategory), parentCategory);
      dispatch(
        ActionCreators.getAudioSuccess.create({
          items: Norm.toNormalized(audioItems),
          categories: Norm.toNormalized(categories)
        })
      );
    } catch (e) {
      dispatch(ActionCreators.getAudioError.create({category: parentCategory}));
    }
  };
};

export function getAudioById(uid: string) {
  return async (dispatch: any, getState: () => IRootState) => {
    dispatch(ActionCreators.getAudioByIdStart.create({}));
    try {
      const ageRating = getAgeRestrictionCode(getState().ifeSettings);
      const {data} = await axios.get(`/content/audio/${uid}`, ageRating);
      dispatch(ActionCreators.getAudioByIdSuccess.create(data as AudioGroup));
    } catch (e) {
      dispatch(ActionCreators.getAudioByIdError.create({}));
    }
  };
}

export const pauseSingleAudio = () => {
  return (dispatch: any) => {
    pauseAudioPlayer();
    dispatch(setPlayerAsPaused());
    if (utils.mobileSdk.isUsingMobileSdk()) {
      dispatch(ActionCreators.mobilePauseAudio.create({}));
    }
  };
};

export const playSingleAudio = (
  audioItem: AudioItem,
  ifeMediaPlayer: MediaPlayerState,
  ifeAudio: IAudioState,
  ifeNotificationDispatcher: INotificationDispatcher
) => {
  return async (dispatch: any, getState: () => IRootState) => {
    const currentItemId = path(['currentItem', 'uid'], ifeMediaPlayer);
    const fileUid = path(['audio', 0, 'fileUid'], audioItem) as any;
    const {src} = await dispatch(getAudioPlayback(audioItem.uid, fileUid));
    const {userConfig} = ifeAudio;
    const currentTime = (userConfig[audioItem.uid] && userConfig[audioItem.uid].currentTime) || 0;

    // Resume or init
    if (path(['messages', 'value'], ifeNotificationDispatcher)) {
      return;
    }
    try {
      if (currentItemId === audioItem.uid && currentTime > 0) {
        playAudioPlayer();
      } else {
        assignTrackToAudioPlayer(dispatch, getState, audioItem.uid, audioItem.title, src);
      }
    } catch (e) {
      dispatch(setErrorAudio(e));
    }
  };
};

const assignTrackToAudioPlayer = (
  dispatch: any,
  getState: () => IRootState,
  audioId: string,
  audioTitle: string,
  src: string
) => {
  if (utils.mobileSdk.isUsingMobileSdk()) {
    const {
      ifeMediaPlayer: {playlistItems}
    } = getState();
    dispatch(
      ActionCreators.mobilePlayAudio.create({
        uid: audioId,
        groupId: playlistItems.byId[audioId].parentId || ''
      })
    );
    return;
  }
  audioPlayer.src = src;
  audioPlayer.title = audioTitle;
  playAudioPlayer();
  dispatch(clearPlayerInfo());
  dispatch(setCurrentItem({uid: audioId, type: AUDIO_TYPE.ITEM}));
  dispatch(setBuffering(true));
  dispatch(setPlayerAsPlay());
  audioPlayer.ontimeupdate = (e: any) => {
    const {
      ifeAudio: {userConfig}
    } = getState();
    const currentDataTime = (userConfig[audioId] && userConfig[audioId].currentTime) || 0;
    const {currentTime, duration} = e.target;
    const updateInterval = 5.0; // in seconds
    if (parseFloat(currentTime) > parseFloat(currentDataTime) + updateInterval) {
      dispatch(setCurrentAudioTime(audioId, currentTime, duration));
    }
  };
  audioPlayer.oncanplay = () => {
    dispatch(setBuffering(false));
    dispatch(setPlayerAsPlaying());
  };
  audioPlayer.onended = () => {
    dispatch(playNextInPlaylist());
    dispatch(removeItemFromPlaylist(audioId));
  };
};

export const setCurrentAudioTime = (uid: string, currentTime: number, duration: number): any => {
  return (dispatch: any) => dispatch(ActionCreators.setCurrentAudioTime.create({uid, duration, currentTime}));
};
