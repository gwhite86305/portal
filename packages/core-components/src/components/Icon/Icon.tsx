import * as React from 'react';
import {withTheme} from 'styled-components';
import {Icon as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  children?: any;
  theme?: any;
}

class Icon extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }

  public render() {
    const {theme, className} = this.props;
    const {name} = this.props.feature;

    let cname: string = `${theme.fontIconBaseClass} ${className}`;
    let text: string = '';
    if (!theme.fontIconUsesLigature) {
      cname += ` ${name}`;
    } else {
      text = name || '';
    }

    type a = Pick<InternalInterface & ExternalInterface, never> & {
      theme?: any;
    };

    const b: a = {};

    return <i className={cname}>{text}</i>;
  }
}

// NOTE(wmoore): This is a hack to get around withTheme messing up the generic, making this
//   unusable in other classes
export default withTheme(Icon) as any;
