import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {LoginLogoutButton as ExternalInterface} from '../common/ComponentsInterface';
import LoginLogoutButton, {InternalInterface} from './LoginLogoutButton';
import {isLoggedIn} from '../../utils/AuthenticationUtils';
import {getAAJwt, logoutAA} from '../../store/Authentication/actions';
import {loginOptionSelected} from '../../store/WifiServiceOptions/actions';
export type PageProps = ExternalInterface & InternalInterface;
export interface DispatchProps {
  onLogin(): any;
  onLogout(): any;
  onLoad(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => ({
    isLoggedIn: isLoggedIn(state)
  }),
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onLogin: () => {
      dispatch(loginOptionSelected());
    },
    onLoad: () => {
      dispatch(getAAJwt());
    },
    onLogout: () => {
      dispatch(logoutAA());
    }
  })
)
export default class LoginLogoutButtonContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
    this.props.onLoad();
  }
  public render() {
    return (
      <LoginLogoutButton
        {...this.props}
        onLoginClick={() => {
          this.props.onLogin();
        }}
        onLogoutClick={() => {
          this.props.onLogout();
        }}
      />
    );
  }
}
