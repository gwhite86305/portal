#!/usr/bin/env node

// -- IMPORTS
import program from 'commander';
import colors from 'colors';
// @ts-ignore
import pjson from '../package.json';
import {login, createOrg, airlineIcao, airlineName, createPortal, URLS} from './utils';
import {URL} from 'url';
import {performTask, TASK_NAME} from './tasks';

// -- READ INPUT
program
  .version(pjson.version, '-v, --version')
  .option('-c, --componentName <componentName>', 'Component to be tested')
  .option('-l, --list', 'List Available Components in Package')
  .option('-t, --test', 'Run Integration Tests')
  .parse(process.argv);

// -- GREETINGS
console.log('****************************************'.bgBlue.black);
console.log(`${'***'.black}   ${'Portal Builder Inspector'.white} 🔍   ${' ***'.black}`.bgBlue);
console.log('****************************************'.bgBlue.black);

// -- DEFAULT FALLBACK (--HELP)
if (!program.list && !program.componentName && !program.test) program.outputHelp() || process.exit();

// -- RUN SERVICES STACK
/* 
TODO: Run Stack if not in development env
-- run tasks:
----- API Portal Builder
----- API Schema Server
----- WEB Portal Builder
----- WEB Portal Render
 */

// -- RUN TASK
const taskName = (program.list && TASK_NAME.LIST) || (program.test && TASK_NAME.TEST) || TASK_NAME.PREVIEW;
performTask(taskName, program);

// -- DIE GRACEFULLY
process.on('SIGINT', () => {
  console.log('\nBusy cleaning a bunch of stuff...⏳');
  console.log('\nINSPECTION COMPLETED 🏁');
  process.exit();
});

export * from './e2e.utils';
