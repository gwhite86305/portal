let store: any = null;

export default {
  set: (s: any) => {
    store = s;
  },
  get: () => {
    return store;
  }
};
