import * as React from 'react';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {FlightTimeRemaining as ExternalInterface} from '../common/ComponentsInterface';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {Plane, ComponentWrapper, TimeToDestination} from './common_utils';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  flightOrigin?: string;
  flightDestination?: string;
  flightMinutesRemaining?: number;
  percentageOfFlightComplete?: number;
}

const FlightDestination = (props: any) => {
  const {percentageOfFlightComplete, flightDestination} = props;
  const Container = styled.div`
    .destination-container {
      ${renderThemeProperty('locationLabelFontSize', 'font-size')};
      ${renderThemeProperty('locationLabelFontWeight', 'font-weight')};
      ${renderThemeProperty('locationLabelColor', 'color')};
      text-align: center;
      justify-content: space-evenly;
      width: 100%;
      margin-left: -30%;
    }
    .destination-circle {
      ${renderThemeProperty('progressBarBackgroundColor', 'background-color')};
      width: 20px;
      height: 20px;
      border-radius: 50%;
      display: inline-block;
    }
    .flight-end {
      ${renderThemeProperty('progressBarForegroundColor', 'background-color')};
    }
    .destination-text {
      padding-top: 10px;
    }
  `;
  const {flightCutoffPercentage} = props.feature;
  const Circle = (props: any) => {
    if (percentageOfFlightComplete < flightCutoffPercentage) {
      return <div className="destination-circle" />;
    }
    return <div className="destination-circle flight-end" />;
  };
  return (
    <Container>
      <div className="destination-container">
        <Circle />
        <div className="destination-text">
          <span>{flightDestination || '---'}</span>
        </div>
      </div>
    </Container>
  );
};

const FlightOrigin = (props: any) => {
  const Container = styled.div`
    .origin-container {
      ${renderThemeProperty('locationLabelFontSize', 'font-size')};
      ${renderThemeProperty('locationLabelFontWeight', 'font-weight')};
      ${renderThemeProperty('locationLabelColor', 'color')};
      text-align: center;
      justify-content: space-evenly;
      width: 100%;
      margin-left: 30%;
    }
    .origin-circle {
      ${renderThemeProperty('progressBarForegroundColor', 'background-color')};
      width: 20px;
      height: 20px;
      border-radius: 50%;
      display: inline-block;
    }
    .origin-text {
      padding-top: 10px;
    }
  `;
  const {flightOrigin} = props;
  const Circle = (props: any) => {
    return <div className="origin-circle" />;
  };
  return (
    <Container>
      <div className="origin-container">
        <Circle />
        <div className="origin-text">
          <span>{flightOrigin || '---'}</span>
        </div>
      </div>
    </Container>
  );
};

const Title = (props: any) => {
  const Container = styled.div`
    .title-text {
      ${renderThemeProperty('titleFontSize', 'font-size')};
      ${renderThemeProperty('titleFontWeight', 'font-weight')};
      ${renderThemeProperty('titleColor', 'color')};
      width: auto;
      text-align: center;
      padding-bottom: 15px;
    }
  `;
  const {localizedTitle} = props;
  return (
    <Container>
      <div className="title-text">
        <span>{localizedTitle || '---'}</span>
      </div>
    </Container>
  );
};

const ProgressBar = (props: any) => {
  const Container = styled.div`
    .progress-wrapper {
      height: 20px;
      ${renderThemeProperty('progressBarWidth', 'width')};
    }
    .progress-bar {
      ${renderThemeProperty('progressBarBackgroundColor', 'background')};
      position: relative;
      ${renderThemeProperty('progressBarHeight', 'height')};
      width: 100%;
      top: 40%;
    }
    .progress-filler {
      ${renderThemeProperty('progressBarForegroundColor', 'background')};
      height: 100%;
      float: left;
      position: relative;
    }
  `;

  const Filler = (props: any) => {
    return <div className="progress-filler" style={{width: `${props.percentageOfFlightComplete}%`}} />;
  };
  const {flightCutoffPercentage} = props.feature;
  return (
    <Container>
      <div className="progress-wrapper">
        <div className="progress-bar">
          <Filler
            percentageOfFlightComplete={
              props.percentageOfFlightComplete < flightCutoffPercentage ? props.percentageOfFlightComplete : 100
            }
          />
          <Plane {...props} />
        </div>
      </div>
      <div>
        <TimeToDestination {...props} />
      </div>
    </Container>
  );
};

class FlightTimeRemaining extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    flightOrigin: '---',
    flightDestination: '---',
    flightMinutesRemaining: 0,
    percentageOfFlightComplete: 0
  };

  public render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <ComponentWrapper>
          <div className="ifc-flighttimeremaining-component ">
            {this.props.feature.showTitle ? <Title {...this.props} /> : null}
            <div className="ifc-flighttimeremaining-progress">
              <FlightOrigin {...this.props} />
              <ProgressBar {...this.props} />
              <FlightDestination {...this.props} />
            </div>
          </div>
        </ComponentWrapper>
      </PortalComponent>
    );
  }
}

export default FlightTimeRemaining;
