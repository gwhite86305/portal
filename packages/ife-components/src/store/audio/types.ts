import {ImageVariants, ImageVariantsType} from '../../components/types';

export enum CATEGORIES {
  ALBUMS = 'albums',
  PODCASTS = 'podcasts',
  AUDIOBOOKS = 'audiobooks'
}
export enum AUDIO_TYPE {
  GROUP = 'audioGroup',
  ITEM = 'audioItem'
}

export interface AudioItem {
  parentId?: string;
  artist: string;
  attributes: any;
  audio: {fileUid: string}[];
  categories: string[];
  galleryImages?: ImageVariants[];
  reviewRating: number;
  title: string;
  titleImages?: ImageVariants[];
  length: number;
  type: AUDIO_TYPE.ITEM;
  uid: string;
  group?: AudioGroup | null;
}

export interface AudioGroup {
  artist: string;
  attributes: any;
  categories: string[];
  galleryImages?: ImageVariants[];
  genres: string[];
  items: AudioItem[];
  releaseDate: string;
  reviewRating: number;
  title: string;
  titleImages?: ImageVariants[];
  type: AUDIO_TYPE.GROUP;
  uid: string;
}
export interface AudioCategory {
  uid: string;
  parentCategory?: string;
  category: string;
  items: AudioGroup[];
}

export interface LocalizedCategoryTree {
  uid: string;
  title: string;
  subcategories: LocalizedCategoryTree[];
}

export interface LocalizedCategory {
  title: string;
  uid: string;
  category?: string;
}
