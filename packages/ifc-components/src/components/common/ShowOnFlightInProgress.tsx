import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getFlightStatus} from '../../utils/FlightStatusUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, flightInProgress: false};
  if (state.device && state.profile) {
    let flightStatus = getFlightStatus(state.device, state.profile);
    props.flightInProgress = flightStatus === 'in_progress';
  }
  return props;
})
export default class ShowOnFlightInProgress extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.flightInProgress ? this.props.children : null;
  }
}
