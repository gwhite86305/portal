import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
const {generateMessage} = utils.APIError;

// Action Creators
export const ActionCreators = {
  ShowModal: new ActionCreator<'ShowModal', any>('ShowModal'),
  HideModal: new ActionCreator<'HideModal', any>(
    'HideModal'
  )
};
export type Action = typeof ActionCreators[keyof typeof ActionCreators];
export function ShowModal(modalId: string) {
  return (dispatch: any, getState: any) => {
    let payload = {
      displayedModalId: modalId,
      errorMessage: {}
    };
    dispatch(ActionCreators.ShowModal.create(payload));
  };
}
export function HideModal() {
  return (dispatch: any) => {
    let payload = {
      displayedModalId: "",
      errorMessage: {}
    };
    dispatch(ActionCreators.HideModal.create(payload));
  };
}
