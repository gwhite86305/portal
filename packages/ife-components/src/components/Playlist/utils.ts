import {IRootState} from '../..';
import {playItem, setPlayerPage} from '../../store/mediaplayer/actions.player';
import {
  loadPlaylistFromStorage,
  pauseCurrentInPlaylist,
  playCurrentInPlaylist,
  playNextInPlaylist,
  playPreviousInPlaylist,
  removeAllItemFromPlaylist,
  removeItemFromPlaylist,
  loadPlaylist
} from '../../store/mediaplayer/actions.playlist';
import {MEDIA_PLAYER_STATUS} from '../../store/mediaplayer/types';
import {DispatchProps, StateProps} from './types';
import {selectPlayListItems, selectPlaylistLoading} from '../../store/mediaplayer/selector';
import {trackPlay} from '../../store/analytics/actions';

export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  pauseCurrentInPlaylist: () => dispatch(pauseCurrentInPlaylist()),
  playCurrentInPlaylist: playerPage => dispatch(playCurrentInPlaylist(playerPage)),
  playItem: (item, playerPage) => dispatch(playItem(item, playerPage)),
  playNextInPlaylist: playerPage => dispatch(playNextInPlaylist(playerPage)),
  playPreviousInPlaylist: playerPage => dispatch(playPreviousInPlaylist(playerPage)),
  setPlayerPage: playerPage => dispatch(setPlayerPage(playerPage)),
  removeAllItems: () => dispatch(removeAllItemFromPlaylist()),
  removeItem: itemUid => dispatch(removeItemFromPlaylist(itemUid)),
  trackPlay: item => dispatch(trackPlay(item)),
  loadPlaylist: () => dispatch(loadPlaylist())
});

export const mapStoreToProps = (state: IRootState): StateProps => ({
  items: selectPlayListItems(state),
  isPlaying: state.ifeMediaPlayer.status === MEDIA_PLAYER_STATUS.PLAYING,
  currentItem: state.ifeMediaPlayer.currentItem,
  playlistLoading: selectPlaylistLoading(state.ifeMediaPlayer)
});
