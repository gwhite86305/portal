import * as shaka from 'shaka-player/dist/shaka-player.compiled';
import {isShaka} from './playerCommon';
import {once} from 'ramda';

export const installPolyfillOnce = once(() => (isShaka ? shaka.polyfill.installAll() : null));

export const initShakaPlayer = (
  playerInstance: any,
  manifestFile: string,
  vudrmToken: string,
  wideWinelicenseUrl: string,
  playreadylicenseUrl: string
): Promise<any> => {
  // Config DRM Engine
  playerInstance.configure({
    drm: {
      servers: {
        'com.widevine.alpha': wideWinelicenseUrl,
        'com.microsoft.playready': `${playreadylicenseUrl}?token=${encodeURIComponent(vudrmToken)}`
      },
      advanced: {
        'com.widevine.alpha': {
          videoRobustness: 'SW_SECURE_CRYPTO',
          audioRobustness: 'SW_SECURE_CRYPTO'
        }
      }
    }
  });

  // Attach vudrmToken to License Server
  playerInstance.getNetworkingEngine().clearAllRequestFilters();
  playerInstance.getNetworkingEngine().registerRequestFilter((type: string, request: any) => {
    if (type !== shaka.net.NetworkingEngine.RequestType.LICENSE) return;
    const selectedDrmInfo = playerInstance.drmInfo();
    if (selectedDrmInfo.keySystem !== 'com.widevine.alpha') return;
    const body = JSON.stringify({
      token: vudrmToken,
      drm_info: Array.apply(null, new Uint8Array(request.body)),
      kid: selectedDrmInfo.keyIds[0].toUpperCase()
    });
    request.body = body;
    request.headers['Content-Type'] = 'application/json';
  });

  // Load Manifest
  return playerInstance.load(manifestFile);
};
