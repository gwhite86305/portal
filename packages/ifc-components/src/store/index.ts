import {combineReducers, Reducer} from 'redux';
import {default as device, State as IDeviceState} from './device/reducer';
import {default as profile, State as IProfileState} from './profile/reducer';
import {default as UserAgreement, State as IUAState} from './UserAgreement/reducer';
import {default as Authentication, State as IAuthenticationState} from './Authentication/reducer';
import {default as User, State as IUser} from './User/reducer';
import * as deviceActions from './device/actions';
import * as profileActions from './profile/actions';
import * as UserAgreementActions from './UserAgreement/actions';
import * as WifiServiceOptionsAction from './WifiServiceOptions/actions';

export type IRootState = {
  device: IDeviceState;
  profile: IProfileState;
  UserAgreement: IUAState;
  Authentication: IAuthenticationState;
  User: IUser;
};

// reducer name maps to state tree its responsible for
const rootReducer: Reducer<IRootState> = combineReducers({
  device,
  profile,
  UserAgreement,
  Authentication,
  User
});

export default rootReducer;

export interface Actions {
  [key: string]: number | any | undefined;
}

export const Actions: Actions = {
  device: deviceActions,
  profile: profileActions,
  UserAgreement: UserAgreementActions,
  WifiServiceOption: WifiServiceOptionsAction,
  Authentication: Authentication,
  User: User
};

// TOD export more for 3rd parties?
// and do it i a way that is useful for 3rd parties???
