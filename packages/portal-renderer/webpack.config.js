const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const apiMocker = require('connect-api-mocker');
const path = require('path');
const webpack = require('webpack');
const request = require('request');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const gitRevisionPlugin = new GitRevisionPlugin();

module.exports = {
  devtool: 'cheap-module-source-map',
  mode: 'development',
  entry: {
    app: ['intersection-observer', 'babel-polyfill', './src/index.tsx']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.[hash].js',
    chunkFilename: '[name].bundle.[hash].js',
    publicPath: '/'
  },
  optimization: {
    namedModules: true,
    noEmitOnErrors: true,
    concatenateModules: false
  },
  devServer: {
    port: 3001,
    hot: true,
    host: '0.0.0.0',
    disableHostCheck: true,
    stats: {
      colors: true,
      warnings: false
    },
    historyApiFallback: {index: '/'},
    // FIXME: this is a temp proxy for storing asset locally,
    // see portalbuilder webpack config asset proxy -> /opt/viasat/portal-builder/assets
    proxy: {
      '/assets': {
        target: `${process.env.UXBUILDER_URL}` + '/assets/',
        pathRewrite: {'^/assets/': ''},
        secure: false
      },
      '/device': {
        target: `${process.env.API_MOCK_URL}`,
        secure: false
      }
    },
    // FIXME: this proxy is needed because tons of setting/layout JSON file
    // still have '/device' as settings injected into components.
    // The setting/layout JSON files would need be generate based on enviroment.
    before: app => {
      // Call Amen mock server to provision it with customer specific data. 
      request.get({
        headers: { 'content-type': 'application/json' },
        url: `${process.env.API_MOCK_URL}` + '/mock/device',
        body: JSON.stringify({
          method: 'GET',
          return: require(path.join(__dirname, process.env.DEVICE_URL))
        })
      }, (_error, response, body) => {
        console.log(body);
      });
      app.use('/profile', apiMocker('mocks/api/profile'));
      app.use('/mockapi', apiMocker('mocks/api'));
      app.use('/signup', apiMocker(process.env.AA_SIGNUP ||'mocks/api/signup'));
      app.use('/proxy/v1/order_history', apiMocker((process.env.PAYMENT_URL || '').replace('POST.json', '')));
      process.env.AA_AUTH_URL && app.use('/aa_auth', apiMocker(process.env.AA_AUTH_URL.replace('POST.json', '')));
      app.use('/voucher', apiMocker('mocks/api/voucher'));
      app.use('/proxy', apiMocker('mocks/api/proxy'));
      app.use('/pa_logger', apiMocker('mocks/api/pa_logger'))
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        API_BUILDER_URL: JSON.stringify(process.env.API_BUILDER_URL),
        PCMS_PORTAL_BUILDER_API_ENDPOINT: JSON.stringify(process.env.PCMS_PORTAL_BUILDER_API_ENDPOINT),
        DEVICE_URL: JSON.stringify(process.env.DEVICE_URL),
        AA_SIGNUP: JSON.stringify(process.env.AA_SIGNUP),
        HASH_ROUTES: process.env.HASH_ROUTES,
        NODE_ENV: JSON.stringify(process.env.ENV),
        PROFILE_URL: JSON.stringify(process.env.PROFILE_URL),
        AA_AUTH_URL: JSON.stringify(process.env.AA_AUTH_URL),
        UXBUILDER_URL: JSON.stringify(process.env.UXBUILDER_URL),
        PCMS_ENV_BUILDER: JSON.stringify(process.env.PCMS_ENV_BUILDER),
        BUILD_DATETIME: JSON.stringify(new Date().toISOString()),
        GIT_VERSION: JSON.stringify(gitRevisionPlugin.version()),
        GIT_COMMIT_HASH: JSON.stringify(gitRevisionPlugin.commithash()),
        GIT_BRANCH: JSON.stringify(gitRevisionPlugin.branch())
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    }),
    new ExtractTextPlugin({
      filename: '[name].[hash:8].css',
      allChunks: true
    })
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      react: path.resolve('../../node_modules/react'), // necessary due to npm being not greate a de-duping
      'react-dom': path.resolve('../../node_modules/react-dom'),
      '@material-ui': path.resolve('../../node_modules/@material-ui'),
      ramda: path.resolve('../../node_modules/ramda'),
      'core-js': path.resolve('../../node_modules/core-js'),
      'styled-components': path.resolve('../../node_modules/styled-components'),
      jss: path.resolve('../../node_modules/jss'),
      axios: path.resolve('../../node_modules/axios'),
      'react-intl': path.resolve('../../node_modules/react-intl'),
      'react-redux': path.resolve('../../node_modules/react-redux'),
      'react-router': path.resolve('../../node_modules/react-router'),
      'react-router-dom': path.resolve('../../node_modules/react-router-dom'),
      'react-router-redux': path.resolve('../../node_modules/react-router-redux'),
      redux: path.resolve('../../node_modules/redux'),
      'redux-actions': path.resolve('../../node_modules/redux-actions'),
      'redux-dynamic-reducer': path.resolve('../../node_modules/redux-dynamic-reducer'),
      'redux-thunk': path.resolve('../../node_modules/redux-thunk'),
      history: path.resolve('../../node_modules/history'),
      intl: path.resolve('../../node_modules/intl'),
      'intl-locales-supported': path.resolve('../../node_modules/intl-locales-supported'),
      soundmanager2: path.resolve('../../node_modules/soundmanager2/script/soundmanager2-nodebug-jsmin.js'),
      'lodash-es': path.resolve('../../node_modules/lodash-es')
    }
  },
  module: {
    rules: [
      {
        test: /\.bundle\.js$/,
        use: 'bundle-loader'
      },
      {
        // The loader that handles ts and tsx files.  These are compiled
        // with the ts-loader and the output is then passed through to the
        // babel-loader.  The babel-loader uses the es2015 and react presets
        // in order that jsx and es6 are processed.
        test: /\.ts(x?)$/,
        exclude: [/node_modules/, /\.spec\.ts(x?)$/],
        loader: 'babel-loader?presets[]=es2015&presets[]=react!ts-loader'
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader?sourceMap',
          'css-loader?sourceMap&modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]!sass-loader'
        ]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$/,
        loader: 'file-loader?name=[name].[ext]' // <-- retain original file name
      }
    ]
  }
};
