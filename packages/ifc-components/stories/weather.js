import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, object, boolean} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';

import Weather from '../dist/components/Weather/Weather.js';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {localizedClickableAreaText: 'Click Here!!'};

const defaultProps = {
  componentTheme: {
    backgroundColor: '#FFF',
    height: {
      xs: '352px',
      lg: '416px',
      xl: '416px'
    },
    weatherIconMargin: {
      xs: '20px auto 20px auto'
    }
  },
  cityName: 'Montreal',
  currentCondition: {
    temperature: '---',
    weatherIcon: 2,
    weatherText: 'Mostly sunny'
  },
  dailyForecast: [
    {
      temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
      epochDate: 1516885200,
      night: {iconPhrase: 'Mostly cloudy', icon: 38},
      day: {iconPhrase: 'Mostly cloudy', icon: 6},
      date: '2018-01-25T07:00:00-06:00'
    },
    {
      temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
      epochDate: 1516971600,
      night: {iconPhrase: 'Showers', icon: 12},
      day: {iconPhrase: 'Rain', icon: 18},
      date: '2018-01-26T07:00:00-06:00'
    },
    {
      temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
      epochDate: 1517058000,
      night: {iconPhrase: 'Intermittent clouds', icon: 36},
      day: {iconPhrase: 'Showers', icon: 12},
      date: '2018-01-27T07:00:00-06:00'
    }
  ],
  icons: {
    '1': './images/Weather_1Sunny_50x50.svg',
    '2': './images/Weather_2MostlySunny_50x50.svg',
    '3': './images/Weather_3PartlyCloudyDay_50x50.svg',
    '4': './images/Weather_4IntermittentCloudsDay_50x50.svg',
    '5': './images/Weather_5HazyDay_50x50.svg',
    '6': './images/Weather_6MostlyCloudyDay_50x50.svg',
    '7': './images/Weather_7PartlyCloudy_50x50.svg',
    '8': './images/Weather_8PartlyCloudyAndHazy_50x50.svg',
    '11': './images/Weather_11Fog_50x50.svg',
    '12': './images/Weather_12PartlyCloudyShowers_50x50.svg',
    '13': './images/Weather_13MostlyCloudyRain_50x50.svg',
    '14': './images/Weather_14PartlyCloudyRainDay_50x50.svg',
    '15': './images/Weather_15PartlyCloudyThunderstorms_50x50.svg',
    '16': './images/Weather_16MostlyCloudyThunderstorms_50x50.svg',
    '17': './images/Weather_17PartlyCloudyThunderstormsDay_50x50.svg',
    '18': './images/Weather_18PartlyCloudyRain_50x50 copy.svg',
    '19': './images/Weather_19PartlyCloudyFlurries_50x50.svg',
    '20': './images/Weather_20MostlyCloudyFlurries_50x50.svg',
    '21': './images/Weather_21PartlyCloudyFlurriesDay_50x50.svg',
    '22': './images/Weather_22PartlyCloudySnow_50x50.svg',
    '23': './images/Weather_23MostlyCloudySnow_50x50.svg',
    '24': './images/Weather_24PartlyCloudyIce_50x50.svg',
    '25': './images/Weather_25PartlyCloudySleet_50x50.svg',
    '26': './images/Weather_26PartlyCloudyFreesingRain_50x50 copy.svg',
    '29': './images/Weather_29PartlyCloudyRainAndSnow_50x50.svg',
    '30': './images/Weather_30HotNoSun_50x50.svg',
    '31': './images/Weather_31ColdNoSnowFlake_50x50.svg',
    '32': './images/Weather_32Windy_50x50.svg',
    '33': './images/Weather_33ClearNight_50x50.svg',
    '34': './images/Weather_34MostlyClearNight_50x50.svg',
    '35': './images/Weather_35PartlyCloudyNight_50x50.svg',
    '36': './images/Weather_36IntermittentCloudsNight_50x50.svg',
    '37': './images/Weather_37HazyNight_50x50.svg',
    '38': './images/Weather_38MostlyCloudyNight_50x50.svg',
    '39': './images/Weather_39PartlyCloudyRainNight_50x50.svg',
    '40': './images/Weather_40MostlyCloudyRainNight_50x50.svg',
    '41': './images/Weather_41PartlyCloudyThunderstormsNight_50x50.svg',
    '42': './images/Weather_42MostlyCloudyThunderstormsNight_50x50.svg',
    '43': './images/Weather_43MostlyCloudySnowDay_50x50.svg',
    '44': './images/Weather_44MostlyCloudySnowNight_50x50.svg'
  },
  feature: {
    clickable: true,
    openPageOnClick: 'Weather',
    showCurrentCondition: true,
    highLowOrientation: 'horizontal',
    flipCelsiusFahrenheit: false
  }
};

mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles = "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';
mockTheme.fontIconBaseClass = 'fa';
const stories = storiesOf('Weather', module);

stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);

stories
  .add('Default', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather {...defaultProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Hide actionable area', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather
        {...defaultProps}
        feature={object('feature', {
          clickable: false,
          openPageOnClick: 'Weather',
          showCurrentCondition: true,
          highLowOrientation: 'horizontal',
          flipCelsiusFahrenheit: false
        })}
        {...defaultLocaleProps}
      />
    </PortalComponentWrapper>
  ))
  .add('Mostly Cloudy', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather
        {...defaultProps}
        {...defaultLocaleProps}
        currentCondition={{
          temperature: 38,
          weatherIcon: 6,
          weatherText: 'Mostly Cloudy'
        }}
      />
    </PortalComponentWrapper>
  ))
  .add('Flurries', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather
        {...defaultProps}
        {...defaultLocaleProps}
        currentCondition={{
          temperature: 34,
          weatherIcon: 19,
          weatherText: 'Flurries'
        }}
      />
    </PortalComponentWrapper>
  ))
  .add('With Forecast', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather
        {...defaultProps}
        {...defaultLocaleProps}
        currentCondition={{
          temperature: 34,
          weatherIcon: 19,
          weatherText: 'Flurries'
        }}
        dailyForecast={[
          {
            temperature: {minimum: {value: 23}, maximum: {value: 44}},
            epochDate: 1516885200,
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            date: '2018-01-25T07:00:00-06:00'
          },
          {
            temperature: {minimum: {value: 12}, maximum: {value: 32}},
            epochDate: 1516971600,
            night: {iconPhrase: 'Showers', icon: 12},
            day: {iconPhrase: 'Rain', icon: 18},
            date: '2018-01-26T07:00:00-06:00'
          },
          {
            temperature: {minimum: {value: '1'}, maximum: {value: 3}},
            epochDate: 1517058000,
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            day: {iconPhrase: 'Showers', icon: 12},
            date: '2018-01-27T07:00:00-06:00'
          }
        ]}
      />
    </PortalComponentWrapper>
  ))
  .add('Knobs', () => (
    <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
      <Weather
        componentTheme={object('componentTheme', defaultProps.componentTheme)}
        feature={object('feature', defaultProps.feature)}
        cityName={text('Internal-City Name', defaultProps.cityName)}
        currentCondition={object('Internal-Current Condition', defaultProps.currentCondition)}
        dailyForecast={object('Internal-Daily Forecast', defaultProps.dailyForecast)}
        icons={object('Icons', defaultProps.icons)}
        {...defaultLocaleProps}
      />
    </PortalComponentWrapper>
  ))
  .add('Today Weather Disabled', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather {...defaultProps} feature={object('feature', {showCurrentCondition: false})} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Flip Celsius and Fahrenheit', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather {...defaultProps} feature={object('feature', {flipCelsiusFahrenheit: true})} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Display High Low Temperatures Vertical', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Weather
        {...defaultProps}
        feature={object('feature', {highLowOrientation: 'vertical'})}
        {...defaultLocaleProps}
      />
    </PortalComponentWrapper>
  ));
