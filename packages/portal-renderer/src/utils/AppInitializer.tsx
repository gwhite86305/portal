import {render} from 'react-dom';
import * as React from 'react';
import {Provider} from 'react-redux';
import {IntlProvider} from 'react-intl';
import {ThemeProvider} from 'styled-components';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';
import {initAPIConfig, initStoreWithComponents} from './StoreUtils';
import * as UXActions from 'component-utils/dist/store/ux/actions';
import FontLoader from 'component-utils/dist/utils/FontLoader';
import {loadRequiredComponents, runComponentLibInitializers} from './ComponentLoader';
import {default as AppBuilder} from '../components/AppBuilder';
import {default as AppPortal} from '../components/AppPortal';
import Error from 'core-components/dist/components/common/Error';
import LocaleLoader from './LocaleLoader';
import 'promise-polyfill';
import 'focus-visible';
import {LayoutInitData} from 'component-utils/dist/store/ux/reducer';

const init = async (appRootDomId: string) => {
  const {store, history, epicMiddleware} = initStoreWithComponents({});
  UXActions.initStore(LocaleLoader);
  const {defaultLocaleMessages, ux, userSelectedLocale, system}: LayoutInitData = await store.dispatch(
    UXActions.initLayout()
  );
  const {layout, theme, settings} = ux;
  FontLoader.applyFontTheme(theme); // do this before app fully loads so u don't get font change jank

  if (system.error) {
    render(
      <IntlProvider locale={userSelectedLocale} messages={defaultLocaleMessages}>
        <ThemeProvider theme={theme}>
          <Error message={system.error} fullHeight={true} />
        </ThemeProvider>
      </IntlProvider>,
      document.getElementById(appRootDomId)
    );
  } else {
    initAPIConfig(); // will ensure api uses settings data on all calls

    const appComponent = isEnvUXBuilder() ? (
      <AppBuilder history={history} />
    ) : (
      <AppPortal theme={theme} history={history} layout={layout} />
    );

    const initializedBodyComponent = runComponentLibInitializers(
      appComponent,
      store,
      settings || {},
      layout,
      epicMiddleware
    );
    await loadRequiredComponents(layout);
    render(<Provider store={store}>{initializedBodyComponent}</Provider>, document.getElementById(appRootDomId));
  }
};

export default {
  init
};
