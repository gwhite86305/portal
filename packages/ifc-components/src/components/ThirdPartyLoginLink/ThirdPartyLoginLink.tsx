import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import styled from 'styled-components';
import {getTierByNpiCode} from '../../utils/ConnectionUtils';
import {pathOr} from 'ramda';
import * as defaults from './defaultProps.json';
const Container = styled(CommonComponentThemeWrapper)`
  > a {
    text-decoration: none;
  }
`;
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, redirect: ''};
  if (state.device && state.profile) {
    let tier = getTierByNpiCode(state.profile, ownProps.feature.npiCode);
    props.redirect = pathOr('', ['metadata', 'redirectUri'], tier);
  }
  return props;
})
export default class ThirdPartyLoginLink extends React.Component<any, any> {
  public static defaultProps = defaults;
  public render() {
    return (
      <Container>
        <a href={this.props.redirect}>{this.props.children}</a>{' '}
      </Container>
    );
  }
}
