import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, showLoadingScreen: false};

  if (state.loading && state.loading.PendingRequestsCounter) {
    props.showLoadingScreen = true;
  }
  return props;
})
export default class ShowOnLoading extends React.Component<any, any> {
  public static defaultProps: {} = {};
  public render() {
    return this.props.showLoadingScreen ? this.props.children : null;
  }
}
