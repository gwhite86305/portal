import axios from 'axios';
import colors from 'colors';
import fs from 'fs';
import {map, pathOr, omit} from 'ramda';

// -- CONSTANTS
export const URLS = {
  apiServerPath: process.env.API_BUILDER_URL,
  builderWebUrl: process.env.UXBUILDER_URL,
  schemaWebUrl: process.env.SCHEMAWEBURL || 'http://localhost:3003/'
};
export const airlineIcao = 'GNL';
export const airlineName = 'Viasat_Airline_INSPECTOR_KEY';
export const portalName = 'Portal_INSPECTOR_KEY';
const theme = {
  activeIconColor: '#E40000',
  gutter: '16px',
  altBackgroundColor: '#013064',
  appFontFamily: "'SourceSansPro'",
  appFontFiles: [
    {file: "url('dab4874b-d8de-4ea1-9529-0fb577a23eb6') format('opentype');", weight: 400},
    {file: "url('dab4874b-d8de-4ea1-9529-0fb577a23eb2') format('opentype');", weight: 600}
  ],
  secondaryFontFamily: "'UniNeue'",
  secondaryFontFiles: [
    {file: "url('89364453-abf8-47f8-bcf1-63ea710b4284') format('opentype');", weight: 400},
    {file: "url('4ea19576-e095-42e6-a0b5-1652955ecc58') format('opentype');", weight: 600}
  ],
  fontIconFontFamily: "'FontAwesome'",
  fontIconFiles:
    "url('a45540e0-7248-4b64-979a-b40d0f50a3d8') format('woff2'), url('2f186296-202f-4423-896e-9304a5953922') format('woff'), url('858050ed-298d-432d-8803-bdb6d388b1b3') format('truetype'), url('0b8fac48-7534-494b-bccf-dcce972a6092') format('svg');",
  fontIconCss: 'edb7cbb7-eb0b-486f-81d3-1db4f15239db',
  fontIconUsesLigature: false,
  fontIconBaseClass: 'fa',
  buttonRadius: '40px',
  defaultFontSize: '16px',
  fontWeight: 'normal',
  formErrorColor: 'red',
  modalRadius: '4px',
  primaryBackgroundColor: '#ffffff',
  primaryButtonColor: '#25b2ef',
  primaryButtonActiveColor: '#9be1ff',
  primaryButtonHoverColor: '#64cefb',
  primaryButtonHoverTextColor: '#202e39',
  primaryButtonTextColor: '#202e39',
  primaryFontSize: '1rem',
  primaryIconColor: '#ffffff',
  primaryIconDisabledColor: '#9c9c9c',
  primaryTextColor: '#202e39',
  primaryTextShadow: 'none',
  secondaryBackgroundColor: '#202e39',
  secondaryButtonColor: '#1694ca',
  secondaryButtonActiveColor: '#015274',
  secondaryButtonHoverColor: '#e6e6e6',
  secondaryButtonHoverTextColor: '#333333',
  secondaryButtonTextColor: '#ffffff',
  secondaryFontSize: '1.25rem',
  secondaryTextShadow: '0px 2px 2px rgba(0, 0, 0, 0.3)',
  secondaryIconColor: '#202e39',
  secondaryIconDisabledColor: '#9c9c9c',
  secondaryTextColor: '#1694CA',
  tertiaryBackgroundColor: '#bed733',
  tertiaryTextColor: '#ffffff',
  tertiaryFontSize: '1.5rem',
  tertiaryIconColor: '#1694CA',
  quaternaryBackgroundColor: '#f2f2f2',
  quaternaryIconColor: '#25b2ef',
  quinaryBackgroundColor: '#25B2EF',
  senaryBackgroundColor: '#465967'
};

// -- AXIOS CONFIGS
axios.defaults.headers['Content-Type'] = 'application/vnd.viasat.vcap.pb.v1.0+json';
axios.defaults.headers['Accept'] = 'application/vnd.viasat.vcap.pb.v1.0+json';

// -- UTILS FUNCTIONS
const printErrorAndExit = errorMessage => {
  console.log(`\nResult:`);
  console.log(` ERROR: ${errorMessage}`.red);
  process.exit(-1);
};

export async function login(username, password) {
  try {
    const response = await axios({
      method: 'POST',
      url: URLS.apiServerPath + 'auth/token',
      withCredentials: true,
      data: {username, password}
    });
    axios.defaults.headers['Authorization'] = 'Bearer ' + response.data.authToken;
  } catch (e) {
    console.log('ERROR LOGIN'.red);
  }
}

export async function createOrg({icaoCode, name}) {
  try {
    const {
      data: {orgs}
    } = await axios({method: 'GET', url: `${URLS.apiServerPath}orgs`});
    const inspectorOrg = orgs.find(({icaoCode}) => icaoCode.toLowerCase() === airlineIcao.toLowerCase());
    try {
      if (inspectorOrg) await axios({method: 'DELETE', url: `${URLS.apiServerPath}orgs/${inspectorOrg.uid}`});
    } catch (e) {}
    const {data} = await axios({
      method: 'POST',
      url: `${URLS.apiServerPath}orgs`,
      data: {icaoCode, name}
    });
    return data;
  } catch ({response}) {
    console.log('ERROR CREATING ORG'.red, response);
  }
}

export async function createPortal(org, componentName, defaultProps) {
  const {
    data: {portals}
  } = await axios({method: 'GET', url: `${URLS.apiServerPath}portals`});
  const portalInspector = portals.find(portal => portal.name === portalName);
  try {
    if (portalInspector) await axios({method: 'DELETE', url: `${URLS.apiServerPath}portals/${portalInspector.uid}`});
  } catch (e) {}
  const response = await axios({
    method: 'POST',
    url: `${URLS.apiServerPath}portals`,
    data: generatePortalJSON(portalName, org, componentName, defaultProps)
  });
  return response.data;
}
export const generatePortalJSON = (name, org, componentName, defaultProps = {}) => ({
  name: name,
  org: org,
  status: 'DRAFT',
  details: {
    ux: {
      layout: {
        app: {
          template: 'HASH_APP_TEMPLATE',
          pages: ['HASH_HOME_PAGE'],
          homePage: 'HASH_HOME_PAGE',
          componentsVersion: '0.0.1'
        },
        components: {
          HASH_APP_TEMPLATE: {
            componentName: 'Template',
            componentLib: 'system',
            properties: {name: 'Portal Template', template: null, children: ['*']}
          },
          HASH_HOME_PAGE: {
            componentName: 'Page',
            componentLib: 'system',
            properties: {name: 'Home', setAsHomePage: true, children: ['HASH_GRID_CONTAINER']}
          },
          HASH_GRID_CONTAINER: {
            componentName: 'GridContainer',
            componentLib: 'system',
            properties: {children: ['HASH_GRID_ITEM']}
          },
          HASH_GRID_ITEM: {
            componentName: 'GridItem',
            componentLib: 'system',
            properties: {children: ['HASH_COMPONENT'], componentTheme: {}}
          },
          HASH_COMPONENT: generateComponentJSON(componentName, defaultProps)
        }
      },
      theme
    }
  }
});

const getComponentTheme = (defaultComponentTheme = {}, theme) =>
  map(value => {
    return typeof value === 'string'
      ? pathOr(value, [value.replace('theme.', '')], theme)
      : getComponentTheme(value, theme);
  }, defaultComponentTheme);

export const generateComponentJSON = (componentName, defaultProps = {}) => ({
  componentName,
  componentLib: 'core', // TODO: Implement Dynamic componentLib, based on folder prefix? or argument?
  properties: {
    componentTheme: getComponentTheme(defaultProps.componentTheme, theme),
    ...omit(['componentTheme'], defaultProps)
  }
});

export const getComponentList = () => {
  let fileString;
  try {
    fileString = fs.readFileSync('./src/schema/components-schema.json', 'utf8');
  } catch (e) {
    printErrorAndExit(`File ${'./src/schema/components-schema.json'.bold} does not exist`);
  }
  return JSON.parse(fileString).properties;
};

export const getComponentDefaultProps = componentName => {
  const fileString = fs.readFileSync(`./src/components/${componentName}/defaultProps.json`, 'utf8');
  const defaultProps = JSON.parse(fileString);
  return defaultProps;
};

export const checkName = componentName => {
  const componentList = Object.keys(getComponentList());
  if (!componentName) {
    printErrorAndExit('Component Name Missing from Arguments');
  }
  if (!componentList.includes(componentName)) {
    printErrorAndExit('Component Name Missing from Library');
  }
};
