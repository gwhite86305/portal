import {PortalComponent, utils} from 'component-utils';
import * as React from 'react';
import Settings from './Settings';
import {StyledContainer} from './styledComponents';
import {mapDispatchToProps, mapStoreToProps, SettingsContainerProps, SettingsContainerState} from './utils';
import {APP_DOM_ID} from 'component-utils/dist/types/Constants';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class SettingsContainer extends React.Component<SettingsContainerProps, SettingsContainerState> {
  public static contextType = RouterContext;
  mountPoint: any;

  constructor(props: any) {
    super(props);
    this.mountPoint = document.getElementById(APP_DOM_ID) as HTMLElement;
  }

  componentDidMount() {
    this.props.actions.loadSettings();
    this.props.actions.getAgeRatingsClasses();
  }

  render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <StyledContainer>
          <Settings
            settingsPassword={this.props.storeState.ageRatingPassword}
            modalNodeRef={this.mountPoint}
            selectedAgeRestriction={this.props.storeState.ageRatingClassSelected}
            ageRestrictions={this.props.storeState.ageRatingClasses}
            onSave={this.props.actions.saveSettings}
          />
        </StyledContainer>
      </PortalComponent>
    );
  }

  private saveSettings = (ageRate: string, ageRatingPassword: string) =>
    this.props.actions.saveSettings({ageRatingClassSelected: ageRate, ageRatingPassword});
}
