import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {ContactUsLink as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  chatURL: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('textColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  flex-grow: 1;
  :hover {
    ${renderThemeProperty('hoverColor', 'background-color')};
  }
  div.links-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    a {
      display: flex;
      flex-basis: 0;
      flex-grow: 1;
      ${(props: any) => applyMediaQueriesForStyle('align-items', props.theme.componentTheme.vAlignitem, props.theme)};
      border-right-width: 1px;
      border-right-style: solid;
      text-decoration: none;
      ${renderThemeProperty('dividerColor', 'border-right-color')};
      ${renderThemeProperty('textDecorationLine', 'text-decoration-line')};
      ${renderThemeProperty('color', 'color')};
      flex-direction: column;
      div.column-view {
        flex-direction: column;
      }
      div.row-view {
        flex-direction: row;
      }
      div.link-img-title {
        display: flex;
        align-items: center;
        ${renderThemeProperty('textColor', 'color')};
        img {
          ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.iconPadding, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.iconHeight, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.iconWidth, props.theme)};
        }
        span {
          margin-top: calc(${(props: any) => props.theme.gutter}*.375);
          ${renderThemeProperty('textFontSize', 'font-size')};
          line-height: 1.56;
        }
      }
    }
    a:last-child {
      border-right: none;
    }
  }
`;

class ContactUsLink extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {
      chatURL,
      helpTitle,
      chatTitle,
      helpAltText,
      chatAltText,
      faqsTitle,
      faqsAltText,
      externalTitle,
      externalAltText
    } = this.props;

    const {
      showHelp,
      helpUrl,
      helpIcon,
      showChat,
      chatIcon,
      showFaqs,
      faqsIcon,
      faqsUrl,
      columnView,
      helpUrlNewTab,
      chatUrlNewTab,
      faqsUrlNewTab,
      externalUrl,
      openInNewTab,
      externalLinkIcon
    } = this.props.feature;
    const helpUrlTarget = helpUrlNewTab ? '_blank' : undefined;
    const chatUrlTarget = chatUrlNewTab ? '_blank' : undefined;
    //const faqsUrlTarget = faqsUrlNewTab ? '_blank' : undefined;
    const externalUrlTarget = openInNewTab ? '_blank' : undefined;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-contactUs-component">
          <div className="links-wrapper">
            {showHelp ? (
              <a className="tertiary-focus" target={helpUrlTarget} href={helpUrl || '#'}>
                <div className={columnView ? 'link-img-title column-view' : 'link-img-title row-view'}>
                  <img src={helpIcon} alt={helpAltText} />
                  <span>{helpTitle}</span>
                </div>
              </a>
            ) : null}

            {showChat ? (
              <a className="tertiary-focus" target={chatUrlTarget} href={chatURL || '#'}>
                <div className={columnView ? 'link-img-title column-view' : 'link-img-title row-view'}>
                  <img src={chatIcon} alt={chatAltText} />
                  <span>{chatTitle}</span>
                </div>
              </a>
            ) : null}

            {showFaqs ? (
              <a className="tertiary-focus" target={faqsUrlNewTab} href={faqsUrl || '#'}>
                <div className={columnView ? 'link-img-title column-view' : 'link-img-title row-view'}>
                  <img src={faqsIcon} alt={faqsAltText} />
                  <span>{faqsTitle}</span>
                </div>
              </a>
            ) : null}

            {externalUrl ? (
              <a className="tertiary-focus" target={externalUrlTarget} href={externalUrl || '#'}>
                <div className={columnView ? 'link-img-title column-view' : 'link-img-title row-view'}>
                  <img src={externalLinkIcon} alt={externalAltText} />
                  <span>{externalTitle}</span>
                </div>
              </a>
            ) : (
              <img src={externalLinkIcon} alt={externalAltText} />
            )}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default ContactUsLink;
