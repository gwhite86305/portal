import * as React from 'react';
import {StyledContainer} from './styledComponents';
import BackNavigation from './BackNavigation';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {RouterContext, IRouter} from 'component-utils/dist/utils/Contexts';
import {BackNavigationProps} from './utils';

const BackNavigationContainer: React.FC<BackNavigationProps> = props => {
  const router = React.useContext<IRouter>(RouterContext) || {};
  return (
    <PortalComponent componentTheme={props.componentTheme}>
      <StyledContainer>
        <BackNavigation onClickBack={() => router.history.goBack()} {...props} />
      </StyledContainer>
    </PortalComponent>
  );
};

export default BackNavigationContainer;
