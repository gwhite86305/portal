import {IRootState} from '../..';
import {StoreProps, DispatchProps} from './types';
import { getHtmlBundleByCategoryName } from '../../store/htmlBundle/actions';

export const mapStoreToProps = (state: IRootState): StoreProps => {
  const {htmlBundleUrl, isFetching, error} = state.ifeHtmlBundle;
  return {htmlBundleUrl, isFetching, error};
};

export const mapDispatchToProps = (dispatch: Function): DispatchProps => ({
  getHtmlBundle: (category: string) => dispatch(getHtmlBundleByCategoryName.started(category))
});
