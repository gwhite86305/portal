import {clearPlayerInfo, setCurrentItem, setPlayerAsPlay, setPlayerAsPaused} from '../store/mediaplayer/actions.player';
import {AUDIO_TYPE} from '../store/audio/types';
import {removeItemFromPlaylist, playNextInPlaylist} from '../store/mediaplayer/actions.playlist';
import {utils} from 'component-utils';

const handleMobileAction = (store: any, action: {type: string; payload: any}) => {
  const {dispatch} = store;
  const {type, payload} = action;
  switch (type) {
    case 'mobile_audioItemPlaying':
      dispatch(clearPlayerInfo());
      dispatch(
        setCurrentItem({
          uid: payload.uid,
          type: AUDIO_TYPE.ITEM
        })
      );
      dispatch(setPlayerAsPlay());
      break;
    case 'mobile_finishedPlayingAudioItem':
      dispatch(playNextInPlaylist());
      dispatch(removeItemFromPlaylist(payload.uid));
      break;
    case 'mobile_pausedPlayingAudioItem':
      dispatch(setPlayerAsPaused());
      break;
    default:
      break;
  }
};

window.addEventListener(
  'mobile_host',
  function(event: any) {
    const store = utils.storeAccess.default.get();
    handleMobileAction(store, event.detail);
  },
  false
);
