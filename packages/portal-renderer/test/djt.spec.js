import {screenshot_test, delay, handleFailures} from './utils';
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');

describe('DJT VISUAL REGRESSION TEST', () => {
  let originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 500000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it(`LAPTOP`, async () => {
    const path = 'test/screenshot/laptop/djt/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    // page.on('console', msg => {
    //   console.log(msg);
    // });
    // page.on('error', msg => {
    //   console.log(msg);
    // });
    await page.setViewport({width: 1000, height: 1000});

    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    await delay(2000);

    // Language Select Page
    await page.evaluate(() => {
      document
        .querySelector('.core-button-component[data-component-guid="HASH_GRID_LANGUAGE_BUTTON_EN"]')
        .scrollIntoView(true);
    });
    results.push(await screenshot_test(page, `${path}language_select`));
    await Promise.all([
      page.waitForNavigation({waitUntil: 'networkidle2'}),
      page.click('.core-button-component[data-component-guid="HASH_GRID_LANGUAGE_BUTTON_EN"]', {delay: 50})
    ]);
    await delay(10000);

    //USER AGREEMENT PAGE
    await page.evaluate(() => {
      document.querySelector('.footer-text').scrollIntoView(true);
    });
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button
    await delay(2000);
    // LANDING PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(2000);

    // Make sure skydeals URL is populated with flight/device data
    const hrefs = await page.evaluate(() => {
      const anchors = document.querySelectorAll('a');
      return [].map.call(anchors, a => a.href);
    });
    expect(hrefs).toEqual(
      expect.arrayContaining([
        'https://shop.skydeals.shop/flight/?flight_number=DJT343&flight_origin=JFK&flight_destination=CDG&flight_airline=B0&cie_code=VIASAT'
      ])
    );

    // Terms of Service Page
    const TOS = await page.$x("//a[contains(., 'Viasat Terms of Service')]");
    await TOS[0].click();
    await delay(2000);
    results.push(await screenshot_test(page, `${path}terms`));
    await browser.close();
    handleFailures('djt', 'LAPTOP', results);
  });

  it(`MOBILE`, async () => {
    const path = 'test/screenshot/mobile/djt/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    await page.emulate(devices['iPhone 6']);

    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});

    // Language Select Page
    await page.evaluate(() => {
      document
        .querySelector('.core-button-component[data-component-guid="HASH_GRID_LANGUAGE_BUTTON_EN"]')
        .scrollIntoView(true);
    });
    results.push(await screenshot_test(page, `${path}language_select`));
    await page.click('.core-button-component[data-component-guid="HASH_GRID_LANGUAGE_BUTTON_EN"]');

    //USER AGREEMENT PAGE
    await page.evaluate(() => {
      document.querySelector('.footer-text').scrollIntoView(true);
    });
    //USER AGREEMENT PAGE
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button
    await delay(2000);
    // LANDING PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(2000);

    // Terms of Service page
    const TOS = await page.$x("//a[contains(., 'Viasat Terms of Service')]");
    await TOS[0].click();
    await delay(2000);
    results.push(await screenshot_test(page, `${path}terms`));

    await browser.close();
    handleFailures('djt', 'MOBILE', results);
  });
});
