import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty
  } from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {path} from 'ramda';

export const AdItemBackground = styled.div`
  ${renderThemeProperty('itemBorderRadius', 'border-radius')};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.itemHeight, props.theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('width', theme.componentTheme.itemWidth, theme)};
  background-repeat: round;
  background-size: auto;
  display: flex;
  flex-direction: column-reverse;

  @media screen and (max-width: 600px) {
    ${({images}: any) => `background-image: url(${path(['small', 'url'], images)})`};
  }

  /* TODO: Update with 2x image variant from CMS when available */
  /* @media only screen and (-webkit-min-device-pixel-ratio: 2) and (max-width: 600px),
    only screen and (min--moz-device-pixel-ratio: 2) and (max-width: 600px),
    only screen and (-o-min-device-pixel-ratio: 2/1) and (max-width: 600px),
    only screen and (min-device-pixel-ratio: 2) and (max-width: 600px),
    only screen and (min-resolution: 192dpi) and (max-width: 600px),
    only screen and (min-resolution: 2dppx) and (max-width: 600px) {
    ${(images: any) => `background-image: url(${path(['small', 'url'], images)})`}
  } */

  @media screen and (min-width: 601px) and (max-width: 1280px) {
    ${({images}: any) => `background-image: url(${path(['medium', 'url'], images)})`};
  }

  /* TODO: Update with 2x image variant from CMS when available */
  /* @media only screen and (-webkit-min-device-pixel-ratio: 2) and (max-width: 1280px),
    only screen and (min--moz-device-pixel-ratio: 2) and (max-width: 1280px),
    only screen and (-o-min-device-pixel-ratio: 2/1) and (max-width: 1280px),
    only screen and (min-device-pixel-ratio: 2) and (max-width: 1280px),
    only screen and (min-resolution: 192dpi) and (max-width: 1280px),
    only screen and (min-resolution: 2dppx) and (max-width: 1280px) {
    ${(images: any) => `background-image: url(${path(['medium', 'url'], images)})`}
  } */

  @media screen and (min-width: 1281px) {
    ${({images}: any) => `background-image: url(${path(['large', 'url'], images)})`};
  }

  /* TODO: Update with 2x image variant from CMS when available */
  /* @media only screen and (-webkit-min-device-pixel-ratio: 2),
    only screen and (min--moz-device-pixel-ratio: 2),
    only screen and (-o-min-device-pixel-ratio: 2/1),
    only screen and (min-device-pixel-ratio: 2),
    only screen and (min-resolution: 192dpi),
    only screen and (min-resolution: 2dppx) {
    ${({images}: any) => `background-image: url(${path(['large', 'url'], images)})`}
  } */
`;

export const AdDescriptionSection = ({targetUrl, attributes, description, children}: any) => {
  const doesElementHaveCtaDesc = (targetUrl && attributes && attributes.cta) || description;
  return doesElementHaveCtaDesc ? (
    <StyledAdDescriptionSection>
      <div>{description}</div>
      {children}
    </StyledAdDescriptionSection>
  ) : null;
};

const StyledAdDescriptionSection = styled.div`
  ${renderThemeProperty('descBackgroundColor', 'background-color')};
  ${renderThemeProperty('descFontColor', 'color')};
  ${renderThemeProperty('descFontWeight', 'font-weight')};
  ${renderThemeProperty('itemBorderRadius', 'border-bottom-right-radius')};
  ${renderThemeProperty('itemBorderRadius', 'border-bottom-left-radius')};
  ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.descFontSize, theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.descPadding, theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('height', theme.componentTheme.descHeight, theme)};
  display: flex;
  line-height: 19px;
  align-items: center;
  justify-content: space-between;
`;

export const AdCta = ({ad, onClick}: any) => {
  return ad.targetUrl && ad.attributes && ad.attributes.cta ? (
    <StyledAdCta className={`cta ${ad.description ? '' : 'cta--fullwidth'}`} onClick={_ => onClick(ad)}>
      {ad.attributes.cta}
    </StyledAdCta>
  ) : null;
};

const StyledAdCta = styled.div`
  &.cta {
    ${renderThemeProperty('buttonBackgroundColor', 'background-color')};
    ${renderThemeProperty('buttonFontColor', 'color')};
    ${renderThemeProperty('buttonBorderRadius', 'border-radius')};
    ${renderThemeProperty('buttonFontWeight', 'font-weight')};
    ${({theme}: any) => applyMediaQueriesForStyle('min-width', theme.componentTheme.buttonWidth, theme)};
    ${({theme}: any) => applyMediaQueriesForStyle('height', theme.componentTheme.buttonHeight, theme)};
    ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.buttonFontSize, theme)};
    ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.buttonPadding, theme)};
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    cursor: pointer;
    margin-left: 5px;
    &--fullwidth {
      width: 100%;
    }
  }
`;

export const AdItemText = ({title, subtitle}: any) => (
  <StyledAdItemText>
    {subtitle && <div className="subtitle">{subtitle}</div>}
    {title && <div className="title">{title}</div>}
  </StyledAdItemText>
);

const StyledAdItemText = styled.div`
  background-color: rgba(0, 0, 0, 0.3);
  .title {
    ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.titleFontSize, theme)};
    ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.titlePadding, theme)};
    ${renderThemeProperty('titleFontColor', 'color')};
    ${renderThemeProperty('titleFontWeight', 'font-weight')};
  }
  .subtitle {
    ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.subtitleFontSize, theme)};
    ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.subtitlePadding, theme)};
    ${renderThemeProperty('subtitleFontColor', 'color')};
    ${renderThemeProperty('subtitleFontWeight', 'font-weight')};
  }
`;

export const AdIcon = ({imgUrl}: any) => {
  return imgUrl ? (
    <StyledAdIcon>
      <img src={imgUrl} />
    </StyledAdIcon>
  ) : null;
};

const StyledAdIcon = styled.div`
  height: 50px;
  width: 50px;
  margin: 10px 10px auto 10px;
  img {
    height: 50px;
    width: 50px;
    border-radius: 5px;
    box-shadow: 1px 1px 2px 0px black;
  }
`;

export const StyledAdvertsGrid = styled.div`
  ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.gridPadding, theme)};
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  .item-container {
    ${renderThemeProperty('itemBorderRadius', 'border-radius')};
    ${({theme}: any) => applyMediaQueriesForStyle('margin', theme.componentTheme.itemMargin, theme)};
    background-color: white;
  }
`;
