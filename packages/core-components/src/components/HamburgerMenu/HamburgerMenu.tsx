import * as React from 'react';
import {slide as Menu, State as BurgerMenuState} from 'react-burger-menu';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {MenuItem} from '../common/ComponentsInterface';
import styled from 'styled-components';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import * as defaults from './defaultProps.json';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import NavLink from '../common/NavLink';
import {
  LocaleResolvedMenuItem,
  MenuItemDecoratedProps,
  willHandlerSinkMenuItemSelection,
  withMenuItemDecorator
} from 'component-utils/dist/components/MultiMenuItem';

const MenuContainer = styled(CommonComponentThemeWrapper)`
  height: ${(props: any) => props.theme.componentTheme.hamburgerMenuHeight};
  display: flex;
  justify-content: center;
  align-items: center;
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};

  .bm-burger-button {
    position: relative;
    width: 75px;
    height: ${(props: any) => props.theme.componentTheme.hamburgerMenuHeight};
    display: flex;
    justify-content: center;
    align-items: center;
    img {
      width: 24px !important;
      height: 24px !important;
    }
  }
  .hamburger-hidden img,
  .hamburger-hidden > button {
    display: none;
  }

  .bm-cross-button {
    display: none;
  }
  .bm-item-list {
    overflow-y: scroll !important;
  }
  .bm-overlay {
    background: rgba(0, 0, 0, 0) !important;
    z-index: 1 !important;
  }
  .bm-menu-wrap {
    position: ${(props: any) => (props.menuOpen && !props.hamburgerOnTheLeft ? 'absolute !important' : 'fixed')};
    left: ${(props: any) => (props.hamburgerOnTheLeft ? '0px' : 'unset')};
    right: ${(props: any) => (!props.hamburgerOnTheLeft ? '0px' : 'unset')};
    ${() => applyMediaQueriesForStyle('width', {xs: '100%'}, {}, true)};
    height: 100%;
    z-index: 10 !important;
    transition: all 0.5s;
  }

  .bm-menu {
    ${renderThemeProperty('menuBackgroundColor', 'background-color')};
  }

  .itemlist-class {
    max-width: 946px;
    margin: 0 auto;
    .menu-item {
      ${renderThemeProperty('fontFamily', 'font-family')};
      ${renderThemeProperty('fontWeight', 'font-weight')};
      ${renderThemeProperty('fontSize', 'font-size')};
      line-height: 1.56;
      text-decoration: none;
      background-position: left center;
      background-repeat: no-repeat;
      background-size: 25px 26px;

      span {
        ${renderThemeProperty('textColor', 'color')};
      }
    }
    .underline-hover:hover {
      text-decoration: underline;
    }

    .root-menu-item {
      border-bottom-width: 1px;
      border-bottom-style: solid;
      ${renderThemeProperty('menuItemSeperatorColor', 'border-bottom-color')};
      padding: 1px ${(props: any) => props.theme.gutter} 1px ${(props: any) => props.theme.gutter};

      &.menu-item-opened {
        > div {
          margin-bottom: calc(${(props: any) => props.theme.gutter} * 0.5) !important;
        }
      }
    }

    .child-menu-item {
      cursor: pointer;
      border-bottom: none;
      display: block;
      padding: 1px 0 1px 0;
      span {
        font-weight: 100;
      }
      &:focus {
        border: 1px solid;
        ${renderThemeProperty('focusBorderColor', 'color')};
        padding: 0;
        margin-left: -1px !important;
      }
    }
  }

  .menu-class {
    margin-top: ${(props: any) => props.theme.componentTheme.hamburgerMenuHeight};
    position: relative !important;
    overflow: hidden !important;
    height: ${(props: any) =>
      props.menuOpen
        ? `${Math.max(
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.all[0].clientHeight,
            document.all[0].scrollHeight
          )}px !important`
        : '100%'};
  }
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: calc(${(props: any) => props.theme.gutter} * 1.25) 0;
  cursor: pointer;

  img {
    flex: 0 1 auto;
    align-self: center;
    margin-right: ${(props: any) => props.theme.gutter} !important;
  }
  span {
    align-self: center;
    flex: 2 1 auto;
  }

  i {
    align-self: center;
    flex: 0 1 auto;
    font-size: ${(props: any) => props.theme.defaultFontSize};
    ${renderThemeProperty('textColor', 'color')};
  }
`;

const ChildrenItems = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
`;

const ChildItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: calc(${(props: any) => props.theme.gutter} * 0.75) 0;

  img {
    flex: 0 1 auto;
    align-self: center;
    margin-right: ${(props: any) => props.theme.gutter} !important;
    display: none;
  }
  span {
    align-self: center;
    flex: 2 1 auto;
    margin-left: calc(${(props: any) => props.theme.gutter} * 3);
  }
`;

const CloseLink = styled.a`
  position: absolute;
  float: right;
  text-align: center;
  ${renderThemeProperty('closeMenuBackgroundColor', 'background-color')};
  cursor: pointer !important;
  z-index: 1001;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 75px;
  height: ${(props: any) => props.theme.componentTheme.hamburgerMenuHeight};

  img {
    margin: auto;
    vertical-align: middle;
    display: inline-block;
    ${renderThemeProperty('closeMenuIconWidth', 'width')};
    ${renderThemeProperty('closeMenuIconHeight', 'height')};
  }
`;

export type ComponentMenuItem = LocaleResolvedMenuItem & MenuItem;

export interface StateInterface {
  menuOpen: boolean;
  openedMenuItem: number | null;
}

export interface PropsInterface {
  className?: string;
  feature: any;
  componentTheme: any;
}

class HamburgerMenu extends React.Component<PropsInterface & MenuItemDecoratedProps, StateInterface> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {menuOpen: false, openedMenuItem: null};
  }

  private _handleStateChange(isOpen: boolean) {
    this.setState({menuOpen: isOpen});
  }

  private _closeMenu() {
    this.setState({menuOpen: false});
  }

  private _toggleMenuItem(menuItemIndex: number) {
    this.setState({openedMenuItem: this.state.openedMenuItem === menuItemIndex ? null : menuItemIndex});
  }

  private _isActive() {
    // TODO: when menus creation is completed
    // const cleanedLocation = location && location.pathname && location.pathname.replace('/', '');
    // return menuUid === cleanedLocation;
    return false;
  }

  private _generateMenuItem(menuItem: ComponentMenuItem, index: number) {
    if (menuItem.children && menuItem.children.length) {
      const isMenuOpen = this.state.openedMenuItem === index;
      const willSinkMenuItemSelection = willHandlerSinkMenuItemSelection(menuItem);
      const {handleMenuItemSelection} = this.props;

      return (
        <div
          className={'menu-item root-menu-item' + (isMenuOpen ? ' menu-item-opened' : '')}
          key={index}
          tabIndex={0}
          onKeyPress={() => this._toggleMenuItem(index)}
        >
          <Item style={{margin: menuItem.margin}} onClick={() => this._toggleMenuItem(index)}>
            <img
              src={menuItem.iconUrl}
              height={menuItem.iconHeight}
              alt={menuItem.labels[this.props.intl.locale] ? menuItem.labels[this.props.intl.locale].iconAltText : ''}
            />
            <span children={menuItem.label ? menuItem.label : ''} />
            {menuItem.children ? <i className={isMenuOpen ? 'fa fa-chevron-up' : 'fa fa-chevron-down'} /> : null}
          </Item>
          {isMenuOpen ? (
            <ChildrenItems>
              {menuItem.children.map((childMenuItem: any, childIndex: number) => {
                const key = index + '_' + childIndex;
                const labelElement = (
                  <ChildItem>
                    <img
                      src={menuItem.iconUrl}
                      height={menuItem.iconHeight}
                      alt={
                        menuItem.labels[this.props.intl.locale]
                          ? menuItem.labels[this.props.intl.locale].iconAltText
                          : ''
                      }
                    />
                    <span>{childMenuItem.label}</span>
                  </ChildItem>
                );

                return willSinkMenuItemSelection ? (
                  <div
                    tabIndex={0}
                    key={key}
                    className="menu-item child-menu-item"
                    onClick={e => handleMenuItemSelection(e, index, childIndex, this._closeMenu.bind(this))}
                  >
                    {labelElement}
                  </div>
                ) : (
                  <NavLink
                    className="menu-item child-menu-item"
                    activeClassName="selected"
                    key={key}
                    to={childMenuItem.url}
                    isActive={this._isActive}
                    onClick={this._closeMenu.bind(this)}
                  >
                    {labelElement}
                  </NavLink>
                );
              })}
            </ChildrenItems>
          ) : null}
        </div>
      );
    } else {
      return (
        <NavLink
          className={
            'menu-item root-menu-item ' +
            (this.props.componentTheme.hamburgerMenuHoverUnderline ? 'underline-hover' : '')
          }
          activeClassName="selected"
          key={index}
          to={menuItem.url}
          isActive={this._isActive}
          onClick={this._closeMenu.bind(this)}
        >
          <Item style={{margin: menuItem.margin}}>
            <img
              src={menuItem.iconUrl}
              height={menuItem.iconHeight}
              alt={menuItem.labels[this.props.intl.locale] ? menuItem.labels[this.props.intl.locale].iconAltText : ''}
            />
            <span children={menuItem.label ? menuItem.label : ''} />
          </Item>
        </NavLink>
      );
    }
  }

  public render() {
    const links = this.props.feature.menuItems || [];
    const {hamburgerCloseMenuIcon, hamburgerMenuIcon} = this.props.feature;
    const props: any = {};
    if (this.props.componentTheme.hamburgerOnTheLeft) {
      props.left = true;
    } else {
      props.right = true;
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <MenuContainer menuOpen={this.state.menuOpen} hamburgerOnTheLeft={this.props.componentTheme.hamburgerOnTheLeft}>
          <Menu
            {...props}
            burgerButtonClassName={this.state.menuOpen ? 'hamburger-hidden' : 'hamburger-show'}
            itemListClassName={'itemlist-class'}
            menuClassName={'menu-class'}
            width={'100%'}
            isOpen={this.state.menuOpen}
            onStateChange={(state: BurgerMenuState) => this._handleStateChange(state.isOpen)}
            disableOverlayClick
            customBurgerIcon={<img src={hamburgerMenuIcon} alt="Open Hamburger Menu" />}
          >
            {links.map((menuItem: ComponentMenuItem, index: number) => this._generateMenuItem(menuItem, index))}
          </Menu>

          <CloseLink
            className="close-menu"
            onClick={() => this.setState({menuOpen: false})}
            style={{display: this.state.menuOpen ? 'flex' : 'none'}}
          >
            <img src={hamburgerCloseMenuIcon} alt="Close hamburger menu" onClick={this._closeMenu.bind(this)} />
          </CloseLink>
        </MenuContainer>
      </PortalComponent>
    );
  }
}

export default withMenuItemDecorator(HamburgerMenu, 'menuItems') as any;
