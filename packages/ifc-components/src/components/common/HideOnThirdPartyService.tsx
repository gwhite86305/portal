import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getThirdPartyVendorState, deviceHasServiceTier} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, connected: true};
  if (state.device && state.profile) {
    props.hasThirdPartyVendorTier = deviceHasServiceTier(state.device) && getThirdPartyVendorState(state.device, state.profile);
  }
  return props;
})
export default class HideOnThirdPartyService extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.hasThirdPartyVendorTier ? null : this.props.children;
  }
}
