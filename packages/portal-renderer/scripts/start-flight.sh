#!/bin/sh

if [ ! -z "${VESSEL_ID}" ] && [ ! -z "${FLIGHT_NUM}" ]; then
    COLLECTION_ID=$(docker exec -i docker_database_1 mysql --user=tp_pbe --password=tppassword1 --host=localhost -rs \
      -e "select id from collection order by id desc limit 1" twinpeaks_${DB_SUFFIX})

    echo Collection ID is: $COLLECTION_ID

    # Add collectionRule for deployed portal
   docker exec -i docker_database_1  mysql  --protocol=tcp --user=tp_pbe --password=tppassword1 --host=localhost \
      -e "insert into collectionRules (customerId, collectionId) VALUES (${CUSTOMER_ID}, $COLLECTION_ID)" twinpeaks_${DB_SUFFIX}

    # Add test vehicle to DB
    docker exec -i docker_database_1 mysql  --protocol=tcp --user=tp_pbe --password=tppassword1 --host=localhost \
      -e "insert ignore into vehicle (vid,customerId,logLevel) VALUES ('${VESSEL_ID}', ${CUSTOMER_ID}, 65535)" twinpeaks_${DB_SUFFIX}

    curl -X POST \
    -H "Content-Type: application/json" \
    -H "Cache-Control: no-cache" \
    -d \
    "
    {
      \"platformConfiguration\":
        {
          \"common\": {
            \"vesselId\": \"${VESSEL_ID}\",
            \"eventTime\": 1465335226826
          },
          \"arincOriginDestination\": \"KDFW KLGA\",
          \"flightIdentity\": \"${VESSEL_ID}_SF_$(date +%s)\",
          \"flightNumber\": \"${FLIGHT_NUM}\"
        }
    }
    " "http://localhost:9876/updatePlatformStatus"
fi

