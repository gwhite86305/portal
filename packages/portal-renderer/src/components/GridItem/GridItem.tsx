import * as React from 'react';
import {GridLayoutThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {GridItem as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

export const GridItemComponentName = 'GridItem'; // single src of truth for componentName comparisons

export interface InternalInterface {
  children?: any;
}
const GridItem: React.FunctionComponent<InternalInterface & ExternalInterface> = props => {
  const {feature, componentTheme, children} = props;

  return (
    <PortalComponent componentTheme={componentTheme}>
      <GridLayoutThemeWrapper
        {...{
          container: false,
          item: true,
          ...feature.columnSizes
        }}
      >
        {children}
      </GridLayoutThemeWrapper>
    </PortalComponent>
  );
};

GridItem.defaultProps = defaults;

export default GridItem;
