import {ISettingsState} from './types';
import {Action, ActionCreators} from './actions';

export const NO_AGE_RESTRICTION = 'SHOW ALL';

export const initialState: ISettingsState = {
  ageRatingPassword: '',
  ageRatingClasses: [],
  ageRatingClassSelected: NO_AGE_RESTRICTION
};

export default function reducer(state: ISettingsState = initialState, {type, payload}: Action): ISettingsState {
  switch (type) {
    case ActionCreators.ageRatingsClassesStart.type:
      return {...state, ageRatingClasses: []};
    case ActionCreators.ageRatingsClassesSuccess.type:
      return {...state, ageRatingClasses: payload};
    case ActionCreators.ageRatingsClassesError.type:
      return {...state, ageRatingClasses: []};
    case ActionCreators.selectAgeRatingClass.type:
      return {...state, ageRatingClassSelected: payload};
    case ActionCreators.loadSettings.type:
    case ActionCreators.saveSettings.type:
      return {
        ...state,
        ageRatingPassword: payload.ageRatingPassword,
        ageRatingClassSelected: payload.ageRatingClassSelected
      };
    default:
      return state;
  }
}
