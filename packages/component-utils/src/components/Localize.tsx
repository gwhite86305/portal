import * as React from 'react';
import drop from 'lodash-es/drop';
import {injectIntl} from 'react-intl';
import {clone, compose} from 'ramda';
import {ComponentContext} from '../utils/Contexts';

export interface ILocalize {
  intl: any;
  messageId: string;
  args?: any;
}

const Localize: React.FunctionComponent<any> = (props: ILocalize) => {
  const componentUid = React.useContext(ComponentContext) as string;
  return localize(props, componentUid);
};

export const localize = (props: ILocalize, componentUid: string) => {
  const {intl, messageId, args} = props;
  const overriddenMessageId = componentUid + '.' + drop(messageId.split('.'), 2).join('.');
  const overriddenMessage = intl.messages[overriddenMessageId];
  const originalMessage = intl.messages[messageId];
  //If we have an overridden message then display that
  //If we have a default message and no override was specified
  if (overriddenMessage || (originalMessage && !intl.messages.hasOwnProperty(overriddenMessageId))) {
    return intl.formatMessage({id: overriddenMessage ? overriddenMessageId : messageId}, args);
  } else {
    return null;
  }
};

export const localizeToProperties = (intl: any, componentUid: string, propLocales: any) => {
  const localizedProps: any = {};
  propLocales.forEach((propLocale: any) => {
    const k = Object.keys(propLocale)[0];
    localizedProps[k] = localize({intl, messageId: propLocale[k]}, componentUid);
  });
  return localizedProps;
};

export const resolveMenuItemsLocale = (intl: any, menuItems: any) => {
  const resolveMenuItems = () => (m: [any]) => {
    const a = m.map(menuItem => ({...menuItem, ...menuItem.labels[intl.locale]}));
    return a;
  };
  return compose(
    resolveMenuItems(),
    clone
  )(menuItems);
};

export function withLocaleProps(WrappedComponent: any, localeToPropsMap: any) {
  const ComponentWithLocaleProps = class extends React.Component<any, any> {
    constructor(props: any) {
      super(props);
    }

    render() {
      const {intl, componentUid} = this.props;

      const localizedProps = localizeToProperties(intl, componentUid, localeToPropsMap);
      // note: important: this.props should be allowed override localizedProps, allowing directing rendering to direclty
      // override localized if required by parent component
      return <WrappedComponent {...localizedProps} {...this.props} />;
    }
  };

  const ComponentWithLocalePropsIntl = injectIntl(ComponentWithLocaleProps);
  ComponentWithLocalePropsIntl.defaultProps = WrappedComponent.defaultProps;
  return ComponentWithLocalePropsIntl;
}

export default injectIntl(Localize);
