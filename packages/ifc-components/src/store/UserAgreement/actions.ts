import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {getUserAgreementHash} from '../../utils/UserAgreementUtils';

const {generateMessage} = utils.APIError;

// Action Creators
export const ActionCreators = {
  UserAgreementAcceptanceSaved: new ActionCreator<'UserAgreementAcceptanceSaved', any>('UserAgreementAcceptanceSaved'),
  UserAgreementAcceptanceRetrieved: new ActionCreator<'UserAgreementAcceptanceRetrived', any>(
    'UserAgreementAcceptanceRetrived'
  ),
  UserAgreementSignupStart: new ActionCreator<'UserAgreementSignupStart', any>('UserAgreementSignupStart'),
  UserAgreementSignupSuccess: new ActionCreator<'UserAgreementSignupSuccess', any>('UserAgreementSignupSuccess'),
  UserAgreementSignupError: new ActionCreator<'UserAgreementSignupError', any>('UserAgreementSignupError')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function saveAcceptanceRecord(accepted: boolean, props: any) {
  return (dispatch: any, getState: any) => {
    const hash = getUserAgreementHash(props, getState());

    let payload = {
      accepted: accepted,
      errorMessage: {},
      hash: hash
    };

    try {
      localStorage.setItem('ua_accepted', 'true');
      localStorage.setItem('ua_hash', hash);
    } catch (e) {
      payload.errorMessage = generateMessage('Could not store ua_accepted value into the localstorage', e);
    }
    dispatch(ActionCreators.UserAgreementAcceptanceSaved.create(payload));
  };
}

export function sendDefaultSignup(tierId: number) {
  return (dispatch: any) => {
    dispatch(ActionCreators.UserAgreementSignupStart.create({signup: {waitingForResponse: true}}));
    axios
      .post('signup', {auth: {type: 'mac'}, terms: true, serviceTierId: tierId})
      .then(response => {
        if (response.data && response.data.code) {
          dispatch(ActionCreators.UserAgreementSignupError.create({signup: {waitingForResponse: false}}));
          throw new Error(response.data && response.data.code);
        } else {
          dispatch(ActionCreators.UserAgreementSignupSuccess.create({signup: {waitingForResponse: false}}));
        }
      })
      .catch(error => {
        dispatch(
          ActionCreators.UserAgreementSignupError.create({
            signup: {
              ...generateMessage('Could not get response from signup call', error),
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function getAcceptanceRecord() {
  return (dispatch: any) => {
    let payload = {
      accepted: false,
      errorMessage: {},
      hash: ''
    };

    try {
      payload.accepted = localStorage.getItem('ua_accepted') == 'true';
      payload.hash = localStorage.getItem('ua_hash') || '';
    } catch (e) {
      payload.errorMessage = generateMessage('Could not retrive ua_accepted value from the localstorage', e);
    }

    dispatch(ActionCreators.UserAgreementAcceptanceRetrieved.create(payload));
  };
}
