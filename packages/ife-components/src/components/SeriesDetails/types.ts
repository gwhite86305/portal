import {trackView, trackPlay} from '../../store/analytics/actions';
import {SystemMessage} from 'component-utils/dist/store/system/reducer';
import {playItem} from '../../store/mediaplayer/actions.player';
import {addItemsToPlaylist, removeItemFromPlaylist} from '../../store/mediaplayer/actions.playlist';
import {getVideoById} from '../../store/videos/actions';
import {SeriesDetails as ExternalInterface} from '../common/ComponentsInterface';
import {VideoGroup, VideoItem} from './../../store/videos/types';

// Container
export interface StoreProps {
  playlistIdsMap: {[key: string]: string};
  videoItems: VideoGroup[];
  videoNotFound: boolean;
  selectedItem?: VideoGroup;
  playlistLoading: boolean;
}
export interface DispatchProps {
  actions: {
    addItemsToPlaylist: typeof addItemsToPlaylist;
    getVideoById: typeof getVideoById;
    playItem: typeof playItem;
    removeItemFromPlaylist: typeof removeItemFromPlaylist;
    trackView: typeof trackView;
    trackPlay: typeof trackPlay;
    showMessage: (message: SystemMessage) => void;
    hideMessage: () => void;
  };
}
export interface SeriesDetailsContainerProps extends StoreProps, DispatchProps, ExternalInterface {}
export interface SeriesDetailsContainerState {
  contentId: string;
}

// SeriesDetails
export interface SeriesDetailsProps {
  addItemsToPlaylist: typeof addItemsToPlaylist;
  imgPlaceholder: string;
  item: VideoGroup;
  onClickBack(): any;
  playItem(item: VideoItem): any;
  playlistIdsMap: {[key: string]: string};
  removeItemFromPlaylist: typeof removeItemFromPlaylist;
}

// Seasons
export interface SeasonsProps {
  addItemsToPlaylist: typeof addItemsToPlaylist;
  playItem(item: VideoItem): any;
  removeItemFromPlaylist: typeof removeItemFromPlaylist;
  playlistIdsMap: {[key: string]: string};
  seasons: {[key: string]: VideoItem[]};
}
export interface SeasonsState {
  expanded: string;
}
