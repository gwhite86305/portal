// @ts-nocheck
const CONFIG = {
  API_SERVER_PATH: process.env.API_BUILDER_URL,
  BUILDER_WEB_URL: process.env.UXBUILDER_URL,
  SCHEMA_WEB_URL: process.env.SCHEMAWEBURL || 'http://localhost:3003',
  TEST_CHROME_URL: process.env.TEST_CHROME_URL || false
};
function getConfig() {
  return {
    appUrl: CONFIG.BUILDER_WEB_URL,
    puppeteer: {args: [`--window-size=1920,1080`]},
    testChromeUrl: CONFIG.TEST_CHROME_URL
  };
}

async function click(page, selector) {
  await page.$eval(selector, el => el.click());
}

async function login(browser) {
  const page = await global.__BROWSER__.newPage();
  await page.goto(`${getConfig().appUrl}/portals`);
  await page.waitFor('.temp-auth-form');
  await page.type('#username', 'viasatAdmin');
  await page.type('#password', 'viasatAdmin');
  await click(page, 'button');
  await page.waitForNavigation();
  await page.waitFor('.portal-page-heading h1');
  return page;
}

async function logout(page) {
  const avatars = await page.$$('.header-avatar');
  if (avatars.length === 0) return;
  await page.$eval('.header-avatar', el => el.click());
  await page.waitFor('[role=menuitem]:last-child');
  await click(page, '[role=menuitem]:last-child');
  await page.waitFor('.temp-auth-form');
  return page;
}

async function logoutAndClose(page) {
  await logout(page);
  await page.close();
  return page;
}

module.exports = {
  click,
  login,
  logout,
  logoutAndClose
};
