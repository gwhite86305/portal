import {PortalComponent, utils} from 'component-utils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import * as React from 'react';
import ScheletonLoader from '../common/CarouselView/ScheletonLoader';
import Series from './Series';
import ErrorMessage from '../common/ErrorMessage';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import {Props} from './types';
import {VideoCategory} from '../../store/videos/types';
import {Subject, fromEvent} from 'rxjs';
import {debounceTime, takeUntil} from 'rxjs/operators';
import SeriesGrid from './SeriesGrid';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
class SeriesContainer extends React.Component<Props, {width: number}> {
  public static contextType = RouterContext;
  private _unsubscribe = new Subject();
  state = {
    width: window.innerWidth
  };

  private handleResize = () => this.setState({width: window.innerWidth});

  componentDidMount() {
    const {feature: {gridLayout}} = this.props;

    gridLayout &&
      fromEvent(window, 'resize')
        .pipe(debounceTime(100), takeUntil(this._unsubscribe))
        .subscribe(() => this.handleResize());

    this.onUpdate(this.props);
    if (!this.props.storeState.seriesCategories.length) {
      this.props.actions.getVideoCategories();
    }
  }

  componentWillUnmount() {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }

  renderSeries = () => {
    const {storeState: {isLoadingSeries, items}, feature: {gridLayout}} = this.props;
    if (isLoadingSeries) {
      return <ScheletonLoader />;
    }
    return items.length ? (
      gridLayout ? (
        <SeriesGrid
          seriesItems={items}
          width={this.state.width}
          handleClick={this.navigateToDetailPage}
        />
      ) : (
        <Series
          onItemInViewPort={this.props.actions.trackImpression}
          seriesCategories={this.seriesCategories()}
          onClick={this.navigateToDetailPage.bind(this)}
        />
      )
    ) : (
      <ErrorMessage text={'No Series Available'} />
    );
  };

  render() {
    return <PortalComponent componentTheme={this.props.componentTheme}>{this.renderSeries()}</PortalComponent>;
  }

  private seriesCategories = (): VideoCategory[] => {
    const {seriesCategories: categories, items} = this.props.storeState;
    return categories
      .map(({title, uid}) => ({
        category: title,
        uid,
        items: items.filter(item => item.categories.find(category => category === uid))
      }))
      .concat({
        category: 'Others',
        uid: 'others',
        items: items.filter(item => !item.categories.length)
      })
      .filter(category => category.items.length);
  };

  private shouldFetch = (nextProps: Props) =>
    !nextProps.storeState.seriesFetchCompleted && !nextProps.storeState.isLoadingSeries;

  private onUpdate(nextProps: Props) {
    this.shouldFetch(nextProps) && this.props.actions.getSeries();
  }

  private navigateToDetailPage = (contentUid: string) => {
    this.context.history.push(`${this.props.feature.synopsisPage}?videoId=${contentUid}`);
  };
}

export default SeriesContainer;
