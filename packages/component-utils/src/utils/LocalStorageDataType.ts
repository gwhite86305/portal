const enum LocalStorageDataType {
  locale = 'locale',
  currency = 'currency'
}

export default LocalStorageDataType;
