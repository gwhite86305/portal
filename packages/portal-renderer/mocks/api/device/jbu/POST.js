module.exports = function(request, response) {
  const respJson = {
    destinationConditions: 'Light rain',
    altitude: 11000,
    latitude: 30.569,
    serviceTimeRemaining: null,
    connectionState: 0,
    profileHash: 'de9a109c10b7129b5b21e7244ed963be',
    username: null,
    guid: '0ac538b6-f983-11e8-80df-0242ac120007',
    heading: 3.14,
    isPausedSystem: false,
    messagesNew: 0,
    destinationTemperature: 59.0,
    serviceTierId: 0,
    messagesOld: 0,
    lanIpAddress: '172.27.121.43',
    wowState: 'Off',
    flightOrigin: 'KLAX',
    category: 'PED',
    logLevel: 65535,
    language: 'it',
    isServiceAvailable: true,
    browserLanguage: 'en-US',
    isPausedUser: false,
    authorizationState: 0,
    isAuthorized: false,
    alertId: '30101',
    flightMinutesRemaining: 222.3,
    typeId: 0,
    isProfileExpired: false,
    flightDestination: 'KDAL',
    isConnected: false,
    tailId: 'NOS01',
    serviceStartTime: null,
    ifeState: null,
    flightNumber: 'NOS143',
    longitude: -96.276,
    destinationWeatherIcon: 12,
    macAddress: '172027121043',
    serviceUid: null,
    authType: null,
    purchaseAvailability: true
  };

  console.log(Object.keys(request));
  const newLang = 'es'; // TODO: HOW TO READ THIS FROM BODY?

  respJson['language'] = newLang;

  response
    .status(200)
    .json(respJson)
    .end();
};
