import * as React from 'react';
import configureStore from './configureStore';
import storeAccess from 'component-utils/dist/utils/storeAccess';
import axios from 'axios';
import { EpicMiddleware } from 'redux-observable';

export const initStoreWithComponents = (initialState: any): {store: any; history: any, epicMiddleware: EpicMiddleware<any>} => {
  const storeInfo = configureStore(initialState);

  storeAccess.set(storeInfo.store);

  initAPIConfig();

  return storeInfo;
};

export const initAPIConfig = () => {
  axios.defaults.headers['Content-Type'] = 'application/json';
  axios.defaults.headers['Accept'] = 'application/json';
  axios.defaults.timeout = 30000; // TODO appropriate??
  // axios.defaults.headers['X-Language'] = getLocale();
  //axios.defaults.headers['X-Currency'] = getCurrency();
  // axios.defaults.baseURL = "/api";
};
