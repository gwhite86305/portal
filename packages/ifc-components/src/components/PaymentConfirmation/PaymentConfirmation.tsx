import * as React from 'react';
import {PortalComponent, sanitize, utils, Currency} from 'component-utils';
import {getUTCDate} from 'component-utils/dist/utils/DateUtils';
import {Checkout as ExternalInterface} from '../common/ComponentsInterface';
import {Button, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  WifiServiceOptions?: any;
  feature?: any;
  header?: string;
  thankYouNoteText?: string;
  transactionIdLabel?: string;
  dateLabel?: string;
  flightNumberLabel?: string;
  merchantOfRecordLabel?: string;
  merchantOfRecordLabelValue?: string;
  paymentMethodLabel?: string;
  serviceCostLabel?: string;
  discountLabel?: string;
  totalLabel?: string;
  buttonText?: string;
  onButtonClick?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  display: flex;
  flex-direction: column;
  h1.header-text {
    margin: 0px;
    ${renderThemeProperty('headerMarginTop', 'margin-top')};
    margin-bottom: calc(${(props: any) => props.theme.gutter} *2.5);
    ${renderThemeProperty('headerFontFamily', 'font-family')};
    ${renderThemeProperty('headerColor', 'color')};
    ${renderThemeProperty('headerFontSize', 'font-size')};
    font-weight: normal;
    line-height: 1.04;
  }
  div.body-text {
    display: flex;
    align-items: center;
    margin-bottom: calc(${(props: any) => props.theme.gutter} *1.25);
    font-weight: normal;
    line-height: 1.187;
    color: ${(props: any) => props.theme.quaternaryBackgroundColor};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} *2.5);
   
    h2 {
      margin: 0px;
      margin-left: 10px;
      font-family: ${(props: any) => props.theme.primaryFontFamily};
      ${renderThemeProperty('thankYouNoteColor', 'color')};
      ${renderThemeProperty('thankYouNoteFontSize', 'font-size')};
    }
    a {
        color: ${(props: any) => props.theme.secondaryTextColor};
      }
    }
  }
  p.validation-summary {
    color: ${(props: any) => props.theme.formErrorColor};
    margin: 0px;
    margin-bottom: calc(${(props: any) => props.theme.gutter} *1.25);
    font-weight: bold;
    display: inline-block;
  }
  .transaction-record {
    display: flex;
    ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
  }
  .infos {
    display: flex;
    flex-direction: column;
    ${(props: any) => applyMediaQueriesForStyle('flex-basis', {xs: 'inherit', md: '0'}, props.theme)};
    flex-grow: 1;
    .row {
      display: flex;
      justify-content: space-between;
      border: 0;
      border-top: 1px solid;
      ${renderThemeProperty('dividerColor', 'border-color')};
      padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
      .label {
        ${renderThemeProperty('labelFontSize', 'font-size')};
        ${renderThemeProperty('labelColor', 'color')};
      }
      .total-label {
        ${renderThemeProperty('subTotalLabelColor', 'color')};
        ${renderThemeProperty('subTotalLabelFontSize', 'font-size')};
      }
      .value {
        ${renderThemeProperty('labelValueFontSize', 'font-size')};
        ${renderThemeProperty('labelValueColor', 'color')};
        text-align: right;
      }
      .total-value {
        ${renderThemeProperty('subTotalCostColor', 'color')};
        ${renderThemeProperty('subTotalCostFontSize', 'font-size')};
      }
    }
    .row:last-child {
      align-items:center;
      min-height: calc(${(props: any) =>
        props.theme.componentTheme.rowLastMinHeight
          ? props.theme.componentTheme.rowLastMinHeight
          : props.theme.gutter} * 3.375);
      border-bottom: 1px solid;
      ${renderThemeProperty('dividerColor', 'border-color')};
    }
  }
  .infos:first-child {
    ${(props: any) => applyMediaQueriesForStyle('padding-right', {xs: '0px', md: '16px'}, props.theme)};
    .row:last-child {
      align-items:center;
      border-style: solid;
      ${renderThemeProperty('dividerColor', 'border-color')};
      ${(props: any) => applyMediaQueriesForStyle('border-bottom-width', {xs: '0px', md: '1px'}, props.theme)};
    }
  }
  .infos:last-child {
    ${(props: any) => applyMediaQueriesForStyle('padding-left', {xs: '0px', md: '16px'}, props.theme)};
  }
  div.button-wrap {
    display: flex;
    justify-content: center;
    align-items: center;
    button {
      ${(props: any) =>
        applyMediaQueriesForStyle('margin-top', {xs: `calc(${props.theme.gutter}*1.875)`}, props.theme)};
      margin: 0px;
      ${(props: any) => applyMediaQueriesForStyle('width', {xs: '160px', md: '160px'}, props.theme)};
      ${renderThemeProperty('buttonWidth', 'width')};
      ${renderThemeProperty('buttonHeight', 'height')};
      ${renderThemeProperty('buttonBorder', 'border')};
      ${renderThemeProperty('buttonFontSize', 'font-size')};
      ${renderThemeProperty('buttonFontWeight', 'font-weight')};
      white-space: nowrap;
      justify-content: center;
    }
  }
  .thankyou-text{
     ${renderThemeProperty('thankYouFontWeight', 'font-weight')};
  }
`;

class PaymentConfirmation extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults
  };

  constructor(props: any) {
    super(props);
    this._handleButtonClick = this._handleButtonClick.bind(this);
  }

  _handleButtonClick(event: any) {
    event.preventDefault();
    this.props.onButtonClick();
  }

  public render() {
    const {WifiServiceOptions} = this.props;
    const {
      header,
      thankYouNoteText,
      transactionIdLabel,
      dateLabel,
      flightNumberLabel,
      merchantOfRecordLabel,
      merchantOfRecordLabelValue,
      paymentMethodLabel,
      serviceCostLabel,
      discountLabel,
      totalLabel,
      buttonText,
      currencyCode,
      currencySuffix,
      locale,
      minimumFractionDigits,
      maximumFractionDigits,
      thankYouIcon
    } = this.props;

    const transaction =
      (WifiServiceOptions &&
        WifiServiceOptions.orderHistory &&
        WifiServiceOptions.orderHistory.transactions &&
        WifiServiceOptions.orderHistory.transactions[0]) ||
      {};
    const showSpinner =
      WifiServiceOptions &&
      WifiServiceOptions.orderHistory &&
      WifiServiceOptions.orderHistory.waitingForResponse === true;

    let error =
      WifiServiceOptions.orderHistory &&
      WifiServiceOptions.orderHistory.waitingForResponse === false &&
      WifiServiceOptions.orderHistory.error;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-payment-confirmation-component">
          {showSpinner ? <Spinner /> : null}
          <div className="header">
            {header ? <h1 className="header-text">{header}</h1> : null}
            {thankYouNoteText ? (
              <div className="body-text">
                {thankYouIcon ? (
                  <img src={thankYouIcon} height="36px" width="36px" />
                ) : (
                  <i className="fa fa-check-circle" />
                )}
                <h2 className="thankyou-text" dangerouslySetInnerHTML={sanitize(thankYouNoteText)} />
              </div>
            ) : null}
          </div>
          {error ? <p className="validation-summary">{error.friendlyMessage}</p> : null}
          <div className="transaction-record">
            <div className="infos">
              <div className="row">
                <span className="label">{transactionIdLabel}</span>{' '}
                <span className="value">{transaction.externalId}</span>
              </div>
              <div className="row">
                <span className="label">{dateLabel}</span>
                <span className="value">{getUTCDate(new Date(transaction.timeOfPurchase * 1000))}</span>
              </div>
              <div className="row">
                <span className="label">{flightNumberLabel}</span>{' '}
                <span className="value">{transaction.flightNumber}</span>
              </div>
              <div className="row">
                <span className="label">{merchantOfRecordLabel}</span>
                <span className="value address">
                  {merchantOfRecordLabelValue ? (
                    <span dangerouslySetInnerHTML={sanitize(merchantOfRecordLabelValue)} />
                  ) : (
                    <span>
                      Viasat Inc. <br /> 6155 El Camino Real <br />Carlsbad, CA 92009
                    </span>
                  )}
                </span>
              </div>
            </div>
            <div className="infos">
              <div className="row">
                <span className="label">{paymentMethodLabel}</span>{' '}
                <span className="value">{transaction.paymentMethod}</span>
              </div>
              <div className="row">
                <span className="label">{serviceCostLabel}</span>{' '}
                <span className="value">
                  <Currency
                    currency={currencyCode}
                    value={transaction.retailCost}
                    locale={locale}
                    minimumFractionDigits={minimumFractionDigits}
                    maximumFractionDigits={maximumFractionDigits}
                    currencySuffix={currencySuffix}
                  />
                </span>
              </div>
              <div className="row">
                <span className="label">{discountLabel}</span>{' '}
                <span className="value">
                  <Currency
                    currency={currencyCode}
                    value={transaction.promoDiscount}
                    locale={locale}
                    minimumFractionDigits={minimumFractionDigits}
                    maximumFractionDigits={maximumFractionDigits}
                    currencySuffix={currencySuffix}
                  />
                </span>
              </div>
              <div className="row">
                <span className="label total-label">{totalLabel}</span>
                <span className="value total-value">
                  <Currency
                    currency={currencyCode}
                    value={transaction.totalCost}
                    locale={locale}
                    minimumFractionDigits={minimumFractionDigits}
                    maximumFractionDigits={maximumFractionDigits}
                    currencySuffix={currencySuffix}
                  />
                </span>
              </div>
            </div>
          </div>
          <div className="button-wrap">
            <Button isPrimary={true} onClick={this._handleButtonClick} aria-label={buttonText}>
              {buttonText}
            </Button>
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default PaymentConfirmation;
