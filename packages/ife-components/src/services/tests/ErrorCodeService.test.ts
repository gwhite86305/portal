import {ErrorCodeService, ErrorCode} from '../ErrorCodeService';
import * as localeMessages from '../../locale/error-messages.json';

// test getErrorMessage
describe('localiseMessage', () => {
  it('should localise message', () => {
    const errorCodeService = new ErrorCodeService();
    const message = errorCodeService.localiseMessage({code: ErrorCode.INVALID_TOTAL_PRICE_OF_ORDER_ITEMS, message: ''});
    expect(message).toEqual((localeMessages as any).INVALID_TOTAL_PRICE_OF_ORDER_ITEMS)
  });

  it('should not localise invalid code message', () => {
    const errorCodeService = new ErrorCodeService();
    const message = errorCodeService.localiseMessage({code: 424242424242424242, message: 'shouldntChange'});
    expect(message).toEqual('shouldntChange')
  });
});

// test parse of errors
describe('localiseErrors', () => {
  it('should handle empty errors', () => {
    const errorCodeService = new ErrorCodeService();
    expect(errorCodeService.localiseErrors([])).toEqual([]);
  });

  it('should localse errors', () => {
    const errorCodeService = new ErrorCodeService();
    const errors = errorCodeService.localiseErrors([
      {
        code: ErrorCode.INVALID_TOTAL_PRICE_OF_ORDER_ITEMS,
        message: ''
      },
      {
        code: ErrorCode.VOUCHER_EXPIRED,
        message: ''
      },
      {
        code: ErrorCode.VOUCHER_ALREADY_USED_IN_DIFFERENT_SESSION,
        message: ''
      },
      {
        code: ErrorCode.ORDER_PAID_BUT_NOT_COMPLETED,
        message: ''
      }
    ]);

    expect(errors[0].message).toEqual((localeMessages as any).INVALID_TOTAL_PRICE_OF_ORDER_ITEMS);
    expect(errors[1].message).toEqual((localeMessages as any).VOUCHER_EXPIRED);
    expect(errors[2].message).toEqual((localeMessages as any).VOUCHER_ALREADY_USED_IN_DIFFERENT_SESSION);
    expect(errors[3].message).toEqual((localeMessages as any).ORDER_PAID_BUT_NOT_COMPLETED);
  });

  it('should not localise invalid code errors', () => {
    const errorCodeService = new ErrorCodeService();
    const initialError = {
      code: 4242424242424242,
      message: 'should not change this value'
    };
    const errors = errorCodeService.localiseErrors([{...initialError}]);
    expect(errors[0]).toEqual(initialError);
  });
});
