import * as React from 'react';
import {SizeMe} from 'react-sizeme';
import {getOptimizedImgSrc} from '../utils';
import {MediaThumbnail} from './MediaThumbnail';
import {MediaThumbnailWithSizeProps} from './types';

export const MediaThumbnailWithSize = (props: MediaThumbnailWithSizeProps) => (
  <SizeMe>
    {({size}) => (
      <MediaThumbnail
        item={props.item}
        className="poster"
        src={getOptimizedImgSrc({
          images: props.imageVariants,
          variant: props.variantType,
          imageWidth: size.width!,
          pixelRatio: window.devicePixelRatio
        })}
        clickable={props.clickable}
      />
    )}
  </SizeMe>
);
