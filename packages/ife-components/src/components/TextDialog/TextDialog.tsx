import * as React from 'react';
import {Dialog, DialogContentText} from '@material-ui/core';
import StyledDialogTitle from './StyledDialogTitle';
import StyledDialogContent from './StyledDialogContent';
import StyledDialogActions from './StyledDialogActions';
import StyledDialogButton from './StyledDialogButton';
import * as defaultProps from './defaultProps.json';

export interface TextDialogProps {
  handleClose: () => void;
  open: boolean;
  dialogTitle: string;
  dialogText: string;
  dialogCloseTitle: string;
}

const TextDialog: React.FC<TextDialogProps> = ({
  dialogTitle,
  open,
  dialogText,
  dialogCloseTitle,
  handleClose
}) => {
  return (
    <Dialog open={open} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <StyledDialogTitle>{dialogTitle}</StyledDialogTitle>
      <StyledDialogContent>
        <DialogContentText>{dialogText}</DialogContentText>
      </StyledDialogContent>
      <StyledDialogActions>
        <StyledDialogButton onClick={handleClose}>{dialogCloseTitle}</StyledDialogButton>
      </StyledDialogActions>
    </Dialog>
  );
};
TextDialog.defaultProps = defaultProps as any;

export default TextDialog;
