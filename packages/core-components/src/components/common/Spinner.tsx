import * as React from 'react';
import {ResponsiveImage, utils, sanitize} from 'component-utils';

import styled from 'styled-components';
const connectWrapper = utils.connectWrapper.default;

const SpinnerContainer = styled.div`
  div.spinner-overlay {
    background-color: ${(props: any) => props.theme.secondaryBackgroundColor};
    opacity: 0.9;
    width: 100%;
    align-items: center;
    justify-content: center;
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 3000;
    overflow: scroll;
  }
  p.spinner-text {
    color: #ffffff;
    z-index: 3002;
    position: fixed;
    text-align: center;
    width: 100%;
    margin: auto;
    top: 59%;
    left: 0;
    right: 0;
    bottom: 0;
  }
  .spinner-modal {
    z-index: 3001;
    background: none;
    width: 147px;
    margin: auto;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }
  img {
    height: 100%;
  }
`;

@connectWrapper((state: any, ownProps: any) => {
  const spinnerIcon = state.ux.settings && state.ux.settings.core && state.ux.settings.core.spinnerIcon;
  return {spinnerIcon};
})
@connectWrapper((state: any, ownProps: any) => {
  const spinnerText = state.ux.settings && state.ux.settings.core && state.ux.settings.core.spinnerText;
  return {spinnerText};
})
class Spinner extends React.Component<any> {
  public render() {
    return (
      <SpinnerContainer>
        <div className="spinner-overlay" />
        <ResponsiveImage className="spinner-modal" image={{xs: this.props.spinnerIcon}} />
        <p className="spinner-text" dangerouslySetInnerHTML={sanitize(this.props.spinnerText)} />
      </SpinnerContainer>
    );
  }
}

export default Spinner;
