import * as React from 'react';
import styled from 'styled-components';

export const StyledContainer = styled.div`
  @keyframes pulsateAnimation {
    0% {
      background-color: #1a1a1a;
    }
    25% {
      background-color: #333;
    }
    50% {
      background-color: #1a1a1a;
    }
    100% {
      background-color: #1a1a1a;
    }
  }
  .pulsate {
    animation-name: pulsateAnimation;
    animation-duration: 3.6s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
  }
`;

/*
PulsateContainer apply pulsate animation to any child with 'pulsate' calssName
Ex. 
  <PulsateContainer>
    <div className="a pulsate" />
    <div className="a pulsate" />
  </PulsateContainer>
  */
const PulsateContainer: React.FC<any> = props => <StyledContainer>{props.children}</StyledContainer>;

export default PulsateContainer;
