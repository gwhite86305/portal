import * as React from 'react';
import {pathOr} from 'ramda';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getAAJwt} from '../../store/Authentication/actions';
export type PageProps = DispatchProps;
var jwtDecode = require('jwt-decode');
export interface DispatchProps {
  onLoad(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => {
    let props = {...ownProps, loggedIn: false};
    const jwt = pathOr(undefined, ['Authentication', 'aaAuth', 'jwt'], state);
    if (jwt) {
      props.loggedIn = jwtDecode(jwt).expire > Math.floor(Date.now() / 1000);
    }
    return props;
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onLoad: () => {
      dispatch(getAAJwt());
    }
  })
)
export default class HideWhenLoggedIn extends React.Component<any, any> {
  public static defaultProps: any = {};
  constructor(props: any) {
    super(props);
    this.props.onLoad();
  }
  public render() {
    return this.props.loggedIn ? null : this.props.children;
  }
}
