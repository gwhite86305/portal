import * as React from 'react';
import NukaAdvertsCarousel from './NukaAdvertsCarousel';
import withWidth from '@material-ui/core/withWidth';
import {AdvertItem} from './../../store/adverts/types';
import {mapDispatchToProps, mapStoreToProps} from '../AdvertsCarousel/utils';
import {NukaAdvertsCarouselProps} from './types';
import {PortalComponent, utils} from 'component-utils';
import {path} from 'ramda';
const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
class AdvertsCarouselContainer extends React.Component<NukaAdvertsCarouselProps> {
  constructor(props: NukaAdvertsCarouselProps) {
    super(props);
    this.openAd = this.openAd.bind(this);
  }

  componentDidMount() {
    this.onUpdate(this.props);
  }

  render() {
    const {isLoading, isFetchCompleted, items, ...rest} = this.props;
    const isLoaded = !isLoading && isFetchCompleted && items.length;
    const filteredItems = this.props.feature.adCategory
      ? items.filter((ad: AdvertItem) => path(['categories', 0, 'categoryUid'], ad) === this.props.feature.adCategory)
      : items;
    const displayItems = this.props.feature.maxSlides
      ? filteredItems.slice(0, this.props.feature.maxSlides)
      : filteredItems;

    return (
      <React.Fragment>
        {isLoaded && (
          <PortalComponent componentTheme={this.props.componentTheme}>
            <NukaAdvertsCarousel
              onItemInViewPort={this.props.actions.trackImpression}
              onClick={this.openAd}
              items={displayItems}
              {...rest}
            />
          </PortalComponent>
        )}
      </React.Fragment>
    );
  }

  private shouldFetch = (nextProps: NukaAdvertsCarouselProps) => !nextProps.isFetchCompleted && !nextProps.isLoading;
  private onUpdate = (nextProps: NukaAdvertsCarouselProps) =>
    this.shouldFetch(nextProps) && this.props.actions.getAdverts();
  private openAd = (itemId: string) => {
    const adItem = this.props.items.find((ad: AdvertItem) => ad.uid === itemId);
    this.props.actions.trackView(adItem!);
    window.open(adItem!.targetUrl, '_blank');
  };
}

export default withWidth()(AdvertsCarouselContainer) as any;
