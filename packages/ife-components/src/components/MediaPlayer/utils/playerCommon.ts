import {BrowserInfo, detect as detectBrowser} from 'detect-browser';
import * as hasTouch from 'has-touch';
import {isNil, prop, propEq} from 'ramda';
import {empty, fromEvent, interval, merge, of, Subject, timer} from 'rxjs';
import {
  filter,
  first,
  map,
  mapTo,
  pluck,
  scan,
  startWith,
  switchMap,
  take,
  takeUntil,
  throttleTime,
  withLatestFrom
} from 'rxjs/operators';
import * as screenfull from 'screenfull';
import {AnalyticEventType} from '../../../store/analytics/actions';
import {AnalyticsErrorCode} from '../../../store/analytics/utils';
import {PlayerProps} from '../types';

export const BROWSER = detectBrowser() as BrowserInfo;
export const browserName = BROWSER && BROWSER.name.toLowerCase();
export const isIOS = browserName === 'ios' || (browserName === 'safari' && hasTouch);
export const isShaka = ['safari', 'ios'].indexOf(browserName) === -1;

export const attachVideoPlayerListeners = (
  playerInstance: any,
  props: PlayerProps,
  unsubscribe$: Subject<boolean>,
  videoRef: any,
  getCurrentProps: any,
  onTimeUpdate: (uid: string, currentTime: number, duration: number) => any,
  stopMovie$: Subject<boolean>
) => {
  // destroy old listerners if any;
  unsubscribe$.next(true);

  const isNotAd = !props.advertId;
  const playbackSessionId = props.playbackSessionId;

  const timeupdate$ = fromEvent(videoRef, 'timeupdate').pipe(
    takeUntil(unsubscribe$),
    map(({currentTarget: {duration, currentTime}}) => ({
      duration,
      currentTime
    })),
    filter(({currentTime}) => Math.round(currentTime) !== 0)
  );

  const ended$ = fromEvent(videoRef, 'ended').pipe(takeUntil(unsubscribe$));

  const pause$ = fromEvent(videoRef, 'pause').pipe(
    takeUntil(unsubscribe$),
    filter(({currentTarget: {duration, currentTime}}) => currentTime !== duration)
  );

  const play$ = fromEvent(videoRef, 'play').pipe(
    takeUntil(unsubscribe$),
    map(({currentTarget: {duration, currentTime}}) => ({duration, currentTime}))
  );

  const buffering$ = getBufferingStream(playerInstance, videoRef);

  const PLAYING_REPORT_INTERVAL = 30000;
  const TIMER_INTERVAL = 50;

  const isPaused$ = pause$.pipe(takeUntil(unsubscribe$), mapTo(false));

  const isNotBuffering$ = buffering$.pipe(takeUntil(unsubscribe$), map(({buffering}) => !buffering));

  const isPlaying$ = play$.pipe(takeUntil(unsubscribe$), mapTo(true));

  const isNotStoped$ = merge(isPaused$, isNotBuffering$);

  const interval$ = interval(TIMER_INTERVAL).pipe(takeUntil(unsubscribe$), mapTo(TIMER_INTERVAL));

  // timer observable counts playingTime.
  const playingTimer$ = merge(isPlaying$, isNotStoped$).pipe(
    takeUntil(unsubscribe$),
    filter(() => isNotAd),
    startWith(false),
    switchMap(isPlaying => (isPlaying ? interval$ : empty())),
    scan((acc, curr) => (curr ? curr + acc : acc), 0),
    withLatestFrom(timeupdate$),
    map(([playingTime, time]) => ({
      eventPlayback: {playingTime, ...time}
    }))
  );

  buffering$
    .pipe(
      pluck('buffering'),
      switchMap(buffering => (buffering ? timer(1000).pipe(take(1), mapTo(buffering)) : of(buffering))),
      takeUntil(unsubscribe$)
    )
    .subscribe(({buffering}) => {
      props.onBuffering(buffering);
    });

  // when we start playing the movie for the first time playingTimer$ didn't emits. we mock playingTime to 0
  const playFirstTimeAnalytics$ = play$.pipe(
    first(),
    map(({duration, currentTime}) => ({duration, currentTime, playingTime: 0}))
  );

  const stopAnalytics$ = stopMovie$.pipe(withLatestFrom(playingTimer$), map(([_, {eventPlayback}]) => eventPlayback));

  // play observable with timer value
  const playAnalytics$ = play$.pipe(withLatestFrom(playingTimer$), map(([_, {eventPlayback}]) => eventPlayback));

  // pause observable with timer value
  const pauseAnalytics$ = pause$.pipe(withLatestFrom(playingTimer$), map(([_, {eventPlayback}]) => eventPlayback));

  // ended observable with timer value
  const endedAnalytics$ = ended$.pipe(withLatestFrom(playingTimer$), map(([_, {eventPlayback}]) => eventPlayback));

  // update movie timeline
  timeupdate$.pipe(throttleTime(1000)).subscribe(({currentTime, duration}) => {
    // only update time when video ready
    const updateAllowed = videoRef.readyState === HTMLMediaElement.HAVE_ENOUGH_DATA;
    if (updateAllowed) {
      onTimeUpdate(getCurrentProps().advertId || props.videoId, currentTime, duration);
    }
  });

  // update current video time in persistent storage
  timeupdate$.pipe(throttleTime(30000)).subscribe(({currentTime}) => {
    if (isNotAd) {
      props.updateVideoConfig({uid: props.videoId, watchTime: Math.round(currentTime)});
    }
  });

  // report playing event
  playingTimer$.subscribe(({eventPlayback}) => {
    if (eventPlayback.playingTime % PLAYING_REPORT_INTERVAL === 0) {
      props.reportEvent(AnalyticEventType.PLAYING, {eventPlayback, playbackSessionId});
    }
  });

  // report ended event
  endedAnalytics$.subscribe(eventPlayback => {
    if (isNotAd) {
      props.reportEvent(AnalyticEventType.ENDED, {eventPlayback, playbackSessionId});
    }
  });

  // reset video time in persistent storage and turn off the video
  ended$.subscribe(() => {
    if (isNotAd) {
      // reset-watchtime to zero on end
      onTimeUpdate(getCurrentProps().advertId || props.videoId, 0, 0);
    }
    props.onVideoEnded();
  });

  pause$.subscribe(() => {
    props.onPause();
  });

  play$.subscribe(() => {
    props.onPlay();
  });

  stopAnalytics$.subscribe(eventPlayback => {
    if (isNotAd) {
      props.reportEvent(AnalyticEventType.STOP, {eventPlayback, playbackSessionId});
    }
  });

  // report pause event
  pauseAnalytics$.subscribe(eventPlayback => {
    if (isNotAd) {
      props.reportEvent(AnalyticEventType.PAUSE, {eventPlayback, playbackSessionId});
    }
  });

  // report play event
  merge(playAnalytics$, playFirstTimeAnalytics$).subscribe(eventPlayback => {
    if (isNotAd) {
      props.reportEvent(AnalyticEventType.PLAY, {
        eventPlayback,
        playbackSessionId
      });
    }
  });

  fromEvent(videoRef, 'loadeddata')
    .pipe(takeUntil(unsubscribe$))
    .subscribe(() => {
      const videoReady = videoRef.readyState === HTMLMediaElement.HAVE_ENOUGH_DATA;
      if (isShaka && isNotAd && !props.isTrailer && videoReady) {
        videoRef.currentTime = props.currentTime || 0;
      }
    });

  fromEvent(videoRef, 'playing')
    .pipe(takeUntil(unsubscribe$))
    .subscribe(() => props.onPlaying());

  if (playerInstance) {
    fromEvent(playerInstance, 'texttrackvisibility')
      .pipe(takeUntil(unsubscribe$))
      .subscribe(() => {
        if (typeof getCurrentProps === 'function') {
          const collection = playerInstance.getTextTracks();
          const selectedTrack = collection.find(propEq('language', getCurrentProps().subtitleLanguage));
          if (selectedTrack) {
            playerInstance.selectTextTrack(selectedTrack);
          }
        }
      });
  }

  getErrorStream(playerInstance, videoRef)
    .pipe(takeUntil(unsubscribe$))
    .subscribe(error => {
      const message = (error.detail && error.detail.toString()) || '';
      props.reportEvent(AnalyticEventType.FAILED, {
        error: {code: AnalyticsErrorCode.player, message},
        playbackSessionId
      });
    });

  getFullScreenStream()
    .pipe(takeUntil(unsubscribe$))
    .subscribe(props.setFullscreen);

  return true;
};

export const seekToRange = (props: PlayerProps, videoRef: HTMLVideoElement) => {
  if (!videoRef || isNaN(videoRef.currentTime) || isNaN(videoRef.duration)) {
    return;
  }
  const target = props.seekToRange * videoRef.duration / 100;
  if (!props.advertId) {
    props.updateVideoConfig({uid: props.videoId, watchTime: Math.round(target)});
  }
  videoRef.currentTime = target;
};

export const handleVolumeChange = (volume: number = 0, videoRef: HTMLVideoElement) => {
  if (!videoRef) {
    return;
  }
  videoRef.volume = volume;
};

export const handleAudioLanguageChange = (
  lang: string | undefined,
  videoRef: HTMLVideoElement,
  playerInstance?: any
) => {
  if (isNil(lang)) {
    return;
  }
  if (isShaka) {
    return playerInstance.selectAudioLanguage(lang);
  }
  if (!videoRef.audioTracks.length) {
    return;
  }
  const audioTrackIndex = Array(videoRef.audioTracks.length)
    .fill(0)
    .map((_, index) => videoRef.audioTracks[index])
    .map(prop('language'))
    .indexOf(lang);

  if (!videoRef.audioTracks[audioTrackIndex]) {
    return;
  }
  videoRef.audioTracks[audioTrackIndex].enabled = true;
};

export const handleSubtitleChange = (lang: string | undefined, videoRef: HTMLVideoElement, playerInstance?: any) => {
  if (lang && lang.toLowerCase() == 'off') {
    setSubtitleOff(videoRef, playerInstance);
    return;
  }
  if (lang && isShaka) {
    playerInstance.setTextTrackVisibility(true);
    return;
  }
  const textTrackIndex = Array(videoRef.textTracks.length)
    .fill(0)
    .map((_, index) => videoRef.textTracks[index])
    .map(prop('language'))
    .indexOf(lang as string);

  const textTracksArr = Array(videoRef.textTracks.length)
    .fill(0)
    .map((_, index) => videoRef.textTracks[index]);

  for (let i = 0; i < textTracksArr.length; i++) {
    videoRef.textTracks[i].mode = i == textTrackIndex ? 'showing' : 'hidden';
  }
  videoRef.textTracks[textTrackIndex].mode = 'showing';
};

export const isNonDRM = (fileManifest: string) => fileManifest && fileManifest.endsWith('.mp4');

export const loadNonDRMContent = (videoRef: HTMLVideoElement, fileManifest: string) => {
  videoRef.src = fileManifest;
  videoRef.load();
};

export const getTimeString = (time: number): string =>
  new Date(1000 * (isNaN(time) ? 0 : time)).toISOString().substr(11, 8);

// -- PRIVATE
const getBufferingStream = (playerInstance: any, videoRef: HTMLVideoElement) =>
  playerInstance
    ? fromEvent(playerInstance, 'buffering')
    : merge(
        fromEvent(videoRef, 'waiting').pipe(map(_ => ({buffering: true}))),
        fromEvent(videoRef, 'seeking').pipe(map(_ => ({buffering: true}))),
        fromEvent(videoRef, 'playing').pipe(map(_ => ({buffering: false}))),
        fromEvent(videoRef, 'seeked').pipe(map(_ => ({buffering: false}))),
        fromEvent(videoRef, 'canplay').pipe(map(_ => ({buffering: false})))
      );

const getErrorStream = (playerInstance: HTMLVideoElement, videoRef: HTMLVideoElement) =>
  playerInstance
    ? merge(fromEvent<CustomEvent>(playerInstance, 'error'), fromEvent<CustomEvent>(videoRef, 'error'))
    : fromEvent<CustomEvent>(videoRef, 'error');

const getFullScreenStream = () => {
  if (!isIOS && (!screenfull || !screenfull.enabled)) {
    return empty();
  }
  const domTarget = document.querySelector(isIOS ? 'video' : 'body') as Element;
  return isIOS
    ? fromEvent(domTarget, 'webkitendfullscreen').pipe(map(_ => false))
    : merge(
        fromEvent(domTarget, 'fullscreenchange'),
        fromEvent(domTarget, 'webkitfullscreenchange'),
        fromEvent(domTarget, 'mozfullscreenchange'),
        fromEvent(domTarget, 'MSFullscreenChange')
      ).pipe(map(_ => (screenfull as any).isFullscreen));
};

const setSubtitleOff = (videoRef: any, playerInstance: any) => {
  if (isShaka) {
    playerInstance.setTextTrackVisibility(false);
  }
  Array(videoRef.textTracks.length)
    .fill(0)
    .forEach((_, index) => (videoRef.textTracks[index].mode = 'hidden'));
};
