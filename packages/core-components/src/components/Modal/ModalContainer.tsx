import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import { Modal as ExternalInterface } from '../common/ComponentsInterface';
import Modal, { InternalInterface } from './Modal';
import { ShowModal, HideModal } from '../../store/Modal/actions';
export type PageProps = ExternalInterface & InternalInterface;
export interface DispatchProps {
  onModalClose(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => {
    return {
      modal: state.modal
    }
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onModalClose: () => {
      dispatch(HideModal());
    }
  })
)
class ModalContainer extends React.Component<PageProps, any> {
  public componentWillUnmount() {
    // Clear the state so this modal doesn't show up when the user revisit that page.
    if(this.props.feature.dynamic) {
      this.props.onModalClose();
    }
  }
  public render() {
    return <Modal 
      {...this.props}
     />;
  }
}

export default ModalContainer;
