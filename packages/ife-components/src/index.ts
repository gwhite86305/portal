import * as React from 'react';
import { Store } from 'redux';
import { combineEpics, EpicMiddleware } from 'redux-observable';
import AdvertsReducer, { IAdvertsState } from './store/adverts/reducer';
import audioReducer, { IAudioState } from './store/audio/reducer';
import { htmlBundleEpics } from './store/htmlBundle/epics';
import { htmlBundleReducer, IHtmlBundleState } from './store/htmlBundle/reducer';
import mediaPlayerReducer from './store/mediaplayer/reducer';
import { MediaPlayerState } from './store/mediaplayer/types';
import moviesReducer from './store/movies/reducer';
import { IMoviesState } from './store/movies/types';
import movingMapReducer from './store/movingmap/reducer';
import { IMovingMapState } from './store/movingmap/types';
import notificationDispatcherReducer from './store/notificationDispatcher/reducer';
import { INotificationDispatcher } from './store/notificationDispatcher/types';
import settingsReducer from './store/settings/reducer';
import { ISettingsState } from './store/settings/types';
import videosReducer from './store/videos/reducer';
import { IVideosState } from './store/videos/types';
import './utils/mobileSdkCommunication';

// IoC / Decorator
const init = (render: React.ReactElement<any>, store: Store<any>, layout: any, epicMiddleware: EpicMiddleware<any>) => {
  (store as any).attachReducers({
    ifeHtmlBundle: htmlBundleReducer,
    ifeAudio: audioReducer,
    ifeAdverts: AdvertsReducer,
    ifeMediaPlayer: mediaPlayerReducer,
    ifeVideos: videosReducer,
    ifeMovies: moviesReducer,
    ifeMovingMap: movingMapReducer,
    ifeSettings: settingsReducer,
    ifeNotificationDispatcher: notificationDispatcherReducer
  });
  epicMiddleware.run(rootEpic)
  return render;
};

export interface IRootState {
  ifeHtmlBundle: IHtmlBundleState;
  ifeAudio: IAudioState;
  ifeAdverts: IAdvertsState;
  ifeMediaPlayer: MediaPlayerState;
  ifeSettings: ISettingsState;
  ifeVideos: IVideosState;
  ifeMovies: IMoviesState;
  ifeMovingMap: IMovingMapState;
  ifeNotificationDispatcher: INotificationDispatcher;
}

export const rootEpic = combineEpics(htmlBundleEpics);

export { init };
export default init;
