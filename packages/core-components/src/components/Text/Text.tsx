import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Text as ExternalInterface} from '../common/ComponentsInterface';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
}

const TextWrapper = styled.div`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('color', 'color')};
  ${renderThemeProperty('fontFamily', 'font-family')};
  ${renderThemeProperty('fontSize', 'font-size')};
  ${renderThemeProperty('letterSpacing', 'letter-spacing')};
  ${renderThemeProperty('zIndex', 'z-index')};
  ${renderThemeProperty('fontWeight', 'font-weight')};
  ${renderThemeProperty('wordWrap', 'word-wrap')};
  ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.align, props.theme, true)};
  ${renderThemeProperty('shadow', 'text-shadow')};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('line-height', props.theme.componentTheme.lineHeight, props.theme)};
  max-width: 100%;
`;

class Text extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme, localizedText} = this.props as any;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <TextWrapper className="core-text-component">{localizedText}</TextWrapper>
      </PortalComponent>
    );
  }
}

export default withLocaleProps(Text, [{localizedText: 'core.Text.defaultText'}]) as any;
