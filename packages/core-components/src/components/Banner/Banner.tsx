import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {Banner as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import Icon from '../Icon/Icon';
import NavButton from '../common/NavButton';
import Button from '../Button/Button';
import styled from 'styled-components';

const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper, renderChildComponent} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) =>
    applyMediaQueriesForStyle('background-image', props.theme.componentTheme.backgroundImage, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};

  position: relative;
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  flex-basis: 100%;
  flex-direction: column;
  font-family: ${(props: any) => props.theme.appFontFamily};

  a {
    text-decoration: none;
  }

  .banner-textregion {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;

    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-left',
        {
          xs: `calc(${props.theme.gutter} * 1.25)`,
          sm: `calc(${props.theme.gutter} * 2.5)`
        },
        props.theme
      )};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-right',
        {
          xs: `calc(${props.theme.gutter} * 1.25)`,
          sm: `calc(${props.theme.gutter} * 2.5)`
        },
        props.theme
      )};
  }

  .subheader-arrow {
    padding-left: calc(${(props: any) => props.theme.gutter} * 3.0625);
    padding-top: calc(${(props: any) => props.theme.gutter} * 0.375);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.375);
  }

  .core-text-component {
    display: inline-block;
  }

  .subHeader-text {
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    font-size: ${(props: any) => props.theme.componentTheme.subheadertextFontSize};
    font-family: ${(props: any) => props.theme.appFontFamily};
    ${(props: any) =>
      applyMediaQueriesForStyle('text-align', props.theme.componentTheme.subheadertextAlign, props.theme)};
    z-index: 1;
    cursor: pointer;
    word-wrap: break-word;
    ${renderThemeProperty('subHeaderTextColor', 'color')};
    a {
      ${renderThemeProperty('subHeaderTextColor', 'color')};
    }
  }
  span {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;

    /* Adds a hyphen where the word breaks, if supported (No Blink) */
    -ms-hyphens: auto;
    -moz-hyphens: auto;
    -webkit-hyphens: auto;
    hyphens: none;
  }
  .button-wrap {
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1;
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    a {
      ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.buttonMargin, props.theme)};
    }
  }
  button {
    font-weight: ${(props: any) => (props.theme.buttonFontWeight ? props.theme.buttonFontWeight : 'normal')};
    width: ${(props: any) => (props.theme.buttonWidth ? props.theme.buttonWidth : '200px')};
    height: ${(props: any) => (props.theme.buttonHeight ? props.theme.buttonHeight : '45px')};
    border: ${(props: any) => (props.theme.buttonBorder ? props.theme.buttonBorder : '0')};
    font-family: ${(props: any) =>
      props.theme.signupButtonFontFamily ? props.theme.signupButtonFontFamily : props.theme.appFontFamily};
    font-size: ${(props: any) => (props.theme.signupButtonFontSize ? props.theme.signupButtonFontSize : '16px')};
    ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.buttonMargin, props.theme)};

    &:focus,
    &:hover {
      border: none !important;
    }
  }
`;

const SubHeaderText = (props: any) => {
  const {subHeaderText, actionLink, displayArrowIcon} = props;
  return (
    <div className="subHeader-text">
      {actionLink == '#' || actionLink.trim() == '' ? (
        <div>
          <span>{subHeaderText}</span>
          {displayArrowIcon ? <Icon className="subheader-arrow" feature={{name: 'fa-angle-right'}} /> : null}
        </div>
      ) : (
        <a href={actionLink} target="_blank" title="(opens in new tab/window)">
          <span>{subHeaderText}</span>
          {displayArrowIcon ? <Icon className="subheader-arrow" feature={{name: 'fa-angle-right'}} /> : null}
        </a>
      )}
    </div>
  );
};

class Banner extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  componentWillUnmount() {
    // Scroll to top when object is unmounted.
    window.scrollTo(0, 0);
  }

  public render() {
    let button;
    const image = this.props.feature.image;
    const {altTextForImage, subHeaderText, headerText, buttonText, feature, componentTheme, children} = this.props;

    if (this.props.buttonClickHook || feature.openPageOnClick || feature.externalUrl) {
      let button_feature = {
        externalUrl: feature.externalUrl,
        openPageOnClick: feature.openPageOnClick
      };
      button = (
        <Button isPrimary={true} onClick={this.props.buttonClickHook} feature={button_feature} aria-label={buttonText}>
          {buttonText}
        </Button>
      );
    } else if (feature.includeButton) {
      button = <NavButton isPrimary={true} to={feature.openPageOnClick} buttonText={buttonText} />;
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          <ResponsiveImage
            altText={altTextForImage}
            image={image}
            imageMargin={componentTheme.imageMargin}
            imageWidth={componentTheme.imageWidth}
          />

          <div className="banner-textregion">
            {headerText ? renderChildComponent(0, children, {localizedText: headerText}) : null}

            {subHeaderText ? (
              <SubHeaderText
                subHeaderText={subHeaderText}
                actionLink={feature.actionLink}
                displayArrowIcon={feature.displayArrowIcon}
              />
            ) : null}
            {feature.includeButton ? <div className="button-wrap">{button}</div> : null}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default Banner;
