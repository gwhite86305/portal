import * as React from 'react';
import NavLink from './NavLink';
import {utils} from 'component-utils';
import styled from 'styled-components';

const StyledNavLink: any = styled(NavLink)`
  &.button {
    cursor: pointer;
    font-size: 1.1rem;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
    border: ${(props: any) => (props.theme.buttonBorder ? props.theme.buttonBorder : '0')};
    font-family: inherit;
    border-radius: ${(props: any) => props.theme.buttonRadius};
    text-decoration: none;
    color: yellow;
    &:active {
      outline: 0;
      box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
    }

    &.primary-button,
    &.primary-button i {
      background: ${(props: any) => props.theme.primaryButtonColor};
      color: ${(props: any) => props.theme.primaryButtonTextColor};
    }

    &.primary-button {
      &:hover,
      &:focus {
        background: ${(props: any) => props.theme.primaryButtonHoverColor};
        color: ${(props: any) => props.theme.primaryButtonHoverTextColor};
        border: none;
        i {
          background: ${(props: any) => props.theme.primaryButtonHoverColor};
          color: ${(props: any) => props.theme.primaryButtonHoverTextColor};
        }
      }
    }

    &.secondary-button,
    &.secondary-button i {
      background: ${(props: any) => props.theme.secondaryButtonColor};
      color: ${(props: any) => props.theme.secondaryButtonTextColor};
    }

    &.secondary-button {
      &:hover,
      &:focus {
        background: ${(props: any) => props.theme.secondaryButtonHoverColor};
        color: ${(props: any) => props.theme.secondaryButtonHoverTextColor};

        i {
          background: ${(props: any) => props.theme.secondaryButtonHoverColor};
          color: ${(props: any) => props.theme.secondaryButtonHoverTextColor};
        }
      }
    }

    &.disabled {
      cursor: not-allowed;
    }
  }
  width: 200px;
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.625);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.625);
`;

const NavButton: React.FC<any> = props => {
  const {isPrimary, isDisabled, to, buttonText, className, ...rest} = props;
  return (
    <StyledNavLink
      className={
        'button ' +
        (isDisabled ? 'disabled ' : '') +
        (isPrimary ? 'primary-button ' : 'secondary-button ') +
        (className ? ' ' + className : '')
      }
      to={to}
    >
      {buttonText}
    </StyledNavLink>
  );
};

export default NavButton;
