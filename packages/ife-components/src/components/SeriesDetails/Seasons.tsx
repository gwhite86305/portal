import {
  Divider,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  ListItem,
  Button
} from '@material-ui/core';
import List from '@material-ui/core/List';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import RemoveIcon from '@material-ui/icons/Remove';
import * as React from 'react';
import {VideoItem} from '../../store/videos/types';
import {getImageSrcByRatio} from '../common/utils';
import PlayButton from './PlayButton';
import {SeasonsProps, SeasonsState} from './types';
import {ImageVariantsType} from '../types';

export default class Seasons extends React.PureComponent<SeasonsProps, SeasonsState> {
  constructor(props: any) {
    super(props);
    this.state = {expanded: ''};
  }

  private handleChange = (panel: string) => (_: any, expanded: boolean) =>
    this.setState({expanded: expanded ? panel : ''});

  private displayEpisodeSubtitle = (episode: VideoItem) => `${episode.episode ? ' - ' : ''}${episode.subtitle}`;

  componentDidMount() {
    this.setState({expanded: Object.keys(this.props.seasons)[0]});
  }

  render() {
    const {seasons} = this.props;
    const {expanded} = this.state;
    return (
      <React.Fragment>
        {Object.keys(seasons).map((seasonNumber, index) => (
          <ExpansionPanel
            className="season"
            expanded={expanded === seasonNumber}
            key={index}
            onChange={this.handleChange(seasonNumber).bind(this)}
          >
            <ExpansionPanelSummary className="season-number" expandIcon={<ExpandMoreIcon />}>
              {seasonNumber && seasonNumber !== 'undefined' ? `Season ${seasonNumber}` : 'Available Episodes'}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className="season-details">
              <List dense={true}>
                {seasons[seasonNumber].map((episode: VideoItem, key: number) => (
                  <ListItem className="episode" key={key}>
                    <div className="title-row-wrapper">
                      <Button
                        aria-label="Play episode"
                        className="img__name__cta"
                        onClick={() => this.props.playItem(episode)}
                      >
                        {episode.galleryImages && (
                          <div className="img-container">
                            <div
                              className={'series-image'}
                              style={{
                                backgroundImage: `url(${getImageSrcByRatio(
                                  episode.galleryImages[0],
                                  ImageVariantsType.videothumb,
                                  window.devicePixelRatio
                                )})`
                              }}
                            />
                          </div>
                        )}
                        <PlayButton />
                      </Button>
                      <div className="episode-details-container">
                        <div className="title-duration-wrapper">
                          <div className="title">
                            {`${episode.episode || ''} ${episode.subtitle ? this.displayEpisodeSubtitle(episode) : ''}`}
                          </div>
                          {episode.durationMinutes ? (
                            <div className="duration">Duration: {episode.durationMinutes} mins</div>
                          ) : null}
                        </div>
                        {this.props.playlistIdsMap[episode.uid] ? (
                          <Button
                            aria-label="Remove from playlist"
                            variant="contained"
                            className="playlist-action"
                            onClick={() => this.props.removeItemFromPlaylist(episode.uid)}
                          >
                            Remove from playlist
                          </Button>
                        ) : (
                          <Button
                            aria-label="Add to playlist"
                            variant="contained"
                            className="playlist-action"
                            onClick={() => this.props.addItemsToPlaylist([episode])}
                          >
                            Add to playlist
                          </Button>
                        )}
                      </div>
                    </div>
                    <div className="description">{episode.description}</div>
                    {key !== seasons[seasonNumber].length - 1 ? <Divider style={{margin: '1.2rem 0'}} /> : null}
                  </ListItem>
                ))}
              </List>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </React.Fragment>
    );
  }
}
