import {Price} from '../../components/common/PaidContent/types';
import {ImageVariantsType, ImageVariants} from '../../components/types';
import {Normalized, NormalizableObject} from '../../utils/Norm';

export enum CATEGORIES {
  MOVIES = 'movies',
  SERIES = 'tv'
}

export enum TYPES {
  VIDEO_GROUP = 'videoGroup',
  VIDEO_ITEM = 'videoItem'
}

export interface VideoGroup {
  parentId?: string;
  attributes: any;
  categories: string[];
  galleryImages?: ImageVariants[];
  genres: string[];
  items?: VideoItem[];
  title: string;
  titleImages?: ImageVariants[];
  type: TYPES.VIDEO_GROUP;
  label?: string;
  uid: string;
}

export interface VideoItem {
  parentId?: string;
  ageRating: string;
  ageRatingAdvisory: any;
  aspectRatio: string;
  attributes: string;
  audioTracks: any[];
  cast: string;
  categories: string[];
  description: string;
  director: string;
  durationMinutes: number;
  episode: number;
  galleryImages?: ImageVariants[];
  genres: string[];
  prerollAdGroups: any[];
  releaseDate: string;
  reviewRating: number;
  season: number;
  soundtrack: string;
  subtitle: string;
  textTracks: any[];
  title: string;
  titleImages?: ImageVariants[];
  trailerVideo?: {url: string};
  type: TYPES.VIDEO_ITEM;
  uid: string;
  group?: VideoGroup | null;
  price?: Price;
}

export interface PrerollAd {
  blockFastForward: boolean;
  skippableAfter?: number;
  title: string;
  uid: string;
  video: {url: string};
}

export interface LocalizedCategory {
  uid: string;
  title: string;
  category?: string;
}

export interface VideoCategory {
  uid: string;
  category: string;
  items: VideoGroup[] | VideoItem[];
}

export interface VideoConfig {
  watchTime: number;
  subtitleLanguage: string;
  audioLangauge: string;
}

export type VideosConfig = Normalized<string, Partial<VideoConfig>>;
export type VideosConfigItem = NormalizableObject;

export interface IVideosState {
  readonly error: any;
  readonly isLoadingAds: boolean;
  readonly isLoadingSeries: boolean;
  readonly isLoadingSynopsis: boolean;
  readonly items: (VideoGroup | VideoItem)[];
  readonly selectedItem?: VideoGroup | VideoItem;
  readonly preRollAds: {[videoId: string]: PrerollAd[]};
  readonly seriesFetchCompleted: boolean;
  readonly videoNotFound: boolean;
  readonly seriesCategories: LocalizedCategory[];
  readonly videosConfig: VideosConfig;
}
