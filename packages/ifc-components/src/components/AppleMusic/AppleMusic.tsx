import * as defaults from './defaultProps.json';
import * as React from 'react';
import {AppleMusic as ExternalInterface} from '../common/ComponentsInterface';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import NavLink from 'core-components/src/components/common/NavLink';
import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  textOverride?: string;
  className?: string;
}

const AppleMusicWrapper = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('fontFamily', 'font-family')};
  ${renderThemeProperty('fontSize', 'font-size')};
  ${renderThemeProperty('letterSpacing', 'letter-spacing')};
  ${renderThemeProperty('zIndex', 'z-index')};
  ${renderThemeProperty('fontWeight', 'font-weight')};
  ${renderThemeProperty('wordWrap', 'word-wrap')};
  ${renderThemeProperty('shadow', 'text-shadow')};

  ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.align, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};

  a {
    ${renderThemeProperty('textDecoration', 'text-decoration')};
    ${renderThemeProperty('color', 'color')};
  }

  .active-class {
    ${renderThemeProperty('activeColor', 'color')};
  }
`;

class AppleMusic extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme, localizedText, children, intl} = this.props;
    const currentLocale = intl.locale;

    const applemusic = this.props.feature.isMediaAsset
      ? `${this.props.feature.openPageOnClick}_${currentLocale}.pdf`
      : this.props.feature.openPageOnClick;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <AppleMusicWrapper className="ifc-component">
          <NavLink
            activeClassName="active-class"
            to={`/${this.props.feature.externalUrl || applemusic}`}
            openInNewTab={this.props.feature.openInNewTab}
            isMediaAsset={this.props.feature.isMediaAsset}
            dangerouslySetInnerHTML={sanitize(localizedText)}
            child={children && (children as any).length ? children : null}
          />
        </AppleMusicWrapper>
      </PortalComponent>
    );
  }
}

export default withLocaleProps(AppleMusic, [{localizedText: 'ifc.AppleMusic.defaultText'}]) as any;
