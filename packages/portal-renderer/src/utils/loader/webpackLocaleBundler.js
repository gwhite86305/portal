var loaderUtils = require('loader-utils');

module.exports = function () {
};

module.exports.pitch = function (remainingRequest) {

  if (typeof this.cacheable === 'function') {
    this.cacheable()
  }

  var query = loaderUtils.parseQuery(this.query);
  var locale = query.locale;

  return [
    'var addLocaleData = require("react-intl").addLocaleData;',
    'var isIntlLocaleSupported = require("intl-locales-supported");',
    'module.exports = function (cb) {',
    '  if (isIntlLocaleSupported("' + locale + '")) {',
    '    require.ensure([',
    '      "react-intl/locale-data/' + locale + '",',
    '    ], function (require) {',
    '      addLocaleData(require("react-intl/locale-data/' + locale + '"));',
    '      cb();',
    '    }, "locale-data-' + locale + '-no-intl");',
    '  } else {',
    '    require.ensure([',
    '      "intl/locale-data/jsonp/' + locale + '",',
    '      "react-intl/locale-data/' + locale + '",',
    '    ], function (require) {',
    '      require("intl/locale-data/jsonp/' + locale + '");',
    '      addLocaleData(require("react-intl/locale-data/' + locale + '"));',
    '      cb();',
    '    }, "locale-data-' + locale + '");',
    '  }',
    '};'
  ].join('\n')
};

// var loaderUtils = require('loader-utils');
//
// module.exports = function () {
// };
//
// module.exports.pitch = function (remainingRequest) {
//
//   if (typeof this.cacheable === 'function') {
//     this.cacheable()
//   }
//
//   var callback = this.async();
//   var query = loaderUtils.parseQuery(this.query);
//   var locale = query.locale;
//
//   return [
//     'var addLocaleData = require("react-intl").addLocaleData;',
//     'var isIntlLocaleSupported = require("intl-locales-supported");',
//     'module.exports = function (cb) {',
//     '  if (isIntlLocaleSupported("' + locale + '")) {',
//     '    require.ensure([',
//     '      "react-intl/locale-data/' + locale + '",',
//     '    ], function (require) {',
//     '      addLocaleData(require("react-intl/locale-data/' + locale + '"));',
//     '      cb();',
//     '    }, "locale-data-' + locale + '-no-intl");',
//     '  } else {',
//     '    require.ensure([',
//     '      "intl/locale-data/jsonp/' + locale + '",',
//     '      "react-intl/locale-data/' + locale + '",',
//     '    ], function (require) {',
//     '      require("intl/locale-data/jsonp/' + locale + '");',
//     '      addLocaleData(require("react-intl/locale-data/' + locale + '"));',
//     '      cb();',
//     '    }, "locale-data-' + locale + '");',
//     '  }',
//     '};'
//   ].join('\n')
// };
