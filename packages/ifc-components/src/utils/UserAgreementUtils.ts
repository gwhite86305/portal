import * as md5 from 'md5';

export const getUserAgreementHash = (props: any, state: any): string => {
  // const userAgreement = {...props.content};
  const userAgreement = {}; //Temporary workaround - now user agreement will be re-required on new origin+destination+date combo
  let locations = state.profile && state.profile.locations;
  (userAgreement as any)['Origin'] = locations && locations.originCode;
  (userAgreement as any)['Destination'] = locations && locations.destinationCode;
  (userAgreement as any)['Date'] = new Date().setHours(0, 0, 0, 0);
  const uaString = JSON.stringify(userAgreement);
  return md5(uaString);
};
