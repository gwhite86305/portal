import * as React from 'react';
import {utils} from 'component-utils';
import {Checkout as ExternalInterface} from '../common/ComponentsInterface';
import Checkout, {InternalInterface} from './Checkout';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {
  signupWithVoucher,
  validateVoucher,
  signupWithPurchase,
  resetVoucher,
  resetSignup,
  signupWithMessaging
} from '../../store/WifiServiceOptions/actions';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import {getPremiumTiers, getPremiumState, getMessagingTier} from '../../utils/ConnectionUtils';
const connectWrapper = utils.connectWrapper.default;

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onVoucherSubmit(voucherCode: string, email: string): any;
  signupWithVoucher(voucherCode: string, email: string, tierId: number, promoType: string): any;
  onPurchaseSubmit(email: string, tierId: number): any;
  resetVoucherForm(): any;
  resetPurchaseForm(): any;
  signupWithMessaging(tierId: number): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const premiumTiers = getPremiumTiers(state.profile);
    const messagingTiers = getMessagingTier(state.profile);
    const connected = getPremiumState(state.device, state.profile);
    const WifiServiceOptions = state.WifiServiceOptions;
    const currencyCode = state.profile.currencyCode || state.ux.settings.ifc.defaultCurrencyCode;
    const currencySuffix = state.ux.settings.ifc.currencySuffix;
    const locale = state.device.language;
    const minimumFractionDigits = state.ux.settings.ifc.defaultMinimumFractionDigits;
    const maximumFractionDigits = state.ux.settings.ifc.defaultMaximumFractionDigits;
    return {
      premiumTiers,
      connected,
      WifiServiceOptions,
      currencyCode,
      locale,
      minimumFractionDigits,
      maximumFractionDigits,
      messagingTiers,
      currencySuffix
    };
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onVoucherSubmit: (voucherCode, email) => {
        dispatch(validateVoucher(voucherCode, email, 'voucher'));
      },
      signupWithVoucher: (voucherCode, email, tierId) => {
        try {
          localStorage.setItem('purchase-email', email);
        } catch (exception) {}
        dispatch(signupWithVoucher(voucherCode, email, tierId, 'voucher'));
      },
      onPurchaseSubmit: (email, tierId) => {
        try {
          localStorage.setItem('purchase-email', email);
        } catch (exception) {}
        dispatch(signupWithPurchase(email, tierId));
      },
      resetVoucherForm: () => {
        dispatch(resetVoucher());
        dispatch(resetSignup());
      },
      resetPurchaseForm: () => {
        dispatch(resetSignup());
      },
      signupWithMessaging: tierId => {
        dispatch(signupWithMessaging(tierId));
      }
    };
  }
)
class CheckoutContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
    // To avoid multiple signup request
    this.state = {
      signupRequestMade: false
    };
  }

  public componentWillMount() {
    if (!this.props.WifiServiceOptions.selected) {
      this.props.history.push('/');
    }
  }
  public componentWillUnmount() {
    if (
      this.props.WifiServiceOptions &&
      this.props.WifiServiceOptions.selected &&
      this.props.WifiServiceOptions.selected.method == 'voucher'
    ) {
      this.props.resetVoucherForm();
    } else {
      this.props.resetPurchaseForm();
    }
  }
  componentWillReceiveProps(newProps: any) {
    const {WifiServiceOptions} = this.props;

    if (
      WifiServiceOptions &&
      WifiServiceOptions.selected &&
      WifiServiceOptions.selected.method == 'voucher' &&
      WifiServiceOptions.voucher &&
      WifiServiceOptions.voucher.valid &&
      !this.state.signupRequestMade
    ) {
      let tierId =
        WifiServiceOptions.voucher &&
        WifiServiceOptions.voucher.voucher &&
        WifiServiceOptions.voucher.voucher.serviceTierId;
      if (!tierId) {
        let premiumTiersOrderByDuration =
          (this.props.premiumTiers &&
            this.props.premiumTiers.sort((tier1: any, tier2: any) => tier2.duration - tier1.duration)) ||
          [];
        tierId = premiumTiersOrderByDuration[0].id;
      }
      this.setState({signupRequestMade: true});
      this.props.signupWithVoucher(
        this.props.WifiServiceOptions.voucher.promoCode,
        this.props.WifiServiceOptions.voucher.email,
        tierId,
        'voucher'
      );
    }
    if (
      WifiServiceOptions &&
      WifiServiceOptions.selected &&
      WifiServiceOptions.selected.method === 'messaging' &&
      WifiServiceOptions.signup &&
      WifiServiceOptions.signup.action === 'accept'
    ) {
      this.props.history.push('/' + this.props.feature.openPageWhenConnected);
    }
    if ((!this.props.connected && newProps.connected) || (this.state.signupRequestMade && newProps.connected)) {
      this.props.history.push('/' + this.props.feature.openPageWhenConnected);
    }
    if (
      WifiServiceOptions &&
      WifiServiceOptions.selected &&
      WifiServiceOptions.selected.method == 'purchase' &&
      WifiServiceOptions.signup &&
      WifiServiceOptions.signup.action == 'redirect'
    ) {
      window.location.href = WifiServiceOptions.signup.url;
    }
  }

  public render() {
    const {intl, componentUid} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {voucherHeader: 'ifc.Checkout.voucherHeader'},
      {voucherBodyText: 'ifc.Checkout.voucherBodyText'},
      {voucherInputLabel: 'ifc.Checkout.voucherInputLabel'},
      {voucherInputErrorHelpText: 'ifc.Checkout.voucherInputErrorHelpText'},
      {voucherEmailLabel: 'ifc.Checkout.voucherEmailLabel'},
      {voucherEmailErrorHelpText: 'ifc.Checkout.voucherEmailErrorHelpText'},
      {voucherNoteText: 'ifc.Checkout.voucherNoteText'},
      {voucherSubmitButtonText: 'ifc.Checkout.voucherSubmitButtonText'},

      {purchaseBodyText: 'ifc.Checkout.purchaseBodyText'},
      {purchaseEmailLabel: 'ifc.Checkout.purchaseEmailLabel'},
      {purchaseEmailErrorHelpText: 'ifc.Checkout.purchaseEmailErrorHelpText'},
      {purchaseNoteText: 'ifc.Checkout.purchaseNoteText'},
      {purchaseAltTextForNoteImage: 'ifc.Checkout.purchaseAltTextForNoteImage'},
      {purchaseTotalLabel: 'ifc.Checkout.purchaseTotalLabel'},
      {purchaseSubmitButtonText: 'ifc.Checkout.purchaseSubmitButtonText'},

      {emailConfirmationLabel: 'ifc.Checkout.emailConfirmationLabel'},

      {errorMessageOnFormHeader: 'ifc.Checkout.errorMessageOnFormHeader'},
      {errorMessageInvalidEmail: 'ifc.Checkout.errorMessageInvalidEmail'},
      {errorMessageEmailMismatch: 'ifc.Checkout.errorMessageEmailMismatch'},
      {errorMessageAccountAlreadyActive: 'ifc.Checkout.errorMessageAccountAlreadyActive'},
      {errorMessageInvalidVoucherCode: 'ifc.Checkout.errorMessageInvalidVoucherCode'},
      {errorMessageExpiredVoucherCode: 'ifc.Checkout.errorMessageExpiredVoucherCode'},
      {errorMessageConsumedVoucherCode: 'ifc.Checkout.errorMessageConsumedVoucherCode'},
      {errorMessageGeneric: 'ifc.Checkout.errorMessageGeneric'},
      {errorMessageVoucherCodeRequired: 'ifc.Checkout.errorMessageVoucherCodeRequired'}
    ]);
    let errorDict = {
      10007: localizedProps.errorMessageAccountAlreadyActive,
      20001: localizedProps.errorMessageInvalidVoucherCode,
      20002: localizedProps.errorMessageConsumedVoucherCode,
      20003: localizedProps.errorMessageInvalidVoucherCode,
      20004: localizedProps.errorMessageExpiredVoucherCode,
      20005: localizedProps.errorMessageInvalidVoucherCode,
      errorHeaderText: localizedProps.errorMessageOnFormHeader,
      emailRequired: localizedProps.errorMessageInvalidEmail,
      emailMismatch: localizedProps.errorMessageEmailMismatch,
      voucherRequired: localizedProps.errorMessageVoucherCodeRequired,
      generic: localizedProps.errorMessageGeneric
    };
    return <Checkout {...this.props} errorDict={errorDict} {...localizedProps} />;
  }
}

export default injectIntl(withRouter(CheckoutContainer));
