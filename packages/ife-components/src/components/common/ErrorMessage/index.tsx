import * as React from 'react';
import StyledContainer from './StyledContainer';

const ErrorMessage: React.FC<{text: string}> = ({text}) => <StyledContainer>{text}</StyledContainer>;

export default ErrorMessage;
