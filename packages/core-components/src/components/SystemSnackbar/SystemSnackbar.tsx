import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {SystemSnackbar as ExternalInterface} from '../common/ComponentsInterface';
import Snackbar from '@material-ui/core/Snackbar';
import {SystemMessage} from 'component-utils/dist/store/system/reducer';
import isEqual from 'lodash-es/isEqual';
import {injectIntl} from 'react-intl';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  message?: SystemMessage;
}

const SnackbarWrapper = styled((props: any) => {
  const {children, ...rest} = props;
  return (
    <Snackbar className={`${props.className}`} {...rest}>
      {children}
    </Snackbar>
  );
})`
  &.core-components-system-snackbar {
    ${renderThemeProperty('bottomOffset', 'bottom')};
    padding: 2px 1rem;

    > div {
      ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.snackbarPadding, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.snackbarWidth, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.snackbarWidth, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.snackbarHeight, props.theme)};
      > div {
        ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.snackbarWidth, props.theme)};
        ${(props: any) =>
          applyMediaQueriesForStyle('max-width', props.theme.componentTheme.snackbarWidth, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.snackbarHeight, props.theme)};
      }
    }

    &.error {
      > div {
        ${renderThemeProperty('errorBackgroundColor', 'background-color', true)};
      }
    }

    &.info {
      > div {
        ${renderThemeProperty('infoBackgroundColor', 'background-color', true)};
      }
    }

    .core-components-system-snackbar-body {
      display: flex;
      padding: 1rem 0;
      ${renderThemeProperty('bodyFlexDirection', 'flex-direction')};

      i {
        ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.iconFontSize, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.iconMargin, props.theme)};
        ${renderThemeProperty('textColor', 'color')};
        ${renderThemeProperty('iconVerticalAlign', 'align-self')};
        flex: 0 1 auto;
      }

      .core-components-system-snackbar-message {
        flex: 0 1 auto;
        display: flex;
        flex-direction: column;

        * {
          ${renderThemeProperty('textColor', 'color')};
        }

        h1 {
          ${(props: any) =>
            applyMediaQueriesForStyle('font-size', props.theme.componentTheme.titleFontSize, props.theme)};
          ${(props: any) =>
            applyMediaQueriesForStyle('line-height', props.theme.componentTheme.titleLineHeight, props.theme)};
          font-weight: bold;
          margin: 0;
          padding: 0;
        }

        span {
          ${(props: any) =>
            applyMediaQueriesForStyle('font-size', props.theme.componentTheme.messageFontSize, props.theme)};
          ${renderThemeProperty('messageFontWeight', 'font-weight')};
          ${(props: any) =>
            applyMediaQueriesForStyle('line-height', props.theme.componentTheme.messageLineHeight, props.theme)};
          ${(props: any) =>
            applyMediaQueriesForStyle('margin-top', props.theme.componentTheme.messageBodyTopMargin, props.theme)};
        }
      }
    }
  }
` as any;

class SystemSnackbar extends React.Component<ExternalInterface & InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }

  public shouldComponentUpdate(nextProps: ExternalInterface & InternalInterface) {
    return !isEqual(nextProps, this.props); // prevents mui re-animating bug
  }

  private _getDefaultMessage(): SystemMessage {
    return {
      title: 'Message Title',
      message: 'Message Body',
      isError: false
    };
  }

  public render() {
    const message = this.props.message ? this.props.message : this._getDefaultMessage();
    let messageBody: React.ReactNode = null;

    if (message !== null) {
      messageBody = (
        <div className="core-components-system-snackbar-body">
          <i
            className={
              'fa ' +
              (message.isError ? 'fa-exclamation-circle' : 'fa-info-circle') +
              ' core-components-system-snackbar-icon'
            }
          />
          <div className="core-components-system-snackbar-message">
            <h1>{message.title}</h1>
            <span>{message.message}</span>
          </div>
        </div>
      );
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <SnackbarWrapper
          open={message !== null}
          className={
            'core-components-system-snackbar ' + (message !== null ? (message.isError ? 'error' : 'info') : '')
          }
          message={messageBody ? messageBody : ''}
        />
      </PortalComponent>
    );
  }
}

export default injectIntl(SystemSnackbar);

/*
* EXAMPLE TO CALL FROM STORE:
* dispatch(systemActions.showMessage({title: "111", message: "222", isError: false}, 50000));
* */
