import React = require('react');
import {Paper, LinearProgress} from '@material-ui/core';

export interface Props {
  aircraftId: string;
  altitude: string;
  airSpeed: string;
  progress: number;
  takeOffTime: string;
  originName: string;
  destinationName: string;
  timeToDestInMinutes: number;
}

export const MovingMapInfo: React.FC<Props> = props => {
  return (
    <Paper className="flight-info-wrapper">
      <div className="flight-info-header-wrapper">
        <div className="flight-info-aircraft-id">{props.aircraftId}</div>
        <div className="flight-info-params">
          <div className="flight-info-param">
            <span className="flight-info-param-label">Altitude</span>
            <span className="flight-info-param-value">{props.altitude} m</span>
          </div>
          <div className="flight-info-param">
            <span className="flight-info-param-label">Speed</span>
            <span className="flight-info-param-value">{props.airSpeed} km/h</span>
          </div>
        </div>
      </div>
      <LinearProgress className="flight-progress" variant="determinate" value={props.progress} />
      <div className="flight-info-time">
        <span className="flight-info-time-label-wrapper">
          <span className="flight-info-time-label">{props.takeOffTime}</span>
          <span> from </span>
          <span className="flight-info-time-label">{props.originName}</span>
        </span>
        <span className="flight-info-time-label-wrapper destination">
          <span>to </span>
          <span className="flight-info-time-label">{props.destinationName}</span>
          <span className="flight-info-time-label"> {props.timeToDestInMinutes}mins</span>
        </span>
      </div>
    </Paper>
  );
};
