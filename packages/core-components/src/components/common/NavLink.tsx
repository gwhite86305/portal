import * as React from 'react';
import {NavLink} from 'react-router-dom';

// -- Utils
const isExternalUrl = (value: string) =>
  /^\/?(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(
    value.trim()
  );
let AppNavLink = NavLink;

const Link = ({to = '', child = null, openInNewTab = false, isMediaAsset = false, ...rest}: any) => {
  const isExternal = isExternalUrl(to);
  const isExternalWithWww = !isMediaAsset && isExternal && /^\/www\./.test(to);
  const isHashLink = !isMediaAsset && !isExternal && openInNewTab && process.env.HASH_ROUTES;
  if (isHashLink) to = `#${to}`;
  if (isExternalWithWww) to = `//${to}`;
  const anchor_props = {href: to.replace(/^\//, ''), target: '_blank'};

  return isExternal || openInNewTab ? (
    child ? (
      <a {...anchor_props}> {child} </a>
    ) : (
      <a {...anchor_props} {...rest} />
    )
  ) : child ? (
    <AppNavLink to={to}>{child}</AppNavLink>
  ) : (
    <AppNavLink to={to} {...rest} />
  );
};

export default Link;
