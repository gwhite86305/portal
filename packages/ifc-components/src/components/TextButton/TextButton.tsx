import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {TextButton as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {withRouter} from 'react-router';
import {Button} from 'core-components';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => {
    return applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme);
  }};
  display: flex;
  justify-content: center;
  font-family: ${(props: any) => props.theme.appFontFamily};
  .tb-component-wrapper {
    width: 100%;
    display: flex;
    justify-content: space-between;
    ${(props: any) =>
      applyMediaQueriesForStyle('flex-direction', props.theme.componentTheme.flexDirection, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.textAlign, props.theme)};
  }
  .wrap-center {
    justify-content: center;
  }
  .button-wrap {
    background-color: ${(props: any) => props.theme.primaryBackgroundColor};
    align-self: center;
    :focus {
      color: ${(props: any) => props.theme.tertiaryIconColor};
    }
  }
  h4 {
    height: 24px;
    margin: 0px;
    font-size: 20px;
    font-weight: 300;
    line-height: 21px;
    ${renderThemeProperty('localizedHeaderTextColor', 'color')};
    ${renderThemeProperty('localizedHeaderFontWeight', 'font-weight')};
  }
  h2 {
    height: 36px;
    margin: 0px;
    margin-top: calc(${(props: any) => props.theme.gutter} * 0.0625);
    font-size: 30px;
    font-weight: 300;
    line-height: 32px;
    ${renderThemeProperty('localizedSubHeaderTextColor', 'color')};
    ${renderThemeProperty('localizedSubHeaderFontWeight', 'font-weight')};
  }
  .text-div {
    text-align: center;
  }
  button {
    display: flex;
    width: 150px;
    height: 41px;
    margin: 0 auto;
    background: ${(props: any) => props.theme.primaryButtonColor};
    justify-content: center;
    font-weight: normal;
    font-family: ${(props: any) => props.theme.appFontFamily};
    ${renderThemeProperty('localizedButtonColor', 'color')};
    ${renderThemeProperty('localizedButtonFontSize', 'font-size')};
    ${renderThemeProperty('localizedButtonBorder', 'border')};
    ${renderThemeProperty('localizedButtonWidth', 'width')};
    ${renderThemeProperty('localizedButtonHeight', 'height')};
  }
`;

class TextButton extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  onClick = () => {
    this.props.history.push('/' + this.props.feature.openPageOnClick);
  };

  public render() {
    const {localizedHeaderText, localizedSubHeaderText, localizedButtonText, componentTheme} = this.props;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <Container>
          <div
            className={this.props.feature.includeButton ? 'tb-component-wrapper' : 'tb-component-wrapper wrap-center'}
          >
            <div className={this.props.feature.includeButton ? '' : 'text-div'}>
              {localizedHeaderText ? <h4>{localizedHeaderText}</h4> : null}
              {localizedSubHeaderText ? <h2>{localizedSubHeaderText}</h2> : null}
            </div>
            {this.props.feature.includeButton ? (
              <div className="button-wrap">
                <Button isPrimary={true} onClick={this.onClick.bind(this)} aria-label={localizedButtonText}>
                  {localizedButtonText}
                </Button>
              </div>
            ) : null}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default withRouter(TextButton);
