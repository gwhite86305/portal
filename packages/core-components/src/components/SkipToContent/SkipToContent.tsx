import * as React from 'react';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {SkipToContent as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

// -- Styled Components
const StyledContainer = styled.div`
  width: 110px;
  > div {
    display: flex;
    flex: 1;
    width: 25%;
    margin-left: 5px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    z-index: 10;
    ${renderThemeProperty('backgroundColor', 'background-color')};
    ${renderThemeProperty('position', 'position')};
    ${renderThemeProperty('textActiveColor', 'color')};
    .icon {
      position: relative;
      z-index: 11;
    }
    a:focus {
      ${renderThemeProperty('textHoverColor', 'color', true)};
    }
  }
  .skipNav span {
    display: none;
  }
  .skipNav:focus span {
    display: inline-block;
    ${renderThemeProperty('boxShadow', 'box-shadow')};
  }
  .skipTarget {
    position: absolute;
    left: -3000px;
  }
`;

const SkipToContentLink = (props: any) => {
  let { feature, keypress } = props;
  return (
    <React.Fragment>
      <div tabIndex={0} className="skipNav" onKeyPress={keypress}>
        <span>{props.skipNavText}</span>
      </div>
    </React.Fragment>
  );
};

class SkipToContent extends React.Component<ExternalInterface, any> {
  public static defaultProps = defaults;
  skipTarget: any;

  constructor(props: any) {
    super(props);
    this.skipTarget = React.createRef();
  }

  onSkip = () => {
    this.skipTarget.current.focus();
  };

  public render() {
    const {componentTheme, feature} = this.props;
    const mergedTheme = {...((SkipToContent.defaultProps as any).componentTheme || {}), ...componentTheme};
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledContainer>
          <div>
            <SkipToContentLink {...this.props} feature={feature} keypress={this.onSkip.bind(this)} ></SkipToContentLink>
            <span tabIndex={0} className="skipTarget" ref={this.skipTarget} />
          </div>
        </StyledContainer>
      </PortalComponent>
    );
  }
}

export default SkipToContent;
