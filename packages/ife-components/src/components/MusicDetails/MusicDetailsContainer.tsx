import {PortalComponent, utils} from 'component-utils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import {lastIndexOf, pathOr, propEq, slice} from 'ramda';
import * as React from 'react';
import {AudioGroup, AudioItem, AUDIO_TYPE} from '../../store/audio/types';
import MusicDetails from './MusicDetails';
import StyledContainer from './StyledContainer';
import {DetailsContainerProps, DetailsContainerState, MusicDetailsProps} from './types';
import {imgPlaceholder, attachImagesToAudioItem, mapDispatchToProps, mapStoreToProps} from './utils';
import Loader from '../SeriesDetails/Loader';
import {playAudioPlayer, pauseAudioPlayer, audioPlayer} from '../../utils/audioPlayer';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class MusicDetailsContainer extends React.PureComponent<DetailsContainerProps, DetailsContainerState> {
  public static contextType = RouterContext;
  public audioId = 'audioId=';
  state = {item: undefined, contentId: ''} as DetailsContainerState;

  componentDidMount() {
    this.readUrlAndfetchData(this.props);
  }

  componentDidUpdate(_: any, prevState: DetailsContainerState) {
    const isValidItem = this.state.item && this.state.item.items;
    if (!isValidItem || prevState.contentId !== this.state.contentId) {
      const item = this.props.storeState.audioGroups.find(propEq('uid', this.state.contentId));
      this.setState({item});
    }
  }

  render() {
    const isPlayerPaused = audioPlayer.paused;
    const {playlistLoading} = this.props;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <StyledContainer {...{feature: this.props.feature}}>
          {!this.props.storeState.isLoading && this.state.item && !playlistLoading ? (
            <MusicDetails
              onClickBack={this.onClickBack}
              currentPlayingUid={
                this.props.storeState.currentPlayingItem && this.props.storeState.currentPlayingItem.uid
              }
              imgPlaceholder={imgPlaceholder}
              isBuffering={this.props.storeState.isBuffering}
              isPlayerPaused={
                utils.mobileSdk.isUsingMobileSdk() ? this.props.storeState.isPlayerPaused : isPlayerPaused
              }
              item={this.state.item}
              onClickPause={this.props.actions.pauseItem}
              onClickPlay={this.onClickPlay}
              onRemoveFromPlaylist={this.props.actions.removeFromPlaylist}
              playlistIdsMap={this.props.storeState.playlistIdsMap}
              addToPlaylist={this.props.actions.addToPlaylist}
              playAll={this.playAll}
              handleListItemClick={this.handleListItemClick}
            />
          ) : (
            <Loader />
          )}
        </StyledContainer>
      </PortalComponent>
    );
  }

  private onClickPlay = (item: AudioItem) => {
    this.props.actions.trackPlay(item);
    this.props.actions.playItem(item, this.props.feature.playerPage as string);
  };

  private onClickBack = () => this.context.history.goBack();

  private getContentId() {
    const querySearch = pathOr('', ['location', 'search'], this.context);
    const index = lastIndexOf(this.audioId, querySearch as any);
    return index > -1 ? slice(index + this.audioId.length, Infinity, querySearch) : '';
  }

  private areEpisodeLoaded = (item: AudioGroup | undefined) => item && !!item.items;

  private readUrlAndfetchData(props: DetailsContainerProps) {
    const contentId = this.getContentId();
    if (!contentId) {
      return;
    }
    this.props.actions.trackView({uid: contentId, type: AUDIO_TYPE.GROUP} as AudioGroup);
    const cachedItem = props.storeState.audioGroups.find(propEq('uid', contentId));
    this.setState({contentId});
    // Exit if available in cache
    if (this.areEpisodeLoaded(cachedItem)) {
      return;
    }
    // Run asyc call -> Item will be set by componentDidUpdate
    props.actions.getAudioById(contentId);
  }

  private playAll = (tracks: AudioItem[], albumItem: AudioGroup) => {
    const parentId = this.state.item && this.state.item.uid;
    const formattedToAdd = tracks.map(track => attachImagesToAudioItem({...track, parentId}, albumItem as AudioGroup));
    this.props.actions.addToPlaylist(formattedToAdd);
    this.onClickPlay(tracks[0]);
    playAudioPlayer();
  };

  private handleListItemClick = (track: AudioItem, isPauseButton: boolean, albumItem: AudioGroup) => (e: any) => {
    if (isPauseButton) {
      this.props.actions.pauseItem(track);
      pauseAudioPlayer();
    } else {
      const parentId = this.state.item && this.state.item.uid;
      this.props.actions.addToPlaylist([attachImagesToAudioItem({...track, parentId}, albumItem as AudioGroup)]);
      this.onClickPlay(track);
      playAudioPlayer();
      e.stopPropagation();
    }
  };
}
