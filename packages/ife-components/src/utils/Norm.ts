import {difference, merge, omit, union, isObject} from 'lodash';

export interface Normalized<K extends string, T> {
  byId: Record<K, T>;
  allIds: K[];
}

export interface NormalizableObject {
  uid: string;
  [key: string]: any;
}

export default class Norm {
  static toNormalized = (source: NormalizableObject | NormalizableObject[]): Normalized<string, any> => {
    if (Array.isArray(source)) {
      return {
        byId: source.reduce(
          (accum: object, item: {[key: string]: any}) => ({
            ...accum,
            [item.uid]: item
          }),
          {}
        ),
        allIds: source.map((item: any) => item.uid)
      };
    }
    return {
      byId: {[source.uid]: source},
      allIds: [source.uid]
    };
  };

  static merge = <T>(source1: Normalized<any, T>, source2: Normalized<any, T>) => ({
    byId: merge({}, source1.byId, source2.byId),
    allIds: union(source1.allIds, source2.allIds)
  });

  static omit = <T>(source1: Normalized<any, T>, source2: Normalized<any, T>) => ({
    byId: omit(source1.byId, source2.allIds),
    allIds: difference(source1.allIds, source2.allIds)
  });

  static deleteId = <T>(source1: Normalized<any, T>, uid: string) => ({
    byId: omit(source1.byId, [uid]),
    allIds: difference(source1.allIds, [uid])
  });

  static filter = <T>(source1: Normalized<any, T>, predicate: (element: T) => boolean) => {
    const validIds = source1.allIds.filter(id => predicate(source1.byId[id]));
    return validIds.reduce(
      (acc, id) => ({
        allIds: [...acc.allIds, id],
        byId: {...acc.byId, [id]: source1.byId[id]}
      }),
      {byId: {}, allIds: []}
    );
  };
}
