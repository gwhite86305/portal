import * as React from 'react';
import GridLayout, {GridItem} from '../GridLayout/GridLayout';
import {VideoGroup} from '../../store/videos/types';
import {getGridTheme, getRowPos, getColumPos} from '../../utils/grid';
import {getImageSrcByRatio} from '../common/utils';
import {ImageVariantsType} from '../types';

export interface SeriesGridProps {
  seriesItems: VideoGroup[];
  width: number;
  handleClick: (id: string) => void;
}

const SeriesGrid: React.FC<SeriesGridProps> = ({seriesItems, width, handleClick}) => {
  const mdFraction = 6;
  const smFraction = 3;
  const xsFraction = 2;
  const imageRatio = 27 / 40;
  const theme = getGridTheme(width, xsFraction, smFraction, mdFraction, imageRatio, seriesItems.length);
  const gridLayoutItems: GridItem[] = seriesItems.map((item, index) => ({
    title: item.title,
    variant: 'small',
    id: item.uid,
    imgSrc: item.titleImages
      ? getImageSrcByRatio(item.titleImages[0], ImageVariantsType.posterthumb, window.devicePixelRatio)
      : '',
    rowPos: getRowPos(index + 1, xsFraction, smFraction, mdFraction),
    columnPos: getColumPos(index + 1, xsFraction, smFraction, mdFraction),
    handleClick
  }));
  return <GridLayout componentTheme={theme} feature={{items: gridLayoutItems}} />;
};

export default SeriesGrid;
