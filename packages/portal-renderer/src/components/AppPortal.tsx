import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {withRouter} from 'react-router';
import FontLoader from 'component-utils/dist/utils/FontLoader';
import {AppProviders, getAppTemplateAndBody, getSystemSnackbar, IntlAndThemeProviders} from '../utils/AppUtils';
import AppContainer from './AppContainer';
import LayoutInterface from 'component-utils/dist/types/LayoutInterface';
import {ThemeSchema} from 'component-utils/dist/components/common/ThemeInterface';
import {pathOr} from 'ramda';
import {IRootState} from '../store';
import {State as ISystemState} from 'component-utils/dist/store/system/reducer';
import {History} from 'history';

export interface StateProps {
  system: ISystemState;
  userSelectedLocale: string | null;
}

export interface AppProps {
  layout: LayoutInterface;
  theme: ThemeSchema;
  system: ISystemState;
  location: Location;
}

export type Props = AppProps & StateProps;

const mapStateToProps = (state: IRootState): StateProps => {
  return {
    system: state.system,
    userSelectedLocale: state.ux.userSelectedLocale // leave this here: it ensure in portal mode UX refreshes after locale change
  };
};

@connectWrapper(mapStateToProps, {})
class AppPortalContainer extends React.Component<Props> {
  public componentDidMount() {
    this.updateFavicon(pathOr('', ['theme', 'favicon'], this.props));
    if (!FontLoader.hasFontThemeApplied()) {
      FontLoader.applyFontTheme(this.props.theme);
    }
  }

  public componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
    }

    if (prevProps.theme.favicon !== this.props.theme.favicon) {
      this.updateFavicon(this.props.theme.favicon);
    }
  }

  public render() {
    const {system, layout} = this.props;
    return (
      <React.Fragment>
        {system.systemSettings ? (
          <AppContainer>
            {getAppTemplateAndBody(layout)}
            {system.systemMessage ? getSystemSnackbar(this.props) : null}
          </AppContainer>
        ) : null}
      </React.Fragment>
    );
  }

  private updateFavicon(url) {
    const currentFavicon = document.querySelector("link[rel*='icon']") as HTMLAnchorElement;
    if (currentFavicon) return (currentFavicon.href = url);
    const newFavicon = document.createElement('link');
    newFavicon.type = 'image/x-icon';
    newFavicon.rel = 'shortcut icon';
    newFavicon.href = url;
    document.getElementsByTagName('head')[0].appendChild(newFavicon);
  }
}

const AppContainerWithRouter = withRouter(AppPortalContainer as any);

export interface AppPortalProps {
  layout: LayoutInterface;
  theme: ThemeSchema;
  history: History;
}

const AppPortal: React.FunctionComponent<AppPortalProps> = (props: AppPortalProps) => {
  return (
    <AppProviders {...props}>
      <IntlAndThemeProviders theme={props.theme}>
        <AppContainerWithRouter {...props} />
      </IntlAndThemeProviders>
    </AppProviders>
  );
};

export default AppPortal;
