import * as React from 'react';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {Footer as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {NavLink} from 'react-router-dom';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper, withKeys, isUrlValid, isValidRelativePath} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  position: relative;
  width: 100%;
  ${(props: any) => {
    return applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme);
  }};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  align-items: center;
  text-align: center;
  border-top-width: 1px;
  border-top-style: solid;
  border-top-color: ${(props: any) => props.theme.componentTheme.dividerColor};
  .text-content {
    display: inline-block;
    box-sizing: border-box;
    position: relative;
    vertical-align: top;
    font-size: ${(props: any) => props.theme.componentTheme.linksRegionFontSize};
    line-height: normal;
    ${(props: any) => {
      return applyMediaQueriesForStyle('width', {xs: '100%', sm: '50%'}, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle('padding', props.theme.componentTheme.paddingLinkSection, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle('border-bottom', {xs: `1px solid`, sm: `0`}, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'border-bottom-color',
        {xs: `${props.theme.componentTheme.dividerColor}`, sm: `none`},
        props.theme
      );
    }};
    .footer-links {
      ${(props: any) => {
        return applyMediaQueriesForStyle(
          'float',
          {
            xs: `initial`,
            sm: `right`
          },
          props.theme
        );
      }};
      text-align: left;
      box-sizing: border-box;
    }
    a {
      display: block;
      ${renderThemeProperty('textColorLinks', 'color')};
      ${renderThemeProperty('fontFamily', 'font-family')};
      ${renderThemeProperty('fontSize', 'font-size')};
      ${renderThemeProperty('fontWeight', 'font-weight')};
      ${renderThemeProperty('textDecorationLine', 'text-decoration-line')};
      ${(props: any) => {
        return applyMediaQueriesForStyle('text-align', {xs: 'center', sm: 'left'}, props.theme);
      }};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 0.5);
    }
  }
  .logo-region {
    display: inline-block;
    box-sizing: border-box;
    line-height: normal;
    ${(props: any) => {
      return applyMediaQueriesForStyle('width', {xs: '100%', sm: '50%'}, props.theme);
    }};
    color: ${(props: any) => props.theme.primaryBackgroundColor};
    ${(props: any) => {
      return applyMediaQueriesForStyle('padding', props.theme.componentTheme.paddingLogoRegion, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle('border-left', {xs: `0`, sm: `1px solid`}, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'border-left-color',
        {xs: `none`, sm: `${props.theme.componentTheme.dividerColor}`},
        props.theme
      );
    }};
    font-size: ${(props: any) => props.theme.componentTheme.logoRegionFontSize};
    span {
      display: block;
      word-wrap: break-word;
      ${renderThemeProperty('textColorLogoRegion', 'color')};
      ${(props: any) => {
        return applyMediaQueriesForStyle(
          'margin-bottom',
          {
            xs: `10px`,
            sm: `20px`
          },
          props.theme
        );
      }};
      margin-top: ${(props: any) => props.theme.gutter};
      ${(props: any) => {
        return applyMediaQueriesForStyle('text-align', {xs: `center`, sm: `left`}, props.theme);
      }};
      ${(props: any) => {
        return applyMediaQueriesForStyle('margin-left', {xs: `auto`, sm: `0`}, props.theme);
      }};
      ${(props: any) => {
        return applyMediaQueriesForStyle('margin-right', {xs: `auto`, sm: `0`}, props.theme);
      }};
    }
    img {
      display: block;
      ${(props: any) => {
        return applyMediaQueriesForStyle('margin', {xs: `auto`, sm: `0`}, props.theme);
      }};
    }
  }
  .notes-section {
    ${renderThemeProperty('textColorLinks', 'color')};
    background-color: inherit;
    border-top-width: 1px;
    border-top-style: solid;
    ${(props: any) => {
      return applyMediaQueriesForStyle('border-top-color', {xs: props.theme.componentTheme.dividerColor}, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'padding-top',
        {
          xs: `18px`,
          sm: `23px`
        },
        props.theme
      );
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'padding-bottom',
        {
          xs: `40px`,
          sm: `26px`
        },
        props.theme
      );
    }};
  }
  p {
      margin: 0;
  }
}`;

class Footer extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public parseUrl = (url: any) => {
    if (typeof url === 'string') {
      return url;
    } else {
      return url[this.props.intl.locale];
    }
  };
  public render() {
    const {
      localizedAltImageText,
      localizedCopyrightText,
      localizedNotesSectionText,
      localizedlogoRegionText,
      localizedMenuItems
    } = this.props;
    const {logoImage} = this.props.feature;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          <div className="text-content">
            <div className="footer-links">
              {localizedMenuItems
                ? withKeys(
                    localizedMenuItems.map(
                      (footerLink: any, index: number) =>
                        isUrlValid(footerLink.url) || isValidRelativePath(footerLink.url) ? (
                          <a
                            className="tertiary-focus"
                            dangerouslySetInnerHTML={sanitize(footerLink.label)}
                            target="_blank"
                            href={this.parseUrl(footerLink.url)}
                          />
                        ) : (
                          <NavLink
                            className="tertiary-focus"
                            to={`/${footerLink.url}`}
                            dangerouslySetInnerHTML={sanitize(footerLink.label)}
                          />
                        )
                    )
                  )
                : null}
            </div>
          </div>
          <div className="logo-region">
            {localizedlogoRegionText ? <span dangerouslySetInnerHTML={sanitize(localizedlogoRegionText)} /> : null}
            {logoImage ? <img src={logoImage} alt={localizedAltImageText} /> : null}
            <span dangerouslySetInnerHTML={sanitize(localizedCopyrightText)} />
          </div>
          {localizedNotesSectionText ? (
            <div className="notes-section" dangerouslySetInnerHTML={sanitize(localizedNotesSectionText)} />
          ) : null}
        </Container>
      </PortalComponent>
    );
  }
}

export default Footer;
