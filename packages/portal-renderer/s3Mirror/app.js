const fs = require('fs');
const express = require('express');
const request = require('request');
const AWS = require('aws-sdk');
const app = express();

// This sdk uses your stored aws credentials to authenticate.
// There will need to be a service account and credentials which are treated as secrets
// Example using docker file from folder up a level:
//  docker build -t pcms-portal-renderer --build-arg CACHEBUST=$(date +%s) --build-arg AWS_SECRET_ACCESS_KEY=${env.AWS_SECRET_ACCESS_KEY} --build-arg AWS_ACCESS_KEY_ID=${env.AWS_ACCESS_KEY_ID} .

const s3 = new AWS.S3({
  region: 'us-west-2'
});

const LATEST_VERSION = process.env.LATEST_VERSION || '0.0.1';
const BUCKET_NAME = process.env.BUCKET_NAME || 'pcms-portal-components';

//We will use middle ware to do RBAC auth of any requests

app.use((req, res, next) => {
  const version = req.query.lib_version || LATEST_VERSION;
  const library = req.query.lib || 'portal-renderer';

  const basePath = `/${version}/lib/${library}`;
  const path = req.path === '/' ? `${basePath}/index.html` : `${basePath}${req.path}`;

  try {
    fs.stat(`${__dirname}/static/${path}`, function(err, stat) {
      if (err === null) {
        res.sendFile(`${__dirname}/static/${path}`);
      } else {
        console.error(`Attempt to reach (${path}) caused error: ${err.code}`);
      }
    });
    // We can also use authentication to get the bucket, allowing us to keep it private
  } catch (e) {
    console.error(e);
  } finally {
  }
});

app.listen(80, () => console.log('S3 Node Mirror Listening'));
