import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {WaitSplash as ExternalInterface} from '../common/ComponentsInterface';
import WaitSplash, {InternalInterface} from './WaitSplash';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

export type PageProps = ExternalInterface & InternalInterface;

@connectWrapper((state: any) => {
  return {
    builderSelectedComponentUid: state.ux.builderSelectedComponentUid
  };
})
class WaitSplashContainer extends React.Component<PageProps, any> {
  public render() {
    const {builderSelectedComponentUid, componentUid, show} = this.props;
    return <WaitSplash {...this.props} />;
  }
}

export default withLocaleProps(WaitSplashContainer, [
  {localizedHeaderText: 'core.WaitSplash.header.text'},
  {localizedSubHeaderText: 'core.WaitSplash.subHeader.text'},
  {localizedMessageText: 'core.WaitSplash.message.text'}
]) as any;
