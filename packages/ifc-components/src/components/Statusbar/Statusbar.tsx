import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {GetTempFC} from 'component-utils/dist/utils/TemperatureConverter';
import {convertMinutesToClock} from 'component-utils/dist/utils/DateUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {Statusbar as ExternalInterface} from '../common/ComponentsInterface';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  isOffline?: boolean;
  flightOrigin?: string;
  flightDestination?: string;
  flightNumber?: string;
  flightMinutesRemaining?: number;
  destinationTemperature?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('position', 'position')};
  ${renderThemeProperty('textColor', 'color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  width: 100%;
  div {
    display: flex;
    align-items: center;
    border-right-width: 1px;
    border-right-style: solid;
    ${renderThemeProperty('dividerColor', 'border-right-color')};
    flex-direction: column;
    padding-left: ${(props: any) => props.theme.gutter};
    padding-right: ${(props: any) => props.theme.gutter};
    span {
      line-height: 0.69;
    }
    span:last-child {
      margin-top: calc(${(props: any) => props.theme.gutter} * 0.625);
    }
  }

  div:last-child {
    display: flex;
    align-items: center;
    border-right: none;
  }

  img.wifi-icon-up {
    width: calc(${(props: any) => props.theme.componentTheme.fontSize.gutter} * 1.25);
    height: calc(${(props: any) => props.theme.componentTheme.fontSize.gutter} * 1.25);
    margin-right: calc(${(props: any) => props.theme.gutter} * 1.25);
  }
  img.wifi-icon-down {
    width: 20px;
    height: 20px;
    margin-right: calc(${(props: any) => props.theme.gutter} * 1.25);
  }
  img.ml6 {
    margin-right: 0px;
    margin-left: calc(${(props: any) => props.theme.gutter} * 0.375);
  }
  .wifiAvailability {
    display: flex;
    flex-direction: row;
    padding-left: 0px;
    padding-right: 0px;
    flex-direction: ${(props: any) =>
      props.theme.componentTheme.switchWifiServiceAvailablityIconTextPosition ? 'row-reverse' : 'row'};
    h1 {
      ${renderThemeProperty('fontWeight', 'font-weight', true)};
    }
    p {
      ${renderThemeProperty('fontWeight', 'font-weight', true)};
    }
  }
  .wifi-up {
    ${renderThemeProperty('backgroundColorWifiUp', 'background-color', true)};
  }
  .wifi-down {
    ${renderThemeProperty('backgroundColorWifiDown', 'background-color', true)};
  }
  .ifc-statusbar-component {
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
    ${renderThemeProperty('backgroundColorWifiUp', 'background-color')};
  }
  h1 {
    margin: 0px;
    ${renderThemeProperty('fontSize', 'font-size')};
  }
  p {
    margin: 0px;
    ${renderThemeProperty('fontSize', 'font-size')};
  }
`;

const FlightOriginDestination = (props: any) => {
  const {flightOrigin, flightDestination} = props;
  return (
    <div>
      <span>{flightOrigin || '---'}</span>
      <span>{flightDestination || '---'}</span>
    </div>
  );
};

const FlightNumber = (props: any) => {
  const {flightNumber} = props;
  return (
    <div>
      <span>Flight</span>
      <span> {flightNumber || '---'} </span>
    </div>
  );
};

const TimeToDestination = (props: any) => {
  const {flightMinutesRemaining} = props;
  return (
    <div>
      <span>{convertMinutesToClock(flightMinutesRemaining).hour}&nbsp;hr</span>
      <span>{convertMinutesToClock(flightMinutesRemaining).minute}&nbsp;mn</span>
    </div>
  );
};

const DestinationTemp = (props: any) => {
  const {destinationTemperature} = props;
  const temp = GetTempFC(destinationTemperature);
  return (
    <div>
      <span>{temp.F}&deg;F</span>
      <span>{temp.C}&deg;C </span>
    </div>
  );
};

const WifiAvailability = (props: any) => {
  const {isOffline} = props;
  const {wifiUpIconPath, wifiDownIconPath} = props.feature;
  const {switchWifiServiceAvailablityIconTextPosition} = props.componentTheme;
  const {
    localizedConnectivityUpText,
    localizedConnectivityDownText,
    localizedConnectivityUpAltText,
    localizedConnectivityDownAltText
  } = props;
  return (
    <div>
      {!isOffline ? (
        <div className="wifiAvailability">
          <img
            className={switchWifiServiceAvailablityIconTextPosition ? 'wifi-icon-up ml6' : 'wifi-icon-up'}
            src={wifiUpIconPath}
            alt={localizedConnectivityUpAltText}
          />{' '}
          {localizedConnectivityUpText ? <p>{localizedConnectivityUpText}</p> : null}
        </div>
      ) : null}
      {isOffline ? (
        <div className="wifiAvailability">
          <img
            className={switchWifiServiceAvailablityIconTextPosition ? 'wifi-icon-down ml6' : 'wifi-icon-down'}
            src={wifiDownIconPath}
            alt={localizedConnectivityDownAltText}
          />{' '}
          {localizedConnectivityDownText ? <p>{localizedConnectivityDownText}</p> : null}
        </div>
      ) : null}
    </div>
  );
};

class Statusbar extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    isOffline: true,
    flightOrigin: '---',
    flightDestination: '---',
    flightNumber: '---',
    flightMinutesRemaining: 0,
    destinationTemperature: '---'
  };

  public render() {
    const {
      showOriginDestination,
      showFlightNumber,
      showTimeToDestination,
      ShowDestinationTemp,
      showWifiServiceAvailablity
    } = this.props.feature;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          <div
            className={!this.props.isOffline ? 'ifc-statusbar-component wifi-up' : 'ifc-statusbar-component wifi-down'}
          >
            {showOriginDestination ? <FlightOriginDestination {...this.props} /> : null}
            {showFlightNumber ? <FlightNumber {...this.props} /> : null}
            {showTimeToDestination ? <TimeToDestination {...this.props} /> : null}
            {ShowDestinationTemp ? <DestinationTemp {...this.props} /> : null}
            {showWifiServiceAvailablity ? <WifiAvailability {...this.props} /> : null}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default Statusbar;
