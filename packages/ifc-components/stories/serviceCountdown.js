import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, object, text, number, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import ServiceCountdown from '../dist/components/ServiceCountdown/ServiceCountdown';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    position: 'static',
    margin: {
      xs: '80px 0 80px 0'
    },
    padding: {
      xs: '150px 0 150px 0'
    },
    headerFontColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.primaryFontSize',
    headerFont: 'theme.appFontFamily',
    timerFontColor: 'theme.primaryTextColor',
    timerFontSize: 'theme.secondaryFontSize',
    timerFont: 'theme.appFontFamily'
  }
};

const neosProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    position: 'static',
    margin: {
      xs: '80px 0 80px 0'
    },
    padding: {
      xs: '150px 0 150px 0'
    },
    headerFontColor: 'theme.secondaryTextColor',
    headerFontSize: '1.5em',
    headerFont: 'theme.appFontFamily',
    timerFontColor: 'theme.secondaryTextColor',
    timerFontSize: '2.25em',
    timerFont: 'theme.appFontFamily'
  }
};

const modifiedProps = {
  componentTheme: {
    backgroundColor: '#674172',
    position: 'static',
    margin: {
      xs: '150px 0 150px 0'
    },
    padding: {
      xs: '80px 0 80px 0'
    },
    headerFontColor: '#ffffff',
    headerFontSize: '1.5em',
    headerFont: 'theme.appFontFamily',
    timerFontColor: '#ffffff',
    timerFontSize: '2.25em',
    timerFont: 'theme.appFontFamily'
  }
};
const localizedText = {
  headerText: 'Service time remaining'
};

const stories = storiesOf('[CMOB-22370]-Service Countdown - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('Service Countdown - Default Props', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultLocaleProps} mockTheme={mockTheme}>
      <ServiceCountdown {...defaultLocaleProps} {...localizedText} />
    </PortalComponentWrapper>
  );
});

stories.add('Service Countdown - Default Props with values', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultLocaleProps} mockTheme={mockTheme}>
      <ServiceCountdown {...defaultLocaleProps} timeRemaining={3595} connected={true} {...localizedText} />
    </PortalComponentWrapper>
  );
});

stories.add('Service Countdown - Neos Theme', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultLocaleProps} mockTheme={mockThemeNeos}>
      <ServiceCountdown {...neosProps} timeRemaining={3595} connected={true} {...localizedText} />
    </PortalComponentWrapper>
  );
});

stories.add('Service Countdown - Modified Theme', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultLocaleProps} mockTheme={mockThemeNeos}>
      <ServiceCountdown {...modifiedProps} timeRemaining={3595} connected={true} {...localizedText} />
    </PortalComponentWrapper>
  );
});

stories.add('Service Countdown - With Knobs', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultLocaleProps} mockTheme={mockThemeNeos}>
      <ServiceCountdown
        componentTheme={object('Component Theme', modifiedProps.componentTheme)}
        headerText={text('Header Text', localizedText.headerText)}
        timeRemaining={number('Time Remaning- Internal', 3595)}
        connected={boolean('Device Connected- Internal', true)}
      />
    </PortalComponentWrapper>
  );
});
