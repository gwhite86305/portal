import {Action, ActionCreators} from './actions';
import {AudioGroup, LocalizedCategory, AudioCategory} from './types';
import Norm, { Normalized } from '../../utils/Norm';

export interface IAudioState {
  categories: Normalized<string, LocalizedCategory>;
  items: Normalized<string, AudioGroup>;
  isLoadingItems: boolean;
  isLoadingSynopsis: boolean;
  userConfig: any;
  error: string;
}

export const initialState: Readonly<IAudioState> = {
  categories: {
    byId: {},
    allIds: []
  },
  items: {
    byId: {},
    allIds: []
  },
  error: '',
  isLoadingItems: false,
  isLoadingSynopsis: false,
  userConfig: {}
};

export default function reducer(state: IAudioState = initialState, action: Action): IAudioState {
  const {type, payload} = action;
  switch (type) {
    case ActionCreators.setCurrentAudioTime.type:
      return {
        ...state,
        userConfig: {
          ...state.userConfig,
          [payload.uid]: {
            ...state.userConfig[payload.uid],
            currentTime: payload.currentTime,
            duration: payload.duration
          }
        }
      };
    // Audio by Id
    case ActionCreators.getAudioByIdStart.type:
      return {
        ...state,
        isLoadingSynopsis: true,
      };
    case ActionCreators.getAudioByIdSuccess.type:
      return {
        ...state,
        items: {byId: {...state.items.byId, [payload.uid]: payload}, allIds: state.items.allIds},
        isLoadingSynopsis: false,
      };
    case ActionCreators.getAudioByIdError.type:
      return {
        ...state,
        isLoadingSynopsis: false,
        error: 'Could Not Fetch Audio',
      };
    case ActionCreators.getAudioStart.type:
      return {
        ...state,
        isLoadingItems: true,
      };
    case ActionCreators.getAudioSuccess.type:
      return {
        ...state,
        items: Norm.merge(state.items, payload.items),
        categories: Norm.merge(state.categories, payload.categories),
        isLoadingItems: false,
      };
    case ActionCreators.getAudioError.type:
      return {
        ...state,
        isLoadingItems: false,
        error: 'Could Not Fetch Data'
      };
    default:
      return state;
  }
}
