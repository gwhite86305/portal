import * as React from 'react';
import  {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export const VideoPlayerContainer = styled.div`
  /* CUSTOMIZATION */
  .circular-progress {
    ${renderThemeProperty('spinnerColor', 'color')};
  }

  display: flex;
  justify-content: center;
  align-items: center;
  background: black;
  height: 100%;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 100;
  *::-webkit-media-controls {
    display: none;
  }
  video::-webkit-media-controls,
  video::-webkit-media-controls-play-button,
  video::-webkit-media-controls-volume-slider {
    display: none !important;
  }

  video::-webkit-media-text-track-container {
    transform: ${(props: any) => {
      const up = props.isShaka ? '60' : '30';
      const down = props.isShaka ? '20' : '10';
      return props.showControls ? `translateY(-${up}px)` : `translateY(-${down}px)`;
    }};
    transition: transform 0.4s ease-out;
  }

  /* DISABLE OUTLINE ON FOCUS FOR NOW */
  *:focus {
    border: none;
    color: unset;
  }

  svg {
    fill: white;
    cursor: pointer;
  }

  color: white;

  [role='progressbar'] {
    z-index: 200;
  }

  .centered-play-button {
    cursor: pointer !important;
    left: calc(50% - 10px);
    position: fixed;
    top: calc(50% - 12px);
    opacity: 1;
    transition: opacity 0.3s ease-out;
    svg {
      shape-rendering: geometricPrecision;
      transform: scale(3);
    }
    z-index: 9999;
  }

  .hidden {
    cursor: default;
    pointer-events: none;
    opacity: 0;
  }

  .back-button svg {
    ${renderThemeProperty('backButtonSize', 'font-size')};
    ${renderThemeProperty('backButtonColor', 'fill')};
  }

  .back-button:hover svg {
    ${renderThemeProperty('backButtonColorHover', 'fill')};
  }

  .back-button {
    transition: opacity 1s;
    left: 0;
    padding: 1rem;
    position: fixed;
    top: 0;
    z-index: 9999;
    opacity: ${({showControls}: any) => (showControls ? 1 : 0)};
  }
`;

export const VideoPlayerControls = styled.div`
  bottom: 0;
  flex-direction: column;
  position: absolute;
  padding: 1rem 0rem 0.8rem 0rem;
  z-index: 101;
  width: 100%;
  transition: opacity 1s;
  opacity: ${({showControls}: any) => (showControls ? 1 : 0)};
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.6) 90%, transparent);
  left: 0;
  right: 0;
  .progress {
    height: 20px;
    padding: 0.5rem 1rem;
    align-items: center;
    display: flex;
    flex: 1;
    justify-content: center;
    input {
      cursor: ${({blockFastForward}: any) => (!blockFastForward ? 'pointer' : 'default')} !important;
    }
  }
  .progress__time {
    padding-left: 1rem;
  }

  .actions-buttons {
    height: 20px;
    padding: 0 1rem;
    align-items: center;
    display: flex;
    flex: 1;
    justify-content: space-between;

    .group {
      display: flex;
      align-items: center;
      * + * {
        margin-left: 0.5rem;
      }
    }
  }

  .progress-time {
    font-size: ${({progressTextSize}: any) => progressTextSize};
  }

  .lang-button {
    cursor: default;
    opacity: 0;
    transition: opacity 1s;
    pointer-events: none;
  }

  .lang-button.show {
    cursor: pointer;
    opacity: 1;
    pointer-events: auto !important;
  }
`;
