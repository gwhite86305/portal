import {Coordinate, Point} from './types';

export const formatTakeOffTime = (takeOffTime: number): string => {
  return takeOffTime
    ? new Date(takeOffTime).toLocaleTimeString(navigator.language, {
        hour: '2-digit',
        minute: '2-digit'
      })
    : '';
};

export const toLatitudeLongitude = (position: Coordinate): Point => {
  return position ? cordToArray(position) : [0, 0];
};

const toRad = (deg: number) => {
  return (deg * Math.PI) / 180;
};

export const bearing = (position: Coordinate, routePoints: Coordinate[]) => {
  if (routePoints && routePoints.length > 1) {
    const lat1Rad: number = toRad(routePoints[routePoints.length - 2].latitude);
    const lat2Rad: number = toRad(position.latitude);
    const lng1Rad: number = toRad(routePoints[routePoints.length - 2].longitude);
    const lng2Rad: number = toRad(position.longitude);
    const y = Math.sin(lng2Rad - lng1Rad) * Math.cos(lat2Rad);
    const x =
      Math.cos(lat1Rad) * Math.sin(lat2Rad) - Math.sin(lat1Rad) * Math.cos(lat2Rad) * Math.cos(lng2Rad - lng1Rad);
    return Math.atan2(y, x);
  } else return 0;
};

export const bezierLine = (a: Point, b: Point): {angle: number; length: number} => {
  const lengthX = b[0] - a[0];
  const lengthY = b[1] - a[1];
  return {
    angle: Math.atan2(lengthY, lengthX),
    length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2))
  };
};

export const controlPoint = (current: Point, previous: Point, next: Point, reverse: boolean): Point => {
  const previousWithFallback = previous || current;
  const nextWithFallback = next || current;
  const smoothingRation = 0.2;
  const opposedLine = bezierLine(previousWithFallback, nextWithFallback);

  // If is end-control-point, add PI to the angle to go backward
  const angle = opposedLine.angle + (reverse ? Math.PI : 0);
  const length = opposedLine.length * smoothingRation;

  const x = current[0] + Math.cos(angle) * length;
  const y = current[1] + Math.sin(angle) * length;
  return [x, y];
};

export const bezierCommand = (point: Point, i: number, a: Point[]): string => {
  const [controlPointStartX, controlPointStartY] = controlPoint(a[i - 1], a[i - 2], point, false);
  const [controlPointEndX, controlPointEndY] = controlPoint(point, a[i - 1], a[i + 1], true);

  return `C ${controlPointStartX},${controlPointStartY} ${controlPointEndX},${controlPointEndY} ${point[0]},${point[1]}`;
};

// returns two index array eg [latitute: number, longitude: number]
export const cordToArray = (cord: Coordinate): number[] => [cord.latitude, cord.longitude];
