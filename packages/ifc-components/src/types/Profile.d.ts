import LayoutInterface from 'component-utils/dist/types/LayoutInterface';

export type breakpointIconName = "SmallPhone" | "PhoneAndroid" | "TabletAndroid" | "LaptopWindows" | "DesktopWindows";

export interface Breakpoint {
  "height": number,
  "iconName": breakpointIconName,
  "scale": number,
  "width": number
}

export interface CurrentCondition {
  "airport": string,
  "cityName": string,
  "currentCondition": {
    "temperature": number,
    "weatherText": string,
    "weatherIcon": number
  }
}

export interface DailyForecast {
  "airport": string,
  "cityName": string,
  "dailyForecast": [{
    "date": Date,
    "epochDate": number,
    "temperature": {
      "minimum": {
        "value": number
      },
      "maximum": {
        "value": number
      }
    },
    "day": {
      "icon": number,
      "iconPhrase": string
    },
    "night": {
      "icon": number,
      "iconPhrase": string
    }
  }
    ]
}

export default interface Profile {
  "breakpoints": Breakpoint[],
  "currentConditions": CurrentCondition[],
  "dailyForecasts": DailyForecast[],
  "debug": boolean,
  "layout": LayoutInterface,
  "locations": {
    "origin": string,
    "originCode": string,
    "originLatitude": number,
    "originLongitude": number,
    "originElevation": number,
    "originCountry": string,
    "destination": string,
    "destinationCode": string,
    "destinationLatitude": number,
    "destinationLongitude": number,
    "destinationElevation": number,
    "destinationCountry": string,
  },
  "media": { [id: number]: string },
  "theme": {
    "activeIconColor": string,
    "altBackgroundColor": string,
    "appFontFamily": string,
    "appFontFiles": [{
      "file": string,
      "weight": number
    }],
    "buttonRadius": string,
    "defaultFontSize": string,
    "fontIconBaseClass": string,
    "fontIconCss": string,
    "fontIconFiles": string,
    "fontIconFontFamily": string,
    "fontIconUsesLigature": boolean,
    "fontWeight": string,
    "formErrorColor": string,
    "gutter": string,
    "modalRadius": string,
    "primaryBackgroundColor": string,
    "primaryButtonActiveColor": string,
    "primaryButtonColor": string,
    "primaryButtonHoverColor": string,
    "primaryButtonHoverTextColor": string,
    "primaryButtonTextColor": string,
    "primaryFontSize": string,
    "primaryIconColor": string,
    "primaryIconDisabledColor": string,
    "primaryTextColor": string,
    "primaryTextShadow": string,
    "quaternaryBackgroundColor": string,
    "quaternaryIconColor": string,
    "quinaryBackgroundColor": string,
    "secondaryBackgroundColor": string,
    "secondaryButtonActiveColor": string,
    "secondaryButtonColor": string,
    "secondaryButtonHoverColor": string,
    "secondaryButtonHoverTextColor": string,
    "secondaryButtonTextColor": string,
    "secondaryFontFamily": string,
    "secondaryFontFiles": [{
      "file": string,
      "weight": number
    }],
    "secondaryFontSize": string,
    "secondaryIconColor": string,
    "secondaryIconDisabledColor": string,
    "secondaryTextColor": string,
    "secondaryTextShadow": string,
    "senaryBackgroundColor": string,
    "tertiaryIconColor": string,
    "tertiaryBackgroundColor": string,
    "tertiaryFontSize": string,
    "tertiaryTextColor": string
  },
  "weather": {
    "destinationTemperature": string,
    "destinationConditions": string,
    "destinationWeatherIcon": number
  },
  "contentPack": {
    "default": {
      "defaultSupportPage": string
    },
    [key:string]: {
      [key:string]: string,
    },
  },
  "cognitoData": {
    "userPoolId": string,
    "clientId": string
  },
  "serviceTiers": [{
    "id": number,
    "name": string,
    "description": string,
    "npiCodeId": string,
    "customerId": number,
    "active": number,
    "cost": number,
    "duration": number,
    "deviceCategoryId": string,
    "deviceTypeMask": number,
    "autoConnect": number,
    "alternateCost": number,
    "metadata": {}
  }],
  "bypassServiceTiers": [{
    "id": number,
    "name": string,
    "description": string,
    "npiCodeId": string,
    "customerId": number,
    "active": number,
    "cost": number,
    "duration": number,
    "deviceCategoryId": string,
    "deviceTypeMask": number,
    "autoConnect": number,
    "alternateCost": number,
    "metadata": {}
  }],
  "languages": {
    default: string,
    [key:string]: any
    // {
    //   "id": number,
    //   "language": string,
    //   "languageEN": string,
    //   "rtl": string
    // }
  },
  "serviceTierGroupId": number,
  "collectionId": number,
  "media_hash": { [id: number]: string },
  "currencyCode": string
}
