import * as React from 'react';
import * as hasTouch from 'has-touch';
import styled from 'styled-components';
import Pulsate from './Pulsate';

const buildRow = (rowIndex: number) => (
  <div className={`single-row${hasTouch ? '' : ' single-row--desktop'}`} key={`loading-${rowIndex}`}>
    <div className="row-header" children={<div className="empty-title" children={<Pulsate />} />} />
    <div className="row-content">
      {Array.from(Array(30)).map((_, itemIndex) => (
        <a
          key={itemIndex}
          className="card"
          children={<Pulsate style={{animationDelay: `${(itemIndex * 2) / 10}s`}} />}
        />
      ))}
    </div>
  </div>
);

const ScheletonLoader: React.FC<any> = () => {
  return <StyledContainer>{Array.from(Array(10)).map((_, index) => buildRow(index))}</StyledContainer>;
};

const StyledContainer = styled.div`
  display: block;
  margin: 0;
  padding: 0;
`;

export default ScheletonLoader;
