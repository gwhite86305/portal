import * as React from 'react';
import Carousel from '../common/CarouselView/Carousel';
import IterableListRenderer from '../common/IterableRenderer';
import {THUMBNAILS_SIZE, ImageVariantsType} from '../types';
import {MusicProps} from './types';
import * as defaultProps from './defaultProps.json';
const {HEIGHT, WIDTH} = THUMBNAILS_SIZE.AUDIO;

const imgPlaceholder = 'http://via.placeholder.com/' + HEIGHT + 'x' + WIDTH;

const Music: React.FC<MusicProps> = ({onItemInViewPort, onItemClick, items}) => {
  return (
    <IterableListRenderer>
      {items.map(({items, category, uid}) => (
        <Carousel
          key={uid}
          onItemInViewPort={onItemInViewPort}
          imgPlaceholder={imgPlaceholder}
          thumbSize={THUMBNAILS_SIZE.AUDIO}
          categoryName={category}
          items={items}
          onClick={onItemClick}
          styleProps={{is1To1Ratio: true, thumbnailHeight: HEIGHT}}
          imageVariant={ImageVariantsType.audiothumb}
        />
      ))}
    </IterableListRenderer>
  );
};

Music.defaultProps = defaultProps as Partial<MusicProps>;

export default Music;
