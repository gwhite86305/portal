import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getFullFlightState} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, connected: false};
  if (state.device && state.profile) {
    props.fullFlight = getFullFlightState(state.device, state.profile);
  }
  return props;
})
export default class HideOnFullFlightService extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.fullFlight ? null : this.props.children;
  }
}
