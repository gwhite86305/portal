import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {RichText as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {withContent} from 'component-utils/dist/components/ContentAsset';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
}

const RichTextWrapper = styled.div`
  min-height: 1rem;
  ${renderThemeProperty('fontFamily', 'font-family')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('zIndex', 'z-index')};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
`;

class RichText extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme, content} = this.props;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <RichTextWrapper className="core-richtext-component">
          {content && content.html ? <div dangerouslySetInnerHTML={{__html: content.html}} /> : null}
        </RichTextWrapper>
      </PortalComponent>
    );
  }
}

export default withContent(RichText, [{contentKey: 'RichText'}]) as any;
