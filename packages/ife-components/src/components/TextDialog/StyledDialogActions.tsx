import * as React from 'react';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {DialogActions} from '@material-ui/core';

export default styled(DialogActions)`
  ${renderThemeProperty('background', 'background')};
  margin: 0;
  padding: 8px 4px;
`;
