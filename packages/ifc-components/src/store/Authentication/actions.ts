import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {ActionCreators as DeviceActionCreator} from '../device/actions';
import {ShowModal} from 'core-components/dist/store/Modal/actions';
import {
  IncrementPendingRequestsCounter,
  DecrementPendingRequestsCounter
} from 'core-components/dist/store/Loading/actions';
const {generateMessage} = utils.APIError;
// Action Creators
export const ActionCreators = {
  aaAuthStarted: new ActionCreator<'aaAuthStarted', any>('aaAuthStarted'),
  clearMessageAA: new ActionCreator<'clearMessageAA', any>('clearMessageAA'),
  logoutAA: new ActionCreator<'logoutAA', any>('logoutAA'),
  aaAuthSuccess: new ActionCreator<'aaAuthSuccess', any>('aaAuthSuccess'),
  aaAuthError: new ActionCreator<'aaAuthError', any>('aaAuthError'),
  aaJwtRetrived: new ActionCreator<'aaJwtRetrived', any>('aaJwtRetrived'),
  aaCheckSubscriptionStarted: new ActionCreator<'aaCheckSubscriptionStarted', any>('aaCheckSubscriptionStarted'),
  aaCheckSubscriptionSuccess: new ActionCreator<'aaCheckSubscriptionSuccess', any>('aaCheckSubscriptionSuccess'),
  aaCheckSubscriptionError: new ActionCreator<'aaCheckSubscriptionError', any>('aaCheckSubscriptionError'),
  aaFetchCardStarted: new ActionCreator<'aaFetchCardStarted', any>('aaFetchCardStarted'),
  aaFetchCardSucccess: new ActionCreator<'aaFetchCardSucccess', any>('aaFetchCardSucccess'),
  aaFetchCardFailed: new ActionCreator<'aaFetchCardFailed', any>('aaFetchCardFailed'),
  gogoPurchaseSubscriptionError: new ActionCreator<'gogoPurchaseSubscriptionError', any>(
    'gogoPurchaseSubscriptionError'
  ),
  gogoPurchaseSubscriptionSuccess: new ActionCreator<'gogoPurchaseSubscriptionSuccess', any>(
    'gogoPurchaseSubscriptionSuccess'
  )
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function americanLoyaltyMemberLogin(
  authUrl: string,
  loginid: string,
  lastname: string,
  password: string,
  bypassModalId: string
) {
  const successCode = 8110001;
  return (dispatch: any) => {
    dispatch(IncrementPendingRequestsCounter());
    dispatch(ActionCreators.aaAuthStarted.create({aaAuth: {waitingForResponse: true}}));
    axios
      .post(authUrl, {
        loginid: loginid,
        password: password,
        lastname: lastname
      })
      .then(response => {
        dispatch(DecrementPendingRequestsCounter());
        if (response.data.body.code == successCode) {
          // Save JWT
          localStorage.setItem('aa_jwt', response.data.body.jwt);
        }
        if (response.data.body.Bypass) {
          dispatch(ShowModal(bypassModalId));
        }
        dispatch(
          ActionCreators.aaAuthSuccess.create({
            aaAuth: {
              ...response.data.body,
              waitingForResponse: false
            }
          })
        );
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.aaAuthError.create({
            aaAuth: {
              ...generateMessage('Could not retrieve response for aa_auth', err),
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function getAAJwt() {
  return (dispatch: any) => {
    let payload = {
      jwt: '',
      errorMessage: {}
    };

    try {
      payload.jwt = localStorage.getItem('aa_jwt') || '';
    } catch (e) {
      payload.errorMessage = generateMessage('Could not retrive aa_jwt value from the localstorage', e);
    }
    dispatch(
      ActionCreators.aaJwtRetrived.create({
        aaAuth: {...payload}
      })
    );
  };
}

function checkSubscription(jwt: string, subscriptionUrl: string) {
  return (dispatch: any, getState: any) => {
    let device = getState().device;
    dispatch(ActionCreators.aaCheckSubscriptionStarted.create({gogoSubscription: {waitingForResponse: true}}));
    axios
      .post(subscriptionUrl, {
        token: jwt,
        flight_number: device.flightNumber,
        origin_icao: device.flightOrigin,
        destination_icao: device.flightDestination,
        errorMessage: {}
      })
      .then(response => {
        dispatch(
          ActionCreators.aaCheckSubscriptionSuccess.create({
            gogoSubscription: {
              ...response.data,
              waitingForResponse: false
            }
          })
        );
      })
      .catch(err => {
        dispatch(
          ActionCreators.aaCheckSubscriptionError.create({
            gogoSubscription: {
              ...generateMessage('Could not retrieve response from check subscription', err),
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function clearMessageAA(aaAuth: any) {
  aaAuth && delete aaAuth['code'] && delete aaAuth['message'];
  return (dispatch: any) => {
    dispatch(ActionCreators.clearMessageAA.create({aaAuth: aaAuth}));
  };
}
export function logoutAA() {
  //TODO: Remove the JWT token when the logout endpoint call is successful.
  localStorage.removeItem('aa_jwt');
  return (dispatch: any) => {
    dispatch(ActionCreators.clearMessageAA.create({aaAuth: {}}));
  };
}

export function getUserCard(userCardApiUrl: string, jwt: string) {
  return (dispatch: any) => {
    dispatch(ActionCreators.aaFetchCardStarted.create({userCard: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());

    axios
      .get(userCardApiUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(response => {
        dispatch(ActionCreators.aaFetchCardSucccess.create({userCard: {waitingForResponse: false}}));
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.aaFetchCardSucccess.create({
            userCard: {
              ...response.data,
              waitingForResponse: false
            }
          })
        );
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.aaFetchCardFailed.create({
            userCard: {
              ...generateMessage('Could not retrieve response for user_card', err),
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function purchaseGogoSubscription(
  purchaseURL: string,
  card: string,
  nameOnTheCard: string,
  email: string,
  subId: number
) {
  return (dispatch: any) => {
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(purchaseURL, {
        card: card,
        nameOnTheCard: nameOnTheCard,
        email: email,
        subId: subId
      })
      .then(response => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.gogoPurchaseSubscriptionSuccess.create({
            gogoSubscriptionPurchase: {
              ...response.data,
              waitingForResponse: false
            }
          })
        );
        dispatch(DeviceActionCreator.waitForServiceTier.create(true));
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.gogoPurchaseSubscriptionError.create({
            gogoSubscriptionPurchase: {
              ...generateMessage('Could not retrieve response for purchase_gogo_subscription', err)
            }
          })
        );
      });
  };
}
