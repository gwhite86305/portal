import * as React from 'react';
import {utils} from 'component-utils';
import {AaPromo as ExternalInterface} from '../common/ComponentsInterface';
import AaPromo, {InternalInterface} from './AaPromo';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
const connectWrapper = utils.connectWrapper.default;
import {
  signupWithVoucher,
  signupWithPurchase,
  validateSSOPromo,
  resetVoucher,
  resetSignup
} from '../../store/WifiServiceOptions/actions';
import {getTierById} from '../../utils/ConnectionUtils';

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onPromoValidate(promoCode: string): any;
  onContinue(promoCode: string, email: string, tierId: number, applyPromo: boolean): any;
  resetPromoAndSignup(): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    let props = {...ownProps};
    props.Authentication = state.Authentication;
    props.device = state.device;
    props.WifiServiceOptions = state.WifiServiceOptions;
    props.tier = getTierById(state.profile, state.WifiServiceOptions.selected.tierId);
    props.currencyCode = state.profile.currencyCode || state.ux.settings.ifc.defaultCurrencyCode;
    props.currencySuffix = state.ux.settings.ifc.currencySuffix;
    props.locale = state.device.language;
    props.minimumFractionDigits = state.ux.settings.ifc.defaultMinimumFractionDigits;
    props.maximumFractionDigits = state.ux.settings.ifc.defaultMaximumFractionDigits;
    return props;
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onPromoValidate: promoCode => {
        dispatch(validateSSOPromo(ownProps.feature.validatePromoEndPoint, promoCode));
      },
      onContinue: (promoCode, email, tierId, applyPromo) => {
        if (applyPromo) {
          dispatch(signupWithVoucher(promoCode, email, tierId, 'sso-promo'));
        } else {
          dispatch(signupWithPurchase(email, tierId));
        }
      },
      resetPromoAndSignup: () => {
        dispatch(resetVoucher());
        dispatch(resetSignup());
      }
    };
  }
)
class AaPromoContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }
  componentDidUpdate(prevProps: any) {
    const {WifiServiceOptions} = this.props;
    if (WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'redirect') {
      window.location.href = WifiServiceOptions.signup && WifiServiceOptions.signup.url;
    }
    if (
      WifiServiceOptions.signup &&
      WifiServiceOptions.signup.action == 'accept' &&
      this.props.device.serviceTierId != prevProps.device.serviceTierId
    ) {
      this.props.history.push(this.props.feature.connectedPage);
    }
  }
  public componentWillUnmount() {
    this.props.resetPromoAndSignup();
  }
  public render() {
    const {intl, componentUid} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.AaPromo.header'},
      {emailNote: 'ifc.AaPromo.emailNote'},
      {promoNote: 'ifc.AaPromo.promoNote'},
      {emailInputLabel: 'ifc.AaPromo.emailInputLabel'},
      {confirmEmailInputLabel: 'ifc.AaPromo.confirmEmailInputLabel'},
      {promoInputLabel: 'ifc.AaPromo.promoInputLabel'},
      {removeButtonText: 'ifc.AaPromo.removeButtonText'},
      {applyButtonText: 'ifc.AaPromo.applyButtonText'},
      {continueButtonText: 'ifc.AaPromo.continueButtonText'},
      {errorMessageOnFormHeader: 'ifc.AaPromo.errorMessageOnFormHeader'},
      {errorMessageInvalidPromo: 'ifc.AaPromo.errorMessageInvalidPromo'},
      {errorMessageInvalidEmail: 'ifc.AaPromo.errorMessageInvalidEmail'},
      {errorMessageEmailRequired: 'ifc.AaPromo.errorMessageEmailRequired'},
      {errorMessageConfirmEmailRequired: 'ifc.AaPromo.errorMessageConfirmEmailRequired'},
      {emailMessageEmailMismatch: 'ifc.AaPromo.emailMessageEmailMismatch'},
      {errorMessageGeneric: 'ifc.AaPromo.errorMessageGeneric'}
    ]);
    let errorDict = {
      errorMessageInvalidPromo: localizedProps.errorMessageInvalidPromo,
      errorMessageOnFormHeader: localizedProps.errorMessageOnFormHeader,
      errorMessageEmailRequired: localizedProps.errorMessageEmailRequired,
      errorMessageInvalidEmail: localizedProps.errorMessageInvalidEmail,
      emailMessageEmailMismatch: localizedProps.emailMessageEmailMismatch,
      errorMessageConfirmEmailRequired: localizedProps.errorMessageConfirmEmailRequired,
      generic: localizedProps.errorMessageGeneric
    };
    return <AaPromo {...this.props} errorDict={errorDict} {...localizedProps} />;
  }
}

export default injectIntl(withRouter(AaPromoContainer));
