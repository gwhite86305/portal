import LayoutInterface from './LayoutInterface';
import {ThemeSchema} from '../components/common/ThemeInterface';

export interface Settings {
  [index: string]: any;
}

interface UXStructure {
  layout: LayoutInterface;
  theme: ThemeSchema;
  settings?: Settings;
  breakpoints?: any;
}

export default UXStructure;
