import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getIfcInitializedStatus} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, uninitialized: true};
  if (state.device && state.profile) {
    props.uninitialized = !getIfcInitializedStatus(state.device, state.profile);
  }
  return props;
})
export default class HideOnIfcUninitialized extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.uninitialized ? null : this.props.children;
  }
}
