import * as React from 'react';

export interface IterableRendererItemProps {
  iterableRendererCallback?: () => void;
}

class IterableRendererItem<P = {}, S = {}> extends React.Component<IterableRendererItemProps & P, S> {
  componentDidMount() {
    if (this.props.iterableRendererCallback) {
      this.props.iterableRendererCallback();
    }
  }
}

export default IterableRendererItem;
