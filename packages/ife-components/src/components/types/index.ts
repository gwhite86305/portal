export interface IThumbnailSize {
  HEIGHT: number;
  WIDTH: number;
}

export const THUMBNAILS_SIZE = {
  VIDEO: {
    HEIGHT: 220,
    WIDTH: 144
  },
  AUDIO: {
    HEIGHT: 229,
    WIDTH: 229
  }
};

export enum ImageVariantsType {
  original = 'original',
  audio = 'audio',
  audiothumb = 'audiothumb',
  poster = 'poster',
  posterthumb = 'posterthumb',
  video = 'video',
  videothumb = 'videothumb'
}

export interface ImageVariantItem {
  url: string;
  width: number;
  height: number;
}

export interface ImageVariants {
  [key: string]: ImageVariantItem | undefined;
  original: ImageVariantItem;
  audio1x?: ImageVariantItem;
  audio2x?: ImageVariantItem;
  audio3x?: ImageVariantItem;
  audiothumb1x?: ImageVariantItem;
  audiothumb2x?: ImageVariantItem;
  audiothumb3x?: ImageVariantItem;
  poster1x?: ImageVariantItem;
  poster2x?: ImageVariantItem;
  poster3x?: ImageVariantItem;
  posterthumb1x?: ImageVariantItem;
  posterthumb2x?: ImageVariantItem;
  posterthumb3x?: ImageVariantItem;
  video1x?: ImageVariantItem;
  video2x?: ImageVariantItem;
  videothumb1x?: ImageVariantItem;
  videothumb2x?: ImageVariantItem;
  videothumb3x?: ImageVariantItem;
}
