import * as React from 'react';
import styled from 'styled-components';

const StyledButtom = styled.button`
  -webkit-appearance: none;
  align-items: stretch;
  background-color: transparent !important;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: 100%;
  margin: 0;
  padding: 0;
  width: 100%;

  .line {
    background-color: white;
    height: 2px;
    width: 100%;
  }

  .second,
  .third {
    margin-top: 5px;
  }
`;
const HamburgerMenuOpenButton: React.FC<{onClick(): any}> = ({onClick}) => (
  <StyledButtom onClick={onClick}>
    <div className="line first" />
    <div className="line second" />
    <div className="line third" />
  </StyledButtom>
);

export default HamburgerMenuOpenButton;
