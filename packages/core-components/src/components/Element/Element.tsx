import * as React from 'react';
import { PortalComponent, utils } from 'component-utils';
import { Text as ExternalInterface } from '../common/ComponentsInterface';
import { withLocaleProps, localize } from 'component-utils/dist/components/Localize';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const { applyMediaQueriesForStyle, renderThemeProperty } = utils.styledComponents;

export interface InternalInterface {
  className?: string;
}

const ElementWrapper = (props: any) => {
  const Container = styled(props.tag)`
    ${renderThemeProperty('backgroundColor', 'background-color')};
    ${renderThemeProperty('color', 'color')};
    ${renderThemeProperty('fontFamily', 'font-family')};
    ${renderThemeProperty('fontSize', 'font-size')};
    ${renderThemeProperty('letterSpacing', 'letter-spacing')};
    ${renderThemeProperty('zIndex', 'z-index')};
    ${renderThemeProperty('fontWeight', 'font-weight')};
    ${renderThemeProperty('wordWrap', 'word-wrap')};
    ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.align, props.theme, true)};
    ${renderThemeProperty('shadow', 'text-shadow')};
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
    ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme)}
  `;

  return <Container className="${props.class}">{props.localizedText}{props.children} </Container>
}

class Element extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const { componentTheme, feature, localizedText } = this.props as any;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <ElementWrapper class="core-text-component" {...this.props} tag={feature.tag} localizedText={localizedText} />
      </PortalComponent>
    );
  }
}

export default withLocaleProps(Element, [{ localizedText: 'core.Element.defaultText' }]) as any;
