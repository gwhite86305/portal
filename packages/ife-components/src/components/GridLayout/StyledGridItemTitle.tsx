import styled from 'styled-components';

export interface StyledGridItemTitleProps {}

export default styled.div<StyledGridItemTitleProps>`
  background: rgba(89, 88, 84, 0.7);
  color: white;
  position: absolute;
  width: 100%;
  height: 40px;
  font-size: 1.8rem;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  padding-left: 1rem;
  box-sizing: border-box;
  text-transform: uppercase;
`;
