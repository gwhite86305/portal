#! /bin/bash
CUSTOMER=$1
. ../../env/test.${CUSTOMER}.env
amen_mock_server start
npm run start:bg
sleep 20
jest --forceExit --verbose test/${CUSTOMER}.spec.js
EXIT_CODE=$?
amen_mock_server stop
npm run stop:bg

exit $EXIT_CODE
