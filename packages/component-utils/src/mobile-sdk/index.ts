import {WebAppInterface, AndroidAppInterface, IosAppInterface} from './appInterface';
import {utils} from '../utils';

const platforms: WebAppInterface[] = [new AndroidAppInterface(), new IosAppInterface()];
const mobileActions: string[] = ['updateMessages'];

const mobileReduxMiddleware = () => (next: any) => (action: {type: string; payload: any}) => {
  if (!utils.mobileSdk.isUsingMobileSdk()) {
    return next(action);
  }

  const {type, payload} = action;
  let mobileAction = {type, payload};
  const isMobileAction = type.includes('mobile_');
  if (mobileActions.indexOf(type) !== -1 || isMobileAction) {
    mobileAction = {
      type: isMobileAction ? type : `mobile_${type}`,
      payload
    };
    for (let platform of platforms) {
      platform.update(mobileAction.type, mobileAction.payload);
    }
  }
  return next(mobileAction);
};

export default mobileReduxMiddleware;
