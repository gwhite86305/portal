import {ThemeSchema} from 'component-utils/dist/components/common/ThemeInterface';
import {State as ISystemState} from 'component-utils/dist/store/system/reducer';
import * as UXActions from 'component-utils/dist/store/ux/actions';
import LayoutInterface from 'component-utils/dist/types/LayoutInterface';
import PortalToBuilderMessageType, {
  PortalToBuilderMessageData
} from 'component-utils/dist/types/PortalToBuilderMessageType';
import {Settings} from 'component-utils/dist/types/UXStructure';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import FontLoader from 'component-utils/dist/utils/FontLoader';
import storeAccess from 'component-utils/dist/utils/storeAccess';
import {equals} from 'ramda';
import * as React from 'react';
import {withRouter} from 'react-router';
import {AnyAction} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {IRootState} from '../store';
import {AppProviders, getAppTemplateAndBody, getSystemSnackbar, IntlAndThemeProviders} from '../utils/AppUtils';
import {componentLibsUsed, loadRequiredComponents, runComponentLibInitializers} from '../utils/ComponentLoader';
import {epicMiddleware} from '../utils/configureStore';
import {InsertPosition} from './common/builder/BuilderComponentOverlay';
import BuilderEventListener from './common/builder/BuilderEventListener';
import {History} from 'history';

export interface StateProps {
  layout: LayoutInterface | null;
  theme: ThemeSchema;
  system: ISystemState;
  settings: Settings;
  builderSelectedComponentUid: string;
  isBuilderInitialized: boolean;
  userSelectedLocale: string | null;
}

export interface DispatchProps {
  sendToBuilder(sendPostMessageData: PortalToBuilderMessageData): void;
}

export type AppProps = StateProps & DispatchProps;

const mapStateToProps = (state: IRootState): StateProps => {
  return {
    layout: state.ux.layout,
    theme: state.ux.theme,
    settings: state.ux.settings,
    system: state.system,
    builderSelectedComponentUid: state.ux.builderSelectedComponentUid,
    isBuilderInitialized: state.ux.isBuilderInitialized,
    userSelectedLocale: state.ux.userSelectedLocale
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<IRootState, {}, AnyAction>): DispatchProps => ({
  sendToBuilder: (sendPostMessageData: PortalToBuilderMessageData) =>
    dispatch(UXActions.sendToBuilder(sendPostMessageData))
});

@connectWrapper(mapStateToProps, mapDispatchToProps)
class AppBuilderContainer extends React.Component<AppProps> {
  public componentWillMount() {
    if (!FontLoader.hasFontThemeApplied()) {
      FontLoader.applyFontTheme(this.props.theme);
    }
  }

  public componentDidMount() {
    this.props.sendToBuilder({
      type: PortalToBuilderMessageType.portalReady,
      data: ''
    });
  }

  // handle if component props changed while mounted (i.e. can only occur from portal builder real-time editing)
  public componentWillReceiveProps(newProps: AppProps) {
    if (this.props.layout === null || newProps.layout === null) {
      return;
    }

    // only re-render if not as a result of component selection
    if (this.props.builderSelectedComponentUid === newProps.builderSelectedComponentUid) {
      if (FontLoader.hasFontChanged(this.props.theme, newProps.theme)) {
        FontLoader.applyFontTheme(newProps.theme);
      }

      if (!equals(componentLibsUsed(this.props.layout), componentLibsUsed(newProps.layout))) {
        // TODO for now this won't wrap lib providers around builder code - ok for now
        runComponentLibInitializers(null, storeAccess.get(), newProps.settings, newProps.layout, epicMiddleware);
      }

      const loaders = [loadRequiredComponents(newProps.layout)];
      Promise.all(loaders).then(([loaded]) => {
        if ((loaded as any).length) {
          console.info('Perform UX re-render, as new component available');
          this.forceUpdate(); // layout state is changed, hurrah: but component not yet loaded, so must force after available
        }
      });
    }
  }

  public render() {
    const {system, builderSelectedComponentUid, layout, theme, userSelectedLocale} = this.props;
    if (!layout) {
      return null;
    }
    const {components} = layout;
    const selectedComponent = builderSelectedComponentUid ? components[builderSelectedComponentUid] : undefined;
    const forceSnackbarRender = selectedComponent && selectedComponent.componentName === 'SystemSnackbar';

    return (
      <IntlAndThemeProviders theme={theme}>
        <React.Fragment>
          {system.systemSettings ? (
            <BuilderEventListener
              layout={layout}
              userSelectedLocale={userSelectedLocale || ''}
              componentSelected={builderSelectedComponentUid}
              onComponentSelected={componentUid => {
                this.props.sendToBuilder({
                  type: PortalToBuilderMessageType.componentSelected,
                  data: componentUid
                });
              }}
              onComponentKeyPress={e => {
                this.props.sendToBuilder({
                  type: PortalToBuilderMessageType.keyPress,
                  data: e
                });
              }}
              onComponentInsert={(componentUid: string, position: InsertPosition) => {
                this.props.sendToBuilder({
                  type: PortalToBuilderMessageType.componentInsertRequest,
                  data: {componentUid, position}
                });
              }}
            >
              {getAppTemplateAndBody(layout)}
              {system.systemMessage || forceSnackbarRender ? getSystemSnackbar(this.props) : null}
            </BuilderEventListener>
          ) : null}
        </React.Fragment>
      </IntlAndThemeProviders>
    );
  }
}

const AppContainerWithRouter = withRouter(AppBuilderContainer as any);

export interface AppBuilderProps {
  history: History;
}

/*
 * Note: below is only ever rendered *once*, all other hierarchy re-renders occur at AppBuilderContainer as the root
 * */
const AppBuilder: React.FunctionComponent<AppBuilderProps> = (props: AppBuilderProps) => (
  <AppProviders {...props}>
    <AppContainerWithRouter {...props} />
  </AppProviders>
);

export default AppBuilder;
