import * as React from 'react';
import FontLoader from './FontLoader.js';
import styled, {ThemeProvider} from 'styled-components';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {IntlProvider} from 'react-intl';
import {ThemeSchema} from '../components/common/ThemeInterface';

export const mockTheme: ThemeSchema = {
  activeIconColor: '#E40000',
  gutter: '16px',
  altBackgroundColor: '#013064',
  appFontFamily: "'SourceSansPro'",
  appFontFiles: [
    {file: "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');", weight: '400'},
    {file: "url('./fonts/source-sans-pro/SourceSansPro-Semibold.otf') format('opentype');", weight: '600'}
  ],
  buttonRadius: '40px',
  defaultFontSize: '16px',
  fontWeight: 'normal',
  formErrorColor: 'red',
  modalRadius: '4px',
  primaryBackgroundColor: '#ffffff',
  primaryButtonColor: '#25b2ef',
  primaryButtonActiveColor: '#9be1ff',
  primaryButtonHoverColor: '#64cefb',
  primaryButtonHoverTextColor: '#202e39',
  primaryButtonTextColor: '#ffffff',
  primaryFontSize: '1rem',
  primaryIconColor: '#ffffff',
  primaryIconDisabledColor: '#9c9c9c',
  primaryTextColor: '#202e39',
  primaryTextShadow: 'none',
  primaryFocusColor: '#0073CB',
  primaryFocusBorder: '1px',
  secondaryFontFamily: "'UniNeue'",
  secondaryFontFiles: [
    {file: "url('./fonts/uni-neue/unineue-regular.otf') format('opentype');", weight: '400'},
    {file: "url('./fonts/uni-neue/unineue-bold.otf') format('opentype');", weight: '600'}
  ],
  fontIconFontFamily: "'FontAwesome'",
  fontIconFiles:
    "url('a45540e0-7248-4b64-979a-b40d0f50a3d8') format('woff2'), url('2f186296-202f-4423-896e-9304a5953922') format('woff'), url('858050ed-298d-432d-8803-bdb6d388b1b3') format('truetype'), url('0b8fac48-7534-494b-bccf-dcce972a6092') format('svg');",
  fontIconCss: 'edb7cbb7-eb0b-486f-81d3-1db4f15239db',
  secondaryBackgroundColor: '#202e39',
  secondaryButtonColor: '#1694ca',
  secondaryButtonActiveColor: '#015274',
  secondaryButtonHoverColor: '#e6e6e6',
  secondaryButtonHoverTextColor: '#333333',
  secondaryButtonTextColor: '#ffffff',
  secondaryFontSize: '1.25rem',
  secondaryTextShadow: '0px 2px 2px rgba(0, 0, 0, 0.3)',
  secondaryIconColor: '#202e39',
  secondaryIconDisabledColor: '#9c9c9c',
  secondaryTextColor: '#1694CA',
  secondaryFocusColor: '#ffffff',
  tertiaryBackgroundColor: '#bed733',
  tertiaryTextColor: '#ffffff',
  tertiaryFontSize: '1.5rem',
  tertiaryIconColor: '#1694CA',
  tertiaryFocusColor: '#6DACE3',
  quaternaryBackgroundColor: '#f2f2f2',
  quaternaryIconColor: '#25b2ef',
  quaternaryFontSize: '1.875rem',
  quinaryBackgroundColor: '#25B2EF',
  quinaryFontSize: '3rem',
  senaryBackgroundColor: '#465967',
  primaryLinkColor: '#202e39',
  primaryLinkActiveColor: '#202e39'
};
export const mockThemeNeos: ThemeSchema = {
  activeIconColor: '#E40000',
  gutter: '16px',
  altBackgroundColor: '#013064',
  appFontFamily: "'neos'",
  appFontFiles: [
    {
      file: "url('./fonts/neos/neos.woff2') format('woff2');",
      weight: '400'
    },
    {
      file: "url('./fonts/neos/neos.woff2') format('woff2');",
      weight: '600'
    }
  ],
  secondaryFontFamily: "'neos2'",
  secondaryFontFiles: [
    {
      file: "url('./fonts/neos/neos.woff2') format('woff2');",
      weight: '400'
    },
    {
      file: "url('./fonts/neos/neos.woff2') format('woff2');",
      weight: '600'
    }
  ],
  fontIconFontFamily: "'FontAwesome'",
  fontIconFiles:
    "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./fonts/font-awesome/fonts/fontawesome-webfont.woff') format('woff'), url('./fonts/font-awesome/fonts/fontawesome-webfont.ttf') format('truetype'), url('./fonts/font-awesome/fonts/fontawesome-webfont.svg#font') format('svg');",
  fontIconCss: './fonts/font-awesome/css/font-awesome.css',
  fontIconUsesLigature: false,
  fontIconBaseClass: 'fa',
  buttonRadius: '40px',
  defaultFontSize: '16px',
  fontWeight: 'normal',
  formErrorColor: '#DF2927',
  modalRadius: '4px',
  primaryBackgroundColor: '#ffffff',
  primaryDividerColor: '#5C5757',
  primaryButtonColor: '#0073CB',
  primaryButtonActiveColor: '#BDBDBD',
  primaryButtonHoverColor: '#025FA7',
  primaryButtonHoverTextColor: '#FFFFFF',
  primaryButtonTextColor: '#ffffff',
  primaryFontSize: '1rem',
  primaryIconColor: '#ffffff',
  primaryIconDisabledColor: '#9c9c9c',
  primaryTextColor: '#1A1919',
  primaryTextShadow: 'none',
  primaryFocusColor: '#0073CB',
  primaryFocusBorder: '1px',
  secondaryBackgroundColor: '#1A1919',
  secondaryButtonColor: '#FFFFFF',
  secondaryButtonActiveColor: '#BDBDBD',
  secondaryButtonHoverColor: '#A9D2F0',
  secondaryButtonHoverTextColor: '#333333',
  secondaryButtonTextColor: '#1A1919',
  secondaryFontSize: '1.25rem',
  secondaryTextShadow: '0px 2px 2px rgba(0, 0, 0, 0.3)',
  secondaryIconColor: '#202e39',
  secondaryIconDisabledColor: '#9c9c9c',
  secondaryTextColor: '#0073CB',
  secondaryFocusColor: '#ffffff',
  tertiaryBackgroundColor: '#0073CB',
  tertiaryTextColor: '#ffffff',
  tertiaryFontSize: '1.5rem',
  tertiaryIconColor: '#1694CA',
  tertiaryFocusColor: '#6DACE3',
  quaternaryBackgroundColor: '#6DACE3',
  quaternaryIconColor: '#25b2ef',
  quaternaryFontSize: '1.875rem',
  quinaryBackgroundColor: '#25B2EF',
  quinaryFontSize: '3rem',
  senaryBackgroundColor: '#6DACE3',
  highlightTextFont: '24px',
  legalMenuIconSize: '10px',
  primaryLinkColor: '#1A1919',
  primaryLinkActiveColor: '#1A1919'
};

export const mockThemeIcelandair: ThemeSchema = {
  activeIconColor: '#E40000',
  gutter: '20px',
  altBackgroundColor: '#013064',
  appFontFamily: "'Altitude'",
  appFontFiles: [
    {
      file: "url('./cms-served-assets/fonts/altitude/AltitudeRegular.ttf') format('truetype');",
      weight: '400'
    },
    {
      file: "url('./cms-served-assets/fonts/altitude/AltitudeMedium.ttf') format('truetype');",
      weight: '500'
    },
    {
      file: "url('./cms-served-assets/fonts/altitude/AltitudeBold.ttf') format('truetype');",
      weight: '600'
    }
  ],
  secondaryFontFamily: "'UniNeue'",
  secondaryFontFiles: [
    {
      file: "url('./cms-served-assets/fonts/uni-neue/unineue-regular.otf') format('opentype');",
      weight: '400'
    },
    {
      file: "url('./cms-served-assets/fonts/uni-neue/unineue-bold.otf') format('opentype');",
      weight: '600'
    }
  ],
  fontIconFontFamily: "'FontAwesome'",
  fontIconFiles:
    "url('./cms-served-assets/fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./cms-served-assets/fonts/font-awesome/fonts/fontawesome-webfont.woff') format('woff'), url('./cms-served-assets/fonts/font-awesome/fonts/fontawesome-webfont.ttf') format('truetype'), url('./cms-served-assets/fonts/font-awesome/fonts/fontawesome-webfont.svg#font') format('svg');",
  fontIconCss: './cms-served-assets/fonts/font-awesome/css/font-awesome.css',
  fontIconUsesLigature: false,
  fontIconBaseClass: 'fa',
  buttonRadius: '22.5px',
  defaultFontSize: '16px',
  fontWeight: 'normal',
  formErrorColor: 'red',
  modalRadius: '4px',
  primaryBackgroundColor: '#ffffff',
  primaryButtonColor: '#003A7D',
  primaryButtonActiveColor: '#F2F2F2',
  primaryButtonHoverColor: '#266AB9',
  primaryButtonHoverTextColor: '#ffffff',
  primaryButtonTextColor: '#ffffff',
  primaryButtonInactiveColor: '#f2f2f2',
  primaryFontSize: '1rem',
  primaryIconColor: '#A9A9A9',
  primaryIconDisabledColor: '#9c9c9c',
  primaryLinkColor: '#5E5E5E',
  primaryLinkActiveColor: '#5E5E5E',
  primaryTextColor: '#5E5E5E',
  primaryTextShadow: 'none',
  secondaryBackgroundColor: '#EAEAEA',
  secondaryButtonColor: '#1694ca',
  secondaryButtonActiveColor: '#BDBDBD',
  secondaryButtonHoverColor: '#EAEAEA',
  secondaryButtonHoverTextColor: '#003A7D',
  secondaryButtonTextColor: '#003A7D',
  secondaryFontSize: '1.125rem',
  secondaryTextShadow: '0px 2px 2px rgba(0, 0, 0, 0.3)',
  secondaryIconColor: '#444444',
  secondaryIconDisabledColor: '#9c9c9c',
  secondaryTextColor: '#444444',
  tertiaryBackgroundColor: '#F8F8F8',
  tertiaryTextColor: '#ffffff',
  tertiaryFontSize: '1.375rem',
  tertiaryIconColor: '#1694CA',
  quaternaryTextColor: '#999999',
  quaternaryBackgroundColor: '#CDCDCD',
  quaternaryIconColor: '#25b2ef',
  quaternaryFontSize: '1.625rem',
  quinaryBackgroundColor: '#25B2EF',
  quinaryFontSize: '2rem',
  senaryBackgroundColor: '#465967',
  primaryFocusColor: '#5E5E5E',
  secondaryFocusColor: '#444444',
  tertiaryFocusColor: '#003A7D',
  primaryFocusBorder: '1px',
  primaryDividerColor: '#CDCDCD'
};

export const mockMessages = {
  'common.Error.button.home': 'Home',
  'common.Error.header.title': 'An Error Occurred',
  'common.SystemSnackbar.title.network': 'A network issue has occurred',
  'common.SystemSnackbar.message.network': 'Media playback is disabled',
  'ifc.ConnectivitySimple.label.buyconnectivity': 'Sign in to Fly-Fi or Buy Connectivity',
  'ifc.ConnectivitySimple.button.getconnected': 'Get Connected',
  'ifc.ConnectivitySignIn.button.disconnect': 'Disconnect',
  'ifc.ConnectivitySignIn.label.youareconnected': 'You are connected!',
  'ifc.ConnectivitySignIn.label.notamember': 'Not a member? Sign Up to get Free Inflight Connectivity',
  'ifc.ConnectivitySignIn.input.username': 'Username',
  'ifc.ConnectivitySignIn.input.password': 'Password',
  'ifc.ConnectivitySignIn.header.signin': 'Sign In',
  'ifc.ConnectivitySignIn.button.signin': 'Sign In',
  'ife.IFEMusic.label.trackscount': '({trackCount, plural, =0 {no tracks} one {# track} other {# tracks}})',
  'ife.IFEMusic.label.addallplaylist': 'Add All',
  'ife.IFEMusic.label.audiotitle': '{title} by {albumTitle}',
  'ife.IFEMovie.label.genre': 'Genre',
  'ife.IFEMovie.label.classification': 'Classificiation',
  'ife.IFEMovie.button.watchnow': 'Watch now',
  'ife.IFEMovie.button.trailer': 'Trailer',
  'ife.IFEMovie.button.addtoplaylist': 'Add to playlist',
  'ife.IFEPlaylist.button.clear': 'Clear',
  'ife.IFEPlaylist.button.close': 'Close',
  'ife.IFEPlaylist.label.title': 'Playlist',
  'ife.IFEPlaylist.label.audiotitle': '{title} by {albumTitle}',
  'ife.IFEMusic.error.title.track': 'Could not play track',
  'ife.IFEMusic.error.message.track': 'An error occurred trying to play audio',
  'ife.IFESettings.button.login': 'Login',
  'ife.IFESettings.button.apply': 'Apply',
  'ife.IFESettings.label.title': 'Settings',
  'ife.IFESettings.label.language': 'Language',
  'ife.IFESettings.label.currency': 'Currency',
  'ife.IFESettings.label.contentRestrictions': 'Content restrictions',
  'ife.IFESettings.label.seat': 'Seat',
  'ife.IFESettings.label.ageUnlockPwdPlaceholderEnter': 'Enter unlock password',
  'ife.IFESettings.label.ageUnlockPwdPlaceholderCreate': 'Create unlock password (min 4 chars)',
  'ife.IFESettings.label.seatingPlaceholder': '99X'
};
export const mockMessagesNeos = {
  'ifc.Weather.clickable.text': 'Explore Global Forecasts',
  'ifc.WifiNotificationScroll.up.message': 'Connected - In-flight Wi-Fi is currently available. ',
  'ifc.WifiNotificationScroll.down.message': 'Disconnected - In-flight Wi-Fi is unavailable. Please try again later.',
  'ifc.UserAgreement.notice.header': 'IMPORTANT',
  'ifc.UserAgreement.notice.text': 'PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION.',
  'ifc.UserAgreement.motif.text': 'User Agreement',
  'ifc.UserAgreement.effectivedate.text': 'Effective Date: --/--/----',
  'ifc.UserAgreement.footer.text':
    "By checking this box and selecting Agree you accept company's Terms of Service Agreement and Consent Declaration, and you also accept company’s Privacy Policy and Acceptable Use Policy.",
  'ifc.UserAgreement.footer.button.label': 'AGREE TO TERMS',
  'ifc.ServiceDisruption.header.text': 'Important Notice:',
  'ifc.ServiceDisruption.disruptionalert.text':
    'Wi-Fi is currently unavailable. We apologize for the inconvenience. Meanwhile, you can still enjoy complimentary Wireless In-Flight Entertainment (IFE) by clicking one of the media icons on the homepage.',
  'ifc.WifiServiceOptions.header.text': '',
  'ifc.WifiServiceOptions.body.text': '',
  'ifc.FYI.header.text': 'Title',
  'ifc.FYI.subHeader.text': ['Description'],
  'ifc.FYI.button.text': 'Buttton',
  'ifc.FYI.altTextForImage.text': ''
};

const AppContainer = styled.div`
  font-family: ${(props: any) => props.theme.appFontFamily};
  font-size: ${(props: any) => props.theme.defaultFontSize};
  background-color: ${(props: any) => props.theme.primaryBackgroundColor};
  position: ${({style}: any) => (style && style.position ? style.position : 'absolute')};
  top: 0;
  left: 0;
  margin: 0;
  overflow-y: auto;
  height: 100%;
  width: 100%;
`;

class PortalComponentWrapper extends React.Component<any, any> {
  render() {
    const theme = createMuiTheme();
    FontLoader.applyFontTheme(this.props.mockTheme || mockTheme);
    return (
      <IntlProvider locale={this.props.mockLocale || 'en'} messages={this.props.mockMessages || mockMessagesNeos}>
        <MuiThemeProvider theme={theme}>
          <ThemeProvider theme={this.props.mockTheme || mockTheme}>
            <AppContainer style={this.props.style}>{this.props.children}</AppContainer>
          </ThemeProvider>
        </MuiThemeProvider>
      </IntlProvider>
    );
  }
}

export default PortalComponentWrapper;
