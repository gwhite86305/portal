import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {ActionCreators as DeviceActionCreator} from '../device/actions';

import {
  IncrementPendingRequestsCounter,
  DecrementPendingRequestsCounter
} from 'core-components/dist/store/Loading/actions';
const {generateMessage} = utils.APIError;
// Action Creators
export const ActionCreators = {
  purchaseOptionSelected: new ActionCreator<'purchaseOptionSelected', any>('purchaseOptionSelected'),
  voucherOptionSelected: new ActionCreator<'voucherOptionSelected', any>('voucherOptionSelected'),
  messagingOptionSelected: new ActionCreator<'messagingOptionSelected', any>('messagingOptionSelected'),
  subscriptionOptionSelected: new ActionCreator<'subscriptionOptionSelected', any>('subscriptionOptionSelected'),
  loginOptionSelected: new ActionCreator<'loginOptionSelected', any>('loginOptionSelected'),
  voucherValidationStart: new ActionCreator<'voucherValidationStart', any>('voucherValidationStart'),
  resetVoucher: new ActionCreator<'resetVoucher', any>('resetVoucher'),
  voucherValidationSuccess: new ActionCreator<'voucherValidationSuccess', any>('voucherValidationSuccess'),
  voucherValidationError: new ActionCreator<'voucherValidationError', any>('voucherValidationError'),
  signupWithVoucherStart: new ActionCreator<'signupWithVoucherStart', any>('signupWithVoucherStart'),
  signupWithVoucherSuccess: new ActionCreator<'signupWithVoucherSuccess', any>('signupWithVoucherSuccess'),
  signupWithVoucherError: new ActionCreator<'signupWithVoucherError', any>('signupWithVoucherError'),
  signupWithPurchaseStart: new ActionCreator<'signupWithPurchaseStart', any>('signupWithPurchaseStart'),
  signupWithPurchaseSuccess: new ActionCreator<'signupWithPurchaseSuccess', any>('signupWithPurchaseSuccess'),
  signupWithPurchaseError: new ActionCreator<'signupWithPurchaseError', any>('signupWithPurchaseError'),
  resetSignup: new ActionCreator<'resetSignup', any>('resetSignup'),
  deviceSwapStart: new ActionCreator<'deviceSwapStart', any>('deviceSwapStart'),
  deviceSwapSuccess: new ActionCreator<'deviceSwapSuccess', any>('deviceSwapSuccess'),
  deviceSwapError: new ActionCreator<'deviceSwapError', any>('deviceSwapError'),
  deviceSwapped: new ActionCreator<'deviceSwapped', any>('deviceSwapped'),
  resetSwapDevice: new ActionCreator<'resetSwapDevice', any>('resetSwapDevice'),
  orderHistoryStart: new ActionCreator<'orderHistoryStart', any>('orderHistoryStart'),
  orderHistorySuccess: new ActionCreator<'orderHistorySuccess', any>('orderHistorySuccess'),
  orderHistoryError: new ActionCreator<'orderHistoryError', any>('orderHistoryError'),
  signupFreeStart: new ActionCreator<'signupFreeStart', any>('signupFreeStart'),
  signupFreeSuccess: new ActionCreator<'signupFreeSuccess', any>('signupFreeSuccess'),
  signupFreeError: new ActionCreator<'signupFreeError', any>('signupFreeError'),
  signupWithMessagingStart: new ActionCreator<'signupWithMessagingStart', any>('signupWithMessagingStart'),
  signupWithMessagingSuccess: new ActionCreator<'signupWithMessagingSuccess', any>('signupWithMessagingSuccess'),
  signupWithMessagingError: new ActionCreator<'signupWithMessagingError', any>('signupWithMessagingError')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function purchaseOptionSelected(tierId: number) {
  return (dispatch: any) => {
    let payload = {selected: {method: 'purchase', tierId: tierId, errorMessage: {}}};
    dispatch(ActionCreators.purchaseOptionSelected.create(payload));
  };
}

export function voucherOptionSelected() {
  return (dispatch: any) => {
    let payload = {selected: {method: 'voucher', errorMessage: {}}, signup: {}};
    dispatch(ActionCreators.voucherOptionSelected.create(payload));
  };
}

export function messagingOptionSelected() {
  return (dispatch: any) => {
    let payload = {selected: {method: 'messaging', errorMessage: {}}};
    dispatch(ActionCreators.messagingOptionSelected.create(payload));
  };
}

export function subscriptionOptionSelected(subId: number) {
  return (dispatch: any) => {
    let payload = {selected: {method: 'subscription', subId: subId, errorMessage: {}}};
    dispatch(ActionCreators.purchaseOptionSelected.create(payload));
  };
}

export function loginOptionSelected() {
  return (dispatch: any) => {
    let payload = {selected: {method: 'login', errorMessage: {}}};
    dispatch(ActionCreators.loginOptionSelected.create(payload));
  };
}

export function validateVoucher(voucherCode: string, email: string, promoType: string) {
  const url = 'voucher';
  return (dispatch: any) => {
    dispatch(ActionCreators.voucherValidationStart.create({voucher: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, {promoCode: voucherCode, promoType: promoType})
      .then(response => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.voucherValidationSuccess.create({
            voucher: {
              ...response.data,
              promoCode: voucherCode,
              email: email,
              waitingForResponse: false,
              used: false
            }
          })
        );
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.voucherValidationError.create({
            voucher: {
              ...generateMessage('Could not retrieve response for voucher data', err),
              email: email,
              waitingForResponse: false,
              used: false
            }
          })
        );
      });
  };
}

export function validateSSOPromo(url: string, promoCode: string) {
  /*
  Expected response(Sample) from API 
    Invalid promo code 
      {
          "statusCode": 500,
          "body": {
              "cgid": "b73dbbf9-111e-40ff-8dca-4b2e93b9e8ed",
              "message": "HTTP Error - Unknown error response",
              "promo_is_valid": false,
              "discount": null,
              "discount_type": null,
              "max_discount_amount": null,
              "promo_type": null,
              "error_code": "valid_promo_fu3ll"
          }
      }
    Valid promo code
      {
          "statusCode": 200,
          "body": {
              "cgid": "b0137b06-5fb3-4d8e-ab67-744f6f49504d",
              "message": "Promo code is valid",
              "promo_is_valid": true,
              "discount": "25",
              "discount_type": "percentage",
              "max_discount_amount": "5",
              "promo_type": "regular"
          }
      }
  */
  return (dispatch: any, getState: any) => {
    let device = getState().device;
    dispatch(ActionCreators.voucherValidationStart.create({voucher: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, {
        promo_code: promoCode,
        flight_number: device.flightNumber,
        origin_icao: device.flightOrigin,
        destination_icao: device.flightDestination
      })
      .then(response => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.voucherValidationSuccess.create({
            voucher: {
              ...response.data,
              promoCode: promoCode,
              waitingForResponse: false
            }
          })
        );
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.voucherValidationError.create({
            voucher: {
              ...generateMessage('Could not retrieve response from the validate sso promo endpoint', err),
              waitingForResponse: false,
              promoCode: promoCode
            }
          })
        );
      });
  };
}
export function resetVoucher() {
  return (dispatch: any) => {
    dispatch(ActionCreators.resetVoucher.create({voucher: {}}));
  };
}

export function signupWithVoucher(voucherCode: string, email: string, tierId: number, promoType: string) {
  /* 
 Expected response(Sample) from API
 
 Response (sample) when reject 
 { "action": "reject",
    "device": {
        <device properties>
    },
    "code": "10001"
  }

Response (Sample) when  accept
  {
  "action": "accept",
  "device": {
      <device properties>
  }

Response (Sample) when  redirect
{
  "action": "redirect",
  "url": "http://<vpssite>/aal/payment?id=twUcOsFKQJXCVDH2GC",
  "device": {
     <device properties>
  }
}

*/
  const url = 'signup';
  return (dispatch: any) => {
    const signupdata = {
      serviceTierId: tierId,
      promoCode: voucherCode,
      promoType: promoType,
      terms: true,
      auth: {type: 'email', data: {email: email, jwt: undefined}}
    };
    dispatch(ActionCreators.signupWithVoucherStart.create({signup: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, signupdata)
      .then(response => {
        dispatch(
          ActionCreators.signupWithVoucherSuccess.create({
            signup: {
              ...response.data,
              waitingForResponse: false,
              used: false
            }
          })
        );
        if (response.data.action == 'accept') {
          dispatch(DeviceActionCreator.waitForServiceTier.create(true));
        } else {
          dispatch(DecrementPendingRequestsCounter());
        }
      })
      .catch(err => {
        dispatch(
          ActionCreators.signupWithVoucherError.create({
            signup: {
              ...generateMessage('Could not retrieve response for signup', err),
              waitingForResponse: false,
              used: false
            }
          })
        );
      });
  };
}

export function signupWithMessaging(tierId: number) {
  const url = 'signup';

  return (dispatch: any) => {
    const signupdata = {
      serviceTierId: tierId,
      terms: true,
      auth: {type: 'mac', data: {jwt: undefined}}
    };
    dispatch(ActionCreators.signupWithMessagingStart.create({signup: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, signupdata)
      .then(response => {
        dispatch(
          ActionCreators.signupWithMessagingSuccess.create({
            signup: {
              ...response.data,
              waitingForResponse: false
            }
          })
        );
        if (response.data.action == 'accept') {
          dispatch(DeviceActionCreator.waitForServiceTier.create(true));
        } else {
          dispatch(DecrementPendingRequestsCounter());
        }
      })
      .catch(err => {
        dispatch(
          ActionCreators.signupWithMessagingError.create({
            signup: {
              ...generateMessage('Could not retrieve response for signup', err),
              waitingForResponse: false,
              used: false
            }
          })
        );
      });
  };
}

export function signupWithPurchase(email: string, tierId: number) {
  const url = 'signup';
  return (dispatch: any) => {
    dispatch(ActionCreators.signupWithPurchaseStart.create({purchase: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, {terms: true, serviceTierId: tierId, auth: {type: 'email', data: {email: email}}})
      .then(response => {
        dispatch(
          ActionCreators.signupWithPurchaseSuccess.create({
            signup: {
              ...response.data,
              email: email,
              waitingForResponse: false
            }
          })
        );
        if (response.data.action == 'accept') {
          dispatch(DeviceActionCreator.waitForServiceTier.create(true));
        } else {
          dispatch(DecrementPendingRequestsCounter());
        }
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.signupWithPurchaseError.create({
            purchase: {
              ...generateMessage('Could not get response from signup call', err),
              email: email,
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function resetSignup() {
  return (dispatch: any) => {
    dispatch(ActionCreators.resetSignup.create({signup: {}}));
  };
}

export function resetSwapDevice() {
  return (dispatch: any) => {
    dispatch(ActionCreators.resetSwapDevice.create({deviceSwap: {}}));
  };
}

export function requestDeviceSwap(email: string) {
  const url = 'proxy/v1/signin_notification';
  return (dispatch: any) => {
    dispatch(ActionCreators.deviceSwapStart.create({deviceSwap: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, {identity: {type: 'email', data: {email: email}}})
      .then(response => {
        dispatch(
          ActionCreators.deviceSwapSuccess.create({
            deviceSwap: {...response.data, email: email, waitingForResponse: false}
          })
        );
      })
      .catch(err => {
        dispatch(
          ActionCreators.deviceSwapError.create({
            deviceSwap: {
              ...generateMessage('Could not get response from signin_notification call', err),
              email: email,
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function deviceSwapped() {
  return (dispatch: any) => {
    dispatch(ActionCreators.deviceSwapped.create({deviceSwap: {}}));
  };
}

export function requestOrderHistory(email: string) {
  const url = 'proxy/v1/order_history';
  return (dispatch: any) => {
    dispatch(ActionCreators.orderHistoryStart.create({orderHistory: {waitingForResponse: true}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post(url, {identity: {type: 'email', data: {email: email}}})
      .then(response => {
        dispatch(
          ActionCreators.orderHistorySuccess.create({
            orderHistory: {...response.data, email: email, waitingForResponse: false}
          })
        );
      })
      .catch(err => {
        dispatch(
          ActionCreators.orderHistoryError.create({
            orderHistory: {
              ...generateMessage('Could not get response from order_history call', err),
              email: email,
              waitingForResponse: false
            }
          })
        );
      });
  };
}

export function _signupFreeCall(tierId: number) {
  return (dispatch: any) => {
    dispatch(ActionCreators.signupFreeStart.create({free: {waitingForResponse: true, serviceTierId: tierId}}));
    dispatch(IncrementPendingRequestsCounter());
    axios
      .post('signup', {terms: true, serviceTierId: tierId, auth: {type: 'mac'}})
      .then(response => {
        dispatch(
          ActionCreators.signupFreeSuccess.create({
            free: {waitingForResponse: false, serviceTierId: tierId}
          })
        );
        dispatch(DeviceActionCreator.waitForServiceTier.create(true));
      })
      .catch(err => {
        dispatch(DecrementPendingRequestsCounter());
        dispatch(
          ActionCreators.signupFreeError.create({
            free: {
              ...generateMessage('Could not get response from signup call', err),
              waitingForResponse: false
            }
          })
        );
      });
  };
}
