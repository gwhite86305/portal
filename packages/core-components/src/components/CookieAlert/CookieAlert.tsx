import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {CookieAlert as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  onClose?: any;
}

const CookieAlertThemeWrapper = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  display: flex;
  .closeButton {
    cursor: pointer;
    /* The responsiveImage component add margin on both the image and on the outer div */
    /* That's why the margin property of that component is not used */
    img {
      ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.closeIconMargin, props.theme)};
    }
  }
`;

class CookieAlert extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme} = this.props;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <CookieAlertThemeWrapper className="cookieAlert-component">
          {this.props.children}
          <div onClick={this.props.onClose} className="closeButton">
            <ResponsiveImage
              altText={this.props.closeButtonAltText}
              image={componentTheme && componentTheme.closeIcon}
              imageWidth={componentTheme && componentTheme.closeIconWidth}
            />
          </div>
        </CookieAlertThemeWrapper>
      </PortalComponent>
    );
  }
}

export default CookieAlert;
