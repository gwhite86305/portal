import {utils} from 'component-utils';
import {APP_DOM_ID} from 'component-utils/dist/types/Constants';
import * as React from 'react';
import {AudioItem, CATEGORIES} from '../../store/audio/types';
import {VideoItem} from '../../store/videos/types';
import Playlist from './Playlist';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import {PlaylistContainerProps, PlaylistContainerState} from './types';
import {audioPlayer, playAudioPlayer, pauseAudioPlayer} from '../../utils/audioPlayer';
import {forkJoin, from} from 'rxjs';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class PlaylistContainer extends React.Component<PlaylistContainerProps, PlaylistContainerState> {

  private mountPoint = document.getElementById(APP_DOM_ID);

  public state: PlaylistContainerState = {
    isModalOpen: false,
    shouldInitialize: false
  };

  componentDidMount() {
    const {setPlayerPage, feature} = this.props;
    this.props.loadPlaylist();

    if (feature && feature.playerPage) {
      setPlayerPage(feature.playerPage);
    }
  }

  componentDidUpdate(prevProps: PlaylistContainerProps) {
    const {
      feature: {playerPage},
      setPlayerPage
    } = this.props;

    if (playerPage && playerPage !== prevProps.feature.playerPage) {
      setPlayerPage(playerPage);
    }
  }

  render() {
    const {items, currentItem, removeAllItems, removeItem} = this.props;
    const isPlaying = !audioPlayer.paused;
    return (
      <Playlist
        {...this.props}
        currentItem={currentItem}
        isModalOpen={this.state.isModalOpen}
        isPlaying={utils.mobileSdk.isUsingMobileSdk() ? this.props.isPlaying : isPlaying}
        items={items}
        mountPoint={this.mountPoint}
        onClearPlaylist={removeAllItems}
        onItemPlay={this.handleItemPlay}
        onItemPause={this.handleItemPause}
        onRemoveItem={removeItem}
        toggleModalVisibility={this.toggleModalVisibility}
      />
    );
  }

  private toggleModalVisibility = () => {
    this.setState((prevState: PlaylistContainerState) => ({isModalOpen: !prevState.isModalOpen}));
  };

  private handleItemPlay = (item: VideoItem | AudioItem) => {
    playAudioPlayer();
    this.props.trackPlay(item);
    return this.props.playItem(item, this.props.feature.playerPage as string);
  };

  private handleItemPause = () => {
    pauseAudioPlayer();
    return this.props.pauseCurrentInPlaylist();
  };
}
