const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const MediaTranslatorPlugin = require('media-translator-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const gitRevisionPlugin = new GitRevisionPlugin();

const node_dir = __dirname + '/node_modules';

let mediaAssetNumber = 7000;

module.exports = [
  {
    name: 'production-app',
    mode: 'production',
    entry: {
      1: './src/index.tsx',
    },
    output: {
      path: path.join(__dirname, 'dist'),
      filename: '[name]',
      chunkFilename: '[name]',
      publicPath: ''
    },

    optimization: {
      namedModules: true,
      noEmitOnErrors: true,
      concatenateModules: true,
      splitChunks: {
        chunks: "async",
        cacheGroups: {
          vendors: false,
          default: false,
        }
      },
      minimizer: [
        new UglifyJsPlugin({
          parallel: true,
          uglifyOptions: {
            compress: true,
            mangle: true,
            output: {
              comments: false
            },
            test: /[1-9]|\.js($|\?)/i,
            exclude: [/\.min\.js$/gi] // skip pre-minified libs
          }
        })
      ]
    },

    plugins: [
      new BundleAnalyzerPlugin({analyzerMode: 'disabled', generateStatsFile: true}),
      new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /[1-9]|\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
          API_BUILDER_URL: JSON.stringify(''),
          PCMS_PORTAL_BUILDER_API_ENDPOINT: JSON.stringify(''),
          DEVICE_URL: JSON.stringify('device'),
          HASH_ROUTES: true,
          PROFILE_URL: JSON.stringify('profile'),
          UXBUILDER_URL: JSON.stringify(''),
          PCMS_ENV_BUILDER: false,
          BUILD_DATETIME: JSON.stringify(new Date().toISOString()),
          GIT_VERSION: JSON.stringify(gitRevisionPlugin.version()),
          GIT_COMMIT_HASH: JSON.stringify(gitRevisionPlugin.commithash()),
          GIT_BRANCH: JSON.stringify(gitRevisionPlugin.branch())  
        },
        'server.env': {
          PCMS_SCHEME: process.env.PCMS_SCHEME ? process.env.PCMS_SCHEME : 'http',
          PCMS_PORTAL_BUILDER_WEB_URL: 'pcmsweeraffyportalbuilderweb.naw01.rbodev.viasat.io',
          PCMS_PORTAL_BUILDER_WEB_PORT: 90,
          HASH_ROUTES: true,
        }
      }),
      new MediaTranslatorPlugin({
        profile: process.env.PROFILE_PATH || './mocks/api/layout/demofree/GET.json',
        urlWhitelist: [new RegExp('s3\.us-west-2\.amazonaws.com/pcms-portal-assets')],
        useMedia: true,
        mediaIdStart: 2000,
        mediaPrefix: 'media/',
        assetBaseUrl: process.env.ASSET_BASE_URL || '',
      }),
      new ManifestPlugin(),
    ],

    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      alias: {
        "react": path.resolve('../../node_modules/react'), // necessary due to npm being not greate a de-duping
        'react-dom': path.resolve('../../node_modules/react-dom'),
        '@material-ui': path.resolve('../../node_modules/@material-ui'),
        'core-js': path.resolve('../../node_modules/core-js'),
        'styled-components': path.resolve('../../node_modules/styled-components'),
        "jss": path.resolve('../../node_modules/jss'),
        "axios": path.resolve('../../node_modules/axios'),
        "ramda": path.resolve('../../node_modules/ramda'),
        "react-intl": path.resolve('../../node_modules/react-intl'),
        "react-redux": path.resolve('../../node_modules/react-redux'),
        "react-router": path.resolve('../../node_modules/react-router'),
        "react-router-dom": path.resolve('../../node_modules/react-router-dom'),
        "react-router-redux": path.resolve('../../node_modules/react-router-redux'),
        "redux": path.resolve('../../node_modules/redux'),
        "redux-actions": path.resolve('../../node_modules/redux-actions'),
        "redux-dynamic-reducer": path.resolve('../../node_modules/redux-dynamic-reducer'),
        "redux-thunk": path.resolve('../../node_modules/redux-thunk'),
        "history": path.resolve('../../node_modules/history'),
        "intl": path.resolve('../../node_modules/intl'),
        "intl-locales-supported": path.resolve('../../node_modules/intl-locales-supported'),
        "soundmanager2": path.resolve('../../node_modules/soundmanager2/script/soundmanager2-nodebug-jsmin.js'),
        "lodash-es": path.resolve("../../node_modules/lodash-es"),
      }
    },

    module: {
      rules: [
        {
          // The loader that handles ts and tsx files.  These are compiled
          // with the ts-loader and the output is then passed through to the
          // babel-loader.  The babel-loader uses the es2015 and react presets
          // in order that jsx and es6 are processed.
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          loader: 'babel-loader?presets[]=es2015&presets[]=react!ts-loader'
        },
        {test: /\.css$/, loader: 'style-loader!css-loader'},

        {
          test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: () => {
                  return `media/${mediaAssetNumber++}`;
                }
              }
            }
          ]
        }
      ]
    }
  }
];
