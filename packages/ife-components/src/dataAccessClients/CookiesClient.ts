import * as Cookies from 'js-cookie';

export class CookiesClient {
  public set(key: string, payload: any, expires: number) {
    Cookies.set(key, payload, {expires});
  }

  public get = <T>(key: string): T => Cookies.getJSON(key);

  public remove = (key: string) => Cookies.remove(key);
}

export const cookiesClient = new CookiesClient();
