import * as defaults from './defaultProps.json';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import NavLink from '../common/NavLink';
import {Image as ExternalInterface} from '../common/ComponentsInterface';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  background-repeat: no-repeat;

  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('min-width', props.theme.componentTheme.minWidth, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.maxWidth, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('min-height', props.theme.componentTheme.minHeight, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('max-height', props.theme.componentTheme.maxHeight, props.theme)};
  ${renderThemeProperty('xImageLocation', 'background-position-x')};
  ${renderThemeProperty('yImageLocation', 'background-position-y')};
  ${renderThemeProperty('backgroundSize', 'background-size')};

  img {
    width: 100%;
    ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  }
`;

class Image extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  public static contextType = RouterContext;

  private _handleSelect() {
    const {openPageOnSelect} = this.props.feature;
    if (openPageOnSelect) {
      this.context.history.push('/' + openPageOnSelect);
    }
  }

  public render() {
    const {componentTheme, altTextForImage, children, ...rest} = this.props;
    const {image, openPageOnSelect, externalUrl, openInNewTab} = this.props.feature;

    const noImage = !image;
    const imageContent = (
      <Container
        className="image-core-component"
        onClick={this._handleSelect.bind(this)}
        style={noImage ? {minHeight: '1rem'} : {}}
        {...rest}
      >
        {children && (children as any).length ? (
          children
        ) : (
          <ResponsiveImage altText={altTextForImage} image={image} useImageAsBackground={false} {...rest} />
        )}
      </Container>
    );

    return (
      <PortalComponent componentTheme={componentTheme}>
        {openPageOnSelect || externalUrl ? (
          <NavLink
            style={{display: 'inline-block', textDecoration: 'none'}}
            to={`/${externalUrl || openPageOnSelect}`}
            openInNewTab={openInNewTab}
          >
            {imageContent}
          </NavLink>
        ) : (
          imageContent
        )}
      </PortalComponent>
    );
  }
}

export default withLocaleProps(Image, [{altTextForImage: 'core.Image.altText'}]);
