import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Checkout as ExternalInterface} from '../common/ComponentsInterface';
import {Button, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';
import {sanitize} from 'component-utils';
import {validEmail} from 'component-utils';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  connected: boolean;
  feature?: any;
  headerText?: string;
  bodyText?: string;
  emailLabel?: string;
  emailErrorHelpText?: string;
  submitButtonText?: string;
  cancelButtonText?: string;
  connectedDeviceHeaderText?: string;
  connectedDevicebodyText?: string;
  connectedDeviceAlertButtonText?: string;
  onSwapDeviceSubmit?: any;
  onCancelButtonClick?: any;
  errorMessageOnFormHeader?: string;
  errorMessageInvalidEmail?: string;
  errorMessageGeneric?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  background-color: ${(props: any) => props.theme.primaryBackgroundColor};
  display: flex;
  flex-direction: column;
  width: 100%;
  div.overlay {
    background-color: ${(props: any) => props.theme.secondaryBackgroundColor};
    opacity: 0.9;
    width: 100%;
    align-items: center;
    justify-content: center;
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 2000;
    overflow: scroll;
  }
  .modal {
    z-index: 2001;
  }
  form.device-swap-form {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    padding-top: calc(${(props: any) => props.theme.gutter} * 2.5);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 2.5);
    padding-left: calc(${(props: any) => props.theme.gutter} * 1.875);
    padding-right: calc(${(props: any) => props.theme.gutter} * 1.875);
    margin: 0 auto;
    max-width: 688px;
    position: absolute;
    left: 0;
    right: 0;
    ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme)};
    h1.header-text {
      margin: 0px;
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 1);
      font-family: ${(props: any) => props.theme.secondaryFontFamily};
      ${renderThemeProperty('headerColor', 'color')};
      ${renderThemeProperty('headerFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.04;
    }
    div.body-text {
      margin: 0px;
      ${renderThemeProperty('bodyTextColor', 'color')};
      ${renderThemeProperty('bodyTextFontSize', 'font-size')};
      margin-bottom: calc(${(props: any) => props.theme.gutter} *1.25);
      font-weight: normal;
      line-height: 1.187;
      a {
        color: ${(props: any) => props.theme.secondaryTextColor};
      }
    }
    p.validation-summary {
      color: ${(props: any) => props.theme.formErrorColor};
      margin: 0px;
      margin-bottom: calc(${(props: any) => props.theme.gutter} *1.25);
      font-weight: bold;
      display: inline-block;
    }
    div.input-wrapper {
      label {
        word-wrap: break-word;
        ${renderThemeProperty('inputLabelColor', 'color')};
        ${renderThemeProperty('inputLabelFontSize', 'font-size')};
        margin: 0px;
        margin-left: calc(${(props: any) => props.theme.gutter} *0.5);
        line-height: 1.25;
      }
      input {
        font-family: ${(props: any) => props.theme.appFontFamily};
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        font-weight: 100;
        margin-top: calc(${(props: any) => props.theme.gutter} *0.25);
        display: block;
        width: 100%;
        height: calc(${(props: any) => props.theme.gutter} *2.5625);
        padding: calc(${(props: any) => props.theme.gutter} *0.375) calc(${(props: any) => props.theme.gutter} *0.625);
        font-size: ${(props: any) => props.theme.tertiaryFontSize};
        line-height: 1.42857;
        color: ${(props: any) => props.theme.primaryTextColor};
        border: 1px solid;
        border-color: ${(props: any) => props.theme.senaryBackgroundColor};
      }
      input:focus {
        border-color: ${(props: any) => props.theme.secondaryTextColor};
        color: ${(props: any) => props.theme.secondaryTextColor};
        border: 2px solid;
      }
      .validation {
        position: relative;
        padding: calc(${(props: any) => props.theme.gutter} *0.5) calc(${(props: any) => props.theme.gutter} *0.625)
          calc(${(props: any) => props.theme.gutter} *0.5) calc(${(props: any) => props.theme.gutter} *0.625);
        div.opacity-background {
          background-color: ${(props: any) => props.theme.formErrorColor};
          opacity: 0.14;
          position: absolute;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
        }
        p.help-text {
          margin: 0px;
          line-height: 1.25;
          margin-bottom: calc(${(props: any) => props.theme.gutter} *0.3125);
        }
        p.message {
          color: ${(props: any) => props.theme.formErrorColor};
          margin: 0px;
          font-weight: bold;
          line-height: 1.25;
        }
        div.circle {
          text-align: right;
          height: 0px;
          span {
            position: relative;
            top: calc(${(props: any) => props.theme.gutter} *-3.75);
            background-color: ${(props: any) => props.theme.formErrorColor};
            width: calc(${(props: any) => props.theme.gutter} *1.375);
            height: calc(${(props: any) => props.theme.gutter} *1.375);
            line-height: 1.375;
            display: inline-block;
            text-align: center;
            justify-content: center;
            border-radius: calc(${(props: any) => props.theme.gutter} *1.375);
            color: ${(props: any) => props.theme.tertiaryTextColor};
          }
          img {
            position: relative;
            top: calc(-75px);
            width: calc(27.5px);
            height: calc(27.5px);
            line-height: 1.375;
            display: inline-block;
            text-align: center;
          }
        }
      }
    }
    div.button-wrap {
      display: flex;
      justify-content: space-between;
      align-items: center;
      ${(props: any) =>
        applyMediaQueriesForStyle('flex-direction', props.theme.componentTheme.buttonFlexDirection, props.theme)};
      button {
        ${(props: any) =>
          applyMediaQueriesForStyle('margin-top', {xs: `calc(${props.theme.gutter}*1.25)`}, props.theme)};
        margin: 0px;
        ${(props: any) => applyMediaQueriesForStyle('width', {xs: '160px', md: '160px'}, props.theme)};
        ${renderThemeProperty('buttonWidth', 'width')};
        ${renderThemeProperty('buttonHeight', 'height')};
        ${renderThemeProperty('buttonBorder', 'border')};
        ${renderThemeProperty('buttonFontSize', 'font-size')};
        ${renderThemeProperty('buttonFontWeight', 'font-weight')};
        white-space: nowrap;
        justify-content: center;
      }
      button.cancel-button {
        border: 1px solid ${(props: any) => props.theme.primaryButtonColor};
        ${renderThemeProperty('buttonWidth', 'width')};
        ${renderThemeProperty('buttonHeight', 'height')};
        ${renderThemeProperty('cancelButtonBorderColor', 'border-color')};
        ${renderThemeProperty('buttonFontSize', 'font-size')};
        ${renderThemeProperty('buttonFontWeight', 'font-weight')};
        ${renderThemeProperty('cancelButtonFontColor', 'color')};
        ${renderThemeProperty('cancelButtonBackground', 'background')};
      }
    }
    div.connected {
      justify-content: center;
      button {
        margin-top: 0px;
      }
    }
  }
`;

class DeviceSwap extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults
  };

  constructor(props: any) {
    super(props);
    this.state = {
      errorDict: {
        errorHeaderText: this.props.errorMessageOnFormHeader,
        emailRequired: this.props.errorMessageInvalidEmail,
        generic: this.props.errorMessageGeneric
      },
      formErrors: {},
      email: ''
    };
    this._handleSwapRequestSubmit = this._handleSwapRequestSubmit.bind(this);
    this._handleCancelButtonClick = this._handleCancelButtonClick.bind(this);
  }

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handleSwapRequestSubmit(event: any) {
    event.preventDefault();
    const {email, errorDict} = this.state;
    let formErrors: any = {};
    if (!validEmail(email)) {
      formErrors['email'] = errorDict['emailRequired'];
    }
    this.setState({formErrors});
    if (Object.keys(formErrors).length == 0) {
      this.props.onSwapDeviceSubmit(email);
    }
  }

  _handleCancelButtonClick(event: any) {
    event.preventDefault();
    this.props.onCancelButtonClick();
  }

  public render() {
    const {connected, WifiServiceOptions} = this.props;
    const showSpinner =
      (WifiServiceOptions &&
        WifiServiceOptions.deviceSwap &&
        WifiServiceOptions.deviceSwap.waitingForResponse === true) ||
      (!connected &&
        WifiServiceOptions &&
        WifiServiceOptions.deviceSwap &&
        WifiServiceOptions.deviceSwap.swapping === true);

    const {
      headerText,
      bodyText,
      emailLabel,
      emailErrorHelpText,
      feature,
      submitButtonText,
      cancelButtonText,
      connectedDeviceHeaderText,
      connectedDevicebodyText,
      connectedDeviceAlertButtonText,
      errorIcon
    } = this.props;

    let {formErrors, errorDict} = this.state;

    if (
      WifiServiceOptions &&
      WifiServiceOptions.deviceSwap &&
      WifiServiceOptions.deviceSwap.waitingForResponse === false &&
      WifiServiceOptions.deviceSwap.error
    ) {
      formErrors['email'] = formErrors['email'] || errorDict['generic'];
    } else if (
      WifiServiceOptions &&
      WifiServiceOptions.deviceSwap &&
      WifiServiceOptions.deviceSwap.waitingForResponse === false &&
      WifiServiceOptions.deviceSwap.swapping === false
    ) {
      formErrors['email'] = formErrors['email'] || errorDict['generic'];
    }
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-device-swap-component">
          {showSpinner ? <Spinner /> : null}
          {feature.modal ? <div className="overlay" /> : null}
          <form className={`device-swap-form ${feature.modal ? 'modal' : ''}`} onSubmit={this._handleSwapRequestSubmit}>
            <div className="header">
              {!connected && headerText ? <h1 className="header-text">{headerText}</h1> : null}
              {connected && connectedDeviceHeaderText ? (
                <h1 className="header-text">{connectedDeviceHeaderText}</h1>
              ) : null}

              {!connected && bodyText ? (
                <div className="body-text" dangerouslySetInnerHTML={sanitize(bodyText)} />
              ) : null}
              {connected && connectedDevicebodyText ? (
                <div className="body-text" dangerouslySetInnerHTML={sanitize(connectedDevicebodyText)} />
              ) : null}
            </div>

            {Object.keys(formErrors).length ? (
              <p className="validation-summary">{errorDict['errorHeaderText']}</p>
            ) : null}
            {connected ? null : (
              <div className="input-wrapper">
                <label htmlFor="email">{emailLabel}</label>
                <input
                  className="input-text"
                  name="email"
                  id="email"
                  type="text"
                  onChange={e => this._handleFormChange('email', e.target.value)}
                />
                {formErrors.email ? (
                  <div className="validation">
                    <div className="opacity-background" />
                    <div className="circle">
                      {errorIcon ? <img src={errorIcon} height="36px" width="36px" /> : <span>!</span>}
                    </div>
                    {emailErrorHelpText ? <p className="help-text">{emailErrorHelpText}</p> : null}
                    <p className="message">{formErrors.email}</p>
                  </div>
                ) : null}
              </div>
            )}
            {connected ? (
              <div className="button-wrap connected">
                <Button
                  isPrimary={true}
                  onClick={this._handleCancelButtonClick}
                  aria-label={connectedDeviceAlertButtonText}
                >
                  {connectedDeviceAlertButtonText}
                </Button>
              </div>
            ) : (
              <div className="button-wrap">
                <Button isPrimary={true} onClick={this._handleSwapRequestSubmit} aria-label={submitButtonText}>
                  {submitButtonText}
                </Button>
                <Button
                  className="cancel-button"
                  isPrimary={false}
                  onClick={this._handleCancelButtonClick}
                  aria-label={cancelButtonText}
                >
                  {cancelButtonText}
                </Button>
              </div>
            )}
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default DeviceSwap;
