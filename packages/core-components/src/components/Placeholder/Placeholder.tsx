import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Placeholder as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const PlaceholderThemeWrapper = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('textColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${(props: any) => props.theme.primaryFontSize};
`;

class Placeholder extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        {this.props.feature.url?
        <a href={this.props.feature.url}>
          <PlaceholderThemeWrapper className="placeholder-component">{this.props.feature.text}</PlaceholderThemeWrapper>
        </a> : 
        <div>
          <PlaceholderThemeWrapper className="placeholder-component">{this.props.feature.text}</PlaceholderThemeWrapper>
        </div>
        }  
      </PortalComponent>
    );
  }
}

export default Placeholder;
