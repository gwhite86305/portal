import {createMockStore} from 'redux-test-utils';
import * as redux from 'redux';

const createAppMockStore = (state: any, middlewares: any[]) => {
  let store = createMockStore(state) as any;
  let dispatch = store.dispatch;

  middlewares.forEach((middleware: any) => {
    dispatch = middleware(store)(dispatch);
  });

  return {
    ...store,
    dispatch
  };
};

export default createAppMockStore;
