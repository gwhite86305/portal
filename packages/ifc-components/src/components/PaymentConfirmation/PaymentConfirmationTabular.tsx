import * as React from 'react';
import {PortalComponent, sanitize, utils, Currency} from 'component-utils';
import {getUTCDate} from 'component-utils/dist/utils/DateUtils';
import {PaymentConfirmation as ExternalInterface} from '../common/ComponentsInterface';
import {Spinner} from 'core-components';
import * as defaults from './defaultProps.json';
import {pathOr} from 'ramda';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  WifiServiceOptions?: any;
  feature?: any;
  header?: string;
  thankYouNoteText?: string;
  dateLabel?: string;
  merchantOfRecordLabel?: string;
  merchantOfRecordLabelValue?: string;
  serviceCostLabel?: string;
  discountLabel?: string;
  totalLabel?: string;
  buttonText?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  display: flex;
  flex-direction: column;
  h1.header-text {
    margin: 0px;
    margin-top: calc(${(props: any) => props.theme.gutter} *1.875);
    margin-bottom: calc(${(props: any) => props.theme.gutter} * 0.625);
    ${renderThemeProperty('headerFontFamily', 'font-family')};
    ${renderThemeProperty('headerColor', 'color')};
    ${renderThemeProperty('headerFontSize', 'font-size')};
    ${(props: any) =>
      applyMediaQueriesForStyle('line-height', props.theme.componentTheme.headerLineHeight, props.theme)};
    font-weight: ${(props: any) => props.theme.componentTheme.headerFontWeight};
  }
  div.body-text {
    display: flex;
    align-items: center;
    margin-bottom: calc(${(props: any) => props.theme.gutter} * 1.875);
    font-weight: normal;
    color: ${(props: any) => props.theme.quaternaryBackgroundColor};
    ${(props: any) =>
      applyMediaQueriesForStyle('line-height', props.theme.componentTheme.bodyTextLineHeight, props.theme)};
    h2 {
      margin: 0px;
      ${renderThemeProperty('bodyTextFontFamily', 'font-family')};
      ${renderThemeProperty('bodyTextColor', 'color')};
      ${renderThemeProperty('bodyTextFontSize', 'font-size')};
      font-weight: ${(props: any) => props.theme.componentTheme.bodyTextFontWeight};
    }
    a {
      color: ${(props: any) => props.theme.secondaryTextColor};
    }
  }
  p.validation-summary {
    color: ${(props: any) => props.theme.formErrorColor};
    margin: 0px;
    margin-bottom: calc(${(props: any) => props.theme.gutter} *1.5625);
    font-weight: bold;
    display: inline-block;
  }
  .transaction-record {
    display: flex;
    ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', lg: 'row'}, props.theme)};
    > div {
      ${(props: any) => applyMediaQueriesForStyle('width', {xs: '100%', lg: '16.6666666667%'}, props.theme)};
      div:first-child {
        ${(props: any) => applyMediaQueriesForStyle('width', {xs: 'auto', lg: '100%'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'inline-block', lg: 'block'}, props.theme)};
        ${renderThemeProperty('labelFontSize', 'font-size')};
        ${renderThemeProperty('labelColor', 'color')};
        padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.625);
        ${(props: any) => applyMediaQueriesForStyle('font-weight', {xs: 'bold', lg: 'normal'}, props.theme)};
        :after {
          content: ':';
          ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'inline-block', lg: 'none'}, props.theme)};
          padding-right: calc(${(props: any) => props.theme.gutter} * 0.3125);
          ${renderThemeProperty('labelColor', 'color')};
        }
      }
      div:last-child {
        ${(props: any) => applyMediaQueriesForStyle('width', {xs: 'auto', lg: '100%'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'inline-block', lg: 'block'}, props.theme)};
        ${renderThemeProperty('labelValueColor', 'color')};
        ${renderThemeProperty('labelValueFontSize', 'font-size')};
      }
    }
  }
`;

class PaymentConfirmationTabular extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }

  public render() {
    const {
      WifiServiceOptions,
      header,
      thankYouNoteText,
      serviceDescription,
      dateLabel,
      merchantOfRecordLabel,
      merchantOfRecordLabelValue,
      serviceCostLabel,
      discountLabel,
      totalLabel,
      currencyCode,
      locale,
      minimumFractionDigits,
      maximumFractionDigits
    } = this.props;
    const transaction = pathOr({}, ['orderHistory', 'transactions', 0], WifiServiceOptions);
    const showSpinner = pathOr(false, ['orderHistory', 'waitingForResponse'], WifiServiceOptions);
    let error =
      pathOr(false, ['orderHistory', 'waitingForResponse', 0], WifiServiceOptions) &&
      WifiServiceOptions.orderHistory.error;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-payment-confirmation-component">
          {showSpinner ? <Spinner /> : null}
          <div className="header">
            {header ? <h1 className="header-text">{header}</h1> : null}
            {thankYouNoteText ? (
              <div className="body-text">
                <h2>{thankYouNoteText} </h2>
              </div>
            ) : null}
          </div>
          {error ? <p className="validation-summary">{error.friendlyMessage}</p> : null}
          <div className="transaction-record">
            <div>
              <div>{serviceDescription}</div>
              <div>{transaction.serviceDescription}</div>
            </div>
            <div>
              <div>{dateLabel}</div>
              <div>{getUTCDate(new Date(pathOr(0, ['timeOfPurchase'], transaction) * 1000))}</div>
            </div>
            <div>
              <div>{merchantOfRecordLabel}</div>
              <div>American</div>
            </div>
            <div>
              <div>{serviceCostLabel}</div>
              <div>
                <Currency
                  currency={currencyCode}
                  value={transaction.retailCost}
                  locale={locale}
                  minimumFractionDigits={minimumFractionDigits}
                  maximumFractionDigits={maximumFractionDigits}
                />
              </div>
            </div>
            <div>
              <div>{discountLabel}</div>
              <div>
                <Currency
                  currency={currencyCode}
                  value={transaction.promoDiscount}
                  locale={locale}
                  minimumFractionDigits={minimumFractionDigits}
                  maximumFractionDigits={maximumFractionDigits}
                />
              </div>
            </div>
            <div>
              <div>{totalLabel}</div>
              <div>
                <Currency
                  currency={currencyCode}
                  value={transaction.totalCost}
                  locale={locale}
                  minimumFractionDigits={minimumFractionDigits}
                  maximumFractionDigits={maximumFractionDigits}
                />
              </div>
            </div>
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default PaymentConfirmationTabular;
