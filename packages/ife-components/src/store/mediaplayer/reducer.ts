import {propEq, prop, findIndex} from 'ramda';
import {Action, ActionCreators} from './actions';
import {
  MediaPlayerState,
  MEDIA_PLAYER_STATUS,
  MEDIA_PLAYER_TYPE,
  SavedPlaylist as PlayListNorm,
  PlaylistItem
} from './types';
import {VideoItem} from '../videos/types';
import {AudioItem} from '../audio/types';
import {merge, union} from 'lodash';
import {omit, difference} from 'lodash-es';
import localStorageService from '../../services/LocalStorageService';
import Norm, {Normalized} from '../../utils/Norm';
import {isIOS} from '../../components/MediaPlayer/utils/playerCommon';

export const initialState: MediaPlayerState = {
  audioLanguages: [],
  currentItem: undefined,
  currentPlaylistItemIndex: -1,
  drmServerUrl: undefined,
  drmToken: '',
  fileManifest: '',
  fileType: MEDIA_PLAYER_TYPE.NONE,
  isBuffering: false,
  isFullScreen: false,
  isLoadingDrmServer: false,
  isLoadingToken: false,
  playerPage: undefined,
  playlistItems: {
    byId: {},
    allIds: []
  },
  seekToRange: 0,
  audioLanguage: 'en',
  subtitleLanguage: 'off',
  status: MEDIA_PLAYER_STATUS.PAUSED,
  subtitles: [],
  volume: 1,
  playlistLoading: false,
};

const updatePlaylist = (state: MediaPlayerState, payload: PlayListNorm): MediaPlayerState => {
  const playlist = {
    byId: merge({}, state.playlistItems.byId, payload.byId),
    allIds: union(state.playlistItems.allIds, payload.allIds)
  };
  return {
    ...state,
    playlistItems: playlist
  };
};

const removeFromPlaylist = (state: MediaPlayerState, uid: string): MediaPlayerState => {
  return {
    ...state,
    playlistItems: Norm.deleteId(state.playlistItems, uid)
  };
};

const removeAllFromPlaylist = (state: MediaPlayerState): MediaPlayerState => {
  const playlistItems = {
    byId: {},
    allIds: []
  };
  return {
    ...state,
    playlistItems,
    currentPlaylistItemIndex: -1
  };
};

export default function reducer(state: MediaPlayerState = initialState, action: Action): MediaPlayerState {
  const {type, payload} = action;
  switch (type) {
    // VideoPlaybackStart
    case ActionCreators.getVideoPlaybackStart.type:
      return {...state, isLoadingToken: true, fileManifest: '', drmToken: ''};
    case ActionCreators.getVideoPlaybackSuccess.type:
      return {
        ...state,
        drmToken: payload.drmToken,
        fileManifest: payload.src,
        isLoadingToken: false,
        status: isIOS ? MEDIA_PLAYER_STATUS.PAUSED : state.status
      };
    case ActionCreators.getVideoPlaybackError.type:
      return {...state, isLoadingToken: false, fileManifest: '', drmToken: ''};

    // getDrmServersStart
    case ActionCreators.getDrmServersStart.type:
      return {...state, isLoadingDrmServer: true, drmServerUrl: undefined};
    case ActionCreators.getDrmServersError.type:
      return {...state, isLoadingDrmServer: false, drmServerUrl: undefined};
    case ActionCreators.getDrmServersSuccess.type:
      return {...state, isLoadingDrmServer: false, drmServerUrl: payload.data};

    // Playlist
    case ActionCreators.addItemsToPlaylist.type:
      return updatePlaylist(state, payload);
    case ActionCreators.removeAllItemFromPlaylist.type:
      return removeAllFromPlaylist(state);
    case ActionCreators.removeItemFromPlaylist.type:
      return removeFromPlaylist(state, payload);
    case ActionCreators.increaseCurrentPlaylistIndex.type:
      return {...state, currentPlaylistItemIndex: state.currentPlaylistItemIndex + 1};
    case ActionCreators.decreaseCurrentPlaylistIndex.type:
      return {
        ...state,
        currentPlaylistItemIndex: state.currentPlaylistItemIndex > -1 ? state.currentPlaylistItemIndex - 1 : -1
      };
    case ActionCreators.resetPlaylistIndex.type:
      return {...state, currentPlaylistItemIndex: -1, status: MEDIA_PLAYER_STATUS.PAUSED};
    case ActionCreators.loadPlaylistFromStorageStart.type:
      return {...state}
    case ActionCreators.loadPlaylistFromStorageSuccess.type:
      return updatePlaylist(state, payload)
    case ActionCreators.loadPlaylistFromStorageFailure.type:
      return {...state}
    // Player
    case ActionCreators.clearPlayerInfo.type:
      return {
        ...initialState,
        currentPlaylistItemIndex: state.currentPlaylistItemIndex,
        drmServerUrl: state.drmServerUrl,
        isFullScreen: state.isFullScreen,
        playlistItems: state.playlistItems,
        volume: state.volume
      };
    case ActionCreators.seekToRange.type:
      return {...state, seekToRange: payload.target};
    case ActionCreators.setAudioLanguages.type:
      return {...state, audioLanguages: payload.langs};
    case ActionCreators.setBuffering.type:
      return {...state, isBuffering: payload.isBuffering};
    case ActionCreators.setCurrentItem.type:
      return {
        ...state,
        currentPlaylistItemIndex: getPlaylistItemIndex(payload.uid, state.playlistItems),
        currentItem: payload as any
      };
    case ActionCreators.setFullscreen.type:
      return {...state, isFullScreen: payload.isFullscreen};
    case ActionCreators.setPlayerAsPaused.type:
      return {...state, status: MEDIA_PLAYER_STATUS.PAUSED};
    case ActionCreators.setPlayerAsPlay.type:
    case ActionCreators.setPlayerAsPlaying.type:
      return {...state, status: MEDIA_PLAYER_STATUS.PLAYING};
    case ActionCreators.setPlayerPage.type:
      return {...state, playerPage: payload};
    case ActionCreators.setSubtitles.type:
      return {...state, subtitles: payload.langs};
    case ActionCreators.updateVolume.type:
      return {...state, volume: payload.value};
    case ActionCreators.mobilePlayVideo.type:
      return state;
    case ActionCreators.loadPlayListStart.type:
      return {...state, playlistLoading: true}
    case ActionCreators.loadPlayListSuccess.type:
        return {...state, playlistLoading: false}
    default:
      return state;
  }
}

const getPlaylistItemIndex = (uid: string, playlist: Normalized<string, PlaylistItem>) => playlist.allIds.indexOf(uid);
