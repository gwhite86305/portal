import * as React from 'react';
import {MediaThumbnail} from '../MediaThumbnail/MediaThumbnail';
import {getImageSrcByRatio} from '../utils';
import Pulsate from './Pulsate';
import {StyledRowHeader} from './StyledRowHeader';
import {MobileCarouselProps} from './types';
import {AUDIO_TYPE} from '../../../store/audio/types';

// TODO mapType to variant
const MobileCarousel: React.FC<MobileCarouselProps> = ({
  categoryName,
  items,
  onClick,
  imgPlaceholder,
  onItemInViewPort,
  imageVariant
}) => (
  <div className="single-row">
    <StyledRowHeader className="row-header">
      {categoryName || <div className="empty-title" children={<Pulsate />} />}
    </StyledRowHeader>
    <div className="row-content">
      {items.map((item, index) => (
        <a key={item.uid} className="card">
          {item.titleImages && (
            <MediaThumbnail
              className="poster"
              inVieport={onItemInViewPort}
              onClick={() => onClick && onClick(item.uid)}
              src={getImageSrcByRatio(item.titleImages[0], imageVariant, window.devicePixelRatio)}
              item={item}
              clickable={true}
            />
          )}
          <div className="thumbnail-title">{item.title}</div>
          {item.type === AUDIO_TYPE.GROUP && <span className="thumbnail-subtitle">{item.artist}</span>}
        </a>
      ))}
    </div>
  </div>
);

export default MobileCarousel;
