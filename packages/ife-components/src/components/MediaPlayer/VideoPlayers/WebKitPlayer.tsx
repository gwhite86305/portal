import {prop} from 'ramda';
import * as React from 'react';
import {Subject} from 'rxjs';
import {AnalyticsErrorCode} from '../../../store/analytics/utils';
import {IPlayerState, PlayerProps, PlayerState} from '../types';
import {
  attachVideoPlayerListeners,
  handleAudioLanguageChange,
  handleSubtitleChange,
  handleVolumeChange,
  isNonDRM,
  loadNonDRMContent,
  seekToRange
} from '../utils/playerCommon';
import {initWebKitPlayer} from '../utils/playerWebKit';
import {utils} from 'component-utils';

export default class WebKitPlayer extends React.Component<PlayerProps, PlayerState> {
  unsubscribe$: Subject<boolean>;

  constructor(props: any) {
    super(props);
    this.unsubscribe$ = new Subject<boolean>();
    this.state = {prevfileManifest: '', fileManifest: '', currentTime: 0, duration: 0};
  }

  static getDerivedStateFromProps(nextProps: PlayerProps, prevState: IPlayerState) {
    if (typeof nextProps.fileManifest === 'string' && nextProps.fileManifest !== prevState.prevfileManifest)
      return {prevManifestUri: nextProps.fileManifest, fileManifest: ''};
    return null;
  }

  onTimeUpdate = (uid: string, currentTime: number, duration: number) => {
    this.setState({...this.state, currentTime, duration});
    this.props.onTimeUpdate(uid, currentTime, duration);
  };

  componentDidMount() {
    // 1 - attach listeners and propagate events to store via props callbacks
    attachVideoPlayerListeners(
      undefined,
      this.props,
      this.unsubscribe$,
      this.refs.videoRef as any,
      () => this.props,
      this.onTimeUpdate,
      this.props.stopMovie$
    );
    this.loadVideo(this.props);
  }

  componentDidUpdate(prevProps: PlayerProps) {
    const videoRef = this.refs.videoRef as HTMLVideoElement;
    const isNewVideo =
      this.state.fileManifest === '' &&
      (this.props.fileManifest !== prevProps.fileManifest ||
        this.props.fairplayLicenseUrl !== prevProps.fairplayLicenseUrl ||
        this.props.drmToken !== prevProps.drmToken);

    if (isNewVideo)
      attachVideoPlayerListeners(
        undefined,
        this.props,
        this.unsubscribe$,
        this.refs.videoRef as any,
        () => this.props,
        this.onTimeUpdate,
        this.props.stopMovie$
      ) && this.loadVideo(this.props);

    if (this.props.seekToRange !== prevProps.seekToRange) seekToRange(this.props, videoRef as any);
    if (this.props.volume !== prevProps.volume) handleVolumeChange(this.props.volume, videoRef as any);
    if (this.props.audioLanguage !== prevProps.audioLanguage)
      handleAudioLanguageChange(this.props.audioLanguage, videoRef);

    const textTrackArr = Array(videoRef.textTracks.length)
      .fill(0)
      .map((_, index) => videoRef.textTracks[index]);
    const subTrack = textTrackArr.find(track => track.mode == 'showing') || {language: ''};
    const subtitleStateChange = this.props.subtitleLanguage !== prevProps.subtitleLanguage;
    const nativeSubtitleChange = subTrack.language && this.props.subtitleLanguage !== subTrack.language;
    if (subtitleStateChange) {
      handleSubtitleChange(this.props.subtitleLanguage, videoRef);
    } else if (nativeSubtitleChange) {
      handleSubtitleChange(subTrack.language, videoRef);
      this.props.updateSubtitleLanguage({uid: this.props.videoId, lang: subTrack.language});
    }

    const audioTrackArr = Array(videoRef.audioTracks.length)
      .fill(0)
      .map((_, index) => videoRef.audioTracks[index]);
    const audioTrack = audioTrackArr.find(track => track.enabled) || {language: ''};
    const languageStateChange =
      this.props.audioLanguage &&
      (this.props.audioLanguage !== prevProps.audioLanguage || (!audioTrack && audioTrackArr.length));
    const nativeLanguageChange = audioTrack.language && audioTrack.language != this.props.audioLanguage;
    if (languageStateChange) {
      handleAudioLanguageChange(this.props.audioLanguage, videoRef);
    } else if (nativeLanguageChange) {
      handleAudioLanguageChange(audioTrack.language, videoRef);
      this.props.updateAudioLanguage({uid: this.props.videoId, lang: audioTrack.language});
    }

    if (prevProps.advertId && prevProps.advertId !== this.props.advertId) {
      this.props.onTimeUpdate(prevProps.advertId, 0, 0);
    }
  }

  componentWillUnmount() {
    (this.refs.videoRef as any) = null;
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  render() {
    return (
      <video
        controls={false}
        autoPlay={!this.props.isPausedForNotification && !utils.mobileSdk.isUsingMobileSdk()}
        playsInline
        webkit-playsinline="true"
        ref="videoRef"
        className="video"
        style={{
          backgroundColor: 'black',
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          zIndex: 100
        }}
      />
    );
  }

  private async loadVideo({
    fileManifest,
    drmToken,
    fairplayCertificateUrl,
    fairplayLicenseUrl,
    currentTime
  }: PlayerProps) {
    if (isNonDRM(fileManifest)) return loadNonDRMContent(this.refs.videoRef as any, fileManifest);
    // 1 - check valid req
    if (!fileManifest || !drmToken || !fairplayLicenseUrl || !fairplayCertificateUrl) return;
    const videoRef = this.refs.videoRef as HTMLVideoElement;
    // 2 - cancel any request in progress
    // TODO: for ads preroll
    // 3 - load video
    this.setState({fileManifest});
    try {
      await initWebKitPlayer(videoRef, `${fileManifest}/.m3u8`, drmToken, fairplayLicenseUrl, fairplayCertificateUrl);
      this.props.onAudioLanguagesLoaded(
        Array(videoRef.audioTracks.length)
          .fill(0)
          .map((_, index) => videoRef.audioTracks[index])
          .map(prop('language'))
      );
      this.props.onSubtitlesLoaded(
        Array(videoRef.textTracks.length)
          .fill(0)
          .map((_, index) => videoRef.textTracks[index])
          .map(prop('language'))
          .concat(['off'])
      );

      videoRef.currentTime = currentTime || 0;
      handleVolumeChange(this.props.volume, this.refs.videoRef as any);
      handleSubtitleChange(this.props.subtitleLanguage, this.refs.videoRef as HTMLVideoElement);
      handleAudioLanguageChange(this.props.audioLanguage, this.refs.videoRef as HTMLVideoElement);
    } catch (error) {
      this.props.onError({code: AnalyticsErrorCode.player, message: error.toString()}, this.props.playbackSessionId);
    }
  }
}
