import {Point, Bounds} from './types';
import {bezierCommand} from './utils';
import React = require('react');

export interface MovingMapLineProps {
  originPoint: number[];
  coords: number[][];
  mapState?: {
    center: number[];
    zoom: number;
    bounds: Bounds;
    width: number;
    height: number;
  };
  latLngToPixel?: (latLng: number[], center?: any, zoom?: any) => number[];
}

export const MovingMapLine: React.FC<MovingMapLineProps> = ({
  mapState,
  latLngToPixel,
  coords,
  originPoint
}) => {
  if (!coords || coords.length < 1 || !latLngToPixel || !mapState) {
    return null;
  }

  const {width, height} = mapState;
  const pixelCoords = [originPoint].concat(coords).map((item: Point) => latLngToPixel(item));
  const d = pixelCoords.reduce(
    (acc: any, point: any, i: any, a: any) =>
      i === 0 ? `M ${point[0]},${point[1]}` : `${acc} ${bezierCommand(point, i, a)}`,
    ''
  );

  return (
    <svg className="travelled-path" width={width} height={height}>
      <path d={d} fill="none" />
    </svg>
  );
};
