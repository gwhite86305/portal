import * as React from 'react';
import {GridLayoutThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {GridContainer as ExternalInterface} from '../common/ComponentsInterface';
import Template from '../Template/Template';
import * as defaults from './defaultProps.json';
import LayoutInterface from 'component-utils/dist/types/LayoutInterface';
import {LayoutContext} from 'component-utils/dist/utils/Contexts';

export const GridContainerComponentName = 'GridContainer'; // single src of truth for componentName comparisons

export interface InternalInterface {
  className?: string;
  children?: any;
}

const GridContainer: React.FunctionComponent<InternalInterface & ExternalInterface> = props => {
  const {template, alignItems, alignContent, justify, spacing, wrap, direction} = props.feature;
  const {components}: LayoutInterface = React.useContext(LayoutContext) as LayoutInterface;

  const gridComponentHierarchy = (
    <PortalComponent componentTheme={props.componentTheme}>
      <GridLayoutThemeWrapper
        {...{
          container: true,
          item: false,
          alignItems,
          alignContent,
          justify,
          spacing,
          wrap,
          direction
        }}
      >
        {props.children}
      </GridLayoutThemeWrapper>
    </PortalComponent>
  );

  return template ? (
    <Template {...components[template].properties} templateBodyComponent={gridComponentHierarchy} />
  ) : (
    gridComponentHierarchy
  );
};

GridContainer.defaultProps = defaults;

export default GridContainer;
