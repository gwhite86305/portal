import { LocalizedCategory } from "../store/audio/types";

export const extractCategories = (payload: any, uid: string) => {
  const foundCategory = payload.subcategories.find((it: any) => it.uid === uid);
  return {subcategories: foundCategory!.subcategories || []};
};

export const flattenCategories = (data: any, category?: string): LocalizedCategory[] => {
  const getAllCategoriesHelper = ({uid, title, subcategories}: any, acc: LocalizedCategory[]) => {
    uid && title && acc.push({uid, title, category});
    if (subcategories) {
      subcategories.forEach((subcategory: any) => {
        getAllCategoriesHelper(subcategory, acc);
      });
    }
  };
  const acc: any[] = [];
  getAllCategoriesHelper(data, acc);
  return acc;
};
