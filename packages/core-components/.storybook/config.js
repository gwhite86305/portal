import {configure} from '@storybook/react';

function loadStories() {
  require('../stories/banner.js');
  require('../stories/billboard.js');
  require('../stories/button.js');
  require('../stories/footer.js');
  require('../stories/header.js');
  require('../stories/horizontalMenu.js');
  require('../stories/legal.js');
  require('../stories/systemsnackbar.js');
  require('../stories/quicklinks.js');
  require('../stories/carousel.js');
  //require('../stories/WaitSplash.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);
