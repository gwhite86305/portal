# Media Translator Plugin

A webpack plugin for converting a PCMS portal into a PFE-ready state. This (along with some changes in the webpack config) handles changing code bundles and assets for use by the media endpoint, enriching the profile, and gathering and preparing CMS-served assets. It is only coded to change chunk and asset names into numerical media IDs, but could be changed to handle other naming schemes. The conversion of chunks happens after compilation (webpack 'this-compilation' plugin hook), and asset handling after the build is done (webpack 'done' plugin hook).

## Getting Started

Install dependencies (lerna bootstrap/npm run bootstrap) and insert this into a webpack configuration.

This plugin exposes 8 options in a configuration object:

* **profile**: Where to find the profile. This can be a URL (remote or file-based), a function, or a promise. It must end up as JSON.
  * **Default**: ''
* **collectAssets**: Whether the plugin should download CMS assets and include them in the output directory, renaming them as appropriate.
  * **Default**: true
* **useMedia**: Whether the plugin should convert chunk and asset names to media IDs.
  * **Default**: false
* **mediaIdStart**: Which media ID should be assigned to the first chunk/asset. All others will follow incrementally.
  * **Default**: 1000
* **mediaPrefix**: A string to prepend to media IDs at conversion. Can be used to add subdirectories for media storage (eg. `"media/"`).
  * **Default**: ''
* **pathBlacklist**: Black list of files based on a list of regexps or strings. If a local file matches these, it won't be downloaded or mediafied.
  * **default**: ['^/']
* **urlWhitelist**: Lets you whitelist URLs that should be downloaded from and mediafied. This ensures you can put something like `http://assets.viasat.com` or `http://cdn.google.com` in the list and download images or CSS, but not try to download anything from other URLs that may be used in the layout, like `http://facebook.com`.
  * **default**: []
* **assetBaseUrl**: Any absolute paths will be attempt to be downloaded from the local system. If that fails, they will be appended to this, then attempted to be downloaded and mediafied. This is to handle the ubiquity of references in the layout that are just `/mock/image/...` that are downloaded at runtime. (This actually isn't true yet. It will be.)
  * **default**: 'http://localhost:3001/'

### Installing

* `lerna bootstrap`
* `npm run deploy` in the top directory

This will output the final, PFE-ready portal into `./packages/portal-renderer/dist`, and deploy it with the tp-deploy-portal submodule.

## Built With

* [Request](https://www.npmjs.com/package/request) - Used to make requests
* [Request Promise Native](https://www.npmjs.com/package/request-promise-native) - Used to make requests return promises
* [Webpack](https://www.npmjs.com/package/webpack) - It's the reason for the treason

## Authors

* **William Moore** - *Initial work* - [wmoore@viasat.com](mailto:wmoore@viasat.com)
