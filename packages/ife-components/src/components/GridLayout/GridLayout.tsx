import * as React from 'react';
import {PortalComponent} from 'component-utils';
import * as defaultProps from './defaultProps.json';
import StyledGridContainerProps from './StyledGridContainer';
import StyledGridItemTitle from './StyledGridItemTitle.js';
import StyledGridItem from './StyledGridItem.js';
import {GridLayout as ExternalInterface} from '../common/ComponentsInterface';
import StyledGridImage from './StyledGridImage.js';
import StyledGridIframe from './StyledGridIframe.js';

export interface GridLayoutProps extends ExternalInterface {}

export interface GridItem {
  title?: string;
  imgSrc?: string;
  iframeSrc?: string;
  id: string;
  columnPos?: {xs: string; sm: string; md: string};
  rowPos?: {xs: string; sm: string; md: string};
  handleClick?: (id: string) => void;
  variant: string;
}

const GridLayout: React.FC<GridLayoutProps> = (props = defaultProps as any) => {
  const {
    feature: {items}
  } = props;

  let gridItems: GridItem[] = [];
  // assign key, this is not ideal & we need an elegant solution
  if (items) {
    gridItems = items.map((item, index) => ({...item, id: item.id || index}));
  }

  return (
    <PortalComponent componentTheme={props.componentTheme}>
      <StyledGridContainerProps>
        {gridItems.map(({id, variant, iframeSrc, imgSrc, title, columnPos, rowPos, handleClick}, index) => (
          <StyledGridItem key={id} columnPos={columnPos} rowPos={rowPos} variant={variant}>
            {iframeSrc && <StyledGridIframe src={iframeSrc} />}
            {imgSrc && <StyledGridImage onClick={() => handleClick && handleClick(id)} imgSrc={imgSrc} />}
            {title && <StyledGridItemTitle>{title}</StyledGridItemTitle>}
          </StyledGridItem>
        ))}
      </StyledGridContainerProps>
    </PortalComponent>
  );
};
export default GridLayout;
