import * as uuidv4 from 'uuid/v4';
import {PortalComponent, utils} from 'component-utils';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import {path, pathOr, prop} from 'ramda';
import * as React from 'react';
import {withTheme} from 'styled-components';
import {PrerollAd, TYPES} from '../../store/videos/types';
import {pauseAudioPlayer} from '../../utils/audioPlayer';
import MediaPlayer from './MediaPlayer';
import {ContainerState as MediaContainerState, MediaContainerProps} from './types';
import {
  DEFAULT_VIDEO_ID,
  getHtmlBundleParams,
  initialState,
  isFullScreenForbidden,
  isNewPlaylistGoToFirstAd,
  mapDispatchToProps,
  mapStoreToProps
} from './utils/container';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
class MediaPlayerContainer extends React.PureComponent<MediaContainerProps, MediaContainerState> {
  public static contextType = RouterContext;
  unsubscribeFromRouteChange: any;

  private playbackSessionId = uuidv4();
  public state = initialState;

  static getDerivedStateFromProps(props: MediaContainerProps, state: MediaContainerState) {
    if (state.adsPlaylist === undefined) {
      return {adsPlaylist: props.preRollAds[state.videoId], currentAdIndex: -1};
    }
    return null;
  }

  componentDidMount() {
    this.props.loadVideosConfig();
    if (this.shouldIFetchDrmConfig(this.props)) {
      this.props.getDrmServers(this.playbackSessionId);
    }
    this.unsubscribeFromRouteChange = this.context.history.listen(this.onRouteChange.bind(this));
    this.onRouteChange({...this.context.location});
    window.addEventListener('offline', this.onNetworkDisconnect);
  }

  componentDidUpdate(_: any, prevState: MediaContainerState) {
    const {adsPlaylist, currentAdIndex, isTrailer} = this.state;
    const {getVideoPlayback, setFullscreen} = this.props;
    switch (true) {
      case isTrailer:
        return;
      case this.shouldIFetchMovie():
        return getVideoPlayback(this.getVideoId(), this.playbackSessionId);
      case isNewPlaylistGoToFirstAd(adsPlaylist, prevState.adsPlaylist, currentAdIndex):
        return this.onAdEnded();
      case currentAdIndex !== prevState.currentAdIndex:
        return isFullScreenForbidden(path([currentAdIndex], adsPlaylist)) && setFullscreen(false);
    }
  }

  componentWillUnmount() {
    this.props.setFullscreen(false);
    this.props.clearPlayerInfo();
    this.unsubscribeFromRouteChange();
    window.removeEventListener('offline', this.onNetworkDisconnect);
  }

  render() {
    const videoItem: any = this.getVideoItem();
    const customSkin = this.getCustomSkin();
    const eventHandlers = this.getEventHandlers();
    let drmServerUrls = {};
    let onVideoEnded = eventHandlers.onVideoEnded;
    let fileManifest = this.props.fileManifest;

    switch (true) {
      case this.state.isTrailer:
        fileManifest = this.props.trailerVideoUrl;
        break;
      case this.isAd(videoItem as PrerollAd):
        onVideoEnded = this.onAdEnded.bind(this);
        fileManifest = pathOr('', ['video', 'url'], videoItem);
        break;
      default:
        drmServerUrls = this.getDrmServersUrl();
    }

    const {
      componentTheme,
      updateVideoConfig,
      isTrailer,
      currentTime,
      duration,
      subtitleLanguage,
      audioLanguage,
      isPausedForNotification,
      setFullscreen,
      audioLanguages,
      drmToken,
      isBuffering,
      isFullScreen,
      status,
      subtitles,
      volume,
      seekToRange,
      reportEvent
    } = this.props;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <MediaPlayer
          {...customSkin}
          {...drmServerUrls}
          {...eventHandlers}
          updateVideoConfig={updateVideoConfig}
          isTrailer={isTrailer}
          currentTime={currentTime as any}
          duration={duration}
          subtitleLanguage={subtitleLanguage}
          audioLanguage={audioLanguage}
          isPausedForNotification={isPausedForNotification}
          setFullscreen={setFullscreen}
          blockFastForward={prop('blockFastForward', videoItem)}
          audioLanguages={audioLanguages}
          drmToken={drmToken}
          prevfileManifest={fileManifest}
          isBuffering={isBuffering}
          isFullScreen={isFullScreen}
          status={status}
          subtitles={subtitles}
          volume={volume}
          advertId={(videoItem && videoItem.uid) || ''}
          videoId={this.getVideoId()}
          seekToRange={seekToRange}
          onVideoEnded={onVideoEnded}
          fileManifest={fileManifest}
          reportEvent={reportEvent}
          playbackSessionId={this.playbackSessionId}
        />
      </PortalComponent>
    );
  }

  // -- Private lifecycle utils
  private getDrmServersUrl() {
    const fairplayCertificateUrl = pathOr('', ['drm', 'fairplayCertificateUrl'], this.props.drmServerUrl);
    const fairplayLicenseUrl = pathOr('', ['drm', 'fairplayLicenseUrl'], this.props.drmServerUrl);
    const playReadyLicenseUrl = pathOr('', ['drm', 'playReadyLicenseUrl'], this.props.drmServerUrl);
    const widevineLicenseUrl = pathOr('', ['drm', 'widevineLicenseUrl'], this.props.drmServerUrl);
    return {fairplayCertificateUrl, fairplayLicenseUrl, playReadyLicenseUrl, widevineLicenseUrl};
  }

  private calculateStyleValue(appTheme: any, componentTheme: any, styleLabel: string) {
    const tmpValue = componentTheme[styleLabel];
    return tmpValue.startsWith('theme.') ? appTheme[tmpValue.replace('theme.', '')] : tmpValue;
  }

  // These styles need to be passed as props to material-ui component (menus) with no configurable mount point
  // So we can't symply use styled components as they are outside PlayerCompoennt scope
  private getCustomSkin() {
    const cTheme = this.props.componentTheme || {};
    const aTheme = this.props.theme || {};
    return {
      menusBackgroundColor: this.calculateStyleValue(aTheme, cTheme, 'menusBackgroundColor'),
      menusTextColor: this.calculateStyleValue(aTheme, cTheme, 'menusTextColor'),
      menusTextSize: this.calculateStyleValue(aTheme, cTheme, 'menusTextSize'),
      menusTextPadding: this.calculateStyleValue(aTheme, cTheme, 'menusTextPadding'),
      progressColor: this.calculateStyleValue(aTheme, cTheme, 'progressColor'),
      progressBackgroundColor: this.calculateStyleValue(aTheme, cTheme, 'progressBackgroundColor'),
      progressTextSize: this.calculateStyleValue(aTheme, cTheme, 'progressTextSize'),
      spinnerSize: this.calculateStyleValue(aTheme, cTheme, 'spinnerSize'),
      spinnerThickness: this.calculateStyleValue(aTheme, cTheme, 'spinnerThickness')
    };
  }

  private getEventHandlers() {
    return {
      clearPlayerInfo: this.props.clearPlayerInfo,
      onAudioLanguagesLoaded: this.props.setAudioLanguages,
      onBuffering: this.props.setBuffering,
      onClickBack: this.onClickBack,
      onError: this.props.setError,
      setFullscreen: this.props.setFullscreen,
      onPause: this.props.setPlayerAsPaused,
      onPlay: this.props.setPlayerAsPlay,
      onPlaying: this.props.setPlayerAsPlaying,
      onSeekToRange: this.props.onSeekToRange,
      onSubtitlesLoaded: this.props.setSubtitles,
      onUpdateChange: this.props.updateVolume,
      onVideoEnded: this.props.playNextInPlaylist.bind(null, this.context.location.pathname),
      onAudioLanguageChange: this.props.setAudioLang,
      updateVideoWatchTime: this.props.updateVideoWatchTime,
      updateVideoSubtitleLanguage: this.props.updateVideoSubtitleLanguage,
      updateVideoAudioLanguage: this.props.updateVideoAudioLanguage
    };
  }

  private getVideoId(querySearch?: string) {
    const localQuery = querySearch || pathOr('', ['location', 'search'], this.context);
    let {videoId} = getHtmlBundleParams(localQuery);
    const isEmpty = !videoId;
    videoId = isEmpty ? DEFAULT_VIDEO_ID : videoId;
    return videoId;
  }

  private getVideoItem() {
    const {adsPlaylist, currentAdIndex} = this.state;
    const adsPlaylistLength = path(['length'], adsPlaylist) as any;
    // Still loading ads
    if (isNaN(adsPlaylistLength)) {
      return undefined;
    }
    // if no ads or ads completed -> drm video
    if (adsPlaylistLength === 0 || currentAdIndex >= adsPlaylistLength) {
      return this.props;
    }
    // return current ad
    return path([currentAdIndex], adsPlaylist);
  }

  private onNetworkDisconnect = () => this.context.history.goBack();

  private isAd = (videoItem: PrerollAd) => !videoItem || videoItem.hasOwnProperty('blockFastForward');

  private isSimpleHtml5Video = (manifestUrl: string) => manifestUrl.endsWith('.mp4');

  private onAdEnded() {
    this.setState(({currentAdIndex}: MediaContainerState) => ({currentAdIndex: ++currentAdIndex}));
  }

  private async onRouteChange(nextRoute: any) {
    if (isEnvUXBuilder() && !nextRoute.search) {
      return;
    }
    const videoId = this.getVideoId(nextRoute.search);
    const isTrailer = nextRoute.search.indexOf('&trailer') !== -1;

    this.setState({...initialState, videoId, isTrailer}, () => {
      this.props.getPreRollAds(videoId);
      this.props.clearPlayerInfo();
      this.props.setCurrentItem({uid: videoId, type: TYPES.VIDEO_ITEM});
      pauseAudioPlayer();
    });
  }

  private shouldIFetchMovie = () => {
    const {adsPlaylist, currentAdIndex} = this.state;
    const {isLoadingToken, drmToken, fileManifest} = this.props;
    const ads = adsPlaylist === undefined ? {} : adsPlaylist;
    const noPrerolls = Array.isArray(ads) && ads.length === 0 && currentAdIndex == -1;
    const prerollsFinished = Array.isArray(ads) && currentAdIndex >= ads.length;
    return !isLoadingToken && !drmToken && !this.isSimpleHtml5Video(fileManifest) && (noPrerolls || prerollsFinished);
  };

  private shouldIFetchDrmConfig = (props: MediaContainerProps) => !props.isLoadingDrmServer && !props.drmServerUrl;

  private onClickBack = () => this.context.history.goBack();
}

export default withTheme(MediaPlayerContainer as any);
