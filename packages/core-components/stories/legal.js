import React from 'react';
import cloneDeep from 'lodash/cloneDeep';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {array, boolean, number, object, text, withKnobs} from '@storybook/addon-knobs/react';
import Legal from '../dist/components/Legal/Legal.js';
import Accordion from '../dist/components/Accordion/Accordion.js';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    motifTextColor: 'theme.primaryTextColor',
    motifTextFontSize: 'theme.tertiaryFontSize',
    motifTextMargin: {xs: '20px auto auto auto'},
    motifTextFontWeight: 'normal',
    highlightTextColor: 'theme.primaryTextColor',
    highlightTextFontSize: '24px',
    highlightTextFontWeight: 'bold',
    noticeHeaderTextColor: 'theme.secondaryTextColor',
    noticeHeaderFontSize: 'theme.tertiaryFontSize',
    noticeHeaderFontWeight: 'bold',
    noticeHeaderMargin: {xs: '40px auto 20px auto'},
    noticeTextColor: 'theme.secondaryTextColor',
    noticeTextFontSize: 'theme.primaryFontSize',
    noticeTextFontWeight: 'normal',
    noticeTextPadding: {xs: '20px 0px 20px 0px'}
  },
  feature: {
    headerImage: {
      xs: './images/ua-background.svg'
    }
  },
  content: {
    header: {
      motifText: 'Terms and Service',
      motifHighlightText: 'Important',
      noticeHeader: '',
      headerText: 'Terms of Service',
      noticeText: 'IMPORTANT NOTICE: PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION.'
    },
    effectiveDate: {
      effectiveDateText: 'Effective Date: August 1, 2017'
    },
    sections: [
      {
        title: 'Section title',
        isOpen: true,
        content: ['Paragraph 1', 'Paragraph 2']
      }
    ]
  }
};

const accordionDefaultProps = {
  componentTheme: {
    textColor: 'theme.secondaryTextColor',
    headerTextColor: 'theme.secondaryTextColor',
    headerFontSize: 'theme.primaryFontSize',
    seperatorColor: '#1694CA',
    sectionTextColor: 'theme.secondaryTextColor',
    sectionFontSize: 'theme.primaryFontSize',
    toggleOpenIconColor: 'theme.primaryTextColor',
    toggleClosedIconColor: 'theme.secondaryTextColor'
  }
};

const sampleProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor'
  },
  feature: {
    headerImage: {
      xs: './images/ua-background.svg'
    }
  },
  content: {
    header: {
      motifText: 'Viasat Terms of Service',
      motifHighlightText: 'Important',
      noticeHeader: 'IMPORTANT',
      headerText: 'Terms of Service',
      noticeText: 'IMPORTANT NOTICE: PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION.'
    },
    effectiveDate: {
      effectiveDateText: 'Effective Date: August 1, 2017'
    },
    sections: [
      {
        title: 'Introduction.',
        isOpen: true,
        paragraphs: [
          'This Terms of Service Agreement (the \u201CAgreement\u201D) is between you and Viasat, Inc. (\u201CViasat,\u201D \u201CService Provider,\u201D \u201Cus,\u201D or \u201Cwe\u201D), and governs your use of WiFi and internet services provided by Viasat (the \u201DService\u201D) to which you are connecting on the airline operator\'s aircraft from which you are connecting to such Service (the \u201CAirline\u201D) (collectively the "Service"). Your acceptance below and continued use of the Service represents your agreement to the terms set forth in this Agreement. If you do not agree with the terms set forth in this Agreement, immediately cease using the Service. If you would like to contact Service Provider, you may write to:',
          'Viasat, Inc.<br/>2908 Finfeather Road<br/>Bryan, TX 77801<br/>USA'
        ]
      },
      {
        title: 'Using The Service.',
        isOpen: true,
        paragraphs: [
          'In exchange for access to and use of the Service, you: (a) agree to provide Service Provider with accurate and complete registration information, if requested, and to notify Service Provider of changes to your registration information; (b) agree to protect the password, username and security information you use to access the Service and to notify Service Provider immediately of any unauthorized use of your account that you become aware of; (c) agree to comply with applicable laws and regulations, including but not limited to copyright and intellectual property rights laws; and (d) represent that you are at least 18 years of age.'
        ]
      },
      {
        title: 'Billing Terms and Payment.',
        isOpen: false,
        paragraphs: [
          'General Billing Terms.  A billing period generally starts on the day and time you log in following the completion of the registration process (the \u201CBilling Commencement Date\u201D). The Billing Commencement Date will be the day and time you log in. You will be logged off of the Service when (i) you click the \u201CLogout\u201D button (\u201CIndividual Logout\u201D); or (ii) the system automatically logs you off because your session time has expired, your device has been powered off or has been inactive for an extended period of time, or Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination (\u201CAutomatic Logout\u201D).  The billing period ends on upon the earliest to occur of an Individual Logout or Automatic Logout.',
          'Single Session Pay Per Use Plan.  At the start of each session, we will charge all fees related to your use of the Service, including taxes, surcharges or other assessments applicable to the Service (\u201CService Fees\u201D) to your credit card, debit card or other payment method (a \u201CCard Payment\u201D).',
          'Pay Per Flight Plan.  Each pay per flight session begins on the Billing Commencement Date and ends at the point in time when Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination. At the start of each session, we will charge all Service Fees related to your use of the Service to your Card Payment.',
          'Roaming Fees.  If you are a subscriber of another service provider that has a contractual relationship allowing that service provider\u2019s subscribers to roam on Viasat\u2019s Wi-Fi network, your service provider may charge you a roaming fee for access to Viasat\u2019s Wi-Fi network.',
          'Payment Terms.  You agree to pay all Service Fees in accordance with the provisions of the Service plan you selected. You authorize Service Provider to charge your Card Payment for payment of all, or any portion of your Service fees, until such amounts are paid in full. Your card issuer agreement governs use of your Card Payment in connection with this Service; please refer to that agreement for your rights and liabilities as a cardholder. If we do not receive payment from your credit or debit card issuer or its agent, you agree to pay us all amounts due upon demand by us. You agree that we will not be responsible for any expenses that you may incur resulting from overdrawing your bank account or exceeding your credit limit as a result of an automatic charge made under this Agreement. ',
          'Billing Errors and Collections.  If you think a charge is incorrect or you need more information on any charges applied to your account, you should contact us through inflight.Viasat.com or by calling +(00) 1 888-649-6711 within 60 days of receiving the statement on which the error or problem appeared. We will not pay you interest on any overcharged amounts later refunded or credited to you.  If we choose to use a collection agency or attorney to collect money that you owe us or to assert any other right that we may have against you, you agree to pay the reasonable costs of collection or other action including, without limitation, collection agency fees, reasonable attorneys\u2019 fees, and court costs. '
        ]
      }
    ]
  }
};

mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles = "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';

const stories = storiesOf('[CMOB-18402] Legal', module);

stories.addDecorator(withKnobs);

stories
  .add('Legal - Default Props', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Legal {...defaultProps} />
    </PortalComponentWrapper>
  ))
  .add('Legal - Without effective date', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.effectiveDate.effectiveDateText = '';

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Legal {...testProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Legal - With three sections', () => {
    const testProps = cloneDeep(defaultProps);
    testProps.content.sections = [
      {
        title: 'Introduction.',
        isOpen: true,
        paragraphs: [
          'This Terms of Service Agreement (the \u201CAgreement\u201D) is between you and Viasat, Inc. (\u201CViasat,\u201D \u201CService Provider,\u201D \u201Cus,\u201D or \u201Cwe\u201D), and governs your use of WiFi and internet services provided by Viasat (the \u201DService\u201D) to which you are connecting on the airline operator\'s aircraft from which you are connecting to such Service (the \u201CAirline\u201D) (collectively the "Service"). Your acceptance below and continued use of the Service represents your agreement to the terms set forth in this Agreement. If you do not agree with the terms set forth in this Agreement, immediately cease using the Service. If you would like to contact Service Provider, you may write to:',
          'Viasat, Inc.<br/>2908 Finfeather Road<br/>Bryan, TX 77801<br/>USA'
        ]
      },
      {
        title: 'Using The Service.',
        isOpen: true,
        paragraphs: [
          'In exchange for access to and use of the Service, you: (a) agree to provide Service Provider with accurate and complete registration information, if requested, and to notify Service Provider of changes to your registration information; (b) agree to protect the password, username and security information you use to access the Service and to notify Service Provider immediately of any unauthorized use of your account that you become aware of; (c) agree to comply with applicable laws and regulations, including but not limited to copyright and intellectual property rights laws; and (d) represent that you are at least 18 years of age.'
        ]
      },
      {
        title: 'Billing Terms and Payment.',
        isOpen: true,
        paragraphs: [
          'General Billing Terms.  A billing period generally starts on the day and time you log in following the completion of the registration process (the \u201CBilling Commencement Date\u201D). The Billing Commencement Date will be the day and time you log in. You will be logged off of the Service when (i) you click the \u201CLogout\u201D button (\u201CIndividual Logout\u201D); or (ii) the system automatically logs you off because your session time has expired, your device has been powered off or has been inactive for an extended period of time, or Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination (\u201CAutomatic Logout\u201D).  The billing period ends on upon the earliest to occur of an Individual Logout or Automatic Logout.',
          'Single Session Pay Per Use Plan.  At the start of each session, we will charge all fees related to your use of the Service, including taxes, surcharges or other assessments applicable to the Service (\u201CService Fees\u201D) to your credit card, debit card or other payment method (a \u201CCard Payment\u201D).',
          'Pay Per Flight Plan.  Each pay per flight session begins on the Billing Commencement Date and ends at the point in time when Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination. At the start of each session, we will charge all Service Fees related to your use of the Service to your Card Payment.',
          'Roaming Fees.  If you are a subscriber of another service provider that has a contractual relationship allowing that service provider\u2019s subscribers to roam on Viasat\u2019s Wi-Fi network, your service provider may charge you a roaming fee for access to Viasat\u2019s Wi-Fi network.',
          'Payment Terms.  You agree to pay all Service Fees in accordance with the provisions of the Service plan you selected. You authorize Service Provider to charge your Card Payment for payment of all, or any portion of your Service fees, until such amounts are paid in full. Your card issuer agreement governs use of your Card Payment in connection with this Service; please refer to that agreement for your rights and liabilities as a cardholder. If we do not receive payment from your credit or debit card issuer or its agent, you agree to pay us all amounts due upon demand by us. You agree that we will not be responsible for any expenses that you may incur resulting from overdrawing your bank account or exceeding your credit limit as a result of an automatic charge made under this Agreement. ',
          'Billing Errors and Collections.  If you think a charge is incorrect or you need more information on any charges applied to your account, you should contact us through inflight.Viasat.com or by calling +(00) 1 888-649-6711 within 60 days of receiving the statement on which the error or problem appeared. We will not pay you interest on any overcharged amounts later refunded or credited to you.  If we choose to use a collection agency or attorney to collect money that you owe us or to assert any other right that we may have against you, you agree to pay the reasonable costs of collection or other action including, without limitation, collection agency fees, reasonable attorneys\u2019 fees, and court costs. '
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Legal {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </Legal>
      </PortalComponentWrapper>
    );
  })
  .add('Legal - External link and bold styled text in the introduction paragraph', () => {
    const testProps = {...defaultProps};
    testProps.feature = {...defaultProps.feature};
    testProps.content = {...defaultProps.content};
    testProps.content.sections = [
      {
        title: 'Introduction.',
        isOpen: true,
        paragraphs: [
          'I am an <a href="https://www.google.com/">external link</a>. Looks like we can make <strong> text</strong> & link <strong>  <a href="https://www.google.com/">strong</a></strong> '
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Legal {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </Legal>
      </PortalComponentWrapper>
    );
  })
  .add('Legal - External link (open in new tab/window) and bold styled text in the introduction paragraph', () => {
    const testProps = {...defaultProps};
    testProps.feature = {...defaultProps.feature};
    testProps.content = {...defaultProps.content};
    testProps.content.sections = [
      {
        title: 'Introduction.',
        isOpen: true,
        paragraphs: [
          'I am an <a target="_blank" href="https://www.google.com/">external link</a>. Looks like we can make <strong> text</strong> & link <strong>  <a href="https://www.google.com/">strong</a></strong> '
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Legal {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </Legal>
      </PortalComponentWrapper>
    );
  })
  .add('Legal - Sanitize bad text in the introduction paragraph', () => {
    const testProps = {...defaultProps};
    testProps.feature = {...defaultProps.feature};
    testProps.content = {...defaultProps.content};
    testProps.content.sections = [
      {
        title: 'Introduction.',
        isOpen: true,
        paragraphs: [
          'There is a script tag with alert here( <script>alert();</script> ). If nothing is showing up inside the bracket then the html sanitizer is working properly'
        ]
      }
    ];

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Legal {...testProps}>
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </Legal>
      </PortalComponentWrapper>
    );
  })
  .add('Legal - With Knob', () => {
    const testProps = {...sampleProps};
    testProps.feature = {...defaultProps.feature};
    testProps.content = {...defaultProps.content};
    testProps.content.sections = sampleProps.content.sections;
    testProps.feature.headerImage = sampleProps.feature.headerImage;

    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <Legal
          componentTheme={object('componentTheme', testProps.componentTheme)}
          feature={object('feature', testProps.feature)}
          content={object('content', testProps.content)}
        >
          {[
            <div>
              <Accordion {...accordionDefaultProps} />
            </div>
          ]}
        </Legal>
      </PortalComponentWrapper>
    );
  });
