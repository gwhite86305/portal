import * as React from 'react';
import {ThemeSchema} from '../components/common/ThemeInterface';
import isEmpty from 'lodash-es/isEmpty';
import isEqual from 'lodash-es/isEqual';

const PORTAL_FONT_FILES_ID = 'portal-font-files';
const PORTAL_FONTICON_FILES_ID = 'portal-fonticon-files';
const PORTAL_FONTICON_CSS_ID = 'portal-fonticon-css';

const _createFontFiles = (id: string, fontFamily: string | string[], fontFiles: string | string[] | any) => {
  // remove if exists
  const existingCustomFontFiles = document.getElementById(id);
  if (existingCustomFontFiles && existingCustomFontFiles.parentNode) {
    existingCustomFontFiles.parentNode.removeChild(existingCustomFontFiles);
  }

  const customFontFiles = document.createElement('style');
  customFontFiles.setAttribute('id', id);
  customFontFiles.setAttribute('type', 'text/css');
  if (typeof fontFamily === 'string') {
    fontFamily = [fontFamily as string];
  }
  if (typeof fontFiles === 'string') {
    fontFiles = [fontFiles as string];
  }
  if (fontFamily.length > 0 && fontFamily.length === fontFiles.length) {
    fontFamily.forEach((val, index, array) => {
      if (typeof fontFiles[index] === 'object' && fontFiles[index].length > 1) {
        for (let i = 0; i < fontFiles[index].length; i++) {
          customFontFiles.appendChild(
            document.createTextNode(
              `@font-face {font-family:${fontFamily[index]};font-weight:${fontFiles[index][i].weight};src:${
                fontFiles[index][i].file
              }}`
            )
          );
        }
      } else {
        customFontFiles.appendChild(
          document.createTextNode(`@font-face{font-family:${fontFamily[index]};src:${fontFiles[index]}}`)
        );
      }
    });
  } else {
    throw 'fontFamily and fontFiles must be the same length';
  }
  document.head.appendChild(customFontFiles);
};

const _createCssLink = (id: string, href: string) => {
  // remove if exists
  const existingCustomCssLink = document.getElementById(id);
  if (existingCustomCssLink && existingCustomCssLink.parentNode) {
    existingCustomCssLink.parentNode.removeChild(existingCustomCssLink);
  }

  const customFontIconCss = document.createElement('link');
  customFontIconCss.setAttribute('id', id);
  customFontIconCss.setAttribute('rel', 'stylesheet');
  customFontIconCss.setAttribute('href', href);
  document.head.appendChild(customFontIconCss);
};

const applyFontTheme = (theme: ThemeSchema) => {
  const fontFamilies = [theme.appFontFamily, theme.secondaryFontFamily].filter(val => {
    return val;
  }) as string[];
  const fontFiles = [theme.appFontFiles, theme.secondaryFontFiles].filter(val => {
    return val;
  }) as any;
  if(theme.extraFonts && Object.keys(theme.extraFonts).length) {
    const extraFonts = theme.extraFonts as any as {[key: string]: any};
    // NOTE(william.moore@viasat.com): this is super janky.
    //   I feel like the schema compiler isn't capturing things for extraFonts as it assigns it the type "string" in TS
    //   despite it being type "object" in JSON schema with additionalProperties.
    Object.keys(extraFonts).forEach((key:string) => {
      fontFamilies.push(key);
      fontFiles.push(extraFonts[key]);
    });
  }
  if (!isEmpty(theme.appFontFiles)) {
    _createFontFiles(
      PORTAL_FONT_FILES_ID,
      fontFamilies,
      fontFiles
    );
  }

  if (!isEmpty(theme.fontIconFiles)) {
    _createFontFiles(PORTAL_FONTICON_FILES_ID, theme.fontIconFontFamily, theme.fontIconFiles);
  }

  if (!isEmpty(theme.fontIconCss)) {
    _createCssLink(PORTAL_FONTICON_CSS_ID, theme.fontIconCss);
  }
};

const hasFontThemeApplied = () => {
  return document.getElementById(PORTAL_FONT_FILES_ID) !== null;
};

const hasFontChanged = (currentTheme: ThemeSchema, newTheme: ThemeSchema) => {
  return (
    newTheme.defaultFontSize !== currentTheme.defaultFontSize ||
    newTheme.appFontFamily !== currentTheme.appFontFamily ||
    newTheme.secondaryFontFamily !== currentTheme.secondaryFontFamily ||
    newTheme.fontIconFiles !== currentTheme.fontIconFiles ||
    newTheme.fontIconCss !== currentTheme.fontIconCss ||
    !isEqual(newTheme.appFontFiles, currentTheme.appFontFiles) ||
    !isEqual(newTheme.secondaryFontFiles, currentTheme.secondaryFontFiles) ||
    !isEqual(newTheme.fontIconFontFiles, currentTheme.fontIconFontFiles) ||
    !isEqual(newTheme.extraFontFiles, currentTheme.extraFontFiles)
  );
};

export default {
  applyFontTheme,
  hasFontThemeApplied,
  hasFontChanged
};
