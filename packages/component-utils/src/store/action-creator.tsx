export class ActionCreator<T, P, M = any> {
  readonly type: T;
  readonly payload: P;

  constructor(type: T) {
    this.type = type;
  }
  create = (payload: P, meta?: M) => ({type: this.type, payload, meta});
}
