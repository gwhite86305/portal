import * as Stomp from '@stomp/stompjs';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {IFlightInfo, IFlightProgress, IMessage} from './types';
import {pauseSingleAudio, playSingleAudio} from '../audio/actions';
import {AUDIO_TYPE, AudioItem} from '../audio/types';
import {MEDIA_PLAYER_STATUS} from '../mediaplayer/types';
import {IRootState} from '../..';
import {path} from 'ramda';
import * as endPointConfig from '../../utils/config.json';
import * as CONFIG from '../../utils/config.json';

interface IConfig {
  [property: string]: string;
}

export const ActionCreators = {
  initConnection: new ActionCreator<'initConnection', any>('initConnection'),
  updateFlightProgress: new ActionCreator<'updateFlightProgress', any>('updateFlightProgress'),
  updateFlightInfo: new ActionCreator<'updateFlightInfo', any>('updateFlightInfo'),
  updateMessages: new ActionCreator<'updateMessages', any>('updateMessages'),
  setAudioPausedForAnnouncement: new ActionCreator<'setAudioPausedForAnnouncement', any>(
    'setAudioPausedForAnnouncement'
  )
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function initConnection() {
  return (dispatch: any) => {
    const client = new Stomp.Client({brokerURL: (endPointConfig as IConfig).NOTIFICATIONS_IFE_WS_BASE_URL});
    client.onConnect = function() {
      if (!(CONFIG as any).PA_TURNED_OFF) {
        client.subscribe('/topic/messages', payload => {
          const activeIframes = Array.from(document.getElementsByTagName('iframe'));
          activeIframes.forEach((iframe: any) => {
            const {name, value} = JSON.parse(payload.body);
            //format to be the same as V1 and send to iframe
            iframe.contentWindow.postMessage({type: name, pa: value}, '*');
          });
          dispatch(updateMessages(JSON.parse(payload.body)));
        });
        client.subscribe('/topic/flight/info', payload =>
          dispatch(ActionCreators.updateFlightInfo.create(JSON.parse(payload.body) as IFlightInfo))
        );
        client.subscribe('/topic/flight/progress', payload =>
          dispatch(ActionCreators.updateFlightProgress.create(JSON.parse(payload.body) as IFlightProgress))
        );
      }
    };
    client.activate();
  };
}

export function updateMessages(message: IMessage) {
  return (dispatch: any, getState: any) => {
    dispatch(ActionCreators.updateMessages.create(message));
    if (path(['ifeMediaPlayer', 'currentItem', 'type'], getState()) === AUDIO_TYPE.ITEM) {
      message.value ? pauseAudio(dispatch, getState) : resumeAudio(dispatch, getState);
    }
  };
}

const pauseAudio = function(dispatch: any, getState: () => IRootState) {
  const {ifeMediaPlayer} = getState();
  if ((ifeMediaPlayer.status as MEDIA_PLAYER_STATUS) === MEDIA_PLAYER_STATUS.PLAYING) {
    dispatch(ActionCreators.setAudioPausedForAnnouncement.create(true));
    dispatch(pauseSingleAudio());
  }
};

const resumeAudio = function(dispatch: any, getState: () => IRootState) {
  const {ifeMediaPlayer, ifeAudio, ifeNotificationDispatcher} = getState();
  if (
    (ifeMediaPlayer.status as MEDIA_PLAYER_STATUS) === MEDIA_PLAYER_STATUS.PAUSED &&
    ifeNotificationDispatcher.isAudioPausedForAnnouncement
  ) {
    dispatch(ActionCreators.setAudioPausedForAnnouncement.create(false));
    dispatch(
      playSingleAudio(ifeMediaPlayer.currentItem as AudioItem, ifeMediaPlayer, ifeAudio, ifeNotificationDispatcher)
    );
  }
};
