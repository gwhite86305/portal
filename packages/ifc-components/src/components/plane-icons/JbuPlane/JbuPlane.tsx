import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {JbuPlane as ExternalInterface} from '../../common/ComponentsInterface';
import {default as PlaneIcon} from '../PlaneIcon';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
}

const Component = (props: any) => {
  const Container = styled.div`
    .jbu-plane {
      float: right;
      position: absolute;
      height: 40px;
      top: -15px;
    }
    .default-plane {
      ${renderThemeProperty('progressBarPlaneFontSize', 'font-size')};
      ${renderThemeProperty('progressBarForegroundColor', 'color')};
      float: right;
      position: absolute;
      transform: rotate(45deg) translateX(-8px) translateY(-2px);
    }
  `;

  let image_name = props.feature.imageMap[props.device.tailId.toLowerCase()];
  let default_image = props.feature.imageMap['default'];
  if (image_name) {
    return (
      <Container>
        <img className="jbu-plane" src={image_name} />
      </Container>
    );
  } else {
    console.warn('Warning: No tail image found for "' + props.device.tailId.toLowerCase() + '"');
    return (
      <Container>
        <img className="jbu-plane" src={default_image} />
      </Container>
    );
  }
};

@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps};

  props.device = state.device;

  return props;
})
class JbuPlane extends PlaneIcon {
  public static defaultProps = defaults;
  public render() {
    return <Component {...this.props} />;
  }
}

export default JbuPlane;
