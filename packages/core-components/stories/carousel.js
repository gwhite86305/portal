/* eslint-disable */
import {array, object, withKnobs} from '@storybook/addon-knobs/react';
import {storiesOf} from '@storybook/react';
import PortalComponentWrapper from 'component-utils/dist/utils/PortalComponentWrapper';
import React from 'react';
import Carousel from '../dist/components/Carousel/Carousel';

// HELPERS
const commonSlideStyle = {width: '100%', height: '100%'};
const portalComponentWrapperStyle = {position: 'relative'};
const containerStyle = {width: '493px', height: '334px'};
const buildChildren = childrenStyle =>
  childrenStyle.map((style, index) => <div key={index} style={{...commonSlideStyle, ...style}} />);

// STORIES
const stories = storiesOf('Carousel Component', module);
stories.addDecorator(withKnobs);
stories.add('Carousel - Default', () => {
  const defaultProps = {
    componentTheme: {
      arrowTopPosition: {
        xs: '50%'
      },
      arrowsColor: 'theme.primaryTextColor',
      arrowsHeight: '5px',
      arrowsMargin: '5px',
      arrowsThikness: '2px',
      bulletButtonBarColor: 'transparent',
      bulletButtonsBorderColor: 'theme.primaryTextColor',
      bulletButtonsBorderColorThikness: '1px',
      bulletButtonsColor: 'transparent',
      bulletButtonsMargin: '10px',
      bulletButtonsSelectedColor: 'theme.primaryTextColor',
      bulletButtonsSize: '5px'
    }
  };
  const childrenStyle = [{backgroundColor: 'coral'}, {backgroundColor: 'lightgreen'}, {backgroundColor: 'lightblue'}];
  return (
    <div style={containerStyle}>
      <PortalComponentWrapper style={portalComponentWrapperStyle}>
        <Carousel {...object('props', defaultProps)}>{buildChildren(object('children', childrenStyle))}</Carousel>
      </PortalComponentWrapper>
    </div>
  );
});

stories.add('Carousel - AA Style', () => {
  const AAProps = {
    componentTheme: {
      arrowTopPosition: {
        xs: '94%'
      },
      arrowsColor: '#919191',
      arrowsHeight: '4px',
      arrowsMargin: '11px',
      arrowsThikness: '4px',
      bulletButtonBarColor: '#2F2F2F',
      bulletButtonsBorderColor: '#ffffff',
      bulletButtonsBorderColorThikness: '1px',
      bulletButtonsColor: 'transparent',
      bulletButtonsMargin: '13px 5px 13px 5px',
      bulletButtonsSelectedColor: '#ffffff',
      bulletButtonsSize: '6px'
    }
  };
  const childrenStyle = [{backgroundColor: 'coral'}, {backgroundColor: 'lightgreen'}, {backgroundColor: 'lightblue'}];

  return (
    <div style={containerStyle}>
      <PortalComponentWrapper style={portalComponentWrapperStyle}>
        <Carousel {...object('props', AAProps)}>{buildChildren(object('children', childrenStyle))}</Carousel>
      </PortalComponentWrapper>
    </div>
  );
});
