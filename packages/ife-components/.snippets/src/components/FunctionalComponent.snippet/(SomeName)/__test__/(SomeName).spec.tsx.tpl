import {shallow} from 'enzyme';
import * as React from 'react';
import {<%= SomeName %>} from '../<%= SomeName %>';

const textStub = 'text';

describe('<%= SomeName %>.tsx', () => {
    it('renders with given text', () => {
        const component = shallow(<<%= SomeName %> text={textStub} />);
        expect(component.text()).toBe(textStub);
    })
})
