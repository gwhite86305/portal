import CircularProgress from '@material-ui/core/CircularProgress';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ClosedCaptionIcon from '@material-ui/icons/ClosedCaption';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import LanguageIcon from '@material-ui/icons/Language';
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import * as React from 'react';
import {fromEvent, merge, Subject, timer} from 'rxjs';
import {switchMapTo, takeUntil, tap, throttleTime} from 'rxjs/operators';
import {MEDIA_PLAYER_STATUS} from '../../store/mediaplayer/types';
import * as defaultProps from './defaultProps.json';
import CentralPlayButton from './Skin/CentralPlayButton';
import HorizontalRange from './Skin/HorizontalRange';
import Language from './Skin/Language';
import SkipButton from './Skin/SkipButton';
import {VideoPlayerContainer, VideoPlayerControls} from './styleComponent';
import {MediaPlayerProps, MediaPlayerState} from './types';
import {isFullScreenForbidden} from './utils/container';
import {getTimeString, isShaka, isIOS} from './utils/playerCommon';
import ShakaPlayer from './VideoPlayers/ShakaPlayer';
import WebKitPlayer from './VideoPlayers/WebKitPlayer';
import {utils} from 'component-utils';

export default class MediaPlayer extends React.PureComponent<MediaPlayerProps, MediaPlayerState> {
  public static defaultProps = defaultProps;

  private unsubscribe$: Subject<boolean> = new Subject();
  private stopMovie$: Subject<boolean> = new Subject();

  public state = {
    showControls: true,
    isPausedByUser: false,
    duration: 0,
    currentTime: 0
  };

  onTimeUpdate = (uid: string, currentTime: number, duration: number) => {
    this.setState({...this.state, currentTime, duration});
  };

  applyFullScreen = () => this.props.setFullscreen(false);

  removeFullScreen = () => this.props.setFullscreen(true);

  turnOffSound = () => this.props.onUpdateChange(0);

  turnOnSound = () => this.props.onUpdateChange(1);

  componentDidMount() {
    this.initFullScreenEnv();
    merge(fromEvent(document, 'mousemove'), fromEvent(document, 'touchstart'), fromEvent(document, 'touchmove'))
      .pipe(
        throttleTime(200),
        tap(_ => this.setState({showControls: true})),
        switchMapTo(timer(1500)),
        tap(_ => this.setState({showControls: false})),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();
  }

  componentWillUnmount() {
    const {advertId, videoId, updateVideoConfig} = this.props;
    const {currentTime} = this.state;
    this.destroyFullScreenEnv();
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
    if (!advertId) {
      updateVideoConfig({uid: videoId, watchTime: Math.round(currentTime)});
    }
  }

  componentDidUpdate(prevProps: MediaPlayerProps) {
    const {isPausedForNotification} = this.props;
    const {isPausedByUser} = this.state;

    if (isPausedForNotification !== prevProps.isPausedForNotification) {
      if (isPausedForNotification) {
        this.pauseDomEl();
      } else if (!isPausedByUser) {
        this.playDomEl();
      }
    }
  }

  render() {
    const {
      blockFastForward,
      progressTextSize,
      updateVideoConfig,
      isTrailer,
      fileManifest,
      drmToken,
      volume,
      audioLanguage,
      subtitleLanguage,
      advertId,
      isPausedForNotification,
      fairplayCertificateUrl,
      fairplayLicenseUrl,
      currentTime,
      widevineLicenseUrl,
      playReadyLicenseUrl,
      videoId,
      seekToRange,
      setFullscreen,
      onPlay,
      onPlaying,
      onPause,
      onVideoEnded,
      onBuffering,
      onAudioLanguagesLoaded,
      onSubtitlesLoaded,
      onError,
      reportEvent,
      onClickBack,
      skippableAfter,
      onSeekToRange,
      progressBackgroundColor,
      progressColor,
      menusBackgroundColor,
      audioLanguages,
      menusTextColor,
      menusTextPadding,
      menusTextSize,
      subtitles,
      isFullScreen
    } = this.props;

    const {showControls, duration} = this.state;
    const customSkinProps = {
      showControls,
      blockFastForward,
      progressTextSize,
      isShaka
    };

    const Player = isShaka ? ShakaPlayer : WebKitPlayer;

    const updateSubtitleLanguage = ({uid, lang}: {uid: string; lang: string}) =>
      updateVideoConfig({uid, subtitleLanguage: lang});

    const updateAudioLanguage = ({uid, lang}: {uid: string; lang: string}) =>
      updateVideoConfig({uid, audioLangauge: lang});

    const handleClickBack = () => {
      this.props.onClickBack();
      this.stopMovie$.next(true);
      this.stopMovie$.complete();
    };

    const displayAudioControl = () => {
      if (isIOS) {
        return;
      }
      return this.props.volume === 0 ? (
        <VolumeOffIcon onClick={this.turnOnSound} />
      ) : (
        <VolumeUpIcon onClick={this.turnOffSound} />
      );
    };

    return (
      <VideoPlayerContainer {...customSkinProps}>
        <div className="back-button" onClick={handleClickBack} children={<ArrowBackIcon />} />
        {this.props.fileManifest && (
          <Player
            updateVideoConfig={updateVideoConfig}
            isTrailer={isTrailer}
            fileManifest={fileManifest}
            prevfileManifest={fileManifest}
            drmToken={drmToken}
            volume={volume}
            audioLanguage={audioLanguage}
            subtitleLanguage={subtitleLanguage}
            advertId={advertId}
            isPausedForNotification={isPausedForNotification}
            fairplayCertificateUrl={fairplayCertificateUrl || ''}
            fairplayLicenseUrl={fairplayLicenseUrl || ''}
            currentTime={currentTime}
            onTimeUpdate={this.onTimeUpdate}
            widevineLicenseUrl={widevineLicenseUrl || ''}
            playReadyLicenseUrl={playReadyLicenseUrl || ''}
            videoId={videoId}
            seekToRange={seekToRange}
            setFullscreen={setFullscreen}
            onPlay={onPlay}
            onPlaying={onPlaying}
            onPause={onPause}
            onVideoEnded={onVideoEnded}
            onBuffering={onBuffering}
            onAudioLanguagesLoaded={onAudioLanguagesLoaded}
            onSubtitlesLoaded={onSubtitlesLoaded}
            onError={onError}
            reportEvent={reportEvent}
            playbackSessionId={this.props.playbackSessionId}
            stopMovie$={this.stopMovie$}
            updateSubtitleLanguage={updateSubtitleLanguage}
            updateAudioLanguage={updateAudioLanguage}
          />
        )}
        {this.props.isBuffering ? (
          <CircularProgress
            className="circular-progress"
            size={parseInt(this.props.spinnerSize)}
            thickness={parseInt(this.props.spinnerThickness)}
          />
        ) : null}
        <CentralPlayButton status={this.props.status} onPlay={this.onClickPlay} />
        <SkipButton
          blockFastForward={!!blockFastForward}
          currentTime={this.state.currentTime}
          skippableAfter={skippableAfter}
          uid={fileManifest}
        />
        <VideoPlayerControls {...customSkinProps}>
          <div className="progress">
            <HorizontalRange
              currentTime={this.state.currentTime}
              duration={duration}
              blockFastForward={!!blockFastForward}
              onSeekToRange={onSeekToRange}
              progressBackgroundColor={progressBackgroundColor}
              progressColor={progressColor}
              progressTextSize={progressTextSize}
            />
          </div>
          <div className="actions-buttons">
            <div className="group">
              {this.props.status === MEDIA_PLAYER_STATUS.PLAYING ? (
                <PauseIcon onClick={this.onClickPause} />
              ) : (
                <PlayArrowIcon onClick={this.onClickPlay} />
              )}
              {displayAudioControl()}
              <div className="progress-time">
                {getTimeString(this.state.currentTime)} / {getTimeString(duration)}
              </div>
            </div>
            <div className="group">
              <Language
                selectedLang={audioLanguage}
                icon={<LanguageIcon />}
                onSelection={updateAudioLanguage}
                backgroundColor={menusBackgroundColor}
                list={audioLanguages}
                textColor={menusTextColor}
                textPadding={menusTextPadding}
                textSize={menusTextSize}
                videoId={videoId}
              />
              <Language
                selectedLang={subtitleLanguage}
                backgroundColor={menusBackgroundColor}
                icon={<ClosedCaptionIcon />}
                list={subtitles}
                onSelection={updateSubtitleLanguage}
                textColor={menusTextColor}
                textPadding={menusTextPadding}
                textSize={menusTextSize}
                videoId={videoId}
              />
              {!isFullScreenForbidden(this.props as any) ? (
                isFullScreen ? (
                  <FullscreenExitIcon onClick={this.applyFullScreen} />
                ) : (
                  <FullscreenIcon onClick={this.removeFullScreen} />
                )
              ) : null}
            </div>
          </div>
        </VideoPlayerControls>
      </VideoPlayerContainer>
    );
  }

  private initFullScreenEnv = () => {
    requestAnimationFrame(() => {
      document.body.style.overflow = 'hidden';
      document.body.style.position = 'absolute';
      document.body.style.left = '0px';
    });
  };

  private destroyFullScreenEnv = () => {
    requestAnimationFrame(() => {
      document.body.style.overflow = 'auto';
      document.body.style.position = 'auto';
      document.body.style.left = 'auto';
    });
  };

  private onClickPlay = () => {
    if (!this.props.isPausedForNotification) {
      this.setState({isPausedByUser: false});
      this.playDomEl();
    }
  };

  private onClickPause = () => {
    this.setState({isPausedByUser: true});
    this.pauseDomEl();
  };

  private playDomEl = () => {
    if (utils.mobileSdk.isUsingMobileSdk()) {
      return;
    }
    const video = document.querySelector('video') as any;
    video && video.play();
  };

  private pauseDomEl = () => {
    const video = document.querySelector('video') as any;
    video && video.pause();
  };
}
