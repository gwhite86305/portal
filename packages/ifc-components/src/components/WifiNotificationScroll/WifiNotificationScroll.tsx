import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {WifiNotificationScroll as ExternalInterface} from '../common/ComponentsInterface';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';
import Marquee from '../common/Marquee';

export interface InternalInterface {
  className?: string;
  text?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('position', 'position')};
  ${renderThemeProperty('textColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};

  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  display: flex;
  width: 100%;
  flex-grow: 1;
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.56);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.56);
`;

class WifiNotificationScroll extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-wifinotificationscroll-component">
          <Marquee text={this.props.text}/>
        </Container>
      </PortalComponent>
    );
  }
}

export default WifiNotificationScroll;
