import React = require('react');

export interface MovingMapMarkerCircleProps {
  anchor: number[];
  offset: number[];
  left?: number;
  top?: number;
}

export const MovingMapMarkerCircle: React.FC<MovingMapMarkerCircleProps> = ({left, top}) => {
  return <div style={{transform: `translate(${left}px, ${top}px)`}} className="orig-dest-marker" />;
};
