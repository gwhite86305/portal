import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {APP_DOM_ID} from 'component-utils/dist/types/Constants';
import * as React from 'react';
import {slide as Menu, State as BurgerMenuState} from 'react-burger-menu';
import * as defaults from './defaultProps.json';
import StyledContainer from './StyledContainer';
import HamburgerMenuOpenButton from './HamburgerMenuOpenButton';
import HamburgerMenuCloseButton from './HamburgerMenuCloseButton';
import {IState} from './types.js';

class HamburgerMenuWithoutChildren extends React.Component<any, IState> {
  public static defaultProps = defaults;
  public state = {isMenuOpen: false};
  constructor(props: any) {
    super(props);
    this._handleStateChange = this._handleStateChange.bind(this);
    this._openMenu = this._openMenu.bind(this);
    this._closeMenu = this._closeMenu.bind(this);
  }
  public componentDidMount() {
    this._handleScroll(false);
  }
  public render() {
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <StyledContainer>
          <Menu
            isOpen={this.state.isMenuOpen}
            onStateChange={this._handleStateChange}
            customCrossIcon={<HamburgerMenuCloseButton onClick={this._closeMenu} />}
          >
            {this.props.children}
          </Menu>
        </StyledContainer>
        <HamburgerMenuOpenButton onClick={this._openMenu} />
      </PortalComponent>
    );
  }
  private _handleStateChange(state: BurgerMenuState) {
    this._handleScroll(state.isOpen);
  }
  private _closeMenu() {
    this.setState({isMenuOpen: false});
  }
  private _openMenu() {
    this.setState({isMenuOpen: true});
  }
  private _handleScroll(isOpen = false) {
    const app = document.getElementById(APP_DOM_ID);
    app && (app.style.overflow = isOpen ? 'hidden' : 'auto');
  }
}

export default HamburgerMenuWithoutChildren;
