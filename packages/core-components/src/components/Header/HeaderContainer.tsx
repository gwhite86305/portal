import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import Header from './Header';

const HeaderContainer: React.FC<any> = (props: any) => {
  return <Header {...props} />;
};

export default withLocaleProps(HeaderContainer, [
  {skipNavText: 'core.Header.skipNav.text'},
  {localizedAltImageText: 'core.Header.logoImage.altText'}
]) as any;
