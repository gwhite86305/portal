import * as React from 'react';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

// -- StyledContainer
export const StyledContainer = styled.div`
  .back-button {
    min-width: auto;
    min-height: auto;
    border-radius: 50%;
    padding: 1rem;
    svg {
      ${renderThemeProperty('backIconColor', 'color')};
    }
  }
`;
