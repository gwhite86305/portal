import {Price} from '../../components/common/PaidContent/types';
import {ImageVariants} from '../../components/types';
import { Normalized } from '../../utils/Norm';
import { LocalizedCategory } from '../videos/types';

export interface MovieCategory {
  uid: string;
  category: string;
  items: any;
}

export interface MovieGenre {
  uid: string;
  name: string;
}

export interface MovieItem {
  parentId?: string;
  ageRating: string;
  ageRatingAdvisory: any;
  aspectRatio: string;
  attributes: string;
  audioTracks: any[];
  cast: string;
  categories: string[];
  description: string;
  director: string;
  durationMinutes: number;
  episode: number;
  galleryImages?: any[];
  genres: string[];
  prerollAdGroups: any[];
  releaseDate: string;
  reviewRating: number;
  season: number;
  soundtrack: string;
  subtitle: string;
  textTracks: any[];
  title: string;
  titleImages?: ImageVariants[];
  trailerVideo?: {url: string};
  type: 'videoItem';
  uid: string;
  price?: Price;
}

export interface AggregatedMovies {
  allIds: string[];
  byId: Record<string, MovieItem>;
  genres: {
    allIds: string[];
    byId: Record<string, MovieGenre>;
  };
  byGenreId: Record<string, string[]>;
  categories: {
    allIds: string[];
    byId: Record<string, MovieCategory>;
  };
  byCategoryId: Record<string, string[]>;
}

export interface IMoviesState {
  categories: Normalized<string, LocalizedCategory>;
  items: Normalized<string, MovieItem>;
  isLoadingItems: boolean;
  isLoadingSynopsis: boolean;
  error: string;
}
