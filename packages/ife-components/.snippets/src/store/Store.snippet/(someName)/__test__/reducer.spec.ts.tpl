import { <%= someName %>Reducer, <%= someName %>InitialState } from '../reducer';
import { <%= someName %>ExampleAction } from '../actions';

const newData = 'new data'

describe('<%= someName %>Reducer', () => {
    it('should handle <%= someName %>ExampleAction', () => {
        const action = <%= someName %>ExampleAction({ data: newData });
        const reduced = <%= someName %>Reducer(<%= someName %>InitialState, action);

        expect(reduced).toEqual({ data: newData });
    })
});
