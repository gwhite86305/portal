import { actionCreatorFactory } from 'typescript-fsa';

const action = actionCreatorFactory('<%= SomeName %>');

export const <%= someName %>ExampleAction = action<{data: string}>('EXAMPLE_ACTION');