import * as React from 'react';
import Carousel from '../common/CarouselView/Carousel';
import {THUMBNAILS_SIZE, ImageVariantsType} from '../types';
import * as defaultProps from './defaultProps.json';
import {SeriesProps} from './types';
import IterableListRenderer from '../common/IterableRenderer';

const {HEIGHT, WIDTH} = THUMBNAILS_SIZE.VIDEO;

const imgPlaceholder = 'http://via.placeholder.com/' + HEIGHT + 'x' + WIDTH;

const Series: React.FC<SeriesProps> = ({seriesCategories, onItemInViewPort, onClick}: SeriesProps) => {
  return (
    <IterableListRenderer>
      {seriesCategories.map(seriesCategory => (
        <Carousel
          onItemInViewPort={onItemInViewPort}
          imgPlaceholder={imgPlaceholder}
          key={seriesCategory.uid}
          thumbSize={THUMBNAILS_SIZE.VIDEO}
          categoryName={seriesCategory.category}
          items={seriesCategory.items}
          onClick={onClick}
          imageVariant={ImageVariantsType.posterthumb}
          styleProps={{
            thumbnailHeight: HEIGHT
          }}
        />
      ))}
    </IterableListRenderer>
  );
};

Series.defaultProps = defaultProps as Partial<SeriesProps>;

export default Series;
