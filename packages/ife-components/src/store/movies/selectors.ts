import {IRootState} from '../..';
import { MovieCategory } from './types';
import {filter} from 'lodash-es';

export const selectItemsByCategory = (state: IRootState): MovieCategory[] => {
  const {categories, items} = state.ifeMovies;
  return categories.allIds
    .map(uid => ({
      uid,
      category: categories.byId[uid].title,
      items: filter(items.byId, item => !!item.categories.find(cat => cat === uid))
    }))
    .concat({
      category: 'Others',
      uid: 'others',
      items: filter(items.byId, item => !item.categories.length)
    })
    .filter(category => category.items.length);
};
