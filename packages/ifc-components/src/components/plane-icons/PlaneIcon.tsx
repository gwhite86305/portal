import * as React from 'react';
import {PlaneIcon as ExternalInterface} from '../common/ComponentsInterface';
import { renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export interface InternalInterface {
  className?: string;
}

const Component = (props: any) => {
  const Container = styled.div`
    .plane {
      ${renderThemeProperty('progressBarPlaneFontSize', 'font-size')};
      ${renderThemeProperty('progressBarForegroundColor', 'color')};
      float: right;
      position: absolute;
      transform: rotate(45deg) translateX(-8px) translateY(-5px);
    }
  `;

  const Plane = (props: any) => {
    return <i className="plane fa fa-plane" />;
  };

  return (
    <Container>
      <Plane />
    </Container>
  );
};

class PlaneIcon extends React.Component<InternalInterface & ExternalInterface, any> {
  public render() {
    return <Component {...this.props} />;
  }
}

export default PlaneIcon;
