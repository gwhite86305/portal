import * as React from 'react';
import {PortalComponent, utils, Currency} from 'component-utils';
import {Button, TextBox, Spinner} from 'core-components';
import Select from 'react-select';
import {NavLink} from 'react-router-dom';
import * as defaults from './defaultProps.json';
import {validEmail} from 'component-utils/dist/utils/Validator';
import {pathOr} from 'ramda';
import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  Authentication?: any;
  locale?: string;
  subscription?: any;
  onPayNowClick?: any;
  onCancelClick?: any;
  componentTheme?: any;
  expirationLabel?: string;
  nameOnCardLabel?: string;
  emailLabel?: string;

  confirmEmailLabel?: string;
  paymentFormFooterNote?: string;
  automaticRenewalNoticeHeader?: string;
  automaticRenewalNotice?: string;
  cancelButtonText?: string;
  paynowButtonText?: string;

  feature?: any;
  header?: string;
  note?: string;
  storedPaymentLabel?: string;

  skipButtonText?: string;
  removeButtonText?: string;
  applyButtonText?: string;
  continueButtonText?: string;
  errorDict?: any;
  minimumFractionDigits?: string;
  maximumFractionDigits?: string;
  successCode?: string;
}
const CostContainer = (props: any) => {
  const {feature, dollar_cent, subscription} = props;
  const Container = styled.div`
    background-image: url(${props => feature.costSectionBackgroundImage}); 
  }`;
  return (
    <Container className="cost-container">
      <div className="service-tier-name">
        <span>{subscription.description}</span>
      </div>
      <span className="final-cost">
        <span className="currency">$</span>
        <span className="dollar-amount">{dollar_cent[0]}</span>
        <span className="cents-amount">{dollar_cent.length == 2 ? '.' + dollar_cent[1] : ''}</span>
      </span>
    </Container>
  );
};
const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('primaryTextColor', 'color')};
  font-family: ${(props: any) => props.theme.primaryFontFamily};
  .header-cost {
    margin: 0 auto;
    ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.maxWidth, props.theme)};
      .row {
        display: flex;
        justify-content:flex-end;
      }
      > div {
        width: 100%;
        flex-direction: row;
      }
      .cost-container {
        
        /* background-image: url(./mocks/images/aa-bg-cost-notification.svg); */
        background-size: auto 100%;
        background-position: right;
        background-repeat: no-repeat;
        width: 277px;
        height: 90px;
        padding: 10px 30px 0 0;
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-end;
        text-align: end;
      }
      .service-tier-name {
        color: #36495A;
        font-size: 18px;
        line-height: 22px;
        font-weight: 700;
        text-transform: uppercase;
        text-align: right;
        width: 100%;
        flex-basis: 100%;
        span {
          display: inline-block;
          max-width: 212px;
        }
      }
      .total-cost * {
        color: #9DA6AB;
        vertical-align: top;
      }
      .final-cost {
        margin-left: 13px;
      }
      .final-cost * {
        color: #0061AB;
        vertical-align: top;
      }
      .currency * {
          font-size: 22px;
          line-height: 28px;
      }
      .currency, .cents-amount {
        font-size: 22px;
        line-height: 28px;
      }
      .dollar-amount {
        font-size: 42px;
        line-height: 48px;
      }
      .total-cost .dollar-amount {
        text-decoration: line-through;
      }
      @media (min-width: 1264px) {
        #header-cost > div {
          max-width: 1010px;
        }

        .cost-container {
          width: 489px;
        }
      }
  }
  form {
    justify-content: space-between;
    flex-direction:column;
    .form-section1 {
        ${renderThemeProperty('formBackgroundColor', 'background-color')};
        margin: 0 auto;
        display:flex;
        >div {
          margin: 0 auto;
          display:flex;
          width:100%;
          ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.maxWidth, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('padding', {xs: '0px 20px', md: '0px 0px'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '20px', md: '35px'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('margin-bottom', {xs: '20px', md: '35px'}, props.theme)};
          .cards-container {
            ${(props: any) => applyMediaQueriesForStyle('max-width', {xs: '100%', md: '236px'}, props.theme)};
            /* ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '20px', md: '35px'}, props.theme)}; */
            /* max-width: 236px; */
            .cards {
              width: 100%;
              flex-basis: 100%;
              h2 {
                margin: 0px 0px 0px 0px;
                /* font-family: "AmericanSansLight"; */
                font-size: 30px;
                line-height: 37px;
                font-weight: 300;
                text-align: left;
                color: white;
              }
             
              .cc {
                background-color: white;
                border: 2px solid #0078d2;
                border-radius: 5px;
                box-sizing: border-box;
                padding: 17px 0px;
                min-width: 236px;
                display: flex;
                justify-content: center;
                flex-wrap: wrap;
                position: relative;
                >div {
                  max-width: 262px;
                  align-items: center;
                  display: flex;
                  flex-wrap: wrap;
                  justify-content:center;
                }
                img {
                  width: 76px;
                  border: 0.5px solid #979797;
                  border-radius: 6px;
                  padding: 0;
                  margin: 5px;
                }
              }
              .paypal-option {
                align-content: center;
                padding: 26px 0;
                margin-top: 10px;
              }

              .paypal-option img.paypal-logo {
                width: 80px;
                height: 21px;
                margin-left: auto;
                margin-right: auto;
                display: block;
              }
              .check {
                display: inline-block;
                width: 30px;
                height: 30px;
                margin: 0;
                border: 0;
                position: absolute;
                right: -37px;
                top: -18px;
                border: none !important;
              }
            }
          }
          .payment-form {
            ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '0px', md: '83px'}, props.theme)};
            div.payment-form-header-note {
              ${renderThemeProperty('primaryTextColor', 'color')};
              font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.937);
              font-family: ${(props: any) => props.theme.appFontFamily};
              ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '16px', md: '0px'}, props.theme)};
            }
            .inputs {
              display: flex;
              ${(props: any) => applyMediaQueriesForStyle('height', {xs: '350px', md: '174px'}, props.theme)};

              flex-direction:column;
              ${(props: any) => applyMediaQueriesForStyle('padding-right', {xs: '0px', md: '33px'}, props.theme)};
              >div {
                width: 100%;
                flex-basis:100%;
                display:flex;
                ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
                
                .left {
                  flex-basis: 50%;
                  ${(props: any) => applyMediaQueriesForStyle('margin-right', {xs: '0px', md: '8px'}, props.theme)};
                  display: flex;
                  justify-content: space-between;
                  flex-wrap: wrap;
                  .promoCode {
                    flex-basis: 70%;
                    input {
                      border-radius: 4px 0px 0px 4px;
                      border-right:0px;
                    }
                  }
                  .error{
                    .expire_label {
                        color: ${(props: any) => props.theme.formErrorColor} !important;
                    } 
                  }
                  .expiration_date_container {
                    width:100%;
                    margin-top: 16px;
                    /* margin-bottom: 16px; */
                    position: relative;
                    .expire_label {
                        color: ${(props: any) => props.theme.textBoxPlaceholderColor};
                        font-size: 14px;
                        position: absolute;
                        z-index: 1;
                        top: 4px;
                        width:100%;
                        display:flex;
                        justify-content:space-between
                        span {
                          padding-right:32px;
                          display:flex;
                          align-items:center;
                        }
                        span:first-child {
                          padding-right:0px;
                          padding-left: 10px;
                        }
                        .fa-circle {
                          color: ${(props: any) => props.theme.formErrorColor};
                          font-size: 4px;
                          margin-left: 7px;
                        }
                    }
                    .expiration_date__control{
                      padding-top: 13px;
                    }
                    .expiration_date__placeholder{
                      color: ${(props: any) => props.theme.textBoxTextColor};
                      font-family: ${(props: any) => props.theme.appFontFamily};
                    }
                    .expiration_date__indicator-separator {
                      display:none
                    }
                    .expiration_date__single-value{
                      color: ${(props: any) => props.theme.textBoxTextColor};
                      font-family: ${(props: any) => props.theme.appFontFamily};
                      font-size: ${(props: any) => props.theme.primaryFontSize};
                      white-space:pre;
                    }
                    .expiration_date__menu {
                      background-color:white;
                      color: ${(props: any) => props.theme.textBoxTextColor};
                      border-radius:5px;
                      font-size:14px;
                      color: ${(props: any) => props.theme.textBoxPlaceholderColor};
                    } 

                  }
                  .email {
                    margin-top: 0px;
                    flex-basis: 100%;
                  }
                }
                .right {
                  display: flex;
                  justify-content: space-between;
                  flex-basis: 50%;
                  flex-wrap: wrap;
                  ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '0px', md: '8px'}, props.theme)};
                  /* ${(props: any) =>
                    applyMediaQueriesForStyle('margin-top', {xs: '-16px', md: '0px'}, props.theme)}; */
                  .nameOnTheCard {
                    flex-basis: 100%;
                  }
              
                  .confirm_email {
                    margin-top: 0px;
                    flex-basis: 100%;
                  }
                }
              }
            }
            div.payment-form-footer-note {
              margin-top:4px;
              ${renderThemeProperty('primaryTextColor', 'color')};
              font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.937);
              font-family: ${(props: any) => props.theme.appFontFamily};
              margin-bottom: ${(props: any) => props.theme.gutter};
            }
            .payment-renewal-notice-section {
              font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.937);
              .header{
                display:inline-block;
                width:100%;
                font-weight:bold;
              }
              .notice {
                display:inline-block;
                width:100%;
                margin-top: 5px;
              }
            }
            .validation-summary-section {
              ${(props: any) => applyMediaQueriesForStyle('margin-right', {xs: '0px', md: '33px'}, props.theme)};
            }
            .validation-summary {
              background-color:white;
              display: flex;
              margin-top: ${(props: any) => props.theme.gutter};
              overflow: hidden;
              -webkit-backdrop-filter: blur(10px);
              backdrop-filter: blur(10px);
              box-shadow: 0 2px 4px 0 rgba(54, 73, 90, 0.2);
              border-radius: ${(props: any) => props.theme.textBoxBorderRadius};
              color: ${(props: any) => props.theme.textBoxTextColor};
              font-family: ${(props: any) => props.theme.appFontFamily};
              width: 100%;
              box-sizing: border-box;
              -webkit-box-sizing: border-box;
              -moz-box-sizing: border-box;
              width: 100%;
              height: calc(${(props: any) => props.theme.gutter} * 3.562);
              font-size: ${(props: any) => props.theme.primaryFontSize};
              div:first-child {
                background-color: ${(props: any) => props.theme.formErrorColor};
                width: calc(${(props: any) => props.theme.gutter} * 3.562);
                align-items: center;
                justify-content: center;
              }
              div {
                width: calc(100% - ${(props: any) => props.theme.gutter} * 3.562);
                display: flex;
              }
              p {
                margin: auto calc(${(props: any) => props.theme.gutter} * 0.75);
              }
            }
         
            .validation{
                position: relative;
                padding: calc(16px * 0.5) calc(16px * 0.625) calc(16px * 0.5) calc(16px * 0.625);
                p {
                  ${renderThemeProperty('primaryTextColor', 'color')};
                  margin: 0px;
                  line-height: 1.25;
                }
              }
          }  
        }
    }
 
    .form-section2 {
      display: flex;
      flex-direction: column;
      margin: 0 auto;
      >div {
        margin: 0 auto;
        width:100%;
        display:flex;
        ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.maxWidth, props.theme)};
        flex-direction: column;
      }
      .checkbox-section {
        justify-content: space-between;
        ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column-reverse', md: 'row'}, props.theme)};
        display: flex;
        ${(props: any) => applyMediaQueriesForStyle('align-items', {xs: 'left', md: 'center'}, props.theme)};
        justify-content: space-between;
        ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '20px', md: '400px'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '20px', md: '30px'}, props.theme)};
        .robot-text {
          ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '18px', md: '0px'}, props.theme)};
          display: flex;
          cursor:pointer;
          align-items: center;
          .span-vertical {
            margin-left:10px;
            font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.875);
          }
          img{
            margin-left:10px;
          }
        }
        .agreement-text{
          font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.875);
          ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '0px', md: '30px'}, props.theme)};
          a {
            ${renderThemeProperty('primaryTextColor', 'color')};
            text-decoration:underline;
          }
        }
        .fa-check{
          padding: 4px;
          border-radius:3px;
          background-color:white;
        }
        .checked {
          ${renderThemeProperty('checkBoxWidth', 'width')};
          ${renderThemeProperty('checkBoxHeight', 'height')};
          vertical-align: top;
          background-color:#0078D2 !important;
          color:white;
          :before {
            @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
              padding-left: 0px;
            }
            border-color: ${(props: any) => props.theme.primaryBackgroundColor};
            color: white
          }
        }
        .unchecked {
          ${renderThemeProperty('checkBoxWidth', 'width')};
          ${renderThemeProperty('checkBoxHeight', 'height')};
          vertical-align: top;
          :before {
            @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
              padding-left: 0px;
            }
            border-color: ${(props: any) => props.theme.primaryBackgroundColor};
            color: transparent
          }
        }
      }
      div.buttons {
        display: flex;
        ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row-reverse'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '20px', md: '436px'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('margin-right', {xs: '20px', md: '0px'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '14px', md: '0px'}, props.theme)};
        div.button-wrap {
        display: flex;
        flex-grow: 1;
        margin-top: calc(${(props: any) => props.theme.gutter} * 1);
        text-align: center;
        button {
            width: 100%;
            height: 50px;
            margin: 0 auto;
            white-space: nowrap;
            justify-content: center;
            border-radius: ${(props: any) => props.theme.buttonRadius};
            font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
          }
        }
        div.cancel-button {
          ${(props: any) => applyMediaQueriesForStyle('margin-right', {xs: '0px !important', md: '12px'}, props.theme)};
          text-align: center;
            button {
              background-color:transparent;
              color:white;
              border: 1px solid white;
              width: 100%;
              height: 50px;
              margin: 0 auto;
              white-space: nowrap;
              justify-content: center;
              border-radius: ${(props: any) => props.theme.buttonRadius};
              font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
            }
        }
        div.paynow-button {
          ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '0px', md: '12px'}, props.theme)};
          .disabled {
              color: #4D4E4F!important;
              background-color: #001F35;
          }
        }
      }
    }
  }
`;

class AaPurchaseSubscription extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;
  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      confirm_email: '',
      nameOnTheCard: '',
      notRobot: false,
      cards: [],
      formErrors: {}
    };
    this._handleFormChange = this._handleFormChange.bind(this);
    this._handlePaynowClick = this._handlePaynowClick.bind(this);
    this._handleCancelClick = this._handleCancelClick.bind(this);
  }
  toggleRobotCheckBox = (e: any) => {
    this.setState((prevState: any) => ({notRobot: !prevState.notRobot}));
  };

  handleCardChange = (card: any) => {
    this.setState({card: card});
  };

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handlePaynowClick(event: any) {
    event.preventDefault();
    const {email, confirm_email, nameOnTheCard, card} = this.state;
    const {errorDict} = this.props;
    let formErrors: any = {};
    if (!card) {
      formErrors['card'] = errorDict['errorMessageCardRequired'];
    }

    if (!nameOnTheCard) {
      formErrors['nameOnTheCard'] = errorDict['errorMessageNameOnTheCardRequired'];
    }
    if (!email) {
      formErrors['email'] = errorDict['errorMessageEmailRequired'];
    }
    if (email && !validEmail(email)) {
      formErrors['email'] = errorDict['errorMessageInvalidEmail'];
    }
    if (!confirm_email) {
      formErrors['confirm_email'] = errorDict['errorMessageConfirmEmailRequired'];
    }
    if (confirm_email && !validEmail(confirm_email)) {
      formErrors['confirm_email'] = errorDict['errorMessageInvalidEmail'];
    }
    if (!formErrors['email'] && !formErrors['confirm_email'] && email != confirm_email) {
      formErrors['confirm_email'] = errorDict['emailMessageEmailMismatch'];
    }
    this.setState({formErrors});
    if (Object.keys(formErrors).length === 0) {
      this.props.onPayNowClick(card.value, nameOnTheCard, email, this.props.subscription.id);
    }
  }

  _handleCancelClick(event: any) {
    event.preventDefault();
    this.props.onCancelClick();
  }
  componentDidUpdate(prevProps: any) {
    let cards: Array<any> = [];
    for (let i = 0; i < Object.keys(this.props.Authentication.userCard.cards || []).length; i++) {
      let card = this.props.Authentication.userCard.cards[i];
      cards.push({
        value: card,
        label:
          card.card_type +
          ' ' +
          '**** **** **** ' +
          card.card_number +
          ' ' +
          ('0' + card.card_expiry_month).slice(-2) +
          '/' +
          (card.card_expiry_year + '').slice(-2)
      });
    }

    this.state.cards.length == 0 &&
      cards.length > 0 &&
      this.setState({
        cards: cards
      });
  }
  public render() {
    const {
      Authentication,
      feature,
      header,
      storedPaymentLabel,
      expirationLabel,
      nameOnCardLabel,
      emailLabel,
      confirmEmailLabel,
      paymentFormFooterNote,
      automaticRenewalNoticeHeader,
      automaticRenewalNotice,
      cancelButtonText,
      paynowButtonText,
      errorDict,
      successCode
    } = this.props;

    const {formErrors} = this.state;

    let errorSummary = '';
    let code = pathOr(null, ['gogoSubscriptionPurchase', 'body', 'code'], Authentication);
    if (code && code !== successCode) {
      errorSummary = errorDict[code] || errorDict['generic'];
    }
    if (Object.keys(formErrors).length) {
      errorSummary = errorDict['errorMessageOnFormHeader'];
    }

    const checked = <i className="fa fa-check checked" />;
    const unchecked = <i className="fa fa-check unchecked" />;
    let dollar_cent = (this.props.subscription.cost + '').split('.');
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="aa-purchase-subscription-form">
          <div className="header-cost">
            <div className="row">
              <CostContainer feature={feature} dollar_cent={dollar_cent} subscription={this.props.subscription} />
            </div>
          </div>
          <form onSubmit={this._handlePaynowClick}>
            <div className="form-section1">
              <div>
                <div className="cards-container">
                  <div className="cards">
                    <h2>Payment</h2>
                    <div className="cc new-card-option checked">
                      <div>
                        <img src={feature.paymentMethodVisaImage} />
                        <img src={feature.paymentMethodMasterCardImage} />
                        <img src={feature.paymentMethodDiscoverImage} />
                        <img src={feature.paymentMethodAmexImage} />
                        <img src={feature.paymentMethodClubImage} />
                        <img src={feature.paymentMethodJCBImage} />
                      </div>
                      <img src={feature.paymentMethodCheckIcon} className="check" />
                    </div>
                  </div>
                </div>
                <div className="payment-form">
                  <div className="payment-form-header-note">
                    {header ? <span className="note-text">{header}</span> : null}
                  </div>
                  {errorSummary ? (
                    <div className="validation-summary-section">
                      <div className="validation-summary">
                        <div>
                          <img src={feature.formErrorIcon} />
                        </div>
                        <div>
                          <p>{errorSummary}</p>
                        </div>
                      </div>
                    </div>
                  ) : null}
                  <div className="inputs">
                    <div>
                      <div className="left">
                        <div className={'expiration_date_container' + (formErrors.card ? ' error' : '')}>
                          <div className="expire_label">
                            <span>{storedPaymentLabel}</span>
                            <span>
                              {expirationLabel}
                              <i className="fa fa-circle" />
                            </span>
                          </div>
                          <Select
                            isSearchable={false}
                            placeholder=""
                            classNamePrefix="expiration_date"
                            className="expiration_date"
                            value={this.state.card}
                            onChange={this.handleCardChange}
                            options={this.state.cards}
                          />
                          {formErrors.nameOnTheCard ? (
                            <div className="validation">
                              <p className="message">{formErrors.nameOnTheCard}</p>
                            </div>
                          ) : null}
                        </div>
                      </div>
                      <div className="right">
                        <TextBox
                          name="nameOnTheCard"
                          className="nameOnTheCard"
                          error={formErrors.nameOnTheCard}
                          placeholder={nameOnCardLabel}
                          required={true}
                          onChange={this._handleFormChange}
                        />
                      </div>
                    </div>
                    <div>
                      <div className="left">
                        <TextBox
                          name="email"
                          className="email"
                          error={formErrors.email}
                          placeholder={emailLabel}
                          required={true}
                          onChange={this._handleFormChange}
                        />
                      </div>
                      <div className="right">
                        <TextBox
                          className="confirm_email"
                          name="confirm_email"
                          error={formErrors.confirm_email}
                          placeholder={confirmEmailLabel}
                          required={true}
                          onChange={this._handleFormChange}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="payment-form-footer-note">
                    {' '}
                    <span className="note-text">{paymentFormFooterNote}</span>{' '}
                  </div>

                  <div className="payment-renewal-notice-section">
                    <span className="header">{automaticRenewalNoticeHeader}</span>
                    <span className="notice">{automaticRenewalNotice}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-section2">
              <div>
                <div className="checkbox-section">
                  <div
                    className="robot-text secondary-focus"
                    tabIndex={0}
                    onKeyPress={this.toggleRobotCheckBox}
                    onClick={this.toggleRobotCheckBox}
                  >
                    {this.state.notRobot ? checked : unchecked}
                    <span className="span-vertical">I am not a robot</span>
                    <img src={feature.lockIcon} />
                  </div>
                  <div className="agreement-text">
                    By clicking Pay now. I agree to the{' '}
                    <span style={{textDecoration: 'underline'}}>
                      <NavLink to="/Terms_of_Service"> terms of sale</NavLink>
                    </span>
                  </div>
                </div>
                <div className="buttons">
                  <div className="button-wrap paynow-button">
                    <Button
                      isDisabled={!this.state.notRobot}
                      isPrimary={true}
                      onClick={this._handlePaynowClick}
                      aria-label={paynowButtonText}
                    >
                      {paynowButtonText}
                    </Button>
                  </div>
                  <div className={'button-wrap  cancel-button'}>
                    <Button isPrimary={false} onClick={this._handleCancelClick} aria-label={cancelButtonText}>
                      {cancelButtonText}
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default AaPurchaseSubscription;
