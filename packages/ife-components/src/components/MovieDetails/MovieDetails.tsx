import {Button, Grid} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {path} from 'ramda';
import * as React from 'react';
import {SizeMe} from 'react-sizeme';
import {MediaThumbnail} from '../common/MediaThumbnail/MediaThumbnail';
import {PaidContent} from '../common/PaidContent';
import {getOptimizedImgSrc} from '../common/utils.js';
import {ImageVariantsType} from '../types/index.js';
import * as defaultProps from './defaultProps.json';
import {appendOrUndefined, capitaliseAndJoin, DetailsProp, MovieDetailsProps, RatingProp} from './utils';
import {MediaThumbnailWithSize} from '../common/MediaThumbnail/MediathumbnailWithSize';

const MovieDetails: React.FC<MovieDetailsProps> = (props: MovieDetailsProps) => {
  const {isInPlaylist, item, onAddToPlaylist, onClickPlay, onClickPlayTrailer, onRemoveFromPlaylist} = props;
  const hasTrailer = !!path(['item', 'trailerVideo', 'url'], props);
  const entry = {itemId: item.uid, itemType: item.type};

  return (
    <Grid container className="movie-details-container">
      <Grid item xs={12} className="header">
        <Button aria-label="Go back" className="back-button" onClick={props.onClickBack} children={<ArrowBackIcon />} />
        <h1 className="title" children={item.title} />
        <div className="place-holder" children={<ArrowBackIcon />} />
      </Grid>
      <Grid item xs={12} md={6} lg={8}>
        <div className="poster-container">
          {item.galleryImages && (
            <MediaThumbnailWithSize
              item={item}
              className="poster"
              clickable={false}
              imageVariants={item.galleryImages[0]}
              variantType={ImageVariantsType.poster}
            />
          )}
        </div>
      </Grid>
      <Grid item xs={12} md={6} lg={4} className="details-container">
        <div className="info-panel">
          <div className="title" children={item.title} />
          <div className="cta-container">
            {item.price ? (
              <PaidContent
                className="cta play"
                price={item.price}
                label="WATCH NOW"
                entry={entry}
                onPaid={() => onClickPlay(item)}
                onPurchaseSuccess={props.onPurchaseSuccess}
              />
            ) : (
              <Button
                aria-label="Watch now"
                variant="contained"
                className="cta play"
                children={'WATCH NOW'}
                onClick={() => onClickPlay(item)}
              />
            )}
            {hasTrailer && (
              <Button
                aria-label="Watch trailer"
                variant="contained"
                className="cta playlist trailer"
                children={'WATCH TRAILER'}
                onClick={() => onClickPlayTrailer(item)}
              />
            )}
            {item.price ? (
              <PaidContent
                className="cta playlist"
                price={item.price}
                entry={entry}
                label={isInPlaylist ? 'REMOVE FROM PLAYLIST' : 'ADD TO PLAYLIST'}
                onPaid={() => (isInPlaylist ? onRemoveFromPlaylist(item.uid) : onAddToPlaylist([item]))}
                onPurchaseSuccess={props.onPurchaseSuccess}
              />
            ) : (
              <Button
                aria-label={isInPlaylist ? 'REMOVE FROM PLAYLIST' : 'ADD TO PLAYLIST'}
                variant="contained"
                className="cta playlist"
                children={isInPlaylist ? 'REMOVE FROM PLAYLIST' : 'ADD TO PLAYLIST'}
                onClick={() => (isInPlaylist ? onRemoveFromPlaylist(item.uid) : onAddToPlaylist([item]))}
              />
            )}
          </div>
          <div>
            <div className="description" children={<span children={item.description} />} />
            <div className="details">
              <DetailsRow
                label={'Rating'}
                value={item.reviewRating ? <ReviewRating stars={item.reviewRating} /> : null}
              />
              <DetailsRow label={'Genre'} value={capitaliseAndJoin(item.genres)} />
              <DetailsRow label={'Classification'} value={item.ageRating} />
              <DetailsRow label={'Advisory'} value={capitaliseAndJoin(item.ageRatingAdvisory)} />
              <DetailsRow label={'Director'} value={item.director} />
              <DetailsRow label={'Stars'} value={item.cast} />
              <DetailsRow label={'Soundtrack'} value={item.soundtrack} />
              <DetailsRow label={'Length'} value={appendOrUndefined(item.durationMinutes, ' mins')} />
              <DetailsRow label={'Released'} value={item.releaseDate ? item.releaseDate.substr(0, 4) : null} />
            </div>
          </div>
        </div>
      </Grid>
    </Grid>
  );
};

export const DetailsRow: React.FC<DetailsProp> = (props: DetailsProp) =>
  props.value ? (
    <div className="info">
      <span aria-hidden="true" id={props.label} className="label" children={props.label} />
      <span aria-labelledby={props.label} className="value" children={props.value} />
    </div>
  ) : null;
export const renderStar = (className: string, index: number) => (
  <svg key={`${className}${index}`} className={`star star--${className}`} viewBox="0 0 51 48">
    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z" />
  </svg>
);
const ReviewRating: React.FC<RatingProp> = (prop: RatingProp) => (
  <div>
    {Array.from(new Array(prop.stars)).map((_, index) => renderStar('full', index))}
    {Array.from(new Array(5 - prop.stars)).map((_, index) => renderStar('empty', index))}
  </div>
);

MovieDetails.defaultProps = defaultProps as any;

export default MovieDetails;
