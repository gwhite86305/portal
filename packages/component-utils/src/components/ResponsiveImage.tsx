import * as React from 'react';
import {applyMediaQueriesForStyle} from '../utils/styledComponents';
import styled from 'styled-components';
import withWidth from '@material-ui/core/withWidth';
import {ScreenSize} from '../types/ScreenSizes';
import {AnalyticsLogger} from '../analytics';

export interface InternalInterface {
  className?: string;
  image: any;
  width: any;
  altText?: any;
  imageMargin?: any;
  imageWidth?: any;
}

const Container = styled.div`
  position: relative;
  display: block;
  ${(props: any) => applyMediaQueriesForStyle('padding', props.imageMargin, props.theme)};
  img {
    display: block;
    height: auto;
    width: ${(props: any) => props.imageWidth || '100%'};
    ${(props: any) => applyMediaQueriesForStyle('margin', props.imageMargin, props.theme)};
  }
` as any;

const analyticsLogger = new AnalyticsLogger();

class ResponsiveImage extends React.Component<InternalInterface, any> {
  private _getMatchingImage() {
    const {image, width} = this.props;
    const screenSizes = Object.keys(ScreenSize);
    const currentScreenSizeIndex = screenSizes.indexOf(width);
    const matchingScreenSizes = screenSizes.filter(
      (screenSize: ScreenSize, index: number) => index <= currentScreenSizeIndex && image && image[screenSize]
    );
    return matchingScreenSizes.length > 0 ? image[matchingScreenSizes[matchingScreenSizes.length - 1]] : null;
  }

  public render() {
    const {altText, className, imageWidth, imageMargin, children} = this.props;
    const matchingScreenSizeImage = this._getMatchingImage();
    return matchingScreenSizeImage ? (
      <Container className={className ? className : ''} imageWidth={imageWidth} imageMargin={imageMargin}>
        <img
          src={matchingScreenSizeImage}
          alt={altText || ''}
          onLoad={() => {
            analyticsLogger.MediaImpression(matchingScreenSizeImage);
          }}
        />
        {children}
      </Container>
    ) : null;
  }
}

export default withWidth()(ResponsiveImage) as any;
