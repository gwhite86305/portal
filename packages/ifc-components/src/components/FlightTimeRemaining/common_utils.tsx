import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {convertMinutesToClock} from 'component-utils/dist/utils/DateUtils';
import PlaneIcon from '../plane-icons/PlaneIcon';

export const ComponentWrapper = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) =>
    applyMediaQueriesForStyle('background-image', props.theme.componentTheme.backgroundImage, props.theme)};
  display: flex;
  height: 100%;
  width: 100%;
  background-position-x: 50%;
  background-position-y: 50%;
  background-size: cover;
  .ifc-flighttimeremaining-component {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: center;
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
  }
  .ifc-flighttimeremaining-progress {
    display: flex;
    flex-direction: row;
    justify-content: center;
  }
`;

export const TimeToDestination = (props: any) => {
  const Container = styled.div`
    margin-right: ${props.feature.flexWidget ? props.componentTheme.timeMarginRight : '0'};
    ${renderThemeProperty('timeToDestinationLabelFontSize', 'font-size')};
    ${renderThemeProperty('timeToDestinationLabelFontWeight', 'font-weight')};
    ${renderThemeProperty('timeToDestinationLabelColor', 'color')};
    ${renderThemeProperty('timeToDestinationLabelPadding', 'padding')};
    width: auto;
    text-align: center;
    justify-content: space-evenly;
    padding-top: ${props.feature.flexWidget ? '0' : '11px'};
    span {
      white-space: pre;
    }
  `;
  const {flightMinutesRemaining} = props;
  return (
    <Container>
      <span>
        {convertMinutesToClock(flightMinutesRemaining, '00', props.feature.hourZeroPadding).hour}
        {props.feature.extraSpaceInTime ? '\u00A0' : ''}
        {props.feature.hourFormat}
        {props.feature.extraSpaceInTime ? ' \u00A0' : ' '}
        {convertMinutesToClock(flightMinutesRemaining, '00', props.feature.hourZeroPadding).minute}
        {props.feature.extraSpaceInTime ? '\u00A0' : ''}
        {props.feature.minuteFormat}
      </span>
    </Container>
  );
};

export const Plane = (props: any) => {
  const {flightCutoffPercentage} = props.feature;
  const show = props.percentageOfFlightComplete < flightCutoffPercentage ? true : false;
  if (show) {
    // TODO: verify that child is of type PlaneIcon
    // const icon = props.children.filter((child: any) => child instanceof PlaneIcon)
    let icon = null;
    if (props.children) {
      icon = props.children[0];
    }

    if (icon) {
      return icon;
    } else {
      return <PlaneIcon {...props} />;
    }
  }
  return null;
};

export default Plane;
