import {Action, ActionCreators} from './actions';

export type State = {
  readonly errorMessage: string | null;
};

export const initialState: State = {
  errorMessage: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;
  switch (action.type) {
    case ActionCreators.ShowModal.type:
      partialState = {...action.payload};
      break;
    case ActionCreators.HideModal.type:
      partialState = {...action.payload};
      break;
    default:
      return state;
  }
  return {...state, ...partialState};
}
