import * as React from 'react';
import * as ReactShallowRenderer from 'react-test-renderer/shallow';
import {mount, render, shallow} from 'enzyme';
import Accordion from './Accordion';

const renderer = ReactShallowRenderer.createRenderer();

describe('<Accordion />', () => {
  it('renders default', () => {
    let tree = renderer.render(<Accordion />);
    expect(tree).toMatchSnapshot();
  });

  it('renders with props', () => {
    let AccordionProps = {
      componentTheme: {
        title: 'Title',
        sections: [],
        componentTheme: {}
      }
    };
    let tree = renderer.render(<Accordion {...AccordionProps} />);
    expect(tree).toMatchSnapshot();
  });
});
