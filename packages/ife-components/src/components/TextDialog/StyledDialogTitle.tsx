import * as React from 'react';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

import {DialogTitle} from '@material-ui/core';
import {DialogTitleProps} from '@material-ui/core/DialogTitle';

export default styled(DialogTitle)`
  ${renderThemeProperty('background', 'background')};
  h2 {
    ${renderThemeProperty('titleFontSize', 'font-size')};
    ${renderThemeProperty('titleColor', 'color')};
  }
`;
