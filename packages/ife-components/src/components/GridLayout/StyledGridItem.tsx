import {applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {reduce} from 'lodash-es';

export interface StyledGridItemProps {
  variant: string;
  rowPos?: {xs: string; sm: string; md: string};
  columnPos?: {xs: string; sm: string; md: string};
}

const StyledGridItem = styled.div<StyledGridItemProps>`
  position: relative;
  width: 100%;
  cursor: pointer;
  overflow: hidden;
  // calc rows
  ${({theme, variant}) => {
    switch (variant) {
      case 'small':
        return applyMediaQueriesForStyle('grid-row', addSpan(theme.componentTheme.smallGridItemRow), theme);
      case 'medium':
        return applyMediaQueriesForStyle('grid-row', addSpan(theme.componentTheme.mediumGridItemRow), theme);
      case 'large':
        return applyMediaQueriesForStyle('grid-row', addSpan(theme.componentTheme.largeGridItemRow), theme);
      default:
        return '';
    }
  }}

  ${({theme, variant}) => {
    switch (variant) {
      case 'small':
        return applyMediaQueriesForStyle('-ms-grid-row-span', theme.componentTheme.smallGridItemRow, theme);
      case 'medium':
        return applyMediaQueriesForStyle('-ms-grid-row-span', theme.componentTheme.mediumGridItemRow, theme);
      case 'large':
        return applyMediaQueriesForStyle('-ms-grid-row-span', theme.componentTheme.largeGridItemRow, theme);
      default:
        return '';
    }
  }}

  // ms column span
  ${({theme, variant}) => {
    switch (variant) {
      case 'small':
        return applyMediaQueriesForStyle('-ms-grid-column-span', theme.componentTheme.smallGridItemCol, theme);
      case 'medium':
        return applyMediaQueriesForStyle('-ms-grid-column-span', theme.componentTheme.mediumGridItemCol, theme);
      case 'large':
        return applyMediaQueriesForStyle('-ms-grid-column-span', theme.componentTheme.largeGridItemCol, theme);
      default:
        return '';
    }
  }}

  // genric span
  ${({theme, variant}) => {
    switch (variant) {
      case 'small':
        return applyMediaQueriesForStyle('grid-column', addSpan(theme.componentTheme.smallGridItemCol), theme);
      case 'medium':
        return applyMediaQueriesForStyle('grid-column', addSpan(theme.componentTheme.mediumGridItemCol), theme);
      case 'large':
        return applyMediaQueriesForStyle('grid-column', addSpan(theme.componentTheme.largeGridItemCol), theme);
      default:
        return '';
    }
  }}

  ${({rowPos}) => applyMediaQueriesForStyle('-ms-grid-row', rowPos, {})};
  ${({columnPos}) => applyMediaQueriesForStyle('-ms-grid-column', columnPos, {})};
`;

export default StyledGridItem;

const addSpan = (collection: any) =>
  reduce(
    collection,
    (r: any, v, k) => {
      r[k] = `span ${v}`;
      return r;
    },
    {}
  );
