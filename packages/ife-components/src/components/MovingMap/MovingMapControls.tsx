import React = require('react');
import {Button} from '@material-ui/core';
import GpsFixed from '@material-ui/icons/GpsFixed';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import * as hasTouch from 'has-touch';

export interface MovingMapControlsProps {
  onZoomIn: () => void;
  onZoomOut: () => void;
  followingPlane: boolean;
  onFollowPlane: () => void;
}

const CenterButton = ({followingPlane, onCenterPositionClick}: any) => (
  <Button
    className={`${followingPlane ? 'centered ' : ''}center-button`}
    variant="fab"
    aria-label="Center on plane"
    onClick={onCenterPositionClick}
  >
    <GpsFixed />
  </Button>
);

export const MovingMapControls: React.FC<MovingMapControlsProps> = props => {
  return hasTouch ? (
    <div className="button-container">
      <CenterButton followingPlane={props.followingPlane} onCenterPositionClick={props.onFollowPlane} />
    </div>
  ) : (
    <div className="button-container">
      <Button className="zoom-button zoom-in" mini variant="fab" aria-label="Zoom in" onClick={props.onZoomIn}>
        <ZoomIn />
      </Button>
      <Button className="zoom-button zoom-out" mini variant="fab" aria-label="Zoom out" onClick={props.onZoomOut}>
        <ZoomOut />
      </Button>
      <CenterButton followingPlane={props.followingPlane} onCenterPositionClick={props.onFollowPlane} />
    </div>
  );
};
