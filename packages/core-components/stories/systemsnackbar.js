import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, object, text, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import SystemSnackbar from '../dist/components/SystemSnackbar/SystemSnackbar';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    textColor: 'white',
    titleFontSize: '1rem',
    messageFontSize: '0.9rem',
    iconFontSize: '1.5rem',
    bottomOffset: '0',
    display: {
      xs: true,
      sm: true,
      md: true,
      lg: true,
      xl: true
    },
    padding: {
      xs: '6px 24px'
    },
    messageBodyTopMargin: '4px',
    infoBackgroundColor: 'rgba(230, 144, 65, 0.95)',
    errorBackgroundColor: 'rgba(255, 0, 0, 0.95)'
  }
};

const modifiedMockTheme_1 = {
  componentTheme: {
    textColor: 'yellow',
    titleFontSize: '1.5rem',
    messageFontSize: '1.3rem',
    iconFontSize: '2rem',
    bottomOffset: '100px',
    display: {
      xs: true,
      sm: true,
      md: true,
      lg: true,
      xl: true
    },
    padding: {
      xs: '20px 50px'
    },
    messageBodyTopMargin: '24px',
    infoBackgroundColor: 'green',
    errorBackgroundColor: 'blue'
  }
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";
mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles =
  "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./fonts/fontawesome-webfont.woff') format('woff'), url('./fonts/fontawesome-webfont.ttf') format('truetype'), url('./fonts/fontawesome-webfont.svg#font') format('svg');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';
(mockTheme.fontIconUsesLigature = false), (mockTheme.fontIconBaseClass = 'fa');

const stories = storiesOf('SystemSnackBar Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);

stories.add(
  'SystemSnackBar 1 - Default Demo Portal Theme',
  () => {
    return (
      <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
        <SystemSnackbar {...defaultProps} />
      </PortalComponentWrapper>
    );
  },
  {dude: 'yes'}
);

stories.add('SystemSnackBar 2 - Test Modified Theme', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <SystemSnackbar {...modifiedMockTheme_1} />
    </PortalComponentWrapper>
  );
});

stories.add('SystemSnackBar 3 - Test Modified Message', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <SystemSnackbar
        {...defaultProps}
        message={{
          title: 'This is a new title',
          message: 'This is a body content with lots of content, yes indeed lots of content!',
          isError: false
        }}
      />
    </PortalComponentWrapper>
  );
});

stories.add('SystemSnackBar 4 - Test Error Message', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <SystemSnackbar
        {...defaultProps}
        message={{title: 'Something bad happened', message: 'Boo hoo, now get back to work!', isError: true}}
      />
    </PortalComponentWrapper>
  );
});
