import {INotificationDispatcher} from './types';
import {Action, ActionCreators} from './actions';

const initialState: INotificationDispatcher = {
  messages: null,
  progress: null,
  flightInfo: null,
  isAudioPausedForAnnouncement: false
};

export default function reducer(
  state: INotificationDispatcher = initialState,
  action: Action
): INotificationDispatcher {
  switch (action.type) {
    case ActionCreators.updateFlightInfo.type:
      return {...state, flightInfo: action.payload};
    case ActionCreators.updateFlightProgress.type:
      return {...state, progress: action.payload};
    case ActionCreators.updateMessages.type:
      return {...state, messages: action.payload};
    case ActionCreators.setAudioPausedForAnnouncement.type:
      return {...state, isAudioPausedForAnnouncement: action.payload};
    default:
      return state;
  }
}
