import {MovieItem, MovieCategory} from '../../store/movies/types';
import {Movies as ExternalInterface} from '../common/ComponentsInterface';
import {trackImpression} from '../../store/analytics/actions';
import { getMovies } from '../../store/movies/actions';

export interface StateProps {
  items: MovieCategory[];
  isLoading: boolean;
}

export interface MoviesProps {
  onClick(id: string): any;
  onItemInViewPort: typeof trackImpression;
  items: MovieCategory[];
}

export interface DispatchProps {
  trackImpression: typeof trackImpression;
}

export interface Props extends StateProps, DispatchProps, ExternalInterface {}
