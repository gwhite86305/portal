import {trackImpression} from '../../../store/analytics/actions';
import {AudioGroup} from '../../../store/audio/types';
import {MovieItem} from '../../../store/movies/types';
import {VideoGroup, VideoItem} from '../../../store/videos/types';
import {ImageVariantsType, IThumbnailSize} from '../../types';

export interface MobileCarouselProps {
  items: (VideoItem | MovieItem | VideoGroup | AudioGroup)[];
  onClick: any;
  categoryName: string;
  imgPlaceholder: string;
  onItemInViewPort: typeof trackImpression;
  imageVariant: ImageVariantsType;
}

export interface DesktopCarouselProps {
  categoryName: string;
  cellSpacing: number;
  imgPlaceholder: string;
  items: (VideoItem | MovieItem | VideoGroup | AudioGroup)[];
  onClick: any;
  thumbSize: IThumbnailSize;
  onItemInViewPort: typeof trackImpression;
  imageVariant: ImageVariantsType;
}

export interface CarouselProps {
  categoryName: string;
  imgPlaceholder: string;
  items: (VideoItem | MovieItem | VideoGroup | AudioGroup)[];
  onClick?(contentId: string): any;
  onItemInViewPort: typeof trackImpression;
  thumbSize: IThumbnailSize;
  styleProps: {is1To1Ratio?: boolean; thumbnailHeight: number};
  spacing?: number;
  imageVariant: ImageVariantsType;
}
