import * as React from 'react';
import {utils} from 'component-utils';
import {WifiServiceOptions as ExternalInterface} from '../common/ComponentsInterface';
import WifiServiceOptions, {InternalInterface} from './WifiServiceOptions';
import WifiServiceOptionsAA from './WifiServiceOptionsAA';
import {
  purchaseOptionSelected,
  voucherOptionSelected,
  messagingOptionSelected,
  subscriptionOptionSelected
} from '../../store/WifiServiceOptions/actions';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import {
  getPremiumTiers,
  getPremiumState,
  getMessagingTier,
  getMessagingState,
  deviceHasServiceTier
} from '../../utils/ConnectionUtils';

const connectWrapper = utils.connectWrapper.default;
export type PageProps = DispatchProps & ExternalInterface & InternalInterface;

export interface DispatchProps {
  onPurchaseClick(tierId: number): any;
  onSubscriptionClick(tierId: number): any;
  onVoucherClick(): any;
  onMessagingClick(): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const premiumTiers = getPremiumTiers(state.profile);
    const messagingTiers = getMessagingTier(state.profile) ? getMessagingTier(state.profile) : [];
    const messagingAvailable =
      (!getMessagingState(state.device, state.profile) || !deviceHasServiceTier(state.device)) && messagingTiers.length
        ? true
        : false;
    const subscriptions = ownProps.feature.includeSubscriptions ? state.profile.subscriptionOptions || [] : [];
    const currencyCode = state.profile.currencyCode || state.ux.settings.ifc.defaultCurrencyCode;
    const currencySuffix = state.ux.settings.ifc.currencySuffix;
    const messagingConnected = state.messagingConnected ? state.messagingConnected : state.profile.messagingConnected;
    const locale = state.device.language;
    const minimumFractionDigits = state.ux.settings.ifc.defaultMinimumFractionDigits;
    const maximumFractionDigits = state.ux.settings.ifc.defaultMaximumFractionDigits;
    const profileSettings = state.profile.settings;
    return {
      premiumTiers,
      subscriptions,
      currencyCode,
      locale,
      minimumFractionDigits,
      maximumFractionDigits,
      messagingAvailable,
      messagingConnected,
      messagingTiers,
      profileSettings,
      currencySuffix
    };
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onPurchaseClick: tierId => {
      dispatch(purchaseOptionSelected(tierId));
      ownProps.history &&
        ownProps.history.push('/' + (ownProps.feature.openPageOnPurchaseClick || ownProps.feature.openPageOnSelection));
    },
    onSubscriptionClick: subscriptionId => {
      dispatch(subscriptionOptionSelected(subscriptionId));
      ownProps.history && ownProps.history.push('/' + ownProps.feature.openPageOnSelection);
    },
    onVoucherClick: () => {
      dispatch(voucherOptionSelected());
      ownProps.history && ownProps.history.push('/' + ownProps.feature.openPageOnSelection);
    },
    onMessagingClick: () => {
      dispatch(messagingOptionSelected());
      ownProps.history && ownProps.history.push('/' + ownProps.feature.openPageOnSelection);
    }
  })
)
class WifiServiceOptionsContainer extends React.Component<PageProps, any> {
  public render() {
    const {intl, componentUid} = this.props;

    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.WifiServiceOptions.header'},
      {body: 'ifc.WifiServiceOptions.body'},
      {voucherTitle: 'ifc.WifiServiceOptions.voucherTitle'},
      {voucherText: 'ifc.WifiServiceOptions.voucherText'},
      {voucherDescripiton: 'ifc.WifiServiceOptions.voucherDescripiton'},
      {voucherButtonText: 'ifc.WifiServiceOptions.voucherButtonText'},
      {messagingTitle: 'ifc.WifiServiceOptions.messagingTitle'},
      {messagingText: 'ifc.WifiServiceOptions.messagingText'},
      {messagingDescripiton: 'ifc.WifiServiceOptions.messagingDescripiton'},
      {messagingButtonText: 'ifc.WifiServiceOptions.messagingButtonText'}
    ]);
    // TODO: Getting currency symbol goes here?
    const props = {...this.props};
    return this.props.feature.optionsDisplayType === 'Normal' ? (
      <WifiServiceOptions {...props} {...localizedProps} />
    ) : (
      <WifiServiceOptionsAA {...props} {...localizedProps} />
    );
  }
}

export default injectIntl(withRouter(WifiServiceOptionsContainer));
