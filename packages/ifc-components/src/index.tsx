import * as React from 'react';
import {Store} from 'redux';
import deviceReducer from './store/device/reducer';
import profileReducer from './store/profile/reducer';
import UserAgreementReducer from './store/UserAgreement/reducer';
import WifiServiceOptionsReducer from './store/WifiServiceOptions/reducer';
import AuthenticationReducer from './store/Authentication/reducer';
import UserReducer from './store/User/reducer';
import {DevicePoller, ProfilePoller} from './components';
import {IFCSettings} from './components/common/SettingsInterface';

export * from './components';

const defaultSettings = {
  deviceUrl: process.env.DEVICE_URL,
  devicePollRate: 5000,
  profileUrl: process.env.PROFILE_URL
};

// IoC / Decorator
const init = (
  render: React.ReactElement<any> | null,
  store: Store<any>,
  settings?: IFCSettings
): React.ReactElement<any> => {
  const assignedSettings: IFCSettings = {...defaultSettings, ...(settings as any)};

  (store as any).attachReducers({
    device: deviceReducer,
    profile: profileReducer,
    userAgreement: UserAgreementReducer,
    WifiServiceOptions: WifiServiceOptionsReducer,
    Authentication: AuthenticationReducer,
    User: UserReducer
  });

  return (
    <DevicePoller url={assignedSettings.deviceUrl} interval={assignedSettings.devicePollRate}>
      <ProfilePoller url={assignedSettings.profileUrl}>{render}</ProfilePoller>
    </DevicePoller>
  );
};

export {init};
export default init;
