import AppInitializer from './utils/AppInitializer';
import {TextEncoder, TextDecoder} from 'text-encoding';
import {APP_DOM_ID} from 'component-utils/dist/types/Constants';

// pull in initial setup css
require('./static/css/normalize.css');

if (!window['TextEncoder'] || !window['TextDecoder']) {
  window['TextEncoder'] = TextEncoder;
  window['TextDecoder'] = TextDecoder;
}

// TODO DROP BELOW WHEN IMAGES CAN BE LOADED FROM UX BUILDER API
// Honestly, these should be required in the component that uses them, if they
//   aren't going to come from props, that way it never leaves the component lib
require.context('../node_modules/core-components/src/static/images/', true, /^\.\/.*\.png/);

AppInitializer.init(APP_DOM_ID);

console.info('********************************************');
console.info('Portal App Details');
console.info('Environment: ', process.env.NODE_ENV);
console.info('Build DateTime: ', process.env.BUILD_DATETIME);
console.info('Git Branch: ', process.env.GIT_BRANCH);
console.info('Git Version: ', process.env.GIT_VERSION);
console.info('Git Commit Hash: ', process.env.GIT_COMMIT_HASH);
console.info('********************************************');
