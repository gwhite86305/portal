import {IMovingMapState, Coordinate} from './types';
export const selectOriginPoint = (state: IMovingMapState) => state.flightInfo.originPoint;
export const selectDestinationPoint = (state: IMovingMapState) => state.flightInfo.destinationPoint;
export const selectAircraftId = (state: IMovingMapState) => state.flightInfo.aircraftId;
export const selectOriginName = (state: IMovingMapState) => state.flightInfo.originName;
export const selectDestinationName = (state: IMovingMapState) => state.flightInfo.destinationName;

export const selectPosition = (state: IMovingMapState) => state.flightProgress.position;
export const selectTrackedRoutePoints = (state: IMovingMapState) => state.flightProgress.trackedRoutePoints;
export const selectAltitude = (state: IMovingMapState) => state.flightProgress.altitude;
export const selectAirSpeed = (state: IMovingMapState) => state.flightProgress.airSpeed;
export const selectProgress = (state: IMovingMapState) => state.flightProgress.progress;
export const selectTakeOffTime = (state: IMovingMapState) => state.flightProgress.takeOffTime;
export const selectTimeToDestInMinutes = (state: IMovingMapState) => state.flightProgress.timeToDestInMinutes;
