import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withNotes} from '@storybook/addon-notes';
import {boolean, number, object, text, withKnobs} from '@storybook/addon-knobs/react';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';
import IfeLink from '../dist/components/IfeLink/IfeLink.js';

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.tertiaryBackgroundColor',
    textColor: 'theme.primaryTextColor',
    dividerColor: 'theme.secondaryBackgroundColor',
    margin: {
      xs: '0 0 0 0'
    },
    padding: {
      xs: '0 0 0 0'
    }
  },
  menuItems: [
    {
      iconHeight: '',
      iconUrl: './images/movie_icon.svg',
      label: 'Movies',
      iconAltText: 'Continue to stream your favorite films',
      url: 'https://www.netflix.com',
      id: 'ifeLink-1'
    },
    {
      iconHeight: '',
      iconUrl: './images/tv_icon.svg',
      label: 'T.V.',
      iconAltText: 'Continue to watch live television',
      url: 'https://www.directv.com',
      id: 'ifeLink-2'
    },
    {
      iconHeight: '',
      iconUrl: './images/book_icon.svg',
      label: 'Reading',
      iconAltText: 'Continue to read a selection of in-flight magazines',
      url: 'https://www.readanybook.com',
      id: 'ifeLink-3'
    },
    {
      iconHeight: '',
      iconUrl: './images/music_icon.svg',
      label: 'Music',
      iconAltText: 'Continue to listen to hit music',
      url: 'https://www.spotify.com',
      id: 'ifeLink-4'
    }
  ],
  feature: {},
  isAccessible: true
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";

const stories = storiesOf('IfeLink', module);

stories.addDecorator(withKnobs);

stories
  .add('CMOB-17768: Default', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <IfeLink {...defaultProps} localizedMenuItems={defaultProps.menuItems} />
    </PortalComponentWrapper>
  ))
  .add('CMOB-17768: With 5 sections', () => {
    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <IfeLink
          {...defaultProps}
          localizedMenuItems={[
            {
              iconHeight: '',
              iconUrl: './images/movie_icon.svg',
              label: 'Movies',
              iconAltText: 'Link to movies',
              url: 'https://www.netflix.com',
              id: 'ifeLink-1'
            },
            {
              iconHeight: '',
              iconUrl: './images/tv_icon.svg',
              label: 'T.V.',
              iconAltText: 'Link to TV',
              url: 'https://www.directv.com',
              id: 'ifeLink-2'
            },
            {
              iconHeight: '',
              iconUrl: './images/book_icon.svg',
              label: 'Reading',
              iconAltText: 'Link to ebooks',
              url: 'https://www.readanybook.com',
              id: 'ifeLink-3'
            },
            {
              iconHeight: '',
              iconUrl: './images/music_icon.svg',
              label: 'Music',
              iconAltText: 'Link to music',
              url: 'https://www.spotify.com',
              id: 'ifeLink-4'
            },
            {
              iconHeight: '',
              iconUrl: './images/music_icon.svg',
              label: 'Music',
              iconAltText: 'Link to music2',
              url: 'https://www.directv2.com',
              id: 'ifeLink-5'
            }
          ]}
          feature={{}}
        />
      </PortalComponentWrapper>
    );
  })
  .add('CMOB-17768: With 3 sections', () => {
    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <IfeLink
          {...defaultProps}
          localizedMenuItems={[
            {
              iconHeight: '',
              iconUrl: './images/movie_icon.svg',
              label: 'Movies',
              iconAltText: 'Link to movies',
              url: 'https://www.netflix.com',
              id: 'ifeLink-1'
            },
            {
              iconHeight: '',
              iconUrl: './images/tv_icon.svg',
              label: 'T.V.',
              iconAltText: 'Link to TV',
              url: 'https://www.directv.com',
              id: 'ifeLink-2'
            },
            {
              iconHeight: '',
              iconUrl: './images/book_icon.svg',
              label: 'Reading',
              iconAltText: 'Link to ebooks',
              url: 'https://www.readanybook.com',
              id: 'ifeLink-3'
            }
          ]}
          feature={{}}
        />
      </PortalComponentWrapper>
    );
  })
  .add(
    'CMOB-12691, 17768: With Knob ',
    withNotes(
      'isAccessible is internal and the value of it is derived from ifeState property of device data (  state.device.ifeState == "Available" || state.device.ifeState == "Down")'
    )(() => {
      return (
        <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
          <IfeLink
            {...defaultProps}
            componentTheme={object('componentTheme', defaultProps.componentTheme)}
            feature={object('feature', defaultProps.feature)}
            localizedMenuItems={object('menuItems', defaultProps.menuItems)}
            isAccessible={boolean('internal-isAccessible', defaultProps.isAccessible)}
          />
        </PortalComponentWrapper>
      );
    })
  );
