ARTY_PATH:=artifactory.viasat.com:8091/cmob
BUILD_IMAGE_NAME:=pcms-builder
BUILD_IMAGE_TAG:=1.0.2.17
COMPOSE_PROJECT_NAME?=docker
DEPLOY_DOCKER_NETWORK_NAME?=${COMPOSE_PROJECT_NAME}_portal-integration
# Execute a command with pcms-builder Docker container
# param 1: optional docker options (eg. -e environment variables, leave blank for nothing)
# param 2: path (relative to current directory) to execute command from
# param 3: command to execute
define pcms_exec
	docker run -i --rm $(1) -e LOCAL_USER_ID=`id -u` -e PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true -v `pwd`/$(2):/tests ${ARTY_PATH}/${BUILD_IMAGE_NAME}:${BUILD_IMAGE_TAG} '$(3)'
endef
.PHONY: build test deploy publish-demofree publish-neos clean-rds-db install-hooks

default:: build

ifeq ($(env),development)
    include ./env/development.env
endif
ifeq ($(env),docker)
    include ./env/docker.env
endif
ifeq ($(env),integration)
    include ./env/integration.env
endif
ifeq ($(env),testing)
    include ./env/testing.env
endif

build: 
	$(call pcms_exec,,.,npm run clean --yes && npm run bootstrap --loglevel verbose && npm run build)

build-integration: 
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,npm run clean --yes && npm run bootstrap --loglevel verbose && npm run build:integration)

# Build iceland for integration
build-iceland-integration: 
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,npm run clean --yes && npm run bootstrap --loglevel verbose && npm run build:iceland)


development:
	make -C ./packages/portal-renderer development
	make -C ./packages/schema-server development

schema-service:
	docker-compose -f docker-compose.dev.yml up pcms-portal-schema

renderer-service:
	docker-compose -f docker-compose.dev.yml up pcms-portal-renderer

services:
	docker-compose -f docker-compose.dev.yml up

test:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test --stream --loglevel verbose -- --scope=portal-renderer)

test-nos:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:neos -- --scope=portal-renderer)

test-ice:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:iceland -- --scope=portal-renderer)

test-aal:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:aa -- --scope=portal-renderer)

test-djt:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:djt -- --scope=portal-renderer)

test-jbu:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:jbu -- --scope=portal-renderer)

test-djtsimple:
	$(call pcms_exec,-e CHROMIUM_PATH=/tmp/latest/chrome,.,lerna run test-e2e:djtsimple -- --scope=portal-renderer)

a:
	$(call pcms_exec,,.,bash)



# NOTE: to make a publish or deploy command pull assets from the PB API, just add `ASSET_BASE_URL="http://pb-preprod.naw01.rbodev.viasat.io/api/" ` before PROFILE_PATH (no backticks)


deploy:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,npm run deploy:pfe)

deploy-demofree:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/test.demofree.env && npm run deploy:pfe)

deploy-demopaid:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/test.demopaid.env && npm run deploy:pfe)

deploy-neos:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/test.neos.env && npm run deploy:pfe)

deploy-iceland:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/test.iceland.env && npm run deploy:pfe)

deploy-jbu:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/dev.jbu.env && npm run deploy:pfe)

deploy-aa:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/test.aa.env && npm run deploy:pfe)

deploy-amx:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/dev.amx.env && PROFILE_PATH=./mocks/api/layout/amx/GET.json npm run deploy:pfe)

deploy-amxpaid:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/dev.amxpaid.env && PROFILE_PATH=./mocks/api/layout/amxpaid/GET.json npm run deploy:pfe)

deploy-djt:
	$(call pcms_exec,--network ${DEPLOY_DOCKER_NETWORK_NAME} -e DB_HOST -e DB_USER -e DB_PASSWORD -e DB_SUFFIX,.,. ./env/dev.djt.env && PROFILE_PATH=./mocks/api/layout/djt/GET.json npm run deploy:pfe)

publish-all: publish-demofree publish-demopaid publish-neos publish-iceland publish-aa publish-amx publish-jbu publish-amxpaid publish-djt

publish-demofree:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.demofree.env && npm run publish:pfe -- portal dev)

publish-demopaid:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.demopaid.env && npm run publish:pfe -- demopaid dev)

publish-neos:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.neos.env && npm run publish:pfe -- neos dev)

publish-iceland:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.iceland.env && npm run publish:pfe -- iceland dev)

publish-jbu:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/dev.jbu.env && npm run publish:pfe -- jbu dev)

publish-aa:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.aa.env && npm run publish:pfe -- aa dev)

publish-amx:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.amx.env && npm run publish:pfe -- amx dev)

publish-amxpaid:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.amxpaid.env && npm run publish:pfe -- amxpaid dev)

publish-djt:
	$(call pcms_exec,-e BUILD_NUMBER -e BRANCH_NAME,.,. ./env/test.djt.env && npm run publish:pfe -- djt dev)

clean-rds-db:
	$(call pcms_exec,,.,mysqladmin -f --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} drop twinpeaks_${DB_SUFFIX}) 

install-hooks:
	scripts/installHooks.sh
