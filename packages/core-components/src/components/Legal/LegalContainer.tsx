import * as React from 'react';
import Legal from './Legal';
import {withContent} from 'component-utils/dist/components/ContentAsset';

export default withContent(Legal, [{contentKey: 'Legal'}]) as any;
