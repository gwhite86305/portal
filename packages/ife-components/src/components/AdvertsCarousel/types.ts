import {AdvertItem} from './../../store/adverts/types';
import {AdvertsCarousel as ExternalInterface} from '../common/ComponentsInterface';
import {trackImpression, trackView} from '../../store/analytics/actions';

export interface ICarouselItem {
  advert: AdvertItem;
  onClick: (uid: string) => void;
}

export interface StateProps {
  items: AdvertItem[];
  isLoading: boolean;
  isFetchCompleted: boolean;
}

export interface DispatchProps {
  actions: {
    getAdverts: () => void;
    trackImpression: typeof trackImpression;
    trackView: typeof trackView;
  };
}
export interface InternalInterface {
  onClick: (adId: string) => void;
  onItemInViewPort: any;
}

export interface Props extends StateProps, DispatchProps, ExternalInterface, InternalInterface {}
