import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const showTitleHeaderCSS = '.header .title {display: flex;} .details-container .title {display: none;}';
const showTitlePosterCSS = '.header .title {display: none;} .details-container .title {display: flex;}';

export interface StyledSeriesDetailsContainerProps {
  showTitleHeader?: boolean;
  showBackButton?: boolean;
}

export const StyledContainer = styled.div<StyledSeriesDetailsContainerProps>`
  .series-details-container {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    flex-grow: 1;
  }
  ${({showTitleHeader}) => (showTitleHeader ? showTitleHeaderCSS : showTitlePosterCSS)};
  .header {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingHeader, theme)};
    display: ${({showBackButton, showTitleHeader}) =>
      showBackButton || showTitleHeader ? 'flex' : 'none'};
    align-content: center;
    justify-content: space-between;
    align-items: center;
    .place-holder {
      visibility: hidden;
    }
    .back-button,
    .place-holder {
      display: ${({showBackButton}) => (showBackButton ? 'flex' : 'none')};
    }
    .back-button {
      min-width: auto;
      min-height: auto;
      border-radius: 50%;
      padding: 1rem;
    }
    .back-button svg {
      display: ${({showBackButton}) => (showBackButton ? 'flex' : 'none')};
      ${renderThemeProperty('backButtonColor', 'fill')};
    }
  }

  .title {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingSeriesTitle, theme)};
    ${({theme}) => applyMediaQueriesForStyle('font-size', theme.componentTheme.seriesTitleTextSize, theme)};
    ${renderThemeProperty('seriesTitleTextColor', 'color')};
    ${renderThemeProperty('seriesTitleTextWeight', 'font-weight')};
    display: flex;
    flex: 1;
    justify-content: center;
  }

  .poster-container {
    .poster {
      ${({theme}) => applyMediaQueriesForStyle('margin', theme.componentTheme.marginPoster, theme)};
      ${() => applyMediaQueriesForStyle('height', {xs: '300px', sm: '508px', md: '600px'}, {})};
      ${() => applyMediaQueriesForStyle('width', {xs: '255px', sm: '357px', md: '421px'}, {})};
    }
    display: flex;
    flex-direction: column;
    flex: 1;
    justify-content: center;
    align-items: center;
  }

  .season {
    margin: 16px 0px;
  }

  .season-number {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingSeasonTitle, theme)};
    ${renderThemeProperty('seasonTitleTextColor', 'color')};
    ${renderThemeProperty('seasonTitleTextSize', 'font-size')};
    ${renderThemeProperty('seasonTitleTextWeight', 'font-weight')};
    ${renderThemeProperty('seasonBackgroundColor', 'background-color')};
    min-height: 0;
    > div {
      margin: 0;
    }
    svg {
      ${renderThemeProperty('seasonTitleTextColor', 'fill')};
    }
  }

  .season-details {
    ${renderThemeProperty('paddingSeasonDetails', 'padding')};
    ${renderThemeProperty('episodeBackgroundColor', 'background-color')};

    > ul {
      width: 100%;
    }

    hr {
      ${renderThemeProperty('seasonBackgroundColor', 'background-color', true)};
    }
    .episode {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingEpisode, theme)};
      display: block;
      .title {
        flex: 0;
        display: block;
      }

      .episode-details-container {
        display: flex;
        flex-grow: 1;
        justify-content: space-between;
        flex-direction: column;
      }

      .title-row-wrapper {
        ${_ => applyMediaQueriesForStyle('flex-direction', {xs: 'column', sm: 'row', md: 'column', lg: 'row'}, {})};
        display: flex;
        margin-bottom: 1rem;
      }

      .title-duration-wrapper {
        ${renderThemeProperty('marginTitleDurationWrapper', 'margin')};
      }
    }
    .description {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingEpisodeDescription, theme)};
      ${renderThemeProperty('episodeDescriptionTextColor', 'color')};
      ${renderThemeProperty('episodeDescriptionTextSize', 'font-size')};
      ${renderThemeProperty('episodeDescriptionTextWeight', 'font-weight')};
    }
    .title {
      ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingEpisodeTitle, theme)};
      ${renderThemeProperty('episodeTitleTextColor', 'color')};
      ${renderThemeProperty('episodeTitleTextSize', 'font-size')};
      ${renderThemeProperty('episodeTitleTextWeight', 'font-weight')};
    }
    .duration {
      ${renderThemeProperty('durationTextSize', 'font-size')};
      ${renderThemeProperty('durationTextColor', 'color')};
    }
    .img__name__cta {
      ${_ => applyMediaQueriesForStyle('flex-direction', {xs: 'column'}, {})};
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      position: relative;
      ${_ => applyMediaQueriesForStyle('background', {xs: 'rgb(0, 0, 0)', sm: 'none'}, {})};
      ${_ =>
        applyMediaQueriesForStyle(
          'margin',
          {xs: '0 0 1.4rem 0', sm: '0 1rem 0 0', md: '0 0 1.4rem 0', lg: '0 1rem 0 0'},
          {}
        )};
      padding: 0;
      border-radius: 0;
      span {
        width: auto;
      }
      .img-container {
        overflow: hidden;
        ${_ => applyMediaQueriesForStyle('height', {xs: '100px', md: '80px', lg: '100px'}, {})};
        ${_ => applyMediaQueriesForStyle('max-width', {xs: '200px', md: '160px', lg: '200px'}, {})};
        margin: 0 auto;
      }
      .series-image {
        background-size: cover;
        background-position: center center;
        ${_ => applyMediaQueriesForStyle('width', {xs: '200px', md: '160px', lg: '200px'}, {})};
        height: 100%;
      }
      svg {
        position: absolute;
        fill: white;
        width: 50px;
        height: 50px;
        left: calc(50% - 25px);
        top: calc(50% - 25px);
        z-index: 1;
      }
    }

    .playlist-action {
      display: flex;
      align-items: center;
      cursor: pointer;
      text-transform: none;
      ${renderThemeProperty('playlistButtonTextSize', 'font-size')};
      ${({theme}) => applyMediaQueriesForStyle('margin', theme.componentTheme.marginButtons, theme)};
      ${renderThemeProperty('playlistButtonBackgroundColor', 'background')};
      ${renderThemeProperty('playlistButtonTextColor', 'color')};

      :hover,
      :active {
        ${renderThemeProperty('playlistButtonBackgroundHover', 'background')};
        ${renderThemeProperty('playlistButtonTextColorHover', 'color')};
      }
    }
  }
`;
export default StyledContainer;
