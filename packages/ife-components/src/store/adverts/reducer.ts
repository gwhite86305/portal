import {Action, ActionCreators} from './actions';
import {AdvertItem} from './types';
export interface IAdvertsState {
  readonly error: any;
  readonly items: AdvertItem[];
  readonly isFetchCompleted: boolean;
  readonly isLoading: boolean;
}

export const initialState: IAdvertsState = {
  error: null,
  items: [],
  isFetchCompleted: false,
  isLoading: false
};

export default function reducer(state: IAdvertsState = initialState, action: Action): IAdvertsState {
  switch (action.type) {
    case ActionCreators.getAdvertsStart.type:
      return {...state, isLoading: true, isFetchCompleted: false};
    case ActionCreators.getAdvertsSuccess.type:
      return {
        ...state,
        items: [...action.payload],
        isLoading: false,
        isFetchCompleted: true
      };
    case ActionCreators.getAdvertsError.type:
      return {...state, error: true, isLoading: false, isFetchCompleted: false};
    default:
      return state;
  }
}
