export interface INotificationDispatcher {
  messages: any;
  progress: any;
  flightInfo: any;
  isAudioPausedForAnnouncement: boolean;
}

export interface IMessage {
  name: string;
  value: boolean;
}

export interface IFlightProgress {
  [key: string]: any;
}

export interface IFlightInfo {
  [key: string]: any;
}
