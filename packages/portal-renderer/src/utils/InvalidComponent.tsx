import styled from 'styled-components';

const InvalidComponent = styled.div`
  font-size: 7rem;
  color: ${props => props.theme.primaryTextColor};
  display: flex;
  align-items: center;
  height: 100%;
  justify-content: center;
`;

export default InvalidComponent;
