import React from 'react';
import cloneDeep from 'lodash/cloneDeep';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, object, boolean} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';

import WaitSplash from '../dist/components/WaitSplash/WaitSplash.js';
import PortalComponentWrapper, {
  mockTheme,
  mockThemeIcelandair
} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  localizedHeaderText: 'Header Text',
  localizedSubHeaderText: 'Sub Header Text',
  localizedMessageText: 'Message text here.'
};

const defaultProps = {
  componentTheme: {
    backgroundGradientAngle: '217deg',
    backgroundGradientStartColor: '#003a7d',
    backgroundGradientEndColor: '#3192c3',
    headerTextColor: 'theme.tertiaryTextColor',
    headerFontSize: '32px',
    headerFontWeight: '500',
    headerMargin: {
      xs: '122px auto 26px auto'
    },
    subHeaderTextColor: 'theme.tertiaryTextColor',
    subHeaderFontSize: 'theme.tertiaryFontSize',
    subHeaderFontWeight: '500',
    subHeaderMargin: {
      xs: '15px auto 5px auto'
    },
    subHeaderMaxWidth: {
      xs: '200px'
    },
    messageTextColor: 'theme.tertiaryTextColor',
    messageFontSize: 'theme.secondaryFontSize',
    messageFontWeight: 'normal',
    messageMargin: {
      xs: '0px auto 0px auto'
    },
    centerIconMargin: {
      xs: '10px auto 10px auto'
    }
  },
  feature: {
    showHeaderText: true,
    showCenterIcon: true,
    showSubHeaderText: true,
    showMessageText: true,
    showFooterImage: true,
    showByDefault: true,
    centerIconPath: './images/loadingImage.svg',
    footerImage: {
      xs: './images/PlaneandClouds_Tablet.svg',
      sm: './images/PlaneandClouds_Mobile.svg',
      lg: './images/PlaneandClouds_Laptop.svg'
    }
  }
};

const stories = storiesOf('WaitSplash', module);

stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);

const hideHeaderText = cloneDeep(defaultProps.feature);
hideHeaderText.showHeaderText = false;

const hideIcon = cloneDeep(hideHeaderText);
hideIcon.showCenterIcon = false;

const hideSubHeaderText = cloneDeep(hideIcon);
hideSubHeaderText.showSubHeaderText = false;

const hideMessageText = cloneDeep(hideSubHeaderText);
hideMessageText.showMessageText = false;

const hideFooterImage = cloneDeep(hideMessageText);
hideFooterImage.showFooterImage = false;

stories
  .add('WaitSplash - Default', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('WaitSplash - Hide Header Text', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} feature={object('feature', hideHeaderText)} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('WaitSplash - Hide Center Icon', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} feature={object('feature', hideIcon)} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('WaitSplash - Hide Sub Header Text', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} feature={object('feature', hideSubHeaderText)} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('WaitSplash - Hide Message Text', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} feature={object('feature', hideMessageText)} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('WaitSplash - Hide Footer Image', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <WaitSplash {...defaultProps} feature={object('feature', hideFooterImage)} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ));
