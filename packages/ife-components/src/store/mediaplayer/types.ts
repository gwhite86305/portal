import {AudioItem} from '../audio/types';
import {VideoItem} from '../videos/types';
import {Normalized} from '../../utils/Norm';

export enum MEDIA_PLAYER_STATUS {
  PLAYING = 'PLAYING',
  PAUSED = 'PAUSED'
}

export enum MEDIA_PLAYER_TYPE {
  AUDIO = 'AUDIO',
  NONE = 'NONE',
  VIDEO = 'VIDEO'
}

export interface IDrmServers {
  vualto: {
    fairplayLicenseServer: string;
    widevineLicenseServer: string;
    playReadyLicenseServer: string;
    vuplayServer: string;
  };
}

export interface PlaylistItem {
  parentId?: string;
  season?: number;
  episode?: number;
  title?: string;
  artist?: string;
  type?: string;
  uid: string;
}

export type SavedPlaylist = Normalized<string, PlaylistItem>;

export interface MediaPlayerState {
  audioLanguages: string[];
  currentItem: VideoItem | AudioItem | undefined;
  currentPlaylistItemIndex: number;
  drmServerUrl: IDrmServers | undefined;
  drmToken: string;
  fileManifest: string;
  fileType: MEDIA_PLAYER_TYPE;
  isBuffering: boolean;
  isFullScreen: boolean;
  isLoadingDrmServer: boolean;
  isLoadingToken: boolean;
  playerPage: string | undefined;
  playlistItems: SavedPlaylist;
  seekToRange: number;
  status: MEDIA_PLAYER_STATUS;
  subtitles: string[];
  volume: number;
  subtitleLanguage: string;
  audioLanguage: string;
  playlistLoading: boolean;
}

export const PLAYLIST_STORAGE_KEY = 'PLAYLIST_STORAGE_KEY';
