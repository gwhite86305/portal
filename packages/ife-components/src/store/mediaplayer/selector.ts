import {IRootState} from '../..';
import {VideoItem} from '../videos/types';
import {AudioItem} from '../audio/types';
import {MediaPlayerState} from './types';
import {values} from 'lodash-es';
type PlaylistItem = VideoItem | AudioItem;

export const selectCurrentVideoUid = (state: IRootState) => {
  const {selectedItem} = state.ifeVideos;
  if (!selectedItem) {
    return '';
  }
  return selectedItem.uid;
};

export const selectPlayListItems = (state: IRootState): PlaylistItem[] => {
  const {allIds, byId} = state.ifeMediaPlayer.playlistItems;
  const {ifeVideos, ifeAudio, ifeMovies} = state;
  const movieItems = ifeMovies.items.allIds.map(id => ifeMovies.items.byId[id]);
  return [
    ...(allIds.reduce((acc: any[], uid) => {
      const playListItem = [...movieItems, ...ifeVideos.items, ...values(ifeAudio.items.byId)].find(
        item => item.uid === uid || byId[uid].parentId === item.uid
      );
      playListItem && acc.push({...playListItem, ...byId[uid], uid});
      return acc;
    }, []) as any)
  ];
};

export const selectPlaylistLoading = (state: MediaPlayerState) => state.playlistLoading;

export const selectCurrentItem = (state: IRootState) => state.ifeMediaPlayer.currentItem;
