import {PortalComponent, utils} from 'component-utils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import * as React from 'react';
import ScheletonLoader from '../common/CarouselView/ScheletonLoader';
import Music from './Music';
import ErrorMessage from '../common/ErrorMessage';
import {IRootState} from '../..';
import {trackImpression} from '../../store/analytics/actions';
import {selectAudioByCategories} from '../../store/audio/selectors';
import {Props, DispatchProps, StateProps} from './types';

class IFEMusicContainer extends React.Component<Props, any> {
  public static contextType = RouterContext;
  private renderMusic = () => {
    const {
      feature: {category},
      isLoading,
      items,
      trackImpression
    } = this.props;

    if (isLoading) {
      return <ScheletonLoader />;
    }

    // items filtered to a category such as 'podcasts'
    const categoryItems = items
      .map(audioCategory => ({
        ...audioCategory,
        items: audioCategory.items.filter(audioGroup => audioGroup.categories.find(cat => cat === category))
      }))
      .filter(audioCategory => audioCategory.items.length);

    return categoryItems.length ? (
      <Music items={categoryItems} onItemInViewPort={trackImpression} onItemClick={this.navigateToDetailPage} />
    ) : (
      <ErrorMessage text={'No Audio Available'} />
    );
  };
  render() {
    return <PortalComponent componentTheme={this.props.componentTheme}>{this.renderMusic()}</PortalComponent>;
  }

  private navigateToDetailPage = (contentUid: string) => {
    this.context.history.push(`${this.props.feature.synopsisPage}?audioId=${contentUid}`);
  };
}

const mapStoreToProps = (state: IRootState, props: Props): StateProps => {
  return{
  isLoading: state.ifeAudio.isLoadingItems,
  items: selectAudioByCategories(state.ifeAudio)
}};

const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  trackImpression: item => dispatch(trackImpression(item))
});

const connectWrapper = utils.connectWrapper.default;
export default connectWrapper(mapStoreToProps, mapDispatchToProps)(IFEMusicContainer);
