const path = require('path');

module.exports = function(request, response) {
  const defaults = {
    debug: true,
    currentConditions: [
      {
        cityName: 'Montreal',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 37.0},
        airport: 'CYUL'
      },
      {
        cityName: 'Atlanta',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 64.0},
        airport: 'KATL'
      },
      {
        cityName: 'Austin',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 57.0},
        airport: 'KAUS'
      },
      {
        cityName: 'Hartford',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 46.0},
        airport: 'KBDL'
      },
      {
        cityName: 'Nashville',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 54.0},
        airport: 'KBNA'
      },
      {
        cityName: 'Boston',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 48.0},
        airport: 'KBOS'
      },
      {
        cityName: 'Baltimore',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 50.0},
        airport: 'KBWI'
      },
      {
        cityName: 'Charlotte',
        currentCondition: {weatherText: 'Mostly cloudy', weatherIcon: 6, temperature: 66.0},
        airport: 'KCLT'
      },
      {
        cityName: 'Dallas',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 56.0},
        airport: 'KDAL'
      },
      {
        cityName: 'Washington D.C.',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 54.0},
        airport: 'KDCA'
      },
      {
        cityName: 'Denver',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 30.0},
        airport: 'KDEN'
      },
      {
        cityName: 'Detroit',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 43.0},
        airport: 'KDTW'
      },
      {
        cityName: 'Newark',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 46.0},
        airport: 'KEWR'
      },
      {
        cityName: 'Fort Lauderdale',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 83.0},
        airport: 'KFLL'
      },
      {
        cityName: 'Houston',
        currentCondition: {weatherText: 'Some clouds', weatherIcon: 36, temperature: 66.0},
        airport: 'KIAH'
      },
      {
        cityName: 'New York, NY',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 47.0},
        airport: 'KJFK'
      },
      {
        cityName: 'Las Vegas',
        currentCondition: {weatherText: 'Mostly sunny', weatherIcon: 2, temperature: 37.0},
        airport: 'KLAS'
      },
      {
        cityName: 'Los Angeles',
        currentCondition: {weatherText: 'Mostly sunny', weatherIcon: 2, temperature: 48.0},
        airport: 'KLAX'
      },
      {
        cityName: 'Kansas City',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 38.0},
        airport: 'KMCI'
      },
      {
        cityName: 'Orlando',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 81.0},
        airport: 'KMCO'
      },
      {
        cityName: 'Miami',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 85.0},
        airport: 'KMIA'
      },
      {
        cityName: 'Minneapolis',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 20.0},
        airport: 'KMSP'
      },
      {
        cityName: 'New Orleans',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 66.0},
        airport: 'KMSY'
      },
      {
        cityName: 'Chicago',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 44.0},
        airport: 'KORD'
      },
      {
        cityName: 'Palm Beach',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 80.0},
        airport: 'KPBI'
      },
      {
        cityName: 'Portland',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 35.0},
        airport: 'KPDX'
      },
      {
        cityName: 'Philadelphia',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 50.0},
        airport: 'KPHL'
      },
      {
        cityName: 'Phoenix',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 44.0},
        airport: 'KPHX'
      },
      {
        cityName: 'Raleigh',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 65.0},
        airport: 'KRDU'
      },
      {
        cityName: 'Reno',
        currentCondition: {weatherText: 'Mostly cloudy', weatherIcon: 6, temperature: 35.0},
        airport: 'KRNO'
      },
      {
        cityName: 'Fort Myers',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 80.0},
        airport: 'KRSW'
      },
      {
        cityName: 'San Diego',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 53.0},
        airport: 'KSAN'
      },
      {
        cityName: 'San Antonio',
        currentCondition: {weatherText: 'Mostly cloudy', weatherIcon: 6, temperature: 64.0},
        airport: 'KSAT'
      },
      {
        cityName: 'Seattle',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 32.0},
        airport: 'KSEA'
      },
      {
        cityName: 'San Francisco',
        currentCondition: {weatherText: 'Light rain', weatherIcon: 12, temperature: 55.0},
        airport: 'KSFO'
      },
      {
        cityName: 'San Jose',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 53.0},
        airport: 'KSJC'
      },
      {
        cityName: 'Salt Lake City',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 39.0},
        airport: 'KSLC'
      },
      {
        cityName: 'Orange County',
        currentCondition: {weatherText: 'Mostly sunny', weatherIcon: 2, temperature: 49.0},
        airport: 'KSNA'
      },
      {
        cityName: 'St. Louis',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 50.0},
        airport: 'KSTL'
      },
      {
        cityName: 'Tampa',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 7, temperature: 74.0},
        airport: 'KTPA'
      },
      {
        cityName: 'Tulsa',
        currentCondition: {weatherText: 'Sunny', weatherIcon: 1, temperature: 50.0},
        airport: 'KTUL'
      },
      {
        cityName: 'Guatemala City',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 67.0},
        airport: 'MGGT'
      },
      {
        cityName: 'Kingston',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 84.0},
        airport: 'MKJP'
      },
      {
        cityName: 'Montego Bay',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 87.0},
        airport: 'MKJS'
      },
      {
        cityName: 'Cozumel',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 85.0},
        airport: 'MMCZ'
      },
      {
        cityName: 'Mexico City',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 64.0},
        airport: 'MMMX'
      },
      {
        cityName: 'Cabo San Lucas',
        currentCondition: {weatherText: 'Mostly sunny', weatherIcon: 2, temperature: 62.0},
        airport: 'MMSD'
      },
      {
        cityName: 'Cancun',
        currentCondition: {weatherText: 'Mostly cloudy', weatherIcon: 6, temperature: 83.0},
        airport: 'MMUN'
      },
      {
        cityName: 'San Jose, CR',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 76.0},
        airport: 'MROC'
      },
      {
        cityName: 'Port-au-Prince',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 84.0},
        airport: 'MTPP'
      },
      {
        cityName: 'St. Thomas',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 83.0},
        airport: 'TIST'
      },
      {
        cityName: 'San Juan, PR',
        currentCondition: {weatherText: 'Partly sunny', weatherIcon: 3, temperature: 81.0},
        airport: 'TJSJ'
      },
      {
        cityName: 'New York, NY',
        currentCondition: {weatherText: 'Cloudy', weatherIcon: 3, temperature: 46.0},
        airport: 'KLGA'
      }
    ],
    bypassServiceTiers: [
      {
        description: 'Aa PED Bypass Plan',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 85,
        cost: 0.0,
        name: 'Aa PED Bypass Plan',
        npiCodeId: 'AaPedBypass',
        autoConnect: false,
        alternateCost: null,
        metadata: {vpsBypassEnabled: false, omBypassEnabled: false},
        duration: 86400
      }
    ],
    dailyForecasts: [
      {
        cityName: 'Montreal',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Snow', icon: 22},
            temperature: {maximum: {value: 38.0}, minimum: {value: 28.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 40.0}, minimum: {value: 24.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 43.0}, minimum: {value: 30.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 43.0}, minimum: {value: 28.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Dreary', icon: 8},
            temperature: {maximum: {value: 40.0}, minimum: {value: 30.0}}
          }
        ],
        airport: 'CYUL'
      },
      {
        cityName: 'Atlanta',
        dailyForecast: [
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 67.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 66.0}, minimum: {value: 50.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 65.0}, minimum: {value: 51.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 62.0}, minimum: {value: 59.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Cloudy', icon: 7},
            temperature: {maximum: {value: 67.0}, minimum: {value: 40.0}}
          }
        ],
        airport: 'KATL'
      },
      {
        cityName: 'Austin',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Thunderstorms', icon: 15},
            temperature: {maximum: {value: 65.0}, minimum: {value: 48.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 70.0}, minimum: {value: 53.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 69.0}, minimum: {value: 62.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Cloudy', icon: 7},
            temperature: {maximum: {value: 75.0}, minimum: {value: 51.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 65.0}, minimum: {value: 43.0}}
          }
        ],
        airport: 'KAUS'
      },
      {
        cityName: 'Hartford',
        dailyForecast: [
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 38.0}, minimum: {value: 31.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 52.0}, minimum: {value: 29.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 52.0}, minimum: {value: 30.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 56.0}, minimum: {value: 34.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Mostly cloudy w/ showers', icon: 13},
            temperature: {maximum: {value: 54.0}, minimum: {value: 39.0}}
          }
        ],
        airport: 'KBDL'
      },
      {
        cityName: 'Nashville',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 56.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 62.0}, minimum: {value: 35.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 67.0}, minimum: {value: 54.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 63.0}, minimum: {value: 55.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Cloudy', icon: 7},
            temperature: {maximum: {value: 60.0}, minimum: {value: 35.0}}
          }
        ],
        airport: 'KBNA'
      },
      {
        cityName: 'Boston',
        dailyForecast: [
          {
            night: {iconPhrase: 'Fog', icon: 11},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 41.0}, minimum: {value: 36.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 51.0}, minimum: {value: 32.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 51.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 56.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Rain and snow', icon: 29},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 48.0}, minimum: {value: 40.0}}
          }
        ],
        airport: 'KBOS'
      },
      {
        cityName: 'Baltimore',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 51.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 55.0}, minimum: {value: 33.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 57.0}, minimum: {value: 36.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 59.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 53.0}, minimum: {value: 42.0}}
          }
        ],
        airport: 'KBWI'
      },
      {
        cityName: 'Charlotte',
        dailyForecast: [
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 71.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 62.0}, minimum: {value: 41.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 64.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 58.0}, minimum: {value: 53.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 63.0}, minimum: {value: 43.0}}
          }
        ],
        airport: 'KCLT'
      },
      {
        cityName: 'Dallas',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 64.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 65.0}, minimum: {value: 48.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 62.0}, minimum: {value: 59.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 69.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 60.0}, minimum: {value: 40.0}}
          }
        ],
        airport: 'KDAL'
      },
      {
        cityName: 'Washington D.C.',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 64.0}, minimum: {value: 46.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 55.0}, minimum: {value: 33.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 59.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 60.0}, minimum: {value: 46.0}}
          },
          {
            night: {iconPhrase: 'Thunderstorms', icon: 15},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 52.0}, minimum: {value: 44.0}}
          }
        ],
        airport: 'KDCA'
      },
      {
        cityName: 'Denver',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-07:00',
            epochDate: 1519567200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 41.0}, minimum: {value: 18.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-07:00',
            epochDate: 1519653600,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 50.0}, minimum: {value: 19.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-07:00',
            epochDate: 1519740000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 47.0}, minimum: {value: 20.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-28T07:00:00-07:00',
            epochDate: 1519826400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 42.0}, minimum: {value: 18.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-07:00',
            epochDate: 1519912800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 52.0}, minimum: {value: 26.0}}
          }
        ],
        airport: 'KDEN'
      },
      {
        cityName: 'Detroit',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 44.0}, minimum: {value: 31.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 50.0}, minimum: {value: 32.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 56.0}, minimum: {value: 39.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 52.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 44.0}, minimum: {value: 32.0}}
          }
        ],
        airport: 'KDTW'
      },
      {
        cityName: 'Newark',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 45.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 54.0}, minimum: {value: 34.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 55.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 56.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 51.0}, minimum: {value: 42.0}}
          }
        ],
        airport: 'KEWR'
      },
      {
        cityName: 'Fort Lauderdale',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 82.0}, minimum: {value: 73.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 82.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 87.0}, minimum: {value: 70.0}}
          }
        ],
        airport: 'KFLL'
      },
      {
        cityName: 'Houston',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-26T07:00:00+11:00',
            epochDate: 1519588800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 69.0}, minimum: {value: 62.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-27T07:00:00+11:00',
            epochDate: 1519675200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 72.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00+11:00',
            epochDate: 1519761600,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 77.0}, minimum: {value: 62.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-03-01T07:00:00+11:00',
            epochDate: 1519848000,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 73.0}, minimum: {value: 63.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-03-02T07:00:00+11:00',
            epochDate: 1519934400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 71.0}, minimum: {value: 65.0}}
          }
        ],
        airport: 'KIAH'
      },
      {
        cityName: 'New York, NY',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 44.0}, minimum: {value: 39.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 51.0}, minimum: {value: 35.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 50.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 52.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 50.0}, minimum: {value: 40.0}}
          }
        ],
        airport: 'KJFK'
      },
      {
        cityName: 'Las Vegas',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 55.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 65.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 53.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 61.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 63.0}, minimum: {value: 47.0}}
          }
        ],
        airport: 'KLAS'
      },
      {
        cityName: 'Los Angeles',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 68.0}, minimum: {value: 48.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 63.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 58.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 62.0}, minimum: {value: 46.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 60.0}, minimum: {value: 52.0}}
          }
        ],
        airport: 'KLAX'
      },
      {
        cityName: 'Kansas City',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 53.0}, minimum: {value: 30.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 56.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 59.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 57.0}, minimum: {value: 34.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 49.0}, minimum: {value: 28.0}}
          }
        ],
        airport: 'KMCI'
      },
      {
        cityName: 'Orlando',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 86.0}, minimum: {value: 67.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 89.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 82.0}, minimum: {value: 64.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 85.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 88.0}, minimum: {value: 61.0}}
          }
        ],
        airport: 'KMCO'
      },
      {
        cityName: 'Miami',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 83.0}, minimum: {value: 69.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 71.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 86.0}, minimum: {value: 70.0}}
          }
        ],
        airport: 'KMIA'
      },
      {
        cityName: 'Minneapolis',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 32.0}, minimum: {value: 17.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 39.0}, minimum: {value: 24.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 39.0}, minimum: {value: 21.0}}
          },
          {
            night: {iconPhrase: 'Rain and snow', icon: 29},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 39.0}, minimum: {value: 26.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Snow', icon: 22},
            temperature: {maximum: {value: 37.0}, minimum: {value: 16.0}}
          }
        ],
        airport: 'KMSP'
      },
      {
        cityName: 'New Orleans',
        dailyForecast: [
          {
            night: {iconPhrase: 'Thunderstorms', icon: 15},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Thunderstorms', icon: 15},
            temperature: {maximum: {value: 73.0}, minimum: {value: 67.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 74.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Mostly cloudy w/ t-storms', icon: 16},
            temperature: {maximum: {value: 77.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 82.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 74.0}, minimum: {value: 53.0}}
          }
        ],
        airport: 'KMSY'
      },
      {
        cityName: 'Chicago',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 45.0}, minimum: {value: 31.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 51.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 56.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 50.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 43.0}, minimum: {value: 30.0}}
          }
        ],
        airport: 'KORD'
      },
      {
        cityName: 'Palm Beach',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 84.0}, minimum: {value: 67.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 84.0}, minimum: {value: 69.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 83.0}, minimum: {value: 69.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 87.0}, minimum: {value: 69.0}}
          }
        ],
        airport: 'KPBI'
      },
      {
        cityName: 'Portland',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy w/ showers', icon: 40},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 46.0}, minimum: {value: 32.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 47.0}, minimum: {value: 34.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 46.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 47.0}, minimum: {value: 39.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 48.0}, minimum: {value: 33.0}}
          }
        ],
        airport: 'KPDX'
      },
      {
        cityName: 'Philadelphia',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 48.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 57.0}, minimum: {value: 36.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 56.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 57.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 53.0}, minimum: {value: 44.0}}
          }
        ],
        airport: 'KPHL'
      },
      {
        cityName: 'Phoenix',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-07:00',
            epochDate: 1519567200,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 64.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-07:00',
            epochDate: 1519653600,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 69.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-27T07:00:00-07:00',
            epochDate: 1519740000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 66.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-28T07:00:00-07:00',
            epochDate: 1519826400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 60.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-07:00',
            epochDate: 1519912800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 68.0}, minimum: {value: 47.0}}
          }
        ],
        airport: 'KPHX'
      },
      {
        cityName: 'Raleigh',
        dailyForecast: [
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 73.0}, minimum: {value: 57.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 59.0}, minimum: {value: 41.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 62.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 60.0}, minimum: {value: 51.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 63.0}, minimum: {value: 43.0}}
          }
        ],
        airport: 'KRDU'
      },
      {
        cityName: 'Reno',
        dailyForecast: [
          {
            night: {iconPhrase: 'Snow', icon: 22},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 48.0}, minimum: {value: 31.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy w/ flurries', icon: 43},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Snow', icon: 22},
            temperature: {maximum: {value: 36.0}, minimum: {value: 14.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 34.0}, minimum: {value: 17.0}}
          },
          {
            night: {iconPhrase: 'Snow', icon: 22},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 43.0}, minimum: {value: 31.0}}
          },
          {
            night: {iconPhrase: 'Rain and snow', icon: 29},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Snow', icon: 22},
            temperature: {maximum: {value: 43.0}, minimum: {value: 30.0}}
          }
        ],
        airport: 'KRNO'
      },
      {
        cityName: 'Fort Myers',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 84.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 84.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 84.0}, minimum: {value: 66.0}}
          }
        ],
        airport: 'KRSW'
      },
      {
        cityName: 'San Diego',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 66.0}, minimum: {value: 49.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy w/ showers', icon: 40},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 64.0}, minimum: {value: 50.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Thunderstorms', icon: 15},
            temperature: {maximum: {value: 58.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 62.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 63.0}, minimum: {value: 51.0}}
          }
        ],
        airport: 'KSAN'
      },
      {
        cityName: 'San Antonio',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Thunderstorms', icon: 15},
            temperature: {maximum: {value: 69.0}, minimum: {value: 48.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 69.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 73.0}, minimum: {value: 64.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 81.0}, minimum: {value: 55.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 67.0}, minimum: {value: 46.0}}
          }
        ],
        airport: 'KSAT'
      },
      {
        cityName: 'Seattle',
        dailyForecast: [
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 44.0}, minimum: {value: 34.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy w/ showers', icon: 40},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 45.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 44.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 44.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 46.0}, minimum: {value: 32.0}}
          }
        ],
        airport: 'KSEA'
      },
      {
        cityName: 'San Francisco',
        dailyForecast: [
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 57.0}, minimum: {value: 46.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ t-storms', icon: 41},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Mostly cloudy w/ t-storms', icon: 16},
            temperature: {maximum: {value: 53.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 60.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 55.0}, minimum: {value: 49.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 54.0}, minimum: {value: 46.0}}
          }
        ],
        airport: 'KSFO'
      },
      {
        cityName: 'San Jose',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 62.0}, minimum: {value: 44.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ t-storms', icon: 41},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 56.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 61.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 59.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 56.0}, minimum: {value: 45.0}}
          }
        ],
        airport: 'KSJC'
      },
      {
        cityName: 'Salt Lake City',
        dailyForecast: [
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-25T07:00:00-07:00',
            epochDate: 1519567200,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 34.0}, minimum: {value: 30.0}}
          },
          {
            night: {iconPhrase: 'Flurries', icon: 19},
            date: '2018-02-26T07:00:00-07:00',
            epochDate: 1519653600,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 44.0}, minimum: {value: 28.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-27T07:00:00-07:00',
            epochDate: 1519740000,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 40.0}, minimum: {value: 22.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-07:00',
            epochDate: 1519826400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 39.0}, minimum: {value: 24.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-07:00',
            epochDate: 1519912800,
            day: {iconPhrase: 'Flurries', icon: 19},
            temperature: {maximum: {value: 46.0}, minimum: {value: 36.0}}
          }
        ],
        airport: 'KSLC'
      },
      {
        cityName: 'Orange County',
        dailyForecast: [
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-25T07:00:00-08:00',
            epochDate: 1519570800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 66.0}, minimum: {value: 49.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy w/ showers', icon: 40},
            date: '2018-02-26T07:00:00-08:00',
            epochDate: 1519657200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 62.0}, minimum: {value: 49.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-27T07:00:00-08:00',
            epochDate: 1519743600,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 58.0}, minimum: {value: 46.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-08:00',
            epochDate: 1519830000,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 61.0}, minimum: {value: 50.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-03-01T07:00:00-08:00',
            epochDate: 1519916400,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 60.0}, minimum: {value: 54.0}}
          }
        ],
        airport: 'KSNA'
      },
      {
        cityName: 'St. Louis',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 52.0}, minimum: {value: 32.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 59.0}, minimum: {value: 37.0}}
          },
          {
            night: {iconPhrase: 'Showers', icon: 12},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 64.0}, minimum: {value: 47.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Mostly cloudy w/ showers', icon: 13},
            temperature: {maximum: {value: 61.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Mostly cloudy', icon: 6},
            temperature: {maximum: {value: 46.0}, minimum: {value: 28.0}}
          }
        ],
        airport: 'KSTL'
      },
      {
        cityName: 'Tampa',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 85.0}, minimum: {value: 67.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 67.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 82.0}, minimum: {value: 61.0}}
          }
        ],
        airport: 'KTPA'
      },
      {
        cityName: 'Tulsa',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 60.0}, minimum: {value: 33.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 63.0}, minimum: {value: 41.0}}
          },
          {
            night: {iconPhrase: 'Dreary', icon: 8},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 62.0}, minimum: {value: 51.0}}
          },
          {
            night: {iconPhrase: 'Cloudy', icon: 7},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Showers', icon: 12},
            temperature: {maximum: {value: 69.0}, minimum: {value: 35.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 56.0}, minimum: {value: 32.0}}
          }
        ],
        airport: 'KTUL'
      },
      {
        cityName: 'Guatemala City',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 79.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 80.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 78.0}, minimum: {value: 55.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 79.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 81.0}, minimum: {value: 58.0}}
          }
        ],
        airport: 'MGGT'
      },
      {
        cityName: 'Kingston',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 84.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 84.0}, minimum: {value: 71.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 85.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 84.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 86.0}, minimum: {value: 70.0}}
          }
        ],
        airport: 'MKJP'
      },
      {
        cityName: 'Montego Bay',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 84.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 85.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 84.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 84.0}, minimum: {value: 71.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 85.0}, minimum: {value: 70.0}}
          }
        ],
        airport: 'MKJS'
      },
      {
        cityName: 'Cozumel',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 73.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 84.0}, minimum: {value: 73.0}}
          }
        ],
        airport: 'MMCZ'
      },
      {
        cityName: 'Mexico City',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 76.0}, minimum: {value: 54.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 76.0}, minimum: {value: 54.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Partly sunny w/ t-storms', icon: 17},
            temperature: {maximum: {value: 77.0}, minimum: {value: 52.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 78.0}, minimum: {value: 53.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 78.0}, minimum: {value: 52.0}}
          }
        ],
        airport: 'MMMX'
      },
      {
        cityName: 'Cabo San Lucas',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-07:00',
            epochDate: 1519567200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 74.0}, minimum: {value: 54.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-26T07:00:00-07:00',
            epochDate: 1519653600,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 76.0}, minimum: {value: 58.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-07:00',
            epochDate: 1519740000,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 76.0}, minimum: {value: 60.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-28T07:00:00-07:00',
            epochDate: 1519826400,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 74.0}, minimum: {value: 56.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-07:00',
            epochDate: 1519912800,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 76.0}, minimum: {value: 57.0}}
          }
        ],
        airport: 'MMSD'
      },
      {
        cityName: 'Cancun',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 74.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 73.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 83.0}, minimum: {value: 73.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 84.0}, minimum: {value: 73.0}}
          }
        ],
        airport: 'MMUN'
      },
      {
        cityName: 'San Jose, CR',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-06:00',
            epochDate: 1519563600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 82.0}, minimum: {value: 63.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-06:00',
            epochDate: 1519650000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 83.0}, minimum: {value: 64.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-06:00',
            epochDate: 1519736400,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 83.0}, minimum: {value: 64.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-28T07:00:00-06:00',
            epochDate: 1519822800,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 83.0}, minimum: {value: 64.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-03-01T07:00:00-06:00',
            epochDate: 1519909200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 85.0}, minimum: {value: 62.0}}
          }
        ],
        airport: 'MROC'
      },
      {
        cityName: 'Port-au-Prince',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 1519560000,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 88.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 1519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 89.0}, minimum: {value: 65.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 1519732800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 88.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 1519819200,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 88.0}, minimum: {value: 66.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 1519905600,
            day: {iconPhrase: 'Mostly sunny', icon: 2},
            temperature: {maximum: {value: 90.0}, minimum: {value: 65.0}}
          }
        ],
        airport: 'MTPP'
      },
      {
        cityName: 'St. Thomas',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy w/ showers', icon: 40},
            date: '2018-02-25T07:00:00-04:00',
            epochDate: 1519556400,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 81.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-04:00',
            epochDate: 1519642800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 81.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Intermittent clouds', icon: 36},
            date: '2018-02-27T07:00:00-04:00',
            epochDate: 1519729200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 81.0}, minimum: {value: 72.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-04:00',
            epochDate: 1519815600,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 81.0}, minimum: {value: 71.0}}
          },
          {
            night: {iconPhrase: 'Mostly clear', icon: 34},
            date: '2018-03-01T07:00:00-04:00',
            epochDate: 1519902000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 81.0}, minimum: {value: 71.0}}
          }
        ],
        airport: 'TIST'
      },
      {
        cityName: 'San Juan, PR',
        dailyForecast: [
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-25T07:00:00-04:00',
            epochDate: 1519556400,
            day: {iconPhrase: 'Mostly cloudy w/ showers', icon: 13},
            temperature: {maximum: {value: 80.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-26T07:00:00-04:00',
            epochDate: 1519642800,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 81.0}, minimum: {value: 70.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy w/ showers', icon: 39},
            date: '2018-02-27T07:00:00-04:00',
            epochDate: 1519729200,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 82.0}, minimum: {value: 69.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-28T07:00:00-04:00',
            epochDate: 1519815600,
            day: {iconPhrase: 'Partly sunny w/ showers', icon: 14},
            temperature: {maximum: {value: 81.0}, minimum: {value: 68.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-03-01T07:00:00-04:00',
            epochDate: 1519902000,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 82.0}, minimum: {value: 66.0}}
          }
        ],
        airport: 'TJSJ'
      },
      {
        cityName: 'New York, NY',
        dailyForecast: [
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-25T07:00:00-05:00',
            epochDate: 2519560000,
            day: {iconPhrase: 'Rain', icon: 18},
            temperature: {maximum: {value: 43.0}, minimum: {value: 42.0}}
          },
          {
            night: {iconPhrase: 'Clear', icon: 33},
            date: '2018-02-26T07:00:00-05:00',
            epochDate: 2519646400,
            day: {iconPhrase: 'Partly sunny', icon: 3},
            temperature: {maximum: {value: 55.0}, minimum: {value: 38.0}}
          },
          {
            night: {iconPhrase: 'Partly cloudy', icon: 35},
            date: '2018-02-27T07:00:00-05:00',
            epochDate: 2519732800,
            day: {iconPhrase: 'Sunny', icon: 1},
            temperature: {maximum: {value: 53.0}, minimum: {value: 40.0}}
          },
          {
            night: {iconPhrase: 'Mostly cloudy', icon: 38},
            date: '2018-02-28T07:00:00-05:00',
            epochDate: 2519819200,
            day: {iconPhrase: 'Intermittent clouds', icon: 4},
            temperature: {maximum: {value: 56.0}, minimum: {value: 43.0}}
          },
          {
            night: {iconPhrase: 'Rain', icon: 18},
            date: '2018-03-01T07:00:00-05:00',
            epochDate: 2519905600,
            day: {iconPhrase: 'Mostly cloudy w/ showers', icon: 13},
            temperature: {maximum: {value: 52.0}, minimum: {value: 44.0}}
          }
        ],
        airport: 'KLGA'
      }
    ],
    serviceTiers: [
      {
        description: 'Aa PED Default Plan',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 53,
        cost: 0.0,
        name: 'Aa PED Default Plan',
        npiCodeId: 'AaPedDefault',
        autoConnect: true,
        alternateCost: null,
        metadata: null,
        duration: 86400
      },
      {
        description: 'Aa PED Free Beta Plan',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 54,
        cost: 0.0,
        name: 'Aa PED Free Beta Plan',
        npiCodeId: 'AaPedFreeBeta',
        autoConnect: false,
        alternateCost: null,
        metadata: null,
        duration: 86400
      },
      {
        description: 'Aa PED GoGo Plan',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 71,
        cost: 0.0,
        name: 'Aa PED GoGo Plan',
        npiCodeId: 'AaPedGogo',
        autoConnect: false,
        alternateCost: null,
        metadata: {
          redirectUri:
            'http://10.137.47.165:9889/v1/authorize?client_id=viasat_aa_client&response_type=code&grant_type=authorization_code&redirect_uri=http://10.137.47.165:8080/roaming/gogo&c_param=71',
          provider: 'gogo'
        },
        duration: 86400
      },
      {
        description: '1-Hour Pass',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 72,
        cost: 12.0,
        name: 'Aa PED LH Premium Plan (1-hour)',
        npiCodeId: 'AaPedPremium',
        autoConnect: false,
        alternateCost: null,
        metadata: {assetKeySuffix: 'Hour'},
        duration: 3600
      },
      {
        description: 'Flight Pass',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 73,
        cost: 16.0,
        name: 'Aa PED LH Premium Plan (full-flight)',
        npiCodeId: 'AaPedPremium',
        autoConnect: false,
        alternateCost: null,
        metadata: {assetKeySuffix: 'Full'},
        duration: 84600
      },
      {
        description: 'Aa PED Boingo Plan',
        customerId: 5,
        active: true,
        deviceTypeMask: 0,
        deviceCategoryId: 'PED',
        id: 81,
        cost: 0.0,
        name: 'Aa PED Boingo Plan',
        npiCodeId: 'AaPedBoingo',
        autoConnect: false,
        alternateCost: null,
        metadata: {
          redirectUri:
            'https://c01.client.boingo.com/uam/viasat/?login_url=https%3A%2F%2Faa.viasat.com%2Froaming%2Fboingo',
          provider: 'boingo'
        },
        duration: 86400
      }
    ],
    locations: {
      destinationLatitude: 40.7771988,
      originCode: 'DFW',
      originCountry: '---',
      destination: 'New York, NY',
      originElevation: '---',
      originLatitude: 32.897,
      destinationElevation: 21,
      originLongitude: -97.038,
      destinationLongitude: -73.8725967,
      destinationCountry: 'US',
      destinationCode: 'LGA',
      origin: 'Dallas, TX'
    },
    currencyCode: 'USD'
  };

  layout = require(path.join('..', '..', '..', process.env.PROFILE_URL));

  respJson = {...defaults, ...layout};

  response
    .status(200)
    .json(respJson)
    .end();
};
