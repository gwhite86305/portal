import {IAnalyticsActionMeta} from 'component-utils';
import * as endPointConfig from '../../utils/config.json';
import {axiosInstanceV2} from '../../utils/utils';
import {AdvertItem} from '../adverts/types';
import {AudioGroup, AudioItem} from '../audio/types';
import {MovieItem} from '../movies/types';
import {VideoGroup, VideoItem} from '../videos/types';
import {AnalyticEventType} from './actions';

const headers = {'Content-Type': 'application/vnd.viasat.ife.v2.1+json;charset=UTF-8'};
interface IConfig {
  [property: string]: string;
}

// check if this works for everying
export const buildContentUsageMeta = (
  usageType: 'view' | 'imp' | 'play',
  items: (VideoItem | MovieItem | AudioGroup | VideoGroup | AdvertItem | AudioItem)[]
): IAnalyticsActionMeta => ({
  analytics: {
    url: `${(endPointConfig as IConfig).API_IFE_BASE_URL}/stats/content/usage`,
    method: 'post',
    data: {
      items: items.map(({uid, type}) => ({
        created: new Date().toISOString(),
        usageType,
        itemId: uid,
        itemType: type
      }))
    },
    headers
  },
  customAxiosInstance: axiosInstanceV2
});

export enum AnalyticsErrorCode {
  player = 0,
  server = 1
}
export interface AnalyticsError {
  code: AnalyticsErrorCode;
  message: string;
}

export interface AnalyticsEventDto {
  eventType: AnalyticEventType;
  item: AudioItem | VideoItem;
  playbackSessionId: string;
  playbackState: {
    currentTime: number;
    duration: number;
    playingTime: number;
  };
  error?: AnalyticsError;
}

const MS_TO_SECOND = 1000;

export const buildPlayerEventMeta = ({
  eventType,
  item,
  playbackState,
  error,
  playbackSessionId
}: AnalyticsEventDto): IAnalyticsActionMeta => ({
  analytics: {
    url: `${(endPointConfig as IConfig).API_IFE_BASE_URL}/stats/player/events`,
    method: 'post',
    data: {
      items: [
        {
          created: new Date().toISOString(),
          eventType,
          itemId: item.uid,
          itemType: item.type,
          error,
          playbackSessionId,
          playbackState: {
            duration: playbackState.duration,
            position: playbackState.currentTime,
            playingTime: playbackState.playingTime / MS_TO_SECOND
          }
        }
      ]
    },
    headers
  },
  customAxiosInstance: axiosInstanceV2
});
