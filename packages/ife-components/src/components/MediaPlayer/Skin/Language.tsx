import * as React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import styled from 'styled-components';
import {LanguageProps, LanguageState} from './types';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';

const StyledMenu = styled(Menu)`
  .selected {
    ${renderThemeProperty('menusSelectedBackgroundColor', 'background')};
    ${renderThemeProperty('menusSelectedTextColor', 'color', true)};
  }
`;

export default class Language extends React.Component<LanguageProps, LanguageState> {
  constructor(props: LanguageProps) {
    super(props);
    this.state = {
      anchorEl: null
    };
  }

  shouldComponentUpdate(nextProps: LanguageProps, nextState: LanguageState) {
    const {selectedLang, list} = this.props;
    const {anchorEl} = this.state;

    const lists_equal = list.length == nextProps.list.length && list.every((elem, i) => elem === nextProps.list[i]);

    return selectedLang !== nextProps.selectedLang || anchorEl !== nextState.anchorEl || !lists_equal;
  }

  render() {
    const {
      list,
      icon,
      backgroundColor,
      textColor,
      textSize,
      textPadding,
      selectedLang,
      onSelection,
      videoId
    } = this.props;
    return (
      <div className={`lang-button ${list.length > 1 ? 'show' : ''}`}>
        <div onClick={this.handleClick} children={icon} />
        <StyledMenu
          PaperProps={{style: {backgroundColor}}}
          MenuListProps={{style: {padding: 0}}}
          anchorEl={this.state.anchorEl as any}
          open={Boolean(this.state.anchorEl)}
          onClose={this.handleClose}
        >
          {list.map((lang, index) => {
            const onClick = () => {
              onSelection({uid: videoId, lang});
              this.setState({anchorEl: null});
            };
            return (
              <MenuItem
                key={index}
                style={{
                  color: textColor,
                  fontSize: textSize,
                  padding: textPadding,
                  fontFamily: 'inherit'
                }}
                selected={lang === selectedLang}
                onClick={onClick}
                children={lang.toUpperCase()}
                classes={{selected: 'selected'}}
              />
            );
          })}
        </StyledMenu>
      </div>
    );
  }

  private handleClick = (event: any) => this.setState({anchorEl: event.currentTarget});
  private handleClose = () => this.setState({anchorEl: null});
}
