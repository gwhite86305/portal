import * as React from 'react';
import Carousel from '../common/CarouselView/Carousel';
import {THUMBNAILS_SIZE, ImageVariantsType} from '../types';
import {MoviesProps} from './types';
import * as defaultProps from './defaultProps.json';
import IterableListRenderer from '../common/IterableRenderer';

const {HEIGHT, WIDTH} = THUMBNAILS_SIZE.VIDEO;

const imgPlaceholder = 'http://via.placeholder.com/' + HEIGHT + 'x' + WIDTH;

const Movies: React.FC<MoviesProps> = ({onItemInViewPort, onClick, items}) => (
  <IterableListRenderer>
    {items.map(({items, category, uid}) => (
      <Carousel
        key={uid}
        onItemInViewPort={onItemInViewPort}
        imgPlaceholder={imgPlaceholder}
        thumbSize={THUMBNAILS_SIZE.VIDEO}
        categoryName={category}
        items={items}
        onClick={onClick}
        styleProps={{thumbnailHeight: HEIGHT}}
        imageVariant={ImageVariantsType.posterthumb}
      />
    ))}
  </IterableListRenderer>
);

Movies.defaultProps = defaultProps as any;

export default Movies;
