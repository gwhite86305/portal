import {TextField} from '@material-ui/core';
import * as React from 'react';
import {SeatSelectionProps} from './types';

const SeatSelection: React.FC<SeatSelectionProps> = ({error, dirty, value, onChange}) => (
  <TextField
    className={`seat-no-input${error && dirty ? ' error' : ''}`}
    name="seat"
    type="text"
    label="Seat *"
    value={value}
    onChange={onChange}
    error={error && dirty}
    helperText={error && dirty ? 'Incorrect seat number' : 'Please type your seat number'}
  />
);

export default SeatSelection;
