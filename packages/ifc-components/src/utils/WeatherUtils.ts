import Device from '../types/Device';
import Profile from '../types/Profile';

const _cityNameFilter = (airport: string, cityName: string) => {
  const airportsWithState = ['KJFK', 'KLGA', 'MROC', 'TJSJ'];
  if (airportsWithState.indexOf(airport) > -1) {
    return cityName;
  }
  return cityName.split(',')[0].trim();
};

export const getCitiesWeatherConditions = (device: any, profile: any) => {
  let currentConditions = profile && profile.currentConditions;
  const flightDestination = device && device.flightDestination;
  if (currentConditions && currentConditions.length) {
    currentConditions.forEach((item: any, i: number) => {
      currentConditions[i].cityName = _cityNameFilter(item.airport, item.cityName);
    });
    // remove duplicate citys
    currentConditions = currentConditions.sort((cond1: any, cond2: any) => {
      if (cond1.cityName < cond2.cityName) {
        return -1;
      }
      if (cond1.cityName > cond2.cityName) {
        return 1;
      }
      if (cond1.airport === flightDestination) {
        // giving higher returning value if the destination
        // matches the airport.
        return -0.5;
      }
      if (cond2.airport === flightDestination) {
        // giving higher returning value if the destination
        // matches the airport.
        return 0.5;
      }
      return 0;
    });
    currentConditions = currentConditions.filter(
      (obj: any, pos: number, arr: any) => arr.map((mapObj: any) => mapObj.cityName).indexOf(obj.cityName) === pos
    );
  }
  return currentConditions;
};

export const getAirportForeCast = (device: any, profile: any, airport: string) => {
  let destinationForecasts = [];
  if (profile && device && profile.dailyForecasts && profile.dailyForecasts.length) {
    destinationForecasts = profile.dailyForecasts.filter((df: any) => df.airport === airport);
    if (destinationForecasts && destinationForecasts.length) {
      destinationForecasts = destinationForecasts[0].dailyForecast;
      let epochYesterday = new Date(Date.now() - 24 * 60 * 60 * 1000).getTime() / 1000;
      destinationForecasts = destinationForecasts
        .filter((fc: any) => epochYesterday < fc.epochDate - 7 * 3600)
        .slice(0, 4);
    }
  }
  return destinationForecasts;
};
