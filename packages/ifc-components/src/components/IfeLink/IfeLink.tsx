import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {IfeLink as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {NavLink} from 'react-router-dom';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper, withKeys, isUrlValid} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  isAccessible: boolean;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('textColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  div.links-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    ${(props: any) =>
  applyMediaQueriesForStyle(
    'padding-top',
    {xs: `calc(${props.theme.gutter}*.625)`, md: `calc(${props.theme.gutter}*.78)`},
    props.theme
  )};
    ${(props: any) =>
  applyMediaQueriesForStyle(
    'padding-bottom',
    {xs: `calc(${props.theme.gutter}*.625)`, md: `calc(${props.theme.gutter}*.78)`},
    props.theme
  )};
    a {
      display: flex;
      flex-basis: 0;
      flex-grow: 1;
      align-items: center;
      border-right-width: 1px;
      border-right-style: solid;
      text-decoration: none;
      ${renderThemeProperty('dividerColor', 'border-right-color')};
      ${(props: any) =>
  applyMediaQueriesForStyle(
    'padding-top',
    {xs: `calc(${props.theme.gutter}*.0625)`, md: `calc(${props.theme.gutter}*.312)`},
    props.theme
  )};
      ${(props: any) =>
  applyMediaQueriesForStyle(
    'padding-bottom',
    {xs: `calc(${props.theme.gutter}*.0625)`, md: `calc(${props.theme.gutter}*.312)`},
    props.theme
  )};
      flex-direction: column;
      div.link-img-title {
        display: flex;
        align-items: center;
        ${renderThemeProperty('textColor', 'color')};
        img {
          ${(props: any) =>
  applyMediaQueriesForStyle(
    'height',
    {xs: `calc(${props.theme.gutter}*1.56)`, md: `calc(${props.theme.gutter}*2.5)`},
    props.theme
  )};
          ${(props: any) =>
  applyMediaQueriesForStyle(
    'width',
    {xs: `calc(${props.theme.gutter}*1.56)`, md: `calc(${props.theme.gutter}*2.5)`},
    props.theme
  )};
        }
        span {
          ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'none', md: 'inherit'}, props.theme)};
          margin-left: calc(${(props: any) => props.theme.gutter}*1.25);
          font-size: ${(props: any) => props.theme.secondaryFontSize};
        }
      }
    }
    a:focus,
    a:hover {
      span {
        color: ${(props: any) => props.theme.secondaryTextColor};
      }
    }
    a:last-child {
      border-right: none;
    }
  }
`;

class IfeLink extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  private _getMenuItem(menuItem: any) {
    return (
      <div className="link-img-title">
        {menuItem.iconUrl ? (
          <img
            src={menuItem.iconUrl}
            alt={menuItem.iconAltText}
            style={menuItem.iconHeight ? {height: menuItem.iconHeight} : {}}
          />
        ) : null}
        <span>{menuItem.label}</span>
      </div>
    );
  }

  public render() {
    const {isAccessible} = this.props;
    const {localizedMenuItems} = this.props;

    return isAccessible ? (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-ife-link-component">
          <div className="links-wrapper">
            {localizedMenuItems
              ? withKeys(
                localizedMenuItems.map(
                  (menuItem: any, index: number) =>
                    isUrlValid(menuItem.url) ? (
                      <a key={index} target={'_blank'} href={menuItem.url}>
                        {this._getMenuItem(menuItem)}
                      </a>
                    ) : (
                      <NavLink to={`/${menuItem.url}`}>{this._getMenuItem(menuItem)}</NavLink>
                    )
                )
              )
              : null}
          </div>
        </Container>
      </PortalComponent>
    ) : null;
  }
}

export default IfeLink;
