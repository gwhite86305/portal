import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {AppleMusic as ExternalInterface} from '../common/ComponentsInterface';
import AppleMusic, {InternalInterface} from './AppleMusic';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import {pathOr} from 'ramda';
import {getTierByNpiCode} from '../../utils/ConnectionUtils';
import {_signupFreeCall} from '../../store/WifiServiceOptions/actions';
import {getFreeTier} from '../../utils/ConnectionUtils';
import {getPremiumTiers, getPremiumState, getMessagingTier} from '../../utils/ConnectionUtils';

import {
    signupWithMessaging
} from '../../store/WifiServiceOptions/actions';  

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  signupFree(tierId: number): any;
  signupWithMessaging(tierId: number): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const messagingTiers = getMessagingTier(state.profile);
    const premiumTiers = getPremiumTiers(state.profile);
    const connected = getPremiumState(state.device, state.profile);
    const WifiServiceOptions = state.WifiServiceOptions;

    let props = {...ownProps, redirect: ''};
    if (state.profile && state.device && state.WifiServiceOptions && state.userAgreement) {
        let tier = getTierByNpiCode(state.profile, ownProps.feature.npiCode);
        props.redirect = pathOr('', ['metadata', 'redirectUri'], tier);

      const freeTier = getFreeTier(state.profile);
      let serviceTierId = freeTier ? freeTier.id : null;
      props = {
        serviceTierId: serviceTierId,
        wifiServiceOptions: state.WifiServiceOptions,
        device: state.device,
        userAgreement: state.userAgreement
      };
    }
    return props;
  },
  (dispatch: any, ownProps: PageProps, state: any): DispatchProps => {
    return {
      signupWithMessaging: tierId => {
           dispatch(signupWithMessaging(tierId));
      },
      signupFree: tierId => {
        dispatch(_signupFreeCall(tierId));
      }
    };
  }
)

class AppleMusicContainer extends React.Component<PageProps, any> {
    constructor(props: any) {
        super(props);
        // To avoid multiple signup request
        this.state = {
          signupRequestMade: false
        };
    }
    
  public render() {
    let onButtonClick;
    // If the user already have freeTier, send the user to the openPageOnClick url
    if (this.props.serviceTierId != this.props.device.serviceTierId) {
      onButtonClick = () => {
        this.props.signupFree(this.props.serviceTierId);
      };
    }
    return <AppleMusic buttonClickHook={onButtonClick} {...this.props} />;
  }

  public componentWillMount() {
   // if (!this.props.WifiServiceOptions.selected) {
   //   this.props.history.push('/');
   // }
  }
  
  componentWillReceiveProps(newProps: any) {
    const {WifiServiceOptions} = this.props;
    
    if (
      WifiServiceOptions &&
      WifiServiceOptions.selected &&
      WifiServiceOptions.selected.method === 'messaging' &&
      WifiServiceOptions.signup &&
      WifiServiceOptions.signup.action === 'accept'
    ) {
      this.props.history.push('/' + this.props.feature);
    }
    if ((!this.props.connected && newProps.connected) || (this.state.signupRequestMade && newProps.connected)) {
      this.props.history.push('/' + this.props.feature);
    }
    if (
      WifiServiceOptions &&
      WifiServiceOptions.selected &&
      WifiServiceOptions.selected.method == 'purchase' &&
      WifiServiceOptions.signup &&
      WifiServiceOptions.signup.action == 'redirect'
    ) {
      window.location.href = WifiServiceOptions.signup.url;
    }
  }

}

export default withRouter(
  withLocaleProps(AppleMusicContainer, [
    {altTextForImage: 'ifc.AppleMusic.image.altText'},
    {subHeaderText: 'ifc.AppleMusic.subHeader.text'},
    {headerText: 'ifc.AppleMusic.header.text'},
    {buttonText: 'ifc.AppleMusic.button.text'}
  ])
) as any;
