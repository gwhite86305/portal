import {always, contains, find, ifElse, path, pathOr, pipe, prop, replace, slice, split} from 'ramda';
import {IRootState} from '../../..';
import {AnalyticEventType, EventPayload, reportEvent} from '../../../store/analytics/actions';
import {setCurrentAudioTime} from '../../../store/audio/actions';
import {
  clearPlayerInfo,
  getDrmServers,
  getVideoPlayback,
  seekToRange,
  setAudioLanguages,
  setBuffering,
  setCurrentItem,
  setFullscreen,
  setPlayerAsPaused,
  setPlayerAsPlay,
  setPlayerAsPlaying,
  setSubtitles,
  updateVolume,
  setErrorMovie
} from '../../../store/mediaplayer/actions.player';
import {playNextInPlaylist} from '../../../store/mediaplayer/actions.playlist';
import {selectCurrentVideoUid} from '../../../store/mediaplayer/selector';
import {
  getPreRollAds,
  getVideoById,
  loadVideosConfig,
  setAudioLang,
  setSubtitle,
  updateVideoConfig
} from '../../../store/videos/actions';
import {
  selectTrailerVideoUrl,
  selectVideoAudioLanguage,
  selectVideoSubtitleLanguage,
  selectVideoWatchTime
} from '../../../store/videos/selectors';
import {PrerollAd, VideosConfigItem} from '../../../store/videos/types';
import {MediaContainerProps} from '../types';
import {isIOS} from './playerCommon';

export const mapStoreToProps = (state: IRootState): Partial<MediaContainerProps> => ({
  isPausedForNotification: path(['ifeNotificationDispatcher', 'messages', 'value'], state) || false,
  preRollAds: state.ifeVideos.preRollAds,
  // TODO: remove state spreading here.. instead use selectors to aquire only whats needed
  ...state.ifeMediaPlayer,
  duration: 0,
  currentTime: selectVideoWatchTime(state),
  audioLanguage: selectVideoAudioLanguage(state),
  subtitleLanguage: selectVideoSubtitleLanguage(state),
  trailerVideoUrl: selectTrailerVideoUrl(state),
  videoUid: selectCurrentVideoUid(state)
});

// TODO return type can't be a partial. everything in partial in optional. we needs to have a list of required props.
export const mapDispatchToProps = (dispatch: any): Partial<MediaContainerProps> => ({
  clearPlayerInfo: () => dispatch(clearPlayerInfo()),
  getDrmServers: playbackSessionId => dispatch(getDrmServers(playbackSessionId)),
  getPreRollAds: videoId => dispatch(getPreRollAds(videoId)),
  getVideoById: uid => dispatch(getVideoById(uid)),
  getVideoPlayback: (videoUid, playbackSessionId) => dispatch(getVideoPlayback(videoUid, playbackSessionId)),
  playNextInPlaylist: playerPage => dispatch(playNextInPlaylist(playerPage)),
  onSeekToRange: value => dispatch(seekToRange(value)),
  setAudioLanguages: langs => dispatch(setAudioLanguages(langs)),
  setBuffering: isBuffering => dispatch(setBuffering(isBuffering)),
  setCurrentItem: item => dispatch(setCurrentItem(item)),
  setError: (error, playbackSessionId) => dispatch(setErrorMovie(error, playbackSessionId)),
  setFullscreen: value => dispatch(setFullscreen(value)),
  setPlayerAsPaused: () => dispatch(setPlayerAsPaused()),
  setPlayerAsPlay: () => dispatch(setPlayerAsPlay()),
  setPlayerAsPlaying: () => dispatch(setPlayerAsPlaying()),
  setSubtitles: langs => dispatch(setSubtitles(langs)),
  updateVolume: value => dispatch(updateVolume(value)),
  setCurrentAudioTime: (uid, currentTime, duration) => dispatch(setCurrentAudioTime(uid, currentTime, duration)),
  setSubtitle: (uid, lang) => dispatch(setSubtitle(uid, lang)),
  setAudioLang: (uid, lang) => dispatch(setAudioLang(uid, lang)),
  updateVideoConfig: (videoConfig: VideosConfigItem) => dispatch(updateVideoConfig(videoConfig)),
  loadVideosConfig: () => dispatch(loadVideosConfig()),
  reportEvent: (event: AnalyticEventType, payload: EventPayload) => dispatch(reportEvent(event, payload))
});

export const getHtmlBundleParams = (url: string): {videoId: string} => {
  const getHtmlBundleParamValue = (paramName: string, urlField: string): any =>
    pipe(
      replace('?', ''),
      split('&'),
      ifElse(find(contains(paramName)) as any, find(contains(paramName)), always('')),
      slice(paramName.length, Infinity)
    )(urlField);
  const VIDEO_URL_PARAM_NAME = 'videoId=';
  const videoId = getHtmlBundleParamValue(VIDEO_URL_PARAM_NAME, url);
  return {videoId};
};

export const DEFAULT_VIDEO_ID = '5a86f3a6e4b0b8f3530497ca';

export const initialState = {adsPlaylist: undefined, currentAdIndex: -1, videoId: '', isTrailer: false};

export const isNewPlaylistGoToFirstAd = (current: any, past: any, index: number) =>
  pathOr(-1, ['length'], current) !== pathOr(-1, ['length'], past) && prop('length', current) > 0 && index === -1;

export const isFullScreenForbidden = (ad = {} as PrerollAd) => isIOS && (ad.skippableAfter || ad.blockFastForward);
