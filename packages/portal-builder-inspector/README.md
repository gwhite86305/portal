# Portal Builder Inspector 🔍

### SYNOPSIS:

```bash
npm run portal-builder-inspector -- [-clstv] [ComponentName]
```

### DESCRIPTION:

Portal Builder Inspector helps you to **run Component e2e tests** into Portal Builder and **preview of the Component and its properties**.

The following options are available:

| Option              | Description                                                 |
| :------------------ | :---------------------------------------------------------- |
| -c, --componentName | Target Component                                            |
| --help              | Manual                                                      |
| -l, --list          | List of Available Components in the Package                 |
| -s, --stack         | Run portal builder stack (API PB, SCHEMA PB, PB WEB, P WEB) |
| -t, --test          | Run e2e Tests                                               |
| -v, --version       | Output CLI Version Number                                   |

### REQUIREMENTS:

* Install package into package.json library

```json
{
  "script": {"portal-builder-inspector": "portal-builder-inspector"},
  "devDependencies": {"portal-builder-inspector": "<version>"}
}
```

* Component implementation need to be placed into `./src/components/${COMPONENT_NAME}` folder
* Component e2e tests need to need to be placed into `./src/components/${COMPONENT_NAME}/__tests__`folder
* Add ComponentName to e2e test file (To Avoid file overridden, TODO: implement this into script)
* Adopt portal-builder-inspector e2e tools for integration tests example:

```js
const e2eUtils = require('portal-builder-inspector');

describe('Horizontal Menu', () => {
  let page;
  beforeAll(async () => {
    page = await e2eUtils.login();
  });

  it('should pass my test', async () => {
    expect(await page.evaluate(() => document.body.textContent)).toContain('Expected');
  });

  afterAll(async function() {
    await e2eUtils.logoutAndClose(page);
  });
});
```

### INSPECTOR FLOW:

![Flow](./static/images/PB_FLOW.png)

### ACTIONS PREVIEW:

* list
  ![list](./static/images/LIST.png)
* PREVIEW
  ![list](./static/images/PREVIEW.png)
* TEST
  ![list](./static/images/TEST.png)

### For Running the test in each component library please follow these steps

## Inital Run

# Run "lerna clean"

# Run "lerna bootstrap"

# Run "npm run build"

# Run "cd `Component library Name` eg. cd core-components"

# Run "chmod +x /Users/`username`/Documents/portal/packages/core-components/node_modules/.bin/portal-builder-inspector"

## Recurring steps

# Run "cd `component library name` eg. cd ifc-components"

# Run "npm run portal-builder-inspector -- -t"

# See the tests run and enjoy
