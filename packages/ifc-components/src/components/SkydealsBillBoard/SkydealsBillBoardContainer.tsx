import * as React from 'react';
import {utils} from 'component-utils';
import {SkydealsBillBoard as ExternalInterface} from '../common/ComponentsInterface';
import SkydealsBillBoard from './SkydealsBillBoard';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

const connectWrapper = utils.connectWrapper.default;
export type PageProps = ExternalInterface & {actions: any};

@connectWrapper((state: any, ownProps: any) => {
  let enrichedURL = ownProps.feature.baseURL;
  let param = '';
  let deviceKeys = ['flightNumber'];
  let propKeys = ['flightAirline', 'cieCode'];
  let snakeKey = '';

  if (state.device) {
    deviceKeys.forEach((key, index) => {
      if (state.device[key]) {
        // convert to snake case
        snakeKey = key
          .split(/(?=[A-Z])/)
          .join('_')
          .toLowerCase();
        param = `${param}${snakeKey}=${state.device[key]}&`;
      }
    });
  }
  if (state.profile.locations) {
    param = `${param}flight_origin=${state.profile.locations.originCode}&flight_destination=${state.profile.locations.destinationCode}&`;
  }

  propKeys.forEach((key, index) => {
    if (ownProps.feature[key]) {
      // convert to snake case
      snakeKey = key
        .split(/(?=[A-Z])/)
        .join('_')
        .toLowerCase();
      param = `${param}${snakeKey}=${ownProps.feature[key]}`;
      if (index != propKeys.length - 1) {
        param = `${param}&`;
      }
    }
  });
  enrichedURL = `${enrichedURL}?${param}`;

  ownProps.feature.openPageOnClick = enrichedURL;
  return enrichedURL;
})
class SkydealsBillBoardContainer extends React.Component<PageProps, any> {
  public render() {
    return <SkydealsBillBoard {...this.props} />;
  }
}

export default withLocaleProps(SkydealsBillBoardContainer, [
  {altTextForImage: 'ifc.SkydealsBillBoard.image.altText'},
  {subHeaderText: 'ifc.SkydealsBillBoard.subHeader.text'},
  {headerText: 'ifc.SkydealsBillBoard.header.text'},
  {buttonText: 'ifc.SkydealsBillBoard.button.text'}
]) as any;
