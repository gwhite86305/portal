import * as React from 'react';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {Checkout as ExternalInterface} from '../common/ComponentsInterface';
import {Button, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  message?: string;
  okButtonText?: string;
  backToHomeButtonText?: string;
  onBackToHomeButtonClick?: any;
  onOkButtonClick?: any;
  hideSwapDeviceModal?: any;
  connected?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  background-color: transparent;
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  display: flex;
  flex-direction: column;
  width: 100%;
  position: fixed;
  ${(props: any) => applyMediaQueriesForStyle('justify-content', props.theme.componentTheme.modalJustify, props.theme)};
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;

  div.overlay {
    background-color: ${(props: any) => props.theme.secondaryBackgroundColor};
    opacity: 0.9;
    width: 100%;
    align-items: center;
    justify-content: center;
    display: flex;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 2000;
    overflow: scroll;
  }
  .modal {
    z-index: 2001;
    border-radius: ${(props: any) => props.theme.modalRadius};
  }
  form.service-unavailable-modal-form {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalPadding, props.theme)};
    margin: 0 auto;
    ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.modalWidth, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('position', props.theme.componentTheme.modalPosition, props.theme)};
    left: 0;
    right: 0;
    width: 100%;
    h1.message-text {
      margin: 0px;
      ${renderThemeProperty('messageTextColor', 'color')};
      ${renderThemeProperty('messageTextFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.187;
      a {
        color: ${(props: any) => props.theme.secondaryTextColor};
      }
    }

    div.button-wrap {
      display: flex;
      justify-content: space-between;
      align-items: center;
      ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row-reverse'}, props.theme)};
      button {
        ${(props: any) =>
          applyMediaQueriesForStyle('margin-top', {xs: `calc(${props.theme.gutter}*1.25)`}, props.theme)};
        margin: 0px;
        ${(props: any) => applyMediaQueriesForStyle('width', {xs: '160px', md: '160px'}, props.theme)};
        width: 160px;
        height: 41px;
        white-space: nowrap;
        justify-content: center;
        font-size: ${(props: any) => props.theme.primaryFontSize};
      }
      button.back-button {
        border: 1px solid ${(props: any) => props.theme.primaryButtonColor};
      }
    }
  }
`;

const CenterIcon = (props: any) => {
  const Container = styled.div`
    .center-container {
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.centerIconMargin, props.theme)};
    }
    .center-icon {
      display: block;
      margin-left: auto;
      margin-right: auto;
      ${props => applyMediaQueriesForStyle('width', props.theme.componentTheme.centerIconWidth, props.theme)};
      ${props => applyMediaQueriesForStyle('height', props.theme.componentTheme.centerIconHeight, props.theme)};
    }
  `;
  const {feature} = props;
  return (
    <Container>
      {feature && feature.centerIconPath && feature.showCenterIcon ? (
        <div className="center-container">
          <img
            src={feature.centerIconPath}
            alt={feature.centerIconAltText ? feature.centerIconAltText : ' '}
            className="center-icon"
          />
        </div>
      ) : null}
    </Container>
  );
};

class ServiceUnavailableModal extends React.Component<InternalInterface & ExternalInterface, {show: boolean}> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults
  };

  constructor(props: any) {
    super(props);
    this.state = {show: false};
    this._handleBackToHomeClick = this._handleBackToHomeClick.bind(this);
    this._handleOkButtonClick = this._handleOkButtonClick.bind(this);
  }

  _handleBackToHomeClick(event: any) {
    event.preventDefault();
    this.setState({show: false});
    this.props.onBackToHomeButtonClick();
  }

  _handleOkButtonClick(event: any) {
    this.setState({show: false});
    event.preventDefault();
  }

  _handleShowState(prevProps: any, prevState: any) {
    if (
      (prevProps.connected === undefined && this.props.connected === false) ||
      (prevProps.connected && !this.props.connected) ||
      (!this.props.connected && this.state.show === false)
    ) {
      this.setState({show: true});
    }
    if (this.props.connected && this.state.show != false) {
      this.setState({show: false});
    }
    // To cover a scenario when user is on device swap form and the connectivity went down.
    if (!prevState.show && this.state.show) {
      this.props.hideSwapDeviceModal();
    }
  }

  public componentDidMount() {
    this._handleShowState({connected: undefined}, {show: false});
  }

  public componentDidUpdate(prevProps: any, prevState: any) {
    this._handleShowState(prevProps, prevState);
  }

  public render() {
    const {message, okButtonText, backToHomeButtonText} = this.props;
    return this.state.show ? (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-service-unavailable-modal-component">
          <div className="overlay" />
          <form className="service-unavailable-modal-form modal" onSubmit={this._handleBackToHomeClick}>
            <CenterIcon {...this.props} />
            <div className="header">
              {message ? <h2 className="message-text" dangerouslySetInnerHTML={sanitize(message)} /> : null}
            </div>

            {this.props.feature.showButtons ? (
              <div className="button-wrap">
                <Button isPrimary={true} onClick={this._handleOkButtonClick} aria-label={okButtonText}>
                  {okButtonText}
                </Button>
                <Button
                  className="back-button"
                  isPrimary={false}
                  onClick={this._handleBackToHomeClick}
                  aria-label={backToHomeButtonText}
                >
                  {backToHomeButtonText}
                </Button>
              </div>
            ) : null}
          </form>
        </Container>
      </PortalComponent>
    ) : null;
  }
}

export default ServiceUnavailableModal;
