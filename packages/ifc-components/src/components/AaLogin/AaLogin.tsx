import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Button, TextBox} from 'core-components';
import * as defaults from './defaultProps.json';
import {path} from 'ramda';
import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  WifiServiceOptions?: any;
  Authentication?: any;
  onAALoginSubmit?: any;
  onGuestLoginSubmit?: any;
  componentTheme?: any;
  feature?: any;
  header?: string;
  headerForPurchasePath?: string;
  note?: string;
  userNameInputLabel?: string;
  lastNameInputLabel?: string;
  passwordInputLabel?: string;
  rememberMeCheckBoxLabel?: string;
  forgotPasswordLinkText?: string;
  loginButtonText?: string;
  guestLoginButtonText?: string;
  errorDict?: any;
  purchasePath?: boolean;
  successCode?: number;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('primaryTextColor', 'color')};
  font-family: ${(props: any) => props.theme.primaryFontFamily};
  div.header {
    ${renderThemeProperty('secondaryTextColor', 'color')};
    font-family: ${(props: any) => props.theme.appFontFamily};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.875);
    padding-bottom: ${(props: any) => props.theme.gutter};
  }
  div.note {
    ${renderThemeProperty('primaryTextColor', 'color')};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.75);
    font-family: ${(props: any) => props.theme.appFontFamily};
  }
  .validation-summrary {
    display: flex;
    margin-bottom: ${(props: any) => props.theme.gutter};
    overflow: hidden;
    -webkit-backdrop-filter: blur(10px);
    backdrop-filter: blur(10px);
    box-shadow: 0 2px 4px 0 rgba(54, 73, 90, 0.2);

    border-radius: ${(props: any) => props.theme.textBoxBorderRadius};
    color: ${(props: any) => props.theme.textBoxTextColor};
    font-family: ${(props: any) => props.theme.appFontFamily};
    width: 100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    height: calc(${(props: any) => props.theme.gutter} * 3.562);
    font-size: ${(props: any) => props.theme.primaryFontSize};
    div:first-child {
      background-color: ${(props: any) => props.theme.formErrorColor};
      width: calc(${(props: any) => props.theme.gutter} * 3.562);
      align-items: center;
      justify-content: center;
    }
    div {
      width: calc(100% - ${(props: any) => props.theme.gutter} * 3.562);
      display: flex;
    }
    p {
      margin: auto calc(${(props: any) => props.theme.gutter} * 0.75);
    }
  }
  .fa-check {
    width: 24px;
    height: 24px;
    margin-right: 10px;
    border-style: solid;
    border-width: 1px;
    border-radius: 4px;
    ${renderThemeProperty('checkboxBorderColor', 'border-color')};
    background: ${(props: any) => props.theme.primaryButtonColor};
    cursor: pointer;
    :before {
      position: relative;
      top: 3px;
      padding-left: calc(${(props: any) => props.theme.gutter} * 0.3);
      border-color: ${(props: any) => props.theme.primaryBackgroundColor};
      color: white;
    }
  }
  .unchecked {
    background-color: white;
    :before {
      ${renderThemeProperty('backgroundColor', 'color')};
      color: transparent;
    }
  }
  .remember-forgot-section {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-top: 3px;
    .remember {
      display: flex;
      align-items: center;
      ${renderThemeProperty('primaryTextColor', 'color')};
      cursor: pointer;
    }
    a {
      ${renderThemeProperty('forgotPasswordLinkColor', 'color')};
      text-decoration: none;
    }
    a::after {
      background: url('./mocks/images/forgot_password_icon.svg') no-repeat;
      width: ${(props: any) => props.theme.primaryFontSize};
      height: ${(props: any) => props.theme.primaryFontSize};
      display: inline-block;
      content: '.';
      margin-left: 6px;
    }
  }
  div.button-wrap {
    margin-top: calc(${(props: any) => props.theme.gutter} * 1);
    text-align: center;
    button {
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
  div.hidden {
    visibility: hidden;
  }
  div.continue-as-guest {
    text-align: center;
    button {
      border: 1px solid ${(props: any) => props.theme.secondaryButtonTextColor};
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
`;

class AaLogin extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      formErrors: '',
      userName: '',
      lastName: '',
      password: '',
      remember: false
    };
    this._handleAALoginSubmit = this._handleAALoginSubmit.bind(this);
    this._handleFormChange = this._handleFormChange.bind(this);
    this._handleGuestLogin = this._handleGuestLogin.bind(this);
  }

  toggleRemember = (e: any) => {
    this.setState((prevState: any) => ({remember: !prevState.remember}));
  };

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handleAALoginSubmit(event: any) {
    event.preventDefault();
    const {userName, lastName, password} = this.state;
    const {errorDict} = this.props;
    let formErrors: any = {};
    if (!userName) {
      formErrors['userName'] = errorDict['errorMessageUserNameRequired'];
    }
    if (!lastName) {
      formErrors['lastName'] = errorDict['errorMessageLastNameRequired'];
    }
    if (!password) {
      formErrors['password'] = errorDict['errorMessagePasswordRequired'];
    }
    this.setState({formErrors});
    if (Object.keys(formErrors).length === 0) {
      this.props.onAALoginSubmit(this.props.feature.authUrl, userName, lastName, password);
    }
  }
  _handleGuestLogin(event: any) {
    event.preventDefault();
    this.props.onGuestLoginSubmit();
  }
  public render() {
    const {
      Authentication,
      WifiServiceOptions,
      feature,
      header,
      headerForPurchasePath,
      userNameInputLabel,
      lastNameInputLabel,
      passwordInputLabel,
      rememberMeCheckBoxLabel,
      forgotPasswordLinkText,
      loginButtonText,
      guestLoginButtonText,
      errorDict,
      purchasePath,
      successCode
    } = this.props;
    const {formErrors} = this.state;
    const headerText = purchasePath ? headerForPurchasePath : header;

    let errorSummary = '';
    if (Authentication.aaAuth && Authentication.aaAuth.code && Authentication.aaAuth.code !== successCode) {
      errorSummary = errorDict[Authentication.aaAuth.code] || errorDict['generic'];
    }
    if (path(['aaAuth', 'error'], Authentication)) {
      errorSummary = errorDict['generic'];
    }
    if (Object.keys(formErrors).length) {
      errorSummary = errorDict['errorMessageOnFormHeader'];
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="aadvantage-login-form">
          <form onSubmit={this._handleAALoginSubmit}>
            <div className="header">{headerText ? <span className="header-text">{headerText}</span> : null}</div>
            {errorSummary ? (
              <div className="validation-summrary">
                <div>
                  <img src="./mocks/images/form_error.svg" />
                </div>
                <div>
                  <p>{errorSummary}</p>
                </div>
              </div>
            ) : null}
            <TextBox
              name="userName"
              error={formErrors.userName}
              placeholder={userNameInputLabel}
              onChange={this._handleFormChange}
            />
            <TextBox
              name="lastName"
              error={formErrors.lastName}
              placeholder={lastNameInputLabel}
              onChange={this._handleFormChange}
            />
            <TextBox
              name="password"
              type="password"
              error={formErrors.password}
              placeholder={passwordInputLabel}
              onChange={this._handleFormChange}
            />

            <div className="remember-forgot-section">
              <div
                className="remember secondary-focus"
                tabIndex={0}
                onKeyPress={this.toggleRemember}
                onClick={this.toggleRemember}
              >
                <i className={this.state.remember ? 'fa fa-check' : 'fa fa-check unchecked'} />
                <span>{rememberMeCheckBoxLabel}</span>
              </div>
              {feature.includeForgotPasswordLink ? (
                <a href={feature.forgotPasswordUrl}>{forgotPasswordLinkText}</a>
              ) : null}
            </div>

            <div className="button-wrap">
              <Button isPrimary={true} onClick={this._handleAALoginSubmit} aria-label={loginButtonText}>
                {loginButtonText}
              </Button>
            </div>
          </form>
          <div className={'button-wrap ' + (purchasePath ? 'continue-as-guest' : 'hidden')}>
            <Button isPrimary={false} onClick={this._handleGuestLogin} aria-label={guestLoginButtonText}>
              {guestLoginButtonText}
            </Button>
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default AaLogin;
