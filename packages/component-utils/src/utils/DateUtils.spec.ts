import {convertMinutesToClock} from './DateUtils';
test('500 minutes equal 08 hr 20 mn', () => {
  expect(convertMinutesToClock(500)).toEqual({hour: '08', minute: '20'});
});
test('60 minutes equal 01 hr 00 mn', () => {
  expect(convertMinutesToClock(60)).toEqual({hour: '01', minute: '00'});
});

test('0 minute equal --- hr --- mn', () => {
  expect(convertMinutesToClock(0)).toEqual({hour: '---', minute: '---'});
});
