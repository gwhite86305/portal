# portal-renderer

ViaSat In-Flight web app

This app mounts the &lt;App/&gt; root component exposed by the _portal-components_ lib, which consumes **Layout**, **App
Theme**, and **App Menu** data from Portal UX builder api, and generates a themed UX from available components.

This app is also responsible for:

1. Initializing the component libraries and their stores
2. Dynamically loading locale files as needed (essential to do any dynamic loading/bundling from client app as not
   possible with lib)
3. Provides a webpack builder for browser consumption
4. Provides an example in its own right of how to import _portal-components_ and use to build a client app (i.e. if
   higher flexibility is required that the _vif-ux-builder_ can provide, direct import of _vif-components_ will allow
   any UX to be built)

## Requirements

* NodeJS >=5.5.1

This project has a static npm dependency on the *core-components*, *ifc-components*, and _ife-components_ libs. Note:
Static resources are currently stored in this repo for demo purposes - these will eventually be served from the ux
builder api.
