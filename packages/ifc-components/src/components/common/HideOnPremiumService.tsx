import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getPremiumState, deviceHasServiceTier} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, connected: false};
  if (state.device && state.profile) {
    props.isPremiumService = getPremiumState(state.device, state.profile) && deviceHasServiceTier(state.device);
  }
  return props;
})
export default class HideOnPremiumService extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.isPremiumService || this.props.isPremiumService === undefined ? null : this.props.children;
  }
}
