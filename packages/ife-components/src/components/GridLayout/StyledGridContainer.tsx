import {applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export interface StyledGridContainerProps {}

export default styled.div<StyledGridContainerProps>`
  width: 100%;
  display: grid;
  display: -ms-grid;
  box-sizing: border-box;
  ${({theme}) => applyMediaQueriesForStyle('grid-gap', theme.componentTheme.gridGap, theme)};
  ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.padding, theme)};
  ${({theme}) => applyMediaQueriesForStyle('grid-auto-rows', theme.componentTheme.rowHeight, theme)};
  ${({theme}) => applyMediaQueriesForStyle('-ms-grid-rows', theme.componentTheme.rowHeightMs, theme)};
  ${({theme}) => applyMediaQueriesForStyle('grid-template-columns', theme.componentTheme.horizontalFraction, theme)};
  ${({theme}) => applyMediaQueriesForStyle('-ms-grid-columns', theme.componentTheme.horizontalFraction, theme)};
  ${({theme}) => applyMediaQueriesForStyle('grid-template-columns', theme.componentTheme.horizontalFraction, theme)};
`;
