import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {FlightTimeRemaining as ExternalInterface} from '../common/ComponentsInterface';
import FlightProgressBar, {InternalInterface} from './FlightProgressBar';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {getFlightRemaining} from '../../utils/FlightStatusUtils';

@connectWrapper((state: any) => {
  if (state.device) {
    let {minutesRemaining, percentageOfFlightComplete} = getFlightRemaining(state.device, state.profile);
    return {
      ...state.device,
      flightMinutesRemaining: state.device.wowState == 'On' ? undefined : minutesRemaining,
      flightOrigin: state.profile && state.profile.locations && state.profile.locations.originCode,
      flightDestination: state.profile && state.profile.locations && state.profile.locations.destinationCode,
      percentageOfFlightComplete: percentageOfFlightComplete
    };
  }
})
class FlightProgressBarContainer extends React.Component<ExternalInterface & InternalInterface, any> {
  public render() {
    return <FlightProgressBar {...this.props} />;
  }
}

export default withLocaleProps(FlightProgressBarContainer, [
  {availableStatusLocalisation: 'ifc.flightProgressBar.available'},
  {unAvailableStatusLocalisation: 'ifc.flightProgressBar.unavailable'},
  {remainingTimeLocalisation: 'ifc.flightProgressBar.remaining'},
  {flightLocalisation: 'ifc.flightProgressBar.flight'}
]) as any;
