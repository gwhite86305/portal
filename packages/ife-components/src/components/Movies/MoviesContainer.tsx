import {PortalComponent, utils} from 'component-utils';
import {IRootState} from '../..';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import * as React from 'react';
import ScheletonLoader from '../common/CarouselView/ScheletonLoader';
import ErrorMessage from '../common/ErrorMessage';
import Movies from './Movies';
import {trackImpression} from '../../store/analytics/actions';
import {Props, StateProps, DispatchProps} from './types';
import {MovieItem} from '../../store/movies/types';
import {selectItemsByCategory} from './../../store/movies/selectors';
import {flatten} from 'lodash-es';
import {fromEvent, Subject} from 'rxjs';
import {takeUntil, debounceTime} from 'rxjs/operators';
import MoviesGrid from './MoviesGrid';

class IFEMoviesContainer extends React.Component<Props, {width: number}> {
  public static contextType = RouterContext;
  private _unsubscribe = new Subject();
  state = {
    width: window.innerWidth
  };

  private handleResize = () => this.setState({width: window.innerWidth});

  componentDidMount() {
    const {feature: {gridLayout}} = this.props;
    gridLayout &&
      fromEvent(window, 'resize')
        .pipe(debounceTime(100), takeUntil(this._unsubscribe))
        .subscribe(() => this.handleResize());
  }

  componentWillUnmount() {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }

  private renderMovies = () => {
    const {feature: {gridLayout}} = this.props;
    const {trackImpression, isLoading, items} = this.props;
    if (isLoading) {
      return <ScheletonLoader />;
    }

    return items.length ? (
      gridLayout ? (
        <MoviesGrid
          movieItems={flatten(items.map((item) => item.items))}
          width={this.state.width}
          handleClick={this.navigateToDetailPage}
        />
      ) : (
        <Movies items={items} onClick={this.navigateToDetailPage} onItemInViewPort={trackImpression} />
      )
    ) : (
      <ErrorMessage text={'No Movies Available'} />
    );
  };

  render() {
    return <PortalComponent componentTheme={this.props.componentTheme}>{this.renderMovies()}</PortalComponent>;
  }

  private navigateToDetailPage = (contentUid: string) => {
    this.context.history.push(`${this.props.feature.synopsisPage}?videoId=${contentUid}`);
  };
}

export const mapStoreToProps = (state: IRootState): StateProps => {
  return {
    items: selectItemsByCategory(state), // or selectItemsByGenre (extend in future)
    isLoading: state.ifeMovies.isLoadingItems,
  };
};

export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  trackImpression: (movie: MovieItem) => dispatch(trackImpression(movie))
});

const connectWrapper = utils.connectWrapper.default;
export default connectWrapper(mapStoreToProps, mapDispatchToProps)(IFEMoviesContainer);
