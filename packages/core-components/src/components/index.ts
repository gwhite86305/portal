// import { Image } from './../../../ifc-components/src/components/common/ComponentsInterface.d';
import {FC, ComponentClass} from 'react';
import {
  BillBoard as BillBoardExternalProps,
  HorizontalMenu as HorizontalMenuExternalProps,
  Icon as IconExternalProps,
  Button as ButtonExternalProps,
  Image as ImageExternalProps,
  Placeholder as PlaceholderExternalProps,
  SystemSnackbar as SystemSnackbarExternalProps
} from './common/ComponentsInterface';
import * as ComponentsInterface from './common/ComponentsInterface';
import BillBoard, {InternalInterface as BillBoardProps} from './BillBoard/BillBoard';
import HorizontalMenu, {InternalInterface as HorizontalMenuProps} from './HorizontalMenu/HorizontalMenu';
import Icon, {InternalInterface as IconProps} from './Icon/Icon';
import Button, {InternalInterface as ButtonProps} from './Button/Button';
import TextBox, {InternalInterface as TextBoxProps} from './TextBox/TextBox';
import Image, {InternalInterface as ImageProps} from './Image/Image';
import Placeholder, {InternalInterface as PlaceholderProps} from './Placeholder/Placeholder';
import SystemSnackbar, {InternalInterface as SystemSnackbarProps} from './SystemSnackbar/SystemSnackbar';
import Spinner from './common/Spinner';
import Accordion from './Accordion/Accordion';
import Banner from './Banner/Banner';
import BackButton from './common/BackButton';
import Poller from './Poller/Poller';
import QuickLinks from './QuickLinks/QuickLinks';
import HorizontalDivider from './HorizontalDivider/HorizontalDivider';
import SkipToContent from './SkipToContent/SkipToContent';
import Error, {Props as ErrorProps} from './common/Error';

const components = {
  BillBoard,
  HorizontalMenu,
  Icon,
  Image,
  Placeholder,
  SystemSnackbar,
  Button,
  TextBox,
  BackButton,
  Error,
  Accordion,
  Poller,
  Spinner,
  QuickLinks,
  HorizontalDivider,
  Banner,
  SkipToContent
};
export {
  components,
  BillBoard,
  HorizontalMenu,
  Icon,
  Image,
  Placeholder,
  SystemSnackbar,
  Button,
  TextBox,
  BackButton,
  Error,
  Accordion,
  Poller,
  Spinner,
  QuickLinks,
  HorizontalDivider,
  Banner,
  SkipToContent
};
