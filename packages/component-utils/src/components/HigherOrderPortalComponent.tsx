import * as React from 'react';
import PortalComponent from './PortalComponent';

const HigherOrderPortalComponent = (WrappedComponent: React.ComponentType<any>) => {
  return class HOC extends React.Component<any> {
    render() {
      const {componentTheme} = this.props;
      return (
        <PortalComponent componentTheme={componentTheme}>
          <WrappedComponent {...this.props} />
        </PortalComponent>
      );
    }
  };
};

export default HigherOrderPortalComponent;
