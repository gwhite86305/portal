import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import { CloseModal as ExternalInterface } from '../common/ComponentsInterface';
import { HideModal } from '../../store/Modal/actions';
export type PageProps = ExternalInterface & InternalInterface;
import { PortalComponent,utils } from 'component-utils';
import styled from 'styled-components';
const { applyMediaQueriesForStyle } = utils.styledComponents;
const { CommonComponentThemeWrapper, renderChildComponent } = utils.ComponentUtils;
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  onClick?: any;
}
const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  display:flex;
  cursor:pointer;
`;
export interface DispatchProps {
  onClick(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => {
    return {
    }
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onClick: () => {
      dispatch(HideModal());
    }
  })
)
class CloseModal extends React.Component<PageProps, any> {
  public static defaultProps = defaults;
  public render() {
    return (
    <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="CloseModal-component" tabIndex={0} onKeyPress={this.props.onClick} onClick={this.props.onClick}> {this.props.children} </Container>
    </PortalComponent>
    );
  }
}

export default CloseModal;
