import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {HorizontalMenu as ExternalInterface, MenuItem} from '../common/ComponentsInterface';
import * as defaultProps from './defaultProps.json';
import NavLink from '../common/NavLink';
import MUIMenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {
  LocaleResolvedMenuItem,
  MenuItemDecoratedProps,
  willHandlerSinkMenuItemSelection,
  withMenuItemDecorator
} from 'component-utils/dist/components/MultiMenuItem';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  layout: any;
}

export type ComponentMenuItem = LocaleResolvedMenuItem & MenuItem;
export type Props = InternalInterface & ExternalInterface & MenuItemDecoratedProps;

const MenuContainer = styled(CommonComponentThemeWrapper)`
  ${(props: any) => (props.theme.componentTheme.position === 'fixed' ? 'left:0; right:0;' : 'flex: 1;')};
  overflow: hidden;
  z-index: 2;
  overflow-x: auto;
  display: flex;
  align-items: center;
  -ms-overflow-style: -ms-autohiding-scrollbar;
  -ms-overflow-style: none;
  ${renderThemeProperty('justify', 'justify-content')};
  ::-webkit-scrollbar {
    width: 0;
    height: 0;
  }
  ${renderThemeProperty('position', 'position')};
  ${renderThemeProperty('backgroundColor', 'background')};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) =>
    applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding || props.theme.gutter, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};

  .icon {
    display: flex;
    justify-content: flex-start;
    border: none;
  }
  a:focus {
    color: ${(props: any) => props.theme.tertiaryIconColor};
  }
`;

const MenuList = styled.div`
  position: relative;
  width: auto;
  height: 100%;
  overflow: auto;
  white-space: nowrap;
  list-style: none;
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.menuListPadding, props.theme)};
  margin: 0;

  .menu-item {
    text-decoration: none;
    ${renderThemeProperty('textColor', 'color')};
    ${renderThemeProperty('fontSize', 'font-size')};
    ${renderThemeProperty('fontFamily', 'font-family')};
    ${renderThemeProperty('fontWeight', 'font-weight')};
    ${renderThemeProperty('textShadow', 'text-shadow')};
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.menuItemsPadding, props.theme)};
  }

  .hoverHighlight:hover,
  .hoverHighlight:focus,
  .multiItemSelected {
    text-decoration: none;
    ${renderThemeProperty('textHoverColor', 'color')};
  }

  span {
    ${renderThemeProperty('activeLinkBorderBottomSize', 'border-bottom-width')};
    align-items: center;
    border-bottom-color: transparent;
    border-bottom-style: solid;
    cursor: pointer;
    display: flex;
    flex: 0 1 auto;
    height: 100%;
    line-height: 1.56;
    padding: 0;
    position: relative;
    ${(props: any) =>
      applyMediaQueriesForStyle('padding', props.theme.componentTheme.menuListItemPadding, props.theme)};
  }

  > *:not(:last-child) {
    ${(props: any) =>
      applyMediaQueriesForStyle('margin-right', props.theme.componentTheme.spaceBetweenElement, props.theme)};
  }

  .active {
    ${renderThemeProperty('activeLinkBackground', 'background')};
    span {
      ${renderThemeProperty('activeLinkBorderBottomColor', 'border-bottom-color')};
      ${renderThemeProperty('primaryFocusActiveColor', 'color')};
    }
    > div > span {
      ${renderThemeProperty('textFocusActiveColor', 'color')};
    }
  }
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;

  img {
    flex: 0 1 auto;
    ${({hasSubMenu}: any) => hasSubMenu && 'margin-right: 6px;'};
  }
  span {
    flex: 0 1 auto;
    ${({hasSubMenu}: any) => hasSubMenu && 'margin-right: 6px;'};
  }
  i {
    flex: 0 1 auto;
  }
`;

const Divider = styled.div`
  border-left: 1px solid ${(props: any) => props.theme.componentTheme.dividerColor};
  height: 80%;
  width: 1px;
`;

export const ThemableMenu = styled((props: any) => {
  const {children, ...rest} = props;
  return <Menu {...rest}>{children}</Menu>;
})`
  > div[role='document'] {
    ${renderThemeProperty('dropdownMenuOffset', 'margin-top')};
    ${renderThemeProperty('boxShadow', 'box-shadow')};
    border-top-width: 2px;
    border-top-style: solid;
    ${renderThemeProperty('textHoverColor', 'border-color')};
    ${renderThemeProperty('dropdownMenuBackgroundColor', 'background-color')};

    ul li {
      ${renderThemeProperty('dropdowntextColor', 'color')};
      ${renderThemeProperty('fontSize', 'font-size')};
      ${renderThemeProperty('fontFamily', 'font-family')};
      padding: 10px;
      min-width: 150px;

      a {
        text-decoration: none;
        ${renderThemeProperty('dropdowntextColor', 'color')};
        display: block;
        width: 100%;
      }
    }
  }
`;

class HorizontalMenu extends React.Component<ExternalInterface, any> {
  public static defaultProps = defaultProps;

  constructor(props: any) {
    super(props);
    this.state = {openedMenuItem: null, anchorEl: null};
  }

  private _openMenuItem(event: any, menuItemIndex: number) {
    this.setState({
      anchorEl: event.currentTarget,
      openedMenuItem: this.state.openedMenuItem === menuItemIndex ? null : menuItemIndex
    });
  }

  private _closePopup() {
    this.setState({anchorEl: null, openedMenuItem: null});
  }

  private _handlePopupItemSelection(e: any) {
    // force an A click - allows keyboard nav
    if (e.target.children[0]) {
      e.target.children[0].click();
    }
    this._closePopup();
  }

  private _handleMouseDown(e: any) {
    // Fixes an issue where the text was still highlighted after clicking
    if (e) {
      e.preventDefault();
    }
  }

  private _getFocusClass(componentTheme: any) {
    let hoverColor = componentTheme.textHoverColor ? componentTheme.textHoverColor : '';
    // Finds which focus class a menu item should be
    // If the textHoverColor is not either theme.secondaryFocusColor or theme.tertiaryFocusColor
    // this function returns an empty string (using the primary focus class)
    if (hoverColor == 'theme.secondaryFocusColor') {
      return ' secondary-focus';
    } else if (hoverColor == 'theme.tertiaryFocusColor') {
      return ' tertiary-focus';
    }
    return '';
  }

  private _generateMenuItem(menuItem: ComponentMenuItem, index: number) {
    if (menuItem.children && menuItem.children.length) {
      const isMenuOpen = this.state.openedMenuItem === index;
      const willSinkMenuItemSelection = willHandlerSinkMenuItemSelection(menuItem);
      const {handleMenuItemSelection} = this.props;
      const {anchorEl} = this.state;

      return (
        <div className={'menu-item hoverHighlight ' + (isMenuOpen ? 'multiItemSelected' : '')} key={index}>
          <Item
            {...{hasSubMenu: true}}
            onClick={e => this._openMenuItem(e, index)}
            onKeyPress={e => this._openMenuItem(e, index)}
            tabIndex={0}
          >
            {menuItem.iconUrl ? <img src={menuItem.iconUrl} height={menuItem.iconHeight} /> : null}
            <span>{menuItem.label}</span>
            {menuItem.children ? <i className={isMenuOpen ? 'fa fa-chevron-up' : 'fa fa-chevron-down'} /> : null}
          </Item>

          {isMenuOpen ? (
            <ThemableMenu
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this._closePopup.bind(this)}
              getContentAnchorEl={null}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center'
              }}
            >
              {menuItem.children.map((childMenuItem: any, childIndex: number) => {
                const key = index + '_' + childIndex;
                return willSinkMenuItemSelection ? (
                  <MUIMenuItem
                    key={key}
                    onClick={e => handleMenuItemSelection(e, index, childIndex, this._closePopup.bind(this))}
                  >
                    <div>{childMenuItem.label}</div>
                  </MUIMenuItem>
                ) : (
                  <MUIMenuItem key={key} onClick={e => this._handlePopupItemSelection(e)}>
                    <NavLink to={`${childMenuItem.url}`}>{childMenuItem.label}</NavLink>
                  </MUIMenuItem>
                );
              })}
            </ThemableMenu>
          ) : null}
        </div>
      );
    } else {
      let focusClass = this._getFocusClass(this.props.componentTheme);
      return (
        <NavLink
          activeClassName="active"
          key={index}
          to={`/${menuItem.url}`}
          className={'menu-item hoverHighlight' + focusClass}
          onMouseDown={this._handleMouseDown}
        >
          <Item>
            {menuItem.iconUrl ? <img src={menuItem.iconUrl} height={menuItem.iconHeight} /> : null}
            <span>{menuItem.label ? menuItem.label : ''}</span>
          </Item>
        </NavLink>
      );
    }
  }

  public render() {
    const menuItems = this.props.feature.menuItems || [];
    const menuItemElements = menuItems.map((menuItem: ComponentMenuItem, index: number) =>
      this._generateMenuItem(menuItem, index)
    );
    const itemWithDivider = this.props.feature.dividerEnabled
      ? menuItemElements.reduce(
          (acc, next, index: number) =>
            index === menuItemElements.length - 1 ? [...acc, next] : [...acc, next, <Divider key={`div${index}`} />],
          []
        )
      : menuItemElements;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <MenuContainer>
          <MenuList
            ref={(ref: any) => {
              // find currently selected item;
              const selectedIndex = menuItems.findIndex(
                item => !!(item.url && window.location.pathname.includes(item.url))
              );

              if (selectedIndex && ref) {
                const scrollTo = (ref.clientWidth / itemWithDivider.length) * selectedIndex;
                if (scrollTo > 0) {
                  ref.scrollLeft = scrollTo;
                }
              }
            }}
            children={itemWithDivider}
          />
        </MenuContainer>
      </PortalComponent>
    );
  }
}

const HorizontalMenuWithMenuItems = withMenuItemDecorator(HorizontalMenu, 'menuItems') as any;
// HorizontalMenuWithMenuItems.defaultProps = defaultProps; // TODO uncomment and use real defaults (currently defaults are wrong so leaving this out)
export default HorizontalMenuWithMenuItems;
export {HorizontalMenu};
