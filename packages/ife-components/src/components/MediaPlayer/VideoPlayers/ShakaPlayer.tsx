import {prop, propEq} from 'ramda';
import * as React from 'react';
import {Subject} from 'rxjs';
import * as shaka from 'shaka-player/dist/shaka-player.compiled';
import {AnalyticsErrorCode} from '../../../store/analytics/utils';
import {PlayerProps, PlayerState} from '../types';
import {
  attachVideoPlayerListeners,
  handleAudioLanguageChange,
  handleSubtitleChange,
  handleVolumeChange,
  isNonDRM,
  loadNonDRMContent,
  seekToRange
} from '../utils/playerCommon';
import {initShakaPlayer, installPolyfillOnce} from '../utils/playerShaka';
import {utils} from 'component-utils';

class ShakaPlayer extends React.PureComponent<PlayerProps, PlayerState> {
  playerInstance: any;
  unsubscribe$: Subject<boolean>;

  constructor(props: any) {
    super(props);
    this.unsubscribe$ = new Subject<boolean>();
    this.state = {prevfileManifest: '', fileManifest: '', currentTime: 0, duration: 0};
  }

  static getDerivedStateFromProps(nextProps: PlayerProps, prevState: PlayerProps): Partial<PlayerState> | null {
    if (typeof nextProps.fileManifest === 'string' && nextProps.fileManifest !== prevState.prevfileManifest) {
      return {prevfileManifest: nextProps.fileManifest, fileManifest: ''};
    }
    return null;
  }

  onTimeUpdate = (uid: string, currentTime: number, duration: number) => {
    this.setState({...this.state, currentTime, duration});
    this.props.onTimeUpdate(uid, currentTime, duration);
  };

  componentDidMount() {
    // 1 - polyfill
    installPolyfillOnce();
    if (!shaka.Player.isBrowserSupported()) {
      return this.props.onError(
        {message: 'unsupported Browser', code: AnalyticsErrorCode.player},
        this.props.playbackSessionId
      );
    }
    // 2 - create unique instance
    this.playerInstance = new shaka.Player(this.refs.videoRef);
    // 3 - attach listeners and propagate events to store via props callbacks
    attachVideoPlayerListeners(
      this.playerInstance,
      this.props,
      this.unsubscribe$,
      this.refs.videoRef as any,
      () => this.props,
      this.onTimeUpdate,
      this.props.stopMovie$
    );
    // 4 - initial load
    this.loadVideo(this.props);
    handleVolumeChange(this.props.volume, this.refs.videoRef as any);
    handleSubtitleChange(this.props.subtitleLanguage, this.refs.videoRef as HTMLVideoElement, this.playerInstance);
  }

  componentDidUpdate(prevProps: PlayerProps) {
    // TODO: simplify this , create utils to detect changes
    const videoRef = this.refs.videoRef as HTMLVideoElement;
    const isNewVideo =
      this.state.fileManifest === '' &&
      (this.props.fileManifest !== prevProps.fileManifest ||
        this.props.drmToken !== prevProps.drmToken ||
        this.props.widevineLicenseUrl !== prevProps.widevineLicenseUrl ||
        this.props.playReadyLicenseUrl !== prevProps.playReadyLicenseUrl);

    if (isNewVideo) {
      this.playerInstance && this.playerInstance.destroy();
      this.playerInstance = new shaka.Player(this.refs.videoRef);
      attachVideoPlayerListeners(
        this.playerInstance,
        this.props,
        this.unsubscribe$,
        this.refs.videoRef as any,
        () => this.props,
        this.onTimeUpdate,
        this.props.stopMovie$
      ) && this.loadVideo(this.props);
    }

    if (this.props.seekToRange !== prevProps.seekToRange) {
      seekToRange(this.props, videoRef as any);
    }
    if (this.props.volume !== prevProps.volume) {
      handleVolumeChange(this.props.volume, videoRef as any);
    }

    const collection = this.playerInstance.getTextTracks();
    const selectedTrack = Array.isArray(collection)
      ? collection.find(propEq('language', this.props.subtitleLanguage))
      : null;

    if (
      this.props.subtitleLanguage !== prevProps.subtitleLanguage ||
      (selectedTrack &&
        selectedTrack.language &&
        (selectedTrack.language != this.props.subtitleLanguage || !selectedTrack.active))
    ) {
      handleSubtitleChange(this.props.subtitleLanguage, videoRef, this.playerInstance);
    }

    const playerAudioObj = this.playerInstance.getVariantTracks().find((elem: any) => elem.active);
    if (playerAudioObj && playerAudioObj.language) {
      if (
        this.props.audioLanguage !== prevProps.audioLanguage ||
        playerAudioObj.language !== this.props.audioLanguage
      ) {
        handleAudioLanguageChange(this.props.audioLanguage, videoRef, this.playerInstance);
      }
    }

    if (prevProps.advertId && prevProps.advertId !== this.props.advertId) {
      this.onTimeUpdate(prevProps.advertId, 0, 0);
    }
  }

  componentWillUnmount() {
    this.playerInstance && this.playerInstance.destroy();
    (this.refs.videoRef as any) = null;
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  render() {
    return (
      <video
        controls={false}
        autoPlay={!this.props.isPausedForNotification && !utils.mobileSdk.isUsingMobileSdk()}
        playsInline
        ref="videoRef"
        className="video"
        style={{
          backgroundColor: 'black',
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          zIndex: 100
        }}
      />
    );
  }

  private async loadVideo({fileManifest, drmToken, widevineLicenseUrl, playReadyLicenseUrl}: PlayerProps) {
    try {
      await this.playerInstance.unload();
      if (isNonDRM(fileManifest)) {
        return loadNonDRMContent(this.refs.videoRef as any, fileManifest);
      }
      // 1 - check valid req
      if (!fileManifest || !drmToken || !widevineLicenseUrl || !playReadyLicenseUrl) {
        return;
      }
      // 2 - cancel any request in progress
      // 3 - load video
      // 4 - update audio langs
      this.setState({fileManifest});
      await initShakaPlayer(
        this.playerInstance,
        `${fileManifest}/manifest.mpd`,
        drmToken,
        widevineLicenseUrl,
        playReadyLicenseUrl
      );
      this.props.onAudioLanguagesLoaded(this.playerInstance.getAudioLanguages());
      this.props.onSubtitlesLoaded(
        this.playerInstance
          .getTextTracks()
          .map(prop('language'))
          .concat(['off'])
      );
    } catch (error) {
      this.props.onError({code: AnalyticsErrorCode.player, message: error.toString()}, this.props.playbackSessionId);
    }
  }
}

export default ShakaPlayer;
