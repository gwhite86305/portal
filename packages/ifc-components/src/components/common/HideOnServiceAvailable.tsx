import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getServiceAvailabilityState} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, connected: false};
  if (state.device) {
    props.connected = getServiceAvailabilityState(state.device);
  }
  return props;
})
export default class HideOnServiceAvailable extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.connected ? null : this.props.children;
  }
}
