export const GetTempFC = (value: number, unit = 'F', def = '---') => {
  // Non-numeric values can't be converted
  if (value == null || isNaN(parseInt(value.toString()))) {
    // Else display the default value
    return {F: def, C: def};
  }

  if (unit === 'C') {
    return {
      F: Math.round(9 / 5 * (value + 32)),
      C: Math.round(value)
    };
  } else {
    return {
      F: Math.round(value),
      C: Math.round(5 / 9 * (value - 32))
    };
  }
};
