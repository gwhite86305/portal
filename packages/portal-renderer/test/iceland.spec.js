import {screenshot_test, delay, handleFailures} from './utils';
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');

describe('ICELAND VISUAL REGRESSION TEST', () => {
  let originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 8000000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it(`LAPTOP`, async () => {
    const path = 'test/screenshot/laptop/iceland/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    page.on('console', msg => {
      //console.log(msg);
    });
    page.on('error', msg => {
      //console.log(msg);
    });
    await page.setViewport({width: 1000, height: 1000});

    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    await delay(2000);
    //USER AGREEMENT PAGE
    await page.evaluate(() => {
      document.querySelector('.footer-text').scrollIntoView(true);
    });
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click('.footer-additonal-checkbox'); // Second checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button
    await delay(10000);
    // LANDING PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(10000);

    // Footer Link
    const footerLinks = await page.$x("//div[contains(@class, 'footer-links')]/a");
    if (footerLinks.length < 3) {
      throw new Error('Footer Menu missing');
    }
    // TERM PAGE
    await footerLinks[0].click(); // Term link
    await delay(10000);
    results.push(await screenshot_test(page, `${path}terms`));
    await page.goBack();
    await delay(10000);
    // PRIVACY PAGE
    await footerLinks[1].click(); // Privacy link
    await delay(10000);
    results.push(await screenshot_test(page, `${path}privacy`));
    await page.goBack();
    await delay(10000);

    const connectNowButton = await page.$x("//button[contains(@class, 'core-button-component')]"); // Voucher Option
    if (!connectNowButton) {
      throw new Error('Connect now button is not found');
    }
    await browser.close();
    handleFailures('ICELAND', 'LAPTOP', results);
  });

  it(`MOBILE`, async () => {
    const path = 'test/screenshot/mobile/iceland/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    await page.emulate(devices['iPhone 6']);

    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    //USER AGREEMENT PAGE
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click('.footer-additonal-checkbox'); // Second checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button
    await delay(10000);
    // LANDING PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(10000);

    // Footer Link
    const footerLinks = await page.$x("//div[contains(@class, 'footer-links')]/a");
    if (footerLinks.length < 3) {
      throw new Error('Footer Menu missing');
    }
    // TERM PAGE
    await footerLinks[0].click(); // Term link
    await delay(10000);
    results.push(await screenshot_test(page, `${path}terms`));
    await page.goBack();
    await delay(10000);
    // PRIVACY PAGE
    await footerLinks[1].click(); // Privacy link
    await delay(10000);
    results.push(await screenshot_test(page, `${path}privacy`));
    await page.goBack();
    await delay(10000);

    const connectNowButton = await page.$x("//button[contains(@class, 'core-button-component')]"); // Voucher Option
    if (connectNowButton.length < 1) {
      throw new Error('Connect now button is not found');
    }
    // Hamburger button
    const hamburgeButton = await page.$x("//div[contains(@class, 'bm-burger-button')]/button");
    if (hamburgeButton.length === 0) {
      throw new Error('Hamburger button is missing');
    }
    await hamburgeButton[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}hamburger_menu`));

    await browser.close();
    handleFailures('ICELAND', 'MOBILE', results);
  });
});
