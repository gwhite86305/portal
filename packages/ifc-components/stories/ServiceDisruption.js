import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, text, object, boolean, number} from '@storybook/addon-knobs/react';
import ServiceDisruption from '../dist/components/ServiceDisruption/ServiceDisruption.js';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';
import {withNotes} from '@storybook/addon-notes';

const defaultLocaleProps = {localizedHeaderText: 'Header', localizedAlertText: 'Alert text'};

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.tertiaryBackgroundColor',
    textColor: 'theme.primaryTextColor',
    margin: {
      xs: '0 0 0 0'
    }
  },
  isIFCDown: true
};

const altLocaleProps = {
  localizedHeaderText: 'Important Notice:',
  localizedAlertText:
    'Wi-Fi is currently unavailable. We apologize for the inconvenience. Meanwhile, you can still enjoy complimentary Wireless In-Flight Entertainment (IFE) by clicking one of the media icons on the homepage. '
};

const viasatthemeProps = {
  componentTheme: {
    backgroundColor: 'theme.tertiaryBackgroundColor',
    textColor: 'theme.primaryTextColor',
    margin: {
      xs: '0 0 0 0'
    }
  },
  isIFCDown: true
};

const differentBackgroundTextColorProps = {
  componentTheme: {
    backgroundColor: '#4183D7',
    textColor: 'white',
    margin: {
      xs: '0 0 0 0'
    }
  },
  isIFCDown: true
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";

const stories = storiesOf('ServiceDisruption - CMOB-17996: Demo Portal Theme', module);

stories.addDecorator(withKnobs);

stories.add('ServiceDisruption - With default props', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <ServiceDisruption {...defaultProps} {...defaultLocaleProps} />
  </PortalComponentWrapper>
));

stories.add('ServiceDisruption - With Viasat theme', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <ServiceDisruption {...viasatthemeProps} {...altLocaleProps} />
  </PortalComponentWrapper>
));

stories.add('ServiceDisruption - Without Header', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <ServiceDisruption {...defaultProps} {...{localizedAlertText: 'Alert text'}} />
  </PortalComponentWrapper>
));

stories.add('ServiceDisruption - Without ifcDisruptionAlertText', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <ServiceDisruption {...defaultProps} {...{localizedHeaderText: 'Header'}} />
  </PortalComponentWrapper>
));

stories.add('ServiceDisruption - With Different Background and Text Color', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <ServiceDisruption {...differentBackgroundTextColorProps} {...altLocaleProps} />
  </PortalComponentWrapper>
));

stories.add(
  'ServiceDisruption - With All Knobs',
  withNotes(
    'isIFCDown is internal and the value of it is derived from isServiceAvailable property of device data (  !state.device.isServiceAvailable || state.device.alertId !== "30101")'
  )(() => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <ServiceDisruption
        componentTheme={object('componentTheme', defaultProps.componentTheme)}
        isIFCDown={boolean('internal-isIFCDown', defaultProps.isIFCDown)}
        {...altLocaleProps}
      />
    </PortalComponentWrapper>
  ))
);
