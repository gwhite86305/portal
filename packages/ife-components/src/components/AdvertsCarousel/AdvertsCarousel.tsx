import * as defaultProps from './defaultProps.json';
import * as hasTouch from 'has-touch';
import * as React from 'react';
import Carousel from 'nuka-carousel';
import ThumbSliderContainer from '../common/CarouselView/ThumbSliderContainer.js';
import {AdvertItem} from '../../store/adverts/types.js';
import {ICarouselItem, Props as AdvertsCarouselProps} from './types';
import {leftCtlr, rightCtlr} from '../common/CarouselView/DesktopCarousel.js';
import {path, pathOr} from 'ramda';
import {StyledAdverts, StyledCarouselItem} from './StyledComponents';

const CarouselItem = (props: ICarouselItem) => {
  const {advert, onClick} = props;
  return (
    <StyledCarouselItem>
      <img className="icon" src={path(['galleryImages', 0, 'original', 'url'], advert)} />
      <div className="text">
        <div className="title">{advert.title}</div>
        <div className="subTitle">{advert.subtitle}</div>
      </div>
      <div className="cta" onClick={_ => onClick(advert.uid)}>
        {pathOr('Launch', ['attributes', 'cta'], advert)}
      </div>
    </StyledCarouselItem>
  );
};

export default class AdvertsCarousel extends React.Component<AdvertsCarouselProps> {
  static defaultProps = defaultProps;
  elementRefs: HTMLDivElement[];
  observer: IntersectionObserver;
  containerRef: any;

  constructor(props: AdvertsCarouselProps) {
    super(props);
    this.elementRefs = [];
  }

  observeEntries = (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => {
    const {onItemInViewPort, items} = this.props;
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        const item = items.find((ad: AdvertItem) => ad.uid === entry.target.id);
        onItemInViewPort && onItemInViewPort(item!);
        observer.unobserve(entry.target);
      }
    });
  };

  componentDidMount() {
    const config = {
      root: this.containerRef.current,
      rootMargin: '0px',
      threshold: 0.5
    };
    this.observer = new IntersectionObserver(this.observeEntries, config);
    this.elementRefs.forEach((entry: Element) => this.observer.observe(entry));
  }

  componentWillUnmount() {
    this.observer && this.observer.disconnect();
  }

  render() {
    const {items, feature, onClick} = this.props;
    const filteredItems = feature.adCategory
      ? items.filter((ad: AdvertItem) => path(['categories', 0, 'categoryUid'], ad) === feature.adCategory)
      : items;
    const displayItems = feature.maxSlides ? filteredItems.slice(0, feature.maxSlides) : filteredItems;

    return (
      <StyledAdverts className="StyleAdverts" ref={(ref: any) => (this.containerRef = ref)}>
        <ThumbSliderContainer className="ThumbSliderContainer">
          <div className="single-row">
            <Carousel
              edgeEasing={'easeCircleOut'}
              dragging={false}
              renderCenterLeftControls={hasTouch ? null : leftCtlr}
              renderCenterRightControls={hasTouch ? null : rightCtlr}
              slideWidth={'320px'}
              slidesToScroll={'auto'}
            >
              {displayItems.map((item: AdvertItem, index: number) => (
                <div
                  id={item.uid}
                  key={item.uid}
                  className="item-container"
                  ref={(elementRef: HTMLDivElement) => this.elementRefs.push(elementRef)}
                >
                  <CarouselItem advert={item} onClick={onClick} key={index} />
                </div>
              ))}
            </Carousel>
          </div>
        </ThumbSliderContainer>
      </StyledAdverts>
    );
  }
}
