import * as React from 'react';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export const StyledAdverts = styled.div`
  height: 70px;
  .poster {
    ${renderThemeProperty('itemBorderRadius', 'border-radius')};
  }
  .single-row.single-row--desktop {
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.carouselPadding, props.theme)};
  }
  .slider-control-centerright button {
    border-top-left-radius: 100px;
    border-bottom-left-radius: 100px;
    span {
      margin-left: 10px;
    }
  }
  .item-container {
    padding-left: 10px;
  }

  .slider-control-centerleft button {
    border-top-right-radius: 100px;
    border-bottom-right-radius: 100px;
    span {
      margin-right: 10px;
    }
  }
  .slider-control-centerright,
  .slider-control-centerleft {
    height: 50px !important;
    width: 36px;
    top: -4px !important;
    transform: unset !important;
    padding: 0px;
    button {
      height: 70px !important;
      right: unset;
      left: unset;
      svg {
        height: 36px;
        width: 36px;
      }
    }
  }
  .row-header {
    display: ${(props: any) => (props.theme.componentTheme.displayCategoryTitles ? 'block' : 'none')};
  }
  .thumbnail-title {
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.itemTitlePadding, props.theme)};
  }
  .slider {
    padding: 0px !important;
  }
`;

export const StyledCarouselItem = styled.div`
  min-height: 50px;
  display: flex;
  padding: 10px;
  .icon {
    height: 50px;
    width: 50px;
    box-shadow: 0 0 6px -2px black;
    border-radius: 10px;
    margin-right: 10px;
  }
  .text {
    font-size: 18px;
    letter-spacing: 0.21px;
    line-height: 24px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    .title {
      font-size: 18px;
      letter-spacing: 0.21px;
      line-height: 17px;
    }
    .subTitle {
      font-size: 14px;
      letter-spacing: 0.17px;
      line-height: 16px;
    }
  }
  .cta {
    height: 30px;
    width: 89px;
    border-radius: 15px;
    margin-left: auto;
    background-color: #209eb2;
    font-size: 14px;
    letter-spacing: 0.17px;
    line-height: 24px;
    align-items: center;
    align-self: center;
    display: flex;
    justify-content: center;
    color: #ffffff;
    cursor: pointer;
  }
`;
