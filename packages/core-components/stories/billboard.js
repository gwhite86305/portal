import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, object, text, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import BillBoard from '../dist/components/BillBoard/BillBoard';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  altTextForImage: 'Alt Text For Image',
  subHeaderText: 'Description',
  headerText: 'Title'
};

const defaultProps = {
  componentTheme: {
    fontFamily: 'SourceSansPro',
    headerTextColor: '#1694CA',
    subHeaderTextColor: '#202e39'
  }
};

const modifiedMockTheme_1 = {
  componentTheme: {
    fontFamily: 'SourceSansPro',
    headerTextColor: 'green',
    subHeaderTextColor: 'darkgreen'
  }
};

const modifiedMockTheme_2 = {
  componentTheme: {
    fontFamily: 'SourceSansPro',
    headerTextColor: 'black',
    subHeaderTextColor: 'black'
  }
};

const image_1 = {
  xs: './images/BillBoard1.png'
};
const image_2 = {
  xs: './images/BillBoard2.png'
};
const image_3 = {
  xl: './images/BillBoard3.png',
  lg: './images/BillBoard3.png',
  md: './images/BillBoard2.png',
  sm: './images/BillBoard1.png',
  xs: './images/BillBoard1.png'
};

const headerText_1 = 'Join the quasar club';
const headerText_2 = 'In-flight entertainment';
const headerText_3 = 'Connect the world';
const subHeadertext_1 = 'Earn frequent flyer miles or reward points while you tour the Earth’s atmosphere with Viasat.';

const subHeadertext_2 =
  "Viasat's Wireless In-Flight Entertainment (IFE) is guaranteed to make your passengers happy-it's available on all flights, even those without Wi-Fi.";

const subHeadertext_3 =
  'Viasat-1 and Viasat-2 launched the era of high-capacity satellite Internet. See how the ultra-high capacity Viasat-3 platform will expand our coverage worldwide.';

const openPageOnClick_1 = 'http://www.viasat.com';
const openPageOnClick_2 = 'https://www.neosair.it';
const openPageOnClick_3 = 'http://www.aa.com';

mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles =
  "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./fonts/fontawesome-webfont.woff') format('woff'), url('./fonts/fontawesome-webfont.ttf') format('truetype'), url('./fonts/fontawesome-webfont.svg#font') format('svg');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';
(mockTheme.fontIconUsesLigature = false), (mockTheme.fontIconBaseClass = 'fa');

const stories = storiesOf('[CMOB-16545]-Billboard - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('BillBoard 1 - Default Demo Portal Theme', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    openPageOnClick: openPageOnClick_1,
    image: image_1
  };

  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <BillBoard
        {...testProps}
        headerText={text('Title', headerText_1)}
        subHeaderText={text('Body text', subHeadertext_1)}
        altTextForImage={'Alt text for image'}
      />
    </PortalComponentWrapper>
  );
});

stories.add('BillBoard 2 - Test Theme & Text Configurability', () => {
  const testFeature = {
    openPageOnClick: openPageOnClick_2,
    image: image_2
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <BillBoard
        feature={object('feature', testFeature)}
        componentTheme={object('componentTheme', modifiedMockTheme_1.componentTheme)}
        headerText={text('Title', headerText_2)}
        subHeaderText={text('Body text', subHeadertext_2)}
        altTextForImage={'Alt text for image'}
      />
    </PortalComponentWrapper>
  );
});

stories.add('BillBoard 3 -Test Image Configurability', () => {
  const testFeature = {
    openPageOnClick: openPageOnClick_3,
    image: image_3
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <BillBoard
        feature={object('feature', testFeature)}
        componentTheme={object('componentTheme', modifiedMockTheme_2.componentTheme)}
        headerText={text('Title', headerText_3)}
        subHeaderText={text('Body text', subHeadertext_3)}
        altTextForImage={'Alt text for image'}
      />
    </PortalComponentWrapper>
  );
});
