const Buffer = require('buffer').Buffer;
const fs = require('fs');
const path = require('path');
const url = require('url');
const URL = require('url').URL;
const request = require('request-promise-native');
const child_process = require('child_process');
const chalk = require('chalk');
const _ = require('lodash');

const defaultOptions = {
  profile: '',
  collectAssets: true,
  useMedia: false,
  mediaIdStart: 1000,
  mediaPrefix: '',
  urlWhitelist: [],
  pathBlacklist: ['^/'],
  assetBaseUrl: ''
};

preportal_path = process.env.CAPTIVE_PORTAL_PATH || "demo"

// Map file names in preportal submodule to the asset ID that is
// served up by the PFE
// Other than captiveportal_index.html, these assets are generic and shared by 
// all customers
const captivePortalMediaMapping = {
  ["dist/" + preportal_path + "/captiveportal_index.html"]: 10000
};

// Map keys in captivePortalMedia section of a full portal's layout
// to the asset ID that is served up by the PFE
// These are the assets that are specific to a customer
const captivePortalOverrides = {
  "style": 10010,
  "logo": 10011,
  "copyIcon": 10012,
  "tvIcon": 10013,
  "wifiIcon": 10014,
  "musicIcon": 10015,
  "movieIcon": 10016,
  "globeIcon": 10017,
  "backgroundTop": 10018,
  "backgroundBottomSM": 10019,
  "backgroundBottomMD": 10020,
  "backgroundBottomLG": 10021,
  "caretIcon": 10022,
  "headerFont": 10100,
  "headerFont2": 10101,
  "pageFont": 10102
}

function MediaTranslatorPlugin(options) {
  // Set  up the plugin instance with options...
  this.options = Object.assign({}, defaultOptions, options);
  this.profilePath = this.options.profile;
  this.collectAssets = this.options.collectAssets;
  this.useMedia = this.options.useMedia;
  this.mediaIdStart = this.options.mediaIdStart;
  this.mediaPrefix = this.options.mediaPrefix;
  this.urlWhitelist = this.options.urlWhitelist;
  this.pathBlacklist = this.options.pathBlacklist;
  this.assetBaseUrl = this.options.assetBaseUrl;

  if (typeof urlWhitelist === 'string') {
    this.urlWhitelist = [urlWhitelist];
  }
  if (typeof pathBlacklist === 'string') {
    this.pathBlacklist = [pathBlacklist];
  }
}

MediaTranslatorPlugin.prototype.apply = function(compiler) {
  let output = this.mediaIdStart;
  let useMedia = this.useMedia;
  let mediaPrefix = this.mediaPrefix;
  let nameMap = {'1': 'main.js'};
  let resolved = false;
  let resolvedProfile = undefined;
  let assetBaseUrl = this.assetBaseUrl;
  let pbToken = undefined;
  let pbRequest = request.defaults({
    resolveWithFullResponse: true,
    headers: {
      Referer: `a`
    }
  });

  const tokenPromise = new Promise((resolve, reject) => {
    if (assetBaseUrl) {
      const tokenRequest = {
        url: new URL('auth/token', assetBaseUrl).toString(),
        headers: {
          'Content-Type': 'application/vnd.viasat.vcap.pb.v1.0+json'
        },
        body: JSON.stringify({username: 'viasatAdmin', password: 'viasatAdmin'})
      };
      return pbRequest
        .post(tokenRequest)
        .then(resp => {
          var auth = JSON.parse(resp);
          pbToken = auth['authToken'];
          pbRequest = pbRequest.defaults({
            resolveWithFullResponse: true,
            headers: {
              Authorization: `Bearer ${pbToken}`,
              Referer: `media-translator`
            }
          });
          return resolve(pbToken);
        })
        .catch(resp => {
          console.error(chalk.red(resp));
        });
    } else {
      resolve(true);
    }
  });

  const getOutputName = name => {
    if (name) {
      addToNameMap(output, name);
    }
    return `${mediaPrefix}${output++}`;
  };

  const isMediaId = name => {
    return new RegExp(`^([0-9]+|${mediaPrefix}[0-9]+|profile)$`).test(name);
  };

  const addToNameMap = (mediaId, name) => {
    nameMap[mediaId] = name;
  };

  const finishNameMap = () => {
    if (useMedia && fs.existsSync('dist/manifest.json')) {
      const manifestJson = fs.readFileSync('dist/manifest.json');
      const manifest = JSON.parse(manifestJson);
      Object.keys(manifest).forEach(key => {
        const value = manifest[key];
        const mediaId = value.substr(mediaPrefix.length + (compiler.options.output.publicPath || '').length);
        if (isMediaId(mediaId) && !nameMap[mediaId]) {
          addToNameMap(mediaId, key);
        }
      });
    }
  };

  const getProfileObject = () => {
    if (resolvedProfile) {
      return resolvedProfile;
    }
    const profile = this.profilePath;
    return new Promise(resolve => {
      if (typeof profile !== 'undefined' && typeof profile !== 'null') {
        if (typeof profile === 'string' && profile) {
          try {
            const profileUrl = url.parse(profile);
            if (/https?|ftp/.test(profileUrl.protocol)) {
              pbRequest.get(profileUrl, {}, (error, response, body) => {
                // TODO: Check for errors...
                resolve(body);
              });
            } else {
              fs.readFile(profileUrl.path, (err, contents) => {
                // TODO: Check for errors...
                resolve(contents);
              });
            }
          } catch (exception) {
            // Hopefully it's JSON. Will be caught later if not.
            resolve(profile);
          }
        } else if (typeof profile === 'function') {
          resolve(profile());
        } else if (profile.then) {
          resolve(profile);
        } else {
          resolve('');
        }
      }
    }).then(value => {
      //Test that it's json
      if (value.buffer) {
        value = value.toString();
      }
      if (typeof value === 'string') {
        try {
          resolvedProfile = JSON.parse(value);
          if (resolvedProfile.uid) {
            resolvedProfile = resolvedProfile.details.ux;
          }
          return resolvedProfile;
        } catch (exception) {
          console.error(`It's not JSON!!`);
          throw exception;
        }
      } else if (typeof value === 'object') {
        return value;
      }
      return {};
    });
  };

  const getGitinfo = () => {
    // Format, based on the module used in the original portals: https://www.npmjs.com/package/grunt-gitinfo

    const branchName =
      process.env.BRANCH_NAME ||
      child_process
        .execSync('git rev-parse --abbrev-ref HEAD')
        .toString()
        .replace('\n', '');
    let userName = 'Jenkins';
    try {
      userName = child_process
        .execSync('git config user.name')
        .toString()
        .replace('\n', '');
    } catch (exception) {}
    let userEmail = '';
    try {
      userEmail = child_process
        .execSync('git config user.email')
        .toString()
        .replace('\n', '');
    } catch (exception) {}
    const commitNumber = child_process
      .execSync('git rev-list --count HEAD')
      .toString()
      .replace('\n', '');
    const remoteOrigin = child_process
      .execSync('git config --get remote.origin.url')
      .toString()
      .replace('\n', '');

    // Gets head SHA, head short SHA, time, message, author
    const delim = (commitDelimiter = '++++');
    const lastCommitString = child_process
      .execSync(`git log -n 1 --format=format:"%H${delim}%h${delim}%at${delim}%s${delim}%an${delim}%ae"`)
      .toString()
      .replace('\n', '');
    const lastCommitValues = lastCommitString.split(commitDelimiter);

    return {
      local: {
        branch: {
          current: {
            SHA: lastCommitValues[0],
            shortSHA: lastCommitValues[1],
            name: branchName,
            currentUser: `${userName}<${userEmail}>`,
            lastCommitTime: lastCommitValues[2],
            lastCommitMessage: lastCommitValues[3],
            lastCommitAuthor: `${lastCommitValues[4]}<${lastCommitValues[5]}>`,
            lastCommitNumber: commitNumber
          }
        }
      },
      remote: {
        origin: {
          url: remoteOrigin
        }
      }
    };
  };

  const createPortalManifest = profile => {
    profile = Object.assign({}, profile, {media: nameMap});
    const manifest = {
      profile: profile,
      gitinfo: getGitinfo()
    };
    return manifest;
  };

  const getResource = resourceUrl => {
    return new Promise((resolve, reject) => {
      if (typeof resourceUrl !== 'undefined' && typeof resourceUrl !== 'null') {
        if (typeof resourceUrl === 'string' && resourceUrl) {
          try {
            const finalResourceUrl = url.parse(resourceUrl);
            if (/https?|ftp/.test(finalResourceUrl.protocol)) {
              pbRequest.get(finalResourceUrl, {encoding: null}, (error, response, body) => {
                if (error) {
                  reject(error);
                }
                if (!body) {
                  reject(undefined);
                }
                // TODO: Check for errors...
                resolve(body);
              });
            } else {
              fs.readFile(resourceUrl, (error, contents) => {
                if (error) {
                  reject(error);
                }
                if (!contents) {
                  reject(undefined);
                }
                // TODO: Check for errors...
                resolve(contents);
              });
            }
          } catch (exception) {
            reject(exception);
          }
        } else {
          resolve('');
        }
      } else {
        resolve('');
      }
    }).catch(reason => {
      console.error(chalk.red(reason));
    });
  };

  const writeFile = (fileName, content) => {
    const filePath = path.join(compiler.outputPath, fileName);
    fs.writeFileSync(filePath, content);
  };

  const addCaptivePortalAssetsFromDir = (dir) => {
    let mediaList = []

    for (file in captivePortalMediaMapping) {
      const file_path = dir + '/' + file
      const content = fs.readFileSync(file_path);
      const asset_id = captivePortalMediaMapping[file];
      mediaList.push({old: file_path, new: "media/" + asset_id, content: content})
      addToNameMap(asset_id, file_path);
    }

    return mediaList;
  }

  const addCaptivePortalAssetsFromProfile = (profile) => {
    let mediaList = [];

    for (key in profile.captivePortalMedia) {
      const filePath = profile.captivePortalMedia[key];
      const content = fs.readFileSync(filePath);
      mediaList.push({old: filePath, new: "media/" + captivePortalOverrides[key], content: content});
      addToNameMap(captivePortalOverrides[key], filePath);
    }

    return mediaList;
  }

  /* Function to handle all captivePortal assets. This function:
      * Pulls assets from the preportal submodule
        ** These assets live in the /lib and /<customer> directories
      * Pulls assets defined in the full portal's layout JSON
        ** These assets are customer-specific overrides for any assets that
            are configurable on a per-airline basis
  */
  const handleCaptivePortalContent = (profile) => {
    let captivePortalMediaList = [];

    const captivePortalDir = "./preportal"

    let airline_assets = addCaptivePortalAssetsFromDir(captivePortalDir)
    captivePortalMediaList = captivePortalMediaList.concat(airline_assets)
    captivePortalMediaList = captivePortalMediaList.concat(addCaptivePortalAssetsFromProfile(profile))

    return captivePortalMediaList
  }

  const handleContent = (profile, context) => {
    const mediaList = [];
    for (let key in profile.layout.components) {
      const component = profile.layout.components[key];
      if (component.properties.config) {
        for (let index in component.properties.config) {
          const config = component.properties.config[index];
          try {
            for (let locale in profile.layout.locale) {
              try {
                const fixedMockUrl = config.mockUrl
                  .replace(':portalUid', profile.layout.app.meta.portalUid)
                  .replace(':locale', profile.layout.locale[locale].code)
                  .replace(':componentUid', key)
                  .replace(':contentKey', config.contentKey);
                const fixedUrl = config.url
                  .replace(':portalUid', profile.layout.app.meta.portalUid)
                  .replace(':locale', profile.layout.locale[locale].code)
                  .replace(':componentUid', key)
                  .replace(':contentKey', config.contentKey);
                let contentValue = null;
                if (assetBaseUrl) {
                  const fixedRequest = {
                    url: new URL(fixedUrl, assetBaseUrl).toString()
                  };
                  contentValue = pbRequest.get(fixedRequest).then(
                    response => {
                      if (response.headers['content-type'] === 'application/vnd.viasat.vcap.pb.v1.0+json') {
                        try {
                          const content = JSON.parse(response.body).contents[0].details;
                          const absoluteUrl = new URL(fixedUrl, assetBaseUrl).toString();
                          const fileName = getOutputName(fixedUrl);
                          return {old: absoluteUrl, new: fileName, prop: fixedUrl, content: content};
                        } catch (error) {
                          return null;
                        }
                      }
                      return null;
                    },
                    reason => {
                      console.error(`PB Resource Contact failed: ${reason}`);
                      return null;
                    }
                  );
                } else {
                  let fixedMockPath = `.${fixedMockUrl}`;
                  if (fs.existsSync(fixedMockPath)) {
                    const mockContent = fs.readFileSync(fixedMockPath);
                    const content = JSON.parse(mockContent);
                    const fileName = getOutputName(fixedMockPath);
                    contentValue = {old: fixedMockUrl, new: fileName, prop: fixedMockUrl, content: content};
                  }
                }
                mediaList.push(
                  Promise.all([contentValue]).then(values => {
                    let value = values[0];
                    if (value) {
                      if (!profile.dynamicMediaMap) {
                        profile.dynamicMediaMap = {};
                      }
                      profile.dynamicMediaMap[fixedMockUrl] = value.new;
                      profile.dynamicMediaMap[fixedUrl] = value.new;
                      let content = value.content;
                      let contentMediaList = createResourceList(content, context);
                      const contentReplace = function contentReplace(list) {
                        const thatString = JSON.stringify(this.promisedContent);
                        this.promisedValue.content = thatString;
                        list.push(this.promisedValue);
                        return list;
                      }.bind({promisedContent: content, promisedValue: value});
                      value = Promise.all(contentMediaList).then(contentReplace);
                    } else {
                      console.error(
                        chalk.red(
                          `CONTENT RESOLUTION FAILED FOR ${key} - ${config.contentKey} - ${
                            profile.layout.locale[locale].code
                          }`
                        )
                      );
                    }
                    return value;
                  })
                );
              } catch (error) {
                console.error(chalk.red(`CONTENT RESOLUTION FAILED ${key}: ${error}`));
              }
            }
          } catch (error) {
            console.error(chalk.red(`LOCALE GETTING FAILED ${key}: ${error}`));
          }
        }
      }
    }
    return mediaList;
  };

  const tryGetPropUrl = (context, key, prop) => {
    if (
      prop !== path.basename(prop) &&
      (!this.pathBlacklist.length || !this.pathBlacklist.every(key => prop.match(key))) &&
      fs.existsSync(path.join(context, prop))
    ) {
      let fileName = prop.split('/').pop();
      if (useMedia) {
        fileName = getOutputName(fileName);
      }
      return {old: prop, new: fileName};
    } else if (this.urlWhitelist.length && !this.urlWhitelist.every(key => !prop.match(key))) {
      const u = url.parse(prop);
      if (u.host) {
        let fileName = prop.split('/').pop();
        if (useMedia) {
          fileName = getOutputName(fileName);
        }
        return {old: prop, new: fileName};
      }
    } else {
      return null;
    }
  };

  const handlePropUrl = (context, key, prop) => {
    if (prop.match('^/')) {
      try {
        if (assetBaseUrl) {
          const absoluteUrl = new URL(prop, assetBaseUrl).toString();
          const absoluteRequest = {
            url: absoluteUrl,
            resolveWithFullResponse: false
          };
          const absolutePromise = pbRequest.get(absoluteRequest).then(
            response => {
              if (/content/.exec(prop)) {
                return null; // TODO: This is a little hacky, but since the PB API sends back html on asset miss it's probably the best solution
              }
              const fileName = getOutputName(prop);
              return {old: absoluteUrl, new: fileName, prop: prop};
            },
            reason => {
              return null;
            }
          );
          return absolutePromise;
        } else {
          const absolutePath = `.${prop}`;
          if (fs.existsSync(absolutePath)) {
            const fileName = getOutputName(prop);
            return {old: absolutePath, new: fileName, prop: prop};
          } else {
            return null;
          }
        }
      } catch (exception) {
        console.error(chalk.red(`${exception} ${prop}`));
      }
    } else {
      try {
        return tryGetPropUrl(context, key, prop);
      } catch (error) {
        console.error(chalk.red(`ERROR: ${key} - ${prop} - ${error}`));
      }
    }
  };

  const createResourceList = (fromObj, context) => {
    let mediaList = [];
    for (let key in fromObj) {
      const prop = fromObj[key];
      const propType = typeof prop;
      switch (propType) {
        case 'array':
        case 'object':
          const myMediaList = createResourceList(prop, context);
          mediaList = mediaList.concat(myMediaList);
          break;
        case 'string':
          let stringList = [prop];
          let outputList = [];
          let outString = prop;
          const cssUrlRegexp = /url\(['"](.+?)['"]\)/g;
          let match = cssUrlRegexp.exec(prop);
          if (match && match[1]) {
            stringList = [];
            while (match && match[1]) {
              stringList.push(match[1]);
              match = cssUrlRegexp.exec(prop);
            }
          }
          const hrefUrlRegexp = /href=['"](.+?)['"]/g;
          match = hrefUrlRegexp.exec(prop);
          if (match && match[1]) {
            stringList = [];
            while (match && match[1]) {
              stringList.push(match[1]);
              match = hrefUrlRegexp.exec(prop);
            }
          }
          for (let i in stringList) {
            let str = handlePropUrl(context, key, stringList[i]);
            outputList.push(str);
            mediaList.push(str);
          }
          for (let i in outputList) {
            if (outputList[i]) {
              if (outputList[i].then) {
                const rename = function rename(str) {
                  if (str) {
                    this.fromObj[this.key] = this.fromObj[this.key].replace(str['prop'], str['new']);
                  }
                }.bind({fromObj: fromObj, key: key});
                outputList[i].then(rename);
              } else {
                outString = outString.replace(stringList[i], outputList[i]['new']);
              }
            }
          }
          fromObj[key] = outString;
          break;
        default:
          break;
      }
    }
    return mediaList;
  };

  const handleUnsupportedDevicePage = (profile, context) => {
    let u = profile.layout.app.unsupportedDevicePage;
    if (!u) {
      u = './cms-served-assets/defaultUnsupportedDevicePage.html';
    }
    profile.layout.app.unsupportedDevicePage = `${mediaPrefix}5000`;
    nameMap['5000'] = u;
    return [{old: u, new: `${mediaPrefix}5000`, prop: 'unsupportedDevicePage.html'}];
  };

  compiler.hooks.done.tap('MediaTranslatorPlugin', () => {
    if (!resolved) {
      resolved = true;
      tokenPromise
        .then(getProfileObject)
        .then(profile => {
          const resourcePromises = [];
          if (this.collectAssets) {
            let resourcesList = handleUnsupportedDevicePage(profile);
            resourcesList = resourcesList.concat(createResourceList(profile.layout, compiler.context));
            resourcesList = resourcesList.concat(createResourceList(profile.settings, compiler.context));
            resourcesList = resourcesList.concat(createResourceList(profile.theme, compiler.context));
            resourcesList = resourcesList.concat(handleContent(profile, compiler.context));
            if (process.env.INCLUDE_CAPTIVEPORTAL) {
              resourcesList = resourcesList.concat(handleCaptivePortalContent(profile));
            }

            Promise.all(resourcesList)
              .then(resources => {
                const resources2 = [];
                for (let index in resources) {
                  if (resources[index] && resources[index].length) {
                    for (let index2 in resources[index]) {
                      resources2.push(resources[index][index2]);
                    }
                  } else {
                    resources2.push(resources[index]);
                  }
                }
                return Promise.all(resources2);
              })
              .then(resources => {
                Object.keys(resources).forEach((item, index, array) => {
                  if (resources[item]) {
                    let contentPromise = new Promise(resolve => {
                      const content = resources[item].content;
                      resolve(content);
                    });
                    if (!resources[item]['content']) {
                      contentPromise = getResource(resources[item]['old']);
                    }
                    resourcePromises.push(
                      contentPromise
                        .then(content => {
                          writeFile(resources[item]['new'], content, profile, item);
                        })
                        .catch(reason => {
                          console.error(chalk.red(reason));
                        })
                    );
                  }
                });
                Promise.all(resourcePromises).then(() => {
                  finishNameMap();
                  const manifest = createPortalManifest(profile);

                  // Generate metadata so that elements can be traced to a build.
                  let branch = _.get(manifest, 'gitinfo.local.branch.current.name', '?');
                  let buildNumber = _.get(process, 'env.BUILD_NUMBER', '?');
                  let shortSHA = _.get(manifest, 'gitinfo.local.branch.current.shortSHA', '?');
                  const metadata = {
                    branch: branch,
                    buildNumber: buildNumber,
                    shortSHA: shortSHA,
                    identifier: `portal.${branch}.${buildNumber}.${shortSHA}`
                  };

                  // Add build metada to all first children of profile and manifest.profile
                  // Must be on the elements because a root level metatada object will be discarded by the pfe
                  _.forEach(profile, (value, key) => {
                    value.meta = metadata;
                  });
                  _.forEach(manifest.profile, (value, key) => {
                    value.meta = metadata;
                  });

                  writeFile('profile', JSON.stringify(profile, null, 2)); // Leaving this here for running locally
                  writeFile('_react_manifest.json', JSON.stringify(manifest, null, 2));
                });
              });
          }
        })
        .catch(reason => {
          console.error(chalk.red('Media Translator Plugin could not find or could not read the profile'));
          console.error(chalk.red(reason));
          console.error(reason.stack);
        });
    }
  });

  compiler.hooks.thisCompilation.tap('MediaTranslatorPlugin', compilation => {
    compilation.hooks.optimizeChunks.tap('MediaTranslatorPlugin', chunks => {
      if (useMedia) {
        // Convert chunks into numeric media
        chunks.forEach(chunk => {
          if (!isMediaId(chunk.name)) {
            let outputName = getOutputName(chunk.name);
            chunk.filenameTemplate = outputName;
            chunk.name = outputName;
          }
        });
      }
    });
  });
};

module.exports = MediaTranslatorPlugin;
