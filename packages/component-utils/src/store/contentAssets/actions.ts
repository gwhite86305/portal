import {ActionCreator} from '../action-creator';
import LayoutInterface from '../../types/LayoutInterface';
import {compose, filter, isNil, keys, map, reject} from 'ramda';
import axios from 'axios';
import {isEnvUXBuilder, getBuilderApi} from '../../utils/ComponentUtils';

export type Action = typeof ActionCreators[keyof typeof ActionCreators];
export const ActionCreators = {
  receiveContent: new ActionCreator<'receiveContent', any>('receiveContent')
};

interface RequestParams {
  locale: string;
  contentKey: string;
  componentUid: string;
}

interface ContentRequests {
  promise: Promise<any>;
  params: RequestParams;
}

const _getContentRequests = (layoutJson: LayoutInterface, locale: string): ContentRequests[] => {
  const requests: ContentRequests[] = [];

  Object.keys(layoutJson.components).forEach((componentUid: string) => {
    const {config} = layoutJson.components[componentUid].properties;

    if (config && config.length) {
      config.forEach(configItem => {
        if (!configItem.lazyload) {
          // for now, content editing api will only be hit when in builder mode
          let apiUrl = null;
          if (isEnvUXBuilder()) {
            apiUrl = getBuilderApi() + configItem.url;
          } else {
            // otherwise use pre-existing mock call
            apiUrl = configItem.mockUrl || configItem.url;
            configItem.contentType = 'application/json';
          }

          apiUrl = apiUrl
            .replace(/\:portalUid/g, layoutJson.app.meta.portalUid)
            .replace(/\:locale/g, locale)
            .replace(/\:componentUid/g, componentUid)
            .replace(/\:contentKey/g, configItem.contentKey);

          const headers: any = {
            'content-type': configItem.contentType
          };

          headers.accept = configItem.accept || configItem.contentType;

          requests.push({
            promise: axios.get(apiUrl, {
              headers,
              validateStatus: status => (status >= 200 && status < 300) || status === 404
            }),
            params: {
              locale: locale,
              contentKey: configItem.contentKey,
              componentUid: componentUid
            }
          });
        }
      });
    }
  });

  return requests;
};

export function updateComponentContent(builderData: any) {
  const {contentKey, componentUid, localizedContent} = builderData;
  return (dispatch: any, getState: any) => {
    const {content} = getState().contentAssets;
    Object.keys(localizedContent).forEach((locale: string) => {
      content[componentUid] = content[componentUid] || {};
      content[componentUid][locale] = content[componentUid][locale] || {};
      content[componentUid][locale][contentKey] = localizedContent[locale].details;
    });
    dispatch(ActionCreators.receiveContent.create(content));
  };
}

// TODO for now preload all content assets on app startup - this can be improved in time (by page for example)
export function initContentAssets(layoutJson: LayoutInterface, locale: string) {
  return (dispatch: any) => {
    const requests: ContentRequests[] = _getContentRequests(layoutJson, locale);

    return new Promise(resolve => {
      const content: any = {};

      // currently dumb and just replaces all in store
      axios
        .all(requests.map((r: any) => r.promise))
        .then(({...args}) => {
          requests.map((r: any) => r.params).forEach((requestParam: any, index: number) => {
            const {componentUid, locale, contentKey} = requestParam;
            const response = args[index] as any;

            if (response.status === 200) {
              let contentData = response.data;

              // i.e. when hitting the *real* api (in builder only for now), we need to dig deeper for content data
              // mock api is just raw JSON content blob
              if (isEnvUXBuilder()) {
                if (response.data.contents[0]) {
                  contentData = response.data.contents[0].details;
                }
              }

              content[componentUid] = content[componentUid] || {};
              content[componentUid][locale] = content[componentUid][locale] || {};
              content[componentUid][locale][contentKey] = contentData;
            }
          });

          dispatch(ActionCreators.receiveContent.create(content));
          console.info('Retrieving content assets for layout...');
          resolve();
        })
        .catch(err => {
          dispatch(ActionCreators.receiveContent.create(content));
          console.info('Error occurred retrieving content assets for layout... carry on as if nothing happened');
          resolve();
        });
    });
  };
}
