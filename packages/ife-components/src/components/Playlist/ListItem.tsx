import CloseIcon from '@material-ui/icons/Close';
import LocalMovies from '@material-ui/icons/LocalMovies';
import MusicNote from '@material-ui/icons/MusicNote';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import {path, pathOr} from 'ramda';
import * as React from 'react';
import {getSmallestAndBiggestImg} from '../common/utils';
import {ListItemContainer} from './styledComponents';
import ListItemText from './ListItemText';
import {AudioItem} from '../../store/audio/types';
import {VideoItem, TYPES} from '../../store/videos/types';
import {prop} from 'ramda';
import { Button } from '@material-ui/core';

export interface ListItemProps {
  currentPlayingItem: VideoItem | AudioItem;
  isPlaying: boolean;
  item: VideoItem | AudioItem;
  onItemPlay(item: any): any;
  onItemPause(item: any): any;
  onRemoveItem(item: any): any;
}

const isPause = (props: ListItemProps) => prop('uid', props.currentPlayingItem) === props.item.uid && props.isPlaying;

const ListItem: React.FC<ListItemProps> = (props: ListItemProps) => {
  const smallestImgUrl = getSmallestAndBiggestImg(
    (props.item as VideoItem).galleryImages,
    (props.item as VideoItem).titleImages,
    ''
  )[0];
  const showPause = isPause(props);
  const action = showPause ? props.onItemPause : props.onItemPlay;

  return (
    <ListItemContainer>
      <div className="play-pause" onClick={action}>
        <div className="img-container">
          <div
            className={'playlist-image'}
            style={{backgroundImage:`url(${smallestImgUrl})`}}
          />
          <div
            className={`play-pause-button${showPause ? ' show' : ''}`}
            children={showPause ? <PauseIcon /> : <PlayArrowIcon />}
          />
        </div>
        <ListItemText item={props.item} />
      </div>
      <Button aria-label="Remove from playlist" className="close-button" onClick={() => props.onRemoveItem(props.item.uid)} children={<CloseIcon />} />
    </ListItemContainer>
  );
};

export default ListItem;
