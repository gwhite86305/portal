import * as React from 'react';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {Legal as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper, renderChildComponent} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  content: any;
}

const LegalWrapper = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  width: 100%;
  .legal-content {
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'margin-left',
        {xs: `calc(${props.theme.gutter}*1.25)`, sm: `calc(${props.theme.gutter}*2.5)`},
        props.theme
      )};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'margin-right',
        {xs: `calc(${props.theme.gutter}*1.25)`, sm: `calc(${props.theme.gutter}*2.5)`},
        props.theme
      )};
    h1,
    h2,
    h3 {
      margin: 0px;
    }
    .motif {
      ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.motifTextMargin, props.theme)};
      ${renderThemeProperty('motifTextFontFamily', 'font-family')};
      ${renderThemeProperty('motifTextColor', 'color')};
      ${renderThemeProperty('motifTextFontSize', 'font-size')};
      ${renderThemeProperty('motifTextFontWeight', 'font-weight')};
      line-height: 1.04;
    }
    .motif-underline {
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 2.5625);
      border-bottom-width: 1px;
      border-bottom-style: solid;
      ${renderThemeProperty('motifUnderlineColor', 'border-bottom-color')};
    }
    .notice-header-margins {
      ${(props: any) =>
        applyMediaQueriesForStyle('margin', props.theme.componentTheme.noticeHeaderMargin, props.theme)};
    }
    .notice-header {
      ${renderThemeProperty('noticeHeaderFontSize', 'font-size')};
      ${renderThemeProperty('noticeHeaderFontWeight', 'font-weight')};
      ${renderThemeProperty('noticeHeaderTextColor', 'color')};
    }
    .notice-text {
      ${renderThemeProperty('noticeTextColor', 'color')};
      ${renderThemeProperty('noticeTextFontSize', 'font-size')};
      ${renderThemeProperty('noticeTextFontWeight', 'font-weight')};
      ${(props: any) =>
        applyMediaQueriesForStyle('padding', props.theme.componentTheme.noticeTextPadding, props.theme)};
      border-bottom-width: 1px;
      border-bottom-style: solid;
      ${renderThemeProperty('noticeTextBorderColor', 'border-bottom-color')};
    }
    .effective-date {
      font-size: ${(props: any) => props.theme.primaryFontSize};
      color: ${(props: any) => props.theme.primaryTextColor};
      margin-top: calc(${(props: any) => props.theme.gutter} * 2.5);
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 6.25);
    }
    .motif-highlight {
      ${(props: any) =>
        applyMediaQueriesForStyle('margin', props.theme.componentTheme.highlightTextMargin, props.theme)};
      ${(props: any) =>
        applyMediaQueriesForStyle('font-size', props.theme.componentTheme.highlightTextFont, props.theme)};
      ${renderThemeProperty('highlightTextColor', 'color')};
      ${renderThemeProperty('highlightTextFontSize', 'font-size')};
      ${renderThemeProperty('highlightTextFontWeight', 'font-weight')};
    }
  }
`;

const Header = (props: any) => {
  const {headerImage} = props;
  const Container = styled.div`
      ${(props: any) => applyMediaQueriesForStyle('height', {xs: '100px', md: '150px'}, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('background-image', headerImage, props.theme)};
      background-position: center;
      background-size: cover;
      background-repeat: no-repeat;
  }`;
  return <Container />;
};

class Legal extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {children, content} = this.props;

    const {header, effectiveDate} = content;

    const {motifText, motifHighlightText, noticeHeader, headerText, noticeText} = header;

    const {headerImage, useMaterial} = this.props.feature;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <LegalWrapper className="core-legal-component">
          {headerImage ? <Header headerImage={headerImage} /> : null}
          <div className="legal-content">
            {!(motifHighlightText || noticeHeader || headerText) ? (
              <h1 className="motif motif-underline">{motifText}</h1>
            ) : (
              <h1 className="motif">{motifText}</h1>
            )}
            {motifHighlightText ? <h2 className="motif-highlight">{motifHighlightText}</h2> : null}
            {noticeHeader ? (
              <h2 className={motifHighlightText ? 'notice-header' : 'notice-header notice-header-margins'}>
                {noticeHeader}
              </h2>
            ) : null}
            {noticeText ? <h3 className="notice-text" dangerouslySetInnerHTML={sanitize(noticeText)} /> : null}

            {renderChildComponent(0, children, {
              feature: {title: headerText, sections: content.sections, useMaterial: useMaterial}
            })}

            <p className="effective-date">{effectiveDate.effectiveDateText}</p>
          </div>
        </LegalWrapper>
      </PortalComponent>
    );
  }
}

export default Legal;
