import * as React from 'react';
import {PortalComponent} from 'component-utils';
import {Carousel as ExternalInterface} from '../common/ComponentsInterface';
import * as defaultProps from './defaultProps.json';
import StyledCarousel from './StyledCarousel';
import NukaCarousel from 'nuka-carousel';

class Carousel extends React.Component<ExternalInterface, any> {
  public static defaultProps = defaultProps;
  state = {maxWidth: '100%'};
  constructor(props: ExternalInterface) {
    super(props);
    this.reRender = this.reRender.bind(this);
  }

  componentDidMount() {
    this.reRender();
    window.addEventListener('resize', this.reRender);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.reRender);
  }

  public render() {
    const {componentTheme, children, feature} = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledCarousel style={{maxWidth: this.state.maxWidth}}>
          <NukaCarousel
            autoplay={feature.autoplay}
            autoplayInterval={feature.autoplayInterval}
            edgeEasing="easeCircleOut"
            renderBottomCenterControls={this.renderBottomCenterControls}
            renderCenterLeftControls={this.renderCenterLeftControls}
            renderCenterRightControls={this.renderCenterRightControls}
            wrapAround={true}
          >
            {children}
          </NukaCarousel>
        </StyledCarousel>
      </PortalComponent>
    );
  }

  private renderCenterLeftControls = ({previousSlide}: any) => (
    <button className="control-container" onClick={previousSlide} children={<i className="arrow arrow--left" />} />
  );

  private renderCenterRightControls = ({nextSlide}: any) => (
    <button className="control-container" onClick={nextSlide} children={<i className="arrow arrow--right" />} />
  );

  private renderBottomCenterControls = ({slideCount, goToSlide, currentSlide}: any) => (
    <div className="bottom-center-controls">
      {new Array(slideCount)
        .fill(true)
        .map((_, index) => (
          <button
            key={index}
            className={`bullet-button ${currentSlide === index && 'selected'}`}
            onClick={goToSlide.bind(null, index)}
          />
        ))}
    </div>
  );

  private reRender() {
    this.setState((_: any) => ({maxWidth: `${document.body.clientWidth}px`}));
  }
}

export default Carousel;
