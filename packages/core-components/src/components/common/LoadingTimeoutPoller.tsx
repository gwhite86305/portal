import * as React from 'react';
import {utils} from 'component-utils';
import {ClearPendingRequestsCounter} from '../../store/Loading/actions';
import Poller from '../Poller/Poller';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';
import {pathOr} from 'ramda';

const connectWrapper = utils.connectWrapper.default;

export interface ExternalInterface {}

export interface InternalInterface {
  timeoutTimestamp: number;
  className?: string;
  children?: any;
}

export interface DispatchProps {
  checkClearPendingRequestsCounter(timeoutTimestamp: number): any;
}

interface FinalProps extends ExternalInterface, InternalInterface, DispatchProps {}

@connectWrapper(
  (state: any, ownProps: ExternalInterface) => {
    const fromState: Partial<FinalProps> = {};
    let timeout = pathOr(15000, ['ux', 'settings', 'core', 'globalLoadingTimeout'], state);
    fromState.timeoutTimestamp = Number.MAX_VALUE;
    if (state.loading && state.loading.TimeoutTimestamp)
      fromState.timeoutTimestamp = state.loading.TimeoutTimestamp + timeout;
    return {...ownProps, ...fromState};
  },
  (dispatch: any, ownProps: ExternalInterface): DispatchProps => ({
    checkClearPendingRequestsCounter: (timeoutTimestamp: number) => {
      if (!isEnvUXBuilder() && new Date().valueOf() > timeoutTimestamp) {
        dispatch(ClearPendingRequestsCounter());
      }
    }
  })
)
export default class LoadingTimeoutPoller extends React.Component<ExternalInterface, any> {
  public render() {
    const props: FinalProps = this.props as FinalProps;
    const timeoutTimestamp = props.timeoutTimestamp;
    return (
      <Poller
        action={() => props.checkClearPendingRequestsCounter(timeoutTimestamp)}
        interval={1001}
        children={props.children}
      />
    );
  }
}
