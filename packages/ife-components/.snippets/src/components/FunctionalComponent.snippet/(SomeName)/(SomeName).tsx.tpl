import * as React from 'react';

interface <%= SomeName %>Props {
    text: string
}

export const <%= SomeName %>: React.FC<<%= SomeName %>Props> = ({ 
    text 
}) => (
    <h1>
        {text}
    </h1>
)