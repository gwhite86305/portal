# portal components lib

ViaSat InFlight components

CommonJS lib containing React components, container components, and corresponding Redux stores for in-flight UXes.
This lib is intended to be imported into clients for the following purposes:
1. Allow a client app direct access to components and stores, so api can be accessed, desired components can be used, and an app can be constructed for bespoke needs.
2. Allow a client app to mount the &lt;App/&gt; root component which consumes **Layout**, **App Theme**, and **App Menu** data from CMS UX builder api, and generates a themed UX from available components.
This is the job of the *portal-renderer* app to simply kick this process off, and provide a webpack build for browser deployment.
3. Allow a client app direct access to meta data describing the available components, their **feature properties**, **theme properties**, and **locale properties**.
This meta-data is used to build a ux building tool: the *portal-builder* project, which builds the **Layout**, **App Theme**, and **App Menu** JSON, which in turn is consumed to render the UX through the CMS builder apis.
This metadata is delivered to the portal-builder in the form:
* components-schema.json for components schema definition
* theme-schema.json for theme schema definition
* component-defaults.json for component defaults


Additionally this lib exposes localization strings for all supported component languages (uses React-intl lib).

* Note: this libs exposed JSON Schema combined with the components locale information contain *everything* that the passport-ux-builder app needs to allow configuration of this app.
The intention of this design is to allow a future version of the ux-builder to dynamically ingest this information for a specific version of this library (i.e. http call to ingest as opposed to static npm dependency), 
allowing *per app version* configuration.  

All components have **feature properties**, **theme properties**, and **locale properties** exposed by their ExternalInterface.ts.
Components inherit interface properties, and expose types as defined in /components/types.
Components bind to their Redux stores through Containers using the usual pattern.

Events

* Inter-component communication is handled using a simple emit/subscribe observer design pattern. Generally, events should be handled through store state changes, but sometimes this isn't practical, or is overkill - either way don't overuse this approach.
* SockJS & StompJS are used for WebSocket comms. Events subscribed to will cause Redux store state changes, which components can respond to in the normal manner. Currently this is disconnected until such events become exposed by api.

Outstanding/Other

* API calls are mocked until future integration with *real* apis - this is an on-going process... 
* Component property defaults (implemented in the usual react manner) are important so components work "out-of-the-box". 
It is also important that the generated schema contains these defaults so the passport-ux-builder doesn't need a future static dependency on this library.
So for now component props must be defined *in two places* (as React prop defaults and in ExternalInterface) for each component. We need a better way to do this in the future (expose props as simple JSON in similar manner to locales?)  
* CMS api's for static data (images, fonts) not yet available, this lib just assumes these resources are available for now. 

## Requirements

+ NodeJS >=5.5.1
+ Typescript compiler (2.5.3 for now): 

```sh
npm install -g typescript@2.5.3
```

## Component & theme TS Definition generation

If you modify *the components or theme schema*, ensure to run this script which generates the ts definition files (will be called by ```$ npm run build``` anyway):

```sh
$ node scripts/generateComponentTypes.js
```
If you modify *a components defaultProps.json*, ensure to run this script which generates the default props for the portal-builder (will be called by ```$ npm run build``` anyway):

```sh
$ node scripts/generateComponentDefaults.js
```
