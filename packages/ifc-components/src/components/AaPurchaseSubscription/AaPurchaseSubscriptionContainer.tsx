import * as React from 'react';
import {utils} from 'component-utils';
import {AaPurchaseSubscription as ExternalInterface} from '../common/ComponentsInterface';
import AaPurchaseSubscription, {InternalInterface} from './AaPurchaseSubscription';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import {getUserCard, purchaseGogoSubscription} from '../../store/Authentication/actions';
import {pathOr} from 'ramda';

const connectWrapper = utils.connectWrapper.default;

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  payNow(card: string, nameOnTheCard: string, email: string, subId: number): any;
  onCancelClick(): any;
  getCards(url: string, jwt: string): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    let props = {...ownProps};
    let subId = pathOr(null, ['WifiServiceOptions', 'selected', 'subId'], state);
    let subscription =
      subId &&
      state.profile &&
      state.profile.subscriptionOptions &&
      state.profile.subscriptionOptions.filter((s: any) => s.id === subId)[0];
    props.subscription = subscription;
    props.Authentication = state.Authentication;
    props.successCode = 100000;
    return props;
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      payNow: (card, nameOnTheCard, email, subId) => {
        dispatch(
          purchaseGogoSubscription(ownProps.feature.subscriptionPurchaseApiUrl, card, nameOnTheCard, email, subId)
        );
      },
      onCancelClick: () => {
        ownProps.history.push('/');
      },
      getCards: (url, jwt) => {
        dispatch(getUserCard(url, jwt));
      }
    };
  }
)
class AaPurchaseSubscriptionContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }
  componentDidMount() {
    if (this.props.Authentication.aaAuth && this.props.Authentication.aaAuth.jwt && this.props.subscription) {
      this.props.getCards(this.props.feature.userCardApiUrl, this.props.Authentication.aaAuth.jwt);
    }
  }
  componentDidUpdate(prevProps: any) {
    let code = pathOr(null, ['Authentication', 'gogoSubscriptionPurchase', 'body', 'code'], this.props);
    if (code == this.props.successCode) {
      // TODO
      // 1) Call check subscription endpoint
      // 2) Dispatch WaitForServiceTier
      // 3) Take user to connected page.
      this.props.history.push(this.props.feature.openPageAfterPurchase);
    }
  }

  public render() {
    const {intl, componentUid} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.AaPurchaseSubscription.header'},
      {storedPaymentLabel: 'ifc.AaPurchaseSubscription.storedPaymentLabel'},
      {nameOnCardLabel: 'ifc.AaPurchaseSubscription.nameOnCardLabel'},
      {expirationLabel: 'ifc.AaPurchaseSubscription.expirationLabel'},
      {emailLabel: 'ifc.AaPurchaseSubscription.emailLabel'},
      {confirmEmailLabel: 'ifc.AaPurchaseSubscription.confirmEmailLabel'},
      {paymentFormFooterNote: 'ifc.AaPurchaseSubscription.paymentFormFooterNote'},
      {automaticRenewalNoticeHeader: 'ifc.AaPurchaseSubscription.automaticRenewalNoticeHeader'},
      {automaticRenewalNotice: 'ifc.AaPurchaseSubscription.automaticRenewalNotice'},
      {cancelButtonText: 'ifc.AaPurchaseSubscription.cancelButtonText'},
      {paynowButtonText: 'ifc.AaPurchaseSubscription.paynowButtonText'},

      {errorMessageCardRequired: 'ifc.AaPurchaseSubscription.errorMessageCardRequired'},
      {errorMessageNameOnTheCardRequired: 'ifc.AaPurchaseSubscription.errorMessageNameOnTheCardRequired'},
      {errorMessageEmailRequired: 'ifc.AaPurchaseSubscription.errorMessageEmailRequired'},
      {errorMessageConfirmEmailRequired: 'ifc.AaPurchaseSubscription.errorMessageConfirmEmailRequired'},
      {emailMessageEmailMismatch: 'ifc.AaPurchaseSubscription.emailMessageEmailMismatch'},
      {errorMessageOnFormHeader: 'ifc.AaPurchaseSubscription.errorMessageOnFormHeader'},
      {errorMessageInvalidEmail: 'ifc.AaPurchaseSubscription.errorMessageInvalidEmail'},
      {invalidPaymentInfo: 'ifc.AaPurchaseSubscription.invalidPaymentInfo'},
      {errorMessageGeneric: 'ifc.AaPurchaseSubscription.errorMessageGeneric'}
    ]);
    let errorDict = {
      errorMessageOnFormHeader: localizedProps.errorMessageOnFormHeader,
      errorMessageCardRequired: localizedProps.errorMessageCardRequired,
      errorMessageNameOnTheCardRequired: localizedProps.errorMessageNameOnTheCardRequired,
      errorMessageEmailRequired: localizedProps.errorMessageEmailRequired,
      errorMessageInvalidEmail: localizedProps.errorMessageInvalidEmail,
      emailMessageEmailMismatch: localizedProps.emailMessageEmailMismatch,
      errorMessageConfirmEmailRequired: localizedProps.errorMessageConfirmEmailRequired,
      '100002': localizedProps.invalidPaymentInfo,
      generic: localizedProps.errorMessageGeneric
    };
    let onPayNowClick = (card: any, nameOnTheCard: string, email: string, subId: number) => {
      this.props.payNow(card, nameOnTheCard, email, subId);
    };
    return this.props.subscription ? (
      <AaPurchaseSubscription {...this.props} errorDict={errorDict} {...localizedProps} onPayNowClick={onPayNowClick} />
    ) : null;
  }
}

export default injectIntl(withRouter(AaPurchaseSubscriptionContainer));
