import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const showTitleHeaderCSS = '.header .title {display: flex;} .details .title {display: none;}';
const showTitlePosterCSS = '.header .title {display: none;} .details .title {display: flex;}';

// TODO: add a script that makes sure the album cover appears in view when the track list is super long - absolute positioning

export const StyledContainer = styled.div`
  .music-container {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    align-items: center;
  }
  .poster-container {
    display: flex;
    flex: 1;
    justify-content: center;
  }
  .poster-container .poster {
    ${_ => applyMediaQueriesForStyle('height', {xs: '300px', sm: '508px', md: '600px'}, {})};
    ${_ => applyMediaQueriesForStyle('width', {xs: '255px', sm: '357px', md: '421px'}, {})};
  }
  .poster-container .poster-image {
    height: 100%;
  }
  .play-pause-action,
  .playlist-action {
    ${renderThemeProperty('trackButtonsColor', 'color', true)};
    &:hover {
      ${renderThemeProperty('trackButtonsColorHover', 'color', true)};
    }
  }
  .play-all-button {
    /*border: 1px solid transparent;*/
    padding: 0.25rem 1rem 0.25rem 0.5rem;
    position: relative;
    left: -0.5rem;
    border-radius: 1.5rem;
    display: flex;
    align-items: center;
    text-transform: none;
    min-width: auto;
    min-height: auto;
    ${renderThemeProperty('playAllButtonColor', 'background', true)};
    ${renderThemeProperty('trackButtonsColor', 'border-color', true)};
    &:hover {
      ${renderThemeProperty('trackButtonsColorHover', 'border-color', true)};
      .play-pause-action {
        ${renderThemeProperty('trackButtonsColorHover', 'color', true)};
      }
    }
    span {
      ${renderThemeProperty('primaryTextColor', 'color', true)};
      ${renderThemeProperty('secondaryTextSize', 'font-size', true)};
      vertical-align: middle;
    }
    .play-label{
      margin-left: 0.5rem;
    }
  }
  .top-row-items {
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    & > div:last-child {
      text-align: right;
      line-height: 100%;
      margin-right: 1rem;
      span {
        vertical-align: middle;
      }
    }
  }
  .track-count {
    ${renderThemeProperty('secondaryTextColor', 'color', true)};
    ${renderThemeProperty('secondaryTextSize', 'font-size', true)};
  }
  .track-number {
    ${renderThemeProperty('secondaryTextSize', 'font-size', true)};
    ${renderThemeProperty('secondaryTextColor', 'color', true)};
    width: 1rem;
  }
  .title-artist span {
    ${renderThemeProperty('primaryTextColor', 'color', true)};
    ${renderThemeProperty('primaryTextSize', 'font-size', true)};
  }
  .title-artist p {
    ${renderThemeProperty('secondaryTextColor', 'color', true)};
    ${renderThemeProperty('secondaryTextSize', 'font-size', true)};
  }
  .title-artist {
    ${_ => applyMediaQueriesForStyle('display', {xs: 'block', md: 'flex'}, {})};
    span,
    p {
      flex: 1;
      align-self: center;
    }
  }
  .header {
    ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingHeader, theme)};
    display: ${({feature: {showBackButton, showTitleHeader}}: any) =>
      showBackButton || showTitleHeader ? 'flex' : 'none'};
    align-content: center;
    justify-content: space-between;
    align-items: center;
    .place-holder {
      visibility: hidden;
    }
    .back-button,
    .place-holder {
      display: ${({feature: {showBackButton}}: any) => (showBackButton ? 'flex' : 'none')};
    }
    .back-button {
      min-width: auto;
      min-height: auto;
      border-radius: 50%;
      padding: 1rem;
    }
    .back-button svg {
      display: ${({feature: {showBackButton}}: any) => (showBackButton ? 'flex' : 'none')};
      ${renderThemeProperty('backButtonColor', 'fill')};
    }
  }
  .audio-list {
    max-width: 100rem;
    margin-left: auto;
    margin-right: auto;
    > div > div,
    li {
      ${renderThemeProperty('trackBackgroundColor', 'background-color')};
    }

    hr {
      ${renderThemeProperty('trackDividerColor', 'background-color')};
      ${renderThemeProperty('trackDividerWidth', 'height')};
    }
  }

  /* this line controls the display of the title inside the music details component - this also appears in the header of the template */
  ${({feature: {showTitleHeader}}: any) => (showTitleHeader ? showTitleHeaderCSS : showTitlePosterCSS)};

  .title {
    ${({theme}: any) => applyMediaQueriesForStyle('padding', theme.componentTheme.paddingTitle, theme)};
    ${_ => applyMediaQueriesForStyle('text-align', {xs: 'center'}, {})};
    .album-title {
      ${renderThemeProperty('titleTextColor', 'color')};
      ${renderThemeProperty('titleTextSize', 'font-size')};
      ${renderThemeProperty('titleTextWeight', 'font-weight')};
    }
    .subtitle {
      ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.subtitleTextSize, theme)};
      ${renderThemeProperty('subtitleTextColor', 'color')};
      ${renderThemeProperty('subtitleTextWeight', 'font-weight')};
      ${renderThemeProperty('subtitlePadding', 'padding')};
    }

    display: flex;
    flex: 1;
    flex-direction: column;
    justify-content: center;
  }
`;

export default StyledContainer;
