import * as React from 'react';
import TextButton from './TextButton';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

const TextButtonContainer: React.FC<any> = (props: any) => {
  return <TextButton {...props} />;
};

export default withLocaleProps(TextButtonContainer, [
  {localizedHeaderText: 'ifc.TextButton.header.text'},
  {localizedSubHeaderText: 'ifc.TextButton.subheader.text'},
  {localizedButtonText: 'ifc.TextButton.button.text'}
]) as any;
