import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {HorizontalDivider as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {renderThemeProperty, applyMediaQueriesForStyle} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  theme?: any;
}

const Line = styled.div`
  width: 100%;
  border-bottom-style: solid;
  ${renderThemeProperty('thickness', 'border-bottom-width')} ${renderThemeProperty('color', 'border-bottom-color')} ${(
      props: any
    ) => applyMediaQueriesForStyle('padding-top', props.theme.componentTheme.paddingTop, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin-bottom', props.theme.componentTheme.marginBottom, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin-top', props.theme.componentTheme.marginTop, props.theme)};
`;

class HorizontalDivider extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }

  public render() {
    const {componentTheme} = this.props;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <Line />
      </PortalComponent>
    );
  }
}

export default HorizontalDivider;
