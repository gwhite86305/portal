#!/bin/bash
# simple script to deploy a portal zip file from S3 to a host where nginx will serve it

function parse_opts {

    donotmodify=0

    while getopts "d?" opt; do
        case "$opt" in
        d)  donotmodify=1
            ;;
        ?)
            ;;
        esac
    done
}

parse_opts $*

# following can be set
GROUPCODE=${GROUPCODE:-"vstest"}
S3_PORTAL_BUCKET=${S3_PORTAL_BUCKET:-"cloudstore-env-staging"}

PORTAL_HOST=${PORTAL_HOST:-"global-onboard2-dev-portal.cloudstore.aero"}
PORTAL_HOST_SSHUSER=${PORTAL_HOST_SSHUSER:-"ec2-user"}
PORTAL_HOST_SSHPORT=${PORTAL_HOST_SSHPORT:-"47391"}

# for Jenkins we use ssh-agent plugin which assumes keys are in ssh-agent, otherwise specify the ssh key location with PORTAL_HOST_SSHKEY
echo "PORTAL_HOST_SSHKEY ${PORTAL_HOST_SSHKEY}"

if [ -z "${PORTAL_HOST_SSHKEY}" ] ; then
	echo "No PORTAL_HOST_SSHKEY - ssh-agent should have the appropriate keys then loaded"
	SSH_KEY_OPTIONS=""
else
	SSH_KEY_OPTIONS="-i ${PORTAL_HOST_SSHKEY}"
fi
echo "SSH_KEY_OPTIONS ${SSH_KEY_OPTIONS}"

# following depend on above variables
PORTAL_DEPLOY_ROOT=/data/ife/contentserver/${GROUPCODE}/files/ife-portal/
PORTAL_ZIP=$(aws s3 ls s3://${S3_PORTAL_BUCKET}/for-upload/portals/${GROUPCODE}/ | grep -v "zip-meta.json" | sort | tail -n 1 | awk '{print $4}')

echo "Deploying ${GROUPCODE} portal file s3://${S3_PORTAL_BUCKET}/for-upload/portals/${GROUPCODE}/${PORTAL_ZIP} to https://${PORTAL_HOST}:${PORTAL_DEPLOY_ROOT}"

# cp locally then scp portal bundle to /tmp
aws s3 cp s3://${S3_PORTAL_BUCKET}/for-upload/portals/${GROUPCODE}/${PORTAL_ZIP} ./${PORTAL_ZIP}
scp -o "StrictHostKeyChecking no" ${SSH_KEY_OPTIONS} -P ${PORTAL_HOST_SSHPORT} ${PORTAL_ZIP} ${PORTAL_HOST_SSHUSER}@${PORTAL_HOST}:/tmp

# remove portal in case it already exists
ssh -o "StrictHostKeyChecking no" ${SSH_KEY_OPTIONS} -p ${PORTAL_HOST_SSHPORT} ${PORTAL_HOST_SSHUSER}@${PORTAL_HOST} "sudo rm -rf ${PORTAL_DEPLOY_ROOT}"

# unzip to /data/portal/<timestamp>
# note we should change the permissions to be 1313.1313 but if we do then v2 appserver will delete the side-loaded portal
echo "Unzipping portal code to ${PORTAL_DEPLOY_ROOT}"
ssh -o "StrictHostKeyChecking no" ${SSH_KEY_OPTIONS} -p ${PORTAL_HOST_SSHPORT} ${PORTAL_HOST_SSHUSER}@${PORTAL_HOST} "sudo unzip -q /tmp/${PORTAL_ZIP} -d ${PORTAL_DEPLOY_ROOT}"

if [ $donotmodify -eq 0 ]; then
  echo "Modifying portal code to point at ${PORTAL_HOST}"
	ssh -o "StrictHostKeyChecking no" ${SSH_KEY_OPTIONS} -p ${PORTAL_HOST_SSHPORT} ${PORTAL_HOST_SSHUSER}@${PORTAL_HOST} "sudo sed -i 's/global-onboard2-dev.cloudstore.aero/${PORTAL_HOST}/g' ${PORTAL_DEPLOY_ROOT}/main*.js"
fi

rm ${PORTAL_ZIP}

exit $?
