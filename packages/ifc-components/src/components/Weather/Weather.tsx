import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {Weekdays} from 'component-utils/dist/utils/DateUtils';
import {GetTempFC} from 'component-utils/dist/utils/TemperatureConverter';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {Icon} from 'core-components';
import {Weather as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {RouterContext} from 'component-utils/dist/utils/Contexts';

export interface InternalInterface {
  className?: string;
  localizedClickableAreaText?: string;
  dailyForecast: any;
  currentCondition: any;
  cityName: string;
  icons: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${renderThemeProperty('primaryTextColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const ClickableArea = (props: any) => {
  const {clickable, clickableAreaText, onClick} = props;
  const Container = styled.div`
    display: ${props => (clickable ? 'flex' : 'none')};
    height: 47px;
    justify-content: center;
    align-items: center;
    ${renderThemeProperty('clickableAreaBackgroundColor', 'background-color')};
    a {
      ${renderThemeProperty('clickableAreaTextColor', 'color')};
      ${renderThemeProperty('clickableAreaFontSize', 'font-size')};
      position:relative;
      text-decoration: none;
      display: flex;
      flex-grow: 1;
      justify-content: center;
      height: 100%;
      align-items: center;
      i {
        margin-left:20px
      }
    }
  }`;

  return (
    <Container>
      <a onClick={onClick} tabIndex={0}>
        <span> {clickableAreaText} </span>
        <Icon className="header-arrow" feature={{name: 'fa-angle-right'}} />
      </a>
    </Container>
  );
};

const Today = (props: any) => {
  const {clickable, currentCondition, icons, todayText} = props;
  const temp = GetTempFC(currentCondition.temperature);

  const Container = styled.div`
    display: flex;
    height: ${props => (clickable ? 'calc(42% - 27px)' : '42.5%')};
    justify-content: center;
    ${renderThemeProperty('borderColor', 'border-color')};
    border-style: solid;
    border-width: 0px;
    border-left-width: 1px;
    border-right-width: 1px;
    .today-text {
      width: 180px;
      margin-top: ${(props: any) => props.theme.gutter};
      margin-bottom: ${(props: any) => props.theme.gutter};
      text-align: center;
      ${renderThemeProperty('weatherTextColor', 'color')};
      display: flex;
      flex-direction: column;
      justify-content: space-evenly;
    }
    .temp {
      min-width: 170px;
    }
    .temp-f {
      font-size: calc(${(props: any) => props.theme.tertiaryFontSize} * 1.5);
      ${renderThemeProperty('primaryTemperatureTextColor', 'color')};
      ${renderThemeProperty('primaryTemperatureFahrenheitFontWeight', 'font-weight')};
    }
    .temp-c {
      font-size: calc(${(props: any) => props.theme.tertiaryFontSize} * 1.5);
      ${renderThemeProperty('secondaryTemperatureTextColor', 'color')};
      ${renderThemeProperty('primaryTemperatureCelsiusFontWeight', 'font-weight')};
      margin-left: 15px;
    }
    .icon {
      text-align: center;
      display: flex;
      align-items: center;
      img {
        height: 100px;
        ${renderThemeProperty('weatherIconWidthLarge', 'width')};
        ${renderThemeProperty('weatherIconMarginLarge', 'margin')};
      }
    }
  `;

  return (
    <Container>
      <div className="today-text">
        <div>{todayText}</div>
        <div className="temp">
          <span className="temp-f">{temp.F}&deg;F</span>
          <span className="temp-c">{temp.C}&deg;C</span>
        </div>
        <div>{props.cityName}</div>
      </div>
      <div className="icon">
        <img alt={currentCondition.weatherText} src={icons[currentCondition.weatherIcon]} />
      </div>
    </Container>
  );
};

const Forecast = (props: any) => {
  let {clickable, showCurrentCondition, highLowOrientation, flipCelsiusFahrenheit, todayText} = props;
  const Container = styled.div`
    display: flex;
    height: ${props =>
      clickable && showCurrentCondition
        ? 'calc(58% - 20px)'
        : !clickable && showCurrentCondition
        ? '57.5%'
        : !clickable && !showCurrentCondition
        ? '100%'
        : 'calc(100% - 47px)'};
    .week-day {
      flex-grow: 1;
      text-align: center;
      ${renderThemeProperty('borderColor', 'border-color')};
      border-style: solid;
      border-width: 0px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-top-width: 1px;
      padding-top: ${(props: any) => props.theme.gutter};
      padding-bottom: ${(props: any) => props.theme.gutter};
      display: flex;
      flex-direction: column;
      justify-content: center;
      .high-temp-c {
        ${renderThemeProperty('highTempCelsiusTextColor', 'color')};
        ${renderThemeProperty('highTempCelsiusFontWeight', 'font-weight')};
        font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
      }
      .high-temp-f {
        ${renderThemeProperty('highTempFahrenheitTextColor', 'color')};
        ${renderThemeProperty('highTempFahrenheitFontWeight', 'font-weight')};
        font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
      }
      .low-temp-c {
        ${renderThemeProperty('lowTempCelsiusTextColor', 'color')};
        ${renderThemeProperty('lowTempCelsiusFontWeight', 'font-weight')};
        font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
      }
      .low-temp-f {
        ${renderThemeProperty('lowTempFahrenheitTextColor', 'color')};
        ${renderThemeProperty('lowTempFahrenheitFontWeight', 'font-weight')};
        font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
      }
      .high-low-vertical {
        width: 45px;
        display: inline-block;
      }
    }
    .week-day:first-child {
      border-left-width: 1px;
    }
    img {
      width: 50px;
      height: 50px;
      ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.weatherIconMargin, props.theme)};
    }
  `;

  const HighTempHorizontal = (props: any) => {
    let {maxTemp, flipCelsiusFahrenheit} = props;
    return (
      <div>
        {flipCelsiusFahrenheit ? (
          <span>
            <span className="high-temp-c">{maxTemp.C}&deg;C</span> /{' '}
            <span className="high-temp-f">{maxTemp.F}&deg;F</span>
          </span>
        ) : (
          <span>
            <span className="high-temp-f">{maxTemp.F}&deg;F</span> /{' '}
            <span className="high-temp-c">{maxTemp.C}&deg;C</span>
          </span>
        )}
      </div>
    );
  };

  const LowTempHorizontal = (props: any) => {
    let {minTemp, flipCelsiusFahrenheit} = props;
    return (
      <div>
        {flipCelsiusFahrenheit ? (
          <span>
            <span className="low-temp-c">{minTemp.C}&deg;C</span> /{' '}
            <span className="low-temp-f">{minTemp.F}&deg;F</span>
          </span>
        ) : (
          <span>
            <span className="low-temp-f">{minTemp.F}&deg;F</span> /{' '}
            <span className="low-temp-c">{minTemp.C}&deg;C</span>
          </span>
        )}
      </div>
    );
  };

  const HighLowTempsHorizontal = (props: any) => {
    let {maxTemp, minTemp, flipCelsiusFahrenheit} = props;
    return (
      <React.Fragment>
        <HighTempHorizontal maxTemp={maxTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
        <LowTempHorizontal minTemp={minTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
      </React.Fragment>
    );
  };

  const HighTempVertical = (props: any) => {
    let {maxTemp, flipCelsiusFahrenheit} = props;
    return (
      <React.Fragment>
        {flipCelsiusFahrenheit ? (
          <span className="high-low-vertical">
            <span className="high-temp-c">{maxTemp.C}&deg;C</span>
            <br />
            <span className="high-temp-f">{maxTemp.F}&deg;F</span>
          </span>
        ) : (
          <span className="high-low-vertical">
            <span className="high-temp-f">{maxTemp.F}&deg;F</span>
            <br />
            <span className="high-temp-c">{maxTemp.C}&deg;C</span>
          </span>
        )}
      </React.Fragment>
    );
  };

  const LowTempVertical = (props: any) => {
    let {minTemp, flipCelsiusFahrenheit} = props;
    return (
      <React.Fragment>
        {flipCelsiusFahrenheit ? (
          <span className="high-low-vertical">
            <span className="low-temp-c">{minTemp.C}&deg;C</span>
            <br />
            <span className="low-temp-f">{minTemp.F}&deg;F</span>
          </span>
        ) : (
          <span className="high-low-vertical">
            <span className="low-temp-f">{minTemp.F}&deg;F</span>
            <br />
            <span className="low-temp-c">{minTemp.C}&deg;C</span>
          </span>
        )}
      </React.Fragment>
    );
  };

  const HighLowTempsVertical = (props: any) => {
    let {maxTemp, minTemp} = props;
    return (
      <div>
        <HighTempVertical maxTemp={maxTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
        <LowTempVertical minTemp={minTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
      </div>
    );
  };

  const Day = (props: any) => {
    let {forecast, icons, flipCelsiusFahrenheit, todayText} = props;
    const minTemp = GetTempFC(forecast.temperature.minimum.value);
    const maxTemp = GetTempFC(forecast.temperature.maximum.value);

    let forecastDate = 'Forecast';
    if (forecast.date === 'Today') {
      forecastDate = 'Today';
    } else if (forecast.date) {
      forecastDate = Weekdays[new Date(forecast.date).getDay()];
    }
    return (
      <div className="week-day">
        <div>{forecastDate}</div>
        <div>
          <img alt={forecast.day.iconPhrase} src={icons[forecast.day.icon]} />
        </div>
        {highLowOrientation == 'vertical' ? (
          <HighLowTempsVertical maxTemp={maxTemp} minTemp={minTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
        ) : (
          <HighLowTempsHorizontal maxTemp={maxTemp} minTemp={minTemp} flipCelsiusFahrenheit={flipCelsiusFahrenheit} />
        )}
      </div>
    );
  };
  return (
    <Container>
      {props.dailyForecast.map((forecast: any, key: any) => {
        return (
          <Day
            key={key}
            forecast={forecast}
            icons={props.icons}
            highLowOrientation={highLowOrientation}
            flipCelsiusFahrenheit={flipCelsiusFahrenheit}
            todayText={todayText}
          />
        );
      })}
    </Container>
  );
};

class Weather extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    cityName: '---',
    currentCondition: {
      temperature: '---',
      weatherIcon: 2,
      weatherText: '---'
    },
    dailyForecast: [
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516798800,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-24T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516885200,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-25T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516971600,
        night: {iconPhrase: '---', icon: 12},
        day: {iconPhrase: '---', icon: 18},
        date: '2018-01-26T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1517058000,
        night: {iconPhrase: '---', icon: 36},
        day: {iconPhrase: '---', icon: 12},
        date: '2018-01-27T07:00:00-06:00'
      }
    ],
    icons: {
      '1': './mocks/images/Weather_1Sunny_50x50.svg',
      '2': './mocks/images/Weather_2MostlySunny_50x50.svg',
      '3': './mocks/images/Weather_3PartlyCloudyDay_50x50.svg',
      '4': './mocks/images/Weather_4IntermittentCloudsDay_50x50.svg',
      '5': './mocks/images/Weather_5HazyDay_50x50.svg',
      '6': './mocks/images/Weather_6MostlyCloudyDay_50x50.svg',
      '7': './mocks/images/Weather_7PartlyCloudy_50x50.svg',
      '8': './mocks/images/Weather_8PartlyCloudyAndHazy_50x50.svg',
      '11': './mocks/images/Weather_11Fog_50x50.svg',
      '12': './mocks/images/Weather_12PartlyCloudyShowers_50x50.svg',
      '13': './mocks/images/Weather_13MostlyCloudyRain_50x50.svg',
      '14': './mocks/images/Weather_14PartlyCloudyRainDay_50x50.svg',
      '15': './mocks/images/Weather_15PartlyCloudyThunderstorms_50x50.svg',
      '16': './mocks/images/Weather_16MostlyCloudyThunderstorms_50x50.svg',
      '17': './mocks/images/Weather_17PartlyCloudyThunderstormsDay_50x50.svg',
      '18': './mocks/images/Weather_18PartlyCloudyRain_50x50.svg',
      '19': './mocks/images/Weather_19PartlyCloudyFlurries_50x50.svg',
      '20': './mocks/images/Weather_20MostlyCloudyFlurries_50x50.svg',
      '21': './mocks/images/Weather_21PartlyCloudyFlurriesDay_50x50.svg',
      '22': './mocks/images/Weather_22PartlyCloudySnow_50x50.svg',
      '23': './mocks/images/Weather_23MostlyCloudySnow_50x50.svg',
      '24': './mocks/images/Weather_24PartlyCloudyIce_50x50.svg',
      '25': './mocks/images/Weather_25PartlyCloudySleet_50x50.svg',
      '26': './mocks/images/Weather_26PartlyCloudyFreesingRain_50x50.svg',
      '29': './mocks/images/Weather_29PartlyCloudyRainAndSnow_50x50.svg',
      '30': './mocks/images/Weather_30HotNoSun_50x50.svg',
      '31': './mocks/images/Weather_31ColdNoSnowFlake_50x50.svg',
      '32': './mocks/images/Weather_32Windy_50x50.svg',
      '33': './mocks/images/Weather_33ClearNight_50x50.svg',
      '34': './mocks/images/Weather_34MostlyClearNight_50x50.svg',
      '35': './mocks/images/Weather_35PartlyCloudyNight_50x50.svg',
      '36': './mocks/images/Weather_36IntermittentCloudsNight_50x50.svg',
      '37': './mocks/images/Weather_37HazyNight_50x50.svg',
      '38': './mocks/images/Weather_38MostlyCloudyNight_50x50.svg',
      '39': './mocks/images/Weather_39PartlyCloudyRainNight_50x50.svg',
      '40': './mocks/images/Weather_40MostlyCloudyRainNight_50x50.svg',
      '41': './mocks/images/Weather_41PartlyCloudyThunderstormsNight_50x50.svg',
      '42': './mocks/images/Weather_42MostlyCloudyThunderstormsNight_50x50.svg',
      '43': './mocks/images/Weather_43MostlyCloudySnowDay_50x50.svg',
      '44': './mocks/images/Weather_44MostlyCloudySnowNight_50x50.svg'
    }
  };

  public static contextType = RouterContext;

  private _handleClick() {
    const {openPageOnClick} = this.props.feature;
    this.context.history.push('/' + openPageOnClick);
  }

  public render() {
    // TODO(wmoore): The icons need to be removed from the layout.JSON files, but are there for now so the PFE conversion will work.
    const {dailyForecast, currentCondition, cityName, icons, localizedClickableAreaText} = this.props;
    const {clickable} = this.props.feature;
    let newForecast = dailyForecast;

    if (this.props.feature.showCurrentCondition) {
      newForecast = newForecast.slice(1, newForecast.length);
    } else {
      newForecast = newForecast.slice(0, newForecast.length - 1);
      newForecast[0].date = 'Today';
    }
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-weather-component">
          <ClickableArea
            clickable={clickable}
            clickableAreaText={localizedClickableAreaText}
            onClick={this._handleClick.bind(this)}
          />
          {this.props.feature.showCurrentCondition ? (
            <Today clickable={clickable} currentCondition={currentCondition} cityName={cityName} icons={icons} />
          ) : null}
          <Forecast
            clickable={clickable}
            showCurrentCondition={this.props.feature.showCurrentCondition}
            highLowOrientation={this.props.feature.highLowOrientation}
            flipCelsiusFahrenheit={this.props.feature.flipCelsiusFahrenheit}
            dailyForecast={newForecast}
            icons={icons}
          />
        </Container>
      </PortalComponent>
    );
  }
}

export default Weather;
