const LOCALE_DEFAULTS = 'en';

/*
 * Webpack will bundle all requires it sees in advance (ignoring code flow), ensuring a bundle exists for each require in dist,
 * only on runtime will code flow become relevant, and appropriate bundle be loaded. Hence the need to explicitly state the locale in string for pre-processing (and no rely on run-time)
 *
 * Yes, the massive amount of duplicate code sucks, but this inevitably be refactored out when local functionality is implemented for real (coming soon)
 *
 * */

import reduce from 'lodash-es/reduce';
import extend from 'lodash-es/extend';

// bundling most common for now
// https://www.science.co.il/language/Locale-codes.php

const localeDataBundles = {
  en: () => require('./loader/webpackLocaleBundler?locale=en!'),
  it: () => require('./loader/webpackLocaleBundler?locale=it!'),
  es: () => require('./loader/webpackLocaleBundler?locale=es!'),
  de: () => require('./loader/webpackLocaleBundler?locale=de!'),
  ar: () => require('./loader/webpackLocaleBundler?locale=ar!'),
  he: () => require('./loader/webpackLocaleBundler?locale=he!'),
  fr: () => require('./loader/webpackLocaleBundler?locale=fr!'),
  ja: () => require('./loader/webpackLocaleBundler?locale=ja!'),
  zh: () => require('./loader/webpackLocaleBundler?locale=zh!'),
  ko: () => require('./loader/webpackLocaleBundler?locale=ko!'),
  pt: () => require('./loader/webpackLocaleBundler?locale=pt!'),
  ru: () => require('./loader/webpackLocaleBundler?locale=ru!'),
  el: () => require('./loader/webpackLocaleBundler?locale=el!')
};

const loader = {
  loadDefaultMessages: () => {
    return new Promise(resolve => {
      require.ensure(
        [
          'core-components/dist/locale/default-locale-messages.json',
          'ife-components/dist/locale/default-locale-messages.json',
          'ifc-components/dist/locale/default-locale-messages.json'
        ],
        require => {
          Promise.all([
            require('core-components/dist/locale/default-locale-messages.json'),
            require('ife-components/dist/locale/default-locale-messages.json'),
            require('ifc-components/dist/locale/default-locale-messages.json')
          ]).then((locales: Array<{}>) => {
            resolve(reduce(locales, extend));
          });
        },
        undefined,
        'locale-default-messages'
      );
    });
  },

  loadData: (locale: string) => {
    const localeId = locale.split('-')[0];

    return new Promise(resolve => {
      if (localeDataBundles[localeId]) {
        localeDataBundles[localeId]()(resolve);
      } else {
        console.error(localeId, ' not supported, defaulting to: ' + LOCALE_DEFAULTS);
        localeDataBundles[LOCALE_DEFAULTS]()(resolve);
      }
    });
  }
};

export default loader;
