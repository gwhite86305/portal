import debounce from 'lodash-es/debounce';
import * as React from 'react';
import {GridItemComponentName} from '../../GridItem/GridItem';
import BuilderComponentOverlay, {InsertPosition} from './BuilderComponentOverlay';
import {TEMPLATE_CONTENT_UID} from 'component-utils/dist/types/Constants';
import AppContainer from '../../AppContainer';
import LayoutInterface from 'component-utils/dist/types/LayoutInterface';
import {Component} from 'component-utils/dist/types/Component';

const SCROLLTO_COMPONENT_OFFSET = 25;
const RESIZE_DEBOUNCE_TIME = 300;
const RERENDER_TIMEOUT = 300;
const BUILDER_OVERLAY_IDENTIFIER = 'data-builder-component-overlay';

let _debounceFn: any;
let _isComponentSelectedFromAppClick = false; // as opposed to builder originated

const closestComponentElement = (el: any, dataAttr: string) => {
  const isOverlayEl =
    el.getAttribute(BUILDER_OVERLAY_IDENTIFIER) === 'true' ||
    el.parentNode.getAttribute(BUILDER_OVERLAY_IDENTIFIER) === 'true';

  if (isOverlayEl) {
    return null;
  }

  while (!el.getAttribute(dataAttr)) {
    // Increment the loop to the parent node
    el = el.parentNode;
    if (!el) {
      return null;
    }
  }

  return {el, componentUid: el ? el.getAttribute(dataAttr) : null};
};

function closestTagNameElement(el: any, tagName: string) {
  if (el.tagName === tagName) {
    return el;
  }

  while (el.parentNode) {
    el = el.parentNode;
    if (el.tagName === tagName) {
      return el;
    }
  }
  return null;
}

export interface BuilderEventListenerProps {
  componentSelected: string;
  layout: LayoutInterface;
  userSelectedLocale: string;
  children?: any;

  onComponentSelected(componentUid: string | null): void;

  onComponentKeyPress(event: any): void;

  onComponentInsert(componentUid: string, position: InsertPosition): void;
}

export interface BuilderEventListenerState {
  boundingRect: ClientRect | null;
  selectedComponent: Component | null;
  selectedComponentUid: string | null;
  childGridBoundingRects: ClientRect[];
  yScrollPosition: number;
}

class BuilderEventListener extends React.Component<BuilderEventListenerProps, BuilderEventListenerState> {
  private _domMutationObserver: any = null;

  constructor(props: BuilderEventListenerProps) {
    super(props);

    this._performResizeCheck = this._performResizeCheck.bind(this);

    this.state = {
      boundingRect: null,
      selectedComponent: null,
      selectedComponentUid: null,
      childGridBoundingRects: [],
      yScrollPosition: 0
    };

    this._domMutationObserver = new MutationObserver(mutations => {
      // this may execute quite a lot for body based events - but its still far faster than depending on layout comparisons
      // to achieve the same outcome
      if (this.props.componentSelected) {
        _isComponentSelectedFromAppClick = true;
        this._performSelection(this.props.componentSelected, true);
      }
    });
  }

  public componentWillReceiveProps(newProps: BuilderEventListenerProps) {
    if (this.props.componentSelected !== newProps.componentSelected) {
      this._performSelection(newProps.componentSelected);
    }
  }

  public componentDidMount() {
    _debounceFn = debounce(this._performResizeCheck, RESIZE_DEBOUNCE_TIME);
    window.addEventListener('resize', _debounceFn);
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', _debounceFn);
  }

  private _performResizeCheck() {
    const {selectedComponentUid} = this.state;
    if (selectedComponentUid) {
      this._performSelection(selectedComponentUid, true);
      setTimeout(() => {
        this._performSelection(selectedComponentUid, true);
      }, RERENDER_TIMEOUT); // perform again - sometimes browser can't keep up
    }
  }

  private _setupMutationListeners(selectedComponentEl: HTMLElement) {
    this._disconnectMutationObserver();

    // listen for all the details for this component
    this._domMutationObserver.observe(selectedComponentEl, {
      subtree: true,
      childList: true,
      characterData: true,
      attributes: true
    });

    // listen for more abstract changes on the document (such as new components appearing etc)
    this._domMutationObserver.observe(document.body, {
      subtree: true,
      childList: true
    });
  }

  private _handleSelectComponentClick(e: React.MouseEvent) {
    if (e.ctrlKey || e.metaKey) {
      // prevent A opening in new window
      if (closestTagNameElement(e.target, 'A')) {
        e.preventDefault();
      }

      e.stopPropagation();

      const closestComponent = closestComponentElement(e.target, 'data-component-guid');

      if (closestComponent) {
        _isComponentSelectedFromAppClick = true;

        const {componentUid} = closestComponent;
        if (componentUid !== this.state.selectedComponentUid) {
          this.props.onComponentSelected(componentUid);
        } else {
          this._triggerComponentUnselected();
        }
      } else {
        this._triggerComponentUnselected();
      }
    }
  }

  private _performSelection(componentUid: string, force?: boolean) {
    const selectedComponentEl = document.querySelectorAll("[data-component-guid='" + componentUid + "']")[0];

    if (componentUid === null) {
      this._clearSelection();
    } else if (selectedComponentEl && componentUid) {
      if (force || componentUid !== this.state.selectedComponentUid) {
        if (componentUid !== this.state.selectedComponentUid) {
          this._setupMutationListeners(selectedComponentEl as HTMLElement);
        }

        const {layout} = this.props;
        const component = layout.components[componentUid];
        const {children} = component.properties;
        let childGridBoundingRects: ClientRect[] = [];

        if (children && children.length) {
          childGridBoundingRects = children
            .filter((childComponentUid: string) => {
              if (childComponentUid !== TEMPLATE_CONTENT_UID) {
                const childComponent = layout.components[childComponentUid];
                return childComponent.componentName === GridItemComponentName;
              } else {
                return false;
              }
            })
            .map((childGridComponentUid: string) => {
              const gridComponentEl = document.querySelectorAll(
                "[data-component-guid='" + childGridComponentUid + "']"
              )[0];
              return gridComponentEl.getBoundingClientRect();
            });
        }

        const boundingRect = selectedComponentEl.getBoundingClientRect();

        this.setState(
          {
            boundingRect,
            selectedComponent: component,
            selectedComponentUid: componentUid,
            childGridBoundingRects,
            yScrollPosition: window.scrollY || window.pageYOffset || document.body.scrollTop
          },
          () => {
            if (!_isComponentSelectedFromAppClick) {
              setTimeout(() => {
                const bodyTop = document.body.getBoundingClientRect().top;
                window.scrollTo(0, boundingRect.top - bodyTop - SCROLLTO_COMPONENT_OFFSET);
              }, 100);
            }
            _isComponentSelectedFromAppClick = false;
          }
        );
      } else {
        this._clearSelection();
      }
    } else {
      console.info('No component found for selection of this uid: ' + componentUid);
    }
  }

  private _triggerComponentUnselected() {
    this.props.onComponentSelected(null);
    this._disconnectMutationObserver();
  }

  private _clearSelection() {
    this.setState({
      boundingRect: null,
      selectedComponent: null,
      selectedComponentUid: null,
      childGridBoundingRects: []
    });
  }

  private _disconnectMutationObserver() {
    this._domMutationObserver.disconnect();
  }

  private _handleKeyPress(event) {
    const {ctrlKey, metaKey, which, shiftKey, altKey} = event;
    let charCode = String.fromCharCode(which).toLowerCase();
    let isCtrlKey = ctrlKey || metaKey;
    if (isCtrlKey && charCode === 's') {
      event.preventDefault();
    }
    this.props.onComponentKeyPress({charCode, isCtrlKey, event: {ctrlKey, metaKey, which, shiftKey, altKey}});
  }

  public render() {
    const {selectedComponentUid, selectedComponent, childGridBoundingRects, yScrollPosition} = this.state;

    let overlayComponents: React.ReactNode = null;

    if (selectedComponent && selectedComponentUid) {
      overlayComponents = [
        <BuilderComponentOverlay
          key={'masterOverlay'}
          yScrollPosition={yScrollPosition}
          boundingRect={this.state.boundingRect}
          component={selectedComponent}
          componentUid={selectedComponentUid}
          hasInnerOverlay={childGridBoundingRects.length > 0}
          onComponentInsert={(position: InsertPosition) => {
            this.props.onComponentInsert(selectedComponentUid, position);
          }}
        />,
        childGridBoundingRects.map((boundingRect: ClientRect, index: number) => (
          <BuilderComponentOverlay
            key={index}
            boundingRect={boundingRect}
            yScrollPosition={yScrollPosition}
            onComponentInsert={() => {}}
          />
        ))
      ];
    }

    return (
      <AppContainer
        tabIndex={0}
        onClickCapture={this._handleSelectComponentClick.bind(this)}
        onKeyDown={this._handleKeyPress.bind(this)}
      >
        {overlayComponents}
        {this.props.children}
      </AppContainer>
    );
  }
}

export default BuilderEventListener;
