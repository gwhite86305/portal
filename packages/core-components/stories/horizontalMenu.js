import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, object, text, boolean, array} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import MockRouter from 'react-mock-router';

import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';
import {HorizontalMenu} from '../dist/components/HorizontalMenu/HorizontalMenu';

const componentTheme = {
  backgroundColor: '#fff',
  dividerColor: 'blue',
  fontSize: '2rem',
  fontWeight: '100',
  height: {
    lg: '4rem',
    md: '4rem',
    sm: '4rem',
    xl: '4rem',
    xs: '4rem'
  },
  justify: 'center',
  margin: {
    lg: '0',
    md: '0',
    sm: '0',
    xl: '0',
    xs: '0'
  },
  padding: {
    lg: '1rem 5rem 1rem 5rem',
    md: '1rem 5rem 1rem 5rem',
    sm: '1rem 5rem 1rem 5rem',
    xl: '1rem 5rem 1rem 5rem',
    xs: '1rem 5rem 1rem 5rem'
  },
  spaceBetweenElement: {
    lg: '1rem',
    md: '1rem',
    sm: '1rem',
    xl: '1rem',
    xs: '1rem'
  },
  textColor: '#000',
  textFocusActiveColor: 'blue',
  textHoverColor: 'red'
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";
const stories = storiesOf('HorizontalMenu', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);

stories.add('Default', () => {
  const localTheme = object('componentTheme', componentTheme);
  const hoverEnabled = true;
  const dividerEnabled = true;
  const menuItems = [
    {
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'one', iconAltText: ''}},
      label: 'one', iconAltText: ''
    },
    {
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'two', iconAltText: ''}},
      label: 'two', iconAltText: ''
    },
    {
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'three', iconAltText: ''}},
      label: 'three', iconAltText: ''
    }
  ];

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <HorizontalMenu
          feature={object('feature', {
            dividerEnabled: dividerEnabled,
            hoverEnabled: hoverEnabled,
            menuItems: menuItems
          })}
          componentTheme={localTheme}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('With Some Theme Changed', () => {
  const localTheme = object('componentTheme', {
    ...componentTheme,
    backgroundColor: '#BED733',
    dividerColor: '#013064',
    textColor: '#013064',
    textHoverColor: 'white'
  });
  const hoverEnabled = true;
  const dividerEnabled = true;
  const menuItems = [
    {
      label: 'one',
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'one', iconAltText: ''}},
      label: 'one', iconAltText: ''
    },
    {
      label: 'two',
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'two', iconAltText: ''}},
      label: 'two', iconAltText: ''
    },
    {
      label: 'three',
      url: 'viasat.com',
      iconUrl: '',
      labels: {en: {label: 'three', iconAltText: ''}},
      label: 'three', iconAltText: ''
    }
  ];

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <HorizontalMenu
          feature={object('feature', {
            dividerEnabled: dividerEnabled,
            hoverEnabled: hoverEnabled,
            menuItems: menuItems
          })}
          componentTheme={localTheme}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('With Some Icons', () => {
  const localTheme = object('componentTheme', {
    ...componentTheme,
    backgroundColor: '#BED733',
    dividerColor: '#013064',
    textColor: '#013064',
    textHoverColor: 'white'
  });
  const hoverEnabled = true;
  const dividerEnabled = true;
  const menuItems = [
    {
      label: 'one',
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {label: 'one', iconAltText: ''}},
      label: 'one', iconAltText: ''
    },
    {
      label: 'two',
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {label: 'two', iconAltText: ''}},
      label: 'two', iconAltText: ''
    },
    {
      label: 'three',
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {label: 'three', iconAltText: ''}},
      label: 'three', iconAltText: ''
    }
  ];

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <HorizontalMenu
          feature={object('feature', {
            dividerEnabled: dividerEnabled,
            hoverEnabled: hoverEnabled,
            menuItems: menuItems
          })}
          componentTheme={localTheme}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Icons Only', () => {
  const localTheme = object('componentTheme', {
    ...componentTheme,
    backgroundColor: '#BED733',
    dividerColor: '#013064',
    textColor: '#013064',
    primaryButtonHoverColor: 'white'
  });
  const hoverEnabled = true;
  const dividerEnabled = true;
  const menuItems = [
    {
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {iconAltText: ''}},
      iconAltText: ''
    },
    {
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {iconAltText: ''}},
      iconAltText: ''
    },
    {
      url: 'viasat.com',
      iconUrl: './images/arrow-right-circle-simple-line-icons.svg',
      iconHeight: 30,
      labels: {en: {iconAltText: ''}},
      iconAltText: ''
    }
  ];

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <HorizontalMenu
          feature={object('feature', {
            dividerEnabled: dividerEnabled,
            hoverEnabled: hoverEnabled,
            menuItems: menuItems
          })}
          componentTheme={localTheme}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});
