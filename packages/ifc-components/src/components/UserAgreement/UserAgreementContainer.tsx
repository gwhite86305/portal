import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {UserAgreement as ExternalInterface} from '../common/ComponentsInterface';
import UserAgreement, {InternalInterface} from './UserAgreement';
import {getAcceptanceRecord, saveAcceptanceRecord, sendDefaultSignup} from '../../store/UserAgreement/actions';
import {withRouter} from 'react-router';
import {pathOr} from 'ramda';
import {withContent} from 'component-utils/dist/components/ContentAsset';
import {getDefaultTier, getPremiumState, getDefaultState, getFreeTier} from '../../utils/ConnectionUtils';
export type PageProps = DispatchProps & ExternalInterface & InternalInterface;
import {getUserAgreementHash} from '../../utils/UserAgreementUtils';

export interface DispatchProps {
  onAccept(): any;
  onLoad(): any;
  sendSignup(tierId: number): any;
}

@connectWrapper(
  (state: any, ownProps: PageProps) => ({
    accepted: state.userAgreement.accepted,
    hash: state.userAgreement.hash,
    builderSelectedComponentUid: state.ux.builderSelectedComponentUid,
    profile: state.profile,
    device: state.device
  }),
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onAccept: () => {
      ownProps.history.push('/' + ownProps.feature.openPageOnAccept);
      dispatch(saveAcceptanceRecord(true, ownProps));
    },
    onLoad: () => {
      dispatch(getAcceptanceRecord());
    },
    sendSignup: tierId => {
      dispatch(sendDefaultSignup(tierId));
    }
  })
)
class UserAgreementContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
    const isUAAccepted = localStorage.getItem('ua_accepted');
    if (this.props.accepted && this.props.device && isUAAccepted) {
      this.props.history.push('/' + this.props.feature.openPageOnAccept);
    }
    this.props.onLoad();
  }
  public sendSignup() {
    let profile = this.props.profile;
    let tier = this.props.feature.freeSignup ? getFreeTier(profile) : getDefaultTier(profile);
    if (tier) {
      this.props.sendSignup(tier.id);
    }
  }
  public componentDidUpdate() {
    let profile = this.props.profile;
    let device = this.props.device;
    const doSignup =
      this.props.accepted &&
      device &&
      profile &&
      profile.locations &&
      this.props.hash === getUserAgreementHash(this.props, {profile: profile});
    if (doSignup) {
      if (
        pathOr(0, ['serviceTiers', 'length'], profile) > 0 &&
        !getPremiumState(device, profile) &&
        !getDefaultState(device, profile)
      ) {
        this.sendSignup();
      }
    }
  }
  public render() {
    if (this.props.accepted && this.props.feature.redirectIfAlreadyAccepted) {
      this.props.history.push('/' + this.props.feature.openPageOnAccept);
    }
    let onButtonClick = () => {
      this.props.onAccept();
      this.sendSignup();
    };
    const isBuilderSelected = this.props.builderSelectedComponentUid === this.props.componentUid;
    const showUA =
      isBuilderSelected ||
      (!this.props.accepted ||
        (this.props.profile &&
          this.props.profile.locations &&
          this.props.hash !== getUserAgreementHash(this.props, {profile: this.props.profile})));
    return showUA ? <UserAgreement {...this.props} buttonClickHook={onButtonClick} /> : null;
  }
}

export default withRouter(
  withContent(
    withLocaleProps(UserAgreementContainer, [
      {localizedAltImageText: 'core.UserAgreement.logoImage.altText'},
      {localizedBackText: 'core.UserAgreement.backText'}
    ]),
    [{contentKey: 'UserAgreement'}]
  )
);
