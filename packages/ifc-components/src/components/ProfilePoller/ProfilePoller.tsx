import * as React from 'react';
import {utils} from 'component-utils';
const connectWrapper = utils.connectWrapper.default;
import * as defaults from './defaultProps.json';
import {fetchProfile} from '../../store/profile/actions';
import {Poller} from 'core-components';

export interface InternalInterface {
  url: string;
  className?: string;
}

export interface DispatchProps {
  fetchProfile(url: string): any;
}

@connectWrapper(
  (state: any, ownProps: InternalInterface) => {
    const fromState: Partial<InternalInterface> = {};
    if (state && state.ux && state.ux.settings && state.ux.settings.ifc) {
      if (state.ux.settings.ifc.profileUrl) fromState.url = state.ux.settings.ifc.profileUrl;
    }
    return {...ownProps, ...fromState};
  },
  (dispatch: any, ownProps: InternalInterface): DispatchProps => ({
    fetchProfile: (url: string) => {
      dispatch(fetchProfile(url));
    }
  })
)
export default class ProfilePoller extends React.Component<InternalInterface, any> {
  public render() {
    return (
      <Poller
        action={() => (this.props as any).fetchProfile(this.props.url)}
        interval={0}
        children={this.props.children}
      />
    );
  }
}
