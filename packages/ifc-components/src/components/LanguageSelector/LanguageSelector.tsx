import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {LanguageSelector as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';

export interface InternalInterface {
  className?: string;
  onClick?: any;
}

class LanguageSelector extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  public render() {
    const Container = styled(CommonComponentThemeWrapper)`
      display: flex;
      cursor: pointer;
      ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme, true)};
      ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme, true)};
    ` as any;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="LanguageSelector-component" onClick={this.props.onClick}>
          {' '}
          {this.props.children}{' '}
        </Container>
      </PortalComponent>
    );
  }
}

export default LanguageSelector;
