import {find} from 'lodash';
import {difference, head, keys, map, mergeAll, pathOr, pickAll, pipe, values} from 'ramda';
import {ImageVariants, ImageVariantsType} from '../types';
import {Price} from './PaidContent/types';

export interface PaidContentProps {
  label: string;
  price: Price;
  onPaid: any;
  entry: {
    itemId: string;
    itemType: string;
  };
  onPurchaseSuccess: any;
  className: string;
}

export interface PaidContentState {
  modalOpen: boolean;
  processing: boolean;
  modalState: {
    seat: string;
    seatDirty: boolean;
    voucherCode: string;
    voucherError: string | undefined;
    modalSection: number;
    paid: boolean;
    successMessage: string;
  };
}

export const getSmallestAndBiggestImg = (titleImg: any, galleryImg: any, imgPlaceholder: string): string[] => {
  const collection = pathOr(pathOr([], ['0'], titleImg), ['0'], galleryImg);
  return pipe(
    keys,
    values => [head(values), values[values.length - 1]],
    map(
      pipe(
        (el: any) => collection[el],
        pathOr(imgPlaceholder, ['url'])
      )
    )
  )(collection) as string[];
};

export const shouldCatalogContainerUpdate = (nextProps: any, prevProps: any, propsKey: string[]) => {
  const pickProps = pickAll(propsKey);
  const newProp = mergeAll([nextProps.feature, nextProps.componentTheme, pickProps(nextProps.storeState)]);
  const oldProp = mergeAll([prevProps.feature, prevProps.componentTheme, pickProps(prevProps.storeState)]);
  return (
    Boolean(difference(values(newProp as any), values(oldProp as any)).length) ||
    nextProps.storeState.items.length !== prevProps.storeState.items.length
  );
};

export const getImageSrcByRatio = (
  images: ImageVariants,
  variantName: ImageVariantsType,
  pixelRatio: number
): string => {
  const roundedRatio = Math.ceil(pixelRatio);
  const imageVariantKey = `${variantName}${roundedRatio > 3 ? 3 : roundedRatio}x`;
  const imageVariant = images[imageVariantKey] || images.original;
  return imageVariant.url;
};

// We use getOptimizedImgSrc only if we know image size
export const getOptimizedImgSrc = ({
  images,
  variant,
  imageWidth,
  pixelRatio = 1
}: {
  images: ImageVariants;
  variant: ImageVariantsType;
  imageWidth?: number;
  pixelRatio?: number;
}): string => {
  if (imageWidth) {
    const sizeVariants = ['1x', '2x', '3x']
      .map(size => images[`${variant}${size}`])
      .filter(sizeVariant => !!sizeVariant);

    const matchingVariant =
      find(sizeVariants, sizeVariant => sizeVariant!.width >= imageWidth * pixelRatio) || images.original;

    return matchingVariant.url;
  }

  return getImageSrcByRatio(images, variant, pixelRatio);
};
