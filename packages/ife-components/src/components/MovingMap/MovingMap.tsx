import * as React from 'react';
import * as defaultProps from './defaultProps.json';
import Map = require('pigeon-maps');
import {MovingMapMarkerPlane} from './MovingMapMarkerPlane';
import {MovingMapLine} from './MovingMapLine';
import {MovingMapMarkerCircle} from './MovingMapMarkerCircle';
import {BoundsChangeEvent} from './types';

export interface MovingMapProps {
  zoom: number;
  maxZoom: number;
  minZoom: number;
  center: number[];
  mapUrl: string;
  originPoint: number[];
  destinationPoint: number[];
  trackedRoutePoints: number[][];
  rotation: any;
  position: any;
  animate: boolean;
  zoomSnap: boolean;
  mouseEvents: boolean;
  touchEvents: boolean;
  onAnimationStart: () => void;
  onAnimationFinish: () => void;
  onBoundsChanged: (change: BoundsChangeEvent) => void;
}

const MovingMap: React.FC<MovingMapProps> = props => {
  return (
    <div className="map-wrapper">
      <Map
        animate={props.animate}
        attribution={false}
        zoomSnap={props.zoomSnap}
        maxZoom={props.maxZoom}
        minZoom={props.minZoom}
        center={props.center}
        mouseEvents={props.mouseEvents}
        touchEvents={props.touchEvents}
        onBoundsChanged={({center, initial, zoom, bounds}: any) => {
          props.onBoundsChanged({center, initial, zoom, bounds});
        }}
        onAnimationStart={props.onAnimationStart}
        onAnimationStop={props.onAnimationFinish}
        zoom={props.zoom}
        provider={(x: number, y: number, z: number) => `${props.mapUrl}/${z}/${x}/${y}.png`}
      >
        <MovingMapLine originPoint={props.originPoint} coords={props.trackedRoutePoints} />
        <MovingMapMarkerCircle anchor={props.originPoint} offset={[15, 15]} />
        <MovingMapMarkerCircle anchor={props.destinationPoint} offset={[15, 15]} />
        <MovingMapMarkerPlane rotation={props.rotation} anchor={props.position} offset={[21, 21]} />
      </Map>
    </div>
  );
};

MovingMap.defaultProps = defaultProps as any;

export default MovingMap;
