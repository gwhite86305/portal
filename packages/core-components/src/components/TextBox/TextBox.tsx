import * as defaults from './defaultProps.json';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Button as ExternalInterface} from '../common/ComponentsInterface';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  placeholder?: string;
}

const TextBoxContainer = styled.div`
  margin-top: ${(props: any) => props.theme.gutter};
  margin-bottom: ${(props: any) => props.theme.gutter};
  position: relative;
  .label {
    color: ${(props: any) => props.theme.textBoxPlaceholderColor};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.875);
    display: flex;
    align-items: center;
    pointer-events: none;
    cursor: default;
    word-wrap: break-word;
    margin: 0px;
    position: absolute;
    top: 4px;
    left: 10px;
    .fa-circle {
      color: ${(props: any) => props.theme.formErrorColor};
      font-size: 4px;
      margin-left: 7px;
    }
  }
  .error .label {
    color: ${(props: any) => props.theme.formErrorColor};
  }
  .error .label::before {
    background: url('./mocks/images/input_error.svg') no-repeat;
    width: ${(props: any) => props.theme.primaryFontSize};
    height: ${(props: any) => props.theme.primaryFontSize};
    display: inline-block;
    content: '';
    margin-right: 6px;
  }
  input {
    border-radius: ${(props: any) => props.theme.textBoxBorderRadius};
    color: ${(props: any) => props.theme.textBoxTextColor};
    font-family: ${(props: any) => props.theme.appFontFamily};
    width: 100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    display: block;
    width: 100%;
    height: calc(${(props: any) => props.theme.gutter} * 3.187);
    padding: calc(${(props: any) => props.theme.gutter} * 0.375) calc(${(props: any) => props.theme.gutter} * 0.625);
    padding-top: calc(${(props: any) => props.theme.gutter} * 0.75);
    padding-bottom: 0px;
    font-size: ${(props: any) => props.theme.primaryFontSize};
    border: 1px solid ${(props: any) => props.theme.textBoxBorderColor};
  }
  .error input {
    border: 1px solid ${(props: any) => props.theme.formErrorColor};
  }
  .validation {
    position: relative;
    padding: calc(${(props: any) => props.theme.gutter} * 0.5) calc(${(props: any) => props.theme.gutter} * 0.625)
      calc(${(props: any) => props.theme.gutter} * 0.5) calc(${(props: any) => props.theme.gutter} * 0.625);
    div.opacity-background {
      background-color: ${(props: any) => props.theme.formErrorColor};
      opacity: 0.14;
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      width: 100%;
    }
    p.message {
      color: ${(props: any) => props.theme.primaryTextColor};
      margin: 0px;
      line-height: 1.25;
    }
    div.circle {
      text-align: right;
      height: 0px;
      span {
        position: relative;
        top: calc(${(props: any) => props.theme.gutter} * -4.4);
        background-color: ${(props: any) => props.theme.formErrorColor};
        width: calc(${(props: any) => props.theme.gutter} * 1.375);
        height: calc(${(props: any) => props.theme.gutter} * 1.375);
        line-height: 1.375;
        display: inline-block;
        text-align: center;
        justify-content: center;
        border-radius: calc(${(props: any) => props.theme.gutter} * 1.375);
        color: ${(props: any) => props.theme.tertiaryTextColor};
      }
    }
  }
`;

class TextBox extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
  }
  _handleFormChange(fieldName: string, fieldValue: string) {
    this.props.onChange && this.props.onChange(fieldName, fieldValue);
  }
  public render() {
    const {className, name, type, componentTheme, placeholder, error, required, disabled} = this.props;
    let disableProp: any = {};
    if (disabled) {
      disableProp.disabled = true;
    }
    return (
      <PortalComponent componentTheme={componentTheme}>
        <TextBoxContainer className={this.props.className}>
          <div className={error ? 'error' : ''}>
            <input
              className={'core-text-box-component ' + (className || '')}
              name={name || ''}
              type={type || 'text'}
              onChange={e => this._handleFormChange(name, e.target.value)}
              {...disableProp}
            />
            <div className="label">
              {placeholder} {required ? <i className="fa fa-circle" /> : null}
            </div>
            {error ? (
              <div className="validation">
                {this.props.showErrorCircle ? (
                  <div className="circle">
                    <span>!</span>
                  </div>
                ) : null}
                <p className="message">{error}</p>
              </div>
            ) : null}
          </div>
        </TextBoxContainer>
      </PortalComponent>
    );
  }
}

export default withLocaleProps(TextBox, [{placeholder: 'core.TextBox.defaultText'}]) as any;
