import * as React from 'react';
import {PortalComponent, utils, Currency} from 'component-utils';
import {Button, TextBox, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';
import { pathOr } from 'ramda';
import styled from 'styled-components';
import { validEmail } from 'component-utils/dist/utils/Validator';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  header?: string;
  emailInputLabel?: string;
  confirmEmailInputLabel?: string;
  WifiServiceOptions?: any;
  currencyCode?: string;
  currencySuffix?: any;
  locale?: string;
  tier?: any;
  onPromoValidate?: any;
  onContinue?: any;
  resetPromoAndSignup?: any;
  componentTheme?: any;
  feature?: any;
  promoNote?: string;
  emailNote?: string;
  promoInputLabel?: string;
  removeButtonText?: string;
  applyButtonText?: string;
  continueButtonText?: string;
  errorDict?: any;
  minimumFractionDigits?: string;
  maximumFractionDigits?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('primaryTextColor', 'color')};
  font-family: ${(props: any) => props.theme.primaryFontFamily};

  div.header {
    ${renderThemeProperty('secondaryTextColor', 'color')};
    font-family: ${(props: any) => props.theme.appFontFamily};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.875);
    padding-bottom: ${(props: any) => props.theme.gutter};
    font-weight: lighter;
    margin-right: 30px;
    .price {
      margin-left: calc(${(props: any) => props.theme.gutter} * 2.2);
    }
    .discounted {
      ${renderThemeProperty('discountedPriceTextColor', 'color')};
      text-decoration: line-through;
    }
  }
  div.second-header {
    margin-top: calc(${(props: any) => props.theme.gutter} * 1.875);
  }
  div.note {
    ${renderThemeProperty('primaryTextColor', 'color')};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.937);
    font-family: ${(props: any) => props.theme.appFontFamily};
    margin-bottom: calc(${(props: any) => props.theme.gutter} * 1.5);
  }
  .validation-summary {
    display: flex;
    margin-bottom: ${(props: any) => props.theme.gutter};
    overflow: hidden;
    -webkit-backdrop-filter: blur(10px);
    backdrop-filter: blur(10px);
    box-shadow: 0 2px 4px 0 rgba(54, 73, 90, 0.2);
    border-radius: ${(props: any) => props.theme.textBoxBorderRadius};
    color: ${(props: any) => props.theme.textBoxTextColor};
    font-family: ${(props: any) => props.theme.appFontFamily};
    width: 100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    height: calc(${(props: any) => props.theme.gutter} * 3.562);
    font-size: ${(props: any) => props.theme.primaryFontSize};
    div:first-child {
      background-color: ${(props: any) => props.theme.formErrorColor};
      width: calc(${(props: any) => props.theme.gutter} * 3.562);
      align-items: center;
      justify-content: center;
    }
    div {
      width: calc(100% - ${(props: any) => props.theme.gutter} * 3.562);
      display: flex;
    }
    p {
      margin: auto calc(${(props: any) => props.theme.gutter} * 0.75);
    }
  }
  div.buttons {
    display: flex;
    ${(props: any) =>
      applyMediaQueriesForStyle('flex-direction', {xs: 'column-reverse', sm: 'row-reverse'}, props.theme)};
  }
  div.button-wrap {
    display: flex;
    flex-grow: 1;
    margin-top: calc(${(props: any) => props.theme.gutter} * 1);
    text-align: center;
    button {
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
  div.hidden {
    visibility: hidden;
  }
  div.continue-button {
    ${(props: any) => applyMediaQueriesForStyle('margin-left', {xs: '0px', sm: '12px'}, props.theme)};
    text-align: center;
    button {
      border: 1px solid ${(props: any) => props.theme.secondaryButtonTextColor};
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
  div.apply-button {
    ${(props: any) => applyMediaQueriesForStyle('margin-right', {xs: '0px', sm: '12px'}, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '0px', sm: '16px'}, props.theme)};
    button {
      border: 1px solid ${(props: any) => props.theme.primaryButtonColor};
      color: ${(props: any) => props.theme.primaryButtonColor};
    }
  }
  .disabled {
    opacity: 0.25;
  }
`;

class AaPromo extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      promoCode: '',
      email: '',
      confirm_email: '',
      formErrors: {}
    };
    this._handleAaPromoSubmit = this._handleAaPromoSubmit.bind(this);
    this._handleFormChange = this._handleFormChange.bind(this);
    this._handleContinueClick = this._handleContinueClick.bind(this);
    this._handleRemovePromoClick = this._handleRemovePromoClick.bind(this);
    this.isPromoValid = this.isPromoValid.bind(this);
  }
  isPromoValid(WifiServiceOptions: any) {
    return (
      WifiServiceOptions.voucher &&
      WifiServiceOptions.voucher.body &&
      this.props.WifiServiceOptions.voucher.body.promo_is_valid
    );
  }
  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handleAaPromoSubmit(event: any) {
    let formErrors: any = {};
    event.preventDefault();
    const {promoCode} = this.state;
    if (promoCode) {
      this.props.onPromoValidate(promoCode);
    }
    this.setState({formErrors});
  }

  _handleContinueClick(event: any) {
    event.preventDefault();
    const {errorDict} = this.props;
    let formErrors: any = {};
    const {email, confirm_email} = this.state;
    if (!email) {
      formErrors['email'] = errorDict['errorMessageEmailRequired'];
    }
    if (email && !validEmail(email)) {
      formErrors['email'] = errorDict['errorMessageInvalidEmail'];
    }
    if (!confirm_email) {
      formErrors['confirm_email'] = errorDict['errorMessageConfirmEmailRequired'];
    }
    if (!formErrors['email'] && !formErrors['confirm_email'] && email != confirm_email) {
      formErrors['confirm_email'] = errorDict['emailMessageEmailMismatch'];
    }
    this.setState({formErrors});
    if (Object.keys(formErrors).length === 0) {
      const tierId = this.props.tier.id;
      const {promoCode} = this.state;
      const applyPromo = this.isPromoValid(this.props.WifiServiceOptions);
      this.props.onContinue(promoCode, email, tierId, applyPromo);
    }
  }
  _handleRemovePromoClick(event: any) {
    event.preventDefault();
    this.props.resetPromoAndSignup();
  }
  public render() {
    const {
      header,
      emailInputLabel,
      confirmEmailInputLabel,
      WifiServiceOptions,
      feature,
      tier,
      emailNote,
      promoNote,
      promoInputLabel,
      removeButtonText,
      continueButtonText,
      applyButtonText,
      errorDict,
      currencyCode,
      currencySuffix,
      locale,
      minimumFractionDigits,
      maximumFractionDigits
    } = this.props;
    const formErrors = Object.assign({}, this.state.formErrors);

    let total = tier.cost;
    let voucher: any = {};
    if (WifiServiceOptions && WifiServiceOptions.voucher && WifiServiceOptions.voucher.promoCode) {
      voucher = (WifiServiceOptions && WifiServiceOptions.voucher.body) || {};
      if (WifiServiceOptions && WifiServiceOptions.voucher.statusCode == '200') {
        if (this.isPromoValid(WifiServiceOptions)) {
          let discount = voucher.discount;
          let deduct = 0;
          // As per the Gogo-ASP-APIs_v1.7 clean.pdf
          if (voucher.discount_type == 'percentage') {
            deduct = (total * discount) / 100;
          } else if (voucher.discount_type == 'Amount') {
            deduct = voucher.discount;
          }
          deduct = Math.min(Math.max(0, deduct), voucher.max_discount_amount);
          total = total - deduct;
          // Check for negative total
          total = total < 0 ? 0 : total;
        } else {
          formErrors['promoCode'] = errorDict['errorMessageInvalidPromo'];
        }
      } else {
        formErrors['promoCode'] =
          formErrors['promoCode'] ||
          (WifiServiceOptions.voucher &&
            WifiServiceOptions.voucher.error_code &&
            errorDict[WifiServiceOptions.voucher.errorCode]) ||
          errorDict['generic'];
      }
    }
    let rejectError = '';
    if (WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'reject') {
      rejectError = errorDict[WifiServiceOptions.signup.code] || errorDict['generic'];
    }
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="aa-promo-form">
          {rejectError ? (
            <div className="validation-summary">
              <div>
                <img src={feature.formErrorIcon} />
              </div>
              <div>
                <p>{rejectError}</p>
              </div>
            </div>
          ) : null}
          <form onSubmit={this._handleAaPromoSubmit}>
            <div className="header">
              <span className="header-text">{header}</span>
            </div>
            <div className="note">{emailNote ? <span className="note-text">{emailNote}</span> : null}</div>
            <TextBox
              name="email"
              error={formErrors.email}
              placeholder={emailInputLabel}
              required={true}
              onChange={this._handleFormChange}
            />
            <TextBox
              className="confirm_email"
              name="confirm_email"
              error={formErrors.confirm_email}
              placeholder={confirmEmailInputLabel}
              required={true}
              onChange={this._handleFormChange}
            />
            <div className="header second-header">
              <span className="header-text">{tier && tier.description}</span>
              {voucher.promo_is_valid ? (
                <span className="discounted price">
                  <Currency
                    currency={currencyCode}
                    value={tier.cost}
                    locale={locale}
                    minimumFractionDigits={minimumFractionDigits}
                    maximumFractionDigits={maximumFractionDigits}
                    currencySuffix={currencySuffix}
                  />
                </span>
              ) : null}
              <span className="price">
                <Currency
                  currency={currencyCode}
                  value={total}
                  locale={locale}
                  minimumFractionDigits={minimumFractionDigits}
                  maximumFractionDigits={maximumFractionDigits}
                  currencySuffix={currencySuffix}
                />
              </span>
            </div>
            <div className="note">{promoNote ? <span className="note-text">{promoNote}</span> : null}</div>

            <TextBox
              name="promoCode"
              error={formErrors.promoCode}
              placeholder={promoInputLabel}
              required={false}
              onChange={this._handleFormChange}
              disabled={voucher.promo_is_valid}
            />
            <div className="buttons">
              <div className={'button-wrap continue-button'}>
                <Button isPrimary={true} onClick={this._handleContinueClick} aria-label={continueButtonText}>
                  {continueButtonText}
                </Button>
              </div>

              <div className="button-wrap apply-button">
                <Button
                  isDisabled={!this.state.promoCode}
                  isPrimary={false}
                  onClick={voucher.promo_is_valid ? this._handleRemovePromoClick : this._handleAaPromoSubmit}
                  aria-label={applyButtonText}
                >
                  {voucher.promo_is_valid ? removeButtonText : applyButtonText}
                </Button>
              </div>
            </div>
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default AaPromo;
