import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, object, text, boolean} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import MockRouter from 'react-mock-router';

import Header from '../dist/components/Header/Header';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';


// TODO bref: Temporarily removed - executing these breaks as component hierarchy now has a connected component
// need to mock store - had major difficulty getting this working in storybook - so holding off for now...
// const componentTheme = {
//   airlineLogoHeight: '30px',
//   airlineLogoMargin: {
//     lg: '1rem',
//     md: '1rem',
//     sm: '1rem',
//     xl: '1rem',
//     xs: '1rem'
//   },
//   backgroundColor: '#3232',
//   fontSize: '1rem',
//   fontWeight: '800',
//   hamburgerMenuBackgroundColor: '#202e39',
//   hamburgerMenuCloseBackgroundColor: '#35b2f0',
//   hamburgerMenuTextColor: 'white',
//   horizontalMenuMargin: {
//     lg: '1rem',
//     md: '1rem',
//     sm: '1rem',
//     xl: '1rem',
//     xs: '1rem'
//   },
//   horizontalMenuTextColor: '#202e39',
//   textHoverColor: '#1694CA',
//   textActiveColor: 'yellow'
// };

// mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";

// const stories = storiesOf('Header', module);
// stories.addDecorator(withKnobs);
// stories.addDecorator(checkA11y);

// stories.add('Default', () => {
//   const localTheme = object('Theme', componentTheme);
//   const hamburgerMenuIcon = './images/fa-bars.png';
//   const hamburgerCloseMenuIcon = './images/arrow-right-circle-simple-line-icons.png';
//   const airlineLogo = './images/viasat-logo-final-rgb.png';
//   const menuItems = [
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'one'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'two'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'three'}}}
//   ];
//   return (
//     <MockRouter location="/home">
//       <PortalComponentWrapper mockTheme={mockTheme}>
//         <Header
//           componentTheme={localTheme}
//           feature={object('feature', {
//             airlineLogo: airlineLogo,
//             hamburgerCloseMenuIcon: hamburgerCloseMenuIcon,
//             hamburgerMenuIcon: hamburgerMenuIcon,
//             menuItems: menuItems
//           })}
//         />
//       </PortalComponentWrapper>
//     </MockRouter>
//   );
// });

// stories.add('With Some Theme Changes', () => {
//   const localTheme = object('Theme', componentTheme);
//   const hamburgerMenuIcon = './images/fa-bars.png';
//   const hamburgerCloseMenuIcon = './images/arrow-right-circle-simple-line-icons.png';
//   const airlineLogo = './images/viasat-logo-final-rgb.png';
//   const menuItems = [
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'one'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'two'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'three'}}}
//   ];
//   return (
//     <MockRouter location="/home">
//       <PortalComponentWrapper mockTheme={mockTheme}>
//         <Header
//           feature={object('feature', {
//             airlineLogo: airlineLogo,
//             hamburgerCloseMenuIcon: hamburgerCloseMenuIcon,
//             hamburgerMenuIcon: hamburgerMenuIcon,
//             menuItems: menuItems
//           })}
//           componentTheme={localTheme}
//         />
//       </PortalComponentWrapper>
//     </MockRouter>
//   );
// });

// stories.add('With Quantas Theme', () => {
//   const localTheme = object('Theme', {
//     ...componentTheme,
//     backgroundColor: 'white',
//     hamburgerMenuBackgroundColor: '#96281B',
//     textHoverColor: '#E74C3C',
//     hamburgerMenuCloseBackgroundColor: 'white'
//   });
//   const hamburgerMenuIcon = './images/fa-bars.png';
//   const hamburgerCloseMenuIcon = './images/arrow-right-circle-simple-line-icons.png';
//   const airlineLogo = './images/Quantas.png';
//   const menuItems = [
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'one'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'two'}}},
//     {url: 'viasat.com', iconUrl: './images/fa-rss.svg', labels: {en: {label: 'three'}}}
//   ];
//   return (
//     <MockRouter location="/home">
//       <PortalComponentWrapper mockTheme={mockTheme}>
//         <Header
//           feature={object('feature', {
//             airlineLogo: airlineLogo,
//             hamburgerCloseMenuIcon: hamburgerCloseMenuIcon,
//             hamburgerMenuIcon: hamburgerMenuIcon,
//             menuItems: menuItems
//           })}
//           componentTheme={localTheme}
//         />
//       </PortalComponentWrapper>
//     </MockRouter>
//   );
// });
