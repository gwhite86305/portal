import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import createGenerateClassName from '@material-ui/core/styles/createGenerateClassName';
import jssPreset from '@material-ui/core/styles/jssPreset';
import LayoutInterface, {Locales} from 'component-utils/dist/types/LayoutInterface';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';
import styled, {ThemeProvider} from 'styled-components';
import {create, JSS} from 'jss';
import {path, merge, mergeAll, compose, keys, map} from 'ramda';
import * as React from 'react';
import {IntlProvider} from 'react-intl';
import JssProvider from 'react-jss/lib/JssProvider';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import {Route, Switch} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';
import {Locale, LocaleMessages} from 'component-utils/dist/types/LayoutInterface';
import Page from '../components/Page/Page';
import Template from '../components/Template/Template';
import {loadedComponents} from '../utils/ComponentLoader';
import {createComponent} from '../utils/LoaderCreateComponent';
import {ThemeSchema} from 'component-utils/dist/components/common/ThemeInterface';
import GlobalStyle from '../components/GlobalStyle';
import {History} from 'history';
import {IRootState} from '../store';
import {LayoutContext, RouterContext} from 'component-utils/dist/utils/Contexts';

const generateClassName = createGenerateClassName();
const jss = create(jssPreset()) as JSS & {options: {insertionPoint: HTMLElement | null}};
jss.options.insertionPoint = document.getElementById('jss-insertion-point');

const _getLocaleMessages = (userSelectedLocale: string, defaultLocaleMessages: LocaleMessages, locale: Locales) => {
  const overriddenLocales: LocaleMessages = locale
    ? ((locale.filter((l: Locale) => l.code === userSelectedLocale)[0] || {messages: {}}) as Locale).messages
    : {};

  return compose(
    merge(defaultLocaleMessages),
    mergeAll,
    map((k: string) => {
      if (k.indexOf('.') !== -1) {
        throw `*** PORTAL LAYOUT PARSE ERROR: INVALID '.' character in localized field name - replace with ':' character - ${k}`;
      }
      return {[k.replace(/:/g, '.')]: overriddenLocales[k]};
    })
  )(keys(overriddenLocales));
};

export const ConnectedIntlProvider = connect((state: IRootState) => {
  return {
    locale: state.ux.userSelectedLocale,
    messages: _getLocaleMessages(
      state.ux.userSelectedLocale || '',
      state.ux.defaultLocaleMessages,
      state.ux.layout ? state.ux.layout.locale : []
    )
  };
})(IntlProvider as any);

export const IntlAndThemeProviders: React.FunctionComponent<{theme?: ThemeSchema}> = props => {
  return (
    <ConnectedIntlProvider>
      <ThemeProvider theme={props.theme}>
        <React.Fragment>
          {props.children as any}
          <GlobalStyle />
        </React.Fragment>
      </ThemeProvider>
    </ConnectedIntlProvider>
  );
};

export const AppProviders: React.FunctionComponent<{theme?: ThemeSchema; history: History}> = props => {
  const darkTheme = props.theme && props.theme.darkTheme;
  const theme = createMuiTheme({
    palette: {
      type: darkTheme ? 'dark' : 'light'
    }
  });
  return (
    <ConnectedRouter history={props.history}>
      <JssProvider jss={jss} generateClassName={generateClassName}>
        <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>
      </JssProvider>
    </ConnectedRouter>
  );
};

export const getSystemSnackbar = props => {
  const {system, layout} = props;
  const {components} = layout;
  const snackBarCUid = Object.keys(components).find(cUid => components[cUid].componentName === 'SystemSnackbar');
  if (snackBarCUid) {
    return createComponent(snackBarCUid, {}, layout.components, null, {message: system.systemMessage}, true);
  } else {
    return <loadedComponents.SystemSnackbar message={system.systemMessage} />;
  }
};

const InvalidRoute = styled.div`
  font-size: 7rem;
  color: ${({theme}) => theme.primaryTextColor};
  display: flex;
  align-items: center;
  height: 100%;
  width: 100%;
  justify-content: center;
`;

const NoMatch = () => (isEnvUXBuilder() ? null : <InvalidRoute>?</InvalidRoute>);

const _getLayoutPageRoutes = (layout: LayoutInterface): React.ReactNode[] => {
  const {app} = layout;
  const layoutComponents = layout.components;
  const homePageRoute = path(['components', app.homePage, 'properties', 'feature', 'name'], layout);

  // https://github.com/ReactTraining/react-router/issues/4105
  // see: "In react-router v4, we usually do the following to pass in a ProductPage component"

  let pageRoutes: React.ReactNode[] = [];

  if (homePageRoute && homePageRoute !== '/') {
    pageRoutes = [<Redirect exact key={'home-redirect'} from="/" to={homePageRoute} />];
  }

  app.pages.forEach((pageComponentUid: string) => {
    const pageComponentJSON = layoutComponents[pageComponentUid];
    const {feature} = pageComponentJSON.properties;
    const pageName = feature ? feature.name.toLowerCase() : '';
    pageRoutes.push(
      <Route
        path={'/' + pageName}
        key={pageComponentUid}
        render={routeProps => {
          return (
            <RouterContext.Provider value={routeProps}>
              {React.createElement(Page, {
                ...pageComponentJSON.properties,
                ...routeProps,
                componentUid: pageComponentUid
              })}
            </RouterContext.Provider>
          );
        }}
      />
    );
  });

  return pageRoutes;
};

export const getAppTemplateAndBody = (layout: LayoutInterface) => {
  const {components, app} = layout;

  // all url changes will cause remounting from route switcher and upwards
  const routeSwitcher = (
    <Switch key={'main-switch'}>
      {_getLayoutPageRoutes(layout)}
      <Route key="NoMatch" component={NoMatch} />
    </Switch>
  );

  let appBodyComponentHierarchy = routeSwitcher;
  if (app.template) {
    appBodyComponentHierarchy = (
      <Template {...components[app.template].properties} templateBodyComponent={routeSwitcher} />
    );
  }

  return <LayoutContext.Provider value={layout}>{appBodyComponentHierarchy}</LayoutContext.Provider>;
};
