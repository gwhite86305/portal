import * as React from 'react';
import {applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const StyledButtom = styled.button`
  --icon-height: 20px;
  position: absolute;
  top: 30px;

  ${(props: any) => applyMediaQueriesForStyle('right', {xs: '30px', md: '110px'}, props.theme)};
  background: none;
  border: none;
  height: var(--icon-height);
  margin: 0;
  padding: 0;
  width: var(--icon-height);
  cursor: pointer;
  .left,
  .right {
    background-color: white;
    height: var(--icon-height);
    position: absolute;
    left: 50%;
    top: 0;
    ${() => applyMediaQueriesForStyle('width', {xs: '2px', md: '3px'}, {})};
  }
  .left {
    transform: rotate(45deg);
  }
  .right {
    transform: rotate(-45deg);
  }
`;

const HamburgerMenuCloseButton: React.FC<{onClick(): any}> = ({onClick}) => (
  <StyledButtom onClick={onClick}>
    <div className="left" />
    <div className="right" />
  </StyledButtom>
);

export default HamburgerMenuCloseButton;
