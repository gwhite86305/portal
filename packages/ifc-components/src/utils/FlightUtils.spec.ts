import {getFlightRemaining} from './FlightStatusUtils';
test('DFW->LAX over DFW', () => {
  let profile = {
    locations: {
      // DFW
      originLatitude: 32.89748,
      originLongitude: -97.040443,

      // LAX
      destinationLatitude: 33.9416,
      destinationLongitude: -118.4085
    }
  };
  // DFW
  let device = {
    latitude: 32.89748,
    longitude: -97.040443
  };
  expect(getFlightRemaining(device, profile)).toEqual({minutesRemaining: 155, percentageOfFlightComplete: 0});
});
test('DFW->LAX over PHX', () => {
  let profile = {
    locations: {
      // DFW
      originLatitude: 32.89748,
      originLongitude: -97.040443,

      // LAX
      destinationLatitude: 33.9416,
      destinationLongitude: -118.4085
    }
  };
  // PHX
  let device = {
    latitude: 33.4484,
    longitude: -112.074
  };
  expect(getFlightRemaining(device, profile)).toEqual({minutesRemaining: 46, percentageOfFlightComplete: 71});
});
test('DFW->LAX over LAX', () => {
  let profile = {
    locations: {
      // DFW
      originLatitude: 32.89748,
      originLongitude: -97.040443,

      // LAX
      destinationLatitude: 33.9416,
      destinationLongitude: -118.4085
    }
  };
  // LAX
  let device = {
    latitude: 33.9416,
    longitude: -118.4085
  };
  expect(getFlightRemaining(device, profile)).toEqual({minutesRemaining: 0, percentageOfFlightComplete: 100});
});
test('DFW->LAX over SYD', () => {
  let profile = {
    locations: {
      // DFW
      originLatitude: 32.89748,
      originLongitude: -97.040443,

      // LAX
      destinationLatitude: 33.9416,
      destinationLongitude: -118.4085
    }
  };
  // SYD
  let device = {
    latitude: 33.9399,
    longitude: 151.1753
  };
  expect(getFlightRemaining(device, profile)).toEqual({minutesRemaining: 624, percentageOfFlightComplete: 0});
});
