import * as React from 'react';
import {
  applyMediaQueriesForStyle,
  renderThemeProperty
  
} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export const StyledContainer = styled.div`
  position: absolute;
  width: 100%;
  height: ${props => `calc(100% - ${props.theme.componentTheme.topMargin})`};

  overflow: hidden;

  .moving-map-wrapper {
    width: 100%;
    height: 100%;
    display: flex;
    position: relative;
  }

  .map-wrapper {
    width: 100%;
    height: 100%;
  }

  .button-container {
    display: flex;
    flex-direction: column;
    position: absolute;
    align-items: center;
    ${() => applyMediaQueriesForStyle('bottom', {xs: '15px', sm: '30px'}, {})};
    ${() => applyMediaQueriesForStyle('right', {xs: '15px', sm: '30px'}, {})};
  }

  .zoom-button {
    margin-bottom: 15px;
  }

  .zoom-button,
  .center-button {
    ${renderThemeProperty('zoomIconColor', 'color')};
    ${renderThemeProperty('zoomIconBackgroundColor', 'background-color')};
    &:disabled {
      ${renderThemeProperty('zoomIconDisabledColor', 'color')};
      ${renderThemeProperty('zoomIconDisabledBackgroundColor', 'background-color')};
    }
  }

  .center-button {
    ${renderThemeProperty('centerIconColor', 'color')};
    ${renderThemeProperty('centerIconBackgroundColor', 'background-color')};
    &.centered {
      ${renderThemeProperty('centerIconColorCentered', 'color')};
    }
  }

  .icon-wrapper {
    border-radius: 50%;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    ${renderThemeProperty('airplaneIconBackgroundColor', 'background-color')};
    .icon {
      font-size: 42px;
      ${renderThemeProperty('airplaneIconColor', 'color')};
    }
  }
  .orig-dest-marker {
    border-radius: 50%;
    position: absolute;
    ${renderThemeProperty('origDestMarkerBackgroundColor', 'background-color')};
    ${renderThemeProperty('origDestMarkerBorder', 'border')};
    height: 30px;
    width: 30px;
    box-sizing: border-box;
  }
  .travelled-path {
    top: -2px;
    left: -2px;
    stroke-width: 4px;
    position: absolute;
    ${renderThemeProperty('airplaneTravelledPath', 'stroke')};
  }

  .flight-info-wrapper {
    ${renderThemeProperty('flightInfoLabelFontWeight', 'font-weight')};
    ${renderThemeProperty('flightInfoWrapperBackgroundColor', 'background-color')};
    ${renderThemeProperty('flightInfoTextColor', 'color')};
    ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.flightInfoTextSize, theme)};
    padding: 1rem 2rem;
    position: absolute;
    top: 10px;
    left: 8px;
    right: 8px;

    .flight-info-header-wrapper {
      display: flex;
      justify-content: space-between;

      .flight-info-aircraft-id {
        ${renderThemeProperty('airplaneIdTextColor', 'color')};
        ${renderThemeProperty('airplaneIdFontWeight', 'font-weight')};
        ${({theme}: any) => applyMediaQueriesForStyle('font-size', theme.componentTheme.airplaneIdTextSize, theme)};
      }

      .flight-info-params {
        display: flex;

        .flight-info-param {
          &:not(:last-child) {
            margin-right: 1rem;
          }
          display: flex;
          flex-direction: column;
          justify-content: space-between;

          .flight-info-param-value {
            ${renderThemeProperty('flightInfoValueFontWeight', 'font-weight')};
          }
        }
      }
    }

    .flight-progress {
      margin: 1rem 0;
    }

    .flight-info-time {
      display: flex;
      justify-content: space-between;

      .flight-info-time-label-wrapper {
        &:not(:last-child) {
          margin-right: 1rem;
        }
        &.destination {
          text-align: right;
        }
        .flight-info-time-label {
          ${renderThemeProperty('flightTimeInfoLabelKeywordsFontWeight', 'font-weight')};
        }
      }
    }
  }
`;

export default StyledContainer;
