{
  "header": {
    "motifText": "Viasat User Agreement",
    "noticeHeader": "IMPORTANT",
    "noticeText": "IMPORTANT NOTICE: PLEASE READ THIS AGREEMENT CAREFULLY TO ENSURE THAT YOU UNDERSTAND EACH PROVISION."
  },
  "terms": {
    "termsHeader": "Terms of Service",
    "termsSections": [
      {
        "title": "Introduction.",
        "isOpen": false,
        "paragraphs": [
          "This Terms of Service Agreement (the \u201C<strong>Agreement</strong>\u201D) is between you and Viasat, Inc. and its subsidiaries, including, with respect to services provided in Mexico, Carmel Comunicaciones, S.A. de C.V. (\u201C<strong>Viasat</strong>,\u201D \u201C<strong>Service Provider</strong>,\u201D \u201C<strong>us</strong>,\u201D or \u201C<strong>we</strong>\u201D), and governs your use of WiFi and internet services provided by Viasat (the \u201C<strong>Service</strong>\u201D) on Aerovías de México, S.A. de C.V.’s (including any affiliate and/or subsidiary, and any other brand names as will be applicable from time-to-time) aircraft from which you are connecting to the Service (the \u201C<strong>Airline</strong>\u201D). Your acceptance below and continued use of the Service represents your agreement to the terms set forth in this Agreement. If you do not agree with the terms set forth in this Agreement, immediately cease using the Service. If would like to contact Service Provider, you may write to:<br><br>Viasat, Inc.<br>3902 South Traditions Drive<br>College Station, TX 77845<br>USA<br><br>"
        ]
      },
      {
        "title": "1. Using the Service.",
        "isOpen": false,
        "paragraphs": [
          "In exchange for access to and use of the Service, you: (a) agree, if prompted, to provide Service Provider with accurate and complete information, including your email address, when accessing the Service, and to notify Service Provider of changes to your  information; (b) if applicable, agree to protect the password, username and security information you use to access the Service and to notify Service Provider immediately of any unauthorized use of your account that you become aware of;  (c) agree to comply with applicable laws and regulations, including but not limited to copyright and intellectual property rights laws; and (d) represent that you are at least 18 years of age."
        ]
      },
      {
        "title": "2. Billing Terms and Payment.",
        "isOpen": false,
        "paragraphs": [
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>a. General Billing Terms.</strong><br/>Your billing period will start the day and time you log in and provide a payment method in order to access the Service (the \u201C<strong>Billing Commencement Date</strong>\u201D). You will be logged off of the Service when (i) you click the \u201C<strong>Logout</strong>\u201D button (\u201C<strong>Individual Logout</strong>\u201D); or (ii) the system automatically logs you off because your session time has expired, your device has been powered off or has been inactive for an extended period of time, or Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination (\u201C<strong>Automatic Logout</strong>\u201D). The billing period ends on upon the earliest to occur of an Individual Logout or Automatic Logout."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>b. Single Session Pay Per Use Plan.</strong><br/>At the start of each session, we will charge all fees related to your use of the Service, including taxes, surcharges or other assessments applicable to the Service (\u201C<strong>Service Fees</strong>\u201D) to your credit card, debit card, or, if available on your flight, a voucher that you may purchase from an Airline personnel (\u201C<strong>Voucher</strong>\u201D)."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>c. Pay Per Flight Plan.</strong><br/>Each pay per flight session begins on the Billing Commencement Date and ends at the point in time when Internet service is no longer permitted on the flight due to the aircraft approaching or having reached its destination. At the start of each session, we will charge all Service Fees related to your use of the Service to your credit card, debit card or, if available, Voucher."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>d. Roaming Fees.</strong><br/>If you are a subscriber of another service provider that has a contractual relationship allowing that service provider\u2019s subscribers to roam on Viasat\u2019s WiFi network, your service provider may charge you a roaming fee for access to Viasat\u2019s WiFi network."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>f. Payment Terms.</strong><br/>You agree to pay all Service Fees in accordance with the provisions of the Service plan you selected. You authorize Service Provider to charge your credit or debit card for payment of all, or any portion of your Service Fees, until such amounts are paid in full. Your card issuer agreement governs use of your credit or debit card in connection with this Service; please refer to that agreement for your rights and liabilities as a cardholder. If available on your flight, you may also pay your Service Fee using a Voucher. If we do not receive payment from your credit or debit card issuer or its agent, or receive payment via a Voucher, you agree to pay us all amounts due upon demand by us. You agree that we will not be responsible for any expenses that you may incur resulting from overdrawing your bank account or exceeding your credit limit as a result of an automatic charge made under this Agreement."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>g. Billing Errors and Collections.</strong><br/>If you think a charge is incorrect or you need more information on any charges applied to your account, you should contact us by calling 1-800-681-8216 or at <a href='https://inflight.viasat.com/AMX' target='_blank'>https://inflight.viasat.com/AMX</a> within 60 days of receiving the statement on which the error or problem appeared. We will not pay you interest on any overcharged amounts later refunded or credited to you. If we choose to use a collection agency or attorney to collect money that you owe us or to assert any other right that we may have against you, you agree to pay the reasonable costs of collection or other action including, without limitation, collection agency fees, reasonable attorneys\u2019 fees, and court costs."
          }
        ]
      },
      {
        "title": "3. Operational Limits of the Service.",
        "isOpen": false,
        "paragraphs": [
          "Provisioning of the Service is subject to the availability and the operational limitations of the requisite equipment and associated facilities. There is no guarantee of bandwidth. Your connection and data rate speeds may not be suitable for some applications. You understand and agree that temporary interruptions of the Service may occur as normal events in the provision of the Service. Virtual private network or other remote computer access (\u201C<strong>VPN</strong>\u201D) may be intermittent and require multiple logins, depending on the VPN provider\u2019s security settings, due to brief losses in connectivity. Voice or video calls, online gaming and software updates are expressly prohibited through the Service. You further understand and agree that Service Provider has no control over third party networks you may access in the course of your use of the Service, and therefore, delays and disruptions of other network transmissions are beyond the control of Service Provider. Service Provider will not be liable for any failure of performance if such failure is due to any cause beyond Service Provider\u2019s reasonable control, including, but not limited to, acts of God, fire, explosion, vandalism, nuclear disaster, terrorism, satellite component failure, cable cut, storm or other weather or solar occurrence, any law, order or regulation by any government, civil, or military authority, national emergencies, insurrections, riots, wars, labor difficulties, supplier failures, shortages, breaches, or delays, or other failures or delays caused by you or your equipment."
        ]
      },
      {
        "title": "4. Third Party Content Disclaimer/ Links to Third Party Sites.",
        "isOpen": false,
        "paragraphs": [
          "Content provided by third parties (\u201C<strong>Third Party Content</strong>\u201D) have not been independently authenticated in whole or in part by Service Provider or the Airline even if Service Provider provides a link to such content. Service Provider does not provide, sell, license, or lease any of the Third Party Content other than that specifically identified as being provided by Service Provider. Service Provider is providing links to Third Party Content as a convenience only. Service Provider and Airline does not make any warranty or representation, of any kind, regarding Third Party Content."
        ]
      },
      {
        "title": "5. Privacy Policy and Acceptable Use Policy.",
        "isOpen": false,
        "paragraphs": [
          "Any data provided to or collected by Service Provider in the performance of the Service is subject to Service Provider\u2019s Privacy Policy for the Service. You acknowledge and confirm that your personal data will be shared with Viasat offices in California, India, and in the United States, and Adyen B.V., Aerovías de México, Amazon Web Services, Inc., Salesforce.com, Inc., and CMC Americas, Inc., which may be located outside your country of residence and may have a different level of data protection than your country of residence, all in accordance with the Privacy Policy. Use of the Service is also subject to the terms and conditions of Service Provider\u2019s Acceptable Use Policy for the Service. Service Provider\u2019s Privacy Policy and Acceptable Use Policy for the Service are incorporated into this Agreement and are accepted together with these Terms of Service at time of registration. Click <a href='/cms-served-assets/Viasat_Privacy_Policy.pdf' target='_blank' lang='en'>here</a> to review the Privacy Policy and click <a href='/cms-served-assets/Viasat_Acceptable_Use_Policy.pdf' target='_blank'>here</a> to review the Acceptable Use Policy."
        ]
      },
      {
        "title": "6. General Use Restrictions.",
        "isOpen": false,
        "paragraphs": [
          "Subject to your acceptance of and compliance with this Agreement, you are hereby granted the right to use the Service through a non-exclusive, non-transferable and non-assignable limited license. The Service is provided for your personal, non-commercial use only (unless otherwise specifically stated) and you agree not to reproduce, duplicate, copy, sell, sublicense, transfer, resell or exploit for any purposes your use of the Service or access to the Service. You agree not to share your access to the internet connection provided through the Service with another party by linking/tethering your connected device to the other party\u2019s device. All information, documents, products, and software (the \u201C<strong>Materials</strong>\u201D) provided with this Service were provided by or to Service Provider by its respective manufacturers, authors, developers, licensees and vendors (the \u201C<strong>Third Party Provider(s)</strong>\u201D) and are the copyrighted work of Service Provider and/or the Third Party Provider(s). Except as permitted for interoperability purposes by applicable law and as stated herein, none of the Materials may be copied, reproduced, resold, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, including, but not limited to, electronic, mechanical, photocopying, recording, or otherwise, without the prior express written permission of Service Provider or the Third Party Provider(s). Except as expressly stated herein, you are not granted any intellectual property rights in or to the Service or Materials by implication, estoppel or other legal theory, and all rights in and to the Service or Materials not expressly granted herein are hereby reserved and retained by Service Provider and/or  the Third Party Provider(s)."
        ]
      },
      {
        "title": "7. Export Laws.",
        "isOpen": false,
        "paragraphs": [
          "Your use of the Service is subject to U.S. export control laws and regulations, or any applicable local laws or regulations. You represent that you are not a prohibited end user under applicable U.S. export laws, regulations and lists, including but not limited to the U.S. Treasury Department list of Specially Designated Nationals or Blocked Persons. You will not use the Service in any manner that would violate applicable law, including but not limited to applicable export control laws and regulations."
        ]
      },
      {
        "title": "8. Disclaimer of Warranties and Liability.",
        "isOpen": false,
        "paragraphs": [
          "YOU ARE USING A PUBLIC INTERNET CONNECTION AND SHOULD TAKE ALL PRECAUTIONS FOR THE SECURITY OF YOUR DEVICE AND INFORMATION. YOU UNDERSTAND THAT YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM ANY MATERIAL AND/OR DATA DOWNLOADED FROM OR OTHERWISE PROVIDED THROUGH THE SERVICE.",
          "THE SERVICE PROVIDER DOES NOT GUARANTEE THAT THE SERVICE WILL BE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.",
          "THE SERVICE PROVIDER SHALL BE LIABLE FOR DAMAGES ACCORDING TO APPLICABLE STATUTORY PROVISIONS FOR ANY DAMAGES CAUSED BY INTENTIONAL OR GROSSLY NEGLIGENT CONDUCT OF LEGAL REPRESENTATIVES OR EXECUTIVES OF THE SERVICE PROVIDER. FOR THE REST, ANY LIABILITY OF THE SERVICE PROVIDER AND AIRLINE IS EXCLUDED."
        ]
      },
      {
        "title": "9. Indemnity.",
        "isOpen": false,
        "paragraphs": [
          "You agree to indemnify and hold harmless Service Provider from any claim or demand, including reasonable attorneys\u2019 fees, made by any third party arising out of: (a) content you submit, post, transmit or otherwise make available through the Service; (b) your violation of this Agreement; (c) your violation of Service Provider\u2019s Acceptable Use Policy; (d) your violation of any rights of another. At our expense and election, you agree to cooperate with us in connection with our defense. Other than as expressly stated in this Agreement, this Agreement shall not be deemed to create any rights in third parties."
        ]
      },
      {
        "title": "10. General.",
        "isOpen": false,
        "paragraphs": [
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>a. Contact Information.</strong><br/> Subject to applicable law, you agree that by entering into this Agreement and, if applicable, providing us with your wireless phone number and/or any other telephone number and/or your billing address and/or e-mail address, we or our agents may contact you for any account-related issues: (a) by calling or texting you at such number(s) using a prerecorded/artificial voice or text message delivered by an automatic telephone dialing system and/or using a call made by live individuals for any account-related issues, and/or (b) by sending an e-mail to such e-mail address."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>b. Applicable Law.</strong><br/>Any action related to this Agreement will be governed by Colorado law and controlling U.S. federal law and no choice of law rules of any jurisdiction will apply, however, if you are an EU resident your consumer protection rights are not limited by this choice of law and you may bring a claim to enforce your consumer protection rights in connection with this Agreement in the EU country in which you live. The European Commission provides for an online dispute resolution platform, which you can access here: <a href='http://ec.europa.eu/consumers/odr/' target='_blank'>http://ec.europa.eu/consumers/odr/</a>."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>c. Construction and Delegation.</strong><br/>This Agreement, as well as the additional online documents specifically incorporated as a part of this Agreement, constitutes the entire and only agreement with respect to its subject matter between you and us, applicable also to all users of your account. This Agreement supersedes all representations, proposals, inducements, assurances, promises, agreements and other communications with respect to its subject matter except as expressly set forth in this Agreement."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>d. Entire Agreement.</strong><br/>This Agreement, as well as the additional online documents specifically incorporated as a part of this Agreement, constitutes the entire and only agreement with respect to its subject matter between you and us, applicable also to all users of your account. This Agreement supersedes all representations, proposals, inducements, assurances, promises, agreements and other communications with respect to its subject matter except as expressly set forth in this Agreement."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>e. Miscellaneous.</strong><br/>We may enforce or decline to enforce any or all of the terms of this Agreement in our sole discretion. Captions used in this document are for convenience only and shall not be considered a part of this Agreement or be used to construe its terms or meaning. The effective date of this Agreement is the date that you first “accept” this Agreement in order to receive the Services. The provisions of this Agreement which by their nature should continue shall survive any termination of this Agreement."
          },
          {
            "style": {"marginLeft": "25px"},
            "content":
              "<strong>f. Notices to Service Provider.</strong><br/>All legal or regulatory Service related questions or notices must be in writing and sent to Service Provider at:<br><br>Viasat, Inc.<br>349 Inverness Drive South<br>Englewood, CO 80112, USA<br>Attn: General Counsel"
          }
        ]
      }
    ]
  },
  "effectiveDate": {
    "effectiveDateText": "Effective Date: October 26, 2018"
  },
  "agreements": {
    "agreementCheckOneText":
      "I have read and accept this Agreement including Viasat's Terms of Service Agreement, Consent Declaration, <a href='/cms-served-assets/Viasat_Privacy_Policy.pdf' target='_blank' lang='en' style='color:white'>Privacy Policy</a>, and <a href='/cms-served-assets/Viasat_Acceptable_Use_Policy.pdf' target='_blank' lang='en' style='color:white'>Acceptable Use Policy.</a>",
    "buttonText": "Continue"
  }
}
