export interface SeatSelectionProps {
  value: string;
  onChange: any;
  error: boolean;
  dirty: boolean;
}

export interface VoucherEntryProps {
  value: string;
  onChange: any;
  error: string | boolean;
}

export enum FormSection {
  seatEntry = 0,
  voucherEntry = 1,
  success = 2
}

export interface PaidContentProps {
  onPaid: () => any;
  onPurchaseSuccess: () => any;
  label: string;
  price: Price;
  entry: {
    itemId: string;
    itemType: string;
  };
  className: string;
}

export interface PaidContentState {
  open: boolean;
  processing: boolean;
  modalSection: FormSection;
  seat: string;
  seatDirty: boolean;
  voucherCode: string;
  voucherError: string | boolean;
  voucherCodeDirty: boolean;
  successMessage: string;
  paid: boolean;
}

export interface Price {
  value: number;
  currency: string;
  paid: boolean;
}

export interface VoucherOrder {
  voucherCode: string;
  totalPrice: string;
  currency: string;
  entries: OrderEntry[];
  seatNumber: string;
}

export interface OrderEntry {
  itemId: string;
  itemType: string;
  quantity?: number;
}

export interface OrderResponse {
  credits: number;
  currency?: string;
}
