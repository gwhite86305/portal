import * as React from 'react';
import {CommonComponentThemeWrapper, renderChildComponent} from 'component-utils/dist/utils/ComponentUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {ServiceCountDownCircle as ExternalInterface} from '../common/ComponentsInterface';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';
import PieChart from 'react-minimal-pie-chart';
export interface InternalInterface {
  className?: string;
  percentageRemaining: number;
  headerText: string;
  hours: string;
  minutes: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.56);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.56);
`;

const Progress = (props: any) => {
  const {feature, percentageRemaining, headerText, hours, minutes, theme} = props;

  const Container = styled.div`
    display: flex;
    ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.diameter, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.diameter, props.theme)};
    align-items: center;
    flex-direction: column;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
    div.circle {
      position: absolute;
      ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.diameter, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.diameter, props.theme)};
    }
  `;
  const StyledHeader = styled.div`
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    ${renderThemeProperty('headerFontColor', 'color')};
    ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.headerFontSize, props.theme)};
    ${renderThemeProperty('headerFont', 'font-family')};
    ${renderThemeProperty('timerTextFontWeight', 'font-weight')};
    padding-top: 8px;
    padding-bottom: 4px;
    line-height: 1.17;
  `;
  const StyledTimer = styled.div`
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    line-height: 1.2;
    ${renderThemeProperty('timerTextFontWeight', 'font-weight')};
    ${renderThemeProperty('timerFontColor', 'color')};
    ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.timerFontSize, props.theme)};
    ${renderThemeProperty('timerFont', 'font-family')};
  `;

  return (
    <Container>
      <PieChart
        className="circle"
        cx={50}
        cy={50}
        startAngle={270}
        lineWidth={parseInt(theme.lineWidth)}
        data={[
          {value: 100 - percentageRemaining, color: theme.consumedAreaColor},
          {value: percentageRemaining, color: theme.remainingAreaColor}
        ]}
      />
      <StyledHeader>{headerText}</StyledHeader>
      <StyledTimer>{`${hours} hr  ${minutes} min`}</StyledTimer>
    </Container>
  );
};
class ServiceCountDownCircle extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  public render() {
    const {percentageRemaining, hours, minutes, headerText, buttonText, componentTheme, feature, children} = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <Container className="ifc-ServiceCountDownCircle-component">
          <Progress
            theme={this.props.componentTheme}
            percentageRemaining={percentageRemaining}
            headerText={headerText}
            hours={hours}
            minutes={minutes}
          />
          {children && Array.isArray(children) ? renderChildComponent(0, children, {localizedText: buttonText}) : null}
        </Container>
      </PortalComponent>
    );
  }
}

export default ServiceCountDownCircle;
