import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {IconText as ExternalInterface} from '../common/ComponentsInterface';
import IconText, {InternalInterface} from './IconText';
import {utils} from 'component-utils';

const connectWrapper = utils.connectWrapper.default;

export type PageProps = ExternalInterface & InternalInterface & {actions: any};

class IconTextContainer extends React.Component<PageProps, any> {
  public render() {
    return <IconText {...this.props} />;
  }
}

export default withLocaleProps(IconTextContainer, [
  {altTextForImage: 'core.IconText.image.altText'},
  {localizedText: 'core.IconText.iconText'}
]) as any;
