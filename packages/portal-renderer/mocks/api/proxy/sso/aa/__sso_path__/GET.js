module.exports = function(req, response) {
  const Request = require('request');
  const data = {};
  data.ui = req.body;
  const ssoPath = req.params.sso_path;

  // Enrich the new message with info only the PFE knows
  data.common = {
    vessel_id: 'TESTPFE',
    event_time: new Date().getTime(),
    flight_id: `TESTPFE_SF_${new Date().getTime().toString()}`
  };
  data.device = {
    guid: '75212b52-2310-11e9-9b70-0242ac130010',
    device_category: 'PED',
    user_agent:
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 ' +
      '(KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
    device_detection: {
      deviceTypeId: 0
    }
  };
  //TODO make the url configurable via env/file
  Request.get(
    {
      headers: {'content-type': 'application/json'},
      url: `http://localhost:8000/aa/${ssoPath}`,
      body: JSON.stringify(data)
    },
    (err, resp, body) => {
      if (err) {
        return response.status(500).send(err);
      }
      return response.status(200).send(body);
    }
  );
};
