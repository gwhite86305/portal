#!/usr/bin/env node

const tar = require('tar');
const request = require('request');
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');

const buildNum = process.env.BUILD_NUMBER || '?';

const options = {
  portal: process.argv[2] || 'portal',
  build: process.argv[3] || 'dev'
};

const manifest = JSON.parse(fs.readFileSync('dist/_react_manifest.json'));

const gitinfo = manifest.gitinfo;

// Copied from original gruntfile.js
const archiveName =
  gitinfo.remote.origin.url
    .split('/')
    .pop()
    .split('.')[0] +
  '.' +
  gitinfo.local.branch.current.name.replace(/\//g, '_') +
  '.' +
  ((buildNum && buildNum + '.') || '') +
  gitinfo.local.branch.current.shortSHA +
  '.' +
  options.build +
  '.tar.gz';

// It's the same thing, but with latest instead of a SHA, and no build num
const archiveNameButLatest =
  gitinfo.remote.origin.url
    .split('/')
    .pop()
    .split('.')[0] +
  '.' +
  gitinfo.local.branch.current.name.replace('/', '_') +
  '.' +
  'latest.' +
  options.build +
  '.tar.gz';

// Copied from original gruntfile.js, but without the environment
const archiveNameButNoEnv =
  gitinfo.remote.origin.url
    .split('/')
    .pop()
    .split('.')[0] +
  '.' +
  gitinfo.local.branch.current.name.replace('/', '_') +
  '.' +
  ((buildNum && buildNum + '.') || '') +
  gitinfo.local.branch.current.shortSHA +
  '.tar.gz';

// It's the same thing, but with latest instead of a SHA, no build num, and no environment
const archiveNameButLatestButNoEnv =
  gitinfo.remote.origin.url
    .split('/')
    .pop()
    .split('.')[0] +
  '.' +
  gitinfo.local.branch.current.name.replace('/', '_') +
  '.latest' +
  '.tar.gz';

console.log(`Building for portal: ${options.portal}, env: ${options.build}
`);
tar.c(
  {
    gzip: true,
    file: `dist/${archiveName}`,
    cwd: 'dist',
    sync: true,
    strict: true
  },
  ['1', '_react_manifest.json', 'media', 'profile']
);

// curl -u p4build_twinpeaks:D3rDmB3t -X PUT https://artifactory.viasat.com/artifactory/twinpeaks-portal/TSC_AA/dev/develop/latest/TSC_AA.develop.latest.dev.tar.gz -T dist/*.tar.gz
const tarballBuffer = fs.readFileSync(`./dist/${archiveName}`);
let requestOptions = {
  headers: {
    'Content-type': 'application/gzip'
  },
  auth: {
    username: 'p4build_twinpeaks',
    password: 'D3rDmB3t',
    sendImmediately: true
  },
  body: tarballBuffer
};

const sendFile = (options, url) => {
  // {
  //     "repo" : "twinpeaks-portal",
  //     "path" : "/portal/dev/develop/1/portal.develop.1.e93f951.dev.tar.gz",
  //     "created" : "2018-03-20T10:31:05.944-07:00",
  //     "createdBy" : "p4build_twinpeaks",
  //     "downloadUri" : "https://artifactory.viasat.com/artifactory/twinpeaks-portal/portal/dev/develop/1/portal.develop.1.e93f951.dev.tar.gz",
  //     "mimeType" : "application/x-gzip",
  //     "size" : "13821742",
  //     "checksums" : {
  //       "sha1" : "c7e7a335870f6d18de0125dc85bc32eb64090dbf",
  //       "md5" : "f19f39bb441afaaa670123d9b99129fa"
  //     },
  //     "originalChecksums" : {
  //     },
  //     "uri" : "https://artifactory.viasat.com/artifactory/twinpeaks-portal/portal/dev/develop/1/portal.develop.1.e93f951.dev.tar.gz"
  //   }

  const requestOptions = Object.assign({}, options, {url: url});
  let chunks = [];
  request.put(requestOptions).on('response', resp => {
    resp
      .on('data', chunk => {
        chunks.push(chunk);
      })
      .on('end', () => {
        let respData = Buffer.concat(chunks);
        try {
          const respJson = JSON.parse(respData.toString());
          if (respJson.size)
            console.log(`Uploaded file: ${url}
size: ${respJson.size}
`);
          else
            console.warn(`There was a problem uploading file: ${url}
Response: ${respJson}
`);
        } catch (e) {
          console.error(`Could not upload file: ${url}`);
        }
      });
  });
};

// Upload a build numbered version for the environment (dev, beta, etc)
let artifactoryUrl = `https://artifactory.viasat.com/artifactory/twinpeaks-portal/${options.portal}/${options.build}/${
  gitinfo.local.branch.current.name
}/${buildNum}/${archiveName}`;
sendFile(requestOptions, artifactoryUrl);
// Upload a latest version for the environment (dev, beta, etc)
artifactoryUrl = `https://artifactory.viasat.com/artifactory/twinpeaks-portal/${options.portal}/${options.build}/${
  gitinfo.local.branch.current.name
}/latest/${archiveNameButLatest}`;
sendFile(requestOptions, artifactoryUrl);
// Upload a build numbered version for the branch (no environment (dev, beta, etc))
artifactoryUrl = `https://artifactory.viasat.com/artifactory/twinpeaks-portal/${options.portal}/${
  gitinfo.local.branch.current.name
}/${buildNum}/${archiveNameButNoEnv}`;
sendFile(requestOptions, artifactoryUrl);
// Upload a latest version for the branch (no environment (dev, beta, etc))
artifactoryUrl = `https://artifactory.viasat.com/artifactory/twinpeaks-portal/${options.portal}/${
  gitinfo.local.branch.current.name
}/latest/${archiveNameButLatestButNoEnv}`;
sendFile(requestOptions, artifactoryUrl);
