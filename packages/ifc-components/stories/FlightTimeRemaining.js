import React from 'react';
import cloneDeep from 'lodash/cloneDeep';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, object, boolean} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';

import FlightTimeRemaining from '../dist/components/FlightTimeRemaining/FlightTimeRemaining.js';
import PortalComponentWrapper, {
  mockTheme,
  mockThemeIcelandair
} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  localizedTitle: 'Flight time remaining'
};

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    backgroundImage: {
      xs: './images/FlightTimeRemaining_worldMap_Tablet.svg',
      sm: './images/FlightTimeRemaining_worldMap_Mobile.svg',
      lg: './images/FlightTimeRemaining_worldMap_Laptop.svg'
    },
    titleColor: 'theme.quaternaryTextColor',
    titleFontSize: 'theme.primaryFontSize',
    titleFontWeight: '500',
    progressBarForegroundColor: '#52ab5b',
    progressBarBackgroundColor: '#d9d9d9',
    progressBarPlaneFontSize: '25px',
    progressBarWidth: '200px',
    locationLabelColor: 'theme.quaternaryTextColor',
    locationLabelFontSize: '20px',
    locationLabelFontWeight: '500',
    timeToDestinationLabelColor: 'theme.secondaryTextColor',
    timeToDestinationLabelFontSize: 'theme.tertiaryFontSize',
    timeToDestinationLabelFontWeight: '500'
  },
  feature: {
    flightCutoffPercentage: 95,
    showTitle: true
  },
  flightOrigin: 'SEA',
  flightDestination: 'SKY',
  percentageOfFlightComplete: 0,
  flightMinutesRemaining: 240
};

const stories = storiesOf('FlightTimeRemaining', module);

stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);

const halfwayProps = cloneDeep(defaultProps);
halfwayProps.percentageOfFlightComplete = 50;
halfwayProps.flightMinutesRemaining = 120;

const flightEndProps = cloneDeep(defaultProps);
flightEndProps.percentageOfFlightComplete = 95;
flightEndProps.flightMinutesRemaining = 12;

const cutoffDisabledProps = cloneDeep(defaultProps);
cutoffDisabledProps.percentageOfFlightComplete = 99;
cutoffDisabledProps.flightMinutesRemaining = 0;
cutoffDisabledProps.feature.flightCutoffPercentage = 100;

const titleDisabledProps = cloneDeep(defaultProps);
titleDisabledProps.feature.showTitle = false;

stories
  .add('Default (0% complete)', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <FlightTimeRemaining {...defaultProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('50% complete', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <FlightTimeRemaining {...halfwayProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Flight End (95% complete)', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <FlightTimeRemaining {...flightEndProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Cutoff Disabled (99% complete)', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <FlightTimeRemaining {...cutoffDisabledProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ))
  .add('Title Disabled (0% complete)', () => (
    <PortalComponentWrapper mockTheme={mockThemeIcelandair}>
      <FlightTimeRemaining {...titleDisabledProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  ));
