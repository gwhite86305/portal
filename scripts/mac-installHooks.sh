 #! /bin/bash
echo \ Running mac-installHooks. symlinking hooks into .git/hooks...
find .git/hooks -type l -exec rm {} \; && find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;
echo 