import * as React from 'react';
import Tile from './Tile';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

const TileContainer: React.FC<any> = (props: any) => {
  return <Tile {...props} />;
};

export default withLocaleProps(TileContainer, [
  {altTextForImage: 'core.Tile.image.altText'},
  {subHeaderText: 'core.Tile.subHeader.text'},
  {headerText: 'core.Tile.header.text'},
  {buttonText: 'core.Tile.button.text'}
]) as any;
