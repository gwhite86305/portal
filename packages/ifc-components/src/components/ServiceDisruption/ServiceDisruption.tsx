import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {ServiceDisruption as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  isIFCDown: boolean;
  localizedHeaderText: string;
  localizedAlertText: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => {
  return applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme);
}};
  padding-top: calc(${(props: any) => props.theme.gutter} * 1.875);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.75);
  padding-right: calc(${(props: any) => props.theme.gutter} * 1.25);
  padding-left: calc(${(props: any) => props.theme.gutter} * 1.25);
  width: 100%;
  ${renderThemeProperty('backgroundColor', 'background-color')};
  .connectivity-lost-notice {
    ${renderThemeProperty('textColor', 'color')};
    > * {
      font-size: ${(props: any) => props.theme.defaultFontSize};
      margin-bottom: 0;
      margin-top: 0;
    }
    .alertHeader {
      font-weight: bold;
      line-height: 1.25;
      text-align: left;
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
    }
    .ifcDisruptionAlertText {
      ${(props: any) => {
  return applyMediaQueriesForStyle(
    'width',
    {
      lg: `728px`,
      sm: `auto`
    },
    props.theme
  );
}};
      word-break: break-word;
      font-weight: ${(props: any) => props.theme.fontWeight};
    }
  }
`;

class ServiceDisruption extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {localizedHeaderText, localizedAlertText, isIFCDown, componentTheme} = this.props;

    return isIFCDown ? (
      <PortalComponent componentTheme={componentTheme}>
        <Container>
          <div className="connectivity-lost-notice">
            {localizedHeaderText ? <h4 className="alertHeader">{localizedHeaderText}</h4> : null}
            {localizedAlertText ? <h5 className="ifcDisruptionAlertText">{localizedAlertText}</h5> : null}
          </div>
        </Container>
      </PortalComponent>
    ) : null;
  }
}

export default ServiceDisruption;
