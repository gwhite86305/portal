import {goBack, push, replace} from 'connected-react-router';
import * as screenfull from 'screenfull';
import {IRootState} from '../..';
import {isIOS} from '../../components/MediaPlayer/utils/playerCommon';
import {axiosWithBaseUrl as axios} from '../../utils/utils';
import {AnalyticEventType, reportEvent} from '../analytics/actions';
import {AnalyticsError, AnalyticsErrorCode} from '../analytics/utils';
import {pauseSingleAudio, playSingleAudio} from '../audio/actions';
import {TYPES as VIDEO_TYPES, VideoItem} from '../videos/types';
import {AudioItem, AUDIO_TYPE} from './../audio/types';
import {ActionCreators} from './actions';
import {utils} from 'component-utils';

export function clearPlayerInfo() {
  return async (dispatch: any) => dispatch(ActionCreators.clearPlayerInfo.create(undefined));
}

export function getAudioPlayback(audioId: string, fileId: string) {
  return async (dispatch: any) => {
    dispatch(ActionCreators.getAudioPlaybackStart.create({audioId, fileId}));
    try {
      const {
        data: {src}
      } = await axios.get(`/content/audio/playback?audioUid=${audioId}&fileUid=${fileId}`);
      dispatch(ActionCreators.getAudioPlaybackSuccess.create({src}));
      return {src} as {src: string};
    } catch (error) {
      dispatch(setErrorAudio(error.toString()));
      dispatch(ActionCreators.getAudioPlaybackError.create(undefined));
      return Promise.reject(error);
    }
  };
}

export function getDrmServers(playbackSessionId: string) {
  return async (dispatch: any) => {
    dispatch(ActionCreators.getDrmServersStart.create(undefined));
    try {
      const {data} = await axios.get(`/player/config`);
      dispatch(ActionCreators.getDrmServersSuccess.create({data} as any));
      return data;
    } catch (error) {
      dispatch(setErrorMovie({message: error.toString(), code: AnalyticsErrorCode.server}, playbackSessionId));
      dispatch(ActionCreators.getDrmServersError.create(undefined));
      return Promise.reject(error);
    }
  };
}

export function getVideoPlayback(videoId: string, playbackSessionId: string) {
  return async (dispatch: any) => {
    dispatch(ActionCreators.getVideoPlaybackStart.create({videoId}));
    try {
      const {
        data: {src, drmToken}
      } = await axios.get(`/content/videos/playback?videoUid=${videoId}`);
      dispatch(ActionCreators.getVideoPlaybackSuccess.create({src, drmToken}));
      return {src, drmToken};
    } catch (error) {
      // TODO uncomment this line when this fix is done https://jira.viasat.com/browse/CMDO-13508
      // dispatch(setErrorMovie({ message: error.toString(), code: AnalyticsErrorCode.server }, playbackSessionId));
      dispatch(ActionCreators.getVideoPlaybackError.create(undefined));
      return Promise.reject(error);
    }
  };
}

export function pauseItem(item?: AudioItem | VideoItem) {
  if (item === undefined) return;
  return (dispatch: any) => {
    switch (item.type) {
      case AUDIO_TYPE.ITEM:
        dispatch(pauseSingleAudio());
      case VIDEO_TYPES.VIDEO_ITEM:
        const video = (document.querySelector('video') || {}) as any;
        video.pause && video.pause();
        return;
    }
  };
}

export function playItem(item: AudioItem | VideoItem, playerPageArg?: string) {
  return (dispatch: any, getState: () => IRootState) => {
    const {ifeMediaPlayer, ifeAudio, ifeNotificationDispatcher} = getState();
    const videoPlayerPage = playerPageArg || ifeMediaPlayer.playerPage || '';
    const {pathname, hash} = window.location;
    const pageUrl = `${pathname} ${hash}`;
    switch (item.type) {
      case AUDIO_TYPE.ITEM:
        dispatch(playSingleAudio(item, ifeMediaPlayer, ifeAudio, ifeNotificationDispatcher));
        videoPlayerPage && pageUrl.includes(videoPlayerPage) && dispatch(goBack());
        break;
      case VIDEO_TYPES.VIDEO_ITEM:
        if (utils.mobileSdk.isUsingMobileSdk()) {
          dispatch(ActionCreators.mobilePlayVideo.create({uid: item.uid}));
          return;
        }
        const targetPage = `${videoPlayerPage}?videoId=${item.uid}`;
        const video = document.querySelector('video') as any;
        // is same video page, so play-pause scenario
        if (pageUrl.indexOf(targetPage) > -1 && video) {
          video.play();
          break;
        }
        const pushOrReplace = pageUrl.includes(videoPlayerPage) ? replace : push;
        dispatch(pushOrReplace(targetPage));
        break;
    }
  };
}

export function playItemTrailer(item: VideoItem, playerPageArg?: string) {
  return (dispatch: any, getState: () => IRootState) => {
    const videoPlayerPage = playerPageArg || getState().ifeMediaPlayer.playerPage || '';
    const targetPage = `${videoPlayerPage}?videoId=${item.uid}&trailer`;
    if (utils.mobileSdk.isUsingMobileSdk()) {
      dispatch(ActionCreators.mobilePlayVideo.create({uid: item.uid}));
      return;
    }
    dispatch(push(targetPage));
  };
}

export function seekToRange(target: number): any {
  return (dispatch: any) => dispatch(ActionCreators.seekToRange.create({target}));
}

export function setAudioLanguages(langs: string[]): any {
  return (dispatch: any) => dispatch(ActionCreators.setAudioLanguages.create({langs}));
}

export function setBuffering(isBuffering: boolean) {
  return (dispatch: any) => {
    dispatch(ActionCreators.setBuffering.create({isBuffering}));
  };
}

export function setCurrentItem(currentItem: Partial<AudioItem | VideoItem> & {uid: string; type: string}): any {
  return (dispatch: any) => dispatch(ActionCreators.setCurrentItem.create(currentItem));
}

export function setFullscreen(isFullscreen: boolean): any {
  return (dispatch: any) => {
    let domTarget: any;
    if (isIOS) {
      domTarget = document.querySelector('video') as HTMLVideoElement;

      if (domTarget) {
        isFullscreen ? domTarget.webkitEnterFullscreen() : domTarget.webkitExitFullscreen();
        return dispatch(ActionCreators.setFullscreen.create({isFullscreen}));
      }
    }
    try {
      if (screenfull && screenfull.enabled) {
        isFullscreen ? screenfull.request(domTarget) : screenfull.exit();
        dispatch(ActionCreators.setFullscreen.create({isFullscreen}));
      }
    } catch (e) {
      /*
        this handles network disconnects or other unexpected cases
        where screenfull will throw an exception
      */
    }
  };
}

export function setPlayerAsPaused(): any {
  return (dispatch: any) => {
    dispatch(ActionCreators.setPlayerAsPaused.create(undefined));
  };
}

export function setPlayerAsPlay(): any {
  return (dispatch: any) => {
    dispatch(ActionCreators.setPlayerAsPlay.create(undefined));
  };
}

export function setPlayerAsPlaying(): any {
  return (dispatch: any) => {
    dispatch(ActionCreators.setPlayerAsPlaying.create(undefined));
  };
}

export function setPlayerPage(page: string): any {
  return (dispatch: any) => dispatch(ActionCreators.setPlayerPage.create(page));
}

export function setErrorAudio(error: any): any {
  return (dispatch: any) => {
    let message;
    try {
      message = JSON.stringify(error);
    } catch {
      message = error.toString && error.toString();
    }
    const errorObject = {code: 0, message};
    dispatch(
      reportEvent(AnalyticEventType.FAILED, {
        error: errorObject,
        playbackSessionId: 'TODO_IMPLEMENT_AUDIO_PLAYBACK_SESSION_ID'
      })
    );
    dispatch(ActionCreators.setError.create(error));
  };
}

export function setErrorMovie(error: AnalyticsError, playbackSessionId: string): any {
  return (dispatch: any) => {
    dispatch(reportEvent(AnalyticEventType.FAILED, {error, playbackSessionId}));
    dispatch(ActionCreators.setError.create(error));
  };
}

export function setSubtitles(langs: string[]): any {
  return (dispatch: any) => dispatch(ActionCreators.setSubtitles.create({langs}));
}

export function updateVolume(value: number): any {
  return (dispatch: any) => dispatch(ActionCreators.updateVolume.create({value}));
}
