import {getGridTheme, getRow, getColumn, getRowPos, getColumPos} from '../grid';

const width = 1000;
const mdFraction = 6;
const smFraction = 3;
const xsFraction = 2;
const imageRatio = 27 / 40;
const numItems = 52;

describe('getGridTheme', () => {
  it('calculate rowHeight', () => {
    const theme = getGridTheme(width, xsFraction, smFraction, mdFraction, imageRatio, numItems);
    expect(theme.rowHeight).toEqual({
      xs: '740.7407407407406px',
      sm: '493.8271604938271px',
      md: '246.91358024691354px'
    });
  });

  it('calculate rowHeightMs', () => {
    const theme = getGridTheme(width, xsFraction, smFraction, mdFraction, imageRatio, numItems);
    expect(theme.rowHeightMs).toEqual({
      xs: '740.7407407407406px '.repeat(26),
      sm: '493.8271604938271px '.repeat(18),
      md: '246.91358024691354px '.repeat(9)
    });
  });

  it('calculate horizontalFraction', () => {
    const theme = getGridTheme(width, xsFraction, smFraction, mdFraction, imageRatio, numItems);
    expect(theme.horizontalFraction).toEqual({
      xs: '1fr 1fr ',
      sm: '1fr 1fr 1fr ',
      md: '1fr 1fr 1fr 1fr 1fr 1fr '
    });
  });
});

describe('getRow', () => {
  it('return correct row', () => {
    // should equal row 1
    expect(getRow(1, mdFraction)).toEqual(1);
    // should equal row 2
    expect(getRow(6, mdFraction)).toEqual(1);
    // should equal row 3
    expect(getRow(7, mdFraction)).toEqual(2);
    // should return invalid row
    expect(getRow(-1, mdFraction)).toEqual(-0);
  });
});

describe('getColumn', () => {
  it('return correct column', () => {
    // column should equal 1
    expect(getColumn(1, mdFraction)).toEqual(1);
    // column should equal 6
    expect(getColumn(6, mdFraction)).toEqual(6);
    // column should equal 1
    expect(getColumn(7, mdFraction)).toEqual(1);
    // column should equal 6
    expect(getColumn(12, mdFraction)).toEqual(6);
    // column should return invalid column
    expect(getColumn(-1, mdFraction)).toEqual(-1);
  });
});


describe('getRowPos', () => {
  it('return correct row', () => {
    // should always be in row 1
    expect(getRowPos(1, xsFraction, smFraction, mdFraction)).toEqual({"md": "1", "sm": "1", "xs": "1"});
    // should be in row two in xs & sm
    expect(getRowPos(4, xsFraction, smFraction, mdFraction)).toEqual({"md": "1", "sm": "2", "xs": "2"});
  });
});

describe('getColumPos', () => {
  it('return correct row', () => {
    // should always be coulmn 1
    expect(getColumPos(1, xsFraction, smFraction, mdFraction)).toEqual({"md": "1", "sm": "1", "xs": "1"});
    // column should change in sm & xs
    expect(getColumPos(4, xsFraction, smFraction, mdFraction)).toEqual({"md": "4", "sm": "1", "xs": "2"});
  });
});
