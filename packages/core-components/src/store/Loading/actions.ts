import axios from 'axios';
import {ActionCreator} from 'component-utils/dist/store/action-creator';

// Action Creators
export const ActionCreators = {
  IncrementPendingRequestsCounter: new ActionCreator<'IncrementPendingRequestsCounter', any>(
    'IncrementPendingRequestsCounter'
  ),
  DecrementPendingRequestsCounter: new ActionCreator<'DecrementPendingRequestsCounter', any>(
    'DecrementPendingRequestsCounter'
  ),
  ClearPendingRequestsCounter: new ActionCreator<'ClearPendingRequestsCounter', any>('ClearPendingRequestsCounter')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function IncrementPendingRequestsCounter() {
  return (dispatch: any) => {
    dispatch(ActionCreators.IncrementPendingRequestsCounter.create(new Date().valueOf()));
  };
}

export function DecrementPendingRequestsCounter() {
  return (dispatch: any) => {
    dispatch(ActionCreators.DecrementPendingRequestsCounter.create(undefined));
  };
}

export function ClearPendingRequestsCounter() {
  return (dispatch: any) => {
    dispatch(ActionCreators.ClearPendingRequestsCounter.create(undefined));
  };
}
