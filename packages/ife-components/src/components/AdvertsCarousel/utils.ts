import {AdvertItem} from '../../store/adverts/types';
import {AnalyticsLogger} from 'component-utils/dist/analytics';
import {DispatchProps, StateProps} from './types';
import {getAdverts} from '../../store/adverts/actions';
import {IRootState} from '../..';
import {trackImpression, trackView} from '../../store/analytics/actions';

export const mapStoreToProps = (state: IRootState): StateProps => ({
  items: state.ifeAdverts.items,
  isFetchCompleted: state.ifeAdverts.isFetchCompleted,
  isLoading: state.ifeAdverts.isLoading
});
const paLogger = new AnalyticsLogger();

export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    getAdverts: () => dispatch(getAdverts()),
    trackImpression: (item: AdvertItem) => {
      paLogger.AdImpression(item.uid);
      return dispatch(trackImpression(item));
    },
    trackView: (item: AdvertItem) => {
      paLogger.AdClickThrough(item.uid);
      return dispatch(trackView(item));
    }
  }
});
