import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getDefaultState, deviceHasServiceTier} from '../../utils/ConnectionUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, connected: true};
  if (state.device && state.profile) {
    props.isDefaultService = getDefaultState(state.device, state.profile) || !deviceHasServiceTier(state.device);
  }
  return props;
})
export default class HideOnDefaultService extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.isDefaultService || this.props.isDefaultService === undefined ? null : this.props.children;
  }
}
