import {pathOr} from 'ramda';
var jwtDecode = require('jwt-decode');
export const isLoggedIn = (state: any) => {
  let loggedIn = false;
  const jwt = pathOr(undefined, ['Authentication', 'aaAuth', 'jwt'], state);
  if (jwt) {
    loggedIn = jwtDecode(jwt).expire > Math.floor(Date.now() / 1000);
  }
  return loggedIn;
};
