import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, text, object, boolean} from '@storybook/addon-knobs/react';
import TextButton from '../dist/components/TextButton/TextButton.js';
import MockRouter from 'react-mock-router';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  localizedHeaderText: 'Thank you',
  localizedSubHeaderText: 'Enjoy Wi-Fi',
  localizedButtonText: 'VIEW RECEIPT'
};

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    textColor: 'theme.tertiaryTextColor',
    margin: {
      xs: '200px 180px 180px 180px'
    }
  },
  feature: {
    includeButton: true
  }
};

const neosProps = {
  componentTheme: {
    backgroundColor: 'rgb(255, 255, 255)',
    margin: {
      xs: '200px 180px 180px 180px'
    }
  },
  feature: {
    includeButton: true,
    openPageOnClick: 'Payment_Confirmation'
  }
};

const altNeosLocaleProps = {
  localizedHeaderText: 'Grazie',
  localizedSubHeaderText: 'Goditi il WiFi!',
  localizedButtonText: 'Vedi la tua ricevuta'
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";

const stories = storiesOf('CMOB-21324:- TextButton Stories', module);

stories.addDecorator(withKnobs);

stories.add('TextButton - With default props', () => (
  <MockRouter location="/home">
    <PortalComponentWrapper mockTheme={mockTheme}>
      <TextButton {...defaultProps} {...defaultLocaleProps} />
    </PortalComponentWrapper>
  </MockRouter>
));

stories.add('TextButton - With Neos theme', () => (
  <MockRouter location="/home">
    <PortalComponentWrapper mockTheme={mockThemeNeos}>
      <TextButton {...neosProps} {...altNeosLocaleProps} />
    </PortalComponentWrapper>
  </MockRouter>
));

stories.add('TextButton - With All Knobs', () => (
  <MockRouter location="/home">
    <PortalComponentWrapper mockTheme={mockTheme}>
      <TextButton
        componentTheme={object('Component Theme', defaultProps.componentTheme)}
        feature={object('Feature Properties', defaultProps.feature)}
        localizedHeaderText={text('Header Text', defaultLocaleProps.localizedHeaderText)}
        localizedSubHeaderText={text('Sub Header Text', defaultLocaleProps.localizedSubHeaderText)}
        localizedButtonText={text('Button Text', defaultLocaleProps.localizedButtonText)}
      />
    </PortalComponentWrapper>
  </MockRouter>
));
