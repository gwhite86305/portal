import * as React from 'react';
import {utils} from 'component-utils';
import {PaymentConfirmation as ExternalInterface} from '../common/ComponentsInterface';
import PaymentConfirmation, {InternalInterface} from './PaymentConfirmation';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {requestOrderHistory} from '../../store/WifiServiceOptions/actions';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import PaymentConfirmationTabular from './PaymentConfirmationTabular';
const connectWrapper = utils.connectWrapper.default;

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onButtonClick(): any;
  getOrders(email: string): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const WifiServiceOptions = state.WifiServiceOptions;
    const device = state.device;
    const currencyCode = state.profile.currencyCode || state.ux.settings.ifc.defaultCurrencyCode;
    const currencySuffix = state.ux.settings.ifc.currencySuffix;
    const locale = state.device.language;
    const minimumFractionDigits = state.ux.settings.ifc.defaultMinimumFractionDigits;
    const maximumFractionDigits = state.ux.settings.ifc.defaultMaximumFractionDigits;
    return {
      WifiServiceOptions,
      device,
      currencyCode,
      locale,
      minimumFractionDigits,
      maximumFractionDigits,
      currencySuffix
    };
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onButtonClick: () => {
        ownProps.history.push({pathname: ownProps.feature.openPageOnClick});
      },
      getOrders: email => {
        dispatch(requestOrderHistory(email));
      }
    };
  }
)
class PaymentConfirmationContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }
  public componentDidMount() {
    try {
      const email = this.props.device.serviceUid || localStorage.getItem('purchase-email');
      this.props.getOrders(email);
    } catch (exception) {}
  }

  public render() {
    const {intl, componentUid, feature} = this.props;

    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.PurchaseConfirmation.header'},
      {thankYouNoteText: 'ifc.PurchaseConfirmation.thankYouNoteText'},
      {transactionIdLabel: 'ifc.PurchaseConfirmation.transactionIdLabel'},
      {dateLabel: 'ifc.PurchaseConfirmation.dateLabel'},
      {flightNumberLabel: 'ifc.PurchaseConfirmation.flightNumberLabel'},
      {merchantOfRecordLabel: 'ifc.PurchaseConfirmation.merchantOfRecordLabel'},
      {merchantOfRecordLabelValue: 'ifc.PurchaseConfirmation.merchantOfRecordLabelValue'},
      {paymentMethodLabel: 'ifc.PurchaseConfirmation.paymentMethodLabel'},
      {serviceCostLabel: 'ifc.PurchaseConfirmation.serviceCostLabel'},
      {discountLabel: 'ifc.PurchaseConfirmation.discountLabel'},
      {totalLabel: 'ifc.PurchaseConfirmation.totalLabel'},
      {buttonText: 'ifc.PurchaseConfirmation.buttonText'},
      {serviceDescription: 'ifc.PurchaseConfirmation.serviceDescription'},
      {thankYouIcon: 'ifc.PurchaseConfirmation.thankYouIcon'}
    ]);
    return feature.tabularView ? (
      <PaymentConfirmationTabular {...this.props} {...localizedProps} />
    ) : (
      <PaymentConfirmation {...this.props} {...localizedProps} />
    );
  }
}

export default injectIntl(withRouter(PaymentConfirmationContainer));
