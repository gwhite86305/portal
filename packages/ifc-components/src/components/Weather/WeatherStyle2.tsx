import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {Weekdays} from 'component-utils/dist/utils/DateUtils';
import {GetTempFC} from 'component-utils/dist/utils/TemperatureConverter';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {Weather as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {RouterContext} from 'component-utils/dist/utils/Contexts';

export interface InternalInterface {
  className?: string;
  localizedClickableAreaText?: string;
  dailyForecast: any;
  currentCondition: any;
  cityName: string;
  icons: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${renderThemeProperty('primaryTextColor', 'color')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  font-family: ${(props: any) => props.theme.secondaryFontFamily};
  width: 100%;
  display: flex;
  ${renderThemeProperty('borderRadius', 'border-radius')};
  overflow: hidden;
  flex-direction: column;
  ${renderThemeProperty('borderColor', 'border-color')};
  border-width: 1px;
  border-style: solid;
  box-sizing: border-box;
`;

const Header = (props: any) => {
  const {localizedHeaderText} = props;
  const Container = styled.div`
    display: flex;
    height: 73px;
    justify-content: center;
    align-items: center;
    ${renderThemeProperty('borderColor', 'border-color')};
    border-width: 0px;
    border-bottom-width:1px;
    border-style: solid;
    box-sizing: border-box;
    span {
      font-family: ${(props: any) => props.theme.appFontFamily};
      ${renderThemeProperty('headerTextColor', 'color')};
      ${renderThemeProperty('headerFontSize', 'font-size')};
    }
  }`;

  return (
    <Container>
      <span>{localizedHeaderText}</span>
    </Container>
  );
};

const Today = (props: any) => {
  const {clickable, currentCondition, icons, todayText} = props;
  const temp = GetTempFC(currentCondition.temperature);
  const Container = styled.div`
    display: flex;
    height: ${props => (clickable ? 'calc(40% - 26px)' : '42%')};
    justify-content: center;
    ${renderThemeProperty('borderColor', 'border-color')};
    border-style: solid;
    border-width: 0px;
    border-bottom-width: 1px;
    .today-text {
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.25);
      ${renderThemeProperty('borderColor', 'border-color')};
      width: 50%;
      flex-direction: column;
      justify-content: center;
      border-style: solid;
      border-width: 0px;
      border-right-width: 1px;
      text-align: center;
      display: flex;
      .title {
        line-height: 1.15;
      }
      .temp {
        font-weight: bold;
        line-height: 1.15;
        margin-top: 6px;
        margin-bottom: 7px;
      }
      .city {
        font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.875);
      }
    }
    .today-icon {
      width: 50%;
      text-align: center;
      display: flex;
      align-items: center;
      img {
        width: 115px;
        height: 115px;
        margin: 0 auto;
      }
    }
  `;

  return (
    <Container>
      <div className="today-text">
        <div className="title">{todayText}</div>
        <div className="temp">
          {temp.F}&deg;F / {temp.C}&deg;C
        </div>
        <div className="city">{props.cityName}</div>
      </div>
      <div className="today-icon">
        <img alt={currentCondition.weatherText} src={icons[currentCondition.weatherIcon]} />
      </div>
    </Container>
  );
};

const Forecast = (props: any) => {
  let {clickable, showCurrentCondition, onClick, feature, clickableAreaText, todayText} = props;

  const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: ${props => (showCurrentCondition ? 'calc(60% - 36px)' : '100%')};
    .row {
      ${renderThemeProperty('borderColor', 'border-color')};
      font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
      flex-grow: 1;
      flex-basis: 0;
      text-align: center;
      border-style: solid;
      border-width: 0px;
      border-bottom-width: 1px;
      padding: 0 20px;
      justify-content: space-between;
      display: flex;
      flex-direction: row;
      .day-name {
        display: flex;
        align-items: center;
        font-weight: bold;
      }
      .day-weather {
        display: flex;
        .weather-icon {
          display: flex;
          align-items: center;
          img {
            width: 42px;
            height: 42px;
            ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.weatherIconMargin, props.theme)};
          }
        }
        .arrow {
          display: flex;
          align-items: center;
          img {
            width: 8px;
            margin-right: 10px;
            margin-left: 10px;
          }
        }
        .temp {
          display: flex;
          justify-content: center;
          flex-direction: column;
          align-items: center;
          font-size: calc(${(props: any) => props.theme.defaultFontSize} * 0.875);
          .seperator {
            margin: 0 5px;
          }
          .flip {
            flex-direction: row-reverse;
          }
          .min {
            display: flex;
            .temp-f {
              ${renderThemeProperty('lowTempFahrenheitTextColor', 'color')};
              ${renderThemeProperty('lowTempFahrenheitFontWeight', 'font-weight')};
            }
            .temp-c {
              ${renderThemeProperty('lowTempCelsiusTextColor', 'color')};
              ${renderThemeProperty('lowTempCelsiusFontWeight', 'font-weight')};
            }
          }
          .max {
            display: flex;
            .temp-f {
              ${renderThemeProperty('highTempFahrenheitTextColor', 'color')};
              ${renderThemeProperty('highTempFahrenheitFontWeight', 'font-weight')};
            }
            .temp-c {
              ${renderThemeProperty('highTempCelsiusTextColor', 'color')};
              ${renderThemeProperty('highTempCelsiusFontWeight', 'font-weight')};
            }
          }
        }
      }
    }
    .row:last-child {
      border-bottom-width: 0px;
    }
    .weather-more {
      position: relative;
      align-items: center;
      cursor: pointer;
      color: black;
      justify-content: center !important;
      ${renderThemeProperty('clickableAreaBackgroundColor', 'background-color')};
      ${renderThemeProperty('clickableAreaTextColor', 'color')};
      ${renderThemeProperty('clickableAreaFontSize', 'font-size')};
      a {
        display: flex;
        align-items: center;
      }
      a::after {
        background: url('./mocks/images/weather_right_arrow.svg') no-repeat;
        width: 11px;
        height: 11px;
        content: '.';
        position: relative;
        font-size: 0;
        display: inline-block;
        left: 9px;
      }
    }
  `;

  const Day = (props: any) => {
    const {forecast, icons, feature, todayText} = props;
    const minTemp = GetTempFC(forecast.temperature.minimum.value);
    const maxTemp = GetTempFC(forecast.temperature.maximum.value);

    let forecastDate = 'Forecast';
    if (forecast.date === 'Today') {
      forecastDate = '' + {todayText};
    } else if (forecast.date) {
      forecastDate = Weekdays[new Date(forecast.date).getDay()];
    }

    return (
      <div className="row">
        <div className="day-name">{forecastDate}</div>
        <div className="day-weather">
          <div className="weather-icon">
            <img alt={forecast.day.iconPhrase} src={icons[forecast.day.icon]} />
          </div>
          <div className="arrow">
            <img src="./mocks/images/weather_up_down_arrow.svg" />
          </div>
          <div className="temp">
            <span className={'max' + (feature.flipCelsiusFahrenheit ? ' flip' : '')}>
              <span className="temp-f">{maxTemp.F}&deg;F</span>
              <span className="seperator"> /</span>
              <span className="temp-c"> {maxTemp.C}&deg;C</span>
            </span>
            <span className={'min' + (feature.flipCelsiusFahrenheit ? ' flip' : '')}>
              <span className="temp-f">{minTemp.F}&deg;F </span>
              <span className="seperator"> /</span>
              <span className="temp-c">{minTemp.C}&deg;C </span>
            </span>
          </div>
        </div>
      </div>
    );
  };
  return (
    <Container>
      {props.dailyForecast.map((forecast: any, key: any) => {
        return <Day key={key} forecast={forecast} icons={props.icons} todayText={todayText} feature={feature} />;
      })}
      {clickable ? (
        <div className="row weather-more">
          <a onClick={onClick} tabIndex={0}>
            {clickableAreaText}
          </a>
        </div>
      ) : null}
    </Container>
  );
};

class WeatherStyle2 extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    cityName: '---',
    currentCondition: {
      temperature: '---',
      weatherIcon: 2,
      weatherText: '---'
    },
    dailyForecast: [
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516798800,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-24T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516885200,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-25T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516971600,
        night: {iconPhrase: '---', icon: 12},
        day: {iconPhrase: '---', icon: 18},
        date: '2018-01-26T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1517058000,
        night: {iconPhrase: '---', icon: 36},
        day: {iconPhrase: '---', icon: 12},
        date: '2018-01-27T07:00:00-06:00'
      }
    ],
    icons: {
      '1': './mocks/images/Weather_1Sunny_50x50.svg',
      '2': './mocks/images/Weather_2MostlySunny_50x50.svg',
      '3': './mocks/images/Weather_3PartlyCloudyDay_50x50.svg',
      '4': './mocks/images/Weather_4IntermittentCloudsDay_50x50.svg',
      '5': './mocks/images/Weather_5HazyDay_50x50.svg',
      '6': './mocks/images/Weather_6MostlyCloudyDay_50x50.svg',
      '7': './mocks/images/Weather_7PartlyCloudy_50x50.svg',
      '8': './mocks/images/Weather_8PartlyCloudyAndHazy_50x50.svg',
      '11': './mocks/images/Weather_11Fog_50x50.svg',
      '12': './mocks/images/Weather_12PartlyCloudyShowers_50x50.svg',
      '13': './mocks/images/Weather_13MostlyCloudyRain_50x50.svg',
      '14': './mocks/images/Weather_14PartlyCloudyRainDay_50x50.svg',
      '15': './mocks/images/Weather_15PartlyCloudyThunderstorms_50x50.svg',
      '16': './mocks/images/Weather_16MostlyCloudyThunderstorms_50x50.svg',
      '17': './mocks/images/Weather_17PartlyCloudyThunderstormsDay_50x50.svg',
      '18': './mocks/images/Weather_18PartlyCloudyRain_50x50.svg',
      '19': './mocks/images/Weather_19PartlyCloudyFlurries_50x50.svg',
      '20': './mocks/images/Weather_20MostlyCloudyFlurries_50x50.svg',
      '21': './mocks/images/Weather_21PartlyCloudyFlurriesDay_50x50.svg',
      '22': './mocks/images/Weather_22PartlyCloudySnow_50x50.svg',
      '23': './mocks/images/Weather_23MostlyCloudySnow_50x50.svg',
      '24': './mocks/images/Weather_24PartlyCloudyIce_50x50.svg',
      '25': './mocks/images/Weather_25PartlyCloudySleet_50x50.svg',
      '26': './mocks/images/Weather_26PartlyCloudyFreesingRain_50x50.svg',
      '29': './mocks/images/Weather_29PartlyCloudyRainAndSnow_50x50.svg',
      '30': './mocks/images/Weather_30HotNoSun_50x50.svg',
      '31': './mocks/images/Weather_31ColdNoSnowFlake_50x50.svg',
      '32': './mocks/images/Weather_32Windy_50x50.svg',
      '33': './mocks/images/Weather_33ClearNight_50x50.svg',
      '34': './mocks/images/Weather_34MostlyClearNight_50x50.svg',
      '35': './mocks/images/Weather_35PartlyCloudyNight_50x50.svg',
      '36': './mocks/images/Weather_36IntermittentCloudsNight_50x50.svg',
      '37': './mocks/images/Weather_37HazyNight_50x50.svg',
      '38': './mocks/images/Weather_38MostlyCloudyNight_50x50.svg',
      '39': './mocks/images/Weather_39PartlyCloudyRainNight_50x50.svg',
      '40': './mocks/images/Weather_40MostlyCloudyRainNight_50x50.svg',
      '41': './mocks/images/Weather_41PartlyCloudyThunderstormsNight_50x50.svg',
      '42': './mocks/images/Weather_42MostlyCloudyThunderstormsNight_50x50.svg',
      '43': './mocks/images/Weather_43MostlyCloudySnowDay_50x50.svg',
      '44': './mocks/images/Weather_44MostlyCloudySnowNight_50x50.svg'
    }
  };

  public static contextType = RouterContext;

  private _handleClick() {
    const {openPageOnClick} = this.props.feature;
    this.context.history.push('/' + openPageOnClick);
  }

  public render() {
    // TODO(wmoore): The icons need to be removed from the layout.JSON files, but are there for now so the PFE conversion will work.
    const {
      dailyForecast,
      currentCondition,
      cityName,
      icons,
      localizedClickableAreaText,
      localizedHeaderText,
      localizedTodayText,
      feature
    } = this.props;

    const {clickable} = this.props.feature;
    let newForecast = dailyForecast;

    if (this.props.feature.showCurrentCondition) {
      newForecast = newForecast.slice(1, newForecast.length);
    } else {
      newForecast = newForecast.slice(0, newForecast.length - 1);
      newForecast[0].date = {localizedTodayText};
    }
    if (this.props.feature.clickable) {
      newForecast = newForecast.slice(0, newForecast.length - 1);
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-aalweather-component">
          <Header localizedHeaderText={localizedHeaderText} />
          {this.props.feature.showCurrentCondition ? (
            <Today
              clickable={clickable}
              currentCondition={currentCondition}
              cityName={cityName}
              todayText={localizedTodayText}
              icons={icons}
            />
          ) : null}

          <Forecast
            clickable={clickable}
            onClick={this._handleClick.bind(this)}
            showCurrentCondition={this.props.feature.showCurrentCondition}
            dailyForecast={newForecast}
            icons={icons}
            feature={feature}
            clickableAreaText={localizedClickableAreaText}
            todayText={localizedTodayText}
          />
        </Container>
      </PortalComponent>
    );
  }
}

export default WeatherStyle2;
