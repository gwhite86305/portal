import {AnyAction, combineReducers, Reducer} from 'redux';
import {default as LoadingReducer, State as ILoadingState} from './Loading/reducer';
import * as loadingActions from './Loading/actions';
import {default as CookieAlert} from './CookieAlert/reducer';
import * as CookieAlertActions from './CookieAlert/actions';
import {default as Modal} from './Modal/reducer';
import * as ModalActions from './Modal/actions';

export type IRootState = {
  loading: ILoadingState;
};

// reducer name maps to state tree its responsible for
const rootReducer: Reducer<IRootState> = combineReducers({
  loading: LoadingReducer,
  CookieAlert
});

export default rootReducer;

export interface Actions {
  [key: string]: number | any | undefined;
}

export const Actions: Actions = {
  loading: loadingActions,
  CookieAlertActions,
  ModalActions
};

// TOD export more for 3rd parties?
// and do it i a way that is useful for 3rd parties???
