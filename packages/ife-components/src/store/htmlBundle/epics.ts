import {Action} from 'redux';
import {Observable, from, of} from 'rxjs';
import {exhaustMap, filter, map, catchError} from 'rxjs/operators';
import {combineEpics} from 'redux-observable';
import {getHtmlBundleByCategoryName} from './actions';
import {htmlBundleService} from '../../services/HtmlBundleService';

const getHtmlBundleByCategoryEpic = (actions$: Observable<Action>) =>
    actions$.pipe(
        filter(getHtmlBundleByCategoryName.started.match),
        exhaustMap(action =>
            from(htmlBundleService.getHtmlBundleByCategory(action.payload)).pipe(
                map(result => getHtmlBundleByCategoryName.done({result, params: action.payload})),
                catchError(error => of(getHtmlBundleByCategoryName.failed({params: action.payload, error})))
            )
        )
);

export const htmlBundleEpics = combineEpics(getHtmlBundleByCategoryEpic);