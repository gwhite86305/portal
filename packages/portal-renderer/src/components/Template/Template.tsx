import * as React from 'react';
import {Template as ExternalInterface} from '../common/ComponentsInterface';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {ContainerComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {createComponent} from '../../utils/LoaderCreateComponent';
import * as defaults from './defaultProps.json';
import * as PropTypes from 'prop-types';
import LayoutInterface, {Components} from 'component-utils/dist/types/LayoutInterface';
import {LayoutContext} from 'component-utils/dist/utils/Contexts';

export type ReactElement = React.ReactNode[] | React.ReactNode;

export interface TemplateProps {
  children?: string[];
  templateBodyComponent: ReactElement;
}

const _generateLayout = (
  children: string[],
  templateBodyComponent: ReactElement,
  components: Components
): ReactElement =>
  children.map((componentUid: string) => createComponent(componentUid, {}, components, templateBodyComponent, null));

const Template: React.FunctionComponent<TemplateProps & ExternalInterface> = props => {
  const {children, templateBodyComponent, componentTheme, feature} = props;
  const {components}: LayoutInterface = React.useContext(LayoutContext) as LayoutInterface;

  let templateComponentHierarchy = (
    <PortalComponent componentTheme={componentTheme}>
      <ContainerComponentThemeWrapper>
        {_generateLayout(children || [], templateBodyComponent, components)}
      </ContainerComponentThemeWrapper>
    </PortalComponent>
  );

  return feature.template ? (
    <Template {...components[feature.template].properties} templateBodyComponent={templateComponentHierarchy} />
  ) : (
    templateComponentHierarchy
  );
};

Template.defaultProps = defaults;

export default Template;
