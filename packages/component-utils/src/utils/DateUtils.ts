export const Weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const convertMinutesToClock = (min: number, defaultValue = '---', zeroPadding = true) => {
  if (!min) return {hour: defaultValue, minute: defaultValue};
  if (min < 0.001 && min > 0) return {hour: defaultValue, minute: defaultValue};
  const m = Math.floor(min % 60);
  const h = Math.floor(min / 60);
  if (zeroPadding) {
    if (m < 10 && h < 10) return {hour: `0${h}`, minute: `0${m}`};
    if (h < 10) return {hour: `0${h}`, minute: `${m}`};
  }
  if (m < 10) return {hour: `${h}`, minute: ` 0${m}`};
  return {hour: `${h}`, minute: `${m}`};
};
export const getUTCDate = (date = new Date(), shortYear = false) => {
  const mm = padLeft(date.getUTCMonth() + 1);
  const dd = padLeft(date.getUTCDate());
  let yyyy = date.getUTCFullYear();
  if (shortYear) {
    return `${mm}/${dd}/${padLeft(yyyy % 100)}`;
  }
  return `${mm}/${dd}/${yyyy}`;
};
const padLeft = (input: any, length = 2, pad = '0') => {
  // Convert input number to a string
  let output = `${input}`;
  // Prepend the pad character until its long enough
  while (output.length < length) output = pad + output;
  // Return the newly padded string
  return output;
};
