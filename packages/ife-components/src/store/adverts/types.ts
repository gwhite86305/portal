import {ImageVariants} from '../../components/types';

export interface AdvertItem {
  uid: string;
  type: string;
  title: string;
  subtitle: string;
  targetUrl: string;
  description: string;
  galleryImages?: ImageVariants[];
  titleImages?: ImageVariants[];
  categories: any[];
  attributes?: Record<string, string>;
}
