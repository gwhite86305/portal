import axios from 'axios';
import {push} from 'connected-react-router';
import {generateMessage} from '../../utils/APIError';
import {ActionCreator} from '../action-creator';
import storeAccess from '../../utils/storeAccess';
import {isEnvUXBuilder} from '../../utils/ComponentUtils';
import * as ContentAssetActions from '../contentAssets/actions';
import {updateComponentContent} from '../contentAssets/actions';
import LocalStorageDataType from '../../utils/LocalStorageDataType';
import * as SystemActions from '../system/actions';
import BuilderToPortalMessageType, {BuilderToPortalMessageData} from '../../types/BuilderToPortalMessageType';
import PortalToBuilderMessageType, {PortalToBuilderMessageData} from '../../types/PortalToBuilderMessageType';
import {LayoutInitData} from './reducer';

// Action Creators
export const ActionCreators = {
  requestLayout: new ActionCreator<'requestLayout', any>('requestLayout'),
  receiveLayout: new ActionCreator<'receiveLayout', any>('receiveLayout'),
  builderInitLayout: new ActionCreator<'builderInitLayout', any>('builderInitLayout'),
  builderSelectComponent: new ActionCreator<'builderSelectComponent', any>('builderSelectComponent'),
  changeLocale: new ActionCreator<'changeLocale', any>('changeLocale')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

// locale will fallback to default from the layout, but will use locale in localStorage if available
const _getUserLocale = (layoutLocaleDefault?: string, portalUid: string = 'default'): string => {
  return localStorage[`${LocalStorageDataType.locale}-${portalUid}`] || layoutLocaleDefault || 'en';
};

const _setBrowserLocale = (locale: string) => {
  document.getElementsByTagName('html')[0].setAttribute('lang', locale);
  axios.defaults.headers['Accept-Language'] = locale;
};

const _persistLocale = (locale: string, portalUid: string = 'default') => {
  localStorage.setItem(`${LocalStorageDataType.locale}-${portalUid}`, locale);
  _setBrowserLocale(locale);
};

let _localeLoader: any = null;

export function initStore(localeLoader: any) {
  _localeLoader = localeLoader;
}

export function initLayout() {
  return (dispatch: any, getState: any) => {
    dispatch(ActionCreators.requestLayout.create({}));

    return new Promise(async (resolve, reject) => {
      let [defaultLocaleMessages, system] = await Promise.all([
        _localeLoader.loadDefaultMessages(),
        dispatch(SystemActions.fetchSettings())
      ]);

      if (isEnvUXBuilder()) {
        // initialize the layout with an empty UX, pending initUX event from the builder when it will inject the editing layout
        const userSelectedLocale = _getUserLocale();
        const layoutData = {
          ux: {
            layout: {app: {meta: {}, pages: []}, components: []},
            theme: {},
            locale: []
          },
          userSelectedLocale,
          defaultLocaleMessages,
          system
        };

        await _localeLoader.loadData(userSelectedLocale);
        dispatch(ActionCreators.receiveLayout.create(layoutData));
        resolve(layoutData);
      } else {
        /* TODO mock layout api call for now, tweak to test different layouts for your own needs */

        try {
          const profileUrl = process.env.PROFILE_URL;
          const response = await axios.get(profileUrl || '');

          let layoutJson = response.data;
          const userSelectedLocale = _getUserLocale(
            layoutJson.layout.app.meta.defaultLocale,
            layoutJson.layout.app.meta.portalUid
          );
          _setBrowserLocale(userSelectedLocale);

          const layoutData: LayoutInitData = {
            ux: layoutJson,
            userSelectedLocale,
            defaultLocaleMessages,
            system
          };

          axios.interceptors.request.use((config: any) => {
            const mappedUrl = layoutJson.dynamicMediaMap && layoutJson.dynamicMediaMap[config.url];
            if (mappedUrl) {
              config.url = mappedUrl;
            }
            return config;
          });
          await dispatch(ContentAssetActions.initContentAssets(layoutJson.layout, userSelectedLocale));
          await _localeLoader.loadData(userSelectedLocale);
          dispatch(ActionCreators.receiveLayout.create(layoutData));
          resolve(layoutData);
        } catch (err) {
          dispatch(ActionCreators.receiveLayout.create(generateMessage('Could not retrieve this layout', err)));
          reject(err);
        }
      }
    });
  };
}

export function changeUserLocale(locale: string) {
  return async (dispatch: any, getState: any) => {
    const {layout} = getState().ux;
    await dispatch(ContentAssetActions.initContentAssets(layout, locale));
    await _localeLoader.loadData(locale);

    _persistLocale(locale, layout.app.meta.portalUid);
    dispatch(ActionCreators.changeLocale.create(locale));

    if (isEnvUXBuilder()) {
      sendToBuilder({
        type: PortalToBuilderMessageType.portalLocaleChange,
        data: {locale: locale}
      });
    }
  };
}

export function sendToBuilder(message: PortalToBuilderMessageData) {
  const builderUrl = process.env.UXBUILDER_URL;
  parent.postMessage(message, builderUrl || '');

  return (dispatch: any) => {
    if (message.type === PortalToBuilderMessageType.componentSelected) {
      dispatch(ActionCreators.builderSelectComponent.create(message.data));
    }
  };
}

export function receiveFromBuilder(message: BuilderToPortalMessageData) {
  return (dispatch: any, getState: any) => {
    const {userSelectedLocale, layout} = getState().ux;

    switch (message.type) {
      case BuilderToPortalMessageType.initUx: // initial layout from builder
        const layoutDefaultLocale = message.data.layout.app.meta.defaultLocale;

        const dispatchBuilderInit = () => {
          _persistLocale(layoutDefaultLocale, layout.app.meta.portalUid);
          dispatch(ActionCreators.builderInitLayout.create(message.data));
        };

        dispatch(ContentAssetActions.initContentAssets(message.data.layout, layoutDefaultLocale)).then(() => {
          if (userSelectedLocale !== layoutDefaultLocale) {
            _localeLoader.loadData(layoutDefaultLocale).then(() => dispatchBuilderInit());
          } else {
            dispatchBuilderInit();
          }
        });

        break;
      case BuilderToPortalMessageType.ux: // subsequent and all further layout changes from builder
        const layoutLocale = _getUserLocale(message.data.layout.app.meta.defaultLocale, layout.app.meta.portalUid);

        const dispatchLayoutChange = () => {
          dispatch(
            ActionCreators.receiveLayout.create({
              ux: message.data,
              userSelectedLocale: layoutLocale
            })
          );
        };

        if (userSelectedLocale !== layoutLocale) {
          _localeLoader.loadData(layoutLocale).then(() => dispatchLayoutChange());
        } else {
          dispatchLayoutChange();
        }

        break;
      case BuilderToPortalMessageType.selectComponent:
        dispatch(ActionCreators.builderSelectComponent.create(message.data));
        break;
      case BuilderToPortalMessageType.routeChange:
        dispatch(push('/' + message.data));
        break;
      case BuilderToPortalMessageType.localeChange:
        dispatch(changeUserLocale(message.data));
        break;
      case BuilderToPortalMessageType.contentChange:
        dispatch(updateComponentContent(message.data));
        break;
      case undefined:
        break;
      default:
        console.info('Portal has received a message from the portal-builder it does not recognise', message.type);
    }
  };
}

if (isEnvUXBuilder()) {
  if (window) {
    window.addEventListener(
      'message',
      function(event: any) {
        const store = storeAccess.get();
        if ((event.origin !== process.env.UXBUILDER_URL && event.origin !== location.origin) || store === null) {
          console.info(
            'Portal has received a message from the portal-builder from an invalid origin:',
            event.origin,
            ' expecting:',
            process.env.UXBUILDER_URL,
            event
          );
          return;
        }
        store.dispatch(receiveFromBuilder(event.data));
      },
      false
    );
  }
}
