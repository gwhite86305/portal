import {analyticsReduxMiddleware, mobileReduxMiddleware} from 'component-utils';
import {createBrowserHistory, createHashHistory, History} from 'history';
import {routerMiddleware} from 'connected-react-router';
import {applyMiddleware, compose, createStore} from 'redux';
import {createEpicMiddleware} from 'redux-observable';
import thunk from 'redux-thunk';
import rootReducer from '../store';
import dynostore, {dynamicReducers} from '@redux-dynostore/core';

const {NODE_ENV} = process.env;

let browserHistory: History = createBrowserHistory();
if (process.env.HASH_ROUTES) {
  browserHistory = createHashHistory({
    hashType: 'slash' // the default
  });
}

export const epicMiddleware = createEpicMiddleware();

const router = routerMiddleware(browserHistory);
const composeEnhancers =
  NODE_ENV === 'production' || NODE_ENV === 'test'
    ? compose
    : (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = [thunk, router, analyticsReduxMiddleware, mobileReduxMiddleware, epicMiddleware];

export default function configureStore(initialState: any) {
  return {
    store: createStore(
      rootReducer(browserHistory),
      initialState,
      composeEnhancers(applyMiddleware(...middlewares), dynostore(dynamicReducers()))
    ),
    history: browserHistory,
    epicMiddleware
  };
}
