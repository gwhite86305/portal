import * as React from 'react';
import {Grid} from '@material-ui/core';
import PulsateContainer from '../common/Pulsate';
const Loader: React.FC<any> = props => {
  return (
    <PulsateContainer>
      <Grid container spacing={24} className="movie-details-container">
        <Grid item xs={12} md={8}>
          <div className="poster-container">
            <div className="poster pulsate" />
          </div>
        </Grid>
        <Grid item xs={12} md={4} className="details-container">
          {Array.from(new Array(3)).map((_, index) => (
            <div key={index}>
              <div className="details pulsate" />
            </div>
          ))}
        </Grid>
      </Grid>
    </PulsateContainer>
  );
};

export default Loader;
