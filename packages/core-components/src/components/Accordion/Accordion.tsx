import * as React from 'react';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {Accordion as ExternalInterface} from '../common/ComponentsInterface';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import * as defaults from './defaultProps.json';
import styled from 'styled-components';

const {renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  feature: any;
}

const Container = styled.div`
  width: 100%;
  ${renderThemeProperty('textColor', 'color')};

  .accordion-header {
    ${renderThemeProperty('headerTextColor', 'color')};
    ${renderThemeProperty('headerFontSize', 'font-size')};
    ${renderThemeProperty('seperatorColor', 'border-bottom-color')};
    ${renderThemeProperty('headerFontWeight', 'font-weight')};
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
    padding-top: calc(${(props: any) => props.theme.gutter} * 2.5);
    margin-top: 0;
    margin-bottom: 0;
    border-bottom-width: 1px;
    border-bottom-style: solid;
  }

  .section {
    ${renderThemeProperty('seperatorColor', 'border-bottom-color')};
    position: relative;
    border-bottom-width: 1px;
    border-bottom-style: solid;

    .section-title {
      ${renderThemeProperty('sectionTextColor', 'color')};
      ${renderThemeProperty('sectionFontSize', 'font-size')};
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
      padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
      margin: 0;
      font-weight: bold;
    }

    .section-title-icon {
      cursor: pointer;
    }

    .open {
      ${renderThemeProperty('toggleOpenIconColor', 'color')};
      ::before {
        ${renderThemeProperty('toggleOpenIconColor', 'color')};
      }
    }

    i,
    svg {
      position: absolute;
      right: 0;
      top: calc(${(props: any) => props.theme.gutter} * 1.25);
      ${renderThemeProperty('toggleClosedIconColor', 'color')};
    }

    .fa::before {
      font-size: 1rem;
    }

    p {
      ${renderThemeProperty('paragraphMarginTop', 'margin-top')};
      ${renderThemeProperty('paragraphFontColor', 'color')};
    }
    li {
      ${renderThemeProperty('paragraphFontColor', 'color')};
    }
  }
`;

class Accordion extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      sections: this.props.feature.sections
    };
  }

  componentWillReceiveProps(nextProps: InternalInterface) {
    this.setState({sections: nextProps.feature.sections});
  }

  toggle = (index: number, e: any) => {
    let sections = this.state.sections;
    let toggledSection = {...sections[index]};
    toggledSection.isOpen = !toggledSection.isOpen;
    sections[index] = toggledSection;
    this.setState({sections: sections});
  };

  public render() {
    const {sections} = this.state;
    const {title, useMaterial, anchor} = this.props.feature;
    const upArrow = useMaterial ? <ExpandLess /> : <i className="fa fa-chevron-up open" />;
    const downArrow = useMaterial ? <ExpandMore /> : <i className="fa fa-chevron-down" />;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="core-accordion-component">
          {title && anchor ? (
            <h4 className="accordion-header" id={anchor}>
              {title}
            </h4>
          ) : null}
          {title && !anchor ? <h4 className="accordion-header">{title}</h4> : null}
          {sections.map((section: any, index: number) => (
            <div className="section" key={index}>
              <div
                className="section-title-icon"
                tabIndex={0}
                onKeyPress={this.toggle.bind(this, index)}
                onClick={this.toggle.bind(this, index)}
              >
                <h5 className={section.isOpen ? 'section-title open' : 'section-title'}>{section.title}</h5>
                {section.isOpen ? upArrow : downArrow}
              </div>
              {section.isOpen ? <div dangerouslySetInnerHTML={{__html: section.content || ''}} /> : null}
              {section.isOpen && section.paragraphs ? (
                <div>
                  {section.paragraphs.map(
                    (paragraph: any, paraIndex: number) =>
                      paragraph.content ? (
                        <p
                          style={paragraph.style}
                          key={index + '/' + paraIndex}
                          dangerouslySetInnerHTML={sanitize(paragraph.content)}
                        />
                      ) : (
                        <p key={index + '/' + paraIndex} dangerouslySetInnerHTML={sanitize(paragraph)} />
                      )
                  )}
                </div>
              ) : null}
            </div>
          ))}
        </Container>
      </PortalComponent>
    );
  }
}

export default Accordion;
