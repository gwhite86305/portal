import * as defaultProps from './defaultProps.json';
import * as React from 'react';
import NukaCarousel from 'nuka-carousel';
import {AdvertItem} from './../../store/adverts/types';
import {debounceTime, takeUntil} from 'rxjs/operators';
import {fromEvent, Subject} from 'rxjs';
import {NukaAdvertsCarouselProps, NukaAdvertsCarouselState} from './types';
import {pathOr} from 'ramda';
import {
  StyledAdItemBackground,
  StyledCarousel,
  StyledCta,
  StyledCtaMobile,
  StyledIcon,
  StyledIconMobile,
  StyledInfoContainer,
  StyledInfoSubSesction,
  StyledSubtitle,
  StyledTitle,
  StyledTitleContainer,
  StyledTitleMobile
} from './StyledNukaAdvertsComponents';

class NukaAdvertsCarousel extends React.Component<NukaAdvertsCarouselProps, NukaAdvertsCarouselState> {
  public static defaultProps = defaultProps;
  private _unsubscribe = new Subject();
  observer: IntersectionObserver;
  elementRefs: HTMLDivElement[];
  containerRef: any;

  constructor(props: NukaAdvertsCarouselProps) {
    super(props);
    this.handleResize = this.handleResize.bind(this);
    this.state = {maxWidth: '100%'};
    this.elementRefs = [];
  }

  componentDidMount() {
    fromEvent(window, 'resize')
      .pipe(
        debounceTime(100),
        takeUntil(this._unsubscribe)
      )
      .subscribe(() => this.handleResize());

    const config = {
      root: this.containerRef.current,
      rootMargin: '0px',
      threshold: 0.5
    };
    this.observer = new IntersectionObserver(this.observeEntries.bind(this), config);
    this.elementRefs.forEach((entry: Element) => this.observer.observe(entry));
  }

  private observeEntries(entries: IntersectionObserverEntry[], observer: IntersectionObserver) {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        const item = this.props.items.find((ad: AdvertItem) => ad.uid === entry.target.id);
        this.props.onItemInViewPort && this.props.onItemInViewPort(item!);
        observer.unobserve(entry.target);
      }
    });
  }

  componentWillUnmount() {
    this._unsubscribe.next();
    this._unsubscribe.complete();
    this.observer && this.observer.disconnect();
  }

  renderItems(items: AdvertItem[] = []) {
    const defaultCta = 'Launch';
    return items.map((ad: AdvertItem) => {
      const iconImageUrl = pathOr('', ['galleryImages', 0, 'original', 'url'], ad);
      return (
        <div
          id={ad.uid}
          key={ad.uid}
          className="item-container"
          ref={(elementRef: HTMLDivElement) => this.elementRefs.push(elementRef)}
        >
          <StyledAdItemBackground {...{images: pathOr({}, ['titleImages', 0], ad)}}>
            <StyledInfoContainer>
              {this.props.width === 'sm' || this.props.width === 'xs' ? (
                <React.Fragment>
                  <StyledInfoSubSesction>
                    <StyledIconMobile>
                      <img src={iconImageUrl} />
                    </StyledIconMobile>
                    <StyledTitleContainer>
                      <StyledTitleMobile>{ad.title}</StyledTitleMobile>
                      <StyledSubtitle>{ad.subtitle}</StyledSubtitle>
                    </StyledTitleContainer>
                  </StyledInfoSubSesction>
                  <StyledInfoSubSesction>
                    <StyledCtaMobile onClick={_ => this.openAd(ad)}>
                      {pathOr(defaultCta, ['attributes', 'cta'], ad)}
                    </StyledCtaMobile>
                  </StyledInfoSubSesction>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <StyledInfoSubSesction>
                    <StyledIcon>
                      <img src={iconImageUrl} />
                    </StyledIcon>
                    <StyledTitle>{ad.title}</StyledTitle>
                  </StyledInfoSubSesction>
                  <StyledInfoSubSesction>
                    <StyledSubtitle>{ad.subtitle}</StyledSubtitle>
                    <StyledCta onClick={_ => this.openAd(ad)}>
                      {pathOr(defaultCta, ['attributes', 'cta'], ad)}
                    </StyledCta>
                  </StyledInfoSubSesction>
                </React.Fragment>
              )}
            </StyledInfoContainer>
          </StyledAdItemBackground>
        </div>
      );
    });
  }

  public render() {
    const {feature, items, rest} = this.props;
    return (
      <StyledCarousel
        ref={(ref: any) => (this.containerRef = ref)}
        itemCount={items.length}
        firstItemImages={pathOr({}, [0, 'titleImages', 0], items)}
        {...rest}
      >
        {items.length === 1 ? (
          this.renderItems(items)
        ) : (
          <NukaCarousel
            autoplay={feature.autoplay}
            autoplayInterval={feature.autoplayInterval && +feature.autoplayInterval}
            edgeEasing="easeCircleOut"
            renderBottomCenterControls={this.renderBottomCenterControls}
            renderCenterLeftControls={this.renderCenterLeftControls}
            renderCenterRightControls={this.renderCenterRightControls}
            wrapAround={true}
            dragging={false}
          >
            {this.renderItems(items)}
          </NukaCarousel>
        )}
      </StyledCarousel>
    );
  }

  private renderCenterLeftControls = ({previousSlide}: any) => (
    <button
      aria-label="See previous titles"
      className="control-container"
      onClick={previousSlide}
      children={<i className="arrow arrow--left" />}
    />
  );

  private renderCenterRightControls = ({nextSlide}: any) => (
    <button
      aria-label="See more titles"
      className="control-container"
      onClick={nextSlide}
      children={<i className="arrow arrow--right" />}
    />
  );

  private renderBottomCenterControls = ({slideCount, goToSlide, currentSlide}: any) => (
    <div className="bottom-center-controls">
      {new Array(slideCount).fill(true).map((_, index) => (
        <button
          key={index}
          className={`bullet-button ${currentSlide === index && 'selected'}`}
          onClick={goToSlide.bind(null, index)}
        />
      ))}
    </div>
  );

  private handleResize() {
    this.setState((_: any) => ({maxWidth: `${document.body.clientWidth}px`}));
  }
  private openAd = (adItem: AdvertItem) => {
    this.props.actions.trackView(adItem);
    window.open(adItem.targetUrl, '_blank');
  };
}

export default NukaAdvertsCarousel as any;
