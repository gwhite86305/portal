export interface StoreProps {
  htmlBundleUrl?: string;
  error: boolean;
  isFetching: boolean;
}

export interface DispatchProps {
  getHtmlBundle: (id: string) => void;
}

export type HtmlBundleContainerProps = StoreProps & DispatchProps;

export interface HtmlBundleContainerState {}

export interface HtmlBundleProps {
  htmlBundleUrl: string;
}
