import {ImageVariants, ImageVariantsType} from '../../types';
import {getImageSrcByRatio, getOptimizedImgSrc} from '../utils';

const images: ImageVariants = {
  original: {
    url: 'orignalurl',
    height: 300,
    width: 300
  },
  poster1x: {
    url: 'poster1xurl',
    height: 100,
    width: 100
  },
  poster2x: {
    url: 'poster2xurl',
    height: 200,
    width: 200
  },
  poster3x: {
    url: 'poster3xurl',
    height: 300,
    width: 300
  }
};

describe('utils', () => {
  describe('getImageSrcByRatio', () => {
    it('should return x1 version for the ratio 1', () => {
      const url = getImageSrcByRatio(images, ImageVariantsType.poster, 1);
      expect(url).toEqual(images.poster1x!.url);
    });

    it('should return x2 version for the ratio 1.4', () => {
      const url = getImageSrcByRatio(images, ImageVariantsType.poster, 1.4);
      expect(url).toEqual(images.poster2x!.url);
    });

    it('should return x3 version for the ratio 4', () => {
      const url = getImageSrcByRatio(images, ImageVariantsType.poster, 4);
      expect(url).toEqual(images.poster3x!.url);
    });

    it('should fallback to the orignal if the variant type doesnt exists', () => {
      const url = getImageSrcByRatio(images, ImageVariantsType.audio, 4);
      expect(url).toEqual(images.original!.url);
    });
  });

  describe('getOptimizedImgSrc', () => {
    it('returns x1 variant when size is not defined', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster});
      expect(url).toEqual(images.poster1x!.url);
    });

    it('fallback to original when variant is not defined', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.audio});
      expect(url).toEqual(images.original.url);
    });

    it('fallback to original when all the variants are to small', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 900});
      expect(url).toEqual(images.original!.url);
    });

    it('returns x1 when image size is smaller than x1', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 90});
      expect(url).toEqual(images.poster1x!.url);
    });

    it('returns x2 when image size is bigger than x1', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 110});
      expect(url).toEqual(images.poster2x!.url);
    });

    it('returns x2 when image width is bigger than x1', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 110});
      expect(url).toEqual(images.poster2x!.url);
    });

    it('returns x3 when image size is bigger than x2', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 210});
      expect(url).toEqual(images.poster3x!.url);
    });

    it('returns original when image size is bigger than x3', () => {
      const url = getOptimizedImgSrc({images, variant: ImageVariantsType.poster, imageWidth: 310});
      expect(url).toEqual(images.original!.url);
    });

    it('returns x2 when image ratio is 2', () => {
      const url = getOptimizedImgSrc({
        images,
        variant: ImageVariantsType.poster,
        imageWidth: 90,
        pixelRatio: 2
      });
      expect(url).toEqual(images.poster2x!.url);
    });

    it('returns x3 when image ratio is 3', () => {
      const url = getOptimizedImgSrc({
        images,
        variant: ImageVariantsType.poster,
        imageWidth: 90,
        pixelRatio: 3
      });
      expect(url).toEqual(images.poster3x!.url);
    });

    it('returns original when image ratio is 4', () => {
      const url = getOptimizedImgSrc({
        images,
        variant: ImageVariantsType.poster,
        imageWidth: 90,
        pixelRatio: 4
      });
      expect(url).toEqual(images.original!.url);
    });
  });
});
