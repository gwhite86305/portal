import * as React from 'react';
import {PortalComponent, utils, Currency} from 'component-utils';
import {WifiServiceOptions as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {Button} from 'core-components';
import {pathOr} from 'ramda';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  premiumTiers?: any;
  subscriptions?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) =>
    applyMediaQueriesForStyle(
      'background-color',
      {xs: 'black', md: props.theme.componentTheme.backgroundColor},
      props.theme
    )};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.62);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.62);
  display: flex;
  flex-direction: column;
  div.header {
    flex-direction: column;
    border-width: 0;
    border-style: solid;
    ${renderThemeProperty('dividerColor', 'border-color')};
    h1.header-text {
      margin: 0px;
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'margin-bottom',
          {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*2.5)`},
          props.theme
        )};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.5);
      font-family: ${(props: any) => props.theme.secondaryFontFamily};
      ${renderThemeProperty('headerColor', 'color')};
      ${renderThemeProperty('headerFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.04;
    }
    h3.body-text {
      margin: 0px;
      ${renderThemeProperty('bodyTextColor', 'color')};
      ${renderThemeProperty('bodyTextFontSize', 'font-size')};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.062);
      font-weight: normal;
      line-height: 1.187;
    }
  }
  div.wifi-options {
    display: flex;
    ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
    div.option {
      border-width: 0;
      border-style: solid;
      ${renderThemeProperty('dividerColor', 'border-color')};
      ${(props: any) => applyMediaQueriesForStyle('border-top-width', {xs: '1px', md: '0px'}, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('border-right-width', {xs: '0px', md: '1px'}, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('border-radius', {xs: '4px', md: '0'}, props.theme)};
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'background-color',
          {xs: props.theme.componentTheme.backgroundColor, md: 'none'},
          props.theme
        )};
      flex-grow: 1;
      flex-basis: 0;
      flex-direction: column;
      padding: 15px 10px 10px;
      ${(props: any) => applyMediaQueriesForStyle('margin-bottom', {xs: '20px', md: '0'}, props.theme)};
      div.top {
        ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'column'}, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('text-align', {xs: 'left', md: 'center'}, props.theme)};
        display: flex;
        justify-content: space-between;
        div.title-cost {
          display: flex;
          ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'row', md: 'column'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('max-width', {xs: '100%', md: 'inherit'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('width', {xs: '100%', md: 'inherit'}, props.theme)};
          ${(props: any) =>
            applyMediaQueriesForStyle('justify-content', {xs: 'space-between', md: 'initial'}, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('align-items', {xs: 'center', md: 'initial'}, props.theme)};
          h4.title {
            ${renderThemeProperty('optionTitleColor', 'color')};
            ${(props: any) =>
              applyMediaQueriesForStyle(
                'font-size',
                {xs: '20px', md: props.theme.componentTheme.optionTitleFontSize},
                props.theme
              )};
            word-wrap: break-word;
            margin: 0px;
            line-height: 1.25;
            ${(props: any) => applyMediaQueriesForStyle('line-height', {xs: '1', md: '1.25'}, props.theme)};
            font-weight: 600;
            height: 30px;
            display: flex;
            align-items: flex-start;
            justify-content: center;
            margin-bottom: 5px;
          }
          h5.cost {
            margin: 0;
            ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '0'}, props.theme)};
            ${(props: any) =>
              applyMediaQueriesForStyle(
                'font-size',
                {xs: '32px', md: props.theme.componentTheme.optionCostFontSize},
                props.theme
              )};
            ${renderThemeProperty('optionCostColor', 'color')};
            line-height: 1;
            word-wrap: break-word;
            ${renderThemeProperty('optionCostFontWeight', 'font-weight')};

            .ratio-text {
              ${(props: any) => applyMediaQueriesForStyle('font-size', {xs: '14px', md: '12px'}, props.theme)};
              line-height: 1;
              ${(props: any) => applyMediaQueriesForStyle('vertical-align', {xs: '12px', md: '12px'}, props.theme)};
            }
          }
        }
      }
      div.bottom {
        align-items: center;
        line-height: 1;
        ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '5px', md: '7px'}, props.theme)};
        span.description {
          display: inline-block;
          ${(props: any) =>
            applyMediaQueriesForStyle(
              'font-size',
              {xs: '14px', md: props.theme.componentTheme.optionDescriptionFontSize},
              props.theme
            )};
          ${(props: any) => applyMediaQueriesForStyle('line-height', {xs: '18px', md: '18px'}, props.theme)};
          ${renderThemeProperty('optionDescriptionColor', 'color')};
          ${(props: any) => applyMediaQueriesForStyle('min-height', {xs: '25px', md: '77px'}, props.theme)};
          text-align: center;
        }
      }
      div.button-wrap {
        ${(props: any) => applyMediaQueriesForStyle('margin-top', {xs: '10px'}, props.theme)};

        button {
          ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.buttonWidth, props.theme)};
          ${(props: any) => applyMediaQueriesForStyle('height', {xs: '50px', md: '30px'}, props.theme)};
          margin: 0 auto;
          white-space: nowrap;
          justify-content: center;
          font-size: 18px;
          ${(props: any) => applyMediaQueriesForStyle('font-weight', {xs: '500', md: '600'}, props.theme)};
          ${renderThemeProperty('buttonBorderRadius', 'border-radius')};
          ${(props: any) =>
            applyMediaQueriesForStyle(
              'height',
              {xs: '#0078d1', md: props.theme.componentTheme.buttonBackgroundColor},
              props.theme
            )};
          ${renderThemeProperty('buttonTextColor', 'color', true)};
          ${renderThemeProperty('buttonTextFontSize', 'font-size')};
        }
      }
    }
    div.option:last-child {
      border-right-width: 0px;
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'padding-bottom',
          {xs: `calc(${props.theme.gutter}*.625)`, md: `calc(${props.theme.gutter})`},
          props.theme
        )};
    }
    div.option:first-child {
      border-top-width: 0px;
    }
  }
`;

const HeaderSection = (props: any) => {
  const {header, body} = props;
  return (
    <div className="header">
      {header ? <h1 className="header-text">{header}</h1> : null}
      {body ? <h3 className="body-text">{body}</h3> : null}
    </div>
  );
};

const DetailsLink1 = styled.a`
  color: #0078d2;
  font-size: 14px;
  text-align: center;
  text-decoration: none;
  font-weight: 600;
  display: inherit;

  &:active {
    color: #0078d2;
  }
`;

const DetailsSpacer1 = styled.div`
  ${(props: any) => applyMediaQueriesForStyle('height', {xs: '20px', md: '14px'}, props.theme)};
  display: block;
`;

const PurchaseOption = (props: any) => {
  const {
    tier,
    showOptionDescription,
    onClick,
    currencyCode,
    locale,
    minimumFractionDigits,
    maximumFractionDigits
  } = props;
  const costInteger = Math.trunc(tier.cost);
  let costDecimal: any = Math.trunc((tier.cost - costInteger) * 100);
  costDecimal = costDecimal < 10 ? (costDecimal === 0 ? '' : `0${costDecimal}`) : costDecimal;
  return (
    <div className="option">
      <div className="top">
        <div className="title-cost">
          <h4 className="title">{tier.description}</h4>
          <h5 className="cost">
            <Currency
              currency={currencyCode}
              value={costInteger}
              locale={locale}
              minimumFractionDigits={minimumFractionDigits}
              maximumFractionDigits={maximumFractionDigits}
            />
            <span className="ratio-text">{costDecimal}</span>
          </h5>
        </div>
      </div>

      <div className="bottom">
        {showOptionDescription && pathOr(false, ['metadata', 'description'], tier) ? (
          <span className="description">{tier.metadata.description}</span>
        ) : null}
        {pathOr(false, ['metadata', 'details'], tier) ? (
          <DetailsLink1 href={tier.metadata.details.url}>{tier.metadata.details.text}</DetailsLink1>
        ) : (
          <DetailsSpacer1 />
        )}
        <div className="button-wrap">
          <Button
            aria-label={`${tier.description}, ${pathOr('Buy', ['metadata', 'button'], tier)}`}
            isPrimary={true}
            onClick={(event: any) => {
              onClick(tier.id);
            }}
          >
            {pathOr('Buy', ['metadata', 'button'], tier)}
          </Button>
        </div>
      </div>
    </div>
  );
};

const VoucherSection = (props: any) => {
  const {voucherTitle, voucherText, voucherButtonText, showOptionDescription, voucherDescripiton, onClick} = props;
  return (
    <div className="option">
      <div className="top">
        <div className="title-cost">
          <h4 className="title">{voucherTitle}</h4>
          <h5 className="cost">{voucherText}</h5>
        </div>
        <div className="button-wrap">
          <Button isPrimary={false} onClick={onClick} aria-label={voucherText}>
            {voucherButtonText}
          </Button>
        </div>
      </div>
      {showOptionDescription && voucherDescripiton ? (
        <div className="bottom">
          <span className="description">{voucherDescripiton}</span>
        </div>
      ) : null}
    </div>
  );
};

class WifiServiceOptions extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,
    premiumTiers: []
  };

  public render() {
    let {
      premiumTiers,
      subscriptions,
      onPurchaseClick,
      onSubscriptionClick,
      onVoucherClick,
      header,
      body,
      voucherTitle,
      voucherText,
      voucherDescripiton,
      voucherButtonText,
      currencyCode,
      locale
    } = this.props;
    let {purchaseOptionSortBy, purchaseOptionOrderBy, voucherAvailable, showOptionDescription} = this.props.feature;
    if (purchaseOptionSortBy == 'cost') {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.cost - tier2.cost);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.cost - tier1.cost);
      }
    } else if (purchaseOptionSortBy == 'duration') {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.duration - tier2.duration);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.duration - tier1.duration);
      }
    } else {
      if (purchaseOptionOrderBy == 'ascending') {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier1.name - tier2.name);
      } else {
        premiumTiers = premiumTiers.sort((tier1: any, tier2: any) => tier2.name - tier1.name);
      }
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-WifiServiceOptions-component">
          {header || body ? <HeaderSection header={header} body={body} /> : null}
          <div className="wifi-options">
            {premiumTiers.map((tier: any, i: number) => (
              <PurchaseOption
                key={i}
                tier={tier}
                showOptionDescription={showOptionDescription}
                onClick={onPurchaseClick}
                currencyCode={currencyCode}
                locale={locale}
              />
            ))}
            {subscriptions.map((tier: any, i: number) => (
              <PurchaseOption
                key={i}
                tier={tier}
                showOptionDescription={showOptionDescription}
                onClick={onSubscriptionClick}
                currencyCode={currencyCode}
                locale={locale}
              />
            ))}
            {voucherAvailable ? (
              <VoucherSection
                voucherTitle={voucherTitle}
                voucherText={voucherText}
                voucherDescripiton={voucherDescripiton}
                voucherButtonText={voucherButtonText}
                onClick={onVoucherClick}
                showOptionDescription={showOptionDescription}
              />
            ) : null}
          </div>
        </Container>
      </PortalComponent>
    );
  }
}

export default WifiServiceOptions;
