import {playItem, setPlayerPage} from '../../store/mediaplayer/actions.player';
import {
  loadPlaylistFromStorage,
  pauseCurrentInPlaylist,
  playCurrentInPlaylist,
  playNextInPlaylist,
  playPreviousInPlaylist,
  removeAllItemFromPlaylist,
  removeItemFromPlaylist
} from '../../store/mediaplayer/actions.playlist';
import {MEDIA_PLAYER_STATUS} from '../../store/mediaplayer/types';
import {Playlist as ExternalInterface} from '../common/ComponentsInterface';
import {AudioItem} from './../../store/audio/types';
import {VideoItem} from './../../store/videos/types';
import {selectPlayListItems} from '../../store/mediaplayer/selector';

export interface PlaylistContainerState {
  isModalOpen: boolean;
  shouldInitialize: boolean;
}
export interface PlaylistProps {
  componentTheme?: any;
  feature?: any;
  currentItem: VideoItem | AudioItem;
  isModalOpen: boolean;
  isPlaying: boolean;
  items: (VideoItem | AudioItem)[];
  mountPoint: any;
  onClearPlaylist: typeof removeAllItemFromPlaylist;
  onItemPlay: typeof playItem;
  onItemPause: typeof pauseCurrentInPlaylist;
  onRemoveItem: typeof removeItemFromPlaylist;
  toggleModalVisibility(): any;
}
export interface StateProps {
  items: (VideoItem | AudioItem)[];
  isPlaying: boolean;
  currentItem: any;
  playlistLoading: boolean;
}
export interface DispatchProps {
  pauseCurrentInPlaylist: typeof pauseCurrentInPlaylist;
  playCurrentInPlaylist: typeof playCurrentInPlaylist;
  playItem: typeof playItem;
  playNextInPlaylist: typeof playNextInPlaylist;
  playPreviousInPlaylist: typeof playPreviousInPlaylist;
  removeAllItems: typeof removeAllItemFromPlaylist;
  removeItem: typeof removeItemFromPlaylist;
  setPlayerPage: typeof setPlayerPage;
  trackPlay: (item: AudioItem | VideoItem) => void;
  loadPlaylist: () => Promise<never>;
}
export interface PlaylistContainerProps extends StateProps, DispatchProps, ExternalInterface {}

export interface EpisodeFormatterProps {
  episode: VideoItem;
}
