import * as defaults from './defaultProps.json';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import {PortalComponent, utils} from 'component-utils';
import {Button as ExternalInterface} from '../common/ComponentsInterface';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {renderChildComponent} from 'component-utils/dist/utils/ComponentUtils';
import styled from 'styled-components';
import {RouterContext, IRouter} from 'component-utils/dist/utils/Contexts';

const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  isPrimary?: boolean;
  isDisabled?: boolean;
  localizedText?: string;
  onClick?: (e: any) => void;
}

const ButtonContainer = styled.button`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  ${renderThemeProperty('fontSize', 'font-size')};
  ${renderThemeProperty('fontFamily', 'font-family')};
  ${renderThemeProperty('fontWeight', 'font-weight')};
  ${renderThemeProperty('textAlign', 'text-align')};
  ${renderThemeProperty('verticalTextAlign', 'vertical-align')};
  ${renderThemeProperty('buttonBorder', 'border')};
  ${renderThemeProperty('borderRadius', 'border-radius')};

  ${(props: any) => applyMediaQueriesForStyle('border-width', props.theme.componentTheme.borderWidth, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};

  &:active {
    outline: 0;
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
  }

  &.primary-button,
  &.primary-button div {
    ${renderThemeProperty('backgroundColor', 'background-color')};
    ${renderThemeProperty('textColor', 'color')};
    ${renderThemeProperty('primaryBorderColor', 'border-color')};

    transition: none;
  }
  &.primary-button {
    &:hover,
    &:focus {
      ${renderThemeProperty('hoverBackgroundColor', 'background-color')};
      ${renderThemeProperty('hoverTextColor', 'color')};
      ${renderThemeProperty('hoverBorderColor', 'border-color')};
      a,
      div {
        ${renderThemeProperty('hoverBackgroundColor', 'background-color')};
        ${renderThemeProperty('hoverTextColor', 'color')};
      }
    }
  }

  &.secondary-button,
  &.secondary-button div {
    transition: none;
    ${renderThemeProperty('secondaryBackgroundColor', 'background-color')};
    ${renderThemeProperty('secondaryTextColor', 'color')};
    ${renderThemeProperty('secondaryBorderColor', 'border-color')};
  }
  &.secondary-button {
    &:hover,
    &:focus {
      ${renderThemeProperty('secondaryHoverBackgroundColor', 'background-color')};
      ${renderThemeProperty('secondaryHoverTextColor', 'color')};
      ${renderThemeProperty('secondaryhoverBorderColor', 'border-color')};
      a,
      div {
        ${renderThemeProperty('secondaryHoverBackgroundColor', 'background-color')};
        ${renderThemeProperty('secondaryHoverTextColor', 'color')};
      }
    }
  }
  &.disabled {
    cursor: not-allowed;
  }

  &.underline-hover:hover {
    text-decoration: underline;
  }
`;

class Button extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  public static contextType = RouterContext;

  constructor(props: any) {
    super(props);
    this._onClick = this._onClick.bind(this);
  }

  public render() {
    const {
      isPrimary,
      isDisabled,
      onClick,
      className,
      componentTheme,
      feature,
      localizedText,
      children,
      ...rest
    } = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <ButtonContainer
          onClick={this._onClick}
          className={
            'core-button-component ' +
            (isDisabled ? 'disabled ' : '') +
            (isPrimary || feature.isButtonPrimary ? 'primary-button ' : 'secondary-button ') +
            (componentTheme ? (componentTheme.hoverUnderline ? 'underline-hover' : '') : '') +
            (className || '')
          }
          {...rest}
        >
          {children && Array.isArray(children) && children.length
            ? renderChildComponent(0, children, localizedText ? {localizedText: localizedText} : null)
            : this.props.children && (this.props.children as any).length
            ? this.props.children
            : localizedText}
        </ButtonContainer>
      </PortalComponent>
    );
  }

  private _onClick(e: any) {
    if (this.props.isDisabled) {
      return;
    }
    let {openPageOnClick, externalUrl, openInNewTab} = this.props.feature;

    if (externalUrl) {
      return window.open(/^www\./.test(externalUrl) ? `//${externalUrl}` : externalUrl, '_blank');
    }

    if (openPageOnClick) {
      return openInNewTab
        ? window.open(process.env.HASH_ROUTES ? `/#/${openPageOnClick}` : `/${openPageOnClick}`, '_blank')
        : this.context.history.push(`/${openPageOnClick}`);
    }
    this.props.onClick && this.props.onClick(e);
  }
}

export default withLocaleProps(Button, [{localizedText: 'core.Button.defaultText'}]) as any;
