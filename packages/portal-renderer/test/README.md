# Level of testing

1. Each page needs to have a screenshot. Please do not include storybook snapshot in the test.
   - Each customer portal is different and they are built with several components, so changing one component for one
     portal might break another portal. Therefore, each portal page should have a screenshot to avoid such incident. We
     can use puppeteer's setViewport function to take the screenshot for different screen size.
2. Each functional flow should have Zephyr tests cases including rainy day & sunny day scenario.
3. Each functional flow (like, upgrade to premium, purchase the service) should have visual regression test included (If
   possible)
   - The visual regression test will ensure that changes made for one portal are not going to affect another portal's
     functionality. Please leverage amen-mock-server to mock the API when preparing the regression test.
4. Each utility function should have a unit test with it.

#Screenshot Testing Procedure

1. Write/update test for a specific airline in packages/portal-renderer/test.
   - [Puppeteer documentation](https://github.com/GoogleChrome/puppeteer/blob/master/docs api.md) is also available on
     github when modifiying or creating an airline snapshot test.
   - If necessary, add the .env file in portal/env
2. Add references to the snapshot test so it can be used
   - Add the individual test command to the makefile
   - Add command for individual test in packages/portal-renderer/package.json
   - Add new test to the test all command
3. Run 'make test' from top of portal directory to generate screenshots. If old screenshots exist, delete them and new
   screenshots will be generated. **Note 'make test' should be used to generate snapshots, as subcommands such as npm
   run test-e2e:jbu may return snapshots that are a few pixels off**

Notes:

- 'npm run test-e2e:<airline>' (ex. npm run test-e2e:jbu) can be used to test the snapshot tests individually from
  portal/packages/portal-renderer, but photos should be deleted and regenerated from 'make test' when finalized (note: on macOS this may not generate the correct screenshots so avoid - better off running thru the make command)
- JBU's serviceTierId is set to 158, PED Free, instead of 157, PED Default, to test the homepage, TOS, and FAQ
