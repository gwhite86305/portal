#!/bin/bash
# simple script to build a portal and put built portal in an S3 bucket

# following can be set
PORTAL_ENV=${PORTAL_ENV:-"ife-aa"}
GROUPCODE=${GROUPCODE:-"aal"}
S3_PORTAL_BUCKET=${S3_PORTAL_BUCKET:-"cloudstore-env-staging"}

# defaults if not run on Jenkins
BUILDERID=${RUN_DISPLAY_URL:-$(hostname)}
PORTAL_BUILD_NUMBER=${BUILD_NUMBER:-"1"}

# following depend on above variables
GIT_COMMIT_HASH=$(git log -1 --pretty=format:"%H")
GIT_COMMIT_MESSAGE=$(git log -1 --pretty=format:"%h - %an, %ad : %s")
BUILD_READABLE_TIME=$(date +%Y-%m-%d-%H-%M)
BUILD_HASH=${GROUPCODE}__${BUILD_READABLE_TIME}__${PORTAL_BUILD_NUMBER}__${GIT_COMMIT_HASH}
PORTAL_ZIP=${BUILD_HASH}.zip

# build
echo "
\n********************\
\n***  SOURCE DIR  ***\
\n********************"
ls -l

echo "
\n********************\
\n***     ENVS     ***\
\n********************"
env

echo "
\n********************\
\n*** INSTALL DEPS ***\
\n********************"
npm run clean
npm install
npm run bootstrap

echo "
\n********************\
\n***   IFE ENVS   ***\
\n********************"
cat env/test.${PORTAL_ENV}.env

echo "
\n********************\
\n***  BUILD IFE   ***\
\n********************"
npm run build:${PORTAL_ENV}

echo "
\n********************\
\n*** BUILD FOLDER ***\
\n********************"
ls -ld packages/portal-renderer/dist

echo "
\n********************\
\n***  BUILD HASH  ***\
\n********************"
echo ${BUILD_HASH}
echo ${PORTAL_ZIP}

echo "
\n*************************\
\n*** CREATE BUILD FILE ***\
\n*************************"
echo ${BUILD_HASH} > packages/portal-renderer/dist/build.txt
ls -ld packages/portal-renderer/dist/build.txt
cat packages/portal-renderer/dist/build.txt


# zip and put on S3
echo "
\n*********************\
\n*** ZIP ARTIFACTS ***\
\n*********************"
# tar -cvzf ${PORTAL_TARBALL}  --exclude stats.json --directory=packages/portal-renderer/dist/ ./
pushd packages/portal-renderer/dist/
zip -qr ../../../${PORTAL_ZIP} * --exclude stats.json
popd
ls -ld ${PORTAL_ZIP}

echo "
\n*************************\
\n*** DESCRIBE ARTIFACTS ***\
\n*************************"
jq --arg key0 'groupcode' --arg value0 "${GROUPCODE}" --arg key1 'builderid' --arg value1 "${BUILDERID}" --arg key2 'version' --arg value2 "${PORTAL_BUILD_NUMBER}" --arg key3 'changelog' --arg value3 "${GIT_COMMIT_MESSAGE}" --arg key4 'file' --arg value4 "${PORTAL_ZIP}" '. | .[$key0]=$value0 | .[$key1]=$value1 | .[$key2]=$value2 | .[$key3]=$value3 | .[$key4]=$value4' <<<'{}' >> ${PORTAL_ZIP}-meta.json

cat ${PORTAL_ZIP}-meta.json

echo "
\n*************************\
\n*** PUBLISH ARTIFACTS ***\
\n*************************"
aws s3 cp ${PORTAL_ZIP} s3://${S3_PORTAL_BUCKET}/for-upload/portals/${GROUPCODE}/
aws s3 cp ${PORTAL_ZIP}-meta.json s3://${S3_PORTAL_BUCKET}/for-upload/portals/${GROUPCODE}/
rm ${PORTAL_ZIP} ${PORTAL_ZIP}-meta.json

exit $?

