import {prop, uniqBy} from 'ramda';
import {Action, ActionCreators} from './actions';
import {IVideosState, VideoGroup, VideosConfig} from './types';
import {merge, union} from 'lodash-es';
import localStorageService from '../../services/LocalStorageService';
import Norm from '../../utils/Norm';

const uniqueById = (collection: VideoGroup[]): VideoGroup[] => uniqBy(prop('uid'), collection);

export const initialState: IVideosState = {
  error: null,
  isLoadingAds: false,
  isLoadingSeries: false,
  isLoadingSynopsis: false,
  items: [],
  selectedItem: undefined,
  preRollAds: {},
  seriesFetchCompleted: false,
  videoNotFound: false,
  seriesCategories: [],
  videosConfig: {
    byId: {},
    allIds: []
  }
};

const updateVideoConfig = (state: IVideosState, payload: VideosConfig): IVideosState => {
  const videosConfig = Norm.merge(state.videosConfig, payload);
  localStorageService.writeData('videosConfig', videosConfig);
  return {
    ...state,
    videosConfig
  };
};

export default function reducer(state: IVideosState = initialState, action: Action): IVideosState {
  const {type, payload} = action;
  switch (type) {
    // Get Series
    case ActionCreators.getSeriesStart.type:
      return {...state, isLoadingSeries: true};
    case ActionCreators.getSeriesSuccess.type:
      return {
        ...state,
        items: uniqueById([...state.items, ...payload]),
        isLoadingSeries: false,
        seriesFetchCompleted: true
      };
    case ActionCreators.getSeriesError.type:
      return {...state, isLoadingSeries: false, seriesFetchCompleted: true};
    // Get VideoById
    case ActionCreators.getVideoByIdStart.type:
      return {...state, videoNotFound: false, isLoadingSynopsis: true, selectedItem: undefined};
    case ActionCreators.getVideoByIdSuccess.type:
      return {
        ...state,
        videoNotFound: false,
        selectedItem: payload,
        isLoadingSynopsis: false
      };
    case ActionCreators.getVideoByIdError.type:
      return {...state, videoNotFound: true, isLoadingSynopsis: false, selectedItem: undefined};
    // PreRollAds
    case ActionCreators.getPreRollAdsStart.type:
      return {...state, isLoadingAds: true};
    case ActionCreators.getPreRollAdsSuccess.type:
      return {
        ...state,
        error: false,
        isLoadingAds: false,
        preRollAds: {
          ...state.preRollAds,
          [payload.videoId]: payload.ads
        }
      };
    case ActionCreators.updateVideoConfig.type:
      return updateVideoConfig(state, payload);
    case ActionCreators.loadVideosConfig.type:
      return {
        ...state,
        videosConfig: payload
      };
    case ActionCreators.getPreRollAdsError.type:
      return {
        ...state,
        error: true,
        isLoadingAds: false,
        preRollAds: {
          ...state.preRollAds,
          [action.payload.videoId]: []
        }
      };
    case ActionCreators.getVideoCategories.type:
      return state;
    case ActionCreators.getVideoCategoriesSuccess.type:
      const {seriesCategories} = payload;
      return {
        ...state,
        seriesCategories
      };
    case ActionCreators.getVideoCategoriesFailure.type:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
}
