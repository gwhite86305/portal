import {utils} from 'component-utils';

export const audioPlayer = new Audio();

export const playAudioPlayer = () => {
  if (utils.mobileSdk.isUsingMobileSdk()) {
    return;
  }
  if (audioPlayer.paused) {
    audioPlayer.play();
  }
};

export const pauseAudioPlayer = () => {
  if (utils.mobileSdk.isUsingMobileSdk()) {
    return;
  }
  audioPlayer.pause();
};
