import * as React from 'react';
import {utils} from 'component-utils';
import {fetchDevice, setDeviceLanguage} from '../../store/device/actions';
import {Poller} from 'core-components';
import Device from '../../types/Device';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';
import * as _ from 'lodash';
import {AnalyticsLogger} from 'component-utils/dist/analytics';
import {pathOr} from 'ramda';

const connectWrapper = utils.connectWrapper.default;
const Analytics = new AnalyticsLogger();

export interface ExternalInterface {
  url: string;
  interval: number;
  className?: string;
  children?: any;
}

export interface InternalInterface {
  device?: any;
  defaultLocaleLanguage: string;
  userSelectedLocale: string;
}

export interface DispatchProps {
  fetchDevice(url: string, oldDevice: Device): any;
  setDeviceLanguage(url: string, defaultLocaleLanguage: string): any;
}

interface FinalProps extends ExternalInterface, InternalInterface, DispatchProps {}

@connectWrapper(
  (state: any, ownProps: ExternalInterface) => {
    const fromState: Partial<FinalProps> = {};
    if (state && state.ux && state.ux.settings && state.ux.settings.ifc) {
      if (state.ux.settings.ifc.deviceUrl) fromState.url = state.ux.settings.ifc.deviceUrl;
      if (state.ux.settings.ifc.devicePollRate) fromState.interval = state.ux.settings.ifc.devicePollRate;
    }
    fromState.device = state.device || {};
    const defaultLocaleLanguage = pathOr('en', ['ux', 'layout', 'app', 'meta', 'defaultLocale'], state);
    const userSelectedLocale = pathOr(defaultLocaleLanguage, ['ux', 'userSelectedLocale'], state);
    return {...ownProps, ...fromState, defaultLocaleLanguage, userSelectedLocale};
  },
  (dispatch: any, ownProps: ExternalInterface): DispatchProps => ({
    fetchDevice: (url: string, oldDevice: Device) => {
      dispatch(fetchDevice(url, oldDevice));
    },
    setDeviceLanguage: (url: string, defaultLocaleLanguage: any) => {
      dispatch(setDeviceLanguage(url, defaultLocaleLanguage));
    }
  })
)
export default class DevicePoller extends React.Component<ExternalInterface, any> {
  public componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
    const props: FinalProps = this.props as FinalProps;
    const oldDevice: Device = prevProps.device;
    const newDevice: Device = props.device;

    // Log DetectedDeviceType change to Analytics
    if (_.get(newDevice, 'typeId') != _.get(oldDevice, 'typeId')) {
      Analytics.DeviceType(_.get(newDevice, 'typeId'));
    }

    const defaultLocaleLanguage = props.defaultLocaleLanguage;
    const userSelectedLocale = props.userSelectedLocale;
    const deviceCurrentLanguage = newDevice.language;
    if (
      oldDevice &&
      oldDevice.hasOwnProperty('flightOrigin') &&
      oldDevice.hasOwnProperty('flightDestination') &&
      oldDevice.hasOwnProperty('flightNumber') &&
      oldDevice.hasOwnProperty('profileHash')
    ) {
      const shouldRefresh = !(
        oldDevice.flightOrigin === newDevice.flightOrigin &&
        oldDevice.flightDestination === newDevice.flightDestination &&
        oldDevice.flightNumber === newDevice.flightNumber &&
        oldDevice.profileHash === newDevice.profileHash
      );
      if (shouldRefresh && !isEnvUXBuilder()) {
        window.location.href = '/';
      }
    }
    if (deviceCurrentLanguage && userSelectedLocale !== deviceCurrentLanguage) {
      props.setDeviceLanguage(props.url, userSelectedLocale);
    }
  }

  public render() {
    const props: FinalProps = this.props as FinalProps;
    return (
      <Poller
        action={() => props.fetchDevice(props.url, props.device)}
        interval={props.interval}
        children={props.children}
      />
    );
  }
}
