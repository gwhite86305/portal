import {ActionCreator} from '../action-creator';
import {SystemMessage, SystemSettings} from './reducer';

export const ActionCreators = {
  requestSettings: new ActionCreator<'requestSettings', any>('requestSettings'),
  receiveSettings: new ActionCreator<'receiveSettings', any>('receiveSettings'),
  toggleSystemMessage: new ActionCreator<'toggleSystemMessage', any>('toggleSystemMessage')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

/*
* TODO for now we assume that currencies supported will come from backed (USD as default), until we determine best approach to support these.
* Most likely this will be part of the init process for each of IFE and IFC, but centralize in this "system" store for now...
* */
export function fetchSettings() {
  return (dispatch: any, getState: any) => {
    const {systemSettings, userSettings} = getState().system;
    if (systemSettings !== null) {
      dispatch(ActionCreators.receiveSettings.create({systemSettings, userSettings}));
    } else {
      dispatch(ActionCreators.requestSettings.create({}));
      return new Promise(resolve => {
        const meta = {
          currencies: {default: 'USD', currencies: ['USD']}
        };
        resolve({data: meta});
      }).then((resp: any) => {
        const {currencies} = resp.data;

        const newSystemSettings: Partial<SystemSettings> = {
          currencies: currencies.currencies,
          systemDefaultCurrency: currencies.default
        };

        return dispatch(
          ActionCreators.receiveSettings.create({
            systemSettings: newSystemSettings
          })
        );
      });
    }
  };
}

export function showMessage(systemMessage: SystemMessage, timeout: number | null = 3000) {
  return (dispatch: any, getState: any) => {
    const {system} = getState();
    if (!system.systemMessage) {
      dispatch(ActionCreators.toggleSystemMessage.create(systemMessage));

      if (timeout) {
        setTimeout(() => {
          dispatch(hideMessage());
        }, timeout);
      }
    }
  };
}

export function hideMessage() {
  return (dispatch: any, getState: any) => {
    const {system} = getState();
    if (system.systemMessage) {
      dispatch(ActionCreators.toggleSystemMessage.create(null));
    }
  };
}
