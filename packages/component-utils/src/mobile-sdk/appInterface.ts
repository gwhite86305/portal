declare var window: any;

export interface WebAppInterface {
  update(action: string, payload: any): void;
}

export class IosAppInterface implements WebAppInterface {
  public update(action: string, payload: any): void {
    try {
      window.webkit.messageHandlers.redux.postMessage(JSON.stringify({action, payload}));
    } catch (err) {}
  }
}

declare var android: WebAppInterface;

export class AndroidAppInterface implements WebAppInterface {
  public update(action: string, payload: any): void {
    try {
      android.update(action, JSON.stringify(payload));
    } catch (err) {}
  }
}
