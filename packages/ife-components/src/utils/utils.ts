// tslint:disable:no-unused-expression
import {uniqBy, prop} from 'ramda';
import axios, {AxiosInstance, AxiosPromise, AxiosError} from 'axios';
import Session from './Session';
import * as endPointConfig from './config.json';
import { errorCodeService } from '../services/ErrorCodeService';

interface IConfig {
  [property: string]: string;
}

const axiosInstanceV2: AxiosInstance = axios.create({
  timeout: 5000,
  headers: {
    Accept: 'application/vnd.viasat.ife.v2.1+json;charset=UTF-8',
    'Content-Type': 'application/vnd.viasat.ife.v2.1+json;charset=UTF-8'
  }
});

axiosInstanceV2.interceptors.request.use(config => {
  if (Session.getToken()) {
    config.headers[Session.headerKey] = Session.getToken();
  }
  return config;
});

axiosInstanceV2.interceptors.response.use(response => {
  const newSessionToken = response.headers[Session.headerKey];
  if (newSessionToken) {
    Session.setToken(newSessionToken);
  }
  return response;
});

// intercept error and then parse the errors to locaized messages
axiosInstanceV2.interceptors.response.use(response => (response), (error) => {
  error.response.data.errors = errorCodeService.localiseErrors(error.response.data.errors)
  return Promise.reject(error);
})

const axiosWithBaseUrl = {
  get: (path: string, params?: any): AxiosPromise =>
    axiosInstanceV2.get(`${(endPointConfig as IConfig).API_IFE_BASE_URL}${path}`, {params: params}),
  post: (path: string, data: any): AxiosPromise =>
    axiosInstanceV2.post(`${(endPointConfig as IConfig).API_IFE_BASE_URL}${path}`, data)
};

export {axiosInstanceV2, Session, axiosWithBaseUrl};

export const uniqueById = (collection: any): any => uniqBy(prop('uid'), collection);
