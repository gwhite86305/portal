import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {Weekdays} from 'component-utils/dist/utils/DateUtils';
import {GetTempFC} from 'component-utils/dist/utils/TemperatureConverter';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {Weather as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  dailyForecast: any;
  currentCondition: any;
  onCityClick: any;
  onUpClick: any;
  onDownClick: any;
  icons: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('primaryTextColor', 'color')};
  ${(props: any) => applyMediaQueriesForStyle('flex-direction', {xs: 'column', md: 'row'}, props.theme)};
  justify-content: space-between;
  font-family: ${(props: any) => props.theme.appFontFamily};
  width: 100%;
  display: flex;
`;

const Cities = (props: any) => {
  const {currentConditions, currentCondition, onCityClick, onUpClick, onDownClick} = props;
  const Container = styled.div`
    ${(props: any) => applyMediaQueriesForStyle('width', {xs: '100%', md: '19.801%'}, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('order', {xs: '1', md: '0'}, props.theme)};
    display: flex;
    justify-content: start;
    margin-top: calc(${(props: any) => props.theme.gutter} * 1.125);
    align-items: center;
    ${renderThemeProperty('borderColor', 'border-color')};
    flex-direction:column;
    div.city-list{
      width: 100%;
      >div {
        cursor:pointer;
        padding:2px;
        border:1px solid transparent;
        margin-bottom:calc(${(props: any) => props.theme.gutter} * .25);
        border-radius:${(props: any) => props.theme.buttonRadius};
      }
      >div:hover{
        border: 1px solid;
        ${renderThemeProperty('cityBorderColor', 'border-color')};
      }
      >div:last-child{
        margin-bottom:0px;
      }
      div{
        div.highlighted{
          ${renderThemeProperty('activeCityColor', 'background-color')};
          border:none;
        }
        div {
          box-sizing: border-box;
          height: calc(${(props: any) => props.theme.gutter} * 2.625);
          width: 100%;
          border: 1px solid;
          ${renderThemeProperty('cityBorderColor', 'border-color')};
          border-radius: ${(props: any) => props.theme.buttonRadius};
          justify-content:center;
          align-items:center;
          display:flex;
          font-size: calc(${(props: any) => props.theme.primaryFontSize} *  1.125);
        }
      }   
    }
  }`;

  return (
    <Container>
      <div className="city-list">
        <div tabIndex={0} onClick={onDownClick.bind(this)} onKeyPress={onDownClick.bind(this)}>
          <div className="arrow">
            <img alt="Previous page" src="./mocks/images/weather_scroll_up.svg" />
          </div>
        </div>
        {currentConditions &&
          currentConditions.map((condition: any, key: any) => {
            return (
              <div
                key={condition.cityName}
                onClick={onCityClick.bind(this, condition.airport)}
                onKeyPress={onCityClick.bind(this, condition.airport)}
                tabIndex={0}
              >
                <div className={condition.cityName == currentCondition.cityName ? 'city highlighted' : 'city'}>
                  {condition.cityName}
                </div>
              </div>
            );
          })}
        <div tabIndex={0} onClick={onUpClick.bind(this)} onKeyPress={onUpClick.bind(this)}>
          <div className="arrow">
            <img alt="Next page" src="./mocks/images/weather_scroll_down.svg" />
          </div>
        </div>
      </div>
    </Container>
  );
};

const Today = (props: any) => {
  const {currentCondition, icons, cityName} = props;
  const temp = GetTempFC(currentCondition.temperature);
  const Container = styled.div`
    ${(props: any) => applyMediaQueriesForStyle('width', {xs: '100%', md: '23.861%'}, props.theme)};
    display: flex;
    margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    flex-direction: column;
    justify-content: center;
    ${renderThemeProperty('borderColor', 'border-color')};
    .today-text {
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.25);
      ${renderThemeProperty('borderColor', 'border-color')};
      flex-direction: column;
      justify-content: center;
      text-align: center;
      display: flex;
      font-family: ${(props: any) => props.theme.secondaryFontFamily};
      .city {
        font-size: calc(${(props: any) => props.theme.primaryFontSize} * 2.25);
      }
      .weather-text {
        font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.25);
        line-height: 1;
        margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
        margin-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
      }
      .temp {
        font-size: calc(${(props: any) => props.theme.primaryFontSize} * 2.25);
        font-weight: bold;
      }
    }
    .today-icon {
      text-align: center;
      display: flex;
      align-items: center;

      img {
        min-width: 207px;
        margin: 0 auto;
      }
    }
  `;

  return (
    <Container>
      <div className="today-icon">
        <img alt={currentCondition.weatherText} src={icons[currentCondition.weatherIcon]} />
      </div>
      <div className="today-text">
        <div className="city">{cityName || '---'}</div>
        <div className="weather-text">{currentCondition.weatherText || '---'}</div>
        <div className="temp">
          {temp.F}&deg;F / {temp.C}&deg;C
        </div>
      </div>
    </Container>
  );
};

const Forecast = (props: any) => {
  let {feature} = props;

  const Container = styled.div`
    display: flex;
    margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    ${(props: any) => applyMediaQueriesForStyle('width', {xs: 'auto', md: '39.900%'}, props.theme)};
    flex-direction: column;
    ${renderThemeProperty('forecastBackgroundColor', 'background-color')};
    border-radius: ${(props: any) => props.theme.buttonRadius};
    ${(props: any) => applyMediaQueriesForStyle('padding-left', {xs: '10px', md: '20px'}, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('padding-right', {xs: '10px', md: '20px'}, props.theme)};
    .row {
      ${renderThemeProperty('borderColor', 'border-color')};
      font-size: calc(${(props: any) => props.theme.defaultFontSize} * 1.25);
      flex-grow: 1;
      text-align: center;
      border-style: solid;
      border-width: 0px;
      border-bottom-width: 1px;

      justify-content: space-between;
      display: flex;
      flex-grow: 1;
      flex-direction: row;
      .day-name {
        display: flex;
        align-items: center;
        font-weight: bold;
        min-width: 100px;
      }
      .weather-icon {
        display: flex;
        align-items: center;
        img {
          ${(props: any) => applyMediaQueriesForStyle('max-width', {xs: '70px', md: '100px'}, props.theme)};
        }
      }
      .day-weather {
        display: flex;
        .arrow {
          display: flex;
          align-items: center;
          img {
            width: calc(${(props: any) => props.theme.gutter} * 0.5);
            margin-right: calc(${(props: any) => props.theme.gutter} * 0.625);
            margin-left: calc(${(props: any) => props.theme.gutter} * 0.625);
          }
        }
        .temp {
          display: flex;
          justify-content: center;
          flex-direction: column;
          align-items: center;

          .seperator {
            margin: 0 calc(${(props: any) => props.theme.gutter} * 0.3125);
          }
          .flip {
            flex-direction: row-reverse;
          }
          .min {
            display: flex;
            .temp-f {
              ${renderThemeProperty('lowTempFahrenheitTextColor', 'color')};
              ${renderThemeProperty('lowTempFahrenheitFontWeight', 'font-weight')};
            }
            .temp-c {
              ${renderThemeProperty('lowTempCelsiusTextColor', 'color')};
              ${renderThemeProperty('lowTempCelsiusFontWeight', 'font-weight')};
            }
          }
          .max {
            display: flex;
            .temp-f {
              ${renderThemeProperty('highTempFahrenheitTextColor', 'color')};
              ${renderThemeProperty('highTempFahrenheitFontWeight', 'font-weight')};
            }
            .temp-c {
              ${renderThemeProperty('highTempCelsiusTextColor', 'color')};
              ${renderThemeProperty('highTempCelsiusFontWeight', 'font-weight')};
            }
          }
        }
      }
    }
    .row:last-child {
      border-bottom-width: 0px;
    }
  `;

  const Day = (props: any) => {
    const {forecast, icons, feature} = props;
    const minTemp = GetTempFC(forecast.temperature.minimum.value);
    const maxTemp = GetTempFC(forecast.temperature.maximum.value);
    let forecastDate = 'Forecast';
    if (forecast.date === 'Today') {
      forecastDate = 'Today';
    } else if (forecast.date) {
      forecastDate = Weekdays[new Date(forecast.date).getDay()];
    }

    return (
      <div className="row">
        <div className="day-name">{forecastDate}</div>
        <div className="weather-icon">
          <img alt={forecast.day.iconPhrase} src={icons[forecast.day.icon]} />
        </div>
        <div className="day-weather">
          <div className="arrow">
            <img src="./mocks/images/weather_dashboard_up_down_arrow.png" />
          </div>
          <div className="temp">
            <span className={'max' + (feature.flipCelsiusFahrenheit ? ' flip' : '')}>
              <span className="temp-f">{maxTemp.F}&deg;F</span>
              <span className="seperator"> /</span>
              <span className="temp-c"> {maxTemp.C}&deg;C</span>
            </span>
            <span className={'min' + (feature.flipCelsiusFahrenheit ? ' flip' : '')}>
              <span className="temp-f">{minTemp.F}&deg;F </span>
              <span className="seperator"> /</span>
              <span className="temp-c">{minTemp.C}&deg;C </span>
            </span>
          </div>
        </div>
      </div>
    );
  };
  return (
    <Container>
      {props.dailyForecast.map((forecast: any, key: any) => {
        return <Day key={key} forecast={forecast} icons={props.icons} feature={feature} />;
      })}
    </Container>
  );
};

class WeatherDashboard extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps: Partial<InternalInterface & ExternalInterface> = {
    ...defaults,

    currentCondition: {
      cityName: '---',
      currentCondition: {
        temperature: '---',
        weatherIcon: 2,
        weatherText: '---'
      }
    },
    dailyForecast: [
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516798800,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-24T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516885200,
        night: {iconPhrase: '---', icon: 38},
        day: {iconPhrase: '---', icon: 6},
        date: '2018-01-25T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1516971600,
        night: {iconPhrase: '---', icon: 12},
        day: {iconPhrase: '---', icon: 18},
        date: '2018-01-26T07:00:00-06:00'
      },
      {
        temperature: {minimum: {value: '---'}, maximum: {value: '---'}},
        epochDate: 1517058000,
        night: {iconPhrase: '---', icon: 36},
        day: {iconPhrase: '---', icon: 12},
        date: '2018-01-27T07:00:00-06:00'
      }
    ],
    icons: {
      '1': './mocks/images/Weather_1Sunny_50x50.svg',
      '2': './mocks/images/Weather_2MostlySunny_50x50.svg',
      '3': './mocks/images/Weather_3PartlyCloudyDay_50x50.svg',
      '4': './mocks/images/Weather_4IntermittentCloudsDay_50x50.svg',
      '5': './mocks/images/Weather_5HazyDay_50x50.svg',
      '6': './mocks/images/Weather_6MostlyCloudyDay_50x50.svg',
      '7': './mocks/images/Weather_7PartlyCloudy_50x50.svg',
      '8': './mocks/images/Weather_8PartlyCloudyAndHazy_50x50.svg',
      '11': './mocks/images/Weather_11Fog_50x50.svg',
      '12': './mocks/images/Weather_12PartlyCloudyShowers_50x50.svg',
      '13': './mocks/images/Weather_13MostlyCloudyRain_50x50.svg',
      '14': './mocks/images/Weather_14PartlyCloudyRainDay_50x50.svg',
      '15': './mocks/images/Weather_15PartlyCloudyThunderstorms_50x50.svg',
      '16': './mocks/images/Weather_16MostlyCloudyThunderstorms_50x50.svg',
      '17': './mocks/images/Weather_17PartlyCloudyThunderstormsDay_50x50.svg',
      '18': './mocks/images/Weather_18PartlyCloudyRain_50x50.svg',
      '19': './mocks/images/Weather_19PartlyCloudyFlurries_50x50.svg',
      '20': './mocks/images/Weather_20MostlyCloudyFlurries_50x50.svg',
      '21': './mocks/images/Weather_21PartlyCloudyFlurriesDay_50x50.svg',
      '22': './mocks/images/Weather_22PartlyCloudySnow_50x50.svg',
      '23': './mocks/images/Weather_23MostlyCloudySnow_50x50.svg',
      '24': './mocks/images/Weather_24PartlyCloudyIce_50x50.svg',
      '25': './mocks/images/Weather_25PartlyCloudySleet_50x50.svg',
      '26': './mocks/images/Weather_26PartlyCloudyFreesingRain_50x50.svg',
      '29': './mocks/images/Weather_29PartlyCloudyRainAndSnow_50x50.svg',
      '30': './mocks/images/Weather_30HotNoSun_50x50.svg',
      '31': './mocks/images/Weather_31ColdNoSnowFlake_50x50.svg',
      '32': './mocks/images/Weather_32Windy_50x50.svg',
      '33': './mocks/images/Weather_33ClearNight_50x50.svg',
      '34': './mocks/images/Weather_34MostlyClearNight_50x50.svg',
      '35': './mocks/images/Weather_35PartlyCloudyNight_50x50.svg',
      '36': './mocks/images/Weather_36IntermittentCloudsNight_50x50.svg',
      '37': './mocks/images/Weather_37HazyNight_50x50.svg',
      '38': './mocks/images/Weather_38MostlyCloudyNight_50x50.svg',
      '39': './mocks/images/Weather_39PartlyCloudyRainNight_50x50.svg',
      '40': './mocks/images/Weather_40MostlyCloudyRainNight_50x50.svg',
      '41': './mocks/images/Weather_41PartlyCloudyThunderstormsNight_50x50.svg',
      '42': './mocks/images/Weather_42MostlyCloudyThunderstormsNight_50x50.svg',
      '43': './mocks/images/Weather_43MostlyCloudySnowDay_50x50.svg',
      '44': './mocks/images/Weather_44MostlyCloudySnowNight_50x50.svg'
    }
  };
  constructor(props: any) {
    super(props);
  }

  public render() {
    // TODO(wmoore): The icons need to be removed from the layout.JSON files, but are there for now so the PFE conversion will work.
    const {dailyForecast, currentCondition, icons, feature} = this.props;
    let newForecast = [];
    if (dailyForecast.length == 4) {
      newForecast = dailyForecast.slice(1, dailyForecast.length);
    }

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="ifc-aalweather-dashboard-component">
          <Cities
            currentConditions={this.props.currentConditions}
            currentCondition={this.props.currentCondition}
            onCityClick={this.props.onCityClick}
            onUpClick={this.props.onUpClick}
            onDownClick={this.props.onDownClick}
          />
          <Today
            currentCondition={currentCondition.currentCondition}
            cityName={currentCondition.cityName}
            icons={icons}
          />
          <Forecast dailyForecast={newForecast} icons={icons} feature={feature} />
        </Container>
      </PortalComponent>
    );
  }
}

export default WeatherDashboard;
