import * as React from 'react';
import { PortalComponent, utils, Currency} from 'component-utils';
import {Button, TextBox} from 'core-components';
import * as defaults from './defaultProps.json';
import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
import { validEmail } from 'component-utils/dist/utils/Validator';
export interface InternalInterface {
  className?: string;
  locale?: string;
  onGuestLoginSubmit?: any;
  onReturnClick?: any;
  onContinueClick?:any;
  componentTheme?: any;
  header?:string;
  note?:string;
  emailInputLabel?: string;
  returnButtonText?: string;
  continueButtonText?: string;
  errorDict?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  ${renderThemeProperty('primaryTextColor', 'color')};
  font-family: ${(props: any) => props.theme.primaryFontFamily};
  div.header {
    ${renderThemeProperty('secondaryTextColor', 'color')};
    font-family: ${(props: any) => props.theme.appFontFamily};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 1.875);
    padding-bottom:calc(${(props: any) => props.theme.gutter} * 1.9); 
    font-weight:lighter;
    margin-right:30px;
  }
  div.note {
    ${renderThemeProperty('primaryTextColor', 'color')};
    font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.937);
    font-family: ${(props: any) => props.theme.appFontFamily};
    margin-bottom: ${(props: any) => props.theme.gutter}; 
  }
 
  div.buttons {
    display:flex;
    ${(props: any) => applyMediaQueriesForStyle('flex-direction', { xs: "column", sm: "row-reverse" }, props.theme)};
  }
  div.button-wrap {
    display:flex;
    flex-grow:1;
    margin-top: calc(${(props: any) => props.theme.gutter} *1);
    text-align: center;
    button {
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
  div.hidden {
    visibility: hidden;
  }
  div.return-button {
    ${(props: any) => applyMediaQueriesForStyle('margin-right', { xs: "0px", sm: "12px" }, props.theme)};
    text-align: center;
    button {
      border: 1px solid ${(props: any) => props.theme.secondaryButtonTextColor};
      width: 100%;
      height: 50px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      border-radius: ${(props: any) => props.theme.buttonRadius};
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 01.125);
    }
  }
  div.continue-button {
    ${(props: any) => applyMediaQueriesForStyle('margin-left', { xs: "0px", sm: "12px" }, props.theme)};
    .disabled{
      ${renderThemeProperty('disableButtonColor', 'background-color')};
    }
  }
`;

class AaGuestLogin extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      formErrors: ''
    };
    this._handleAaGuestLoginSubmit = this._handleAaGuestLoginSubmit.bind(this);
    this._handleFormChange = this._handleFormChange.bind(this);
    this._handleReturnClick = this._handleReturnClick.bind(this);
  }

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handleAaGuestLoginSubmit(event: any) {
    event.preventDefault();
    let formErrors: any = {};
    const { email } = this.state;
    const { errorDict } = this.props;
    if (!validEmail(email)) {
      formErrors['email'] = errorDict['errorMessageInvalidEmail'];
    }
    this.setState({ formErrors });
    if (Object.keys(formErrors).length === 0) {
      this.props.onGuestLoginSubmit(email);
    }
  }
 
  _handleReturnClick(event: any) {
    event.preventDefault();
    this.props.onReturnClick();
  }
  
  public render() {
    const {
      note,
      header,
      emailInputLabel,
      returnButtonText,
      continueButtonText,
    } = this.props;

    let { formErrors } = this.state;
    
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="aa-guest-login-form">
          <form onSubmit={this._handleAaGuestLoginSubmit}>
            <div className="header">
              <span className="header-text">{header}
              </span>
            </div>
            <div className="note">{(note) ? <span className="note-text">{note}</span> : null}</div>
            <TextBox
              name="email"
              error={formErrors.email}
              placeholder={emailInputLabel}
              required={true}
              onChange={this._handleFormChange}
            />
            <div className="buttons">
                <div className="button-wrap continue-button">
                  <Button isDisabled={!validEmail(this.state.email)} isPrimary={true} onClick={this._handleAaGuestLoginSubmit} aria-label={continueButtonText}>
                    { continueButtonText }
                  </Button>
                </div>
              <div className={'button-wrap return-button'}>
                <Button isPrimary={false} onClick={this._handleReturnClick} aria-label={returnButtonText}>
                  {returnButtonText}
                </Button>
              </div>
            </div>
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default AaGuestLogin;
