import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {getFlightStatus} from '../../utils/FlightStatusUtils';

// TODO: Make this use generics to determine child props types
@connectWrapper((state: any, ownProps: any) => {
  let props = {...ownProps, flightComplete: true};
  if (state.device && state.profile) {
    let flightStatus = getFlightStatus(state.device, state.profile);
    props.flightComplete = flightStatus === 'complete';
  }
  return props;
})
export default class ShowOnFlightComplete extends React.Component<any, any> {
  public static defaultProps: any = {};
  public render() {
    return this.props.flightComplete ? this.props.children : null;
  }
}
