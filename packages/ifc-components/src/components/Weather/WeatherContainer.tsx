import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {Weather as ExternalInterface} from '../common/ComponentsInterface';
import Weather, {InternalInterface} from './Weather';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import WeatherStyle2 from './WeatherStyle2';
import WeatherStyle3 from './WeatherStyle3';

export type CombinedProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {}

@connectWrapper((state: any) => {
  let props = {};
  if (state.profile && state.device && state.profile.dailyForecasts && state.profile.dailyForecasts.length) {
    let destinationForecasts = state.profile.dailyForecasts.filter(
      (df: any) => df.airport === state.device.flightDestination
    );
    if (destinationForecasts && destinationForecasts.length) {
      destinationForecasts = destinationForecasts[0].dailyForecast;
      // Calculate the epoch time
      let epochYesterday = new Date(Date.now() - 24 * 60 * 60 * 1000).getTime() / 1000;
      // For testing (local = central, destination = pacific):
      // let now = new Date('2017-03-09T01:59:00-06:00').getTime() / 1000;
      // let now = new Date('2017-03-09T02:00:00-06:00').getTime() / 1000;
      // Each set of daily forecast data has a date/timestamp at 7am, based on the
      //   destination timezone
      // We are going to filter out the today's forecasts
      //   (and yesterday's if it exists, depending on how old the data is)

      // Make a new forecast array with only 4 elements.
      destinationForecasts = destinationForecasts
        .filter((fc: any) => epochYesterday < fc.epochDate - 7 * 3600)
        .slice(0, 4);
      let destinationCondition = state.profile.currentConditions.filter(
        (df: any) => df.airport === state.device.flightDestination
      );
      if (
        destinationForecasts &&
        destinationForecasts.length == 4 &&
        destinationCondition &&
        destinationCondition.length
      ) {
        props = {
          dailyForecast: destinationForecasts,
          currentCondition: destinationCondition[0].currentCondition,
          cityName: destinationCondition[0].cityName
        };
      } else if (destinationCondition && destinationCondition.length) {
        props = {
          currentCondition: destinationCondition[0].currentCondition,
          cityName: destinationCondition[0].cityName
        };
      }
    }
  }
  return props;
})
class WeatherContainer extends React.Component<CombinedProps, any> {
  public render() {
    switch (this.props.feature.weatherLayout) {
      case 'style3':
        return <WeatherStyle3 {...this.props} />;
      case 'style2':
        return <WeatherStyle2 {...this.props} />;
      default:
        return <Weather {...this.props} />;
    }
  }
}

export default withLocaleProps(WeatherContainer, [
  {localizedClickableAreaText: 'ifc.Weather.clickable.text'},
  {localizedHeaderText: 'ifc.Weather.header.text'},
  {localizedTodayText: 'ifc.Weather.today.text'}
]) as any;
