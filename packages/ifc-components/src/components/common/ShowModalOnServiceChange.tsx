import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {ShowModalOnServiceChange as ExternalInterface} from './ComponentsInterface';
import {ShowModal} from 'core-components/dist/store/Modal/actions';
import {pathOr} from 'ramda';

export interface StateProps {
  expectingServiceTierChange: boolean;
}
export interface InternalProps {
  serviceName: string;
  expectingServiceTierChange: boolean;
  omBypassEnabled: boolean;
  vpsBypassEnabled: boolean;
}
export interface DispatchProps {
  onServiceChange(): any;
}
export type FinalProps = ExternalInterface & InternalProps & DispatchProps;
@connectWrapper(
  (state: any, ownProps: ExternalInterface) => {
    let serviceTier = pathOr([], ['profile', 'serviceTiers'], state)
      .concat(pathOr([], ['profile', 'bypassServiceTiers'], state))
      .find((item: any) => item.id === state.device.serviceTierId);
    return {
      serviceName: pathOr('', ['name'], serviceTier),
      expectingServiceTierChange: pathOr(false, ['device', 'expectingServiceTierChange'], state)
    };
  },
  (dispatch: any, ownProps: ExternalInterface): DispatchProps => ({
    onServiceChange: () => {
      dispatch(ShowModal(ownProps.feature.modalId));
    }
  })
)
export default class ShowModalOnServiceChange extends React.Component<FinalProps, any> {
  public static defaultProps: any = {};

  constructor(props: FinalProps) {
    super(props);
    this.state = {expectingServiceTierChange: false};
  }
  public componentDidUpdate(prevProps: any, prevState: any) {
    if (!this.state.expectingServiceTierChange && this.props.expectingServiceTierChange) {
      this.setState({expectingServiceTierChange: true});
    }
    if (
      this.state.expectingServiceTierChange &&
      prevProps.serviceName !== this.props.serviceName &&
      new RegExp(this.props.feature.serviceTierPattern, 'i').test(this.props.serviceName)
    ) {
      this.props.onServiceChange();
      this.setState({expectingServiceTierChange: false});
    }
  }
  public render() {
    return this.props.children;
  }
}
