import {SettingsState} from './utils';
import {AgeRatingsClass, ISettingsState} from './../../store/settings/types';
import {IRootState} from './../../index';
import {Settings as ExternalInterface} from '../common/ComponentsInterface';
import SettingsActions from '../../store/settings/actions';

// SettingsContainer
export interface StateProps {
  storeState: ISettingsState;
}
export interface DispatchProps {
  actions: typeof SettingsActions;
}
export interface SettingsContainerProps extends StateProps, DispatchProps, ExternalInterface {}
export interface SettingsContainerState {}
export const mapStoreToProps = ({ifeSettings}: IRootState): StateProps => ({
  storeState: {...ifeSettings}
});
export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    getAgeRatingsClasses: () => dispatch(SettingsActions.getAgeRatingsClasses()),
    loadSettings: () => dispatch(SettingsActions.loadSettings()),
    saveSettings: payload => dispatch(SettingsActions.saveSettings(payload)),
    selectAgeRatingClass: ageRate => dispatch(SettingsActions.selectAgeRatingClass(ageRate))
  }
});

// Settings
export interface SettingsProps {
  ageClassSaved: string;
  ageRatingClasses: AgeRatingsClass[];
  ageRatingPasswordSaved: string;
  mountPoint: HTMLElement;
  onSave(ageRating: string, ageRatingPassword: string): any;
}
export interface SettingsState {
  ageClassSelected: string;
  ageRatingPasswordCurrent: string;
  isApplyEnabled: boolean;
  isModalOpen: boolean;
  isPasswordMismatch: boolean;
  showInputBox: boolean;
}
export const SettingsInitialState = {
  ageClassSelected: 'SHOW ALL',
  ageRatingPasswordCurrent: '',
  isApplyEnabled: false,
  isModalOpen: false,
  isPasswordMismatch: false,
  showInputBox: false
};

export const validateSettingsPassword = ({
  password,
  isNewPasswordRequired,
  lockPassword
}: {
  password: string;
  isNewPasswordRequired: boolean;
  lockPassword: string;
}): string | undefined => {
  if (isNewPasswordRequired && password.length < 4) {
    return 'Password should have at least 4 characters.';
  }

  if (!isNewPasswordRequired && password !== lockPassword) {
    return 'Wrong password.';
  }

  return undefined;
};
