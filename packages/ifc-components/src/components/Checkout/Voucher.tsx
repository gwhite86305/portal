import * as React from 'react';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {Button, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';
import {validEmail, confirmEmailMatch} from 'component-utils/dist/utils/Validator';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  WifiServiceOptions?: any;
  onVoucherSubmit?: any;
  componentTheme?: any;
  voucherHeader?: string;
  voucherBodyText?: string;
  voucherInputLabel?: string;
  emailConfirmationLabel?: string;
  voucherInputErrorHelpText?: string;
  voucherEmailLabel?: string;
  voucherEmailErrorHelpText?: string;
  voucherNoteText?: string;
  voucherSubmitButtonText?: string;
  errorDict?: any;
  feature?: any;
}

const Container = styled(CommonComponentThemeWrapper)`
  form.checkout-voucher-form {
    margin: auto;
    max-width: 688px;
    h1.header-text {
      margin: 0px;
      ${renderThemeProperty('voucherHeaderMarginTop', 'margin-top')};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.5);
      ${renderThemeProperty('voucherHeaderFontFamily', 'font-family')};
      ${renderThemeProperty('voucherHeaderColor', 'color')};
      ${renderThemeProperty('voucherHeaderFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.04;
    }
    h2.body-text {
      margin: 0px;
      ${renderThemeProperty('voucherBodyTextColor', 'color')};
      ${renderThemeProperty('voucherBodyTextFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.187;
    }
    p.validation-summary {
      color: ${(props: any) => props.theme.formErrorColor};
      margin: 0px;
      margin-top: calc(${(props: any) => props.theme.gutter} *1.25);
      font-weight: bold;
      display: inline-block;
    }
    div.input-wrapper {
      label {
        display: inline-block;
        word-wrap: break-word;
        ${renderThemeProperty('voucherInputLabelColor', 'color')};
        ${renderThemeProperty('voucherInputLabelFontSize', 'font-size')};
        margin: 0px;
        margin-left: calc(${(props: any) => props.theme.gutter} *0.5);
        line-height: 1.25;
        margin-top: calc(${(props: any) => props.theme.gutter} *1.25);
      }
      input {
        font-family: ${(props: any) => props.theme.appFontFamily};
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        font-weight: 100;
        margin-top: calc(${(props: any) => props.theme.gutter} *0.25);
        display: block;
        width: 100%;
        height: calc(${(props: any) => props.theme.gutter} *2.5625);
        padding: calc(${(props: any) => props.theme.gutter} *0.375) calc(${(props: any) => props.theme.gutter} *0.625);
        font-size: ${(props: any) => props.theme.tertiaryFontSize};
        line-height: 1.42857;
        color: ${(props: any) => props.theme.primaryTextColor};
        border: 1px solid;
        border-color: ${(props: any) => props.theme.senaryBackgroundColor};
      }
      input:focus {
        border-color: ${(props: any) => props.theme.secondaryTextColor};
        color: ${(props: any) => props.theme.secondaryTextColor};
        border: 2px solid;
      }
      .validation {
        position: relative;
        padding: calc(${(props: any) => props.theme.gutter} *0.5) calc(${(props: any) => props.theme.gutter} *0.625)
          calc(${(props: any) => props.theme.gutter} *0.5) calc(${(props: any) => props.theme.gutter} *0.625);
        div.opacity-background {
          background-color: ${(props: any) => props.theme.formErrorColor};
          opacity: 0.14;
          position: absolute;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
        }
        p.help-text {
          margin: 0px;
          line-height: 1.25;
          margin-bottom: 5px;
        }
        p.message {
          color: ${(props: any) => props.theme.formErrorColor};
          margin: 0px;
          font-weight: bold;
          line-height: 1.25;
        }
        div.circle {
          text-align: right;
          height: 0px;
          span {
            position: relative;
            top: calc(${(props: any) => props.theme.gutter} *-3.75);
            background-color: ${(props: any) => props.theme.formErrorColor};
            width: calc(${(props: any) => props.theme.gutter} *1.375);
            height: calc(${(props: any) => props.theme.gutter} *1.375);
            line-height: 1.375;
            display: inline-block;
            text-align: center;
            justify-content: center;
            border-radius: calc(${(props: any) => props.theme.gutter} *1.375);
            color: ${(props: any) => props.theme.tertiaryTextColor};
          }
        }
      }
    }
    div.button-wrap {
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'margin-top',
          {xs: `calc(${props.theme.gutter}*1.875)`, md: `calc(${props.theme.gutter}*1.875)`},
          props.theme
        )};
      text-align: center;
      button {
        ${renderThemeProperty('voucherButtonWidth', 'width')};
        ${renderThemeProperty('voucherButtonHeight', 'height')};
        ${renderThemeProperty('voucherButtonBorder', 'border')};
        ${renderThemeProperty('voucherButtonFontSize', 'font-size')};
        ${renderThemeProperty('voucherButtonFontWeight', 'font-weight')};
        margin: 0 auto;
        white-space: nowrap;
        justify-content: center;
      }
    }
    p.note {
      ${renderThemeProperty('voucherNoteColor', 'color')};
      ${renderThemeProperty('voucherNoteFontSize', 'font-size')};
      margin: 0px;
      padding-top: calc(${(props: any) => props.theme.gutter} * 0.625);
    }
  }
`;

class Voucher extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      formErrors: {},
      email: '',
      confirmEmail: '',
      voucher: ''
    };
    this._handleVoucherSubmit = this._handleVoucherSubmit.bind(this);
  }

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handleVoucherSubmit(event: any) {
    event.preventDefault();
    const {voucher, email, confirmEmail} = this.state;
    const {errorDict, feature} = this.props;
    let formErrors: any = {};
    if (!validEmail(email)) {
      formErrors['email'] = errorDict['emailRequired'];
    }
    if (feature.addEmailConfirmationField && !confirmEmailMatch(email, confirmEmail)) {
      formErrors['confirmEmail'] = errorDict['emailMismatch'];
    }
    if (!voucher) {
      formErrors['voucher'] = errorDict['voucherRequired'];
    }
    this.setState({formErrors});
    if (Object.keys(formErrors).length === 0) {
      this.props.onVoucherSubmit(voucher, email);
    }
  }

  public render() {
    const {WifiServiceOptions, errorDict} = this.props;
    const {
      voucherHeader,
      voucherBodyText,
      voucherInputLabel,
      voucherInputErrorHelpText,
      voucherEmailLabel,
      emailConfirmationLabel,
      voucherEmailErrorHelpText,
      voucherNoteText,
      voucherSubmitButtonText,
      feature
    } = this.props;

    let {formErrors} = this.state;

    if (
      (WifiServiceOptions.voucher && WifiServiceOptions.voucher.error) ||
      (WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'reject')
    ) {
      formErrors['voucher'] =
        formErrors['voucher'] ||
        (WifiServiceOptions.voucher &&
          WifiServiceOptions.voucher.error &&
          errorDict[WifiServiceOptions.voucher.errorCode]) ||
        (WifiServiceOptions.signup &&
          WifiServiceOptions.signup.action == 'reject' &&
          errorDict[WifiServiceOptions.signup.code]) ||
        errorDict['generic'];
    }
    const keepTheSpinner =
      (WifiServiceOptions &&
        WifiServiceOptions.selected &&
        WifiServiceOptions.selected.method == 'voucher' &&
        WifiServiceOptions.voucher &&
        WifiServiceOptions.voucher.waitingForResponse == true) ||
      (WifiServiceOptions && WifiServiceOptions.signup && WifiServiceOptions.signup.waitingForResponse === true) ||
      (WifiServiceOptions && WifiServiceOptions.voucher && WifiServiceOptions.voucher.valid === true) ||
      (WifiServiceOptions && WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'accept');
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          {/* Show spinner until the voucher validation is done.
          If the voucher is valid, keep the spinner until the wifi service get activated */}
          {keepTheSpinner ? <Spinner /> : null}
          <form className="checkout-voucher-form" onSubmit={this._handleVoucherSubmit}>
            {voucherHeader || voucherBodyText ? (
              <div className="header">
                {voucherHeader ? <h1 className="header-text">{voucherHeader}</h1> : null}
                {voucherBodyText ? (
                  <h2 className="body-text" dangerouslySetInnerHTML={sanitize(voucherBodyText)} />
                ) : null}
              </div>
            ) : null}
            {Object.keys(formErrors).length > 0 ? (
              <p className="validation-summary">{errorDict['errorHeaderText']}</p>
            ) : null}
            <div className="input-wrapper">
              <label htmlFor="voucher">{voucherInputLabel}</label>
              <input
                className="input-text"
                name="voucher"
                id="voucher"
                type="text"
                onChange={e => this._handleFormChange('voucher', e.target.value)}
              />
              {formErrors.voucher ? (
                <div className="validation">
                  <div className="opacity-background" />
                  <div className="circle">
                    <span>!</span>
                  </div>
                  {voucherInputErrorHelpText ? <p className="help-text">{voucherInputErrorHelpText}</p> : null}
                  <p className="message">{formErrors.voucher}</p>
                </div>
              ) : null}
            </div>
            <div className="input-wrapper">
              <label htmlFor="email">{voucherEmailLabel}</label>
              <input
                className="input-text"
                name="email"
                id="email"
                type="text"
                onChange={e => this._handleFormChange('email', e.target.value)}
              />
              {formErrors.email ? (
                <div className="validation">
                  <div className="opacity-background" />
                  <div className="circle">
                    <span>!</span>
                  </div>

                  {voucherEmailErrorHelpText ? <p className="help-text">{voucherEmailErrorHelpText}</p> : null}
                  <p className="message">{formErrors.email}</p>
                </div>
              ) : null}
              {feature.addEmailConfirmationField ? (
                <React.Fragment>
                  <label htmlFor="email">{emailConfirmationLabel}</label>
                  <input
                    className="input-text"
                    name="confirmEmail"
                    id="confirmEmail"
                    type="text"
                    onChange={e => this._handleFormChange('confirmEmail', e.target.value)}
                  />
                  {formErrors.confirmEmail ? (
                    <div className="validation">
                      <div className="opacity-background" />
                      <div className="circle">
                        <span>!</span>
                      </div>
                      <p className="message">{formErrors.confirmEmail}</p>
                    </div>
                  ) : null}
                </React.Fragment>
              ) : null}
            </div>
            {voucherNoteText ? <p className="note">{voucherNoteText}</p> : null}
            <div className="button-wrap">
              <Button isPrimary={true} onClick={this._handleVoucherSubmit} aria-label={voucherSubmitButtonText}>
                {voucherSubmitButtonText}
              </Button>
            </div>
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default Voucher;
