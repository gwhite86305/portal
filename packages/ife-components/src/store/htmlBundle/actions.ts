import { actionCreatorFactory } from 'typescript-fsa';
import { IHtmlBundle } from './reducer';

const action = actionCreatorFactory('HTML_BUNDLES');

export const getHtmlBundleByCategoryName = action.async<string, IHtmlBundle>('GET_HTML_BUNDLE_BY_CATEGORY');