import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, object, text, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import MockRouter from 'react-mock-router';
import Banner from '../dist/components/Banner/Banner';
import Text from '../dist/components/Text/Text';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const messagesNeosBanner = {
  headerText: 'Attiva la tua connessione',
  subHeaderText: '',
  altTextForImage: 'Dreamliner Airplane'
};

const messagesNeosConnected = {
  headerText: 'ADESSO SEI CONNESSO',
  subHeaderText: 'TEMPO RIMANENTE',
  altTextForImage: 'Dreamliner Airplane'
};

const defaultMessages = {
  headerText: 'Title',
  subHeaderText: 'Description',
  altTextForImage: 'Alt Text For Image'
};

const icelandText = {
  headerText: 'Welcome!',
  subHeaderText: 'Stay well connected above the clouds with our free wifi service onboard this aircraft.',
  buttonText: 'Connect Now',
  altTextForImage: 'Airplane wing'
};

const defaultProps = {
  componentTheme: {
    fontFamily: 'theme.appFontFamily',
    headerTextColor: 'theme.primaryTextColor',
    subHeaderTextColor: 'theme.secondaryTextColor',
    backgroundColor: 'theme.primaryBackgroundColor',
    headertextAlign: 'left',
    subheadertextAlign: 'left',
    headertextFontSize: '48px',
    subheadertextFontSize: '24px',
    backgroundImage: {
      xs: ''
    },
    imageWidth: '100%'
  },
  feature: {
    image: {
      xs: './images/Banner_laptop.png'
    },
    actionLink: '#',
    displayArrowIcon: true,
    includeButton: false
  }
};
const modifiedMockTheme_1 = {
  componentTheme: {
    backgroundColor: '#1BBC9B',
    fontFamily: 'SourceSansPro',
    headerTextColor: 'white',
    subHeaderTextColor: 'white'
  }
};

const modifiedMockTheme_2 = {
  componentTheme: {
    backgroundColor: '#EC644B',
    fontFamily: 'SourceSansPro',
    headerTextColor: 'white',
    subHeaderTextColor: 'white'
  }
};

const headerText = 'Welcome, passenger!';
const subHeaderText = 'Browse the web';
const actionLink = 'http://www.viasat.com';

const image_xs = {
  xs: './images/Banner_laptop.png',
  lg: './images/qantas.png'
};

const image = {
  xs: './images/Banner_mobile.png',
  sm: './images/Banner_tablet.png',
  md: './images/Banner_tablet.png',
  lg: './images/Banner_laptop.png',
  xl: './images/Banner_laptop.png'
};

const icelandImage = {
  xs: './images/iceland_logo.svg'
};

const neosPropsBanner = {
  componentTheme: {
    fontFamily: 'theme.appFontFamily',
    headerTextColor: 'theme.primaryTextColor',
    subHeaderTextColor: 'theme.secondaryTextColor',
    backgroundColor: 'rgb(255, 255, 255)'
  },
  feature: {
    image: {
      xs: './images/dreamliner_mobile.png',
      sm: './images/dreamliner_tablet.png',
      md: './images/dreamliner_tablet.png',
      lg: './images/dreamliner_laptop.png',
      xl: './images/dreamliner_laptop.png'
    },
    actionLink: '',
    displayArrowIcon: false
  }
};
const neosPropsConnectedPage = {
  componentTheme: {
    fontFamily: 'theme.appFontFamily',
    headerTextColor: 'theme.primaryTextColor',
    subHeaderTextColor: 'theme.secondaryTextColor',
    backgroundColor: 'rgb(255, 255, 255)',
    headertextAlign: 'center',
    subheadertextAlign: 'center'
  },
  feature: {
    image: {
      xs: './images/wifi_airline_mobile.svg',
      sm: './images/wifi_airline_tablet.svg',
      md: './images/wifi_airline_tablet.svg',
      lg: './images/wifi_airline_laptop.svg',
      xl: './images/wifi_airline_laptop.svg'
    },
    actionLink: '',
    displayArrowIcon: false
  }
};

const icelandProps = {
  componentTheme: {
    padding: {
      xs: '34px 0px 54px 0px'
    },
    backgroundColor: '',
    display: {
      xs: true
    },
    subheadertextAlign: 'center',
    headertextAlign: 'center',
    headertextFontSize: '32px',
    subheadertextFontSize: '18px',
    backgroundImage: {
      sm: './images/largeBannerImage.png',
      xs: './images/mediumBannerImage.png'
    }
  },
  feature: {
    image: {
      xs: ''
    },
    includeButton: true,
    actionLink: '',
    openPageOnClick: 'http://www.viasat.com',
    displayArrowIcon: false
  }
};

const icelandProps2 = {
  componentTheme: {
    padding: {
      xs: '34px 0px 54px 0px'
    },
    backgroundColor: '',
    display: {
      xs: true
    },
    fontFamily: 'theme.appFontFamily',
    headerTextColor: '#5E5E5E',
    subHeaderTextColor: '#444444',
    subheadertextAlign: 'center',
    headertextAlign: 'center',
    headertextFontSize: '32px',
    subheadertextFontSize: '18px',
    backgroundImage: {
      sm: './images/largeBannerImage.png',
      xs: './images/mediumBannerImage.png'
    },
    imageWidth: '30%',
    imageMargin: 'auto'
  },
  feature: {
    image: {
      xs: './images/iceland_logo.svg'
    },
    includeButton: true,
    actionLink: '',
    openPageOnClick: 'http://www.viasat.com',
    displayArrowIcon: false
  }
};

const headerTextProps = {
  componentTheme: {
    fontSize: '32px',
    align: 'center'
  }
};

mockTheme.appFontFiles = "url('./fonts/altitude/AltitudeLight.ttf') format('truetype');";

const stories = storiesOf('[CMOB-18429]-Banner - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('Banner - Default Demo Portal Theme', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <Banner
        {...defaultProps}
        altTextForImage={defaultMessages.altTextForImage}
        headerText={defaultMessages.headerText}
        subHeaderText={defaultMessages.subHeaderText}
      >
        {[
          <div>
            <Text {...headerTextProps} />
          </div>
        ]}
      </Banner>
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test Viasat Theme', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    image: image,
    actionLink: actionLink
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Banner
        {...testProps}
        headerText={headerText}
        subHeaderText={subHeaderText}
        altTextForImage={'Alt Text For Image'}
      >
        {[
          <div>
            <Text {...headerTextProps} />
          </div>
        ]}
      </Banner>
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test Modified Theme', () => {
  const testProps = {...modifiedMockTheme_1};
  testProps.feature = {
    image: image,
    actionLink: actionLink
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Banner {...testProps} headerText={headerText} subHeaderText={subHeaderText}>
        {[
          <div>
            <Text {...headerTextProps} />
          </div>
        ]}
      </Banner>
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test Text Configurability', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    image: image,
    actionLink: actionLink
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Banner
        {...testProps}
        headerText={text('Header Text', headerText)}
        subHeaderText={text('Sub Header Text', subHeaderText)}
        altTextForImage={'Alt Text For Image'}
      >
        {[
          <div>
            <Text {...headerTextProps} />
          </div>
        ]}
      </Banner>
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test No Header Text Configurability', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    image: image,
    actionLink: actionLink
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Banner {...testProps} headerText={''} subHeaderText={subHeaderText} altTextForImage={'Alt Text For Image'} />
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test No Sub Header Text Configurability', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    image: image,
    actionLink: actionLink
  };

  return (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Banner {...testProps} headerText={headerText} subHeaderText={''} altTextForImage={'Alt Text For Image'}>
        {[
          <div>
            <Text {...headerTextProps} />
          </div>
        ]}
      </Banner>
    </PortalComponentWrapper>
  );
});

stories.add('Banner - Test Neos Banner', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper
        mockTheme={object('theme', mockTheme)}
        mockMessages={object('Localized messages', messagesNeosBanner)}
      >
        <Banner
          componentTheme={object('componentTheme', neosPropsBanner.componentTheme)}
          feature={object('Feature Configuration', neosPropsBanner.feature)}
          altTextForImage={text('Alt Text for image', messagesNeosBanner.altTextForImage)}
          headerText={text('Header Text', messagesNeosBanner.headerText)}
          subHeaderText={text('Sub Header Text', messagesNeosBanner.subHeaderText)}
        >
          {[
            <div>
              <Text {...headerTextProps} />
            </div>
          ]}
        </Banner>
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Banner - Test Neos You are Connected Page', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper
        mockTheme={object('theme', mockTheme)}
        mockMessages={object('Localized messages', messagesNeosConnected)}
      >
        <Banner
          componentTheme={object('componentTheme', neosPropsConnectedPage.componentTheme)}
          feature={object('Feature Configuration', neosPropsConnectedPage.feature)}
          altTextForImage={text('Alt Text for image', messagesNeosConnected.altTextForImage)}
          headerText={text('Header Text', messagesNeosConnected.headerText)}
          subHeaderText={text('Sub Header Text', messagesNeosConnected.subHeaderText)}
        >
          {[
            <div>
              <Text {...headerTextProps} />
            </div>
          ]}
        </Banner>
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Banner - Iceland Configuration', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper
        mockTheme={object('theme', mockTheme)}
        mockMessages={object('Localized messages', icelandText)}
      >
        <Banner
          componentTheme={object('componentTheme', icelandProps.componentTheme)}
          feature={object('Feature Configuration', icelandProps.feature)}
          altTextForImage={text('Alt Text for image', icelandText.altTextForImage)}
          headerText={text('Header Text', icelandText.headerText)}
          subHeaderText={text('Sub Header Text', icelandText.subHeaderText)}
          buttonText={text('Button Text', icelandText.buttonText)}
        >
          {[
            <div>
              <Text {...headerTextProps} />
            </div>
          ]}
        </Banner>
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Banner - Test all props', () => {
  const modifiedMockTheme = {...mockTheme};
  modifiedMockTheme.appFontFamily = "url('./fonts/altitude/AltitudeLight.ttf') format('truetype');";
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Banner
          componentTheme={object('componentTheme', icelandProps2.componentTheme)}
          feature={object('Feature Configuration', icelandProps2.feature)}
          altTextForImage={text('Alt Text for image', icelandText.altTextForImage)}
          headerText={text('Header Text', icelandText.headerText)}
          subHeaderText={text('Sub Header Text', icelandText.subHeaderText)}
          buttonText={text('Button Text', icelandText.buttonText)}
        >
          {[
            <div>
              <Text {...headerTextProps} />
            </div>
          ]}
        </Banner>
      </PortalComponentWrapper>
    </MockRouter>
  );
});
