import React = require('react');
import Flight from '@material-ui/icons/Flight';

export interface MovingMapMarkerPlaneProps {
  rotation: number;
  anchor: number[];
  offset: number[];
  left?: number;
  top?: number;
}

export const MovingMapMarkerPlane: React.FC<MovingMapMarkerPlaneProps> = props => {
  return (
    <div
      style={{transform: `translate(${props.left}px, ${props.top}px) rotate(${props.rotation}rad)`}}
      className="icon-wrapper"
    >
      <Flight className="icon" />
    </div>
  );
};
