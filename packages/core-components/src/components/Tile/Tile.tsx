import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils} from 'component-utils';
import {renderChildComponent} from 'component-utils/dist/utils/ComponentUtils';
import {Tile as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {sanitize} from 'component-utils';
import NavButton from '../common/NavButton';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  position: relative;
  ${renderThemeProperty('backgroundColor', 'background-color')};
  display: flex;
  flex-direction: column;
  height: 100%;
  img {
    display: block;
  }
  > a {
    display: flex;
    flex-direction: column;
    text-decoration: none;
    :hover {
      background-color: ${(props: any) => props.theme.componentTheme.hoverColor};
    }
  }
  .tile-textregion {
    flex-wrap: wrap;
    align-self: flex-end;
    flex-grow: 2;
    width: 100%;
  }
  .tile-header {
    ${(props: any) => {
      return applyMediaQueriesForStyle('padding', props.theme.componentTheme.headerTextPadding, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle('text-align', props.theme.componentTheme.headertextAlign, props.theme);
    }};
  }
  .subHeader-text {
    ${(props: any) => {
      return applyMediaQueriesForStyle('padding', props.theme.componentTheme.subHeaderTextPadding, props.theme);
    }};
    ${(props: any) => {
      return applyMediaQueriesForStyle('text-align', props.theme.componentTheme.subheadertextAlign, props.theme);
    }};
  }
  p {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;
    margin: 0;
  }
`;

const TileTextRegion = (props: any) => {
  const {headerText, subHeaderText} = props;
  return (
    <div className="tile-textregion">
      <div className="tile-header">
        <p dangerouslySetInnerHTML={sanitize(headerText)} />
      </div>
      <div className="subHeader-text">
        <p dangerouslySetInnerHTML={sanitize(subHeaderText)} />
      </div>
    </div>
  );
};

class Tile extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {altTextForImage, subHeaderText, headerText, buttonText, feature, componentTheme, children} = this.props;
    const innerContent = (
      <React.Fragment>
        <ResponsiveImage
          altText={altTextForImage}
          image={feature.image}
          imageWidth={componentTheme.imageWidth}
          imageMargin={componentTheme.imageMargin}
        />
        <TileTextRegion subHeaderText={subHeaderText} headerText={headerText} />
      </React.Fragment>
    );
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          {!feature.includeButton ? (
            feature.openPageOnClick == '#' || feature.openPageOnClick == '' ? (
              innerContent
            ) : (
              <a href={feature.openPageOnClick} target="_blank" title="(opens in new tab/window)">
                {innerContent}
              </a>
            )
          ) : (
            <React.Fragment>
              {innerContent}
              {renderChildComponent(0, children, {localizedText: buttonText})}
            </React.Fragment>
          )}
        </Container>
      </PortalComponent>
    );
  }
}

export default Tile;
