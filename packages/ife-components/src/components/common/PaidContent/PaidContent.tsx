import {PaidContentState, PaidContentProps, VoucherOrder, OrderResponse, FormSection} from './types';
import React = require('react');
import {AxiosResponse, AxiosError} from 'axios';
import {Button, Dialog} from '@material-ui/core';
import SeatSelection from './SeatSelection';
import VoucherEntry from './VoucherEntry';
import {axiosWithBaseUrl as http} from '../../../utils/utils';
import StyledContainer from './StyledContainer';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import CloseIcon from '@material-ui/icons/Close';

function toCurrencyString(currency: string, value: number) {
  try {
    return value.toLocaleString(navigator.language, {style: 'currency', currency});
  } catch (e) {
    return value.toLocaleString(navigator.language, {minimumFractionDigits: 2});
  }
}

const defaultState: PaidContentState = {
  open: false,
  processing: false,
  modalSection: FormSection.seatEntry,
  seat: '',
  seatDirty: false,
  voucherCode: '',
  voucherError: false,
  voucherCodeDirty: false,
  successMessage: '',
  paid: false
};

export default class PaidContent extends React.Component<PaidContentProps, PaidContentState> {
  state = {...defaultState};

  toggleModal = () => this.setState({...this.state, open: !this.state.open});

  handleClose = () => {
    if (this.state.processing) {
      return;
    }
    this.setState({...defaultState});
    if (this.state.paid) {
      this.props.onPurchaseSuccess();
      this.props.onPaid();
    }
  };

  handleSeatChange = (event: React.FormEvent<HTMLInputElement>) =>
    this.setState({
      ...this.state,
      seat: event.currentTarget.value,
      seatDirty: true
    });

  handleVoucherCodeChange = (event: React.FormEvent<HTMLInputElement>) =>
    this.setState({
      ...this.state,
      voucherCode: event.currentTarget.value,
      voucherError: false,
      voucherCodeDirty: true
    });

  handleBack = () =>
    this.setState({
      ...this.state,
      modalSection: FormSection.seatEntry,
      voucherError: false
    });

  handleContinue = () =>
    this.setState({
      ...this.state,
      modalSection: FormSection.voucherEntry
    });

  isSeatValid = (): boolean => /^([1-9][0-9]|0[1-9]|[1-9])([A-K]|[a-k])$/.test(this.state.seat);

  handlePlaceOrder = () => {
    this.setState({...this.state, processing: true});

    const voucherOrder: VoucherOrder = {
      voucherCode: this.state.voucherCode,
      totalPrice: String(this.props.price.value),
      currency: this.props.price.currency,
      entries: [this.props.entry],
      seatNumber: this.state.seat
    };
    http
      .post('/orders/voucherpayment', voucherOrder)
      .then((res: AxiosResponse) => {
        this.setState({
          ...this.state,
          processing: false,
          modalSection: FormSection.success,
          paid: true,
          successMessage: this.createSuccessMessage(res.data)
        });
      })
      .catch((error: AxiosError) => {
        const response = error.response;
        if (response) {
          const errors: {code: number; message: string}[] = response.data.errors;
          this.setState({
            ...this.state,
            processing: false,
            voucherError: errors[0].message
          });
        }
      });
  };

  createSuccessMessage(data: OrderResponse): string {
    const currency = data.currency;
    const credits = Number(data.credits);
    const message = 'Payment successful. Remaining voucher credits: ';
    if (currency) {
      return `${message}${toCurrencyString(currency, credits)}`;
    } else {
      return `${message}${credits}`;
    }
  }

  seatEntry = () => {
    const {seat, seatDirty} = this.state;
    return (
      <>
        <SeatSelection value={seat} onChange={this.handleSeatChange} error={!this.isSeatValid()} dirty={seatDirty} />
        <div className="btn-wrapper">
          <Button
            aria-label="Continue"
            disabled={!this.isSeatValid()}
            variant="contained"
            className="cta primary continue"
            onClick={this.handleContinue}
          >
            <span className="button-label">Continue</span>
          </Button>
        </div>
      </>
    );
  };

  voucherEntry = () => {
    const {processing, voucherCode, voucherError} = this.state;
    return (
      <>
        <VoucherEntry value={voucherCode} onChange={this.handleVoucherCodeChange} error={voucherError} />
        <div className="btn-wrapper">
          <Button aria-label="Back" disabled={processing} variant="contained" className="cta" onClick={this.handleBack}>
            <span className="button-label">Back</span>
          </Button>
          <Button
            disabled={processing || !voucherCode.length}
            variant="contained"
            className="cta primary"
            aria-label="Place your order"
            onClick={this.handlePlaceOrder}
          >
            <span className="button-label">Place your order</span>
          </Button>
        </div>
      </>
    );
  };

  success = () => {
    const {successMessage} = this.state;
    return <>
        {successMessage}
        <div className="btn-wrapper">
          <Button aria-label="Close" variant="contained" className="cta primary continue" onClick={this.handleClose}>
            <span className="button-label">Close</span>
          </Button>
        </div>
      </>;
  };

  render() {
    const {price, label, className, onPaid: handlePaid} = this.props;
    const {processing, open, modalSection} = this.state;
    return (
      <>
        <Button aria-label={label} variant="contained" className={className} onClick={price.paid ? handlePaid : this.toggleModal}>
          {price.paid ? label : `${label} ${toCurrencyString(price.currency, price.value)}`}
          {price.paid ? <LockOpenIcon className="price-label-icon" /> : <LockIcon className="price-label-icon" />}
        </Button>
        <Dialog onClose={this.toggleModal} open={open}>
          <StyledContainer>
            <div className="modal-header">
              <div className="title">
                <span>Payment</span>
              </div>
              <Button
                aria-label="Close"
                className="close-button"
                disabled={processing}
                onClick={this.toggleModal}
                children={<CloseIcon />}
              />
            </div>
            <div className="modal-content">
              {modalSection === FormSection.seatEntry && this.seatEntry()}
              {modalSection === FormSection.voucherEntry && this.voucherEntry()}
              {modalSection === FormSection.success && this.success()}
            </div>
          </StyledContainer>
        </Dialog>
      </>
    );
  }
}
