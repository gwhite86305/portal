import * as React from 'react';
import {History} from 'history';
export interface IRouter {
  history: History;
}

export const LayoutContext = React.createContext(undefined as any);
export const RouterContext = React.createContext<IRouter>({history: {} as any});
export const ComponentContext = React.createContext('');
