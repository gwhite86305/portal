import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
const {generateMessage} = utils.APIError;

// Action Creators
export const ActionCreators = {
  CookieAlertUserResponseSaved: new ActionCreator<'CookieAlertUserResponseSaved', any>('CookieAlertUserResponseSaved'),
  CookieAlertUserResponseRetrieved: new ActionCreator<'CookieAlertUserResponseRetrieved', any>(
    'CookieAlertUserResponseRetrieved'
  )
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function saveUserResponse(dismissed: boolean, props: any) {
  return (dispatch: any, getState: any) => {
    let payload = {
      dismissed: dismissed,
      errorMessage: {}
    };

    try {
      localStorage.setItem('cookie_alert_dismissed', dismissed.toString());
    } catch (e) {
      payload.errorMessage = generateMessage('Could not store cookie_alert_dismissed value into the localstorage', e);
    }
    dispatch(ActionCreators.CookieAlertUserResponseSaved.create(payload));
  };
}

export function getUserResponse() {
  return (dispatch: any) => {
    let payload = {
      dismissed: false,
      errorMessage: {}
    };

    try {
      payload.dismissed = localStorage.getItem('cookie_alert_dismissed') == 'true';
    } catch (e) {
      payload.errorMessage = generateMessage('Could not retrive cookie_alert_dismissed value from the localstorage', e);
    }
    dispatch(ActionCreators.CookieAlertUserResponseRetrieved.create(payload));
  };
}
