import * as React from 'react';
import {map, uniq, compose, keys, clone} from 'ramda';
import {Store} from 'redux';
import {CONTAINER_PREFIX} from 'component-utils/dist/types/Constants';
import MandatoryComponents from 'component-utils/dist/types/MandatoryComponents';
import LayoutInterface from 'component-utils/dist/types/LayoutInterface';
import {Settings} from 'component-utils/dist/types/UXStructure';
import { EpicMiddleware } from 'redux-observable';

/*
    Components *must* consist of the pure component
    e.g. Movie
    and *may* consist of a wrapping container component with the name "Container" prefixed:
    e.g. MovieContainer

    So, creating a new component? Add here so can be loaded as needed...
*/

// Just to key in on the init function. It wraps the render parameter (HOC-style) and returns it, and attaches reducers to the store

export type LibraryInitFunction = (
  render: React.ReactElement<any>,
  store: Store<any>,
  settings: Settings,
  epicMiddleware: EpicMiddleware<any>,
) => React.ReactElement<any>;

export type AllLibrariesInitFunction = (
  render: React.ReactElement<any> | null,
  store: Store<any>,
  settings: Settings,
  layout: LayoutInterface,
  epicMiddleware: EpicMiddleware<any>,
) => React.ReactElement<any>;

export interface LibraryInterface {
  init: LibraryInitFunction;
}

// allow for dynamic lookup from layout JSON
export interface IndexInterface {
  [key: string]: any;
}

const loadedComponentLibraries: IndexInterface = {
  core: require('core-components'),
  ife: require('ife-components'),
  ifc: require('ifc-components')
};

/* REGISTER THE FOLLOWING FILES FOR BUNDLING BY WEBPACK - *note: not a preprocessor so cannot be dynamic*  */

const loadedComponentBundles: IndexInterface = {
  // Super Core
  GridContainer: require('bundle-loader?lazy&name=GridContainer!../components/GridContainer/GridContainer'),
  GridItem: require('bundle-loader?lazy&name=GridItem!../components/GridItem/GridItem'),

  // Core
  Accordion: require('bundle-loader?lazy&name=Accordion!core-components/dist/components/Accordion/Accordion'),
  BackNavigation: require('bundle-loader?lazy&name=BackNavigation!ife-components/dist/components/BackNavigation/BackNavigation'),
  BackNavigationContainer: require('bundle-loader?lazy&name=BackNavigationContainer!ife-components/dist/components/BackNavigation/BackNavigationContainer'),
  Banner: require('bundle-loader?lazy&name=Banner!core-components/dist/components/Banner/Banner'),
  BannerContainer: require('bundle-loader?lazy&name=BannerContainer!core-components/dist/components/Banner/BannerContainer'),
  BillBoard: require('bundle-loader?lazy&name=BillBoard!core-components/dist/components/BillBoard/BillBoard'),
  BillBoardContainer: require('bundle-loader?lazy&name=BillBoardContainer!core-components/dist/components/BillBoard/BillBoardContainer'),
  Button: require('bundle-loader?lazy&name=Button!core-components/dist/components/Button/Button'),
  Carousel: require('bundle-loader?lazy&name=Carousel!core-components/dist/components/Carousel/Carousel'),
  CloseModal: require('bundle-loader?lazy&name=CloseModal!core-components/dist/components/CloseModal/CloseModal'),
  CookieAlert: require('bundle-loader?lazy&name=CookieAlert!core-components/dist/components/CookieAlert/CookieAlert'),
  CookieAlertContainer: require('bundle-loader?lazy&name=CookieAlertContainer!core-components/dist/components/CookieAlert/CookieAlertContainer'),
  Element: require('bundle-loader?lazy&name=Element!core-components/dist/components/Element/Element'),
  Footer: require('bundle-loader?lazy&name=Footer!core-components/dist/components/Footer/Footer'),
  FooterContainer: require('bundle-loader?lazy&name=Footer!core-components/dist/components/Footer/FooterContainer'),
  HamburgerMenu: require('bundle-loader?lazy&name=HamburgerMenu!core-components/dist/components/HamburgerMenu/HamburgerMenu'),
  HamburgerMenuWithoutChildren: require('bundle-loader?lazy&name=HamburgerMenuWithoutChildren!core-components/dist/components/HamburgerMenuWithoutChildren/HamburgerMenuWithoutChildren'),
  Header: require('bundle-loader?lazy&name=Header!core-components/dist/components/Header/Header'),
  HeaderContainer: require('bundle-loader?lazy&name=HeaderContainer!core-components/dist/components/Header/HeaderContainer'),
  HorizontalDivider: require('bundle-loader?lazy&name=HorizontalDivider!core-components/dist/components/HorizontalDivider/HorizontalDivider'),
  HorizontalMenu: require('bundle-loader?lazy&name=HorizontalMenu!core-components/dist/components/HorizontalMenu/HorizontalMenu'),
  Icon: require('bundle-loader?lazy&name=Grid!core-components/dist/components/Icon/Icon'),
  IconText: require('bundle-loader?lazy&name=IconText!core-components/dist/components/IconText/IconText'),
  IconTextContainer: require('bundle-loader?lazy&name=IconTextContainer!core-components/dist/components/IconText/IconTextContainer'),
  Image: require('bundle-loader?lazy&name=Image!core-components/dist/components/Image/Image'),
  Legal: require('bundle-loader?lazy&name=Legal!core-components/dist/components/Legal/Legal'),
  LegalContainer: require('bundle-loader?lazy&name=LegalContainer!core-components/dist/components/Legal/LegalContainer'),
  Link: require('bundle-loader?lazy&name=Link!core-components/dist/components/Link/Link'),
  Modal: require('bundle-loader?lazy&name=Modal!core-components/dist/components/Modal/Modal'),
  ModalContainer: require('bundle-loader?lazy&name=ModalContainer!core-components/dist/components/Modal/ModalContainer'),
  OpenModal: require('bundle-loader?lazy&name=OpenModal!core-components/dist/components/OpenModal/OpenModal'),
  Placeholder: require('bundle-loader?lazy&name=Placeholder!core-components/dist/components/Placeholder/Placeholder'),
  QuickLinks: require('bundle-loader?lazy&name=QuickLinks!core-components/dist/components/QuickLinks/QuickLinks'),
  RichText: require('bundle-loader?lazy&name=RichText!core-components/dist/components/RichText/RichText'),
  ShowOnLoading: require('bundle-loader?lazy&name=ShowOnLoading!core-components/dist/components/common/ShowOnLoading'),
  SkipToContent: require('bundle-loader?lazy&name=SkipToContent!core-components/dist/components/SkipToContent/SkipToContent'),
  SkipToContentContainer: require('bundle-loader?lazy&name=SkipToContentContainer!core-components/dist/components/SkipToContent/SkipToContentContainer'),
  SystemSnackbar: require('bundle-loader?lazy&name=SystemSnackbar!core-components/dist/components/SystemSnackbar/SystemSnackbar'),
  Text: require('bundle-loader?lazy&name=Text!core-components/dist/components/Text/Text'),
  Tile: require('bundle-loader?lazy&name=Tile!core-components/dist/components/Tile/Tile'),
  TileContainer: require('bundle-loader?lazy&name=TileContainer!core-components/dist/components/Tile/TileContainer'),
  WaitSplash: require('bundle-loader?lazy&name=WaitSplash!core-components/dist/components/WaitSplash/WaitSplash'),
  WaitSplashContainer: require('bundle-loader?lazy&name=WaitSplashContainer!core-components/dist/components/WaitSplash/WaitSplashContainer'),

  // IFE
  AdvertsCardGrid: require('bundle-loader?lazy&name=AdvertsCardGrid!ife-components/dist/components/AdvertsCardGrid/AdvertsCardGrid'),
  AdvertsCardGridContainer: require('bundle-loader?lazy&name=AdvertsCardGrid!ife-components/dist/components/AdvertsCardGrid/AdvertsCardGridContainer'),
  AdvertsCarousel: require('bundle-loader?lazy&name=AdvertsCarousel!ife-components/dist/components/AdvertsCarousel/AdvertsCarousel'),
  AdvertsCarouselContainer: require('bundle-loader?lazy&name=AdvertsCarouselContainer!ife-components/dist/components/AdvertsCarousel/AdvertsCarouselContainer'),
  GridLayout: require('bundle-loader?lazy&name=GridLayout!ife-components/dist/components/GridLayout/GridLayout'),
  HtmlBundle: require('bundle-loader?lazy&name=HtmlBundle!ife-components/dist/components/HtmlBundle/HtmlBundle'),
  HtmlBundleContainer: require('bundle-loader?lazy&name=Container!ife-components/dist/components/HtmlBundle/HtmlBundleContainer'),
  MediaPlayer: require('bundle-loader?lazy&name=MediaPlayer!ife-components/dist/components/MediaPlayer/MediaPlayer'),
  MediaPlayerContainer: require('bundle-loader?lazy&name=MediaPlayerContainer!ife-components/dist/components/MediaPlayer/MediaPlayerContainer'),
  MovieDetails: require('bundle-loader?lazy&name=MovieDetails!ife-components/dist/components/MovieDetails/MovieDetails'),
  MovieDetailsContainer: require('bundle-loader?lazy&name=MovieDetailsContainer!ife-components/dist/components/MovieDetails/MovieDetailsContainer'),
  Movies: require('bundle-loader?lazy&name=Movies!ife-components/dist/components/Movies/Movies'),
  MoviesContainer: require('bundle-loader?lazy&name=MoviesContainer!ife-components/dist/components/Movies/MoviesContainer'),
  MovingMap: require('bundle-loader?lazy&name=MovingMap!ife-components/dist/components/MovingMap/MovingMap'),
  MovingMapContainer: require('bundle-loader?lazy&name=MovingMapContainer!ife-components/dist/components/MovingMap/MovingMapContainer'),
  TextDialog: require('bundle-loader?lazy&name=TextDialog!ife-components/dist/components/TextDialog/TextDialog'),
  TextDialogContainer: require('bundle-loader?lazy&name=TextDialogContainer!ife-components/dist/components/TextDialog/TextDialogContainer'),
  Music: require('bundle-loader?lazy&name=Music!ife-components/dist/components/Music/Music'),
  MusicContainer: require('bundle-loader?lazy&name=MusicContainer!ife-components/dist/components/Music/MusicContainer'),
  MusicDetails: require('bundle-loader?lazy&name=MovieDetails!ife-components/dist/components/MusicDetails/MusicDetails'),
  MusicDetailsContainer: require('bundle-loader?lazy&name=MusicDetailsContainer!ife-components/dist/components/MusicDetails/MusicDetailsContainer'),
  NotificationDispatcher: require('bundle-loader?lazy&name=NotificationDispatcher!ife-components/dist/components/NotificationDispatcher/NotificationDispatcher'),
  NukaAdvertsCarousel: require('bundle-loader?lazy&name=NukaAdvertsCarousel!ife-components/dist/components/NukaAdvertsCarousel/NukaAdvertsCarousel'),
  NukaAdvertsCarouselContainer: require('bundle-loader?lazy&name=NukaAdvertsCarouselContainer!ife-components/dist/components/NukaAdvertsCarousel/NukaAdvertsCarouselContainer'),
  Playlist: require('bundle-loader?lazy&name=Playlist!ife-components/dist/components/Playlist/Playlist'),
  PlaylistContainer: require('bundle-loader?lazy&name=PlaylistContainer!ife-components/dist/components/Playlist/PlaylistContainer'),
  Series: require('bundle-loader?lazy&name=Series!ife-components/dist/components/Series/Series'),
  SeriesContainer: require('bundle-loader?lazy&name=SeriesContainer!ife-components/dist/components/Series/SeriesContainer'),
  SeriesDetails: require('bundle-loader?lazy&name=SeriesDetails!ife-components/dist/components/SeriesDetails/SeriesDetails'),
  SeriesDetailsContainer: require('bundle-loader?lazy&name=SeriesDetailsContainer!ife-components/dist/components/SeriesDetails/SeriesDetailsContainer'),
  Settings: require('bundle-loader?lazy&name=Settings!ife-components/dist/components/Settings/Settings'),
  SettingsContainer: require('bundle-loader?lazy&name=SettingsContainer!ife-components/dist/components/Settings/SettingsContainer'),

  // IFC
  AaGuestLogin: require('bundle-loader?lazy&name=AaGuestLogin!ifc-components/dist/components/AaGuestLogin/AaGuestLogin'),
  AaGuestLoginContainer: require('bundle-loader?lazy&name=AaGuestLoginContainer!ifc-components/dist/components/AaGuestLogin/AaGuestLoginContainer'),
  AaLogin: require('bundle-loader?lazy&name=AaLogin!ifc-components/dist/components/AaLogin/AaLogin'),
  AaLoginContainer: require('bundle-loader?lazy&name=AaLoginContainer!ifc-components/dist/components/AaLogin/AaLoginContainer'),
  AaPromo: require('bundle-loader?lazy&name=AaPromo!ifc-components/dist/components/AaPromo/AaPromo'),
  AaPromoContainer: require('bundle-loader?lazy&name=AaPromoContainer!ifc-components/dist/components/AaPromo/AaPromoContainer'),
  AaPurchaseSubscription: require('bundle-loader?lazy&name=AaPurchaseSubscription!ifc-components/dist/components/AaPurchaseSubscription/AaPurchaseSubscription'),
  AaPurchaseSubscriptionContainer: require('bundle-loader?lazy&name=AaPurchaseSubscriptionContainer!ifc-components/dist/components/AaPurchaseSubscription/AaPurchaseSubscriptionContainer'),
  AppleMusic: require('bundle-loader?lazy&name=AppleMusic!ifc-components/dist/components/AppleMusic/AppleMusic'),
  AppleMusicContainer: require('bundle-loader?lazy&name=AppleMusicContainer!ifc-components/dist/components/AppleMusic/AppleMusicContainer'),
  Checkout: require('bundle-loader?lazy&name=Checkout!ifc-components/dist/components/Checkout/Checkout'),
  CheckoutContainer: require('bundle-loader?lazy&name=CheckoutContainer!ifc-components/dist/components/Checkout/CheckoutContainer'),
  ContactUsLink: require('bundle-loader?lazy&name=ContactUsLink!ifc-components/dist/components/ContactUsLink/ContactUsLink'),
  ContactUsLinkContainer: require('bundle-loader?lazy&name=ContactUsLinkContainer!ifc-components/dist/components/ContactUsLink/ContactUsLinkContainer'),
  DeviceSwap: require('bundle-loader?lazy&name=Checkout!ifc-components/dist/components/DeviceSwap/DeviceSwap'),
  DeviceSwapContainer: require('bundle-loader?lazy&name=DeviceSwapContainer!ifc-components/dist/components/DeviceSwap/DeviceSwapContainer'),
  FlightProgressBar: require('bundle-loader?lazy&name=FlightProgressBar!ifc-components/dist/components/FlightProgressBar/FlightProgressBar'),
  FlightProgressBarContainer: require('bundle-loader?lazy&name=FlightProgressBarContainer!ifc-components/dist/components/FlightProgressBar/FlightProgressBarContainer'),
  FlightTimeRemaining: require('bundle-loader?lazy&name=FlightTimeRemaining!ifc-components/dist/components/FlightTimeRemaining/FlightTimeRemaining'),
  FlightTimeRemainingContainer: require('bundle-loader?lazy&name=FlightTimeRemainingContainer!ifc-components/dist/components/FlightTimeRemaining/FlightTimeRemainingContainer'),
  FYI: require('bundle-loader?lazy&name=FYI!ifc-components/dist/components/FYI/FYI'),
  FYIContainer: require('bundle-loader?lazy&name=FYIContainer!ifc-components/dist/components/FYI/FYIContainer'),
  HideOnBypassService: require('bundle-loader?lazy&name=HideOnBypassService!ifc-components/dist/components/common/HideOnBypassService'),
  HideOnThirdPartyService: require('bundle-loader?lazy&name=HideOnThirdPartyService!ifc-components/dist/components/common/HideOnThirdPartyService'),
  HideOnDefaultService: require('bundle-loader?lazy&name=HideOnDefaultService!ifc-components/dist/components/common/HideOnDefaultService'),
  HideOnFullFlightService: require('bundle-loader?lazy&name=HideOnFullFlightService!ifc-components/dist/components/common/HideOnFullFlightService'),
  HideOnIfcInitialized: require('bundle-loader?lazy&name=HideOnIfcInitialized!ifc-components/dist/components/common/HideOnIfcInitialized'),
  HideOnIfcUninitialized: require('bundle-loader?lazy&name=HideOnIfcUninitialized!ifc-components/dist/components/common/HideOnIfcUninitialized'),
  HideOnMessagingService: require('bundle-loader?lazy&name=HideOnMessagingService!ifc-components/dist/components/common/HideOnMessagingService'),
  HideOnPremiumService: require('bundle-loader?lazy&name=HideOnPremiumService!ifc-components/dist/components/common/HideOnPremiumService'),
  HideOnServiceAvailable: require('bundle-loader?lazy&name=HideOnServiceAvailable!ifc-components/dist/components/common/HideOnServiceAvailable'),
  HideOnServiceUnavailable: require('bundle-loader?lazy&name=HideOnServiceUnavailable!ifc-components/dist/components/common/HideOnServiceUnavailable'),
  HideWhenLoggedIn: require('bundle-loader?lazy&name=HideWhenLoggedIn!ifc-components/dist/components/common/HideWhenLoggedIn'),
  IfeLink: require('bundle-loader?lazy&name=IfeLink!ifc-components/dist/components/IfeLink/IfeLink'),
  IfeLinkContainer: require('bundle-loader?lazy&name=IfeLinkContainer!ifc-components/dist/components/IfeLink/IfeLinkContainer'),
  JbuPlane: require('bundle-loader?lazy&name=JbuPlane!ifc-components/dist/components/plane-icons/JbuPlane/JbuPlane'),
  LanguageSelector: require('bundle-loader?lazy&name=LanguageSelector!ifc-components/dist/components/LanguageSelector/LanguageSelector'),
  LanguageSelectorContainer: require('bundle-loader?lazy&name=LanguageSelectorContainer!ifc-components/dist/components/LanguageSelector/LanguageSelectorContainer'),
  LoginLogoutButton: require('bundle-loader?lazy&name=LoginLogoutButton!ifc-components/dist/components/LoginLogoutButton/LoginLogoutButton'),
  LoginLogoutButtonContainer: require('bundle-loader?lazy&name=LoginLogoutButtonContainer!ifc-components/dist/components/LoginLogoutButton/LoginLogoutButtonContainer'),
  PaymentConfirmation: require('bundle-loader?lazy&name=Checkout!ifc-components/dist/components/PaymentConfirmation/PaymentConfirmation'),
  PaymentConfirmationContainer: require('bundle-loader?lazy&name=PaymentConfirmationContainer!ifc-components/dist/components/PaymentConfirmation/PaymentConfirmationContainer'),
  PlaneIcon: require('bundle-loader?lazy&name=PlaneIcon!ifc-components/dist/components/plane-icons/PlaneIcon'),
  ServiceCountdown: require('bundle-loader?lazy&name=ServiceCountdown!ifc-components/dist/components/ServiceCountdown/ServiceCountdown'),
  ServiceCountdownContainer: require('bundle-loader?lazy&name=ServiceCountdownContainer!ifc-components/dist/components/ServiceCountdown/ServiceCountdownContainer'),
  ServiceDisruption: require('bundle-loader?lazy&name=ServiceDisruption!ifc-components/dist/components/ServiceDisruption/ServiceDisruption'),
  ServiceDisruptionContainer: require('bundle-loader?lazy&name=ServiceDisruptionContainer!ifc-components/dist/components/ServiceDisruption/ServiceDisruptionContainer'),
  ServiceUnavailableModal: require('bundle-loader?lazy&name=Checkout!ifc-components/dist/components/ServiceUnavailableModal/ServiceUnavailableModal'),
  ServiceUnavailableModalContainer: require('bundle-loader?lazy&name=ServiceUnavailableModalContainer!ifc-components/dist/components/ServiceUnavailableModal/ServiceUnavailableModalContainer'),
  ShowModalOnServiceChange: require('bundle-loader?lazy&name=ShowModalOnServiceChange!ifc-components/dist/components/common/ShowModalOnServiceChange'),
  ShowOnFlightComplete: require('bundle-loader?lazy&name=ShowOnFlightComplete!ifc-components/dist/components/common/ShowOnFlightComplete'),
  ShowOnFlightInProgress: require('bundle-loader?lazy&name=ShowOnFlightInProgress!ifc-components/dist/components/common/ShowOnFlightInProgress'),
  ShowOnFlightStarting: require('bundle-loader?lazy&name=ShowOnFlightStarting!ifc-components/dist/components/common/ShowOnFlightStarting'),
  ShowOnDestinationCodeMatch: require('bundle-loader?lazy&name=ShowOnDestinationCodeMatch!ifc-components/dist/components/common/ShowOnDestinationCodeMatch'),
  SignupBanner: require('bundle-loader?lazy&name=SignupBanner!ifc-components/dist/components/SignupBanner/SignupBanner'),
  SignupBannerContainer: require('bundle-loader?lazy&name=SignupBannerContainer!ifc-components/dist/components/SignupBanner/SignupBannerContainer'),
  SkydealsBillBoard: require('bundle-loader?lazy&name=SkydealsBillBoard!ifc-components/dist/components/SkydealsBillBoard/SkydealsBillBoard'),
  SkydealsBillBoardContainer: require('bundle-loader?lazy&name=SkydealsBillBoardContainer!ifc-components/dist/components/SkydealsBillBoard/SkydealsBillBoardContainer'),
  Statusbar: require('bundle-loader?lazy&name=Statusbar!ifc-components/dist/components/Statusbar/Statusbar'),
  StatusbarContainer: require('bundle-loader?lazy&name=StatusbarContainer!ifc-components/dist/components/Statusbar/StatusbarContainer'),
  TextButton: require('bundle-loader?lazy&name=TextButton!ifc-components/dist/components/TextButton/TextButton'),
  TextButtonContainer: require('bundle-loader?lazy&name=TextButtonContainer!ifc-components/dist/components/TextButton/TextButtonContainer'),
  ThirdPartyLoginLink: require('bundle-loader?lazy&name=ThirdPartyLoginLink!ifc-components/dist/components/ThirdPartyLoginLink/ThirdPartyLoginLink'),
  UserAgreement: require('bundle-loader?lazy&name=UserAgreement!ifc-components/dist/components/UserAgreement/UserAgreement'),
  UserAgreementContainer: require('bundle-loader?lazy&name=UserAgreement!ifc-components/dist/components/UserAgreement/UserAgreementContainer'),
  Weather: require('bundle-loader?lazy&name=Weather!ifc-components/dist/components/Weather/Weather'),
  WeatherContainer: require('bundle-loader?lazy&name=WeatherContainer!ifc-components/dist/components/Weather/WeatherContainer'),
  WeatherDashboard: require('bundle-loader?lazy&name=WeatherDashboard!ifc-components/dist/components/WeatherDashboard/WeatherDashboard'),
  WeatherDashboardContainer: require('bundle-loader?lazy&name=WeatherDashboardContainer!ifc-components/dist/components/WeatherDashboard/WeatherDashboardContainer'),
  WifiNotificationScroll: require('bundle-loader?lazy&name=WifiNotificationScroll!ifc-components/dist/components/WifiNotificationScroll/WifiNotificationScroll'),
  WifiNotificationScrollContainer: require('bundle-loader?lazy&name=WifiNotificationScrollContainer!ifc-components/dist/components/WifiNotificationScroll/WifiNotificationScrollContainer'),
  WifiServiceOptions: require('bundle-loader?lazy&name=WifiServiceOptions!ifc-components/dist/components/WifiServiceOptions/WifiServiceOptions'),
  WifiServiceOptionsContainer: require('bundle-loader?lazy&name=WifiServiceOptionsContainer!ifc-components/dist/components/WifiServiceOptions/WifiServiceOptionsContainer')
};

const loadedComponents: IndexInterface = {};

const loadComponent = (componentName: string) => {
  return new Promise((resolve, reject) => {
    const componentBundle = loadedComponentBundles[componentName];
    if (componentBundle) {
      componentBundle((mod: any) => {
        const containerComponent = componentName + CONTAINER_PREFIX;
        loadedComponents[componentName] = mod.default;

        if (loadedComponentBundles[containerComponent]) {
          loadedComponentBundles[containerComponent]((containerMod: any) => {
            loadedComponents[containerComponent] = containerMod.default;
            resolve();
          });
        } else {
          resolve();
        }
      });
    } else {
      loadedComponents[componentName] = {}; // component doesn't exist a bundle, this is ok, just mark as loaded so we don't try again
      resolve();
    }
  });
};

const loadRequiredComponents = (layout: LayoutInterface) => {
  let componentsToLoad = clone(MandatoryComponents);

  componentsToLoad = uniq(
    componentsToLoad
      .concat(
        Object.keys(layout.components).map((componentUid: string) => layout.components[componentUid].componentName)
      )
      .filter((componentName: string) => !loadedComponents[componentName])
  );

  if (componentsToLoad.length) {
    console.info('Loading Components: ', componentsToLoad);
    return Promise.all(componentsToLoad.map((componentName: string) => loadComponent(componentName)));
  } else {
    return new Promise(resolve => resolve([]));
  }
};

const componentLibsUsed = (layout: LayoutInterface) =>
  compose(
    uniq as any,
    map((cUuid: string) => layout.components[cUuid].componentLib)
  )(keys(layout.components)) as string[];

const runComponentLibInitializers: AllLibrariesInitFunction = (
  render: React.ReactElement,
  store: Store<any>,
  settings: Settings,
  layout: LayoutInterface,
  epicMiddleware: EpicMiddleware<any>,
) => {
  let finalRender = render;

  componentLibsUsed(layout).forEach((libName: string) => {
    const lib: LibraryInterface = loadedComponentLibraries[libName];
    if (lib && lib.init) {
      console.info(`Initializing Component Library: ${libName}`);
      finalRender = lib.init(finalRender, store, settings[libName], epicMiddleware);
    }
  });
  return finalRender;
};

export {loadedComponents, loadRequiredComponents, loadComponent, runComponentLibInitializers, componentLibsUsed};
