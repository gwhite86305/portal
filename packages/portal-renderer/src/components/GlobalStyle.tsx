import {createGlobalStyle} from 'styled-components';
import {ThemeSchema} from 'component-utils/dist/components/common/ThemeInterface';

export default createGlobalStyle`

    @font-face {
        /* Legacy iOS */
        font-style: normal;
        font-weight: normal;
        text-rendering: optimizeLegibility;
    }

    /****************************************************
    * Core page styles
    */

    * {
        -webkit-touch-callout: none;
        -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
        -webkit-tap-highlight-color: transparent;
        outline: none !important;
    }

    input:-webkit-autofill {
        background-color: #ffffff !important;
        background-image: none !important;
        color: #000000 !important;
        -webkit-box-shadow: 0 0 0px 1000px #fff inset;
    }

    body {
        margin: 0;
        background-repeat: no-repeat;
        background-attachment: fixed;
        user-select: none;
        -webkit-overflow-scrolling: touch;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        width: 100%; /* ensure body 100% on full screen mode */
    }


    /* ensure all root elements are height 100% as AppContainer child represents 
    the container for the entire app and must fill full screen (see BuilderComponentOverlay) */

    html,
    body,
    #root,
    #app {
        height: 100%;
    }

    @media all and (-ms-high-contrast: none),
    (-ms-high-contrast: active) {
        body {
            width: 100%;
            overflow-x: hidden;
        }
    }

    html, body {
        background-color: white;
        font-family: ${({theme}: ThemeSchema) => theme.appFontFamily};
        font-size: ${({theme}: ThemeSchema) => theme.defaultFontSize};
        background: ${({theme}: ThemeSchema) => theme.primaryBackgroundColor};
    }
    
    /* TODO does this belong here? Surely this can be on the component style? */
    *:focus {
        outline: ${({theme}: ThemeSchema) => theme.primaryFocusBorder} solid ${({theme}: ThemeSchema) =>
  theme.primaryFocusColor} !important;
    }
    .secondary-focus:focus {
        outline: ${({theme}: ThemeSchema) => theme.primaryFocusBorder} solid ${({theme}: ThemeSchema) =>
  theme.secondaryFocusColor} !important;
    }
    .tertiary-focus:focus {
        outline: ${({theme}: ThemeSchema) => theme.primaryFocusBorder} solid ${({theme}: ThemeSchema) =>
  theme.tertiaryFocusColor} !important;
    }
    .js-focus-visible *:focus:not(.focus-visible) {
      outline: none !important;
    }

    /* TODO does this not belong in the modal itself?? */
    .ReactModal__Content:focus {
        outline: none !important;
    }
`;
