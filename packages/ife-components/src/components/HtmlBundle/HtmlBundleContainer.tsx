import {PortalComponent, utils} from 'component-utils';
import * as React from 'react';
import HtmlBundle from './HtmlBundle';
import {HtmlBundle as ExternalInterface} from '../common/ComponentsInterface';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import {HtmlBundleContainerProps, HtmlBundleContainerState} from './types';

const connectWrapper = utils.connectWrapper.default;
@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class HtmlBundleContainer extends React.Component<
  HtmlBundleContainerProps & ExternalInterface,
  HtmlBundleContainerState
> {


  componentDidMount() {
    const {getHtmlBundle, feature: {category}} = this.props;
    if (category) {
      getHtmlBundle(category);
    }
  }

  render() {
    const {htmlBundleUrl, error, isFetching} = this.props;
    let inner;
    if (isFetching) {
      inner = <div>loading htmlBundle</div>;
    } else if (error || !htmlBundleUrl) {
      inner = <div>error</div>;
    } else {
      inner = <HtmlBundle htmlBundleUrl={htmlBundleUrl} />;
    }
    return <PortalComponent componentTheme={this.props.componentTheme}>{inner}</PortalComponent>;
  }
}
