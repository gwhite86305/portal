import {cookiesClient, CookiesClient} from '../dataAccessClients/CookiesClient';
import {UserSettings} from '../store/settings/types';
import {NO_AGE_RESTRICTION} from '../store/settings/reducer';

export class UserSettingsService {
  private settingsKey = 'IFE_SETTINGS';
  private expirationDays = 2;

  public saveSettings = (settings: UserSettings) => cookiesClient.set(this.settingsKey, settings, this.expirationDays);

  public getSettings = (): UserSettings =>
    cookiesClient.get<UserSettings | undefined>(this.settingsKey) || {
      ageRatingClassSelected: NO_AGE_RESTRICTION,
      ageRatingPassword: ''
    };

  public deleteSettings = () => cookiesClient.remove(this.settingsKey);
}

export const userSettingsService = new UserSettingsService();
