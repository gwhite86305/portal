import {MovingMap as ExternalInterface} from '../common/ComponentsInterface';

export interface BackNavigationProps extends ExternalInterface {
  onClickBack(): () => void;
}
