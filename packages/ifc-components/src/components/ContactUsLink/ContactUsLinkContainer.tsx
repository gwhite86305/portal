import * as React from 'react';
import {utils} from 'component-utils';
import {pathOr} from 'ramda';
import {ContactUsLink as ExternalInterface} from '../common/ComponentsInterface';
import ContactUsLink, {InternalInterface} from './ContactUsLink';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

const connectWrapper = utils.connectWrapper.default;

export type PageProps = ExternalInterface & InternalInterface & {actions: any};

@connectWrapper((state: any, ownProps: any) => {
  let deviceKeysDefault = ['flightOrigin', 'flightDestination', 'flightNumber', 'tailId', 'language', 'guid'];
  let param = '';
  let ICAO = '';
  let chatURL = '';
  let chatURLPrefix = pathOr(
    'https://inflight.viasat.com',
    ['profile', 'contentPack', 'default', 'defaultSupportPage'],
    state
  );

  if (ownProps.feature.enrichChatUrl) {
    deviceKeysDefault.forEach((key, index) => {
      if (state.device && state.device[key]) {
        param = `${param}${key}=${state.device[key]}`;
        if (index != deviceKeysDefault.length - 1) {
          param = `${param}&`;
        }
      }
    });
    if (state && state.ux && state.ux.settings && state.ux.settings.ifc && state.ux.settings.ifc.ICAO) {
      ICAO = state.ux.settings.ifc.ICAO;
    }
    chatURL = `${chatURLPrefix}/${ICAO}?` + param;
  } else {
    chatURL = `${chatURLPrefix}`;
  }
  return {
    chatURL: chatURL
  };
})
class ContactUsLinkContainer extends React.Component<PageProps, any> {
  public render() {
    return <ContactUsLink {...this.props} />;
  }
}

export default withLocaleProps(ContactUsLinkContainer, [
  {helpTitle: 'ifc.ContactUsLink.help.title'},
  {chatTitle: 'ifc.ContactUsLink.chat.title'},
  {externalTitle: 'ifc.ContactUsLink.external.title'},
  {externalAltText: 'ifc.ContactUsLink.external.iconAltText'},
  {faqsTitle: 'ifc.ContactUsLink.faqs.title'},
  {chatAltText: 'ifc.ContactUsLink.chat.iconAltText'},
  {helpAltText: 'ifc.ContactUsLink.help.iconAltText'},
  {faqsAltText: 'ifc.ContactUsLink.faqs.iconAltText'}
]) as any;
