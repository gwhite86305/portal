import {AudioGroup, AudioCategory} from '../../store/audio/types';
import {Music as ExternalInterface} from '../common/ComponentsInterface';
import {trackImpression} from '../../store/analytics/actions';

export interface MusicProps {
  items: AudioCategory[];
  onItemClick(contentUid: string): any;
  onItemInViewPort: typeof trackImpression;
}

// -- Interfaces
export interface StateProps {
  isLoading: boolean;
  items: AudioCategory[];
}
export interface DispatchProps {
  trackImpression: typeof trackImpression;
}
export interface Props extends StateProps, DispatchProps, ExternalInterface {}
