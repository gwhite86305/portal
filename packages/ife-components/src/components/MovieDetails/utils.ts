import {IRootState} from './../../index';
import {MovieDetails as ExternalInterface} from '../common/ComponentsInterface';
import {VideoItem} from './../../store/videos/types';
import {SystemMessage} from 'component-utils/dist/store/system/reducer';
import {hideMessage, showMessage} from 'component-utils/dist/store/system/actions';
import {getPreRollAds, getVideoById} from '../../store/videos/actions';
import {pipe, map, join} from 'ramda';
import {capitalize} from 'lodash-es';
import {addItemsToPlaylist, removeItemFromPlaylist} from '../../store/mediaplayer/actions.playlist';
import {playItem, playItemTrailer} from '../../store/mediaplayer/actions.player';
import {trackView, trackPlay} from '../../store/analytics/actions';
import {selectPlayListItems, selectPlaylistLoading} from '../../store/mediaplayer/selector';
import {AudioItem} from '../../store/audio/types';

// Container
export interface StoreProps {
  playlistItems: (AudioItem | VideoItem)[];
  videoNotFound: boolean;
  selectedItem?: VideoItem;
  playlistLoading: boolean;
}
export interface DispatchProps {
  actions: {
    addToPlaylist: typeof addItemsToPlaylist;
    getPreRollAds: typeof getPreRollAds;
    getVideoById: typeof getVideoById;
    removeFromPlaylist: typeof removeItemFromPlaylist;
    playItem: typeof playItem;
    playItemTrailer: typeof playItemTrailer;
    trackView: typeof trackView;
    trackPlay: typeof trackPlay;
    showMessage: (message: SystemMessage) => void;
    hideMessage: () => void;
  };
}
export interface DetailsContainerProps extends StoreProps, DispatchProps, ExternalInterface {}
export interface DetailsContainerState {
  contentId: string;
}
export const mapStoreToProps = (state: IRootState): StoreProps => ({
  playlistItems: selectPlayListItems(state),
  videoNotFound: state.ifeVideos.videoNotFound,
  selectedItem: state.ifeVideos.selectedItem as VideoItem,
  playlistLoading: selectPlaylistLoading(state.ifeMediaPlayer)
});
export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    addToPlaylist: items => dispatch(addItemsToPlaylist(items)),
    getPreRollAds: videoId => dispatch(getPreRollAds(videoId)),
    getVideoById: uid => dispatch(getVideoById(uid)),
    removeFromPlaylist: id => dispatch(removeItemFromPlaylist(id)),
    playItem: (item, playerPage) => dispatch(playItem(item, playerPage)),
    playItemTrailer: (item, playerPage) => dispatch(playItemTrailer(item, playerPage)),
    trackView: item => dispatch(trackView(item)),
    trackPlay: item => dispatch(trackPlay(item)),
    showMessage: (message: SystemMessage) => dispatch(showMessage(message, 0)),
    hideMessage: () => dispatch(hideMessage())
  }
});

// Component
export interface MovieDetailsProps {
  imgPlaceholder: string;
  isInPlaylist: boolean;
  item: VideoItem;
  onAddToPlaylist(items: VideoItem[]): any;
  onClickBack(): any;
  onClickPlay(item: any): any;
  onClickPlayTrailer(item: any): any;
  onRemoveFromPlaylist(uid: string): any;
  onPurchaseSuccess: any;
}

export interface DetailsProp {
  label: string;
  value: any;
}

export interface RatingProp {
  stars: number;
}

export const appendOrUndefined = (value: any, valueToAppend: string) => (value ? value + valueToAppend : undefined);
export const capitaliseAndJoin = pipe(
  value => (Array.isArray(value) ? value : [value]),
  map(capitalize),
  join(', ')
);
export const imgPlaceholder = 'http://via.placeholder.com/' + 600 + 'pxx' + 420 + 'px';
