import * as React from 'react';
import {PortalComponent, ResponsiveImage, utils, sanitize} from 'component-utils';
import {UserAgreement as ExternalInterface} from '../common/ComponentsInterface';
import Check from '@material-ui/icons/Check';
import * as defaults from './defaultProps.json';
import {Button, Icon} from 'core-components';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper, renderChildComponent} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
  onAccept?: () => void;
  content: any;
}

const UserAgreementWrapper = styled(CommonComponentThemeWrapper)`
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  width: 100%;
  /* This block of code is to cover the entire window with this component.  */
  ${renderThemeProperty('position', 'position')};
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  z-index: 9001;
  overflow: scroll;
  /* This block of code is to cover the entire window with this component */
  .ua-content {
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'margin-left',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*2.5)`},
        props.theme
      )};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'margin-right',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*2.5)`},
        props.theme
      )};
    .motif {
      ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.motifTextMargin, props.theme)};
      ${renderThemeProperty('motifTextFontFamily', 'font-family')};
      ${renderThemeProperty('motifTextColor', 'color')};
      ${renderThemeProperty('motifTextFontSize', 'font-size')};
      ${renderThemeProperty('motifTextFontWeight', 'font-weight')};
      line-height: 1.04;
    }
    .notice-header {
      ${(props: any) =>
        applyMediaQueriesForStyle('margin', props.theme.componentTheme.noticeHeaderMargin, props.theme)};
      ${renderThemeProperty('noticeHeaderFontSize', 'font-size')};
      ${renderThemeProperty('noticeHeaderTextColor', 'color')};
      ${renderThemeProperty('noticeHeaderFontWeight', 'font-weight')};
    }
    .notice-text {
      ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.noticeTextMargin, props.theme)};
      ${renderThemeProperty('noticeTextColor', 'color')};
      ${renderThemeProperty('noticeTextFontSize', 'font-size')};
      ${renderThemeProperty('noticeTextFontWeight', 'font-weight')};
    }
    .effective-date {
      ${(props: any) =>
        applyMediaQueriesForStyle('margin', props.theme.componentTheme.effectiveDateMargin, props.theme)};
      ${renderThemeProperty('effectiveDateTextColor', 'color')};
      ${renderThemeProperty('effectiveDateTextFontSize', 'font-size')};
    }
    .consent-declaration {
      ${renderThemeProperty('consentDeclarationTextColor', 'color')};
      ${renderThemeProperty('consentDeclarationTextFontSize', 'font-size')};
    }
    .consent-declaration-header {
      ${renderThemeProperty('consentDeclarationTextColor', 'color')};
      ${renderThemeProperty('consentDeclarationHeaderFontSize', 'font-size')};
      padding-top: calc(${(props: any) => props.theme.gutter} * 2.5);
      margin-top: 0;
      margin-bottom: 0;
      font-weight: normal;
    }
  }
  .footer-wrapper {
    ${renderThemeProperty('footerWrapperColor', 'background-color')};
  }
  div.footer {
    ${renderThemeProperty('footerBackgroundColor', 'background-color')};
    color: ${(props: any) => props.theme.tertiaryTextColor};
    padding-top: calc(${(props: any) => props.theme.gutter} * 0.875);
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-left',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*9.875)`},
        props.theme
      )};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-right',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*9.875)`},
        props.theme
      )};
  }
  .footer-additonal-checkbox {
    ${renderThemeProperty('footerBackgroundColor', 'background-color')};
    color: ${(props: any) => props.theme.tertiaryTextColor};
    border: 0;
    border-top-width: 1px;
    border-top-style: solid;
    border-top-color: ${(props: any) => props.theme.primaryDividerColor};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-left',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*9.875)`},
        props.theme
      )};
    ${(props: any) =>
      applyMediaQueriesForStyle(
        'padding-right',
        {xs: `calc(${props.theme.gutter}*1.25)`, md: `calc(${props.theme.gutter}*9.875)`},
        props.theme
      )};
  }
  .footer-text {
    cursor: pointer;
    flex: 0 1 auto;
    text-indent: calc(${(props: any) => props.theme.gutter} * -2.375);
    padding-top: calc(${(props: any) => props.theme.gutter});
    padding-bottom: calc(${(props: any) => props.theme.gutter});
    padding-left: calc(${(props: any) => props.theme.gutter} * 2.5);
    padding-right: calc(${(props: any) => props.theme.gutter} * 3.125);
    span {
      position: relative;
      font-size: calc(${(props: any) => props.theme.primaryFontSize} * 0.875);
      ${renderThemeProperty('footerTextSpanPaddingLeft', 'padding-left')};
      ${renderThemeProperty('lineHeight', 'line-height')};
    }
    ${renderThemeProperty('footerTextColor', 'color')};
  }
  .checked {
    ${renderThemeProperty('checkBoxWidth', 'width')};
    ${renderThemeProperty('checkBoxHeight', 'height')};
    border-style: solid;
    vertical-align: top;
    border-width: 1.5px;
    ${renderThemeProperty('checkBoxBorderRadius', 'border-radius')};
    ${renderThemeProperty('checkBoxBorderColor', 'border-color')};
    ${renderThemeProperty('checkedBackgroundColor', 'background')};
    :before {
      padding-left: calc(${(props: any) => props.theme.gutter} * 2.4375);
      @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        padding-left: 0px;
      }
      border-color: ${(props: any) => props.theme.primaryBackgroundColor};
      color: ${(props: any) => props.theme.secondaryBackgroundColor};
    }
  }
  .unchecked {
    ${renderThemeProperty('checkBoxWidth', 'width')};
    ${renderThemeProperty('checkBoxHeight', 'height')};
    border-style: solid;
    vertical-align: top;
    border-width: 1.5px;
    ${renderThemeProperty('checkBoxBorderRadius', 'border-radius')};
    ${renderThemeProperty('checkBoxBorderColor', 'border-color')};
    ${renderThemeProperty('footerBackgroundColor', 'background-color')};
    ${renderThemeProperty('footerBackgroundColor', 'color')};
  }
  .button-wrap {
    ${renderThemeProperty('footerBackgroundColor', 'background-color')};
    align-self: center;
    padding-top: calc(${(props: any) => props.theme.gutter} * 0.875);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.875);
    text-align: center;
  }
  button {
    ${renderThemeProperty('buttonWidth', 'width')};
    height: ${(props: any) => (props.theme.buttonHeight ? props.theme.buttonHeight : '45px')};
    margin: 0 auto;
    background: ${(props: any) => props.theme.primaryButtonColor};
    color: ${(props: any) => props.theme.primaryIconColor};
    justify-content: center;
    ${renderThemeProperty('buttonFontWeight', 'font-weight')};

    border: ${(props: any) => (props.theme.buttonBorder ? props.theme.buttonBorder : '0')};
    ${renderThemeProperty('buttonBorderColor', 'border-color', true)};
    ${(props: any) =>
      applyMediaQueriesForStyle('border-width', props.theme.componentTheme.buttonBorderWidth, props.theme)};

    &:focus,
    &:hover {
      border: none !important;
    }
  }
  .disabled {
    color: ${(props: any) =>
      props.theme.primaryTextDisabledColor ? props.theme.primaryTextDisabledColor : props.theme.primaryTextColor};
    background: ${(props: any) => props.theme.primaryButtonInactiveColor};
    ${renderThemeProperty('buttonDisabledBorderColor', 'border-color', true)};
    :hover {
      border: ${(props: any) => (props.theme.buttonBorder ? props.theme.buttonBorder : '0')};
      color: ${(props: any) =>
        props.theme.primaryTextActiveColor || props.theme.primaryTextDisabledColor || props.theme.primaryTextColor};
      background: ${(props: any) => props.theme.primaryButtonInactiveColor};
      ${renderThemeProperty('buttonDisabledBorderColor', 'border-color', true)};
    }
    :focus {
      border: ${(props: any) => (props.theme.buttonBorder ? props.theme.buttonBorder : '0')};
      color: ${(props: any) =>
        props.theme.primaryTextDisabledColor ? props.theme.primaryTextDisabledColor : props.theme.primaryTextColor};
      background: ${(props: any) => props.theme.primaryButtonInactiveColor};
      ${renderThemeProperty('buttonDisabledBorderColor', 'border-color', true)};
    }
  }
  .span-vertical {
    vertical-align: top;
  }
`;

const BackLink = (props: any) => {
  const {localizedBackText} = props;
  const {backLinkUrl, backLinkImage, backLinkIcon} = props.feature;
  const Container = styled.div`
    ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.motifTextMargin, props.theme)};

    a, a * {
      ${renderThemeProperty('backLinkTextColor', 'color', true)};
      ${renderThemeProperty('backLinkFontSize', 'font-size', true)};
      ${renderThemeProperty('backLinkFontWeight', 'font-weight', true)};
      ${renderThemeProperty('backLinkLineHeight', 'line-height', true)};
      ${renderThemeProperty('backLinkTextDecoration', 'text-decoration', true)};
    }

    i {
      margin-right: 7.5px;
    }
  `;
  return <Container> {
            backLinkUrl ?
              (<a href={backLinkUrl}>
                  <span>
                    {backLinkImage ? (<img src={backLinkIcon} />) : backLinkIcon ? <Icon feature={{name: backLinkIcon}} /> : <ArrowBackIcon />}
                    {localizedBackText}
                  </span>
                </a>
              ) : null
        }</Container>
}

const Header = (props: any) => {
  const {headerImage, componentTheme} = props;
  const Container = styled.div`
    ${(props: any) => applyMediaQueriesForStyle('height', componentTheme.headerHeight, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('background-image', headerImage, props.theme)};
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
  `;
  return <Container>{props.children}</Container>;
};

class UserAgreement extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      checked1: false,
      checked2: false
    };
  }

  toggle1 = (e: any) => {
    //do not toggle the state if it's a link
    if (e.target.nodeName != 'A') {
      this.setState((prevState: any) => ({checked1: !prevState.checked1}));
    }
  };
  toggle2 = (e: any) => {
    //do not toggle the state if it's a link
    if (e.target.nodeName != 'A') {
      this.setState((prevState: any) => ({checked2: !prevState.checked2}));
    }
  };

  public render() {
    const {children, content, componentTheme, localizedAltImageText, localizedBackText} = this.props;
    const {header, terms, privacy, effectiveDate, agreements, consentDeclaration} = content;
    const {headerImage, airlineLogo, useMaterial, backLinkUrl, backLinkIcon} = this.props.feature;
    const checked = useMaterial ? <Check className="checked" /> : <i className="fa fa-check checked" />;
    const unchecked = <i className="fa fa-check unchecked" />;

    return (
      <PortalComponent componentTheme={componentTheme}>
        <UserAgreementWrapper className="core-useragreement-component">
          <div className="ua-body">
            {headerImage ? (
              <Header headerImage={headerImage} componentTheme={componentTheme}>
                {airlineLogo ? (
                  <ResponsiveImage
                    image={airlineLogo}
                    altText={localizedAltImageText}
                    imageMargin={componentTheme && componentTheme.airlineLogoMargin}
                    imageWidth={componentTheme && componentTheme.airlineLogoWidth}
                  />
                ) : null}
              </Header>
            ) : null}

            <div className="ua-content">
              <BackLink {...this.props} />
              <h1 className="motif">{header.motifText}</h1>
              <h2 className="notice-header">{header.noticeHeader}</h2>
              <h3 className="notice-text" dangerouslySetInnerHTML={sanitize(header.noticeText)} />

              {renderChildComponent(0, children, {
                feature: {
                  title: terms.termsHeader,
                  anchor: 'terms',
                  sections: terms.termsSections,
                  useMaterial: useMaterial
                }
              })}
              {this.props.feature.showPrivacyPolicy
                ? renderChildComponent(0, children, {
                    feature: {
                      title: privacy.privacyHeader,
                      anchor: 'privacy-policy',
                      sections: privacy.privacySections,
                      useMaterial: useMaterial
                    }
                  })
                : null}
              {this.props.feature.showConsentDeclaration && consentDeclaration ? (
                <div className="consent-declaration">
                  <h4 className="consent-declaration-header">{consentDeclaration.consentDeclarationHeader}</h4>
                  <div dangerouslySetInnerHTML={sanitize(consentDeclaration.consentDeclarationContent)} />
                </div>
              ) : null}
              <p className="effective-date">{effectiveDate.effectiveDateText}</p>
            </div>
          </div>
          <div className="footer-wrapper">
            <div className="footer">
              <div
                className="footer-text secondary-focus"
                tabIndex={0}
                onKeyPress={this.toggle1}
                onClick={this.toggle1}
              >
                {this.state.checked1 ? checked : unchecked}
                <span className="span-vertical" dangerouslySetInnerHTML={sanitize(agreements.agreementCheckOneText)} />
              </div>
            </div>
            {this.props.feature.includeAddtionalCheckbox ? (
              <div className="footer-additonal-checkbox">
                <div
                  className="footer-text secondary-focus"
                  tabIndex={0}
                  onKeyPress={this.toggle2}
                  onClick={this.toggle2}
                >
                  {this.state.checked2 ? checked : unchecked}
                  <span
                    className="span-vertical"
                    dangerouslySetInnerHTML={sanitize(agreements.agreementCheckTwoText)}
                  />
                </div>
              </div>
            ) : null}
            <div className="button-wrap">
              <Button
                isDisabled={
                  this.props.feature.includeAddtionalCheckbox
                    ? !this.state.checked1 || !this.state.checked2
                    : !this.state.checked1
                }
                onClick={this.props.buttonClickHook}
                isPrimary={true}
              >
                {agreements.buttonText}
              </Button>
            </div>
          </div>
        </UserAgreementWrapper>
      </PortalComponent>
    );
  }
}

export default UserAgreement;
