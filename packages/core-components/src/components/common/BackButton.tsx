import * as React from 'react';
import {createBrowserHistory} from 'history';
import {utils, PortalComponent} from 'component-utils';
import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

const ButtonContainer = styled.button`
  cursor: pointer;
  height: ${(props: any) => (props.height ? props.height : '45px')};
  font-size: 1.1rem;
  margin-bottom: 0;
  font-weight: normal;
  text-align: center;
  vertical-align: middle;
  border: 0;
  font-family: inherit;
  border-radius: ${(props: any) => props.theme.buttonRadius};
  float: left;

  &:active {
    outline: 0;
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
  }

  &.primary-button {
    background: ${(props: any) => props.theme.primaryButtonColor};
    color: ${(props: any) => props.theme.primaryButtonTextColor};

    &:hover {
      background: ${(props: any) => props.theme.primaryButtonHoverColor};
      color: ${(props: any) => props.theme.primaryButtonHoverTextColor};
    }
  }

  &.secondary-button {
    background: ${(props: any) => props.theme.secondaryButtonColor};
    color: ${(props: any) => props.theme.secondaryButtonTextColor};

    &:hover {
      background: ${(props: any) => props.theme.secondaryButtonHoverColor};
      color: ${(props: any) => props.theme.secondaryButtonHoverTextColor};
    }
  }
`;

const history = createBrowserHistory();

class BackButton extends React.Component<any, any> {
  clickBack() {
    console.log(history);
    if (history.length > 2) {
      history.goBack();
    }
    // history.goBack()
  }

  public render() {
    return <ButtonContainer onClick={this.clickBack.bind(this)}>Back</ButtonContainer>;
  }
}

export default BackButton;
