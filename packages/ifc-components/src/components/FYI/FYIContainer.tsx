import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {FYI as ExternalInterface} from '../common/ComponentsInterface';
import FYI, {InternalInterface} from './FYI';
import {PortalComponent, utils} from 'component-utils';

export type PageProps = ExternalInterface & InternalInterface & {actions: any};

class FYIContainer extends React.Component<PageProps, any> {
  public render() {
    return <FYI {...this.props} />;
  }
}

export default withLocaleProps(FYIContainer, [
  {altTextForImage: 'ifc.FYI.altTextForImage.text'},
  {subHeaderText: 'ifc.FYI.subHeader.text'},
  {headerText: 'ifc.FYI.header.text'},
  {buttonText: 'ifc.FYI.button.text'}
]) as any;
