import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {axiosWithBaseUrl as axios} from '../../utils/utils';

export const ActionCreators = {
  loadMovingMap: new ActionCreator<'loadMovingMap', any>('loadMovingMap'),
  loadMovingMapSuccess: new ActionCreator<'loadMovingMapSuccess', any>('loadMovingMapSuccess'),
  loadMovingMapError: new ActionCreator<'loadMovingMapError', any>('loadMovingMapError'),
  followPlane: new ActionCreator<'followPlane', {}>('followPlane'),
  unFollowPlane: new ActionCreator<'unFollowPlane', {}>('unFollowPlane')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function loadMovingMap() {
  return async (dispatch: any) => {
    dispatch(ActionCreators.loadMovingMap.create({}));
    try {
      const {data} = await axios.get('/content/movingmap');
      dispatch(ActionCreators.loadMovingMapSuccess.create(data));
    } catch (e) {
      dispatch(ActionCreators.loadMovingMapError.create({}));
    }
  };
}

export const followPlane = () => (dispatch: any) => dispatch(ActionCreators.followPlane.create({}));
export const unFollowPlane = () => (dispatch: any) => dispatch(ActionCreators.unFollowPlane.create({}));
