import * as React from 'react';
import {applyMediaQueriesForStyle} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const StyledContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999;

  /* Fix minor issues react-hamburger-menu package */
  .bm-overlay {
    top: 0;
  }
  .bm-item-list {
    height: auto !important;
  }
  .bm-cross-button {
    top: 0 !important;
    right: 0 !important;
  }

  /* Responsivess */
  .bm-menu-wrap {
    top: 0;
    ${() => applyMediaQueriesForStyle('width', {xs: '100%', md: '40%'}, {}, true)};
    background-color: rgba(0, 0, 0, 0.9);
  }
  .bm-item-list {
    ${() => applyMediaQueriesForStyle('padding', {xs: '50px 30px', md: '50px 110px'}, {})};
  }
`;

export default StyledContainer;
