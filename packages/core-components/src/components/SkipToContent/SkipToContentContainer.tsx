import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import SkipToContent from './SkipToContent';

const SkipToContentContainer: React.FC<any> = (props: any) => {
  return <SkipToContent {...props} />;
};

export default withLocaleProps(SkipToContentContainer, [
  {skipNavText: 'core.Header.skipNav.text'}
]) as any;
