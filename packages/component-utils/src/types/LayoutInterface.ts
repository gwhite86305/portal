import {Component} from './Component';

export interface Components {
  [uuid: string]: Component;
}

export interface Meta {
  portalUid: string;
  componentsVersion: string;
  defaultLocale: string;
}

export interface App {
  template: string | null;
  pages: string[];
  homePage: string;
  meta: Meta;
}

export type LocaleMessages = {
  [localeMessageName: string]: string;
};

export type Locale = {
  code: string;
  title: string;
  messages: LocaleMessages;
};

export type Locales = Locale[];

type LayoutInterface = {
  app: App;
  components: Components;
  locale: Locales;
};

export default LayoutInterface;
