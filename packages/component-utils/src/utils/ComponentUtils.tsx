import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import {applyMediaQueriesForStyle, renderThemeProperty} from './styledComponents';
import styled, {StyledComponent} from 'styled-components';
import storeAccess from './storeAccess';
import LocalStorageDataType from '../utils/LocalStorageDataType';

export const isEnvUXBuilder = () => (process.env.PCMS_ENV_BUILDER ? process.env.PCMS_ENV_BUILDER : false);

export const getBuilderApi = () =>
  process.env.API_BUILDER_URL ? process.env.API_BUILDER_URL + process.env.PCMS_PORTAL_BUILDER_API_ENDPOINT : '';

export const isUrlValid = (value: any) => {
  if (typeof value === 'string') {
    return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(
      value.trim()
    );
  } else if (typeof value === 'object') {
    return Object.keys(value).every((urlValue: string) => {
      return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(
        value[urlValue].trim()
      );
    });
  } else {
    return false;
  }
};

export const isValidRelativePath = (value: any) => {
  if (typeof value === 'string') {
    return /(\.\.|\.)?\/[^\n"?:*<>|]+([a-z0-9]+\/?)+(\.[a-z0-9]+)?/g.test(value.trim());
  } else if (typeof value === 'object') {
    return Object.keys(value).every((urlValue: string) => {
      return /(\.\.|\.)?\/[^\n"?:*<>|]+([a-z0-9]+\/?)+(\.[a-z0-9]+)?/g.test(value[urlValue].trim());
    });
  } else {
    return false;
  }
};

export const getCurrency = () => {
  let currencyCode = null;

  try {
    currencyCode = storeAccess.get().getState().ifeSystem.userSettings.userSelectedCurrency;
  } catch (e) {
    currencyCode = localStorage[LocalStorageDataType.currency] || 'USD';
  }

  return currencyCode;
};

export const renderChildComponent = (index: number, children: any, injectProps: any = {}) => {
  if (children && children[index]) {
    const childWrapperComponent: any = children[index];
    return React.cloneElement(childWrapperComponent, {
      children: React.cloneElement(childWrapperComponent.props.children, injectProps)
    });
  }
  return null;
};

const DEFAULT_EMPTY_GRID_HEIGHT = '3rem';
export const GridLayoutThemeWrapper = styled((props: any) => {
  const {children, ...rest} = props;
  return (
    <Grid
      className={`${props.className}`}
      {...rest}
      style={!children || children.length === 0 ? {minHeight: DEFAULT_EMPTY_GRID_HEIGHT} : {}}
    >
      {children}
    </Grid>
  );
})`
  ${renderThemeProperty('boxShadow', 'box-shadow')};
  ${renderThemeProperty('background', 'background')};
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('position', 'position')};
  ${renderThemeProperty('left', 'left')};
  ${renderThemeProperty('top', 'top')};
  ${renderThemeProperty('right', 'right')};
  ${renderThemeProperty('bottom', 'bottom')};
  ${renderThemeProperty('zIndex', 'z-index')};
  ${renderThemeProperty('borderStyle', 'border-style')};
  ${renderThemeProperty('borderColor', 'border-color')};
  ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.align, props.theme, true)};

  /* lazy to force padding but need to do this to override mui grid defaults */
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('border-color', props.theme.componentTheme.borderColor, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('border-radius', props.theme.componentTheme.borderRadius, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('order', props.theme.componentTheme.order, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('width', props.theme.componentTheme.width, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('min-width', props.theme.componentTheme.minWidth, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('max-width', props.theme.componentTheme.maxWidth, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('overflow', props.theme.componentTheme.overflow, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('min-height', props.theme.componentTheme.minHeight, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('max-height', props.theme.componentTheme.maxHeight, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('border-width', props.theme.componentTheme.borderWidth, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('flex-basis', props.theme.componentTheme.flexBasis, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('flex-shrink', props.theme.componentTheme.flexShrink, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('flex-grow', props.theme.componentTheme.flexGrow, props.theme, true)};
  ${(props: any) =>
    applyMediaQueriesForStyle('background-image', props.theme.componentTheme.backgroundImage, props.theme)};
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  transition: background 0.3s;
  ${({theme}: any) =>
    applyMediaQueriesForStyle('background-position', theme.componentTheme.backgroundImagePosition, theme)};
  ${({theme}: any) =>
    applyMediaQueriesForStyle('background-repeat', theme.componentTheme.backgroundImageRepeat, theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('background-size', theme.componentTheme.backgroundImageSize, theme)};
  &:hover {
    ${renderThemeProperty('backgroundColorHover', 'background')};
  }
` as StyledComponent<any, any>;

// prettier-ignore
export const ContainerComponentThemeWrapper = styled.div`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('background-image', props.theme.componentTheme.backgroundImage, props.theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('background-position', theme.componentTheme.backgroundImagePosition, theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('background-repeat', theme.componentTheme.backgroundImageRepeat, theme)};
  ${({theme}: any) => applyMediaQueriesForStyle('background-size', theme.componentTheme.backgroundImageSize, theme)};
  width: 100%;
  transition: background 0.3s;
` as StyledComponent<any, any>;

// every normal component will use this component as a wrapper, to ensure each component has an initial set of properties
export const CommonComponentThemeWrapper = styled.div`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('primaryTextColor', 'color')};

  transition: background 0.3s;
` as StyledComponent<any, any>;

export interface keyFunc {
  (child: React.ReactChild, index: number | string): any;
}

// ensure every node in a component hierarchy as at least an index key if none already defined at the node level
export const withKeys = (children: React.ReactNode[], keyFunc?: keyFunc): React.ReactNode[] =>
  React.Children.map(children, (child, index) => {
    if (React.isValidElement(child)) {
      if (!keyFunc) {
        keyFunc = (child: React.ReactElement<any>, index) => child.key || index;
      }
      return React.cloneElement(child as React.ReactElement<any>, {key: keyFunc(child, index)});
    }
    return child;
  });
