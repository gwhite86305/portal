import {Button, Grid} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import * as React from 'react';
import {MediaThumbnailWithSize} from '../common/MediaThumbnail/MediathumbnailWithSize.js';
import {ImageVariantsType} from '../types/index.js';
import * as defaultProps from './defaultProps.json';
import Seasons from './Seasons';
import {SeriesDetailsProps} from './types';
import {groupBySeasons} from './utils';

const SeriesDetails: React.FC<SeriesDetailsProps> = props => (
  <Grid container className="series-details-container">
    <Grid item xs={12} className="header">
      <Button aria-label="Go back" className="back-button" onClick={props.onClickBack} children={<ArrowBackIcon />} />
      <h1 className="title" children={props.item.title} />
      <div className="place-holder" children={<ArrowBackIcon />} />
    </Grid>
    <Grid item xs={12} md={7}>
      <div className="poster-container">
        {props.item.titleImages && (
          <MediaThumbnailWithSize
            imageVariants={props.item.titleImages[0]}
            variantType={ImageVariantsType.poster}
            item={props.item}
            className="poster"
            clickable={false}
          />
        )}
      </div>
    </Grid>
    <Grid item xs={12} md={5} className="details-container">
      <div className="title" children={props.item.title} />
      <Seasons
        playlistIdsMap={props.playlistIdsMap}
        playItem={props.playItem}
        addItemsToPlaylist={props.addItemsToPlaylist}
        removeItemFromPlaylist={props.removeItemFromPlaylist}
        seasons={groupBySeasons(
          props.item.items!.map(el => ({
            ...el,
            parentId: props.item.uid
          }))
        )}
      />
    </Grid>
  </Grid>
);

SeriesDetails.defaultProps = defaultProps as any;

export default SeriesDetails;
