import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {Weather as ExternalInterface} from '../common/ComponentsInterface';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import WeatherDashboard, {InternalInterface} from './WeatherDashboard';
import {getCitiesWeatherConditions, getAirportForeCast} from '../../utils/WeatherUtils';
export type CombinedProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};
export interface DispatchProps {}

@connectWrapper((state: any) => {
  let props = {};
  if (state.profile && state.device) {
    props = {
      profile: state.profile,
      device: state.device,
      allCitiesCondition: getCitiesWeatherConditions(state.device, state.profile)
    };
  }
  return props;
})
class WeatherContainer extends React.Component<CombinedProps, any> {
  constructor(props: any) {
    super(props);
    this.state = {currentCondition: undefined, grid_start: -1};
    this.onCityClick = this.onCityClick.bind(this);
    this.onUpClick = this.onUpClick.bind(this);
    this.onDownClick = this.onDownClick.bind(this);
  }

  onCityClick = (airport: string, event: any) => {
    // filter(<condition>)[0] is used instead of find(<condition>) in order to support IE 11
    let currentCondition = this.props.allCitiesCondition.filter((condition: any) => condition.airport == airport)[0];
    this.setState({currentCondition: currentCondition});
  };

  onUpClick = () => {
    if (this.state.grid_start + this.props.feature.pageSize * 2 > this.props.allCitiesCondition.length) {
      this.setState({grid_start: this.props.allCitiesCondition.length - this.props.feature.pageSize});
    } else {
      this.setState({grid_start: this.state.grid_start + this.props.feature.pageSize});
    }
  };

  onDownClick = () => {
    if (this.state.grid_start - this.props.feature.pageSize < 0) {
      this.setState({grid_start: 0});
    } else {
      this.setState({grid_start: this.state.grid_start - this.props.feature.pageSize});
    }
  };

  componentDidUpdate(prevProps: any) {
    this.setCurrentCondition();
  }
  shouldComponentUpdate(nextProps: any, nextState: any) {
    return (
      !this.state.currentCondition ||
      this.state.grid_start != nextState.grid_start ||
      !!(
        this.state.currentCondition &&
        nextState.currentCondition &&
        this.state.currentCondition.airport != nextState.currentCondition.airport
      )
    );
  }
  setCurrentCondition() {
    if (
      this.props.device &&
      this.props.device.flightDestination &&
      this.props.allCitiesCondition &&
      !this.state.currentCondition
    ) {
      let highlighted_city_index = this.props.allCitiesCondition.findIndex(
        (condition: any) => condition.airport == this.props.device.flightDestination
      );
      let grid_start = 0;
      if (highlighted_city_index - Math.ceil(this.props.feature.pageSize / 2) < 0) {
        grid_start = 0;
      } else if (
        highlighted_city_index + Math.ceil(this.props.feature.pageSize) / 2 >
        this.props.allCitiesCondition.length
      ) {
        grid_start = this.props.allCitiesCondition.length - this.props.feature.pageSize;
      } else {
        grid_start = highlighted_city_index - Math.floor(this.props.feature.pageSize / 2);
      }
      this.setState({grid_start: grid_start});
      this.setState({currentCondition: this.props.allCitiesCondition[highlighted_city_index]});
    }
  }

  componentWillMount() {
    this.setCurrentCondition();
  }

  public render() {
    let props: any = {};
    if (this.state.currentCondition) {
      let currentCondition = {...this.state.currentCondition};
      //Take temperature from device if the destination is highlighted.
      if (currentCondition.airport == this.props.device.flightDestination) {
        currentCondition.currentCondition.weatherText = this.props.device.destinationConditions;
        currentCondition.currentCondition.temperature = this.props.device.destinationTemperature;
        currentCondition.currentCondition.weatherIcon = this.props.device.destinationWeatherIcon;
      }
      props['currentCondition'] = currentCondition;
      let forecast = getAirportForeCast(this.props.device, this.props.profile, this.state.currentCondition.airport);
      if (forecast && forecast.length == 4) {
        props['dailyForecast'] = forecast;
      }
      let currentConditions =
        this.props.allCitiesCondition &&
        this.props.allCitiesCondition.slice(this.state.grid_start, this.state.grid_start + 5);
      if (currentConditions.length > 0) {
        props['currentConditions'] = currentConditions;
      }
    }

    return (
      <WeatherDashboard
        {...this.props}
        {...props}
        onCityClick={this.onCityClick}
        onUpClick={this.onUpClick}
        onDownClick={this.onDownClick}
      />
    );
  }
}

export default withLocaleProps(WeatherContainer, []) as any;
