import {capitalize} from 'lodash-es';
import {groupBy, join, map, pipe, prop, zipObj} from 'ramda';
import {SystemMessage} from 'component-utils/dist/store/system/reducer';
import {hideMessage, showMessage} from 'component-utils/dist/store/system/actions';
import {trackView, trackPlay} from '../../store/analytics/actions';
import {playItem} from '../../store/mediaplayer/actions.player';
import {addItemsToPlaylist, removeItemFromPlaylist} from '../../store/mediaplayer/actions.playlist';
import {getVideoById} from '../../store/videos/actions';
import {IRootState} from './../../index';
import {TYPES, VideoGroup} from './../../store/videos/types';
import {DispatchProps, StoreProps} from './types';
import {selectPlayListItems, selectPlaylistLoading} from '../../store/mediaplayer/selector';

export const mapStoreToProps = (state: IRootState): StoreProps => ({
  playlistIdsMap: pipe(
    map(prop('uid')),
    (list: any) => zipObj(list, list)
  )(selectPlayListItems(state)) as any,
  videoItems: [...state.ifeVideos.items.filter(item => item.type === TYPES.VIDEO_GROUP)] as VideoGroup[],
  videoNotFound: state.ifeVideos.videoNotFound,
  selectedItem: state.ifeVideos.selectedItem as VideoGroup,
  playlistLoading: selectPlaylistLoading(state.ifeMediaPlayer)
});
export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    addItemsToPlaylist: items => dispatch(addItemsToPlaylist(items)),
    getVideoById: uid => dispatch(getVideoById(uid)),
    removeItemFromPlaylist: uid => dispatch(removeItemFromPlaylist(uid)),
    playItem: (item, playerPage) => dispatch(playItem(item, playerPage)),
    trackView: item => dispatch(trackView(item)),
    trackPlay: item => dispatch(trackPlay(item)),
    showMessage: (message: SystemMessage) => dispatch(showMessage(message, 0)),
    hideMessage: () => dispatch(hideMessage())
  }
});

export const capitaliseAndJoin = pipe(
  value => (Array.isArray(value) ? value : [value]),
  map(capitalize),
  join(', ')
);
export const imgPlaceholder = 'http://via.placeholder.com/' + 600 + 'pxx' + 420 + 'px';
export const groupBySeasons = groupBy(prop('season')) as (episodes: any[]) => any;
