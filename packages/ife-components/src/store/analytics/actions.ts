import {ActionCreator} from 'component-utils/dist/store/action-creator';
import storeAccess from 'component-utils/dist/utils/storeAccess';
import {length} from 'ramda';
import {Subject} from 'rxjs';
import {bufferTime, filter, map, tap} from 'rxjs/operators';
import {IRootState} from '../..';
import {AdvertItem} from '../adverts/types';
import {AudioGroup, AudioItem} from '../audio/types';
import {selectCurrentItem} from '../mediaplayer/selector';
import {MovieItem} from '../movies/types';
import {VideoGroup, VideoItem} from '../videos/types';
import {buildContentUsageMeta, buildPlayerEventMeta} from './utils';
import {IAnalyticsActionMeta} from 'component-utils';

export enum AnalyticEventType {
  PLAY = 'play',
  PLAYING = 'playing',
  PAUSE = 'pause',
  BUFFERING = 'buffering',
  STOP = 'stop',
  ENDED = 'ended',
  FAILED = 'failed'
}

export interface PlayerEvent {
  eventType: AnalyticEventType;
  itemId: string;
  itemType: 'videoItem' | 'audioItem';
}

export const ActionCreators = {
  trackImpression: new ActionCreator<'ANALYTICS/TRACK_IMPRESSION', any>('ANALYTICS/TRACK_IMPRESSION'),
  trackView: new ActionCreator<'ANALYTICS/TRACK_VIEW', any>('ANALYTICS/TRACK_VIEW'),
  reportEvent: new ActionCreator<'ANALYTICS/REPORT_EVENT', {}, IAnalyticsActionMeta>('ANALYTICS/REPORT_EVENT')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

const impressionSource$ = new Subject();
impressionSource$
  .asObservable()
  .pipe(
    bufferTime(1000),
    filter(length as any),
    map((impressions: any[]) => buildContentUsageMeta('imp', impressions)),
    tap((meta: any) => storeAccess.get().dispatch(ActionCreators.trackImpression.create(undefined, meta)))
  )
  .subscribe();

export function trackImpression(item: AudioGroup | VideoItem | MovieItem | VideoGroup | AdvertItem) {
  return () => impressionSource$.next(item);
}

export function trackView(item: AudioGroup | VideoItem | VideoGroup | AdvertItem | AudioItem) {
  return (dispatch: any) => dispatch(ActionCreators.trackView.create(undefined, buildContentUsageMeta('view', [item])));
}

export function trackPlay(item: VideoItem | VideoGroup | AdvertItem | AudioItem) {
  return (dispatch: any) => dispatch(ActionCreators.trackView.create(undefined, buildContentUsageMeta('play', [item])));
}

export interface EventPayload {
  error?: {message: string; code: number};
  eventPlayback?: {duration: number; currentTime: number; playingTime: number};
  playbackSessionId: string;
}

export const reportEvent = (eventType: AnalyticEventType, payload: EventPayload) => {
  return (dispatch: any, getState: () => IRootState) => {
    const state = getState();

    const item = selectCurrentItem(state)!;
    const playbackState = (payload && payload.eventPlayback) || {duration: 0, currentTime: 0, playingTime: 0};

    const meta = buildPlayerEventMeta({
      eventType,
      item,
      playbackState,
      playbackSessionId: payload.playbackSessionId,
      error: payload && payload.error
    });

    dispatch(ActionCreators.reportEvent.create({}, meta));
  };
};
