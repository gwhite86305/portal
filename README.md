# portal

Lerna packages for Portal components (and renderer)

Useful Lerna info: https://github.com/lerna/lerna https://www.nearform.com/blog/sharing-react-components-with-lerna/
https://hackernoon.com/the-highs-and-lows-of-using-lerna-to-manage-your-javascript-projects-ff5c5cd82a99

## Requirements

- NodeJS == 8.11.3

```
nvm install 8.11.3
nvm use 8.11.3
```

- Global packages: npm, lerna (exact versions)

```
npm install -g npm@5.6.0
npm install -g lerna@2.9.0
```

## Development Setup

Clone Repository

```sh
git clone git@git.viasat.com:CMOB/portal.git
```

Install dependencies:

```sh
npm run bootstrap
```

Initialize submodules:

```sh
git submodule init
git submodule update --remote
```

Install git hooks:

```sh
make install-hooks
```

Source the appropriate environment:

```
source env/dev.<airline>.env
```

Build the component libraries:

```sh
npm run build
```

After building, you can either run the server standalone, or deploy to an integration environment.

### Running Standalone

Start the web dev server from the root folder to run at [http://localhost:3001/](http://localhost:3001/):

```sh
npm run start:<airline>
```

To run the server in release mode, set the environment variable `NODE_ENV=production`

### Deploying to Integration

First, ensure portal-integration (or equivalent) is running on your machine.

Then, change directories to the portal-renderer packages, and run a deploy command:

```
cd packages/portal-renderer/
PROFILE_PATH=./mocks/api/layout/<airline>/GET.json npm run deploy:pfe
```

## Make

Build

```sh
$ make build
```

Test

```sh
$ make test
```

Development Stack

```sh
$ make development env=development
```

```sh
$ make services env=development
```

```sh
$ make schema-service env=development
```

```sh
$ make renderer-service env=development
```

## Release

Generate a release build in `portal-renderer/dist` - in a production env, the /dist folder contents can sit behind an
nginx/apache server (with reverse proxy & configured for single page url handling).

```sh
$ npm run build
```

_portal-components_ lib is built into the `portal-components/dist` folder, its contents will eventually end up on any
artifactory npm repo for use as an npm dependency when it matures sufficiently as a project.

Note: TypeScript compiler processes **\*.ts** to es5 - you may need to do a webpack babel transform on any using client
(depending on its usage)

`dist` folder contents should be checked in\* if you require distribution in lieu of artifactory publish.

You can also generate a PFE-ready build by running:

```sh
npm run build:pfe
```

and deploy to a local portal integration environment by running:

```sh
npm run deploy:pfe
```

or deploy customer-specific versions with any of these commands:

```sh
npm run deploy:demofree
npm run deploy:demopaid
npm run deploy:neos
npm run deploy:iceland
npm run deploy:aa
```

You can also have the Jenkins develop build automatically create a build of a client portal and publish it to
artifactory by adding a `publish-*` target to the Makefile, then adding that as a dependency of the `publish-all`
target.

NOTE: to make a publish or deploy command pull assets from the PB API, just add
`ASSET_BASE_URL="http://pb-preprod.naw01.rbodev.viasat.io/api/"` before PROFILE_PATH (no backticks)

## Testing

To run screen comparison testing:

```sh
$ make build && make test
```

Please follow the [link](https://git.viasat.com/CMOB/portal/blob/develop/packages/portal-renderer/test/README.md).
