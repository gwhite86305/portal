import {css} from 'styled-components';
import isEmpty from 'lodash-es/isEmpty';
import isBoolean from 'lodash-es/isBoolean';
import {ScreenSize, ScreenSizes} from '../types/ScreenSizes';

// https://material-ui.com/layout/breakpoints
const screenSizes: ScreenSizes = {
  xs: 360,
  sm: 600,
  md: 960,
  lg: 1280,
  xl: 1920
};

// Iterate through the sizes and create a media template
const screenSizeQuery = Object.keys(screenSizes).reduce((acc: any, label: ScreenSize) => {
  const minWidthSize = label === ScreenSize.xs ? 0 : screenSizes[label];
  acc[label] = (v1: any, v2: any) => css`
    @media (min-width: ${minWidthSize}px) {
      ${css(v1, v2)};
    }
  `;
  return acc;
}, {});

export const applyMediaQueriesForStyle = (
  styleProperty: string,
  multiScreenSettings: any,
  appTheme: any,
  force?: any
) => {
  // ordering of generation of css for screen sizes items must be in order of size
  if (multiScreenSettings) {
    return Object.keys(screenSizes).map((screenSize: ScreenSize) => {
      const screenSizeProp = screenSizeQuery[screenSize];
      if (screenSizeProp) {
        let styleValue = multiScreenSettings[screenSize];

        if (!isBoolean(styleValue) && isEmpty(styleValue) && typeof styleValue !== 'function') {
          return '';
        }

        // handle where default belongs to the app theme
        if (typeof styleValue === 'function') {
          styleValue = styleValue(appTheme);
        } else if (typeof styleValue === 'string' && styleValue.indexOf('theme.') !== -1) {
          styleValue = appTheme[styleValue.split('.')[1]];
        }

        if (styleProperty === 'background-image') {
          const bgToString = `${styleProperty}: url('${styleValue}');`;
          return screenSizeProp`${bgToString}`;
        } else {
          if (force) {
            styleValue = styleValue + '!important';
          }
          return screenSizeProp`${styleProperty + ':' + styleValue};`;
        }
      } else {
        return '';
      }
    });
  } else {
    return '';
  }
};

// given a component theme attribute: apply its *value* to an actual *css setting*
// note: app theme => componentTheme already merged at this stage
export const renderThemeProperty = (themePropName: string, styleName: string, force?: boolean) => {
  return (props: any) => {
    let styleValue = props.theme.componentTheme[themePropName];

    if (!isBoolean(styleValue) && isEmpty(styleValue) && typeof styleValue !== 'function') {
      return '';
    }

    // handle where default belongs to the app theme
    if (typeof styleValue === 'function') {
      styleValue = styleValue(props.theme);
    } else if (typeof styleValue === 'string' && styleValue.indexOf('theme.') !== -1) {
      styleValue = props.theme[styleValue.split('.')[1]];
    }

    if (props.theme.componentTheme[themePropName]) {
      return styleName === 'background-image'
        ? styleName + ": url('" + styleValue + "');"
        : styleName + ':' + styleValue + (force ? '!important;' : ';');
    } else {
      return '';
    }
  };
};
