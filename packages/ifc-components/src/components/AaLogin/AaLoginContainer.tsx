import * as React from 'react';
import {utils} from 'component-utils';
import {AaLogin as ExternalInterface} from '../common/ComponentsInterface';
import AaLogin, {InternalInterface} from './AaLogin';
import {localizeToProperties} from 'component-utils/dist/components/Localize';
import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
import {pathOr, pathEq} from 'ramda';
const connectWrapper = utils.connectWrapper.default;
import {americanLoyaltyMemberLogin, clearMessageAA} from '../../store/Authentication/actions';
import {ShowModal} from 'core-components/dist/store/Modal/actions';
export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onAALoginSubmit(authUrl: string, subscriptionUrl: string, userName: string, lastName: string, password: string): any;
  onGuestLoginSubmit(): any;
  resetLoginForm(aaAuth: any): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    let props = {...ownProps, successCode: 8110001};
    props.Authentication = state.Authentication;
    props.WifiServiceOptions = state.WifiServiceOptions;
    return props;
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onAALoginSubmit: (authUrl, username, lastName, password) => {
        dispatch(americanLoyaltyMemberLogin(authUrl, username, lastName, password, ownProps.feature.bypassModalId));
      },
      onGuestLoginSubmit: () => {
        dispatch(ShowModal(ownProps.feature.openModalOnGuestLoginClick));
      },
      resetLoginForm: aaAuth => {
        dispatch(clearMessageAA(aaAuth));
      }
    };
  }
)
class AaLoginContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }
  componentWillReceiveProps(newProps: any) {
    let {Authentication} = newProps;
    if (Authentication && Authentication.aaAuth && Authentication.aaAuth.code == this.props.successCode) {
      this.props.history.push('/' + this.props.feature.openPageWhenLogin);
    }
  }
  public componentWillUnmount() {
    this.props.resetLoginForm(this.props.Authentication.aaAuth);
  }
  public render() {
    const {intl, componentUid} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {header: 'ifc.AaLogin.header'},
      {headerForPurchasePath: 'ifc.AaLogin.headerForPurchasePath'},
      {userNameInputLabel: 'ifc.AaLogin.userNameInputLabel'},
      {lastNameInputLabel: 'ifc.AaLogin.lastNameInputLabel'},
      {passwordInputLabel: 'ifc.AaLogin.passwordInputLabel'},
      {rememberMeCheckBoxLabel: 'ifc.AaLogin.rememberMeCheckBoxLabel'},
      {forgotPasswordLinkText: 'ifc.AaLogin.forgotPasswordLinkText'},
      {loginButtonText: 'ifc.AaLogin.loginButtonText'},
      {guestLoginButtonText: 'ifc.AaLogin.guestLoginButtonText'},

      {errorMessageOnFormHeader: 'ifc.AaLogin.errorMessageOnFormHeader'},
      {errorMessageGeneric: 'ifc.AaLogin.errorMessageGeneric'},
      {errorMessageUserNameRequired: 'ifc.AaLogin.errorMessageUserNameRequired'},
      {errorMessageLastNameRequired: 'ifc.AaLogin.errorMessageLastNameRequired'},
      {errorMessagePasswordRequired: 'ifc.AaLogin.errorMessagePasswordRequired'},
      {errorMessageInvalidCredential: 'ifc.AaLogin.errorMessageInvalidCredential'}
    ]);
    let errorDict = {
      '8000071': localizedProps.errorMessageInvalidCredential,
      '8004001': localizedProps.errorMessageInvalidCredential,
      errorMessageUserNameRequired: localizedProps.errorMessageUserNameRequired,
      errorMessageLastNameRequired: localizedProps.errorMessageLastNameRequired,
      errorMessagePasswordRequired: localizedProps.errorMessagePasswordRequired,
      errorMessageOnFormHeader: localizedProps.errorMessageOnFormHeader,
      generic: localizedProps.errorMessageGeneric
    };
    const purchasePath = pathEq(['selected', 'method'], 'purchase', this.props.WifiServiceOptions);
    return <AaLogin {...this.props} errorDict={errorDict} {...localizedProps} purchasePath={purchasePath} />;
  }
}

export default injectIntl(withRouter(AaLoginContainer));
