import * as PropTypes from 'prop-types';
import * as React from 'react';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {withStyles} from '@material-ui/core/styles';

// -- ListButton
export const ListButtonContainer = styled.div`
  position: relative;
  display: flex;

  .list-button {
    min-width: auto;
    min-height: auto;
    border-radius: 50%;
    padding: 1rem;
    svg {
      ${renderThemeProperty('playlistIconColor', 'fill')};
    }
  }

  .list-button-counter {
    background-color: red;
    width: 20px;
    height: 20px;
    text-align: center;
    border-radius: 10px;
    color: white;
    position: absolute;
    top: -5px;
    left: -5px;
  }
`;

export const StyledItemCount = styled.div`
  ${renderThemeProperty('itemCountBadgeBackgroundColor', 'background')};
  ${renderThemeProperty('itemCountBadgeTextColor', 'color')};
  height: 16px;
  width: 16px;
  font-size: 9px;
  border-radius: 40px;
  position: relative;
  left: 42px;
  top: 4px;
  z-index: 1;
  box-shadow: 2px 2px 10px 0px black;
  font-weight: 600;
  text-align: center;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

// -- Modal Body
export const StyledContent = styled.div`
  ${renderThemeProperty('dialogBackgroundColor', 'background')};
  ${renderThemeProperty('primaryTextColor', 'color')};
  ${renderThemeProperty('textSize', 'font-size')};
  ${renderThemeProperty('modalRadius', 'border-radius')};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalPadding, props.theme)};

  .close-button {
    ${renderThemeProperty('primaryTextColor', 'color')};
    min-width: auto;
    min-height: auto;
    border-radius: 50%;
    padding: 1rem;
  }

  .cta {
    ${renderThemeProperty('buttonFontSize', 'font-size')};
    ${renderThemeProperty('buttonTextColor', 'color')};
    cursor: pointer;
    padding: 1rem 2rem;
    text-transform: none;
  }

  .clear {
    ${renderThemeProperty('clearButtonColor', 'background')};
  }
  .close {
    ${renderThemeProperty('closeButtonColor', 'background')};
  }

  display: flex;
  flex-direction: column;
  .modal-header,
  .modal-footer {
    display: flex;
    justify-content: space-between;
  }

  .modal-header svg {
    cursor: pointer;
  }

  .modal-header {
    align-content: center;
    padding: 1rem 0;
    margin: 0 1rem;
    ${renderThemeProperty('headerSeparator', 'border-bottom')};

    .title {
      display: flex;
      align-items: center;
    }
  }
  .modal-footer {
    padding: 1rem;
  }

  .modal-content {
    margin: 1rem 0;
    max-height: 350px;
    overflow: auto;

    > * {
      ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.itemsPadding, props.theme)};
      display: flex;
      align-items: center;
      min-height: 20px;
      margin-bottom: 1px;
    }
  }
`;

// -- Responsive
const styles = (theme: any) => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    height: '100px',
    width: '100px',
    transform: 'translate(-50%, -50%)',
    display: 'table',
    [theme.breakpoints.up('md')]: {width: '650px'},
    [theme.breakpoints.down('sm')]: {width: '90%'},
    'box-shadow': '0 5px 15px rgba(0,0,0,0.5)'
  }
});
const ResponsiveDiv = (props: any) => (
  <div tabIndex={-1} className={props.classes.root}>
    {props.children}
  </div>
);
(ResponsiveDiv as any).propTypes = {
  classes: PropTypes.object.isRequired
};
export const ResponsiveContainer = withStyles(styles as any)(ResponsiveDiv);

// Playlist List Item
export const ListItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  .play-pause {
    display: flex;
    flex: 1;
    justify-content: space-between;
    align-items: center;
  }

  .play-pause:hover .play-pause-button,
  .show {
    opacity: 1 !important;
    transition: 0.2s;
  }

  .img-container {
    cursor: pointer;
    background: black;
    height: 42px;
    width: 42px;
    position: relative;
    .playlist-image {
      width: 100%;
      height: 100%;
      background-size: contain;
      background-repeat: no-repeat;
      background-position: center center;
    }
    .play-pause-button {
      width: 100%;
      height: 100%;
      position: absolute;
      opacity: 0;
      top: 0;
      svg {
        background: rgba(0, 0, 0, 0.5);
        height: 42px;
        width: 42px;
      }
    }
  }

  .primary-info,
  .title {
    margin-left: 3px;
    overflow: hidden;
    text-overflow: ellipsis;
    ${renderThemeProperty('entryTextTopColor', 'color')};
    ${renderThemeProperty('entryTextTopSize', 'font-size')};
  }

  .description-body {
    cursor: pointer;
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: start;
    ${renderThemeProperty('entryRightBorderSeparator', 'border-right')};
  }

  .description-type-artist,
  .secondary-info {
    ${renderThemeProperty('entryTextBottomColor', 'color')};
    ${renderThemeProperty('entryTextBottomSize', 'font-size')};
    margin-left: 3px;
    display: flex;
    align-items: center;
    svg {
      width: 14px;
      margin-right: 0.3rem;
    }

    .artist {
      margin-left: 3px;
    }
  }

  .close-button {
    cursor: pointer;
    ${_ => applyMediaQueriesForStyle('margin-left', {xs: '0', sm: '1rem'}, {})};
    margin-left: 1rem;
    > span > svg {
      font-size: 1.4rem;
    }
  }
`;
