import styled from 'styled-components';

export default styled.div`
  min-height: 100%;
  height: 100%;
`;
