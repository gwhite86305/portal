const pixelmatch = require('pixelmatch');
const PNG = require('pngjs').PNG;
const fs = require('fs');

function delay(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

function compareScreenshots(fileName) {
  return new Promise((resolve, reject) => {
    const img1 = fs
      .createReadStream(`${fileName}.png`)
      .pipe(new PNG())
      .on('parsed', doneReading);
    const img2 = fs
      .createReadStream(`${fileName}_.png`)
      .pipe(new PNG())
      .on('parsed', doneReading);

    let filesRead = 0;
    function doneReading() {
      // Wait until both files are read.
      if (++filesRead < 2) return;
      const diff = new PNG({width: img1.width, height: img2.height});
      const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, img1.width, img1.height, {threshold: 0.5});
      if (numDiffPixels != 0) {
        const buff = PNG.sync.write(diff);
        fs.writeSync(fs.openSync(`${fileName}_diff.png`, 'w'), buff);
        //diff.pack().pipe(fs.createWriteStream(`${fileName}_diff.png`));
      } else {
        if (fs.existsSync(`${fileName}_diff.png`)) {
          fs.unlinkSync(`${fileName}_diff.png`);
        }
      }
      fs.unlinkSync(`${fileName}_.png`);
      const result = {fileName, numDiffPixels};
      resolve(result);
    }
  });
}

async function takeAndCompareScreenshot(page, fileName) {
  if (fs.existsSync(`${fileName}.png`)) {
    await page.screenshot({path: `${fileName}_.png`, fullPage: true});
    const result = compareScreenshots(fileName);
    return result;
  } else {
    await page.screenshot({path: `${fileName}.png`, fullPage: true});
    return {fileName, numDiffPixels: 0};
  }
}

async function screenshot_test(page, name) {
  console.log('Perform screenshot on:', name);
  const result = await takeAndCompareScreenshot(page, name);
  await delay(3000);
  return result;
}

function handleFailures(customerName, testName, results) {
  const failures = results.filter(result => result.numDiffPixels !== 0);
  if (failures.length) {
    let message = `${customerName} ${testName} FAILED: ${failures.length} failures:`;
    failures.forEach(
      result => (message = `${message}\n  * ${result.fileName}.png failed: ${result.numDiffPixels} pixels different`)
    );
    console.error(message);
  }
  expect(failures.length).toBe(0);
}

module.exports.screenshot_test = screenshot_test;
module.exports.delay = delay;
module.exports.handleFailures = handleFailures;
