import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {ServiceDisruption as ExternalInterface} from '../common/ComponentsInterface';
import ServiceDisruption, {InternalInterface} from './ServiceDisruption';
import {withLocaleProps} from 'component-utils/dist/components/Localize';

export type PageProps = ExternalInterface & InternalInterface;

@connectWrapper((state: any) => {
  const stateProps = {
    builderSelectedComponentUid: state.ux.builderSelectedComponentUid,
    isIFCDown: false
  };

  if (state.device) {
    stateProps.isIFCDown = !state.device.isServiceAvailable || state.device.alertId !== '30101';
  }

  return stateProps;
})
class ServiceDisruptionContainer extends React.Component<PageProps, any> {
  public render() {
    const {builderSelectedComponentUid, componentUid, isIFCDown} = this.props;
    const isBuilderSelected = (builderSelectedComponentUid === componentUid);
    if (isBuilderSelected || isIFCDown) {
      return <ServiceDisruption {...this.props} isIFCDown={isBuilderSelected || isIFCDown}/>;
    } else {
      return null;
    }
  }
}

export default withLocaleProps(ServiceDisruptionContainer, [
  {localizedHeaderText: 'ifc.ServiceDisruption.header.text'},
  {localizedAlertText: 'ifc.ServiceDisruption.disruptionalert.text'}
]) as any;
