import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {FlightTimeRemaining as ExternalInterface} from '../common/ComponentsInterface';
import FlightTimeRemaining, {InternalInterface} from './FlightTimeRemaining';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {getFlightRemaining} from '../../utils/FlightStatusUtils';
import FlightTimeRemainingFlex from './FlightTimeRemainingFlex';
export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {}

@connectWrapper((state: any) => {
  if (state.device) {
    let {minutesRemaining, percentageOfFlightComplete} = getFlightRemaining(state.device, state.profile);
    return {
      ...state.device,
      flightMinutesRemaining:
        state.device.wowState == 'On' ? undefined : minutesRemaining > 1439 ? 1439 : minutesRemaining,
      flightOrigin: state.profile && state.profile.locations && state.profile.locations.originCode,
      flightDestination: state.profile && state.profile.locations && state.profile.locations.destinationCode,
      percentageOfFlightComplete: percentageOfFlightComplete
    };
  }
})
class FlightTimeRemainingContainer extends React.Component<PageProps, any> {
  public render() {
    return this.props.feature.flexWidget ? (
      <FlightTimeRemainingFlex {...this.props} />
    ) : (
      <FlightTimeRemaining {...this.props} />
    );
  }
}

export default withLocaleProps(FlightTimeRemainingContainer, [
  {localizedTitle: 'ifc.FlightTimeRemaining.title.text'}
]) as any;
