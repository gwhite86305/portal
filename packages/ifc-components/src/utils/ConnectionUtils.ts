import Device from '../types/Device';
import Profile from '../types/Profile';

const _deviceIsGood = (device: any) => {
  return (
    !!device &&
    (device.hasOwnProperty('isServiceAvailable') && typeof device.isServiceAvailable === 'boolean') &&
    (device.hasOwnProperty('alertId') && (typeof device.alertId === 'number' || typeof device.alertId === 'string')) &&
    (device.hasOwnProperty('serviceTierId') &&
      (typeof device.serviceTierId === 'number' || typeof device.serviceTierId === 'string'))
  );
};

const _profileIsGood = (profile: any) => {
  return !!profile && (profile.hasOwnProperty('serviceTiers') && profile.serviceTiers && profile.serviceTiers.length);
};

export const getIfcInitializedStatus = (device: any, profile: any) => {
  return _deviceIsGood(device) && _profileIsGood(profile);
};

export const getDefaultTier = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const defaultTier = profile.serviceTiers.filter((tier: any) => tier.npiCodeId && ~tier.name.indexOf('Default'))[0];
  return defaultTier;
};
export const getFreeTier = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const freeTier = profile.serviceTiers.filter(
    (tier: any) => tier.npiCodeId && (~tier.name.indexOf('Free') || ~tier.name.indexOf('Beta'))
  )[0];
  return freeTier;
};

export const getMessagingTier = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const messagingTier = profile.serviceTiers.filter((tier: any) => ~tier.npiCodeId.toLowerCase().indexOf('messaging'));
  return messagingTier ? messagingTier : [];
};

export const getPremiumTiers = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const premiumTiers = profile.serviceTiers.filter(
    (tier: any) =>
      tier.cost > 0 ||
      (tier.npiCodeId &&
        (~tier.npiCodeId.indexOf('Premium') || ~tier.npiCodeId.indexOf('Basic') || ~tier.npiCodeId.indexOf('Free')))
  );
  return premiumTiers;
};
export const getServiceAvailabilityState = (device: any) => {
  if (!_deviceIsGood(device)) {
    return undefined;
  }
  return device.isServiceAvailable && device.alertId === '30101';
};
export const deviceHasServiceTier = (device: any) => {
  if (!_deviceIsGood(device)) {
    return undefined;
  }
  return device.serviceTierId != 0;
};
export const getPremiumState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const premiumTiers = getPremiumTiers(profile);
  const connected =
    premiumTiers &&
    Object.keys(premiumTiers).length > 0 &&
    Boolean(premiumTiers.filter((tier: any) => tier.id === device.serviceTierId)[0]);
  return connected;
};
export const getBypassTiers = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  return profile.bypassServiceTiers;
};
export const getGogoTiers = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const gogoTier = profile.serviceTiers.filter((tier: any) => tier.npiCodeId && ~tier.name.toLowerCase().indexOf('gogo'));
  return gogoTier;
};
export const getBoingoTiers = (profile: any) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const boingoTier = profile.serviceTiers.filter((tier: any) => tier.npiCodeId && ~tier.name.toLowerCase().indexOf('boingo'));
  return boingoTier;
};
export const getDefaultState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const defaultTier = getDefaultTier(profile);
  const connected = defaultTier && Object.keys(defaultTier).length > 0 && device.serviceTierId === defaultTier.id;
  return connected;
};
export const getFullFlightState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const defaultTier = getDefaultTier(profile);
  const fullFlightService =
    defaultTier &&
    Object.keys(defaultTier).length > 0 &&
    device.serviceTierId != defaultTier.id &&
    device.serviceTimeRemaining / 60 > device.flightMinutesRemaining;
  return fullFlightService;
};
export const checkKeepLoading = (device: any, wifiServiceOptions: any, userAgreement: any) => {
  let keepLoading =
    (wifiServiceOptions.free && wifiServiceOptions.free.waitingForResponse) ||
    (wifiServiceOptions.free &&
      wifiServiceOptions.free.serviceTierId &&
      wifiServiceOptions.free.serviceTierId != device.serviceTierId) ||
    (userAgreement.signup && userAgreement.signup.waitingForResponse);

  if (wifiServiceOptions.free && wifiServiceOptions.free.error && !wifiServiceOptions.free.waitingForResponse) {
    keepLoading = false;
  }
  return keepLoading;
};

export const getTierByNpiCode = (profile: any, npiCode: string) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const tier = profile.serviceTiers.filter((tier: any) => tier.npiCodeId && ~tier.npiCodeId.indexOf(npiCode))[0];
  return tier;
};

export const getTierById = (profile: any, id: number) => {
  if (!_profileIsGood(profile)) {
    return undefined;
  }
  const tier = profile.serviceTiers.filter((tier: any) => tier.id && tier.id === id)[0];
  return tier;
};

export const getMessagingState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const messagingTier = getMessagingTier(profile);
  const connected =
    messagingTier &&
    Object.keys(messagingTier).length > 0 &&
    Boolean(messagingTier.find((tier: any) => tier.id === device.serviceTierId));
  return connected;
};
export const getBypassState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const bypassTiers = getBypassTiers(profile);
  const hasBypass =
    bypassTiers &&
    Object.keys(bypassTiers).length > 0 &&
    Boolean(bypassTiers.find((tier: any) => tier.id === device.serviceTierId));
  return hasBypass;
};

export const getThirdPartyVendorState = (device: any, profile: any) => {
  if (!_deviceIsGood(device) || !_profileIsGood(profile)) {
    return undefined;
  }
  const gogoTiers = getGogoTiers(profile) || [];
  const boingoTiers = getBoingoTiers(profile) || [];
  const thirdPartyTiers = gogoTiers.concat(boingoTiers);
  const hasThirdPartyTiers =
        thirdPartyTiers &&
        Object.keys(thirdPartyTiers).length > 0 &&
        Boolean(thirdPartyTiers.find((tier: any) => tier.id === device.serviceTierId));
  return hasThirdPartyTiers;
};
