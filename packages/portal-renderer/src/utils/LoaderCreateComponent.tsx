import * as React from 'react';
import merge from 'lodash-es/merge';
import {loadedComponents} from './ComponentLoader';
import {Component, ComponentProperties} from 'component-utils/dist/types/Component';
import {CONTAINER_PREFIX, TEMPLATE_CONTENT_UID} from 'component-utils/dist/types/Constants';
import SystemMountedComponents from 'component-utils/dist/types/SystemMountedComponents';
import {Components} from 'component-utils/dist/types/LayoutInterface';
import {ComponentContext} from 'component-utils/dist/utils/Contexts';

export const createComponent: (...args: any[]) => React.ReactNode = (
  componentUid: string,
  parentComponentChildrenProperties: ComponentProperties,
  layoutComponents: Components,
  templateBodyComponent: React.ReactNode,
  creationProperties = {},
  renderSystemMounted: boolean = false
) => {
  const componentDefinition: Component = layoutComponents[componentUid];
  let reactNodeComponent: React.ReactNode = null;

  if (
    componentDefinition &&
    !renderSystemMounted &&
    SystemMountedComponents.indexOf(componentDefinition.componentName) !== -1
  ) {
    return null;
  } else if (componentUid === TEMPLATE_CONTENT_UID) {
    return templateBodyComponent;
  } else if (componentDefinition) {
    // always look for container component first
    let actualComponentClass = loadedComponents[componentDefinition.componentName];
    let componentClass = loadedComponents[componentDefinition.componentName + CONTAINER_PREFIX] || actualComponentClass;

    if (componentClass) {
      // default props need to be explicitly passed in as will not be available to container component otherwise
      const componentDefaultProps = actualComponentClass.defaultProps || {};
      const componentParentDefaultProps = parentComponentChildrenProperties || {};
      const componentLayoutProps = {...componentDefinition.properties};

      if (!actualComponentClass.defaultProps) {
        console.error('Missing default props for ', componentDefinition.componentName);
      }

      // ensure defaults exist
      componentDefaultProps.feature = componentDefaultProps.feature || {};
      componentLayoutProps.feature = componentLayoutProps.feature || {};
      componentDefaultProps.componentTheme = componentDefaultProps.componentTheme || {};
      componentLayoutProps.componentTheme = componentLayoutProps.componentTheme || {};
      componentDefaultProps.childrenProperties = componentDefaultProps.childrenProperties || {};

      // resolve children component uids to real components
      if (componentLayoutProps.children) {
        componentLayoutProps.children = componentLayoutProps.children.map((componentUid: string, index: number) => {
          return createComponent(
            componentUid,
            componentDefaultProps.childrenProperties[index],
            layoutComponents,
            templateBodyComponent
          );
        }) as string[];
      }

      // deep merge defaults and layout definitions
      const mergedProps = {};
      merge(mergedProps, componentDefaultProps, componentParentDefaultProps, componentLayoutProps, creationProperties, {
        componentUid: componentUid,
        key: componentUid
      });

      // React only accepts strings or functions as react elements
      // If this activates, the page is beyond salvation
      if (!~['function', 'string', 'object'].indexOf(typeof componentClass)) {
        console.error(`Missing component class for ${componentUid}`, typeof componentClass, componentClass);
      }

      // TODO: Upgrade to latest react-redux (v7) for hooks etc is held back due to below React.createElement.
      // On a store state change "connected" components will always destroy & remount (unlike normal non-connected components which behave as expected).
      // This occurs regardless of context functionality from ContextProvider below. Needs R&D to determine why the memo component returned by Redux.connect forces a full tree remount (already lot of R&D done with no conclusive answer). So for now redux 5 is being used (redux 6 is poorer performance so best avoided).
      reactNodeComponent = React.createElement(componentClass, mergedProps);
      return (
        <ContextProvider componentUid={componentUid} key={'component-context-outer-' + componentUid}>
          {reactNodeComponent}
        </ContextProvider>
      );
    }
  }

  return reactNodeComponent;
};

// FYI needs to be a seperate provider component in order to prevent full remount of hierarchy on redux state change
// TODO ContextProvider is expensive (new context functionality is not as peformant as oldskool context functionality when used in a nested way as below - future task required to remove the need for context based approach when creating components to pass componentUid)
class ContextProvider extends React.Component<{componentUid: string}> {
  render() {
    return (
      <ComponentContext.Consumer>
        {() => (
          <ComponentContext.Provider value={this.props.componentUid}>{this.props.children}</ComponentContext.Provider>
        )}
      </ComponentContext.Consumer>
    );
  }
}
