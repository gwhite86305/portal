import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import * as React from 'react';
import {MEDIA_PLAYER_STATUS} from '../../../store/mediaplayer/types';
import {CentralPlayButtonProps} from './types';

const CentralPlayButton: React.FC<CentralPlayButtonProps> = (props: CentralPlayButtonProps) => (
  <div
    className={`centered-play-button ${props.status === MEDIA_PLAYER_STATUS.PLAYING && 'hidden'}`}
    children={<PlayArrowIcon onClick={props.onPlay} />}
  />
);

export default CentralPlayButton;
