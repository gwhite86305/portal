import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {axiosWithBaseUrl as axios} from '../../utils/utils';
import Norm, {Normalized} from '../../utils/Norm';
import {IRootState} from '../..';
import {getAgeRestrictionCode} from '../settings/selectors';
import {flattenCategories, extractCategories} from '../../utils/categories';

export const getMoviesStart = new ActionCreator<'getMoviesStart', any>('getMoviesStart');
export const getMoviesError = new ActionCreator<'getMoviesError', any>('getMoviesError');
export const getMoviesSuccess = new ActionCreator<'getMoviesSuccess', {items: any; categories: any}>(
  'getMoviesSuccess'
);

const fetchItems = async (path: string, queryParams?: object) => {
  const {data: {items}} = await axios.get(path, queryParams || {});
  return items || [];
};

const fetchData = async (path: string, queryParams?: object) => {
  const {data} = await axios.get(path, queryParams || {});
  return data || {};
};

export const getMovies = () => {
  return async (dispatch: any, getState: () => IRootState) => {
    dispatch(getMoviesStart.create({}));
    try {
      const ageRating = getAgeRestrictionCode(getState().ifeSettings);
      const [allCategories, movieItems] = await Promise.all([
        fetchData('/content/videos/categories'),
        fetchItems('/content/videos', {ageRating, categoryUid: 'movies'})
      ]);

      const categories = flattenCategories(extractCategories(allCategories, 'movies'));
      dispatch(getMoviesSuccess.create({items: Norm.toNormalized(movieItems), categories: Norm.toNormalized(categories)}));
    } catch (e) {
      dispatch(getMoviesError.create({}));
    }
  };
};
