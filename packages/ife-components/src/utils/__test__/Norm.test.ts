import Norm from '../Norm';

const emptyNormalized = {
  byId: {},
  allIds: []
};

const normalized1 = {
  byId: {'1': {uid: '1'}},
  allIds: ['1']
};

const normalized2 = {
  byId: {'2': {uid: '2', property: 'property'}},
  allIds: ['2']
};

const normalized2_2 = {
  byId: {'2': {uid: '2', property2: 'property2'}},
  allIds: ['2']
};

const normalized3 = {
  allIds: ['1', '2', '3'],
  byId: {
    '1': {uid: '1'},
    '2': {uid: '2'},
    '3': {uid: '3'}
  }
};

describe('Norm.ts', () => {
  describe('Norm.toNormalized', () => {
    it('should normalize object', () => {
      const result = Norm.toNormalized({uid: '1', property: 'property'});
      expect(result).toEqual({
        allIds: ['1'],
        byId: {'1': {uid: '1', property: 'property'}}
      });
    });
    it('should normalize array of objects', () => {
      const result = Norm.toNormalized([{uid: '1', property: 'property'}, {uid: '2', property: 'property2'}]);
      expect(result).toEqual({
        allIds: ['1', '2'],
        byId: {
          '1': {uid: '1', property: 'property'},
          '2': {uid: '2', property: 'property2'}
        }
      });
    });
  });

  describe('Norm.merge', () => {
    it('should return empty object when merging two empty objects', () => {
      const result = Norm.merge(emptyNormalized, emptyNormalized);
      expect(result).toEqual(emptyNormalized);
    });

    it('should return normalized when mergin with empty object', () => {
      const result = Norm.merge(normalized1, emptyNormalized);
      expect(result).toEqual(normalized1);
    });

    it('should return merged normalized', () => {
      const result = Norm.merge(normalized1, normalized2);
      expect(result).toEqual({
        byId: {'2': {uid: '2', property: 'property'}, '1': {uid: '1'}},
        allIds: ['1', '2']
      });
    });
    it('should deep merge two normalized', () => {
      const result = Norm.merge<{property?: string; property2?: string}>(normalized2, normalized2_2);
      expect(result).toEqual({
        byId: {'2': {uid: '2', property: 'property', property2: 'property2'}},
        allIds: ['2']
      });
    });
  });

  describe('Norm.omit', () => {
    it('should return equal normalized when there is no common uids', () => {
      const result = Norm.omit(normalized1, normalized2);
      expect(result).toEqual(normalized1);
    });

    it('should return empty normalized when all uids covers', () => {
      const result = Norm.omit<{property?: string; property2?: string}>(normalized2, normalized2_2);

      expect(result).toEqual({allIds: [], byId: {}});
    });

    it('should return diff when some uids covers', () => {
      const result = Norm.omit(normalized3, {
        allIds: ['1', '2'],
        byId: {
          '1': {uid: '1'},
          '2': {uid: '2'}
        }
      });

      expect(result).toEqual({
        allIds: ['3'],
        byId: {
          '3': {uid: '3'}
        }
      });
    });
  });

  describe('Norm.delete', () => {
    it('should return the same normalized when id is not present in the source', () => {
      const result = Norm.deleteId(normalized3, '5');
      expect(result).toEqual(result);
    });

    it('should return source without specified ids', () => {
      const result = Norm.deleteId(normalized3, '1');
      expect(result).toEqual({
        allIds: ['2', '3'],
        byId: {
          '2': {uid: '2'},
          '3': {uid: '3'}
        }
      });
    });
  });

  describe('Norm.filter', () => {
    it('should return only elements true for the predicate', () => {
      const normalizedAudio = {
        allIds: ['1', '2', '3', '4'],
        byId: {
          '1': {
            uid: '1',
            parentId: 'p1'
          },
          '2': {
            uid: '2',
            parentId: 'p2'
          },
          '3': {
            uid: '3',
            parentId: 'p3'
          },
          '4': {
            uid: '3',
            parentId: 'p4'
          }
        }
      };

      const validIds = ['3', 'p4'];

      const result = Norm.filter(
        normalizedAudio,
        audioElement => validIds.indexOf(audioElement.uid) > -1 || validIds.indexOf(audioElement.parentId) > -1
      );

      expect(result).toEqual({
        allIds: ['3', '4'],
        byId: {
          '3': {
            uid: '3',
            parentId: 'p3'
          },
          '4': {
            uid: '3',
            parentId: 'p4'
          }
        }
      });
    });

    it('returns empty normalized when predicate returns false', () => {
      const result = Norm.filter(normalized3, () => false);
      expect(result).toEqual(emptyNormalized);
    });

    it('returns source when predicate returns always true', () => {
      const result = Norm.filter(normalized3, () => true);
      expect(result).toEqual(normalized3);
    });
  });
});
