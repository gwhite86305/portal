import TextDialog from './TextDialog';
import * as React from 'react';
import {PortalComponent} from 'component-utils';
import {TextDialog as ExternalInterface} from '../common/ComponentsInterface';

export interface TextDialogContainerState {
  open: boolean;
}

export interface TextDialogContainerProps extends ExternalInterface {
  dialogTitle: string;
  dialogText: string;
  dialogCloseTitle: string;
}

class TextDialogContainer extends React.Component<TextDialogContainerProps, TextDialogContainerState> {
  state = {
    open: true
  };

  public render() {
    const {componentTheme, feature: {dialogTitle, dialogText, dialogCloseTitle}} = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <TextDialog
          handleClose={this.handleClose}
          open={this.state.open}
          dialogTitle={dialogTitle || ''}
          dialogText={dialogText || ''}
          dialogCloseTitle={dialogCloseTitle || ''}
        />
      </PortalComponent>
    );
  }

  handleClose = () => this.setState({open: false});
}

export default TextDialogContainer;
