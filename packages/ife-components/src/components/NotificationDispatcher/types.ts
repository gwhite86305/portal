import {hideMessage, showMessage} from 'component-utils';
import {initConnection} from '../../store/notificationDispatcher/actions';

export interface StateProps {
  isNotificationInProgress: boolean;
}

export interface DispatchProps {
  actions: {
    initConnection: typeof initConnection;
    showMessage: typeof showMessage;
    hideMessage: typeof hideMessage;
  };
}

export interface Props extends StateProps, DispatchProps {}
