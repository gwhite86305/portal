import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import Device from '../../types/Device';
import {equals, omit} from 'ramda';
import {
  IncrementPendingRequestsCounter,
  DecrementPendingRequestsCounter
} from 'core-components/dist/store/Loading/actions';

const {generateMessage} = utils.APIError;

// Action Creators
export const ActionCreators = {
  receiveDevice: new ActionCreator<'receiveDevice', any>('receiveDevice'),
  updateServiceTier: new ActionCreator<'updateServiceTier', number>('updateServiceTier'),
  waitForServiceTier: new ActionCreator<'waitForServiceTier', any>('waitForServiceTier')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function fetchDevice(url: string, oldDevice: Device) {
  return (dispatch: any, getState: any) => {
    axios.get(url).then(response => {
      const originalDeviceData: any = omit(['error'], getState().device);
      const newDeviceData: any = {...response.data};
      if (originalDeviceData['expectingServiceTierChange']) {
        if (originalDeviceData.serviceTierId === response.data.serviceTierId) {
          newDeviceData['expectingServiceTierChange'] = true;
        } else {
          newDeviceData['expectingServiceTierChange'] = false;
          dispatch(DecrementPendingRequestsCounter());
        }
      }
      if (!equals(response.data, originalDeviceData)) {
        dispatch(ActionCreators.receiveDevice.create(newDeviceData));
      }
    });
  };
}

export function setDeviceLanguage(url: string, defaultLocaleLanguage: string) {
  return (dispatch: any, getState: any) => {
    axios.post(url, {language: defaultLocaleLanguage}).catch(err => {
      dispatch(
        ActionCreators.receiveDevice.create(
          generateMessage('Could not update device endpoint with defaultLocale language', err)
        )
      );
    });
  };
}
