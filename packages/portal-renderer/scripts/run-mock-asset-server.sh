#! /bin/sh

case "$1" in
    start)
        node ../../node_modules/webpack-dev-server/bin/webpack-dev-server.js &> ./.server.log &
        echo $! > ./.server.pid
        sleep 25
        curl http://localhost:3001 &> /dev/null
        if [ $? -eq 0 ]; then
            echo -e "\e[92mAsset server started\e[0m"
            exit 0
        else
            echo -e "\e[91mAsset server could not be started\e[0m"
            kill $(cat ./.server.pid)
            exit 1

        fi
        ;;
    stop)
        kill $(cat ./.server.pid)
        if [ $? -eq 0 ]; then
            echo -e "\e[92mAsset server stopped\e[0m"
            exit 0
        else
            echo -e "\e[91mAsset server is unstoppable\e[0m"
            exit 0
        fi
        ;;
    *)
        echo $"Usage: $) {start|stop}"
        ;;
esac
