import {PortalComponent, utils} from 'component-utils';
import {lastIndexOf, pathOr, propEq, slice} from 'ramda';
import * as React from 'react';
import {VideoItem} from '../../store/videos/types';
import MovieDetails from './MovieDetails';
import {Loader, StyledContainer} from './StyledComponents';
import {
  DetailsContainerProps,
  DetailsContainerState,
  imgPlaceholder,
  mapDispatchToProps,
  mapStoreToProps
} from './utils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class MovieDetailsContainer extends React.PureComponent<DetailsContainerProps, DetailsContainerState> {
  public static contextType = RouterContext;
  public videoId = 'videoId=';
  private isViewReported = false;

  componentDidUpdate(prevProps: DetailsContainerProps) {
    if (!prevProps.videoNotFound && this.props.videoNotFound) {
      this.context.history.goBack();
    }

    if (!this.isViewReported && this.props.selectedItem) {
      this.isViewReported = true;
      this.props.actions.trackView(this.props.selectedItem);
    }
  }

  componentDidMount() {
    if (navigator.onLine) {
      this.readUrlAndFetchData();
    } else {
      this.props.actions.showMessage({
        title: 'Network Disconnect',
        message: 'You have been disconnected from the network',
        isError: true
      });
    }
    window.addEventListener('online', this.onNetworkReconnect);
  }

  componentWillUnmount() {
    window.removeEventListener('online', this.onNetworkReconnect);
  }

  render() {
    const {componentTheme, selectedItem, playlistItems, actions, playlistLoading} = this.props;
    const {showTitleHeader, showBackButton} = this.props.feature;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledContainer showTitleHeader={showTitleHeader} showBackButton={showBackButton}>
          {selectedItem && !playlistLoading ? (
            <MovieDetails
              imgPlaceholder={imgPlaceholder}
              onClickBack={this.onClickBack}
              isInPlaylist={!!playlistItems.find(propEq('uid', selectedItem.uid))}
              item={selectedItem as VideoItem}
              onAddToPlaylist={actions.addToPlaylist}
              onClickPlay={this.onClickPlay}
              onClickPlayTrailer={this.onClickPlayTrailer}
              onRemoveFromPlaylist={actions.removeFromPlaylist}
              onPurchaseSuccess={this.handlePurchaseSuccess}
            />
          ) : (
            <Loader />
          )}
        </StyledContainer>
      </PortalComponent>
    );
  }

  private onNetworkReconnect = () => {
    this.props.actions.hideMessage();
    this.context.history.goForward();
  };

  private handlePurchaseSuccess = () => this.readUrlAndFetchData();

  private onClickBack = () => this.context.history.goBack();

  private onClickPlay = (item: VideoItem) => {
    this.props.actions.trackPlay(item);
    this.props.actions.playItem(item, this.props.feature.playerPage as string);
  };

  private onClickPlayTrailer = (item: VideoItem) =>
    this.props.actions.playItemTrailer(item, this.props.feature.playerPage as string);

  private getContentId(querySearch = '') {
    const index = lastIndexOf(this.videoId, querySearch as any);
    return index > -1 ? slice(index + this.videoId.length, Infinity, querySearch) : '';
  }

  private readUrlAndFetchData() {
    const contentId = this.getContentId(pathOr('', ['location', 'search'], this.context));
    if (!contentId) {
      return;
    }
    this.props.actions.getVideoById(contentId);
  }
}
