import * as React from 'react';
import {MediaThumbnailProps, MediaThumbnailState} from './types';
import {Button} from '@material-ui/core';

export class MediaThumbnail extends React.Component<MediaThumbnailProps, MediaThumbnailState> {
  loadingRef: any;
  observer: any;
  threshold: object;
  unMounted: boolean;

  constructor(props: any) {
    super(props);
    this.state = {inView: false};
    this.threshold = {root: null, rootMargin: '0px', threshold: 0.1};
  }

  componentDidMount() {
    this.observer = new IntersectionObserver(this.observeEntries, this.threshold);
    this.observer.observe(this.loadingRef);
  }

  componentWillUnmount() {
    this.observer && this.observer.disconnect();
  }

  render() {
    let clickable = true;
    let pulsate = (
      <div
        className="pulsate"
        style={{
          visibility: 'visible',
          position: 'absolute',
          top: 0,
          left: 0,
          opacity: this.state.inView && this.props.src.length ? 0 : 1,
          transition: 'opacity .5s ease-out'
        }}
      />
    );

    return (
      <div
        className={this.props.className}
        ref={loadingRef => (this.loadingRef = loadingRef)}
        onClick={clickable ? this.props.onClick : () => {}}
        style={{
          backgroundSize: 'contain',
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundImage: `url(${this.state.inView && this.props.src})`,
          position: 'relative'
        }}
        children={clickable ? <Button aria-label={this.props.item.title} children={pulsate} /> : pulsate}
      />
    );
  }

  private observeEntries = (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => {
    for (let key in entries) {
      if (entries[key].intersectionRatio > 0.0) {
        this.setState({inView: true});
        this.props.inVieport && this.props.inVieport(this.props.item);
        observer.disconnect();
        return;
      }
    }
  };
}
