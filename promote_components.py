import boto3
import os
import sys
from botocore.client import ClientError
import json

# To find which directory the published files are in (default is dist is nothing is provided)


def findPublishedDirectory(filePath):
    with open(filePath) as f:
        data = json.load(f)
    return data["directories"]["lib"] if "directories" in data else 'dist'

# To find which version of library is latest in schema-server and update accordingly on s3


def findVersionInfo(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    dir_list = []
    for root, dirs, files in os.walk(some_dir):
        dir_list.extend(dirs)
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this+1:
            del dirs[:]
    return dir_list[-1]

# Traversing function to read through output folder of each directory


def directorySurfer(rootDir, bucket_name):
    for dirName, subdirList, fileList in os.walk(rootDir):
        for fname in fileList:
            upDir = putDir+dirName.split("/dist", 1)[1].replace("/dist", "")
            if fname not in excluded:
                s3.upload_file(dirName+'/'+fname, bucket_name, upDir+'/'+fname)
            else:
                s3.upload_file(dirName+'/'+fname,
                               bucket_name, putDir+'/'+fname)


try:
    arg_names = ['command', 'first', 'second', 'operation', 'option']
    args = dict(zip(arg_names, sys.argv))
    bucket_name = ""
    version_name = ""
    session = boto3.Session()
    s3 = session.client('s3')
    if 'first' in args:
        bucket_name = args['first']
    if 'second' in args:
        version_name = args['second']
    excluded = ['index.d.ts', 'index.js', 'index.js.map']

    # Getting the latest version for schema-server
    outputDir = findPublishedDirectory('packages/schema-server/package.json')
    versionLookUpDir = 'packages/schema-server/{}'.format(outputDir)
    versionDir = version_name if(
        version_name) else findVersionInfo(versionLookUpDir)

    # Clean out existing folder (to remove files no longer in the structure)
    s3resource = session.resource('s3')
    bucket = s3resource.Bucket(bucket_name)
    print("Deleting current objects in the version directory: %s" % versionDir)
    if versionDir and bucket:
        print("Deleting the following objects")
        print(list(bucket.objects.filter(Prefix=versionDir)))
        bucket.objects.filter(Prefix=versionDir).delete()

    print("Uploading version:- " + versionDir +
          " of the portals to S3 bucket -" + bucket_name)
    # Adding contents of core components
    outputDir = findPublishedDirectory('packages/core-components/package.json')
    print('Core-components: Output Directory-'+outputDir)
    coreDir = 'packages/core-components/{}'.format(outputDir)
    print('Core-components: Core Directory-'+coreDir)
    putDir = versionDir + '/lib/core'
    directorySurfer(coreDir, bucket_name)

    # Adding contents of ifc components
    outputDir = findPublishedDirectory('packages/ifc-components/package.json')
    print('ifc-components: Output Directory-'+outputDir)
    coreDir = 'packages/ifc-components/{}'.format(outputDir)
    print('ifc-components: Core Directory-'+coreDir)
    putDir = versionDir + '/lib/ifc'
    directorySurfer(coreDir, bucket_name)

    # Adding contents of ife components
    outputDir = findPublishedDirectory('packages/ife-components/package.json')
    print('ife-components: Output Directory-'+outputDir)
    coreDir = 'packages/ife-components/{}'.format(outputDir)
    print('ife-components: Core Directory-'+coreDir)
    putDir = versionDir + '/lib/ife'
    directorySurfer(coreDir, bucket_name)

    # Adding contents of portal-renderer components
    outputDir = findPublishedDirectory('packages/portal-renderer/package.json')
    print('portal-renderer: Output Directory-'+outputDir)
    coreDir = 'packages/portal-renderer/{}'.format(outputDir)
    print('portal-renderer: Core Directory-'+coreDir)
    putDir = versionDir + '/lib/portal-renderer'
    directorySurfer(coreDir, bucket_name)

    # Adding contents of schema-server components
    outputDir = findPublishedDirectory('packages/schema-server/package.json')
    print('schema-server: Output Directory-'+outputDir)
    coreDir = 'packages/schema-server/{}'.format(outputDir)
    print('schema-server: Core Directory-'+coreDir)
    putDir = versionDir + '/lib/schema-server'
    directorySurfer(coreDir, bucket_name)

except ClientError as excp:
    print(bucket_name+" S3 bucket does not exists or the version " +
          version_name+" is invalid")
    print("exception: %s" % excp)
