import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {isEnvUXBuilder} from 'component-utils/dist/utils/ComponentUtils';

const {generateMessage} = utils.APIError;

// Action Creators
export const ActionCreators = {
  receiveProfile: new ActionCreator<'receiveProfile', any>('receiveProfile')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function fetchProfile(url: string) {
  return (dispatch: any) => {
    // if (!isEnvUXBuilder()) { // Removing because there should be no collision between the profile endpoint and the layout request. This is necessary for simulation.
    axios
      .get(url)
      .then(response => {
        dispatch(ActionCreators.receiveProfile.create(response.data));
      })
      .catch(err => {
        dispatch(
          ActionCreators.receiveProfile.create(generateMessage('Could not retrieve profile endpoint data', err))
        );
      });
    // }
  };
}
