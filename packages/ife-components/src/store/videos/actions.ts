import {axiosWithBaseUrl as axios} from '../../utils/utils';
import {IRootState} from './../../index';
import {
  PrerollAd,
  VideoGroup,
  VideoItem,
  LocalizedCategory,
  VideosConfig,
  VideosConfigItem,
  VideoConfig
} from './types';
import {ActionCreator} from 'component-utils/dist/store/action-creator';
import localStorageService from '../../services/LocalStorageService';
import Norm, {Normalized, NormalizableObject} from '../../utils/Norm';
import {getAgeRestrictionCode} from '../settings/selectors';
import {flattenCategories, extractCategories} from '../../utils/categories';

export const ActionCreators = {
  getPreRollAdsError: new ActionCreator<'getPreRollAdsError', any>('getPreRollAdsError'),
  getPreRollAdsStart: new ActionCreator<'getPreRollAdsStart', any>('getPreRollAdsStart'),
  getPreRollAdsSuccess: new ActionCreator<'getPreRollAdsSuccess', {videoId: string; ads: PrerollAd[]}>(
    'getPreRollAdsSuccess'
  ),
  getSeriesError: new ActionCreator<'getSeriesError', any>('getSeriesError'),
  getSeriesStart: new ActionCreator<'getSeriesStart', any>('getSeriesStart'),
  getSeriesSuccess: new ActionCreator<'getSeriesSuccess', VideoGroup[]>('getSeriesSuccess'),
  getVideoByIdError: new ActionCreator<'getVideoByIdError', any>('getVideoByIdError'),
  getVideoByIdStart: new ActionCreator<'getVideoByIdStart', any>('getVideoByIdStart'),
  getVideoByIdSuccess: new ActionCreator<'getVideoByIdSuccess', VideoItem | VideoGroup>('getVideoByIdSuccess'),
  getVideoCategories: new ActionCreator<'getVideoCategories', any>('getVideoCategories'),
  getVideoCategoriesSuccess: new ActionCreator<'getVideoCategoriesSuccess', any>('getVideoCategoriesSuccess'),
  getVideoCategoriesFailure: new ActionCreator<'getVideoCategoriesFailure', any>('getVideoCategoriesFailure'),
  setSubtitle: new ActionCreator<'setSubtitle', {uid: string; lang: string}>('setSubtitle'),
  setAudioLang: new ActionCreator<'setAudioLang', {uid: string; lang: string}>('setAudioLang'),
  updateVideoConfig: new ActionCreator<'updateVideoConfig', VideosConfig>('updateVideoConfig'),
  updateVideoSubtitleLanguage: new ActionCreator<'updateVideoSubtitleLanguage', VideosConfig>(
    'updateVideoSubtitleLanguage'
  ),
  updateVideoAudioLanguage: new ActionCreator<'updateVideoAudioLanguage', VideosConfig>('updateVideoAudioLanguage'),
  loadVideosConfig: new ActionCreator<'loadVideosConfig', any>('loadVideosConfig')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function getSeries() {
  return async (dispatch: any, getState: () => IRootState) => {
    dispatch(ActionCreators.getSeriesStart.create({}));
    try {
      const ageRating = getAgeRestrictionCode(getState().ifeSettings);
      const {data: {items}} = await axios.get('/content/videos', {ageRating, categoryUid: 'tv'});
      dispatch(ActionCreators.getSeriesSuccess.create((items || []) as VideoGroup[]));
    } catch (e) {
      dispatch(ActionCreators.getSeriesError.create({}));
    }
  };
}

export function getVideoById(uid: string) {
  return async (dispatch: any, getState: () => IRootState) => {
    dispatch(ActionCreators.getVideoByIdStart.create({}));
    try {
      const ageRating = getAgeRestrictionCode(getState().ifeSettings);
      const {data} = await axios.get(`/content/videos/${uid}`, ageRating);
      dispatch(ActionCreators.getVideoByIdSuccess.create(data as VideoItem | VideoGroup));
      return data;
    } catch (e) {
      dispatch(ActionCreators.getVideoByIdError.create({}));
    }
  };
}

export function getVideoCategories() {
  return async (dispatch: any) => {
    dispatch(ActionCreators.getVideoCategories.create({}));
    try {
      const {data} = await axios.get(`/content/videos/categories`);
      dispatch(
        ActionCreators.getVideoCategoriesSuccess.create({
          seriesCategories: flattenCategories(extractCategories(data, 'tv'))
        })
      );
      return data;
    } catch (e) {
      dispatch(ActionCreators.getVideoCategoriesFailure.create({}));
    }
  };
}

export function getPreRollAds(videoId: string | undefined) {
  return async (dispatch: any, getState: () => IRootState) => {
    // Check video Id is valid
    if (!videoId) {
      return Promise.reject(undefined);
    }

    // Return value if already fetched
    if (getState().ifeVideos.preRollAds[videoId]) {
      return getState().ifeVideos.preRollAds[videoId];
    }

    // Perform async call
    dispatch(ActionCreators.getPreRollAdsStart.create(undefined));
    try {
      const {data: {items}} = await axios.get(`/content/videos/${videoId}/prerolls`);
      const ads = items || [];
      dispatch(ActionCreators.getPreRollAdsSuccess.create({videoId, ads}));
      return ads;
    } catch {
      dispatch(ActionCreators.getPreRollAdsError.create({videoId, ads: []}));
      return [];
    }
  };
}

export const updateVideoConfig = (videoConfig: NormalizableObject) => (dispatch: any) => {
  const payload = Norm.toNormalized(videoConfig);
  return dispatch(ActionCreators.updateVideoConfig.create(payload));
};

export const loadVideosConfig = () => async (dispatch: any) => {
  const videosConfig: any = (await localStorageService.readData('videosConfig')) || {
    byId: {},
    allIds: []
  };

  localStorageService.writeData('videosConfig', videosConfig);
  return dispatch(ActionCreators.loadVideosConfig.create(videosConfig));
};

export function setSubtitle(uid: string, lang: string): any {
  return (dispatch: any) => dispatch(ActionCreators.setSubtitle.create({uid, lang}));
}

export function setAudioLang(uid: string, lang: string): any {
  return (dispatch: any) => dispatch(ActionCreators.setAudioLang.create({uid, lang}));
}
