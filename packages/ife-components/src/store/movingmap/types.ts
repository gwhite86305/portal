export interface IMovingMapState {
  readonly flightInfo: FlightInfo;
  readonly flightProgress: FlightProgress;
  readonly error: boolean;
  readonly loading: boolean;
  readonly firstLoad: boolean;
  readonly followingPlane: boolean;
  readonly map: {
    minZoom: number;
    maxZoom: number;
    mapUrl: string;
  };
}

export interface Coordinate {
  latitude: number;
  longitude: number;
}

export interface FlightInfo {
  readonly originPoint: Coordinate;
  readonly destinationPoint: Coordinate;
  readonly aircraftId: string;
  readonly originName: string;
  readonly destinationName: string;
}

export interface FlightProgress {
  readonly trackedRoutePoints: Coordinate[];
  readonly position: Coordinate;
  readonly altitude: number;
  readonly airSpeed: number;
  readonly progress: number;
  readonly takeOffTime: number;
  readonly timeToDestInMinutes: number;
}
