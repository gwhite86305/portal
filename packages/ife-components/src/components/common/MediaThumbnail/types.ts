import {AudioGroup} from '../../../store/audio/types';
import {VideoGroup, VideoItem} from '../../../store/videos/types';
import {MovieItem} from '../../../store/movies/types';
import {ImageVariants, ImageVariantsType} from '../../types';

export interface MediaThumbnailProps {
  className?: string;
  inVieport?(item: AudioGroup | VideoGroup | VideoItem | MovieItem): any;
  item: AudioGroup | VideoGroup | VideoItem | MovieItem;
  onClick?(id: any): any;
  src: string;
  clickable: boolean;
}

export type OmitUtil<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export type MediaThumbnailWithSizeProps = OmitUtil<MediaThumbnailProps, 'src'> & {
  imageVariants: ImageVariants;
  variantType: ImageVariantsType;
};

export interface MediaThumbnailState {
  inView: boolean;
}
