import {screenshot_test, delay, handleFailures} from './utils';
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');

describe('JBU VISUAL REGRESSION TEST', () => {
  let originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 8000000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it(`LAPTOP`, async () => {
    const path = 'test/screenshot/laptop/jbu/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    page.on('console', msg => {
      //console.log(msg);
    });
    page.on('error', msg => {
      //console.log(msg);
    });
    await page.setViewport({width: 1000, height: 1000});
    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    await delay(2000);

    //LANGUAGE PAGE
    results.push(await screenshot_test(page, `${path}language`));
    const LANG = await page.$x("//div[@data-component-guid='HASH_LANGUAGE_SELECTOR_EN']");
    await LANG[0].click(); //Selects English language button
    await delay(10000);

    // HOME PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(10000);

    // FAQ PAGE
    const FAQ = await page.$x("//div[contains(@data-component-guid, 'HASH_FOOTER_SECTION_1_2_LINK')]/a");
    await FAQ[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}faq`));

    // TOS PAGE
    const TOS = await page.$x("//div[contains(@data-component-guid, 'HASH_FOOTER_SECTION_2_2_LINK')]/a");
    await TOS[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}tos`));

    await browser.close();
    handleFailures('jbu', 'LAPTOP', results);
  });

  it(`MOBILE`, async () => {
    const path = 'test/screenshot/mobile/jbu/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    await page.emulate(devices['iPhone 6']);
    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});

    //LANGUAGE PAGE
    results.push(await screenshot_test(page, `${path}language`));
    const LANG = await page.$x("//div[@data-component-guid='HASH_LANGUAGE_SELECTOR_EN']");
    await LANG[0].click(); //Selects English language button
    await delay(10000);

    // HOME PAGE
    results.push(await screenshot_test(page, `${path}home`));
    await delay(10000);

    // FAQ PAGE
    const FAQ = await page.$x("//div[contains(@data-component-guid, 'HASH_FOOTER_SECTION_1_2_LINK')]/a");
    await FAQ[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}faq`));

    // TOS PAGE
    const TOS = await page.$x("//div[contains(@data-component-guid, 'HASH_FOOTER_SECTION_2_2_LINK')]/a");
    await TOS[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}tos`));

    await browser.close();
    handleFailures('jbu', 'MOBILE', results);
  });
});
