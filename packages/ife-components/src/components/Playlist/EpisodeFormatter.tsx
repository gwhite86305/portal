/**
 * Customers will likely want different formats for presenting how Season/Episode
 * It will also need localisation. To ease this, we can use a stateless component for now and pass it more things when we need to.
 */

import * as React from 'react';
import {EpisodeFormatterProps} from './types';

const EpisodeFormatter = (props: EpisodeFormatterProps) => {
  let episode = props.episode;
  return (
    <span>
      S{episode.season}:E{episode.episode}
    </span>
  );
};

export default EpisodeFormatter;
