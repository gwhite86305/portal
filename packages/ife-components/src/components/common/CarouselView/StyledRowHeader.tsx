import * as React from 'react';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

export const StyledRowHeader = styled.div`
    ${(props: any) => renderThemeProperty('rowHeaderTransform', 'text-transform')};
}`;
