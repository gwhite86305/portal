import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {ServiceCountdown as ExternalInterface} from '../common/ComponentsInterface';
import {getServiceAvailabilityState} from '../../utils/ConnectionUtils';
import ServiceCountdown, {InternalInterface} from './ServiceCountdown';
import ServiceCountDownCircle, {InternalInterface as internalIntefaceForCircle} from './ServiceCountDownCircle';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {pathOr} from 'ramda';

export type PageProps = DispatchProps & ExternalInterface;

export interface DispatchProps {}

@connectWrapper((state: any, ownProps: PageProps) => {
  let props = {
    ...ownProps,
    timeRemaining: 0,
    percentageRemaining: 100,
    hours: '- -',
    minutes: '- -',
    buttonText: '-- --'
  };
  if (state.device) {
    if (state.profile) {
      let connected = !!getServiceAvailabilityState(state.device);
      let remaining =
        state.device.serviceTimeRemaining > state.device.flightMinutesRemaining * 60
          ? state.device.flightMinutesRemaining * 60
          : state.device.serviceTimeRemaining;
      props.hours = remaining && connected ? Math.trunc(remaining / 3600).toString() : '- -';
      props.minutes = remaining && connected ? Math.trunc((remaining / 60) % 60).toString() : '- -';

      if (state.profile.serviceTiers) {
        const currentTier = state.profile.serviceTiers.filter((tier: any) => tier.id == state.device.serviceTierId)[0];
        if (pathOr(1, ['duration'], currentTier) >= 24 * 60 * 60) {
          props.percentageRemaining = 100;
        } else {
          props.percentageRemaining =
            Math.min(Math.max(0, props.timeRemaining / pathOr(1, ['duration'], currentTier)), 1) * 100;
        }
        props.buttonText = pathOr(props.buttonText, ['description'], currentTier);
      }
    }
  }
  return props;
})
class ServiceCountdownContainer extends React.Component<
  PageProps & InternalInterface & internalIntefaceForCircle,
  any
> {
  public render() {
    return this.props.feature.showCircle ? (
      <ServiceCountDownCircle {...this.props} />
    ) : (
      <ServiceCountdown {...this.props} />
    );
  }
}

export default withLocaleProps(ServiceCountdownContainer, [
  {headerText: 'ifc.ServiceCountdown.header.text'},
  {mainHeaderText: 'ifc.ServiceCountdown.mainHeader.text'}
]);
