import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import * as React from 'react';
import * as defaultProps from './defaultProps.json';
import {Button} from '@material-ui/core';
import {BackNavigationProps} from './utils.js';

const BackNavigation: React.FunctionComponent<BackNavigationProps> = props => {
  return (
    <Button aria-label="Go back" className="back-button" onClick={props.onClickBack} children={<ArrowBackIcon />} />
  );
};

BackNavigation.defaultProps = defaultProps as any;

export default BackNavigation;
