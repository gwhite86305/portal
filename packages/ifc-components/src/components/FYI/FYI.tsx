import * as React from 'react';
import {PortalComponent, ResponsiveImage, sanitize, utils} from 'component-utils';
import {withRouter} from 'react-router';
import {FYI as ExternalInterface} from '../common/ComponentsInterface';
import * as defaults from './defaultProps.json';
import {Button} from 'core-components';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  className?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  position: relative;
  ${(props: any) => {
    return applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme);
  }};
  flex-basis: 100%;
  flex-direction: column;
  ${renderThemeProperty('backgroundColor', 'background')};
  font-family: ${(props: any) => props.theme.secondaryFontFamily};
  > * {
    display: flex;
  }
  div.button-wrap {
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.875);
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 6.25);
    button {
      width: 160px;
      height: 41px;
      margin: 0 auto;
      white-space: nowrap;
      justify-content: center;
      font-size: ${(props: any) => props.theme.primaryFontSize};
      font-weight: 400;
    }
  }
  .fyi-textregion {
    flex-wrap: wrap;
    flex-direction: column;
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'padding-left',
        {
          xs: `calc(${props.theme.gutter} * 1.25)`,
          sm: `calc(${props.theme.gutter} * 2.5)`
        },
        props.theme
      );
    }};
    padding-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    ${(props: any) => {
      return applyMediaQueriesForStyle(
        'padding-right',
        {
          xs: `calc(${props.theme.gutter} * 1.25)`,
          sm: `calc(${props.theme.gutter} * 2.5)`
        },
        props.theme
      );
    }};
  }
  .fyi-header {
    display: inline-block;
    word-wrap: break-word;
    ${renderThemeProperty('headerTextColor', 'color')};
    h1 {
      display: inline-block;
      font-size: calc(${(props: any) => props.theme.tertiaryFontSize} * 2);
      margin: auto;
      ${(props: any) => {
        return applyMediaQueriesForStyle(
          'width',
          {
            xs: `292px`,
            sm: `auto`
          },
          props.theme
        );
      }};
    }
  }
  .subheader-text {
    padding-top: calc(${(props: any) => props.theme.gutter} * 1.5);
    font-size: ${(props: any) => props.theme.primaryFontSize};
    font-family: ${(props: any) => props.theme.appFontFamily};
    line-height: 19px;
    text-align: start;
    cursor: pointer;
    word-wrap: break-word;
    ${renderThemeProperty('subHeaderTextColor', 'color')};
  }
  h1 {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;
    /* Adds a hyphen where the word breaks, if supported (No Blink) */
    -ms-hyphens: auto;
    -moz-hyphens: auto;
    -webkit-hyphens: auto;
    hyphens: auto;
  }
`;

const HeaderText = (props: any) => {
  const {headerText} = props;
  return (
    <div className="fyi-header">
      <h1>{headerText}</h1>
    </div>
  );
};

class FYI extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;
  onClick = () => {
    this.props.history.push('/' + this.props.feature.openPageOnClick);
  };

  public render() {
    const image = this.props.feature.image;
    const {altTextForImage, subHeaderText, headerText, buttonText} = this.props;
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          <div className="fyi-textregion">
            {headerText ? <HeaderText headerText={headerText} /> : null}
            {subHeaderText ? (
              <div className="subheader-text">
                <p dangerouslySetInnerHTML={sanitize(subHeaderText)} />
              </div>
            ) : null}
          </div>
          <ResponsiveImage altText={altTextForImage} image={image} />
          {this.props.feature.includeAcceptButton ? (
            <div className="button-wrap">
              <Button isPrimary={true} onClick={this.onClick.bind(this)} aria-label={buttonText}>
                {buttonText}
              </Button>
            </div>
          ) : null}
        </Container>
      </PortalComponent>
    );
  }
}

export default withRouter(FYI);
