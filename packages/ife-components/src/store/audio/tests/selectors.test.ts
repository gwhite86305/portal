import {selectAudioByCategories} from '../selectors';
import {IAudioState} from '../reducer';

// test getErrorMessage
describe('selectAudioByCategories', () => {
  it('should select appropriate data', () => {
    const fixture: any = {
      categories: {
        byId: {
          audiobookseducational: {
            uid: 'audiobookseducational',
            title: 'Educational',
            category: 'audiobooks'
          }
        },
        allIds: ['audiobookseducational']
      },
      items: {
        byId: {
          '5a86f309e4b0b8f353049770': {
            uid: '5a86f309e4b0b8f353049770',
            type: 'audioGroup',
            releaseDate: '2015-01-01',
            artist: 'Various Artists',
            title: 'Antique Phonograph Music Program',
            galleryImages: [
              {
                original: {
                  url: 'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf.jpg',
                  width: 290,
                  height: 290
                },
                small: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-small.jpg',
                  width: 144,
                  height: 144
                },
                medium: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-medium.jpg',
                  width: 288,
                  height: 288
                },
                large: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-large.jpg',
                  width: 432,
                  height: 432
                },
                poster1x: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-poster1x.jpg',
                  width: 1,
                  height: 1
                }
              }
            ],
            titleImages: [
              {
                original: {
                  url: 'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf.jpg',
                  width: 290,
                  height: 290
                },
                small: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-small.jpg',
                  width: 144,
                  height: 144
                },
                medium: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-medium.jpg',
                  width: 288,
                  height: 288
                },
                large: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-large.jpg',
                  width: 432,
                  height: 432
                },
                poster1x: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-poster1x.jpg',
                  width: 1,
                  height: 1
                }
              }
            ],
            categories: ['audiobookseducational', 'audiobooks'],
            reviewRating: 0
          }
        },
        allIds: ['5a86f309e4b0b8f353049770']
      },
      error: '',
      isLoadingItems: false,
      isLoadingSynopsis: false,
      userConfig: {}
    };
    const expectedResult = [
      {
        parentCategory: 'audiobooks',
        category: 'Educational',
        uid: 'audiobookseducational',
        items: [
          {
            uid: '5a86f309e4b0b8f353049770',
            type: 'audioGroup',
            releaseDate: '2015-01-01',
            artist: 'Various Artists',
            title: 'Antique Phonograph Music Program',
            galleryImages: [
              {
                original: {
                  url: 'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf.jpg',
                  width: 290,
                  height: 290
                },
                small: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-small.jpg',
                  width: 144,
                  height: 144
                },
                medium: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-medium.jpg',
                  width: 288,
                  height: 288
                },
                large: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-large.jpg',
                  width: 432,
                  height: 432
                },
                poster1x: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-poster1x.jpg',
                  width: 1,
                  height: 1
                }
              }
            ],
            titleImages: [
              {
                original: {
                  url: 'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf.jpg',
                  width: 290,
                  height: 290
                },
                small: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-small.jpg',
                  width: 144,
                  height: 144
                },
                medium: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-medium.jpg',
                  width: 288,
                  height: 288
                },
                large: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-large.jpg',
                  width: 432,
                  height: 432
                },
                poster1x: {
                  url:
                    'https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5d1373efe4b0b8d572e45fbf-poster1x.jpg',
                  width: 1,
                  height: 1
                }
              }
            ],
            categories: ['audiobookseducational', 'audiobooks'],
            reviewRating: 0
          }
        ]
      }
    ];
    expect(selectAudioByCategories(fixture as IAudioState)).toEqual(expectedResult);
  });
});
