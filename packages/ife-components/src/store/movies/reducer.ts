import {getMoviesStart, getMoviesError, getMoviesSuccess} from './actions';
import {IMoviesState} from './types';
import Norm from '../../utils/Norm';

export const initialState: IMoviesState = {
  categories: {
    byId: {},
    allIds: []
  },
  items: {
    byId: {},
    allIds: []
  },
  error: '',
  isLoadingItems: false,
  isLoadingSynopsis: false
};

export default function reducer(state: IMoviesState = initialState, action: {type: string; payload: any}) {
  const {type, payload} = action;
  switch (type) {
    case getMoviesStart.type:
      return {
        ...state,
        isLoadingItems: true,
        error: ''
      };
    case getMoviesSuccess.type:
      return {
        ...state,
        items: Norm.merge(state.items, payload.items),
        categories: Norm.merge(state.categories, payload.categories),
        isLoadingItems: false,
        error: ''
      };
    case getMoviesError.type:
      return {
        ...state,
        isLoadingItems: false,
        error: 'Could Not Fetch Data'
      };
    default:
      return state;
  }
}
