import * as React from 'react';
import {FormattedNumber} from 'react-intl';
import {IntlProvider} from 'react-intl';

export const Currency = (props: any) => {
  const currency = props.currency;
  const minimum = props.minimumFractionDigits || 0;
  const maximum = props.maximumFractionDigits || 0;
  return (
    <IntlProvider locale={props.locale}>
      <span>
        <FormattedNumber
          value={props.value}
          style="currency"
          currency={currency}
          minimumFractionDigits={minimum}
          maximumFractionDigits={maximum}
        />
        {props.currencySuffix && props.locale === 'en' && <span style={{paddingLeft: '5px'}}>{currency}</span>}
      </span>
    </IntlProvider>
  );
};
