/*
* TODO:
* Attach <%= someName %>Reducer in packages/ife-components/src/index.ts
* Add <%= SomeName %>State to the IRootState in packages/ife-components/src/index.ts
* Remove this comment. 
*/

import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { <%= someName %>ExampleAction } from './actions';

export interface <%= SomeName %>State {
    data: string
};

export const <%= someName %>InitialState: <%= SomeName %>State = {
    data: 'exampleData'
};

export const <%= someName %>Reducer = reducerWithInitialState(<%= someName %>InitialState).case(
    <%= someName %>ExampleAction,
    (state, { data }): <%= SomeName %>State => ({
        ...state,
        data
    })
)