import * as React from 'react';
import {assocPath, compose, isNil, keys, map, pathOr, reject} from 'ramda';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {injectIntl} from 'react-intl';
import * as UXActions from '../store/ux/actions';
import {Locale} from '../types/LayoutInterface';
import {LayoutContext} from '../utils/Contexts';

const FIELD_NOT_DEFINED_MSG = '{undefined for locale}';

export enum MenuItemType {
  LINK = 'link',
  LANGUAGE = 'language'
}

export interface LocaleResolvedMenuItem {
  label: string;
  iconAltText: string;
}

export interface MenuItemDecoratedProps {
  handleMenuItemSelection(event: any, index: number, childIndex: number, completionHandlerFn: any): any;

  intl: any;
}

export interface WithMenuItemDecoratorHOCProps {
  changeUserLocale(locale: String): any;
}

export const willHandlerSinkMenuItemSelection = (menuItem: any) => {
  return menuItem.type === MenuItemType.LANGUAGE;
};

export function withMenuItemDecorator(WrappedComponent: any, menuItemsPropId: string) {
  const ComponentWithMenuItemProps = class extends React.Component<
    MenuItemDecoratedProps & WithMenuItemDecoratorHOCProps,
    any
  > {
    public static contextType = LayoutContext;

    constructor(props: any) {
      super(props);
    }

    private _handleMenuSelection(event: any, index: number, childIndex: number, completionHandlerFn: any) {
      const menuItems = pathOr([], ['feature', menuItemsPropId], this.props);

      if (menuItems[index].type === MenuItemType.LANGUAGE) {
        event.preventDefault();
        this.props.changeUserLocale(menuItems[index].children[childIndex].data);
        completionHandlerFn(event);
      }
    }

    public render() {
      const layout = this.context;
      const {intl} = this.props;
      const menuItems = pathOr([], ['feature', menuItemsPropId], this.props);
      const decoratedMenuItems = map((menuItem: any) => {
        const localizedLabel = menuItem.labels[intl.locale];
        const {label, iconAltText} = localizedLabel || {
          label: FIELD_NOT_DEFINED_MSG,
          iconAltText: FIELD_NOT_DEFINED_MSG
        };

        menuItem.label = label;
        menuItem.iconAltText = iconAltText;

        // resolve locale onto menuItem object body to make a LocaleResolvedMenuItem for simpler render in component
        if (menuItem.type === MenuItemType.LANGUAGE) {
          // filter(<condition>)[0] is used instead of find(<condition>) in order to support IE 11
          const selectedLocale: Locale = layout.locale.filter((l: Locale) => l.code === intl.locale)[0] as Locale;
          menuItem.label = label.replace(/{locale}/g, selectedLocale.title);
          menuItem.iconAltText = iconAltText ? iconAltText.replace(/{locale}/g, selectedLocale.title) : '';

          const layoutLocales = layout.locale;
          if (layoutLocales && layoutLocales.length > 1) {
            menuItem.children = map((locale: Locale) => {
              return {
                data: locale.code,
                label: locale.title || FIELD_NOT_DEFINED_MSG
              };
            }, layout.locale);

            return menuItem;
          } else {
            return null;
          }
        } else {
          // resolve locale onto menuItem object body to make a LocaleResolvedMenuItem for simpler render in component
          if (menuItem.children) {
            menuItem.children.forEach((childMenuItem: any) => {
              childMenuItem.label = childMenuItem.labels[intl.locale]
                ? childMenuItem.labels[intl.locale].label
                : FIELD_NOT_DEFINED_MSG;
            });
          }
          return menuItem;
        }
      }, menuItems);

      return (
        <WrappedComponent
          {...assocPath(['feature', menuItemsPropId], reject(isNil, decoratedMenuItems), this.props)}
          handleMenuItemSelection={this._handleMenuSelection.bind(this)}
        />
      );
    }
  };

  function mapDispatchToProps(dispatch: any) {
    return {
      changeUserLocale: (locale: string) => dispatch(UXActions.changeUserLocale(locale))
    };
  }

  return connect(
    null,
    mapDispatchToProps
  )(injectIntl(ComponentWithMenuItemProps));
}
