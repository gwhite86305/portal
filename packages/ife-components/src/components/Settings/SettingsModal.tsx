import {Button, Modal} from '@material-ui/core';
import * as React from 'react';
import {ResponsiveContainer} from '../Playlist/styledComponents';
import {StyledModalContent} from './styledComponents';
import CloseIcon from '@material-ui/icons/Close';

export interface SettingsModalProps {
  children: React.ReactElement<any>;
  isModalOpen: boolean;
  toggleModal: () => void;
  mountRef: HTMLElement;
  ctaLabel: string;
  isCtaEnabled: boolean;
  handleCtaClick: (e: any) => void;
}

export const SettingsModal = ({
  children,
  isModalOpen,
  toggleModal,
  mountRef,
  ctaLabel,
  isCtaEnabled,
  handleCtaClick
}: SettingsModalProps) => {
  return (
    <div>
      <Modal
        container={mountRef}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={isModalOpen}
        onClose={toggleModal}
      >
        <ResponsiveContainer>
          <StyledModalContent>
            <div className="modal-header">
              <div className="title">
                <span>Settings</span>
              </div>
              <Button aria-label="Close" className="close-button" onClick={toggleModal} children={<CloseIcon />} />
            </div>
            <div className="modal-content">{children}</div>
            <div className="modal-footer">
              <Button
                aria-label={ctaLabel}
                variant="contained"
                disabled={!isCtaEnabled}
                className="cta apply"
                onClick={handleCtaClick}
              >
                {ctaLabel}
              </Button>
            </div>
          </StyledModalContent>
        </ResponsiveContainer>
      </Modal>
    </div>
  );
};
