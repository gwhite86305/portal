import {SystemMessage} from 'component-utils/dist/store/system/reducer';
import {pathOr} from 'ramda';
import {IRootState} from '../..';
import {hideMessage, showMessage} from '../../../../component-utils/dist/store/system/actions';
import {initConnection} from '../../store/notificationDispatcher/actions';
import {DispatchProps, StateProps} from './types';

export const mapStoreToProps = (state: IRootState): StateProps => ({
  isNotificationInProgress: pathOr(false, ['ifeNotificationDispatcher', 'messages', 'value'], state)
});

export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    initConnection: () => dispatch(initConnection()),
    showMessage: (message: SystemMessage) => dispatch(showMessage(message, 0)),
    hideMessage: () => dispatch(hideMessage())
  }
});
