import * as React from 'react';
import HamburgerMenu from '../HamburgerMenu/HamburgerMenu';
import HorizontalMenu from '../HorizontalMenu/HorizontalMenu';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {Header as ExternalInterface} from '../common/ComponentsInterface';
import NavLink from '../common/NavLink';
import * as defaults from './defaultProps.json';
import {getHamburgerMenuProps, getHorizontalMenuProps, HamburgerMenuProps, HorizontalMenuProps} from './utils';

// -- Styled Components
const StyledContainer = styled.div`
  width: 100%;
  > div {
    display: flex;
    flex: 1;
    width: 100%;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    z-index: 10;
    ${renderThemeProperty('backgroundColor', 'background-color')};
    ${renderThemeProperty('position', 'position')};
    ${renderThemeProperty('textActiveColor', 'color')};
    .icon {
      position: relative;
      z-index: 11;
    }
    a:focus {
      ${renderThemeProperty('textHoverColor', 'color', true)};
    }
    button:focus {
      ${renderThemeProperty('textHoverColor', 'color', true)};
    }
  }
  .skipNav span {
    display: none;
    ${renderThemeProperty('skipNavTextColor', 'color')};
  }
  .skipNav:focus span {
    display: inline-block;
    ${renderThemeProperty('boxShadow', 'box-shadow')};
    ${renderThemeProperty('skipNavTextColor', 'color')};
  }
  .skipTarget {
    position: absolute;
    left: -3000px;
  }
`;

const HorizontalContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
`;

const BurgerContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  ${renderThemeProperty('hamburgerPadding', 'padding')};
`;

const StyledNavLink: any = styled(NavLink)`
  ${props => applyMediaQueriesForStyle('margin', props.theme.componentTheme.airlineLogoMargin, props.theme)};
  img {
    height: ${(props: any) => props.theme.componentTheme.airlineLogoHeight};
  }
`;

export const AirlineLogo = (props: any) => {
  const {airlineLogo, localizedAltImageText} = props;
  return airlineLogo ? <div className="icon" children={<img src={airlineLogo} alt={localizedAltImageText} />} /> : null;
};

const HamburgerLeft = (props: any) => {
  let {feature, horizontalMenuProps, hamburgerProps, localizedAltImageText, keypress} = props;
  return (
    <React.Fragment>
      <div tabIndex={0} className="skipNav" onKeyPress={keypress}>
        <span>{props.skipNavText}</span>
      </div>
      <BurgerContainer children={<HamburgerMenu {...hamburgerProps} />} />
      <StyledNavLink
        to={feature.airlineLogoUrl}
        activeClassName="selected"
        children={<AirlineLogo localizedAltImageText={localizedAltImageText} {...feature} />}
      />
      <HorizontalContainer children={<HorizontalMenu {...horizontalMenuProps} />} />
    </React.Fragment>
  );
};
const HamburgerRight = (props: any) => {
  let {feature, horizontalMenuProps, hamburgerProps, localizedAltImageText, keypress} = props;
  let horizontalMenu = props.feature.includeHorizontalMenu ? (
    <HorizontalContainer children={<HorizontalMenu {...horizontalMenuProps} />} />
  ) : null;
  let hamburgerMenu = props.feature.includeHamburgerMenu ? (
    <BurgerContainer children={<HamburgerMenu {...hamburgerProps} />} />
  ) : null;
  return (
    <React.Fragment>
      <StyledNavLink
        to={feature.airlineLogoUrl}
        activeClassName="selected"
        children={<AirlineLogo localizedAltImageText={localizedAltImageText} {...feature} />}
      />
      <div tabIndex={0} className="skipNav" onKeyPress={keypress}>
        <span>{props.skipNavText}</span>
      </div>
      {horizontalMenu}
      {hamburgerMenu}
    </React.Fragment>
  );
};

class Header extends React.Component<ExternalInterface, any> {
  public static defaultProps = defaults;
  skipTarget: any;

  constructor(props: any) {
    super(props);
    this.skipTarget = React.createRef();
  }

  onSkip = () => {
    this.skipTarget.current.focus();
  };

  public render() {
    const {componentTheme, feature, localizedAltImageText} = this.props;
    const mergedTheme = {...((Header.defaultProps as any).componentTheme || {}), ...componentTheme};
    const horizontalMenuProps: HorizontalMenuProps = getHorizontalMenuProps(
      feature,
      mergedTheme
    ) as HorizontalMenuProps;
    const hamburgerProps: HamburgerMenuProps = getHamburgerMenuProps(feature, mergedTheme) as HamburgerMenuProps;
    const hamburgerOnTheLeft = componentTheme && componentTheme.hamburgerOnTheLeft;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledContainer>
          <div>
            {hamburgerOnTheLeft ? (
              <HamburgerLeft
                {...this.props}
                horizontalMenuProps={horizontalMenuProps}
                feature={feature}
                hamburgerProps={hamburgerProps}
                localizedAltImageText={localizedAltImageText}
                keypress={this.onSkip.bind(this)}
              />
            ) : (
              <HamburgerRight
                {...this.props}
                horizontalMenuProps={horizontalMenuProps}
                feature={feature}
                hamburgerProps={hamburgerProps}
                localizedAltImageText={localizedAltImageText}
                keypress={this.onSkip.bind(this)}
              />
            )}
            <span tabIndex={0} className="skipTarget" ref={this.skipTarget} />
          </div>
        </StyledContainer>
      </PortalComponent>
    );
  }
}

export default Header;
