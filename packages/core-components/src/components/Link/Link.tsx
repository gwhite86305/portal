import * as defaults from './defaultProps.json';
import * as React from 'react';
import {Link as ExternalInterface} from '../common/ComponentsInterface';
import {PortalComponent, sanitize, utils} from 'component-utils';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import NavLink from '../common/NavLink';

import styled from 'styled-components';
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;
const {CommonComponentThemeWrapper} = utils.ComponentUtils;

export interface InternalInterface {
  textOverride?: string;
  className?: string;
}

const LinkWrapper = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${renderThemeProperty('fontFamily', 'font-family')};
  ${renderThemeProperty('fontSize', 'font-size')};
  ${renderThemeProperty('letterSpacing', 'letter-spacing')};
  ${renderThemeProperty('zIndex', 'z-index')};
  ${renderThemeProperty('fontWeight', 'font-weight')};
  ${renderThemeProperty('wordWrap', 'word-wrap')};
  ${renderThemeProperty('shadow', 'text-shadow')};

  ${(props: any) => applyMediaQueriesForStyle('text-align', props.theme.componentTheme.align, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme, true)};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};

  a {
    ${renderThemeProperty('textDecoration', 'text-decoration')};
    ${renderThemeProperty('color', 'color')};
  }

  .active-class {
    ${renderThemeProperty('activeColor', 'color')};
  }
`;

class Link extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme, localizedText, children, intl} = this.props;
    const currentLocale = intl.locale;

    const link = this.props.feature.isMediaAsset
      ? `${this.props.feature.openPageOnClick}_${currentLocale}.pdf`
      : this.props.feature.openPageOnClick;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <LinkWrapper className="core-link-component">
          <NavLink
            activeClassName="active-class"
            to={`/${this.props.feature.externalUrl || link}`}
            openInNewTab={this.props.feature.openInNewTab}
            isMediaAsset={this.props.feature.isMediaAsset}
            dangerouslySetInnerHTML={sanitize(localizedText)}
            child={children && (children as any).length ? children : null}
          />
        </LinkWrapper>
      </PortalComponent>
    );
  }
}

export default withLocaleProps(Link, [{localizedText: 'core.Link.defaultText'}]) as any;
