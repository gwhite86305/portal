import styled from 'styled-components';

const ZINDEX = 999;

export interface StyledOverlayProps {
  isFixedComponent: boolean;
  isInnerOverlay: boolean;
  hasInnerOverlay?: boolean;
}

export const InsertOverlay = styled.div`
  cursor: pointer;
  font-size: 16px;
  text-align: center;
  font-weight: bold;
  background-color: #75a0ff;
  position: absolute;
  color: white;
  width: 18px;
  justify-content: space-around;
  z-index: ${() => ZINDEX + 1};
`;

export const InsertCenterOverlay = styled.div`
  cursor: pointer;
  font-size: 40px;
  text-align: center;
  color: #75a0ff !important;
  position: absolute;
  justify-content: space-around;
  align-self: center;
  z-index: ${() => ZINDEX + 1};
  height: 24px;
  line-height: 20px;
  padding: 0;
`;

export const InsertOverlayTitle = styled.div`
  background-color: #75a0ff;
  color: white;
  left: -2px;
  padding: 3px 8px;
  font-family: sans-serif;
  font-size: 12px;
  display: inline-block;
  height: 14px;
  line-height: 13px;
  border-radius: 5px;
  position: absolute;
`;

export default styled.div<StyledOverlayProps>`
  outline: ${(props: StyledOverlayProps) =>
    props.hasInnerOverlay ? 'none!important' : '5px solid rgba(119,162,252,1) !important'};
  position: ${(props: StyledOverlayProps) => (props.isFixedComponent ? 'fixed' : 'absolute')};
  background-color: transparent;
  z-index: ${(props: StyledOverlayProps) =>
    props.isInnerOverlay || props.isFixedComponent ? ZINDEX : 'auto'}; /* auto to allow + icons etc to sit on top */
  display: flex;
  justify-content: space-around;

  ${InsertOverlay} {
    &.top {
      top: -5px;
      border-bottom-right-radius: 50px;
      border-bottom-left-radius: 50px;
    }

    &.bottom {
      bottom: -5px;
      border-top-right-radius: 50px;
      border-top-left-radius: 50px;
    }

    &.left {
      left: -3px;
      align-self: center;
      border-top-right-radius: 50px;
      border-bottom-right-radius: 50px;
    }

    &.right {
      right: -3px;
      align-self: center;
      border-top-left-radius: 50px;
      border-bottom-left-radius: 50px;
    }
  }
`;
