import * as React from 'react';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {DialogContent} from '@material-ui/core';
import {DialogContentProps} from '@material-ui/core/DialogContent';

export default styled(DialogContent)`
  ${renderThemeProperty('background', 'background')};
  p {
    ${renderThemeProperty('contentFontSize', 'font-size')};
    ${renderThemeProperty('contentColor', 'color')};
  }
`;
