import {Button, FormControl, InputLabel, MenuItem, Select, TextField} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import * as React from 'react';
import {AgeRatingsClass, UserSettings} from '../../store/settings/types';
import * as defaultProps from './defaultProps.json';
import {SettingsModal} from './SettingsModal';
import {NO_AGE_RESTRICTION} from '../../store/settings/reducer';
import {validateSettingsPassword} from './utils';

export interface SettingsProps {
  settingsPassword: string;
  selectedAgeRestriction: string;
  ageRestrictions: AgeRatingsClass[];
  modalNodeRef: HTMLElement;
  onSave: (settings: UserSettings) => void;
}

export interface SettingsState {
  selectedAgeRestriction: string;
  password: string;
  isPasswordRequired: boolean;
  isModalVisible: boolean;
  isFormValid: boolean;
  isNewPasswordRequired: boolean;
  passwordErrorMsg?: string;
}

export default class Settings extends React.Component<SettingsProps, SettingsState> {
  public static defaultProps = defaultProps;

  private getIntialState = (): SettingsState => ({
    isModalVisible: false,
    selectedAgeRestriction: this.props.selectedAgeRestriction || NO_AGE_RESTRICTION,
    isNewPasswordRequired: !this.props.settingsPassword,
    password: '',
    isPasswordRequired: false,
    isFormValid: false
  });

  public state: SettingsState = this.getIntialState();

  private handleToggleModal = () =>
    this.setState({
      ...this.getIntialState(),
      isModalVisible: !this.state.isModalVisible
    });

  // TODO inject props
  private isPasswordRequired = (selectedAgeRestriction: string) => {
    const hasChanged = selectedAgeRestriction !== this.props.selectedAgeRestriction;
    const hasChangedFromDefault = hasChanged && this.props.selectedAgeRestriction === NO_AGE_RESTRICTION;
    const passwordWasSetBefore = !!this.props.settingsPassword;

    if (hasChangedFromDefault && passwordWasSetBefore) {
      return false;
    }

    return hasChanged;
  };

  private handleRestrictionChange = (event: React.ChangeEvent<HTMLSelectElement>) =>
    this.setState({
      selectedAgeRestriction: event.target.value,
      isPasswordRequired: this.isPasswordRequired(event.target.value)
    });

  private handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newPassword = event.target.value.toString();

    this.setState({
      password: newPassword,
      passwordErrorMsg: validateSettingsPassword({
        isNewPasswordRequired: this.state.isNewPasswordRequired,
        password: newPassword,
        lockPassword: this.props.settingsPassword
      })
    });
  };

  private isFormValid = () => {
    const {isPasswordRequired, password, passwordErrorMsg, selectedAgeRestriction} = this.state;

    const isPasswordSetWithoutErrors = isPasswordRequired && password.length > 0 && !passwordErrorMsg;

    const isPasswordNotRequired = !isPasswordRequired;

    const ageRestrictionChanged = selectedAgeRestriction !== this.props.selectedAgeRestriction;

    return ageRestrictionChanged && (isPasswordSetWithoutErrors || isPasswordNotRequired);
  };

  private renderSettingsButton = () => (
    <Button
      aria-label="Settings"
      className="settings-button"
      onClick={this.handleToggleModal}
      children={<SettingsIcon />}
    />
  );

  private renderAgeRestrictionSelect = () => (
    <FormControl>
      <InputLabel htmlFor="restriction">Content restrictions</InputLabel>
      <Select
        id="restriction"
        name="restriction"
        className="age-select"
        value={this.state.selectedAgeRestriction}
        onChange={this.handleRestrictionChange}
      >
        {[{code: NO_AGE_RESTRICTION, label: NO_AGE_RESTRICTION}, ...this.props.ageRestrictions].map(ageClass => (
          <MenuItem style={{fontSize: '1.4rem'}} className="select-option" key={ageClass.code} value={ageClass.code}>
            {ageClass.label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );

  private renderPasswordInput = () => (
    <TextField
      className="password-input"
      name="password"
      type="text"
      label={this.state.isNewPasswordRequired ? 'Create password' : 'Password'}
      value={this.state.password}
      onChange={this.handlePasswordChange}
      error={!!this.state.passwordErrorMsg}
      helperText={this.state.passwordErrorMsg}
    />
  );

  private handleSave = (e: any) => {
    e.preventDefault();
    this.props.onSave({
      ageRatingClassSelected: this.state.selectedAgeRestriction,
      ageRatingPassword: this.state.password || this.props.settingsPassword
    });
  };

  render() {
    const {isModalVisible} = this.state;
    const {modalNodeRef} = this.props;

    return (
      <React.Fragment>
        {this.renderSettingsButton()}
        <SettingsModal
          isModalOpen={isModalVisible}
          toggleModal={this.handleToggleModal}
          mountRef={modalNodeRef}
          handleCtaClick={this.handleSave}
          isCtaEnabled={this.isFormValid()}
          ctaLabel="Save settings"
        >
          <form className="age-restriction-form" noValidate autoComplete="off" onSubmit={this.handleSave}>
            {this.renderAgeRestrictionSelect()}
            {this.state.isPasswordRequired && this.renderPasswordInput()}
          </form>
        </SettingsModal>
      </React.Fragment>
    );
  }
}
