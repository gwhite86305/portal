import * as React from 'react';
import {utils} from 'component-utils';
import {ServiceUnavailableModal as ExternalInterface} from '../common/ComponentsInterface';
import ServiceUnavailableModal, {InternalInterface} from './ServiceUnavailableModal';
import {localizeToProperties} from 'component-utils/dist/components/Localize';

import {withRouter} from 'react-router';
import {injectIntl} from 'react-intl';
const connectWrapper = utils.connectWrapper.default;
import {getServiceAvailabilityState} from '../../utils/ConnectionUtils';

export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {
  onBackToHomeButtonClick(): any;
  hideSwapDeviceModal(): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    const connected = getServiceAvailabilityState(state.device);
    return {connected};
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => {
    return {
      onBackToHomeButtonClick: () => {
        ownProps.history.push({pathname: '/'});
      },
      hideSwapDeviceModal: () => {
        ownProps.history.push({pathname: ownProps.location.pathname, search: ''});
      }
    };
  }
)
class ServiceUnavailableModalContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    const {intl, componentUid, feature} = this.props;
    const localizedProps = localizeToProperties(intl, componentUid, [
      {message: 'ifc.ServiceUnavailableModal.message'},
      {okButtonText: 'ifc.ServiceUnavailableModal.okButtonText'},
      {backToHomeButtonText: 'ifc.ServiceUnavailableModal.backToHomeButtonText'}
    ]);
    return <ServiceUnavailableModal {...this.props} {...localizedProps} />;
  }
}

export default injectIntl(withRouter(ServiceUnavailableModalContainer));
