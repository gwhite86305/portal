import {Action, ActionCreators} from './actions';

export type State = {
  readonly errorMessage: string | null;
};

export const initialState: State = {
  errorMessage: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;

  switch (action.type) {
    case ActionCreators.receiveProfile.type:
      if (action.payload.error) {
        partialState = {
          errorMessage: action.payload.error,
          ...action.payload
        };
      } else {
        partialState = {
          ...action.payload
        };
      }
      break;
    default:
      return state;
  }

  return {...state, ...partialState};
}
