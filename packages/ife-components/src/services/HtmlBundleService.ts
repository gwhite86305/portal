import {axiosWithBaseUrl as axios} from '../utils/utils';
import {includes} from 'lodash-es';

export interface HtmlBundleDto {
  uid: string;
  type: 'htmlBundle';
  title: string;
  htmlBundleUrl: string;
  categories?: string[];
}

export interface HtmlBundleResponseDto {
  items: HtmlBundleDto[];
}

export class HtmlBundleService {
  public getHtmlBundleByCategory = async (categoryName: string) => {
    // TODO - refactor axios to the API.ts class component.
    // TODO - pass generic type to the call so we dont need to cast response. API.get<HtmlBundleResponseDto>('/content/htmlbundles/')
    const {data} = await axios.get('/content/htmlbundles/');
    const {items} = data as HtmlBundleResponseDto;

    const result = items.filter(item => item.categories && includes(item.categories, categoryName))[0];

    return result;
  };
}

export const htmlBundleService = new HtmlBundleService();
