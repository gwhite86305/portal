const dompurify = require('dompurify')();
export const sanitize = (html: string) => {
  return {
    __html: dompurify.sanitize(html, {
      ALLOWED_TAGS: ['strong', 'a', 'br', 'p', 'img', 'span', 'h1', 'h2', 'ul', 'li'],
      ALLOWED_ATTR: ['href', 'target', 'style', 'lang', 'src', 'class']
    })
  };
};
