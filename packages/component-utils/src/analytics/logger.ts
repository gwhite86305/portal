import {ILogger} from './types';
import {v1 as uuid} from 'uuid';
import axios from 'axios';
import * as _ from 'lodash';

/**
 * A class to handle basic logging functions.
 */
class Logger implements ILogger {
  // A list of logger instances, designated by their LoggerName string
  static loggerInstances: {[id: string]: ILogger} = {};

  // A single guid to define a browser session.
  static browserGuid: string = uuid();

  // Log queueing at the class level,
  static logQueue: any[] = [];
  static nextSubmissionId: any | null = null;
  static queueDuration: number = 1000;

  // PFE endpoint with which this logger instance interacts.
  endpoint: string;

  // ID of any pending submission.  Serves as a semaphore.

  constructor(LoggerName: string, LoggerEndpoint: string, sessionId: string | null = null) {
    // Handle singleton-by-name functionality.
    let instance = Logger.loggerInstances[LoggerName];
    if (!instance) {
      // Initialize a new logger instance for return.
      this.endpoint = LoggerEndpoint;
      Logger.loggerInstances[LoggerName] = this;
      if (sessionId) {
        Logger.browserGuid = sessionId;
      }
      // Events to ensure log submission on tab/window closure.
      window.addEventListener('beforeunload', this.handleUnload);
      window.addEventListener('unload', this.handleUnload);
    }

    return Logger.loggerInstances[LoggerName];
  }

  /**
   * Queue a message for logging and kickoff a log submission timeout.
   * @param
   */
  log(log: any): void {
    // Add log to queue for submission
    Logger.logQueue.push({
      endpoint: this.endpoint,
      data: log
    });

    // If we don't have a log submission queued, queue one.
    if (Logger.nextSubmissionId === null) {
      Logger.nextSubmissionId = setTimeout(Logger.submitLogs, Logger.queueDuration);
    }
  }

  /**
   * Submit all queued logs to this loggers endpoint.
   * Return logs to queue if submission fails.
   */
  static submitLogs() {
    // Split log queue up by endpoint
    let logsByEndpoint = _.reduce(
      Logger.logQueue,
      (logsByEndpoint: any, log) => {
        if (log && logsByEndpoint) {
          if (!(log.endpoint in logsByEndpoint)) {
            logsByEndpoint[log.endpoint] = [];
          }
          logsByEndpoint[log.endpoint].push(log.data);
        }
        return logsByEndpoint;
      },
      {}
    );

    // Send each list of logs to their respective PFE endpoint.
    for (let endpoint in logsByEndpoint) {
      axios
        .post(endpoint, {
          browserSessionId: Logger.browserGuid,
          logs: logsByEndpoint[endpoint]
        })
        .then(
          () => {},
          response => {
            // In event of a failure, just stick those logs back in the queue.
            Logger.logQueue.push(...JSON.parse(response.config.data).logs);
          }
        );
    }

    // Clear queue and semaphore.
    Logger.logQueue = [];
    Logger.nextSubmissionId = null;
  }

  handleUnload() {
    // Remove all log submission events to prevent multiple-submission.
    window.removeEventListener('beforeunload', this.handleUnload);
    window.removeEventListener('unload', this.handleUnload);
    clearTimeout(Logger.nextSubmissionId);

    // Do the thing.
    Logger.submitLogs();
  }
}

export default Logger;
