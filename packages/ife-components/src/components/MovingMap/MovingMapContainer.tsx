import {utils, PortalComponent} from 'component-utils';
import * as React from 'react';
import {bearing, toLatitudeLongitude, formatTakeOffTime, cordToArray} from './utils';
import StyledContainer from './StyledContainer';
import {Coordinate, BoundsChangeEvent} from './types';
import {isEqual} from 'lodash';
import {MovingMap as MovingMapInterface} from '../common/ComponentsInterface';
import MovingMap from './MovingMap';
import {IRootState} from '../..';
import {loadMovingMap, followPlane, unFollowPlane} from '../../store/movingmap/actions';
import {MovingMapInfo} from './MovingMapInfo';
import {MovingMapControls} from './MovingMapControls';
import * as hasTouch from 'has-touch';
import {
  selectOriginPoint,
  selectDestinationPoint,
  selectAircraftId,
  selectOriginName,
  selectDestinationName,
  selectPosition,
  selectTrackedRoutePoints,
  selectAltitude,
  selectAirSpeed,
  selectProgress,
  selectTakeOffTime,
  selectTimeToDestInMinutes
} from '../../store/movingmap/selectors';

const connectWrapper = utils.connectWrapper.default;

export interface StateProps {
  originPoint: Coordinate;
  destinationPoint: Coordinate;
  aircraftId: string;
  originName: string;
  destinationName: string;
  position: Coordinate;
  trackedRoutePoints: Coordinate[];
  altitude: number;
  airSpeed: number;
  progress: number;
  takeOffTime: number;
  timeToDestInMinutes: number;
  error: boolean;
  loading: boolean;
  followingPlane: boolean;
  minZoom: number;
  maxZoom: number;
  mapUrl: string;
  firstLoad: boolean;
}

export interface DispatchProps {
  followPlane: () => void;
  unFollowPlane: () => void;
  loadMovingMap: () => void;
}

export interface OwnProps {}

export interface State {
  center: number[];
  zoom: number;
  animating: boolean;
}

export type Props = StateProps & DispatchProps & OwnProps & MovingMapInterface;

@connectWrapper(
  ({ifeMovingMap}: IRootState): StateProps => ({
    originPoint: selectOriginPoint(ifeMovingMap),
    destinationPoint: selectDestinationPoint(ifeMovingMap),
    aircraftId: selectAircraftId(ifeMovingMap),
    originName: selectOriginName(ifeMovingMap),
    destinationName: selectDestinationName(ifeMovingMap),
    position: selectPosition(ifeMovingMap),
    trackedRoutePoints: selectTrackedRoutePoints(ifeMovingMap),
    altitude: selectAltitude(ifeMovingMap),
    airSpeed: selectAirSpeed(ifeMovingMap),
    progress: selectProgress(ifeMovingMap),
    takeOffTime: selectTakeOffTime(ifeMovingMap),
    timeToDestInMinutes: selectTimeToDestInMinutes(ifeMovingMap),
    error: ifeMovingMap.error,
    loading: ifeMovingMap.loading,
    followingPlane: ifeMovingMap.followingPlane,
    minZoom: ifeMovingMap.map.minZoom,
    maxZoom: ifeMovingMap.map.maxZoom,
    mapUrl: ifeMovingMap.map.mapUrl,
    firstLoad: ifeMovingMap.firstLoad
  }),
  (dispatch: any): DispatchProps => ({
    followPlane: () => dispatch(followPlane()),
    unFollowPlane: () => dispatch(unFollowPlane()),
    loadMovingMap: () => dispatch(loadMovingMap())
  })
)
export default class MovingMapContainer extends React.Component<Props, State> {

  movingMapInterval: NodeJS.Timer | undefined = undefined;
  state = {
    center: [0, 0],
    zoom: 6,
    animating: false
  };

  componentDidMount() {
    this.props.loadMovingMap();
    this.movingMapInterval = setInterval(() => {
      if (!this.state.animating) {
        this.props.loadMovingMap();
      }
    }, 2000);
  }

  componentWillUnmount() {
    if (this.movingMapInterval) {
      clearInterval(this.movingMapInterval);
    }
  }

  componentWillUpdate(nextProps: Props) {
    return isEqual(this.props, nextProps);
  }

  componentDidUpdate(prevProps: Props) {
    if (!isEqual(prevProps, this.props)) {
      isEqual(this.state.center, [0, 0]) &&
        this.setState({
          center: cordToArray(this.props.originPoint)
        });
      this.props.followingPlane &&
        this.setState({
          center: cordToArray(this.props.position)
        });
    }
  }

  render() {
    const {
      firstLoad,
      componentTheme,
      maxZoom,
      minZoom,
      mapUrl,
      followingPlane,
      position,
      trackedRoutePoints,
      destinationPoint,
      originPoint,
      altitude,
      airSpeed,
      aircraftId,
      progress,
      takeOffTime,
      originName,
      destinationName,
      timeToDestInMinutes
    } = this.props;
    const {center, zoom} = this.state;
    return (
      !firstLoad && (
        <PortalComponent componentTheme={componentTheme}>
          <StyledContainer>
            <div className="moving-map-wrapper">
              <MovingMap
                zoom={zoom}
                maxZoom={maxZoom}
                minZoom={minZoom}
                center={center}
                mapUrl={mapUrl}
                originPoint={toLatitudeLongitude(originPoint)}
                destinationPoint={toLatitudeLongitude(destinationPoint)}
                rotation={bearing(position, trackedRoutePoints)}
                position={toLatitudeLongitude(position)}
                trackedRoutePoints={trackedRoutePoints.map((point: Coordinate) => toLatitudeLongitude(point))}
                animate={true}
                zoomSnap={true}
                mouseEvents={!hasTouch}
                touchEvents={hasTouch}
                onBoundsChanged={this.handleBoundsChange}
                onAnimationStart={this.handleAnimStart}
                onAnimationFinish={this.handleAnimStop}
              />
              <MovingMapInfo
                aircraftId={aircraftId}
                altitude={Math.round(altitude).toLocaleString(navigator.language)}
                airSpeed={Math.round(airSpeed).toLocaleString(navigator.language)}
                progress={progress * 100}
                takeOffTime={formatTakeOffTime(takeOffTime)}
                originName={originName}
                destinationName={destinationName}
                timeToDestInMinutes={timeToDestInMinutes}
              />
              <MovingMapControls
                onZoomIn={this.handleZoomIn}
                onZoomOut={this.handleZoomOut}
                followingPlane={followingPlane}
                onFollowPlane={this.handleFollowPlane}
              />
            </div>
          </StyledContainer>
        </PortalComponent>
      )
    );
  }

  handleFollowPlane = () => {
    if (this.props.followingPlane) {
      this.props.unFollowPlane();
    } else {
      this.props.followPlane();
      this.setState({
        center: cordToArray(this.props.flightProgress.position)
      });
    }
  };

  handleZoomIn = () => this.setState({zoom: Math.min(this.state.zoom + 1, this.props.maxZoom)});
  handleZoomOut = () => this.setState({zoom: Math.max(this.state.zoom - 1, this.props.minZoom)});
  handleBoundsChange = (change: BoundsChangeEvent) => this.setState({zoom: change.zoom, center: change.center});
  handleAnimStart = () => this.setState({animating: true});
  handleAnimStop = () => this.setState({animating: false});
}
