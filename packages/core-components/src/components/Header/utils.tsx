import {PropsInterface as HamburgerMenuInterface} from '../HamburgerMenu/HamburgerMenu';
import {InternalInterface as HorizontalMenuInterface} from '../HorizontalMenu/HorizontalMenu';
import {HorizontalMenu as HorizontalMenuExternalInterface} from '../common/ComponentsInterface';

// -- Utils
export const getHorizontalMenuProps = (feature: any, componentTheme: any) => {
  return {
    feature: {
      menuItems: (feature.menuItems || []).map((el: any) => ({...el, iconUrl: ''})),
      dividerEnabled: false
    },
    componentTheme: {
      ...componentTheme,
      backgroundColor: 'rgba(0,0,0,0)',
      display: componentTheme.horizontalMenuDisplay,
      margin: componentTheme.horizontalMenuMargin,
      padding: {xs: '12px 0 0 0'},
      menuListPadding: componentTheme.menuListPadding,
      justify: 'flex-end',
      spaceBetweenElement: componentTheme.horizontalMenuSpaceBetweenElement,
      dropdownMenuBackgroundColor: componentTheme.horizontalMenuDropdownBackgroundColor,
      dropdownMenuTextColor: componentTheme.horizontalMenuDropdownTextColor,
      dropdownMenuOffset: componentTheme.horizontalMenuDropdownMenuOffset,
      textColor: componentTheme.horizontalMenuTextColor,
      fontSize: componentTheme.horizontalMenuFontSize || componentTheme.fontSize
    }
  };
};
export const getHamburgerMenuProps = (feature: any, componentTheme: any) => {
  return {
    feature: feature,
    componentTheme: {
      ...componentTheme,
      backgroundColor: 'transparent',
      menuBackgroundColor: componentTheme.hamburgerMenuBackgroundColor,
      closeMenuBackgroundColor: componentTheme.hamburgerMenuCloseBackgroundColor,
      closeMenuIconHeight: componentTheme.hamburgerCloseMenuIconHeight,
      closeMenuIconWidth: componentTheme.hamburgerCloseMenuIconWidth,
      textColor: componentTheme.hamburgerMenuTextColor,
      menuItemSeperatorColor: componentTheme.hamburgerSeperatorColor,
      focusBorderColor: componentTheme.hamburgerFocusColor,
      display: componentTheme.hamburgerMenuDisplay,
      fontSize: componentTheme.hamburgerMenuFontSize || componentTheme.fontSize,
      hamburgerOnTheLeft: componentTheme.hamburgerOnTheLeft,
      hamburgerMenuHeight: componentTheme.hamburgerMenuHeight,
      hamburgerMenuHoverUnderline: componentTheme.hamburgerMenuHoverUnderline
    }
  };
};

// -- Interfaces
export type HorizontalMenuProps = HorizontalMenuInterface & HorizontalMenuExternalInterface;
export type HamburgerMenuProps = HamburgerMenuInterface;
