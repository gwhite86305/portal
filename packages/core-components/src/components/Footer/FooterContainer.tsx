import * as React from 'react';
import Footer from './Footer';
import {resolveMenuItemsLocale, withLocaleProps} from 'component-utils/dist/components/Localize';

const FooterContainer: React.FC<any> = (props: any) => {
  return <Footer {...props} localizedMenuItems={resolveMenuItemsLocale(props.intl, props.feature.menuItems)} />;
};

export default withLocaleProps(FooterContainer, [
  {localizedAltImageText: 'core.Footer.image.alttext'},
  {localizedCopyrightText: 'core.Footer.copyright.text'},
  {localizedlogoRegionText: 'core.Footer.logoRegion.text'},
  {localizedNotesSectionText: 'core.Footer.notesSection.text'}
]) as any;
