import * as React from 'react';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';

const ThumbSliderContainer = styled.div`
  ${renderThemeProperty('backgroundColor', 'background')};
  padding: 0 1rem;

  /***** MOBILE *****/
  /* card width */
  .card {
    clear: none;
    display: inline-block;
    float: none;
    line-height: 16px;
    overflow-x: visible;
    overflow-y: visible;
    position: relative;
    white-space: nowrap;
    width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '10%' : '6%')};

    @media screen and (max-width: 600px) {
      width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '56%' : '44%')};
    }
    @media screen and (min-width: 601px) and (max-width: 800px) {
      width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '34%' : '22%')};
    }
    @media screen and (min-width: 801px) and (max-width: 960px) {
      width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '31%' : '19%')};
    }
    @media screen and (min-width: 961px) and (max-width: 1280px) {
      width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '26%' : '16%')};
    }
    @media screen and (min-width: 1281px) and (max-width: 1920px) {
      width: ${({is1To1Ratio}: any) => (is1To1Ratio ? '16%' : '10%')};
    }
  }

  .poster {
    height: 100%;
    display: flex;
    & > button {
      width: 100%;
      position: absolute;
      height: 100%;
      top: 0;
      :hover {
        background: none;
      }
    }
  }

  .card > div:first-of-type {
    background-origin: padding-box;
    background-position: center;
    background-repeat: repeat;
    background-size: cover;
    cursor: pointer;
    display: block;
    display: block;
    margin-right: 1%;
    margin-right: 1%;
    overflow-wrap: normal;
    overflow-x: visible;
    overflow-y: visible;
    ${({is1To1Ratio}: any) => (is1To1Ratio ? 'padding: 50% 0;' : 'padding: 73.8% 0;')};
    white-space: nowrap;
    width: 98%;
    word-break: normal;
    word-spacing: 0px;
  }

  .single-row {
    position: relative;
    ${props => applyMediaQueriesForStyle('padding', props.theme.componentTheme.carouselPadding, props.theme)};
  }

  .single-row--desktop {
    padding: 0 4rem;
    > .slider {
      padding: 0 0.4rem;
    }
  }
  .empty-title {
    height: 20px;
    width: 100px;
  }

  .row-header {
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.categoryTitlePadding, theme)};
    ${renderThemeProperty('categoryTitleFontWeight', 'font-weight')};
    ${renderThemeProperty('categoryTitleColor', 'color')};
    ${renderThemeProperty('categoryTitleSize', 'font-size')};
  }

  .thumbnail-subtitle,
  .thumbnail-title {
    display: flex;
    white-space: normal;
  }

  .thumbnail-title {
    ${renderThemeProperty('itemTitleFontWeight', 'font-weight')};
    ${renderThemeProperty('itemTitleColor', 'color')};
    ${renderThemeProperty('itemTitleFontSize', 'font-size')};
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.itemTitlePadding, theme)};
  }

  .thumbnail-subtitle {
    ${renderThemeProperty('itemSubtitleFontWeight', 'font-weight')};
    ${renderThemeProperty('itemSubtitleColor', 'color')};
    ${renderThemeProperty('itemSubtitleFontSize', 'font-size')};
    ${({theme}) => applyMediaQueriesForStyle('padding', theme.componentTheme.itemSubtitlePadding, theme)};
  }

  .row-content {
    -webkit-overflow-scrolling: touch;
    overflow: -moz-scrollbars-none;
    overflow-wrap: normal;
    overflow-x: auto;
    overflow-y: hidden;
    padding-right: 30px;
    white-space: nowrap;
  }

  /***** DESKTOP *****/
  .slider-control-bottomcenter  {
    display: none;
  }
  .slider-control-centerright,
  .slider-control-centerleft {
    cursor: pointer;
    display: flex;
    width: 50px;
    height: 100%;
    padding: 2px;
    pointer-events: none;
    & > button {
      background: rgba(0, 0, 0, 0.3);
      border-radius: 0px;
      min-width: initial;
      ${({thumbnailHeight}: any) => `height: ${thumbnailHeight}px`};
      margin: 0 auto;
      opacity: 0;
      pointer-events: none;
      transition: 200ms ease-in-out;
      &.active-control {
        pointer-events: initial;
        opacity: 1;
      }

      & > span > svg {
        font-size: 4.8rem;
        ${renderThemeProperty('carouselButtonColor', 'fill')};
      }
    }
  }

  .slider-control-centerright {
    & > button {
      right: 2px;
    }
  }
  .slider-control-centerleft {
    & > button {
      left: 2px;
    }
  }
  .slider {
    height: auto !important;
  }
  .slider-frame {
    height: 100% !important;
  }
  .slider-control-centerleft button,
  .slider-control-centerright button {
    align-items: center;
    display: flex;
    width: 100%;
  }

  svg {
    fill: rgba(255, 255, 255, 0.4);
  }

  .image-link__image {
    cursor: pointer;
  }
  .pulsate {
    animation-duration: 3.6s;
    animation-iteration-count: infinite;
    animation-name: pulsateAnimation;
    animation-timing-function: ease-in-out;
    height: 100%;
    width: 100%;
  }

  @keyframes pulsateAnimation {
    0% {
      background-color: #1a1a1a;
    }
    25% {
      background-color: #333;
    }
    50% {
      background-color: #1a1a1a;
    }
    100% {
      background-color: #1a1a1a;
    }
  }
`;

export default ThumbSliderContainer;
