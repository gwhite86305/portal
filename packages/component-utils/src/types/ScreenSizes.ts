// https://material-ui.com/layout/breakpoints

export interface ScreenSizes {
  [key: string]: number;
}

export enum ScreenSize {
  xs = 'xs',
  sm = 'sm',
  md = 'md',
  lg = 'lg',
  xl = 'xl'
}
