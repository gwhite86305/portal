import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {Statusbar as ExternalInterface} from '../common/ComponentsInterface';
import Statusbar, {InternalInterface} from './Statusbar';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {getServiceAvailabilityState} from '../../utils/ConnectionUtils';
export type PageProps = DispatchProps & ExternalInterface & InternalInterface & {actions: any};

export interface DispatchProps {}

@connectWrapper((state: any) => {
  if (state.device) {
    const serviceAvailability = getServiceAvailabilityState(state.device);
    return {
      ...state.device,
      flightMinutesRemaining:
        state.device.wowState == 'On'
          ? undefined
          : state.device.flightMinutesRemaining > 1439 ? 1439 : state.device.flightMinutesRemaining,
      flightOrigin: state.profile && state.profile.locations && state.profile.locations.originCode,
      flightDestination: state.profile && state.profile.locations && state.profile.locations.destinationCode,
      isOffline: serviceAvailability !== undefined && serviceAvailability === false
    };
  }
})
class StatusbarContainer extends React.Component<PageProps, any> {
  public render() {
    return <Statusbar {...this.props} />;
  }
}

export default withLocaleProps(StatusbarContainer, [
  {localizedConnectivityUpText: 'ifc.Statusbar.wifiConnectivityUpText'},
  {localizedConnectivityDownText: 'ifc.Statusbar.wifiConnectivityDownText'},
  {localizedConnectivityUpAltText: 'ifc.Statusbar.wifiConnectivityUpAltText'},
  {localizedConnectivityDownAltText: 'ifc.Statusbar.wifiConnectivityDownAltText'}
]) as any;
