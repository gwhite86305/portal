import * as React from 'react';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {MenuItem, QuickLinks as ExternalInterface} from '../common/ComponentsInterface';
import NavLink from '../common/NavLink';
import {injectIntl} from 'react-intl';
import * as defaults from './defaultProps.json';

interface InternalInterface {
  intl: any;
}

// -- Styled Components
const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  ${renderThemeProperty('backgroundColor', 'background')};
  ${renderThemeProperty('justify', 'justify-content')};
`;

const StyledDiv = styled.div`
  display: flex;

  a.hover_enabled {
    img:nth-child(2) {
      display: none;
    }
  }
  a.hover_enabled:hover {
    img:nth-child(1) {
      display: none;
    }
    img:nth-child(2) {
      display: inherit;
    }
  }

  padding-bottom: ${(props: any) => props.theme.gutter};
  ${(props: any) => {
    return `flex-basis: ${100 / props.itemsperrow}%;`;
  }};
  ${(props: any) => {
    return `height: ${props.height || 'auto'};`;
  }};
` as any;

const StyledNavLink = styled(NavLink)`
  margin: 0 auto;

  img {
    margin-left: auto;
    margin-right: auto;
  }
` as any;

class QuickLinks extends React.Component<ExternalInterface & InternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {componentTheme, feature, intl} = this.props;
    const menuItems: MenuItem[] = feature.menuItems || [];
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledContainer>
          {menuItems.map((link: MenuItem, index: number) => (
            <StyledDiv key={index} itemsperrow={feature.itemsPerRow} height={link.iconHeight}>
              <StyledNavLink to={link.url} className={link.hoverIconUrl ? 'hover_enabled' : ''}>
                <img
                  src={link.iconUrl}
                  height={link.iconHeight}
                  alt={link.labels[this.props.intl.locale] ? link.labels[this.props.intl.locale].iconAltText : ''}
                  aria-label={link.labels[this.props.intl.locale] ? link.labels[this.props.intl.locale].label : ''}
                />
                {link.hoverIconUrl ? (
                  <img
                    src={link.hoverIconUrl}
                    height={link.iconHeight}
                    alt={link.labels[this.props.intl.locale] ? link.labels[this.props.intl.locale].iconAltText : ''}
                    aria-label={link.labels[this.props.intl.locale] ? link.labels[this.props.intl.locale].label : ''}
                  />
                ) : null}
              </StyledNavLink>
            </StyledDiv>
          ))}
        </StyledContainer>
      </PortalComponent>
    );
  }
}

const QuickLinksIntl = injectIntl(QuickLinks as any);
QuickLinksIntl.defaultProps = QuickLinks.defaultProps;
export default QuickLinksIntl;
