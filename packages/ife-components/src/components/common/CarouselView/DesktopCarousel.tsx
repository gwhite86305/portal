import {Button} from '@material-ui/core';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Carousel from 'nuka-carousel';
import * as React from 'react';
import {AUDIO_TYPE} from '../../../store/audio/types';
import {MediaThumbnail} from '../MediaThumbnail/MediaThumbnail';
import {getOptimizedImgSrc} from '../utils';
import Pulsate from './Pulsate';
import {StyledRowHeader} from './StyledRowHeader';
import {DesktopCarouselProps} from './types';

const isPrevActive = (currentSlide: number): boolean => currentSlide !== 0;
const isNextActive = (currentSlide: number, slideCount: number, slidesToScroll: number): boolean =>
  slidesToScroll + currentSlide < slideCount;

export const leftCtlr = ({previousSlide, currentSlide}: any) => (
  <Button
    aria-label="See previous titles"
    disabled={!isPrevActive(currentSlide)}
    className={isPrevActive(currentSlide) ? 'active-control' : ''}
    onClick={previousSlide}
    children={<ChevronLeft />}
  />
);
export const rightCtlr = ({nextSlide, currentSlide, slideCount, slidesToScroll}: any) => (
  <Button
    aria-label="See more titles"
    disabled={!isNextActive(currentSlide, slideCount, slidesToScroll)}
    className={isNextActive(currentSlide, slideCount, slidesToScroll) ? 'active-control' : ''}
    onClick={nextSlide}
    children={<ChevronRight />}
  />
);

const DesktopCarousel: React.FC<DesktopCarouselProps> = ({
  categoryName,
  onItemInViewPort,
  items,
  onClick,
  thumbSize,
  cellSpacing,
  imageVariant
}) => (
  <div className="single-row single-row--desktop">
    <StyledRowHeader className="row-header">
      {categoryName || <div className="empty-title" children={<Pulsate />} />}
    </StyledRowHeader>
    <Carousel
      edgeEasing={'easeCircleOut'}
      dragging={false}
      renderCenterLeftControls={leftCtlr}
      renderCenterRightControls={rightCtlr}
      cellSpacing={cellSpacing}
      slideWidth={thumbSize.WIDTH + 'px'}
      slidesToScroll={'auto'}
    >
      {items.map(item => (
        <div key={item.uid} className="item-container">
          <div className="item" style={{width: thumbSize.WIDTH + 'px', height: thumbSize.HEIGHT + 'px'}}>
            {item.titleImages && (
              <MediaThumbnail
                className="poster"
                inVieport={onItemInViewPort}
                onClick={() => onClick && onClick(item.uid)}
                src={getOptimizedImgSrc({
                  images: item.titleImages[0],
                  variant: imageVariant,
                  imageWidth: thumbSize.WIDTH
                })}
                item={item}
                clickable={true}
              />
            )}
          </div>
          <span className="thumbnail-title">{item.title}</span>
          {item.type === AUDIO_TYPE.GROUP && <span className="thumbnail-subtitle">{item.artist}</span>}
        </div>
      ))}
    </Carousel>
  </div>
);

export default DesktopCarousel;
