import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, text, object} from '@storybook/addon-knobs/react';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';
import Checkout from '../dist/components/Checkout/Checkout.js';

const defaultLocaleProps = {
  voucherHeader: 'Use a voucher',
  voucherBodyText:
    'To connect to the on-board Wi-Fi system, we ask you to enter your VOUCHER code number and your e-mail address.',
  voucherInputLabel: 'Voucher code',
  voucherInputErrorHelpText: '',
  voucherEmailLabel: 'Email',
  voucherEmailErrorHelpText: '',
  voucherNoteText: 'The Wi-Fi service can be used at any time if the network is available.',
  voucherSubmitButtonText: 'Apply Voucher',

  purchaseBodyText:
    'To buy the selected service and access the onboard Wi-Fi, we ask you to choose the payment method you prefer',
  purchaseEmailLabel: 'Email',
  purchaseEmailErrorHelpText: '',
  purchaseNoteText: 'The Wi-Fi service can be used at any time if the network is available.',
  purchaseAltTextForNoteImage: '',
  purchaseTotalLabel: 'Total',
  purchaseSubmitButtonText: 'CONTINUE'
};

const defaultProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',

    voucherHeaderColor: 'theme.primaryTextColor',
    voucherHeaderFontSize: 'theme.quinaryFontSize',
    voucherBodyTextColor: 'theme.primaryTextColor',
    voucherBodyTextFontSize: 'theme.primaryFontSize',
    voucherInputLabelColor: 'theme.primaryTextColor',
    voucherInputLabelFontSize: 'theme.primaryFontSize',
    voucherNoteColor: 'theme.primaryTextColor',
    voucherNoteFontSize: 'theme.primaryFontSize',

    purchaseOptionHeaderColor: 'theme.primaryTextColor',
    purchaseOptionHeaderFontSize: 'theme.quinaryFontSize',
    purchaseOptionBodyTextColor: 'theme.primaryTextColor',
    purchaseOptionBodyTextFontSize: 'theme.primaryFontSize',
    purchaseOptionInputLabelColor: 'theme.primaryTextColor',
    purchaseOptionInputLabelFontSize: 'theme.primaryFontSize',
    purchaseOptionNoteColor: 'theme.primaryTextColor',
    purchaseOptionNoteFontSize: 'theme.primaryFontSize',
    subTotalLabelColor: 'theme.primaryTextColor',
    subTotalLabelFontSize: 'theme.tertiaryFontSize',
    subTotalCostColor: 'theme.primaryTextColor',
    subTotalCostFontSize: 'theme.tertiaryFontSize',
    subTotalLineColor: 'theme.senaryBackgroundColor'
  },
  feature: {
    openPageWhenConnected: ''
  }
};

const neosLocaleProps = {
  voucherHeader: 'Utilizza il voucher',
  voucherBodyText:
    "Per connetteri al sistema Wi-fi di bordo ti chiediamo di inserire il numero del tuo codice VOUCHER e il tuo indirizzo email <br /> <br/> Informazioni del <a href='/Privacy_Policy'> servizio Wi-Fi</a>",
  voucherInputLabel: 'Codice voucher',
  voucherInputErrorHelpText: '',
  voucherEmailLabel: 'Indirizzo email',
  voucherEmailErrorHelpText: '',
  voucherNoteText:
    'Il servizio Wi-Fi e utiliazzabile in qualsiasi momento se le rete e disponibile. Decidi tu quando connetterti perdere i minuti che hai acquistato.',
  voucherSubmitButtonText: 'Apply Voucher',

  purchaseBodyText:
    'Per acuistare il servizio selezionato ed accedere al Wi-Fi di bordo, ti chiediamo di scegliere il metodo di pagamento che preferisci in modo rapido e sicuro. <br /> <br/> Informazioni del <a href="/Privacy_Policy"> servizio Wi-Fi</a>',
  purchaseEmailLabel: 'Indirizzo email',
  purchaseEmailErrorHelpText: '',
  purchaseNoteText:
    'Il servizio Wi-Fi e utiliazzabile in qualsiasi momento se le rete e disponibile. Decidi tu quando connetterti perdere i minuti che hai acquistato.',
  purchaseAltTextForNoteImage: '',
  purchaseTotalLabel: 'Total',
  purchaseSubmitButtonText: 'CONTINUE'
};
const propsNeos = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    voucherHeaderColor: 'theme.primaryTextColor',
    voucherHeaderFontSize: 'theme.quinaryFontSize',
    voucherBodyTextColor: 'theme.primaryTextColor',
    voucherBodyTextFontSize: 'theme.primaryFontSize',
    voucherInputLabelColor: 'theme.primaryTextColor',
    voucherInputLabelFontSize: 'theme.primaryFontSize',
    voucherNoteColor: 'theme.primaryTextColor',
    voucherNoteFontSize: 'theme.primaryFontSize',

    purchaseOptionHeaderColor: 'theme.primaryTextColor',
    purchaseOptionHeaderFontSize: 'theme.quinaryFontSize',
    purchaseOptionBodyTextColor: 'theme.primaryTextColor',
    purchaseOptionBodyTextFontSize: 'theme.primaryFontSize',
    purchaseOptionInputLabelColor: 'theme.primaryTextColor',
    purchaseOptionInputLabelFontSize: 'theme.primaryFontSize',
    purchaseOptionNoteColor: 'theme.primaryTextColor',
    purchaseOptionNoteFontSize: 'theme.primaryFontSize',
    subTotalLabelColor: 'theme.primaryTextColor',
    subTotalLabelFontSize: 'theme.tertiaryFontSize',
    subTotalCostColor: 'theme.primaryTextColor',
    subTotalCostFontSize: 'theme.tertiaryFontSize',
    subTotalLineColor: '#BDBDBD'
  },
  feature: {
    noteImage: {
      xs: './images/purchasenote-mobile.svg',
      sm: './images/purchasenote-tablet.svg'
    },
    openPageWhenConnected: ''
  }
};

const WifiServiceOptionsVoucher = {
  errorMessage: {},
  selected: {
    errorMessage: {},
    method: 'voucher'
  }
};
const WifiServiceOptionsPurchase = {
  errorMessage: {},
  selected: {
    tierId: 72,
    errorMessage: {},
    method: 'purchase'
  }
};

const premiumTiers = [
  {
    description: '1-Hour Pass',
    customerId: 5,
    active: true,
    deviceTypeMask: 0,
    deviceCategoryId: 'PED',
    id: 72,
    cost: 12.0,
    name: 'Aa PED LH Premium Plan (1-hour)',
    npiCodeId: 'AaPedPremium',
    autoConnect: false,
    alternateCost: null,
    metadata: {
      assetKeySuffix: 'Hour',
      description: 'Only need Wi-Fi for a short time? Purchase our One-Hour Access plan for a quick dip in the clouds.'
    },
    duration: 3600
  },
  {
    description: 'Flight Pass',
    customerId: 5,
    active: true,
    deviceTypeMask: 0,
    deviceCategoryId: 'PED',
    id: 73,
    cost: 16.0,
    name: 'Aa PED LH Premium Plan (full-flight)',
    npiCodeId: 'AaPedPremium',
    autoConnect: false,
    alternateCost: null,
    metadata: {
      assetKeySuffix: 'Full',
      description: 'Have a long flight? Purchase our Flight-Time Package to stay connected the entire flight.'
    },
    duration: 84600
  }
];
const stories = storiesOf('Checkout Component (CMOB-20560)', module);
stories.addDecorator(withKnobs);

stories
  .add('Default Demo( Voucher Option)', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Checkout
        {...defaultProps}
        {...defaultLocaleProps}
        premiumTiers={premiumTiers}
        WifiServiceOptions={WifiServiceOptionsVoucher}
      />
    </PortalComponentWrapper>
  ))

  .add('Neos portal (Voucher Option)', () => (
    <PortalComponentWrapper mockTheme={mockThemeNeos}>
      <Checkout
        {...propsNeos}
        {...neosLocaleProps}
        premiumTiers={premiumTiers}
        WifiServiceOptions={WifiServiceOptionsVoucher}
      />
    </PortalComponentWrapper>
  ))

  .add('Default Demo portal ( Purchase option)', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Checkout
        {...defaultProps}
        {...defaultLocaleProps}
        premiumTiers={premiumTiers}
        WifiServiceOptions={WifiServiceOptionsPurchase}
      />
    </PortalComponentWrapper>
  ))

  .add('Neos portal (Purchase option)', () => (
    <PortalComponentWrapper mockTheme={mockThemeNeos}>
      <Checkout
        {...propsNeos}
        {...neosLocaleProps}
        premiumTiers={premiumTiers}
        WifiServiceOptions={WifiServiceOptionsPurchase}
      />
    </PortalComponentWrapper>
  ))

  .add('with Knob (Voucher Option) ', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <Checkout
          {...defaultProps}
          voucherHeader={text('voucherHeader', defaultLocaleProps.header)}
          voucherBodyText={text('voucherBodyText', defaultLocaleProps.voucherBodyText)}
          voucherInputLabel={text('voucherInputLabel', defaultLocaleProps.voucherInputLabel)}
          voucherInputErrorHelpText={text('voucherInputErrorHelpText', defaultLocaleProps.voucherInputErrorHelpText)}
          voucherEmailLabel={text('voucherEmailLabel', defaultLocaleProps.voucherEmailLabel)}
          voucherEmailErrorHelpText={text('voucherEmailErrorHelpText', defaultLocaleProps.voucherEmailErrorHelpText)}
          voucherNoteText={text('voucherNoteText', defaultLocaleProps.voucherNoteText)}
          voucherSubmitButtonText={text('voucherSubmitButtonText', defaultLocaleProps.voucherSubmitButtonText)}
          purchaseBodyText={text('purchaseBodyText', defaultLocaleProps.purchaseBodyText)}
          purchaseEmailLabel={text('purchaseEmailLabel', defaultLocaleProps.purchaseEmailLabel)}
          purchaseEmailErrorHelpText={text('purchaseEmailErrorHelpText', defaultLocaleProps.purchaseEmailErrorHelpText)}
          purchaseNoteText={text('purchaseNoteText', defaultLocaleProps.purchaseNoteText)}
          purchaseAltTextForNoteImage={text(
            'purchaseAltTextForNoteImage',
            defaultLocaleProps.purchaseAltTextForNoteImage
          )}
          purchaseTotalLabel={text('purchaseTotalLabel', defaultLocaleProps.purchaseTotalLabel)}
          purchaseSubmitButtonText={text('purchaseSubmitButtonText', defaultLocaleProps.purchaseSubmitButtonText)}
          feature={object('feature', defaultProps.feature)}
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          premiumTiers={object('internal-premiumTiers', premiumTiers)}
          WifiServiceOptions={object('Internal-WifiServiceOptions', WifiServiceOptionsVoucher)}
        />
      </PortalComponentWrapper>
    );
  })
  .add('with Knob (Purchase Option)', () => {
    return (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <Checkout
          {...defaultProps}
          voucherHeader={text('voucherHeader', defaultLocaleProps.header)}
          voucherBodyText={text('voucherBodyText', defaultLocaleProps.voucherBodyText)}
          voucherInputLabel={text('voucherInputLabel', defaultLocaleProps.voucherInputLabel)}
          voucherInputErrorHelpText={text('voucherInputErrorHelpText', defaultLocaleProps.voucherInputErrorHelpText)}
          voucherEmailLabel={text('voucherEmailLabel', defaultLocaleProps.voucherEmailLabel)}
          voucherEmailErrorHelpText={text('voucherEmailErrorHelpText', defaultLocaleProps.voucherEmailErrorHelpText)}
          voucherNoteText={text('voucherNoteText', defaultLocaleProps.voucherNoteText)}
          voucherSubmitButtonText={text('voucherSubmitButtonText', defaultLocaleProps.voucherSubmitButtonText)}
          purchaseBodyText={text('purchaseBodyText', defaultLocaleProps.purchaseBodyText)}
          purchaseEmailLabel={text('purchaseEmailLabel', defaultLocaleProps.purchaseEmailLabel)}
          purchaseEmailErrorHelpText={text('purchaseEmailErrorHelpText', defaultLocaleProps.purchaseEmailErrorHelpText)}
          purchaseNoteText={text('purchaseNoteText', defaultLocaleProps.purchaseNoteText)}
          purchaseAltTextForNoteImage={text(
            'purchaseAltTextForNoteImage',
            defaultLocaleProps.purchaseAltTextForNoteImage
          )}
          purchaseTotalLabel={text('purchaseTotalLabel', defaultLocaleProps.purchaseTotalLabel)}
          purchaseSubmitButtonText={text('purchaseSubmitButtonText', defaultLocaleProps.purchaseSubmitButtonText)}
          feature={object('feature', defaultProps.feature)}
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          premiumTiers={object('internal-premiumTiers', premiumTiers)}
          WifiServiceOptions={object('Internal-WifiServiceOptions', WifiServiceOptionsPurchase)}
        />
      </PortalComponentWrapper>
    );
  });
