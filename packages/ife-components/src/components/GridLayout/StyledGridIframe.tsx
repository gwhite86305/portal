import styled from 'styled-components';

export interface StyledGridIframeProps {}

export default styled.iframe<StyledGridIframeProps>`
  width: 100%;
  height: 100%;
`;
