import {screenshot_test, delay, handleFailures} from './utils';
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const fs = require('fs');

describe('NEOS VISUAL REGRESSION TEST', () => {
  let originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 8000000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it(`LAPTOP`, async () => {
    const path = 'test/screenshot/laptop/neos/';

    let results = [];
    const browserConfig = {
      headless: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--disable-gpu',
        '--disable-accelerated-2d-canvas',
        "--proxy-server='direct://'",
        '--proxy-bypass-list=*'
      ]
    };

    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();

    await page.setViewport({width: 1000, height: 1000});
    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});

    await delay(10000);
    //USER AGREEMENT PAGE
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click('.footer-additonal-checkbox'); // Second checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button
    await delay(10000);
    // LANDING PAGE
    results.push(await screenshot_test(page, `${path}home`));
    const buyNowButtons = await page.$x("//button[contains(text(), 'Connetti')]"); // Voucher Option
    if (buyNowButtons.length < 1) {
      throw new Error('Voucher Button not found');
    }
    await buyNowButtons[0].click();
    // COVERAGE MAP PAGE
    await delay(10000);
    results.push(await screenshot_test(page, `${path}coverage_map`));
    await page.click('.core-button-component'); // Continue button
    await delay(10000);
    results.push(await screenshot_test(page, `${path}voucher`));
    await page.click('.core-button-component'); // Voucher submit button
    await delay(10000);
    results.push(await screenshot_test(page, `${path}voucher_validation`));
    // SESSION STATUS
    const headerMenu = await page.$x("//a[contains(@class, 'menu-item')]");
    if (headerMenu.length < 3) {
      throw new Error('Header menu item missing');
    }
    await headerMenu[0].click(); // Session menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}session_status`));
    // SWAP DEVICE PAGE

    await headerMenu[1].click(); // Device Swap menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}device_swap`));
    const swapPageButtons = await page.$x("//button[contains(@class, 'core-button-component')]");
    await swapPageButtons[1].click(); // Cancel button
    // COVERAGE MAP2 PAGE
    await headerMenu[2].click(); // Coverage Map Page menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}coverage_map2`));

    // FAQ PAGE
    await headerMenu[3].click(); // FAQ menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}faq`));

    // TERMS PAGE
    await headerMenu[4].click(); // TERMS menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}terms`));

    // Footer Link
    const footerLinks = await page.$x("//div[contains(@class, 'footer-links')]/a");
    if (footerLinks.length < 2) {
      throw new Error('Footer Menu missing');
    }

    // TERM PAGE
    await footerLinks[0].click(); // Term link
    await delay(10000);
    results.push(await screenshot_test(page, `${path}terms`));

    await browser.close();

    handleFailures('NEOS', 'LAPTOP', results);
  });

  it(`MOBILE`, async () => {
    const path = 'test/screenshot/mobile/neos/';
    let results = [];
    const browserConfig = {
      headless: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        "--proxy-server='direct://'",
        '--proxy-bypass-list=*'
      ]
    };
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    await page.emulate(devices['iPhone 6']);

    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    //USER AGREEMENT PAGE
    await delay(10000);
    results.push(await screenshot_test(page, `${path}user_agreement`));
    await page.click('.footer-text'); //First checkbox
    await page.click('.footer-additonal-checkbox'); // Second checkbox
    await page.click("button[data-component-guid='HASH_USER_AGREEMENT']"); // Agree button

    // LANDING PAGE
    await delay(10000);
    results.push(await screenshot_test(page, `${path}home`));
    const buyNowButtons = await page.$x("//button[contains(text(), 'Connetti')]"); // Voucher Option
    if (buyNowButtons.length < 1) {
      throw new Error('Voucher Button not found');
    }
    await buyNowButtons[0].click();
    // COVERAGE MAP PAGE
    await delay(10000);

    results.push(await screenshot_test(page, `${path}coverage_map`));
    await page.click('.core-button-component'); // Continue button
    // VOUCHER SUBMIT FORM
    await delay(10000);
    results.push(await screenshot_test(page, `${path}voucher`));
    await page.click('.core-button-component'); // Voucher submit button
    await delay(10000);
    results.push(await screenshot_test(page, `${path}voucher_validation`));

    // Hamburger button
    const hamburgeButton = await page.$x("//div[contains(@class, 'bm-burger-button')]/button");
    if (hamburgeButton.length == 0) {
      throw new Error('Hamburger button is missing');
    }
    await hamburgeButton[0].click();
    await delay(10000);
    results.push(await screenshot_test(page, `${path}hamburger_menu`));

    let burgerMenu = await page.$x("//a[contains(@class, 'bm-item')]");
    if (burgerMenu.length < 4) {
      throw new Error('Burger menu item is missing');
    }
    await burgerMenu[0].click(); // Session menu
    // SESSION STATUS
    await delay(10000);
    results.push(await screenshot_test(page, `${path}session_status`));
    // SWAP DEVICE PAGE
    await hamburgeButton[0].click();
    await delay(10000);
    burgerMenu = await page.$x("//a[contains(@class, 'bm-item')]");
    await burgerMenu[1].click(); // Device Swap menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}device_swap`));
    const swapPageButtons = await page.$x("//button[contains(@class, 'core-button-component')]");
    await swapPageButtons[1].click(); // Cancel button

    // COVERAGE MAP2 PAGE
    await hamburgeButton[0].click();
    await delay(10000);
    burgerMenu = await page.$x("//a[contains(@class, 'bm-item')]");

    await burgerMenu[2].click(); // Device Swap menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}coverage_map2`));

    // FAQ PAGE
    await hamburgeButton[0].click();
    await delay(10000);
    burgerMenu = await page.$x("//a[contains(@class, 'bm-item')]");

    await burgerMenu[3].click(); // FAQ menu
    await delay(10000);
    results.push(await screenshot_test(page, `${path}faq`));

    // Footer Link
    const footerLinks = await page.$x("//div[contains(@class, 'footer-links')]/a");
    if (footerLinks.length < 2) {
      throw new Error('Footer Menu missing');
    }
    // TERM PAGE
    await footerLinks[0].click(); // Term link
    results.push(await screenshot_test(page, `${path}terms`));

    await browser.close();
    handleFailures('NEOS', 'MOBILE', results);
  });
});
