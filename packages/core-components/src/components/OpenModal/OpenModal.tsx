import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import { OpenModal as ExternalInterface } from '../common/ComponentsInterface';
import { ShowModal } from '../../store/Modal/actions';
export type PageProps = ExternalInterface & InternalInterface;
import { PortalComponent,utils } from 'component-utils';
import styled from 'styled-components';
const { CommonComponentThemeWrapper } = utils.ComponentUtils;
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  onClick?: any;
  feature?:any
}
const Container = styled(CommonComponentThemeWrapper)`
  display:flex;
  cursor:pointer;
`;
export interface DispatchProps {
  onClick(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => {
    return {
    }
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onClick: () => {
      dispatch(ShowModal(ownProps.feature.modalId));
    }
  })
)
class OpenModal extends React.Component<PageProps, any> {
  public static defaultProps = defaults;
  public render() {
    return (
    <PortalComponent componentTheme={this.props.componentTheme}>
        <Container className="OpenModal-component" onClick={this.props.onClick}> {this.props.children} </Container>
    </PortalComponent>
    );
  }
}

export default OpenModal;
