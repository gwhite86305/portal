export interface InternalErrorAPIMessage {
  code: number;
  message: string;
}

export interface ErrorAPIMessage {
  friendlyMessage: string;
  internalError: InternalErrorAPIMessage | undefined;
  status: string | undefined;
}

export function generateMessage(friendlyMessage: string, error: any) {
  console.error(
    'API call failed with status: ',
    error.response ? error.response.status : error.message ? error.message : 'unknown'
  );

  let errors = error.response && error.response.data ? error.response.data.errors : null;
  let internalError = errors && errors.length ? errors[0] : null;

  if (!internalError) {
    internalError = error.message ? {message: error.message, code: 0} : null;
  }

  const errorMessage: ErrorAPIMessage = {
    friendlyMessage,
    internalError,
    status: error.response ? error.response.status : undefined
  };

  return {error: errorMessage};
}

export function generateErrorState(payload: any) {
  if (payload.error) {
    return {
      error: payload.error,
      isFetching: false
    };
  }
}
