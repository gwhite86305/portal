import {TYPES, VideoItem, VideoGroup, LocalizedCategory, VideoConfig} from './types';
import {IRootState} from '../..';

export const selectSeriesCategories = (state: IRootState): LocalizedCategory[] => state.ifeVideos.seriesCategories;

export const selectMovies = (state: IRootState): VideoItem[] =>
  state.ifeVideos.items.filter(el => el.type === TYPES.VIDEO_ITEM) as VideoItem[];

export const selectSeries = (state: IRootState): VideoGroup[] =>
  state.ifeVideos.items.filter(el => el.type === TYPES.VIDEO_GROUP) as VideoGroup[];

export const selectVideoConfig = (state: IRootState): Partial<VideoConfig> => {
  const videoId = location.href.includes('videoId=') && location.href.split('videoId=')[1].split('&')[0];
  if (!videoId || !state.ifeVideos.videosConfig.allIds.find((x: string) => x === videoId)) {
    return {};
  }
  return state.ifeVideos.videosConfig.byId[videoId];
};

export const selectVideoWatchTime = (state: IRootState): number | boolean => {
  const {watchTime} = selectVideoConfig(state);
  // make sure nan cannot be returned
  return watchTime || false;
};

export const selectVideoAudioLanguage = (state: IRootState): string => {
  const {audioLangauge} = selectVideoConfig(state);
  const {audioLanguages} = state.ifeMediaPlayer;

  if (!audioLanguages) {
    return '';
  }

  if (audioLangauge) {
    return audioLangauge;
  }

  const enExists = audioLanguages.find((l) => l === 'en');
  if (enExists) {
    return 'en'
  }
  return audioLanguages[0];
};

export const selectVideoSubtitleLanguage = (state: IRootState): string => {
  const {subtitleLanguage} = selectVideoConfig(state);
  // make sure unefiend cannot be returned
  return subtitleLanguage || 'off';
};

export const selectTrailerVideoUrl = (state: IRootState): string => {
  const {selectedItem} = state.ifeVideos;
  if (!selectedItem) {
    return '';
  }

  if (selectedItem.type == TYPES.VIDEO_ITEM) {
    return selectedItem.trailerVideo ? selectedItem.trailerVideo.url : '';
  }
  return '';
};
