import {Action, ActionCreators} from './actions';

export interface KeyedContent {
  [contentKey: string]: any;
}

export interface LocalizedContent {
  [locale: string]: KeyedContent;
}

export interface ComponentContent {
  [componentUuid: string]: LocalizedContent;
}

export type State = {
  content: ComponentContent | undefined;
};

export const initialState: State = {
  content: undefined
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;

  switch (action.type) {
    case ActionCreators.receiveContent.type:
      partialState = {
        content: action.payload
      };

      break;

    default:
      return state;
  }

  return {...state, ...partialState};
}
