import * as React from 'react';
import styled from 'styled-components';
import {SkipButtonProps} from './types';
import SkipNextIcon from '@material-ui/icons/SkipNext';

const StyledContainer = styled.div`
  bottom: 100px;
  font-size: 18px;
  font-weight: bold;
  position: fixed;
  right: 0;
  z-index: 9999;
  .box {
    align-items: center;
    display: flex;
    justify-content: flex-end;
    padding: 10px;
  }
  .skip {
    color: inherit;
    cursor: pointer;
    font-family: inherit;
    font-size: inherit;
    background-color: rgba(0, 0, 0, 0.6);
    font-weight: inherit;
    border: 1px solid white;
    border-right-width: 0px;
  }
`;

const SkipButton: React.FC<SkipButtonProps> = (props: SkipButtonProps) => {
  if (!props.blockFastForward) return null;
  const secondLeft = Math.floor((props.skippableAfter || 0) - props.currentTime);
  const skip = () => {
    const video = document.getElementsByTagName('video')[0];
    video.currentTime = video.duration;
  };
  return (
    <StyledContainer>
      {secondLeft > 0 ? (
        <div className="box pre-skip" children={`You can skip in: ${secondLeft}`} />
      ) : (
        <button aria-label="Skip" className="box skip" onClick={skip}>
          SKIP <SkipNextIcon />
        </button>
      )}
    </StyledContainer>
  );
};

export default SkipButton;
