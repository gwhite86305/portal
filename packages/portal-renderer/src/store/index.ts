import {combineReducers, Reducer} from 'redux';
import {default as system, State as ISystemState} from 'component-utils/dist/store/system/reducer';
import {default as contentAssets, State as IContentState} from 'component-utils/dist/store/contentAssets/reducer';
import {default as ux, State as IUXState} from 'component-utils/dist/store/ux/reducer';
import * as systemActions from 'component-utils/dist/store/system/actions';
import * as contentAssetActions from 'component-utils/dist/store/contentAssets/actions';
import * as uxActions from 'component-utils/dist/store/ux/actions';
import {connectRouter} from 'connected-react-router';
import {History} from 'history';

export type IRootState = {
  router: any;
  ux: IUXState;
  system: ISystemState;
  contentAssets: IContentState;
};

// reducer name maps to state tree its responsible for
const rootReducer = (history: History): Reducer<IRootState> =>
  combineReducers({
    system: system as Reducer<ISystemState>,
    router: connectRouter(history),
    ux,
    contentAssets
  });

export default rootReducer;

export interface Actions {
  [key: string]: number | any | undefined;
}

export const Actions: Actions = {
  system: systemActions,
  ux: uxActions,
  contentAssets: contentAssetActions
};
