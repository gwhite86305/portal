import * as React from 'react';
import styled from 'styled-components';

const Pulsate = styled.div`
  animation-duration: 3.6s;
  animation-iteration-count: infinite;
  animation-name: pulsateAnimation;
  animation-timing-function: ease-in-out;
  height: 100%;
  width: 100%;

  @keyframes pulsateAnimation {
    0% {
      background-color: #1a1a1a;
    }
    25% {
      background-color: #333;
    }
    50% {
      background-color: #1a1a1a;
    }
    100% {
      background-color: #1a1a1a;
    }
  }
`;

export default Pulsate;
