import * as defaults from './defaultProps.json';
import * as React from 'react';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {FlightProgressBar as ExternalInterface} from '../common/ComponentsInterface';
import {PortalComponent} from 'component-utils';

export interface InternalInterface {
  className?: string;
  flightMinutesRemaining: number;
  flightOrigin: string;
  flightDestination: string;
  percentageOfFlightComplete: number;
}

const StyledContainer = styled.div`
    display: flex;
    width: 100%;
    ${renderThemeProperty('textColor', 'color')};
    ${renderThemeProperty('fontFamily', 'font-family')};
    ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
    ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.fontSize, props.theme)};
    .responsive-hidden-md {
      ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'none', md: 'inherit'}, props.theme, true)};
    }
    .responsive-hidden-sm {
      ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'none', sm: 'inherit'}, props.theme, true)};
    }
    .responsive-shown-sm {
      ${(props: any) => applyMediaQueriesForStyle('display', {xs: 'inherit', sm: 'none'}, props.theme, true)};
    }

    .flight-origin {
      font-weight: bold;
      padding-right: 5px;
      ${renderThemeProperty('timeElapsedBackgroundColor', 'background-color')};
      ${(props: any) => applyMediaQueriesForStyle('line-height', props.theme.componentTheme.height, props.theme)};
      ${(props: any) =>
        applyMediaQueriesForStyle('padding-left', props.theme.componentTheme.leftBarPadding, props.theme)};
    }

    .progress-bar {
      flex: 1;
      display: inline-flex;
      .ellapsed {
        text-align: end;
        ${renderThemeProperty('timeElapsedBackgroundColor', 'background-color')};
        ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
        ${(props: any) => `width: ${props.percentageOfFlightComplete}%`};
      }
      .progresIcon{
        ${(props: any) =>
          `background: linear-gradient(to right, ${props.theme.componentTheme.timeElapsedBackgroundColor} 0%, ${
            props.theme.componentTheme.timeElapsedBackgroundColor
          } ${props.feature.progressIconBackgroundHorizontalTransitionPercent}%,${
            props.theme.componentTheme.timeRemainingBackgroundColor
          } ${props.feature.progressIconBackgroundHorizontalTransitionPercent}%,${
            props.theme.componentTheme.timeRemainingBackgroundColor
          } 100%)`};
         img {
          ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
         }
      }
      .remaining {
        ${renderThemeProperty('timeRemainingBackgroundColor', 'background-color')};
        ${(props: any) => applyMediaQueriesForStyle('height', props.theme.componentTheme.height, props.theme)};
        ${(props: any) => `width: ${100 - props.percentageOfFlightComplete}%`};
      }
      .info-text {
        display: inline-flex !important;
        padding: 0px 10px 0px 10px;
        ${(props: any) => applyMediaQueriesForStyle('line-height', props.theme.componentTheme.height, props.theme)};
      }
    }
    .flight-destination {
      padding-left: 5px;
      font-weight: bold;
      display: inline-flex !important;
      ${renderThemeProperty('timeRemainingBackgroundColor', 'background-color')};
      ${(props: any) =>
        applyMediaQueriesForStyle('padding-right', props.theme.componentTheme.rightBarPadding, props.theme)};
      ${(props: any) => applyMediaQueriesForStyle('line-height', props.theme.componentTheme.height, props.theme)};
      img {
        ${(props: any) => applyMediaQueriesForStyle('max-height', props.theme.componentTheme.height, props.theme)};
        ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.iconPadding, props.theme)};
      }
      .service-availability-text {
        font-weight: 400;
      }
    }
  }
`;

const FlightNumber = (props: any) => (
  <div className="info-text responsive-hidden-sm">
    <span className="responsive-hidden-md">{props.flightLocalisation}&nbsp;</span>
    {props.flightNumber}
  </div>
);

const TimeRemaining = (props: any) => (
  <div className="info-text">
    {`${Math.floor(props.flightMinutesRemaining / 60)}h ${Math.floor(props.flightMinutesRemaining) % 60}m`}
    <span className="responsive-hidden-md">&nbsp;{props.remainingTimeLocalisation}</span>
  </div>
);

const PlaneIcon = (props: any) => (
  <div className="progresIcon">
    <img src={props.feature.flightProgressIcon} />
  </div>
);

const EarlyProgress = (props: any) => (
  <div className="progress-bar responsive-hidden-sm">
    <div className="ellapsed" />
    <PlaneIcon {...props} />
    <div className="remaining">
      <FlightNumber {...props} />
      <TimeRemaining {...props} />
    </div>
  </div>
);

const MidProgress = (props: any) => (
  <div className="progress-bar responsive-hidden-sm">
    <div className="ellapsed">
      <FlightNumber {...props} />
    </div>
    <PlaneIcon {...props} />
    <div className="remaining">
      <TimeRemaining {...props} />
    </div>
  </div>
);

const LateProgress = (props: any) => (
  <div className="progress-bar responsive-hidden-sm">
    <div className="ellapsed">
      <FlightNumber {...props} />
      <TimeRemaining {...props} />
    </div>
    <PlaneIcon {...props} />
    <div className="remaining" />
  </div>
);

const EarlyProgressMobile = (props: any) => (
  <div className="progress-bar responsive-shown-sm">
    <div className="ellapsed" />
    <PlaneIcon {...props} />
    <div className="remaining">
      <TimeRemaining {...props} />
    </div>
  </div>
);

const LateProgressMobile = (props: any) => (
  <div className="progress-bar responsive-shown-sm">
    <div className="ellapsed">
      <TimeRemaining {...props} />
    </div>
    <PlaneIcon {...props} />
    <div className="remaining" />
  </div>
);

const Progress = (props: any) => (
  <React.Fragment>
    {props.percentageOfFlightComplete < 33 ? (
      <EarlyProgress {...props} />
    ) : props.percentageOfFlightComplete < 66 ? (
      <MidProgress {...props} />
    ) : (
      <LateProgress {...props} />
    )}

    {props.percentageOfFlightComplete < 50 ? <EarlyProgressMobile {...props} /> : <LateProgressMobile {...props} />}
  </React.Fragment>
);

const Destination = (props: any) => (
  <div className="flight-destination">
    {props.flightDestination}
    <img
      alt={props.isServiceAvailable ? props.availableStatusLocalisation : props.unAvailableStatusLocalisation}
      src={props.isServiceAvailable ? props.feature.serviceAvailableIcon : props.feature.serviceUnavailableIcon}
    />
    <span className="service-availability-text responsive-hidden-md">
      {props.isServiceAvailable ? props.availableStatusLocalisation : props.unAvailableStatusLocalisation}
    </span>
  </div>
);

const ifcDefaults = {
  isServiceAvailable: false,
  percentageOfFlightComplete: 0,
  flightMinutesRemaining: 0,
  flightNumber: '---',
  flightOrigin: '---',
  flightDestination: '---'
};

const FlightProgressBar: React.FC<InternalInterface & ExternalInterface> = props => (
  <PortalComponent componentTheme={props.componentTheme}>
    <StyledContainer className="ifc-flightprogressbar-component" {...props}>
      <div className="flight-origin">{props.flightOrigin}</div>
      <Progress {...props} />
      <Destination {...props} />
    </StyledContainer>
  </PortalComponent>
);
FlightProgressBar.defaultProps = {...defaults, ...ifcDefaults};
export default FlightProgressBar;
