// tslint:disable:curly
import axios, {AxiosInstance} from 'axios';
import {IAnalyticsPayload, IAnalyticsActionMeta, IAnalyticsLogger} from './types';
import Compose from './composer';
import Logger from './logger';

import * as _ from 'lodash';
import {APP_DOM_ID} from '../types/Constants';

/**
 * A Singleton class that handles PFE analytics logging information
 */
class AnalyticsLogger implements IAnalyticsLogger {
  // Static variable for Analytics Logger Singleton pattern
  static loggerInstance: IAnalyticsLogger | null = null;
  static lastSubmit: Number = 0;
  static mediaHashTable = null;

  logger: Logger;

  constructor() {
    if (!AnalyticsLogger.loggerInstance) {
      AnalyticsLogger.loggerInstance = this;

      // Do these on init
      const sessionId = this.retrieveQueryParams().sessionId;
      this.logger = new Logger('Analytics', 'pa_logger', sessionId);
      this.UserAgent(navigator.userAgent);
      document.addEventListener('click', this.ClickThrough);
    }

    return AnalyticsLogger.loggerInstance;
  }

  /**
   * Return relevant information from a mouse-click
   * @param event: a Mouseclick event to parse
   */
  getClickData(event: any): any {
    let path = _.transform(
      event.path || event.composedPath(),
      (result: Array<Object>, node: any): Array<Object> | boolean | any => {
        // Build the node name
        let name =
          node.localName +
          (node.id ? '#' + node.id : '') +
          (node.className ? '.' + node.className : '').replace(' ', '.');

        // Grab attributes we care about
        let src = node.getAttribute('src');
        let href = node.getAttribute('href');
        let dataComponentGuid = node.getAttribute('data-component-guid');
        let text = node.testContent;
        if (text && text.length > 20) text = text.substring(0, 20) + '...';

        // Add step to path with attributes found.
        result.push({
          name: name,
          ...(src ? {src: src} : {}),
          ...(href ? {href: href} : {}),
          ...(dataComponentGuid ? {guid: dataComponentGuid} : {}),
          ...(text ? {text: text} : {})
        });

        // if we're up to the app level, stop tracing.
        if (node.id === APP_DOM_ID) return false;
      },
      []
    );

    return {
      x: event.pageX,
      y: event.pageY,
      path: path
    };
  }

  /**
   * Retrieve URL query params so that the session ID can be injected into the logger on init
   */
  retrieveQueryParams() {
    return window.location.hash
      .substr(window.location.hash.lastIndexOf('?') + 1)
      .split('&')
      .reduce((q: any, query: string) => {
        const [key, value] = query.split('=');
        return (q[key] = value), q;
      }, {});
  }

  /**
   * Condense state for logging, because data takes space.  substitute meta.identifier where possible.
   * @param state Any action that may need condensing
   */
  metafyState(state: any): object {
    // Because we don't want to actually modify the object.
    state = _.cloneDeep(state);

    let removeThese: {[index: string]: Array<string>} = {
      receiveProfile: [
        'theme',
        'layout',
        'settings',
        'serviceTiers',
        'dynamicMediaMap',
        'media',
        'media_hash',
        'dailyForecasts',
        'currentConditions',
        'contentPack'
      ],
      receiveLayout: [
        'ux.theme',
        'ux.layout',
        'ux.settings',
        'ux.serviceTiers',
        'ux.dynamicMediaMap',
        'ux.media',
        'ux.media_hash',
        'ux.dailyForecasts',
        'ux.currentConditions',
        'ux.contentPack'
      ]
    };

    // Remove specified fields, subsituting meta.identifier if availible
    if (_.get(removeThese, state.type))
      removeThese[state.type].forEach(path => {
        let payload = state.payload;
        let target = _.get(payload, path);
        if (target) {
          if (target.meta && target.meta.identifier) _.set(payload, path, `FROM ${target.meta.identifier}`);
          else _.set(payload, path, `${path} REMOVED FOR LOGGER`);
        }
      });

    return state;
  }

  /**
   * ID1: User Agent string
   * @param userAgent The UserAgent string, generally gathered from the browser.
   */
  public UserAgent(userAgent: string) {
    this.logger.log(Compose.ID1(userAgent));
  }

  /**
   * ID2: Timing Metrics
   * We dont currently have a method of capturing these metrics, so this is just a stub.
   * @param timingMetrics
   */
  public TimingMetrics(timingMetrics: object) {}

  /**
   * ID3: State Machine States
   * @param action A React state
   */
  public StateChange(action: object) {
    if (!action) return;
    this.logger.log(Compose.ID3(this.metafyState(action)));
  }

  /**
   * ID4: Page Views
   * @param path A route to an internal page, probably gathered from a React LOCATION_CHANGE
   * @param language The language of the page
   */
  public PageView(path: string, language: string = 'en') {
    this.logger.log(Compose.ID4(path, language));
  }

  /**
   * ID5: Clickthrough
   * @param clickEvent A MouseClick event to parse
   */
  public ClickThrough(clickEvent: object) {
    if (!this.logger)
      // I know this looks silly, but this solves a webpack code-duplication issue.
      return;
    this.logger.log(Compose.ID5(this.getClickData(clickEvent)));
  }

  /**
   * ID6: Device Type
   * @param deviceType An integer representing the detected device type from the Device object.
   */
  public DeviceType(deviceType: number) {
    this.logger.log(Compose.ID6(deviceType));
  }

  /**
   * ID7: Media Impression
   * @param mediaFileName The file name or path of the asset.
   */
  public MediaImpression(mediaFileName: string) {
    this.logger.log(Compose.ID7(mediaFileName));
  }

  /**
   * ID8: Advert Impression
   * @param adId The ID of the ad.
   */
  public AdImpression(adId: string) {
    this.logger.log(Compose.ID11(adId));
  }

  /**
   * ID9: Advert Click Through
   * @param adId The ID of the ad.
   */
  public AdClickThrough(adId: string) {
    this.logger.log(Compose.ID12(adId));
  }
}

async function logger({url, method, data, headers}: IAnalyticsPayload, customAxiosInstance?: AxiosInstance) {
  const axiosInstance = customAxiosInstance || axios;
  try {
    if (method === 'get') {
      await axiosInstance.get(url, {headers: headers});
    } else if (method === 'post') {
      await axiosInstance.post(url, data, {headers: headers});
    } else if (method === 'put') {
      await axiosInstance.put(url, data, {headers: headers});
    }
  } catch (e) {
    console.debug(e);
  }
}

const analyticsLogger = new AnalyticsLogger();

const analyticsReduxMiddleware = () => (next: any) => (action: any) => {
  const returnValue = next(action);
  if (action && action.type == '@@router/LOCATION_CHANGE' && action.payload)
    analyticsLogger.PageView(action.payload.pathname);

  analyticsLogger.StateChange(action);

  const event = _.get(action, 'meta.analytics', {});
  const customAxiosInstance = _.get(action, 'meta.customAxiosInstance');
  if (event && _.get(action, 'meta')) logger(event, customAxiosInstance);

  return returnValue;
};

export {IAnalyticsPayload, IAnalyticsActionMeta, AnalyticsLogger};
export default analyticsReduxMiddleware;

/*
  Analytics Usage:

  To perform analytics we can follow the redux pattern and hook analytics calls to store action,
  this middelware take care of performing side effect base on action meta data
  action that will like to perform analytics call can be shaped as follow:

  interface Action {
    type: string;
    paylad: any
    meta?: {
      analytics?: any,
      customAxiosInstance?: any --> if you need a prebuilt intance with interceptors
    }
  }

  - install middleware:
        const store = createStore(rootReducer, applyMiddleware(analyticsReduxMiddleware));

  - append meta object with analytic payload to the actions you want to log in your analytics endpoint:
        export function myAction() {
          return (dispatch: any) => {
            const meta = {...} as IAnalyticsPayload;
            dispatch(ActionCreators.setCurrentItem.create(payload, meta));
        };
*/
