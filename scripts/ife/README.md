## Rendering a portal and delivering it to IFE

Some Jenkins jobs for

- rendering a portal and deploying to S3 - https://jenkins-ife-dev-eu-west-1.cloudstore.aero/view/portal/job/IFE-Portal-Render/
- sideloading a previously rendered portal on S3 to an onboard-server host https://jenkins-ife-dev-eu-west-1.cloudstore.aero/view/portal/job/IFE-Portal-Sideload/

![](images/portal-renderer_deploy_to-onboard-server.png 'Rendering a portal deploying to CMS')
