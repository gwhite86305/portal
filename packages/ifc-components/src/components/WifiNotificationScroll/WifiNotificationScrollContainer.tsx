import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {WifiNotificationScroll as ExternalInterface} from '../common/ComponentsInterface';
import WifiNotificationScroll from './WifiNotificationScroll';
import Localize from 'component-utils/dist/components/Localize';

export type PageProps = DispatchProps & ExternalInterface;

export interface DispatchProps {}

@connectWrapper((state: any, ownProps: PageProps) => {
  let localizedMessageName = 'ifc.WifiNotificationScroll.down.message';
  if (state.device) {
    localizedMessageName =
      !state.device.isServiceAvailable || state.device.alertId !== '30101'
        ? 'ifc.WifiNotificationScroll.down.message'
        : 'ifc.WifiNotificationScroll.up.message';
  }
  return {text: <Localize messageId={localizedMessageName} />};
})
export default class WifiNotificationScrollContainer extends React.Component<PageProps, any> {
  public render() {
    return <WifiNotificationScroll {...this.props} />;
  }
}
