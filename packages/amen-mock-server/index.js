#!/usr/bin/env node
const express = require('express');
const app = express();
app.use(express.json());
var collection = {};
app.all('/mock/*', (req, res) => {
  collection[req.url.replace('/mock', '') + req.body.method.toLowerCase()] = req.body;
  res.send('amen');
});
app.all('*', (req, res) => {
  console.log(Date.now() + req.url + req.method);
  let mockbody = collection[req.url + req.method.toLowerCase()];
  if (mockbody) {
    setTimeout(function() {
      res.send(mockbody.return);
    }, mockbody.latency || 0);
  } else {
    res.sendStatus(200);
  }
});
app.listen(4000, () => console.log('Amen Mock Server listening on port 4000'));
