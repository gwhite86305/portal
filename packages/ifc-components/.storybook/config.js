import {configure} from '@storybook/react';

function loadStories() {
  require('../stories/statusbar.js');
  require('../stories/WifiNotificationScroll.js');
  require('../stories/weather.js');
  require('../stories/ifeLink.js');
  require('../stories/contactUsLink.js');
  require('../stories/ServiceDisruption.js');
  require('../stories/WifiServiceOptions.js');
  require('../stories/Checkout.js');
  require('../stories/FYI.js');
  require('../stories/deviceSwap.js');
  require('../stories/serviceCountdown.js');
  require('../stories/TextButton.js');
  require('../stories/FlightTimeRemaining.js');
  require('../stories/UserAgreement.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);
