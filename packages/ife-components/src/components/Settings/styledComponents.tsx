import * as PropTypes from 'prop-types';
import * as React from 'react';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import {withStyles} from '@material-ui/core/styles';

// -- StyledContainer
export const StyledContainer = styled.div`
  .settings-button {
    min-width: auto;
    min-height: auto;
    border-radius: 50%;
    padding: 1rem;
    svg {
      ${renderThemeProperty('settingsIconColor', 'fill')};
      ${({theme}) => theme.appFontFamily};
    }
  }
`;

// -- Modal Body
export const StyledModalContent = styled.div`
  ${renderThemeProperty('modalRadius', 'border-radius')};
  ${renderThemeProperty('modalBackgroundColor', 'background')};
  ${renderThemeProperty('textColor', 'color')};
  ${renderThemeProperty('textSize', 'font-size')};
  .modal-content {
    ${renderThemeProperty('textColor', 'color')};
    ${renderThemeProperty('modalContentBackground', 'background')};
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.bodyPadding, props.theme)};
    max-height: 350px;
    overflow: auto;
  }
  .apply {
    ${renderThemeProperty('applyButtonTextSize', 'font-size')};
    ${renderThemeProperty('applyButtonColor', 'background')};
    ${renderThemeProperty('applyButtonTextColor', 'color')};
    text-transform: none;
  }
  .apply:disabled {
    ${renderThemeProperty('applyButtonDisabledColor', 'background')};
  }
  display: flex;
  flex-direction: column;
  .modal-header {
    & > span {
      ${renderThemeProperty('headerTextColor', 'color')};
    }
    display: flex;
    justify-content: space-between;
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalHeaderPadding, props.theme)};
    border-style: solid;
    border-width: 0;
    ${renderThemeProperty('modalDividerTopThickness', 'border-bottom-width')};

    .title {
      display: flex;
      align-items: center;
    }

    .close-button {
      ${renderThemeProperty('textColor', 'color')};
      min-width: auto;
      min-height: auto;
      border-radius: 50%;
      padding: 1rem;
    }

    ${(props: any) =>
      applyMediaQueriesForStyle('background-color', props.theme.componentTheme.modalDividerColor, props.theme)};
    ${renderThemeProperty('modalDividerColor', 'border-color')};
  }
  .modal-footer {
    display: flex;
    justify-content: flex-end;
    ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.modalFooterPadding, props.theme)};
    border-style: solid;
    border-width: 0;
    ${renderThemeProperty('modalDividerBottomThickness', 'border-top-width')};
    ${renderThemeProperty('modalDividerColor', 'border-color')};
  }

  .modal-header svg {
    cursor: pointer;
  }

  .age-restriction-form {
    .age-select {
      margin-bottom: 1rem;
    }

    & > div:before {
      ${renderThemeProperty('inputBorderBottom', 'border-bottom')};
    }
    & > div:hover:before {
      ${renderThemeProperty('inputBorderBottomHover', 'border-bottom', true)};
    }

    & > div {
      width: 100%;
      & > * {
        ${renderThemeProperty('textSize', 'font-size')};
        ${renderThemeProperty('textColor', 'color')};
      }

      & > p {
        transform: scale(0.75);
        transform-origin: left;
      }
    }
  }
`;

// -- Responsive Modal
const styles = (theme: any) => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    height: '100px',
    width: '100px',
    transform: 'translate(-50%, -50%)',
    display: 'table',
    [theme.breakpoints.up('md')]: {width: '650px'},
    [theme.breakpoints.down('sm')]: {width: '90%'}
  }
});
const ResponsiveDiv = (props: any) => (
  <div tabIndex={-1} className={props.classes.root}>
    {props.children}
  </div>
);
(ResponsiveDiv as any).propTypes = {classes: PropTypes.object.isRequired};
export const ResponsiveContainer = withStyles(styles as any)(ResponsiveDiv);
