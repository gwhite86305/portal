import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withNotes} from '@storybook/addon-notes';
import {withKnobs, text, object, boolean, number} from '@storybook/addon-knobs/react';
import Statusbar from '../dist/components/Statusbar/Statusbar.js';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    backgroundColorWifiUp: 'theme.secondaryBackgroundColor',
    backgroundColorWifiDown: 'theme.secondaryBackgroundColor',
    textColor: 'theme.tertiaryTextColor',
    dividerColor: 'theme.primaryBackgroundColor'
  },
  isOffline: true,
  flightOrigin: '---',
  flightDestination: '---',
  flightNumber: '---',
  flightMinutesRemaining: 0,
  destinationTemperature: '---',
  displayWifiServiceScroll: true,

  feature: {
    wifiUpIconPath: './images/wifi-up.svg',
    wifiDownIconPath: './images/wifi-down.svg',
    showOriginDestination: true,
    showFlightNumber: true,
    showTimeToDestination: true,
    showWifiServiceAvailablity: true,
    ShowDestinationTemp: true
  }
};
const sampleProps = {
  componentTheme: {
    backgroundColorWifiUp: 'theme.secondaryBackgroundColor',
    backgroundColorWifiDown: 'theme.formErrorColor',
    textColor: 'theme.tertiaryTextColor',
    dividerColor: 'theme.primaryBackgroundColor'
  },
  isOffline: false,
  flightOrigin: 'DFW',
  flightDestination: 'LGA',
  flightNumber: 'XYZ43',
  flightMinutesRemaining: 222.3,
  destinationTemperature: 35.0,

  feature: {
    wifiUpIconPath: './images/wifi-up.svg',
    wifiDownIconPath: './images/wifi-down.svg',
    showOriginDestination: true,
    showFlightNumber: true,
    showTimeToDestination: true,
    showWifiServiceAvailablity: true,
    ShowDestinationTemp: true
  }
};
const neosLocaleProps = {
  localizedConnectivityUpText: 'CONNESSIONE INTERNET WI-FI',
  localizedConnectivityDownText: 'INTERNET WI-FI NON DISPONIBILE'
};

const stories = storiesOf('Statusbar - Demo Portal Theme', module);

stories.addDecorator(withKnobs);

stories
  .add('Default settting with no data', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Statusbar {...defaultProps} />
    </PortalComponentWrapper>
  ))
  .add('Default setting with sample data', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <Statusbar {...sampleProps} />
    </PortalComponentWrapper>
  ))
  .add('Hide Origin & Destination', () => {
    const testProps = {...sampleProps};
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Statusbar {...testProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Hide Origin, Destination & flight number', () => {
    const testProps = {...sampleProps};
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;
    testProps.feature.showFlightNumber = false;

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Statusbar {...testProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Hide origin, destination, flight number & time to destination', () => {
    const testProps = {...sampleProps};
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;
    testProps.feature.showFlightNumber = false;
    testProps.feature.showTimeToDestination = false;

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Statusbar {...testProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Hide origin, destination, flight number,  time to destination & destination weather', () => {
    const testProps = {...sampleProps};
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;
    testProps.feature.showFlightNumber = false;
    testProps.feature.showTimeToDestination = false;
    testProps.feature.ShowDestinationTemp = false;

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Statusbar {...testProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Neos theme Wifi Up', () => {
    const testProps = {...sampleProps};
    testProps.componentTheme = {...sampleProps.componentTheme};
    testProps.componentTheme.backgroundColorWifiUp = '#0073CB';
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;
    testProps.feature.showFlightNumber = false;
    testProps.feature.showTimeToDestination = false;
    testProps.feature.ShowDestinationTemp = false;
    testProps.feature.wifiUpIconPath = './images/wifi_up_icon.svg';
    return (
      <PortalComponentWrapper mockTheme={mockThemeNeos}>
        <Statusbar {...testProps} {...neosLocaleProps} />
      </PortalComponentWrapper>
    );
  })
  .add('Neos theme Wifi Down', () => {
    const testProps = {...sampleProps};
    testProps.componentTheme = {...sampleProps.componentTheme};
    testProps.componentTheme.backgroundColorWifiUp = '#DF2927';
    testProps.feature = {...sampleProps.feature};
    testProps.feature.showOriginDestination = false;
    testProps.feature.showFlightNumber = false;
    testProps.feature.showTimeToDestination = false;
    testProps.feature.ShowDestinationTemp = false;
    testProps.feature.wifiUpIconPath = './images/wifi_down_icon.svg';
    return (
      <PortalComponentWrapper mockTheme={mockThemeNeos}>
        <Statusbar {...testProps} localizedConnectivityUpText={neosLocaleProps.localizedConnectivityDownText} />
      </PortalComponentWrapper>
    );
  })
  .add(
    'With Knob',
    withNotes(
      '{isOffline: !state.device.isServiceAvailable || state.device.alertId !== 30101} {flightOrigin: state.profile && state.profile.locations && state.profile.locations.originCode} {flightDestination: state.profile && state.profile.locations && state.profile.locations.destinationCode} {flightMinutesRemaining=state.device.wowState == On? undefined: state.device.flightMinutesRemaining > 5999 ? 5999 : state.device.flightMinutesRemaining}'
    )(() => (
      <PortalComponentWrapper mockTheme={object('Theme', mockTheme)}>
        <Statusbar
          componentTheme={object('componentTheme', sampleProps.componentTheme)}
          feature={object('feature', sampleProps.feature)}
          isOffline={boolean('internal_isOffline', sampleProps.isOffline)}
          flightOrigin={text('internal_flightOrigin', sampleProps.flightOrigin)}
          flightDestination={text('internal_flightDestination', sampleProps.flightDestination)}
          flightNumber={text('internal_flightNumber', sampleProps.flightNumber)}
          flightMinutesRemaining={number('internal_flightMinutesRemaining', sampleProps.flightMinutesRemaining)}
          destinationTemperature={number('internal_destinationTemperature', sampleProps.destinationTemperature)}
          localizedConnectivityUpText={text('Wifi-Text-Up', 'CONNECTIVITY AVAILBLE')}
          localizedConnectivityDownText={text('Wifi-Text-Down', 'CONNECTIVITY DOWN')}
        />
      </PortalComponentWrapper>
    ))
  );
