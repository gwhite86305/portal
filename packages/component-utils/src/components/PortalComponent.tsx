import * as React from 'react';
import Hidden from '@material-ui/core/Hidden';
import {Breakpoint} from '@material-ui/core/styles/createBreakpoints';
import {ThemeProvider} from 'styled-components';
import {ComponentContext} from '../utils/Contexts';

export interface PortalComponentProps {
  componentTheme: any;
}

// * Ensure to wrap all portal components in this component.
// * Its role is to determine if your component should be rendered (based on its display property)
// * Additionally it makes your components theme properties object (componentTheme) available to your styledComponent CSS
// * And injects componentUid into the dom tree (for builder related functionalty)
const PortalComponent: React.FunctionComponent<PortalComponentProps> = props => {
  const {children, componentTheme} = props;
  const display = (componentTheme as any).display;

  let hideForScreens: Array<Breakpoint> = display
    ? (Object.keys(display).filter((screenSize: string) => display[screenSize] === false) as Array<Breakpoint>)
    : [];

  const componentUid = React.useContext(ComponentContext) as string;

  return (
    <ThemeProvider theme={(appTheme: any) => (componentTheme ? {...appTheme, componentTheme} : appTheme)}>
      <Hidden only={hideForScreens}>
        {React.Children.map(children, (child: any) => React.cloneElement(child, {'data-component-guid': componentUid}))}
      </Hidden>
    </ThemeProvider>
  );
};

export default PortalComponent;
