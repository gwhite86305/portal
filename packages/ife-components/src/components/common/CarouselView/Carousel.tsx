import * as hasTouch from 'has-touch';
import * as React from 'react';
import DesktopCarousel from './DesktopCarousel';
import MobileCarousel from './MobileCarousel';
import {CarouselProps} from './types';
import ThumbSliderContainer from './ThumbSliderContainer';
import IterableRendererItem, {IterableRendererItemProps} from '../IterableRenderer/IterableRendererItem';

class Carousel extends IterableRendererItem<CarouselProps> {
  constructor(props: CarouselProps) {
    super(props);
  }

  render() {
    const {
      categoryName,
      onItemInViewPort,
      imgPlaceholder,
      items,
      onClick,
      thumbSize,
      styleProps,
      spacing,
      imageVariant
    } = this.props;

    const genericCarouselProps = {categoryName, onItemInViewPort, imgPlaceholder, items, onClick, imageVariant};
    const desktopExclusiveProps = {thumbSize, cellSpacing: spacing || 4};

    return (
      <ThumbSliderContainer {...styleProps}>
        {hasTouch ? (
          <MobileCarousel {...genericCarouselProps} />
        ) : (
          <DesktopCarousel {...genericCarouselProps} {...desktopExclusiveProps} />
        )}
      </ThumbSliderContainer>
    );
  }
}

export default Carousel;
