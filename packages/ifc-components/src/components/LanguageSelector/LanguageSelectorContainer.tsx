import * as React from 'react';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {LanguageSelector as ExternalInterface} from '../common/ComponentsInterface';
import LanguageSelector, {InternalInterface} from './LanguageSelector';
import * as UXActions from 'component-utils/dist/store/ux/actions';
import {withRouter} from 'react-router';
export type PageProps = ExternalInterface & InternalInterface;
export interface DispatchProps {
  onClick(): any;
}
@connectWrapper(
  (state: any, ownProps: PageProps) => {
    return {};
  },
  (dispatch: any, ownProps: PageProps): DispatchProps => ({
    onClick: () => {
      let code = ownProps.feature.code || 'en';
      dispatch(UXActions.changeUserLocale(code));
      const {openPageOnSelect} = ownProps.feature;
      ownProps.history.push('/' + openPageOnSelect);
    }
  })
)
class LanguageSelectorContainer extends React.Component<PageProps, any> {
  constructor(props: any) {
    super(props);
  }
  public render() {
    return <LanguageSelector {...this.props} onClick={this.props.onClick} />;
  }
}

export default withRouter(LanguageSelectorContainer);
