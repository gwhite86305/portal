import {Action, ActionCreators} from './actions';
import LayoutInterface from '../../types/LayoutInterface';
import UXStructure from '../../types/UXStructure';

export type State = {
  readonly errorMessage: string | null;
  readonly isFetching: boolean;
  readonly layout: LayoutInterface | null;
  readonly settings: any | null;
  readonly theme: any | null;
  readonly locale: any;
  readonly builderSelectedComponentUid: any;
  readonly isBuilderInitialized: boolean;
  readonly userSelectedLocale: string | null;
  readonly defaultLocaleMessages: any;
};

export const initialState: State = {
  errorMessage: null,
  isFetching: false,
  layout: null,
  settings: null,
  theme: null,
  locale: null,
  builderSelectedComponentUid: null,
  userSelectedLocale: null,
  isBuilderInitialized: false, // builder mode only
  defaultLocaleMessages: null
};

export type LayoutInitData = {

  defaultLocaleMessages?: any;
  system?: any;
  userSelectedLocale: string;
  ux: UXStructure;
}

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;

  switch (action.type) {
    case ActionCreators.builderSelectComponent.type:
      partialState = {
        builderSelectedComponentUid: action.payload
      };
      break;

    case ActionCreators.changeLocale.type:
      partialState = {
        userSelectedLocale: action.payload
      };

      break;

    case ActionCreators.builderInitLayout.type:

      const {layout, settings, theme, locale} = action.payload;

      partialState = {
        isFetching: false,
        layout: layout,
        settings: settings,
        theme: theme,
        locale: locale,
        userSelectedLocale: layout.app.meta.defaultLocale, // on builder init store default layout locale
        isBuilderInitialized: true
      };

      break;

    case ActionCreators.receiveLayout.type:
      if (action.payload.error) {
        partialState = {
          errorMessage: action.payload.error,
          isFetching: false
        };
      } else {
        const {defaultLocaleMessages, ux, userSelectedLocale}:LayoutInitData = action.payload;
        partialState = {
          isFetching: false,
          layout: ux.layout,
          settings: ux.settings,
          theme: ux.theme,
          locale: ux.layout.locale,
          userSelectedLocale: userSelectedLocale,
          defaultLocaleMessages: (defaultLocaleMessages || state.defaultLocaleMessages)
        };
      }

      break;

    case ActionCreators.requestLayout.type:
      partialState = {
        isFetching: true
      };
      break;

    default:
      return state;
  }

  return {...state, ...partialState};
}
