import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { getHtmlBundleByCategoryName } from './actions';

export interface IHtmlBundle {
  categories?: string[];
  htmlBundleUrl?: string;
  title?: string;
  type?: string;
  uid?: string;
}

export type IHtmlBundleState = IHtmlBundle & {
  isFetching: boolean;
  error: boolean;
};

const initialState: IHtmlBundleState = {
  categories: [],
  htmlBundleUrl: '',
  title: '',
  type: '',
  uid: '',
  isFetching: false,
  error: false
};

export const htmlBundleReducer = reducerWithInitialState(initialState)
  .case(getHtmlBundleByCategoryName.started, (state): IHtmlBundleState => ({
    ...initialState,
    isFetching: true
  }))
  .case(getHtmlBundleByCategoryName.done, (state, {result}): IHtmlBundleState => ({
    ...state,
    ...result,
    isFetching: false
  }))
  .case(getHtmlBundleByCategoryName.failed, (state): IHtmlBundleState => ({
    ...state,
    isFetching: false,
    error: true
  }));