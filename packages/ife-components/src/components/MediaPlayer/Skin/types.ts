import {MEDIA_PLAYER_STATUS} from '../../../store/mediaplayer/types';
import {seekToRange} from '../../../store/mediaplayer/actions.player';
import {setSubtitle, setAudioLang} from '../../../store/videos/actions';
import {NormalizableObject} from '../../../utils/Norm';
import {VideosConfigItem} from '../../../store/videos/types';

export interface SkipButtonProps {
  blockFastForward: boolean;
  currentTime: number;
  skippableAfter?: number;
  uid: string;
}
export interface SkipButtonState {
  prevUid: string;
}

export interface CentralPlayButtonProps {
  onPlay(): any;
  status: MEDIA_PLAYER_STATUS;
}

export interface HorizontalRangeState {
  progressPercentage: number;
}

export interface HorizontalRangeProps {
  blockFastForward: boolean;
  progressColor: string;
  progressBackgroundColor: string;
  progressTextSize: string;
  duration: number;
  currentTime: number;
  onSeekToRange: typeof seekToRange;
}

export interface LanguageState {
  anchorEl: any;
}

export interface LanguageProps {
  icon: any;
  list: string[];
  onSelection: (language: {uid: string; lang: string}) => {};
  selectedLang: string;
  backgroundColor: string;
  textColor: string;
  textSize: string;
  textPadding: string;
  videoId: string;
}

export interface TimeDisplayProps {
  currentTime: number;
  duration: number;
}
