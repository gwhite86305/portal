import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withKnobs, text, object, boolean, number} from '@storybook/addon-knobs/react';
import DeviceSwap from '../dist/components/DeviceSwap/DeviceSwap.js';
import PortalComponentWrapper, {mockTheme, mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    headerColor: 'theme.primaryTextColor',
    headerFontSize: 'theme.quinaryFontSize',
    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',
    inputLabelColor: 'theme.primaryTextColor',
    inputLabelFontSize: 'theme.primaryFontSize'
  },
  feature: {
    modal: '',
    openPageWhenConnected: ''
  }
};

const modifiedProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '70px 20px 70px 20px'
    },
    backgroundColor: '#52B3D9',
    headerColor: '#ffffff',
    headerFontSize: 'theme.quinaryFontSize',
    bodyTextColor: '#ffffff',
    bodyTextFontSize: 'theme.primaryFontSize',
    inputLabelColor: '#ffffff',
    inputLabelFontSize: 'theme.primaryFontSize'
  },
  feature: {
    modal: '',
    openPageWhenConnected: 'http://www.viasat.com'
  }
};

const neosProps = {
  componentTheme: {
    margin: {
      xs: '0px 0px 0px 0px'
    },
    padding: {
      xs: '20px 20px 20px 20px'
    },
    backgroundColor: 'theme.primaryBackgroundColor',
    headerColor: 'theme.primaryTextColor',
    headerFontSize: '1.25rem',
    bodyTextColor: 'theme.primaryTextColor',
    bodyTextFontSize: 'theme.primaryFontSize',
    inputLabelColor: 'theme.primaryTextColor',
    inputLabelFontSize: 'theme.primaryFontSize'
  },
  feature: {
    modal: true,
    openPageWhenConnected: 'Session_status_page'
  }
};
const localizedtextProps = {
  headerText: 'SWITCH WIFI TO THIS DEVICE',
  bodyText: 'To connect to this device, enter the email address you used to purchase your WiFi pass.',
  emailLabel: 'Email address',
  emailErrorHelpText: '',
  submitButtonText: 'SWITCH SERVICE',
  cancelButtonText: 'CANCEL',
  connectedDeviceHeaderText: 'You are already connected',
  connectedDevicebodyText:
    'You are already connected on this device. If you want to connect to the internet on a different device open the portal on that device and select device swap.',
  connectedDeviceAlertButtonText: 'Ok'
};

const neostextProps = {
  headerText: 'Utilizza il WiFi con questo dispositivo',
  bodyText:
    "Per connettere il tuo dispositivo inserisci l'indirizzo email utilizzato durante l'acquisto. Il servizio è utilizzabile solamente su un dispositivo.",
  emailLabel: 'Indirizzo email',
  emailErrorHelpText: '',
  submitButtonText: 'Cambia dispositivo',
  cancelButtonText: 'Cancella',
  connectedDeviceHeaderText: 'Sei già connesso',
  connectedDevicebodyText:
    'Sei già connesso a Internet su questo dispositivo. Se vuoi utilizzare il servizio WiFi su un altro device, connettiti al portale su quel dispositivo e seleziona Cambia dispositivo',
  connectedDeviceAlertButtonText: 'Ok'
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";
mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles =
  "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./fonts/fontawesome-webfont.woff') format('woff'), url('./fonts/fontawesome-webfont.ttf') format('truetype'), url('./fonts/fontawesome-webfont.svg#font') format('svg');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';
(mockTheme.fontIconUsesLigature = false), (mockTheme.fontIconBaseClass = 'fa');

const stories = storiesOf('CMOB-22362:- DeviceSwap Component', module);

stories.addDecorator(withKnobs);

stories.add('Default Props', () => (
  <PortalComponentWrapper mockTheme={mockTheme}>
    <DeviceSwap componentTheme={defaultProps.componentTheme} {...localizedtextProps} />
  </PortalComponentWrapper>
));

stories.add('Modified Props', () => (
  <PortalComponentWrapper mockTheme={mockThemeNeos}>
    <DeviceSwap {...modifiedProps} {...localizedtextProps} />
  </PortalComponentWrapper>
));

stories.add('Neos Props', () => (
  <PortalComponentWrapper mockTheme={mockThemeNeos}>
    <DeviceSwap componentTheme={neosProps.componentTheme} {...neostextProps} />
  </PortalComponentWrapper>
));

stories.add('Connected Screen', () => (
  <PortalComponentWrapper mockTheme={mockThemeNeos}>
    <DeviceSwap {...localizedtextProps} connected={true} {...modifiedProps} />
  </PortalComponentWrapper>
));

stories.add('With Knob', () => (
  <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
    <DeviceSwap
      componentTheme={object('componentTheme', defaultProps.componentTheme)}
      connected={boolean('Device Connected', false)}
      headerText={text('Header Text', 'SWITCH WIFI TO THIS DEVICE')}
      bodyText={text(
        'Body Text',
        'To connect to this device, enter the email address you used to purchase your WiFi pass.'
      )}
      emailLabel={text('Email Label', 'Email address')}
      emailErrorHelpText={text('Email Error Help Text', '')}
      submitButtonText={text('Submit Button Text', 'SWITCH SERVICE')}
      cancelButtonText={text('Cancel Button Text', 'CANCEL')}
      connectedDeviceHeaderText={text('Connected Device Header Text', 'You are already connected')}
      connectedDevicebodyText={text(
        'Connected Device Body Text',
        'You are already connected on this device. If you want to connect to the internet on a different device open the portal on that device and select device swap.'
      )}
      connectedDeviceAlertButtonText={text('Connected Device Alert Button Text', 'Ok')}
    />
  </PortalComponentWrapper>
));
