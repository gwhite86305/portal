import {AdvertItem} from './../../store/adverts/types';
import {NukaAdvertsCarousel as ExternalInterface} from '../common/ComponentsInterface';
import {getAdverts} from '../../store/adverts/actions';
import {trackImpression} from '../../store/analytics/actions';
// import {Props as AdvertsCarouselProps} from '../AdvertsCarousel/types';

export interface StateProps {
  items: AdvertItem[];
  isLoading: boolean;
  isFetchCompleted: boolean;
}

export interface DispatchProps {
  actions: {
    getAdverts: typeof getAdverts;
    trackImpression: typeof trackImpression;
  };
}
export interface IWidth {
  width: any;
}

export interface InternalInterface {
  items: AdvertItem[];
  onClick: (id: string) => void;
  onItemInViewPort: typeof trackImpression;
}

export interface NukaAdvertsCarouselProps extends InternalInterface, ExternalInterface, IWidth {}

export interface NukaAdvertsCarouselState {
  maxWidth: string;
}
