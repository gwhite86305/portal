import * as React from 'react';
import styled from 'styled-components';

const TextContainer = styled.div`
  width: 100%;
  overflow: hidden;
  position: relative;
  backface-visibility: hidden; // to fix chrome render issue // https://www.sitepoint.com/fix-chrome-animation-flash-bug/
  transform: scale(1);
  display: flex;
  align-items: center;
  div {
    display: flex;
    width: 200%;
    align-items: center;
    position: absolute;
    overflow: hidden;
    animation: marquee100 25s linear infinite;
    @media (max-width: 1500px) {
      animation: marquee100 15s linear infinite;
    }
    @media (max-width: 1000px) {
      animation: marquee100 10s linear infinite;
    }
    @media (max-width: 600px) {
      width: 300%;
      animation: marquee150 10s linear infinite;
    }
  }
  &:before {
    content: '.';
    visibility: hidden;
  }
  span {
    float: left;
    white-space: nowrap;
    width: 50%;
    line-height: 1.25;
    padding-bottom: 2px;
    padding-top: 1px;
  }

  @keyframes marquee100 {
    0% {
      left: 0;
    }
    100% {
      left: -100%;
    }
  }
  @keyframes marquee150 {
    0% {
      left: 0;
    }
    100% {
      left: -150%;
    }
  }
` as any;

class Marquee extends React.Component<any, any> {
  public render() {
    const {text} = this.props;
    var marqueeText = [];
    for (var i = 0; i < 2; i++) {
      marqueeText.push(<span key={i}>{text}</span>);
    }
    return (
      <TextContainer>
        <div>{marqueeText}</div>
      </TextContainer>
    );
  }
}

export default Marquee;
