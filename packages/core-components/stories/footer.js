import React from 'react';
import {storiesOf} from '@storybook/react';
import {array, boolean, number, object, text, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import Footer from '../dist/components/Footer/Footer';
import MockRouter from 'react-mock-router';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultLocaleProps = {
  localizedAltImageText: 'Viasat Logo',
  localizedCopyrightText: '© 2018 Viasat, Inc. All rights reserved.'
};

const defaultProps = {
  componentTheme: {
    margin: {
      xs: '0px 0 0 0'
    },
    display: {
      xs: true
    },
    backgroundColor: 'theme.secondaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor'
  },
  localizedMenuItems: [
    {
      label: 'Title 1',
      url: '#',
      id: 'footer-uuid-1'
    },
    {
      label: 'Title 2',
      url: '#',
      id: 'footer-uuid-2'
    }
  ],

  feature: {
    logoImage: './images/ViasatLogo_Footer.svg'
  }
};

const viasatProps = {
  componentTheme: {
    margin: {
      xs: '0px 0 0 0'
    },
    display: {
      xs: true
    },
    backgroundColor: 'theme.secondaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor'
  },
  localizedMenuItems: [
    {
      label: 'Viasat Terms of Service',
      url: 'Terms_of_Service',
      id: 'footer-uuid-1'
    },
    {
      label: 'Viasat Privacy Policy',
      url: 'Privacy_Policy',
      id: 'footer-uuid-2'
    }
  ],
  feature: {
    logoImage: './images/ViasatLogo_Footer.svg'
  }
};

const modifiedProps_external = {
  componentTheme: {
    margin: {
      xs: '0px 0 0 0'
    },
    display: {
      xs: true
    },
    backgroundColor: 'theme.secondaryBackgroundColor',
    dividerColor: 'theme.senaryBackgroundColor'
  },
  localizedMenuItems: [
    {
      label: 'Viasat Terms of Service',
      url: 'http://www.viasat.com',
      id: 'footer-uuid-1'
    },
    {
      label: 'Viasat Privacy Policy',
      url: 'http://www.viasat.com',
      id: 'footer-uuid-2'
    }
  ],
  feature: {
    logoImage: './images/ViasatLogo_Footer.svg'
  }
};

const qantasLocaleProps = {
  localizedAltImageText: 'Viasat Logo',
  localizedCopyrightText: '© 2018 Qantas, Inc. All rights reserved.'
};

const qantasProps = {
  componentTheme: {
    margin: {
      xs: '0px 0 0 0'
    },
    display: {
      xs: true
    },
    backgroundColor: '#96281B',
    dividerColor: 'theme.senaryBackgroundColor'
  },
  localizedMenuItems: [
    {
      label: 'Viasat Terms of Service',
      url: 'http://www.qantas.com',
      id: 'footer-uuid-1'
    },
    {
      label: 'Viasat Privacy Policy',
      url: 'http://www.qantas.com',
      id: 'footer-uuid-2'
    }
  ],
  feature: {
    logoImage: './images/Quantas.png'
  }
};

const modified_theme = {
  componentTheme: {
    margin: {
      xs: '0px 0 0 0'
    },
    display: {
      xs: true
    },
    backgroundColor: '#663399',
    dividerColor: 'theme.senaryBackgroundColor'
  }
};

const footerLinks = [
  {
    label: 'Viasat Terms of Service',
    url: 'http://www.qantas.com',
    id: 'footer-uuid-1'
  },
  {
    label: 'Viasat Privacy Policy',
    url: 'http://www.aa.com',
    id: 'footer-uuid-2'
  }
];

const morefooterLinks = [
  {
    label: 'Viasat Terms of Service',
    url: 'http://www.viasat.com',
    id: 'footer-uuid-1'
  },
  {
    label: 'Viasat Privacy Policy',
    url: 'http://www.viasat.com',
    id: 'footer-uuid-2'
  },
  {
    label: 'Viasat Contact Us',
    url: 'http://www.viasat.com',
    id: 'footer-uuid-3'
  },
  {
    label: 'Viasat Live Chat',
    url: 'http://www.viasat.com',
    id: 'footer-uuid-4'
  }
];

const icelandProps = {
  feature: {
    text: 'Footer',
    menuItems: [
      {
        url: 'Terms_of_Service',
        id: 'footer-uuid-1',
        labels: {
          en: {
            label: 'Viasat Terms of Service',
            iconAltText: ''
          }
        }
      },
      {
        url: 'Privacy_Policy',
        id: 'footer-uuid-2',
        labels: {
          en: {
            label: 'Viasat Privacy Policy',
            iconAltText: ''
          }
        }
      }
    ],
    logoImage: './images/viasat-logo-final-rgb.svg'
  },
  componentTheme: {
    margin: {
      xs: '150px 0 150px 0'
    },
    display: {
      xs: true
    },
    backgroundColor: '#EAEAEA',
    textColorLogoRegion: '#444444',
    textColorLinks: '#444444',
    dividerColor: '#CDCDCD',
    paddingTextContent: {
      xs: '23px 0px 27px 0px',
      sm: '95px 69px 29px 0px'
    },
    paddingLogoRegion: {
      xs: '2px 0px 16px 0px',
      sm: '4px 0px 15px 65px'
    }
  }
};
mockTheme.fontIconFontFamily = "'FontAwesome'";
mockTheme.fontIconFiles =
  "url('./fonts/font-awesome/fonts/fontawesome-webfont.woff2') format('woff2'), url('./fonts/fontawesome-webfont.woff') format('woff'), url('./fonts/fontawesome-webfont.ttf') format('truetype'), url('./fonts/fontawesome-webfont.svg#font') format('svg');";
mockTheme.fontIconCss = './fonts/font-awesome/css/font-awesome.css';
(mockTheme.fontIconUsesLigature = false), (mockTheme.fontIconBaseClass = 'fa');

const stories = storiesOf('[CMOB-12694]-Footer - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('Footer - Default Demo Portal Theme', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
        <Footer {...defaultProps} {...defaultLocaleProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer - Viasat Theme & Text Configurability', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer {...viasatProps} {...defaultLocaleProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test External link Configurability', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer {...modifiedProps_external} {...defaultLocaleProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test Logo text removed', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer {...modifiedProps_external} localizedCopyrightText={''} localizedAltImageText={'Viasat Logo'} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test Footer Text with knobs', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer
          componentTheme={object('componentTheme', modifiedProps_external.componentTheme)}
          feature={object('feature', modifiedProps_external.feature)}
          localizedMenuItems={object('feature', modifiedProps_external.localizedMenuItems)}
          {...defaultLocaleProps}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test Footer links with knobs', () => {
  const testProps = {...modifiedProps_external};
  testProps.feature = {...modifiedProps_external.feature};
  testProps.localizedMenuItems = footerLinks;

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer
          {...testProps}
          feature={object('feature', testProps.feature)}
          localizedMenuItems={object('localizedMenuItems', testProps.localizedMenuItems)}
          {...defaultLocaleProps}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test More link Configurability with knobs', () => {
  const testProps = {...modifiedProps_external};
  testProps.feature = {...modifiedProps_external.feature};
  testProps.localizedMenuItems = morefooterLinks;

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer
          {...testProps}
          feature={object('feature', testProps.feature)}
          localizedMenuItems={object('localizedMenuItems', testProps.localizedMenuItems)}
          {...defaultLocaleProps}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test Modified Theme with knobs', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={mockTheme}>
        <Footer
          {...modifiedProps_external}
          componentTheme={object('componentTheme', modified_theme.componentTheme)}
          feature={object('feature', modifiedProps_external.feature)}
          localizedMenuItems={object('localizedMenuItems', modifiedProps_external.localizedMenuItems)}
          {...defaultLocaleProps}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Iceland props', () => {
  const testProps = {...icelandProps};
  testProps.localizedMenuItems = footerLinks;

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <Footer
          componentTheme={testProps.componentTheme}
          feature={testProps.feature}
          localizedMenuItems={testProps.localizedMenuItems}
          {...defaultLocaleProps}
          localizedlogoRegionText={'Onboard WiFi services<br/> provided by'}
          localizedNotesSectionText={'© 2018 Icelandair. Icelandair - Reykjavik Airport - 101 Reykjavik'}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('Footer -Test Modified Theme and Props with knobs', () => {
  const testProps = {...icelandProps};
  testProps.localizedMenuItems = footerLinks;

  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <Footer
          componentTheme={object('componentTheme', testProps.componentTheme)}
          feature={object('feature', testProps.feature)}
          localizedMenuItems={object('localizedMenuItems', testProps.localizedMenuItems)}
          {...defaultLocaleProps}
          localizedlogoRegionText={text('logo region text', 'Onboard WiFi services<br/> provided by')}
          localizedNotesSectionText={text(
            'copyright address text',
            '© 2018 Icelandair. Icelandair - Reykjavik Airport - 101 Reykjavik'
          )}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});
