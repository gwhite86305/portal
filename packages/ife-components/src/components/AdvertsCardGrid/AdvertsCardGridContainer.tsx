import * as React from 'react';
import AdvertsCardGrid from './AdvertsCardGrid';
import {AdvertItem} from '../../store/adverts/types';
import {AdvertsCardGrid as ExternalInterface} from '../common/ComponentsInterface';
import {mapDispatchToProps, mapStoreToProps} from './utils';
import {PortalComponent, utils} from 'component-utils';
import {Props} from './types';
const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class AdvertsCardGridContainer extends React.Component<Props & ExternalInterface> {
  constructor(props: Props & ExternalInterface) {
    super(props);
    this.openAd = this.openAd.bind(this);
  }
  componentDidMount() {
    this.onUpdate(this.props);
  }

  render() {
    const {items, actions, isLoading, isFetchCompleted} = this.props;
    const isLoaded = !isLoading && isFetchCompleted && items.length;

    const displayItems = this.props.feature.adCategory
      ? items.filter((ad: AdvertItem) => ad.categories[0].categoryUid === this.props.feature.adCategory)
      : items;
    const filteredItems = this.props.feature.maxNumberOfAds
      ? displayItems.slice(0, this.props.feature.maxNumberOfAds)
      : displayItems;

    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        {!isLoaded ? (
          <div />
        ) : (
          <AdvertsCardGrid onClick={this.openAd} items={filteredItems} onItemInViewPort={actions.trackImpression} />
        )}
      </PortalComponent>
    );
  }

  private shouldFetch = ({isFetchCompleted, isLoading}: Props) => !isFetchCompleted && !isLoading;
  private onUpdate = (nextProps: Props) => this.shouldFetch(nextProps) && this.props.actions.getAdverts();
  private openAd = (adItem: AdvertItem) => {
    this.props.actions.trackView(adItem);
    window.open(adItem.targetUrl, '_blank');
  };
}
