import {PortalComponent, utils} from 'component-utils';
import {RouterContext} from 'component-utils/dist/utils/Contexts';
import {lastIndexOf, pathOr, propEq, slice} from 'ramda';
import * as React from 'react';
import {TYPES, VideoGroup, VideoItem} from '../../store/videos/types';
import {Loader} from './Loader';
import SeriesDetails from './SeriesDetails';
import {StyledContainer} from './StyledContainer';
import {SeriesDetailsContainerProps, SeriesDetailsContainerState} from './types';
import {imgPlaceholder, mapDispatchToProps, mapStoreToProps} from './utils';

const connectWrapper = utils.connectWrapper.default;

@connectWrapper(mapStoreToProps, mapDispatchToProps)
export default class SeriesDetailsContainer extends React.PureComponent<
  SeriesDetailsContainerProps,
  SeriesDetailsContainerState
> {
  public static contextType = RouterContext;
  public videoId = 'videoId=';
  public state = {contentId: ''};

  componentDidUpdate(prevProps: SeriesDetailsContainerProps) {
    if (!prevProps.videoNotFound && this.props.videoNotFound) {
      this.context.history.goBack();
    }
  }

  componentDidMount() {
    if (navigator.onLine) {
      this.readUrlAndFetchData();
    } else {
      this.props.actions.showMessage({
        title: 'Network Disconnect',
        message: 'You have been disconnected from the network',
        isError: true
      });
    }
    window.addEventListener('online', this.onNetworkReconnect);
  }

  componentWillUnmount() {
    window.removeEventListener('online', this.onNetworkReconnect);
  }

  render() {
    const {componentTheme, selectedItem, actions, playlistIdsMap, playlistLoading} = this.props;
    const {showTitleHeader, showBackButton} = this.props.feature;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <StyledContainer showTitleHeader={showTitleHeader} showBackButton={showBackButton}>
          {selectedItem && !playlistLoading ? (
            <SeriesDetails
              onClickBack={this.onClickBack}
              imgPlaceholder={imgPlaceholder}
              item={selectedItem as VideoGroup}
              addItemsToPlaylist={actions.addItemsToPlaylist}
              playItem={this.onPlayItem}
              removeItemFromPlaylist={actions.removeItemFromPlaylist}
              playlistIdsMap={playlistIdsMap}
            />
          ) : (
            <Loader />
          )}
        </StyledContainer>
      </PortalComponent>
    );
  }

  private onNetworkReconnect = () => {
    this.props.actions.hideMessage();
    this.context.history.goForward();
  };

  private onPlayItem = (item: VideoItem) => {
    this.props.actions.trackPlay(item);
    this.props.actions.playItem(item, this.props.feature.playerPage || '');
  };

  private onClickBack = () => this.context.history.goBack();

  private getContentId() {
    const querySearch = pathOr('', ['location', 'search'], this.context);
    const index = lastIndexOf(this.videoId, querySearch as any);
    return index > -1 ? slice(index + this.videoId.length, Infinity, querySearch) : '';
  }

  private areEpisodeLoaded = (item: VideoGroup | undefined) => item && !!item.items;

  private readUrlAndFetchData() {
    const contentId = this.getContentId();
    if (!contentId) {
      return;
    }
    this.props.actions.trackView({uid: contentId, type: TYPES.VIDEO_GROUP} as VideoGroup);
    const cachedItem = this.props.videoItems.find(propEq('uid', contentId));
    this.setState({contentId});
    // Exit if available in cache
    if (this.areEpisodeLoaded(cachedItem)) {
      return;
    }
    // Run asyc call -> Item will be set by getDerivedStateFromProps
    this.props.actions.getVideoById(contentId);
  }
}
