import {TextField} from '@material-ui/core';
import * as React from 'react';
import {VoucherEntryProps} from './types';

const VoucherEntry: React.FC<VoucherEntryProps> = ({error, value, onChange}) => (
  <TextField
    className={`voucher-input${error ? ' error' : ''}`}
    name="voucher"
    type="text"
    label="Voucher code *"
    value={value}
    onChange={onChange}
    error={error != false}
    helperText={
      error ||
      'If you have one of our vouchers, please enter the code now. If not please purchase from one of our cabin crew.'
    }
  />
);

export default VoucherEntry;
