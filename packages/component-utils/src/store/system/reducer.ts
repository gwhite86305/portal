import {Action, ActionCreators} from './actions';
import {ErrorAPIMessage, generateErrorState} from '../../utils/APIError';

export interface SystemMessage {
  title: string;
  message: string;
  isError: boolean;
}

export interface SystemSettings {
  languages: any[];
  currencies: any[];
  systemDefaultLocale: string;
  systemDefaultCurrency: string;
}

export type State = {
  readonly error: ErrorAPIMessage | null;
  readonly isFetching: boolean;
  readonly systemSettings: SystemSettings | null;
  readonly systemMessage: SystemMessage | null;
};

export const initialState: State = {
  error: null,
  isFetching: false,
  systemSettings: null,
  systemMessage: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;

  switch (action.type) {
    case ActionCreators.requestSettings.type:
      partialState = {
        isFetching: true,
        error: null
      };

      break;

    case ActionCreators.receiveSettings.type:
      if (!(partialState = generateErrorState(action.payload))) {
        partialState = {
          isFetching: false,
          systemSettings: action.payload.systemSettings
        };
      }

      break;

    case ActionCreators.toggleSystemMessage.type:
      partialState = {
        systemMessage: action.payload
      };

      break;

    default:
      return state;
  }

  return {...state, ...partialState};
}
