// @ts-nocheck
const e2eUtils = require('portal-builder-inspector');

describe('Hamburger Menu', () => {
  let page;

  beforeAll(async () => {
    page = await e2eUtils.login();
  });

  it('should load without error', async () => {
    const text = await page.evaluate(() => document.body.textContent);
  });

  afterAll(async function() {
    await e2eUtils.logoutAndClose(page);
  });
});
