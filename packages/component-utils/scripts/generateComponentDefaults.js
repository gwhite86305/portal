#!/usr/bin/env node
/*
 * GENERATE COLLECTION OF JSON DESCRIBING COMPONENT DEFAULTS FOR INGESTION TO DIST BY UX BUILDER ALONG WITH COMPONENT SCHEMA DEFINITIONS
 */

const path = require('path');
const fs = require('fs');
const lstatSync = fs.lstatSync;
const readdirSync = fs.readdirSync;
const join = require('path').join;

const COMPONENTS_DIR = 'src/components';
const SCHEMA_DIST_PATH = 'dist/schema/';
const COMPONENTS_DIST_PATH = 'dist/components/';

const isDirectory = source => lstatSync(source).isDirectory();
const getDirectories = source =>
  readdirSync(source)
    .map(name => join(source, name))
    .filter(isDirectory);

traverseDirectory = folderName => {
  const componentFolderPath = folderName + '/defaultProps.json';
  if (fs.existsSync(componentFolderPath)) {
    const componentName = folderName.split(path.sep).pop();
    defaultPropsWrapper[componentName] = JSON.parse(fs.readFileSync(componentFolderPath));

    let folder_split = folderName.split(path.sep);
    // Change "src" in path to "dist"
    folder_split[0] = 'dist';
    const dist_path = folder_split.join(path.sep);

    if (!fs.existsSync(dist_path)) {
      fs.mkdirSync(dist_path);
    }
    fs.createReadStream(componentFolderPath).pipe(fs.createWriteStream(dist_path + '/defaultProps.json'));
  }

  subFolders = getDirectories(folderName);
  subFolders.forEach(traverseDirectory);
};

const defaultPropsWrapper = {};

if (!fs.existsSync('dist')) {
  fs.mkdirSync('dist');
}

traverseDirectory(COMPONENTS_DIR);

const defaultPropsWrapperAsString = JSON.stringify(defaultPropsWrapper);
if (!fs.existsSync(SCHEMA_DIST_PATH)) {
  fs.mkdirSync(SCHEMA_DIST_PATH);
}

fs.writeFile(SCHEMA_DIST_PATH + 'component-defaults.json', defaultPropsWrapperAsString, 'utf8', function(err) {
  if (err) {
    return console.log(err);
  }
  console.log('\nComponent default properties built successfully to: ' + SCHEMA_DIST_PATH);
});
