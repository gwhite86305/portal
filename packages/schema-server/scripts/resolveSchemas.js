#!/usr/bin/env node

const fs = require('fs');

// TODO: For the moment, the top file combines individually versioned libraries,
//   and the existing way of having one version with several libraries.
//   The portal builder needs to be changed to use individually versioned libraries.
const OLDMODE = true;

const mainFile = JSON.parse(fs.readFileSync('./src/schema/index.json'));

fs.mkdirSync('./dist');

if (OLDMODE) {
  const versions = mainFile.versions;

  const indexOutput = JSON.stringify({
    versions: versions
  });
  fs.writeFileSync('./dist/index.json', indexOutput);

  for (i in versions) {
    const version = versions[i].version;
    const libs = mainFile.libs;
    fs.mkdirSync(`./dist/${version}`);
    fs.mkdirSync(`./dist/${version}/lib`);

    for (j in libs) {
      // TODO: Make this pull from the final store, instead of locally
      //   linked packages ('./node_modules/...' etc).
      //   Same for theme schema tbh.

      let lib = libs[j];
      fs.mkdirSync(`./dist/${version}/lib/${lib.type}`);

      if (fs.existsSync(`./node_modules/${lib.package}/dist/schema/settings-schema.json`)) {
        fs.copyFileSync(
          `./node_modules/${lib.package}/dist/schema/settings-schema.json`,
          `./dist/${version}/lib/${lib.type}/settings-schema.json`
        );
      }
      if (fs.existsSync(`./node_modules/${lib.package}/dist/locale/default-locale-messages.json`)) {
        fs.copyFileSync(
          `./node_modules/${lib.package}/dist/locale/default-locale-messages.json`,
          `./dist/${version}/lib/${lib.type}/default-locale-messages.json`
        );
      }
      fs.copyFileSync(
        `./node_modules/${lib.package}/dist/schema/components-schema.json`,
        `./dist/${version}/lib/${lib.type}/components-schema.json`
      );
      fs.copyFileSync(
        `./node_modules/${lib.package}/dist/schema/component-defaults.json`,
        `./dist/${version}/lib/${lib.type}/component-defaults.json`
      );
    }

    fs.copyFileSync(
      './node_modules/component-utils/dist/schema/theme-schema.json',
      `./dist/${version}/theme-schema.json`
    );

    const libsIndexOutput = JSON.stringify({
      libs: libs
    });
    fs.writeFileSync(`./dist/${version}/index.json`, libsIndexOutput);
  }
} else {
  fs.mkdirSync('./dist/lib');
  const libs = mainFile.libs;

  for (i in libs) {
    // TODO: Make this pull from the final store, instead of locally
    //   linked packages ('./node_modules/...' etc).
    //   Same for theme schema tbh.

    let lib = libs[i];
    fs.mkdirSync(`./dist/lib/${lib.type}`);

    for (j in lib.versions) {
      let version = lib.versions[j];
      fs.mkdirSync(`./dist/lib/${lib.type}/${version}`);
      if (fs.existsSync(`./node_modules/${lib.package}/dist/schema/settings-schema.json`)) {
        fs.copyFileSync(
          `./node_modules/${lib.package}/dist/schema/settings-schema.json`,
          `./dist/lib/${lib.type}/${version}/settings-schema.json`
        );
      }

      if (fs.existsSync(`./node_modules/${lib.package}/dist/locale/default-locale-messages.json`)) {
        fs.copyFileSync(
          `./node_modules/${lib.package}/dist/locale/default-locale-messages.json`,
          `./dist/lib/${lib.type}/${version}/default-locale-messages.json`
        );
      }

      fs.copyFileSync(
        `./node_modules/${lib.package}/dist/schema/components-schema.json`,
        `./dist/lib/${lib.type}/${version}/components-schema.json`
      );
      fs.copyFileSync(
        `./node_modules/${lib.package}/dist/schema/component-defaults.json`,
        `./dist/lib/${lib.type}/${version}/component-defaults.json`
      );
    }
  }

  for (i in mainFile.themes) {
    const themeVersion = mainFile.themes[i];
    fs.copyFileSync(
      './node_modules/component-utils/dist/schema/theme-schema.json',
      `./dist/themes/${themeVersion}/theme-schema.json`
    );
  }

  const indexOutput = JSON.stringify({
    libs: libs,
    themes: mainFile.themes,
    renderers: mainFile.renderers
  });
  fs.writeFileSync('./dist/index.json', indexOutput);
}
process.exit(0);
