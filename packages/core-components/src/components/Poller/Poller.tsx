import * as React from 'react';

export interface InternalInterface {
  action(): any;
  interval: number;
  className?: string;
}

export default class Poller extends React.Component<InternalInterface, any> {
  timeoutId: any; // State is async so no need here, plain var is ok

  public poll() {
    this.props.action();
    // set safe limit of 1000
    if (!isNaN(this.props.interval) && this.props.interval > 1000) {
      this.timeoutId = setTimeout(this.poll.bind(this), this.props.interval);
    }
  }

  public unpoll() {
    if (this.timeoutId !== null) {
      clearTimeout(this.timeoutId);
      this.timeoutId = null;
    }
  }

  public componentDidMount() {
    this.poll();
  }

  public componentDidUpdate(prevProps: InternalInterface) {
    if (prevProps.interval !== this.props.interval) {
      this.unpoll();
      this.poll();
    }
  }

  public componentWillUnmount() {
    this.unpoll();
  }

  public componentDidCatch() {
    this.unpoll();
  }

  public render() {
    return this.props.children || null;
  }
}
