import {setItem, getItem, removeItem, clear} from 'localforage';

type ListedKeys = 'playlist' | 'videosConfig';

// TODO refactor this to be only a dataacess layer. Dataacess layer would be used by the service which has return types for the specific items

export class LocalStorageService {
  public writeData = (key: ListedKeys, data: any) => setItem(key, data);
  public readData = <T>(key: ListedKeys): Promise<T | null> => getItem(key);
  public removeData = (key: ListedKeys) => removeItem(key);
  public clearForage = () => clear();
}

const localStorageService = new LocalStorageService();
export default localStorageService;
