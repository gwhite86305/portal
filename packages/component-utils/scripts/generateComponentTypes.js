#!/usr/bin/env node
/*
 * GENERATE TS DEFN FILES IN SRC TREE FROM COMPONENT AND THEME SCHEMAS, AND MOVE SCHEMAS TO DIST FOR UX BUILDER INGESTION
 */

const fs = require('fs');
const $RefParser = require('json-schema-ref-parser');
const jsonSchema = require('json-schema-to-typescript');
const path = require('path');

const SCHEMA_COMPONENTS_FILE_NAME = 'components-schema.json';
const SCHEMA_THEME_FILE_NAME = 'theme-schema.json';
const SCHEMA_SETTINGS_FILE_NAME = 'settings-schema.json';
const SCHEMA_COMPONENTS_PATH = 'src/schema/';
const SCHEMA_DIST_PATH = 'dist/schema/';
const SCHEMA_DEFN_SRC_PATH = 'src/components/common';
const SCHEMA_DEFN_DIST_PATH = 'dist/components/common/';

const COMPONENT_DEFN_SRC_FILE = path.join(SCHEMA_DEFN_SRC_PATH, 'ComponentsInterface.d.ts');
const THEME_DEFN_SRC_FILE = path.join(SCHEMA_DEFN_SRC_PATH, 'ThemeInterface.d.ts');
const SETTINGS_DEFN_SRC_FILE = path.join(SCHEMA_DEFN_SRC_PATH, 'SettingsInterface.d.ts');
const COMPONENT_DEFN_DIST_FILE = path.join(SCHEMA_DEFN_DIST_PATH, 'ComponentsInterface.d.ts');
const THEME_DEFN_DIST_FILE = path.join(SCHEMA_DEFN_DIST_PATH, 'ThemeInterface.d.ts');
const SETTINGS_DEFN_DIST_FILE = path.join(SCHEMA_DEFN_DIST_PATH, 'SettingsInterface.d.ts');

if (!fs.existsSync('dist')) {
  fs.mkdirSync('dist');
}
if (!fs.existsSync('dist/schema')) {
  fs.mkdirSync('dist/schema');
}
if (!fs.existsSync('dist/components')) {
  fs.mkdirSync('dist/components');
  fs.mkdirSync('dist/components/common');
}
if (!fs.existsSync('dist/components/common')) {
  fs.mkdirSync('dist/components/common');
}

const resolveLibSchema = schemaUrl => {
  const dereference = (url, options) => {
    return $RefParser
      .bundle(url, options)
      .then(data => {
        return data;
      })
      .catch(err => {
        return {error: true, message: err};
      });
  };

  /*
    Note: "correct" way to de-ref is to simply point to field-schema.json as top level $ref, and for
    apply $ref for content schema to dependant components like this:
      "$ref": "./content-schema.json#/definitions/Legal"
    and the dereference parser will just deref into our schema and off we go!
    However dereference parser has a bug:
    it doesn't dereference multiple local schema when have you have a top level ref:
    https://github.com/BigstickCarpet/json-schema-ref-parser/issues/48
    So, *only* way to do this for now is to just append content schema definitions to that of document...
   */

  const fieldSchemaOptions = {
    resolve: {
      fields: {
        order: 0,
        canRead: /field-schema\.json/i,
        read: (file, callback) => {
          fs.readFile(`./node_modules/portal-schema/src/field-schema.json`, function(err, fileSchemaDataDefinition) {
            if (err) {
              callback(err);
            } else {
              fs.readFile(`./src/schema/content-schema.json`, function(err, contentSchemaDataDefinitions) {
                const fileSchemaDefns = JSON.parse(fileSchemaDataDefinition);
                const contentSchemaDefns = JSON.parse(contentSchemaDataDefinitions);
                fileSchemaDefns.definitions = Object.assign(
                  fileSchemaDefns.definitions,
                  contentSchemaDefns.definitions
                );
                callback(null, JSON.stringify(fileSchemaDefns));
              });
            }
          });
        }
      }
    }
  };

  return dereference(schemaUrl, fieldSchemaOptions);
};

const compileWithFieldSchema = (inFile, outFileSrc, outFileDist) => {
  if (fs.existsSync(SCHEMA_COMPONENTS_PATH + inFile)) {
    // eslint-ignore-line
    resolveLibSchema(SCHEMA_COMPONENTS_PATH + inFile).then(data => {
      fs.writeFileSync(SCHEMA_DIST_PATH + inFile, JSON.stringify(data));
      jsonSchema
        .compile(data, 'components-schema', {enableConstEnums: true})
        .then(ts => {
          fs.writeFileSync(outFileSrc, ts);
          fs.writeFileSync(outFileDist, ts);
        })
        .then(() => {
          console.log(`\ ${inFile} TS Definitions built successfully`);
        })
        .catch(() => {
          process.exitCode = 1;
        });
    });
  }
};

compileWithFieldSchema(SCHEMA_COMPONENTS_FILE_NAME, COMPONENT_DEFN_SRC_FILE, COMPONENT_DEFN_DIST_FILE);
compileWithFieldSchema(SCHEMA_SETTINGS_FILE_NAME, SETTINGS_DEFN_SRC_FILE, SETTINGS_DEFN_DIST_FILE);

// compile theme
if (fs.existsSync(SCHEMA_COMPONENTS_PATH + SCHEMA_THEME_FILE_NAME)) {
  // eslint-ignore-line
  jsonSchema
    .compileFromFile(SCHEMA_COMPONENTS_PATH + SCHEMA_THEME_FILE_NAME)
    .then(ts => {
      fs.writeFileSync(THEME_DEFN_SRC_FILE, ts);
      fs.writeFileSync(THEME_DEFN_DIST_FILE, ts);
    })
    .then(() => {
      console.log('\nTheme TS Definitions built successfully');
    })
    .catch(() => {
      process.exitCode = 1;
    });
}

if (fs.existsSync(SCHEMA_COMPONENTS_PATH + SCHEMA_THEME_FILE_NAME)) {
  fs.createReadStream(SCHEMA_COMPONENTS_PATH + SCHEMA_THEME_FILE_NAME).pipe(
    fs.createWriteStream(SCHEMA_DIST_PATH + SCHEMA_THEME_FILE_NAME)
  );
}
