import * as React from 'react';
import {CommonComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {ServiceCountdown as ExternalInterface} from '../common/ComponentsInterface';
import {applyMediaQueriesForStyle, renderThemeProperty} from 'component-utils/dist/utils/styledComponents';
import styled from 'styled-components';
import * as defaults from './defaultProps.json';

export interface InternalInterface {
  className?: string;
  timeRemaining: number;
  connected: boolean;
  headerText: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  ${renderThemeProperty('backgroundColor', 'background-color')};
  ${(props: any) => applyMediaQueriesForStyle('margin', props.theme.componentTheme.margin, props.theme)};
  ${(props: any) => applyMediaQueriesForStyle('padding', props.theme.componentTheme.padding, props.theme)};
  width: 100%;
  padding-top: calc(${(props: any) => props.theme.gutter} * 0.56);
  padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.56);
  .main-header {
    text-align: center;
    ${renderThemeProperty('mainHeaderDisplay', 'display')};
    ${renderThemeProperty('mainHeaderFontColor', 'color')};
    ${renderThemeProperty('mainHeaderFontSize', 'font-size')};
    ${renderThemeProperty('mainHeaderFontWeight', 'font-weight')};
  }
`;

const StyledHeader = styled.div`
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  ${renderThemeProperty('headerFontColor', 'color')};
  ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.headerFontSize, props.theme)};
  ${renderThemeProperty('headerFont', 'font-family')};
`;
const StyledTimer = styled.div`
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  ${renderThemeProperty('timerFontColor', 'color')};
  ${(props: any) => applyMediaQueriesForStyle('font-size', props.theme.componentTheme.timerFontSize, props.theme)};
  ${renderThemeProperty('timerFont', 'font-family')};
`;

class ServiceCountdown extends React.Component<InternalInterface & ExternalInterface, any> {
  public static defaultProps = defaults;

  public render() {
    const {hours, minutes, headerText, componentTheme, mainHeaderText} = this.props;
    return (
      <PortalComponent componentTheme={componentTheme}>
        <Container className="ifc-servicecountdown-component">
          <h2 className="main-header">{mainHeaderText}</h2>
          <StyledHeader>{headerText}</StyledHeader>
          <StyledTimer>{`${hours} HR : ${minutes} MIN`}</StyledTimer>
        </Container>
      </PortalComponent>
    );
  }
}
export default ServiceCountdown;
