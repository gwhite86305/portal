import * as React from 'react';

// This components renders list elements one by one.
// It passes callback to the list elements to communicate component mounted.
class IterableListRenderer extends React.Component<
  {children: Array<React.ReactElement<any>>},
  {iteratorIndex: number}
> {
  public state = {
    iteratorIndex: 0
  };

  private timeouts: NodeJS.Timer[] = [];

  componentWillUnmount() {
    this.timeouts.forEach(timeout => clearTimeout(timeout));
  }

  render() {
    const {iteratorIndex} = this.state;
    const {children} = this.props;

    const incrementIndexProp = () =>
      (this.timeouts[iteratorIndex] = setTimeout(() => this.setState({iteratorIndex: iteratorIndex + 1}), 0));

    const childrenWithIncrementProp = children.map(child =>
      React.cloneElement(child, {...child.props, iterableRendererCallback: incrementIndexProp})
    );

    const shouldRenderThisItem = (i: number) => i <= iteratorIndex;
    return (
      <div>{React.Children.map(childrenWithIncrementProp, (child, i) => (shouldRenderThisItem(i) ? child : null))}</div>
    );
  }
}

export default IterableListRenderer;
