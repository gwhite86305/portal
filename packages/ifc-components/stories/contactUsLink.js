import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {withNotes} from '@storybook/addon-notes';
import {boolean, number, object, text, withKnobs} from '@storybook/addon-knobs/react';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';
import ContactUsLink from '../dist/components/ContactUsLink/ContactUsLink.js';

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.tertiaryBackgroundColor',
    textColor: 'theme.primaryTextColor',
    dividerColor: 'theme.secondaryBackgroundColor',
    margin: {
      xs: '0 0 0 0'
    },
    padding: {
      xs: '0 0 0 0'
    }
  },
  chatURL:
    'https://inflight.viasat.com/?flightOrigin=KDFW&&flightDestination=KLGA&&flightNumber=XYZ43&&tailId=XYZFREE01&&language=en',
  helpTitle: 'Need Help?',
  chatTitle: 'Chat With Viasat',
  helpAltText: 'Continue to read frequently asked questions',
  chatAltText: 'Continue to live chat or contact viasat customer care',
  feature: {
    showHelp: true,
    helpIcon: './images/help_icon.svg',
    helpUrl: '',
    showChat: true,
    chatIcon: './images/chat_icon.svg'
  }
};

mockTheme.appFontFiles = "url('./fonts/source-sans-pro/SourceSansPro-Light.otf') format('opentype');";

const stories = storiesOf('ContactUsLink', module);

stories.addDecorator(withKnobs);

stories
  .add('CMOB-15664: Default', () => (
    <PortalComponentWrapper mockTheme={mockTheme}>
      <ContactUsLink {...defaultProps} />
    </PortalComponentWrapper>
  ))
  .add('CMOB-15664: Show Chat Only', () => {
    const testProps = {...defaultProps};
    const testFeature = {...defaultProps.feature};

    testFeature.showHelp = false;
    testFeature.helpIcon = './images/help_icon.svg';
    testFeature.helpUrl = '';
    testFeature.showChat = true;
    testFeature.chatIcon = './images/chat_icon.svg';

    return (
      <PortalComponentWrapper mockTheme={mockTheme}>
        <ContactUsLink
          {...defaultProps}
          feature={testFeature}
          helpTitle="Need Help?"
          chatTitle="Chat With Viasat"
          helpAltText="Continue to read frequently asked questions"
          chatAltText="Continue to live chat or contact viasat customer care"
        />
      </PortalComponentWrapper>
    );
  })
  .add(
    'CMOB-15664 With Knob ',
    withNotes('chatURL is internal and the value of it is derived from device endpoints')(() => (
      <PortalComponentWrapper mockTheme={object('theme', mockTheme)}>
        <ContactUsLink
          {...defaultProps}
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          feature={object('feature', defaultProps.feature)}
        />
      </PortalComponentWrapper>
    ))
  );
