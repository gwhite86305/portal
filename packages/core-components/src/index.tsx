import * as React from 'react';
import LoadingReducer from './store/Loading/reducer';
import CookieAlertReducer from './store/CookieAlert/reducer';
import ModalReducer from './store/Modal/reducer';
import {Store} from 'redux';
import LoadingTimeoutPoller from './components/common/LoadingTimeoutPoller';

export * from './components';

// IoC / Decorator
const init = (render: React.ReactElement<any>, store: Store<any>): React.ReactElement<any> => {
  (store as any).attachReducers({
    loading: LoadingReducer,
    cookieAlert: CookieAlertReducer,
    modal: ModalReducer
  });

  return <LoadingTimeoutPoller>{render}</LoadingTimeoutPoller>;
};

export {init};
export default init;
