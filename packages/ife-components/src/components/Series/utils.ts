import {IRootState} from '../..';
import {getSeries, getVideoCategories} from '../../store/videos/actions';
import {selectSeries, selectSeriesCategories} from '../../store/videos/selectors';
import {DispatchProps, StateProps} from './types';
import {trackImpression} from '../../store/analytics/actions';

export const mapStoreToProps = (state: IRootState): StateProps => ({
  storeState: {
    items: selectSeries(state),
    seriesFetchCompleted: state.ifeVideos.seriesFetchCompleted,
    isLoadingSeries: state.ifeVideos.isLoadingSeries,
    seriesCategories: selectSeriesCategories(state)
  }
});
export const mapDispatchToProps = (dispatch: any): DispatchProps => ({
  actions: {
    getSeries: () => dispatch(getSeries()),
    trackImpression: item => dispatch(trackImpression(item)),
    getVideoCategories: () => dispatch(getVideoCategories())
  }
});
