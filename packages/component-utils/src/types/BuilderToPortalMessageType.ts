enum BuilderToPortalMessageType {
  initUx = 'initUx',
  ux = 'ux',
  selectComponent = 'selectComponent',
  routeChange = 'routeChange',
  localeChange = 'localeChange',
  contentChange = 'contentChange'
}

export default BuilderToPortalMessageType;

export interface BuilderToPortalMessageData {
  type: BuilderToPortalMessageType;
  data: any;
}
