
import styled from 'styled-components';
import * as hasTouch from 'has-touch';
import * as React from 'react';
import {fromEvent, Subject} from 'rxjs';
import {map, takeUntil, tap, filter} from 'rxjs/operators';
import {HorizontalRangeProps, HorizontalRangeState} from './types';
import {isIOS} from '../utils/playerCommon';

export const Container = styled.div`
  font-size: ${({textSize}: any) => textSize};
  align-items: center;
  display: flex;
  flex: 1;
  input {
    --max: 100;
    --min: 0;
    --range: calc(var(--max) - var(--min));
    --ratio: calc((var(--val) - var(--min)) / var(--range));
    --sx: calc(0.5 * 1.5em + var(--ratio) * (100% - 1.5em));
    --val: 0;
    background: transparent;
    cursor: pointer;
    flex: 1;
    font: 1em/1 arial, sans-serif;
    height: 1.5em;
    margin: 0;
    padding: 0;
    width: 12.5em;
  }
  input {
    -webkit-appearance: none;
  }
  input::-webkit-slider-runnable-track {
    box-sizing: border-box;
    border: none;
    width: 12.5em;
    height: 0.25em;
    background: ${({backgroundColor}: any) => backgroundColor};
  }
  input::-webkit-slider-runnable-track {
    background: ${({progressColor, backgroundColor}: any) =>
      `linear-gradient(${progressColor}, ${progressColor}) 0/var(--sx) 100% no-repeat ${backgroundColor}`};
  }
  input::-moz-range-track {
    box-sizing: border-box;
    border: none;
    height: 0.25em;
    background: ${({backgroundColor}: any) => backgroundColor};
  }
  input::-ms-track {
    box-sizing: border-box;
    border: none;
    height: 0.25em;
    color:transparent
    background: ${({backgroundColor}: any) => backgroundColor};
  }
  input::-moz-range-progress {
    height: 0.25em;
    background: ${({progressColor}: any) => progressColor};
  }
  input::-ms-fill-lower {
    height: 0.25em;
    background: ${({progressColor}: any) => progressColor};
  }

  input::-webkit-slider-thumb {
    -webkit-appearance: none;
    margin-top: -0.4em;
    box-sizing: border-box;
    border: none;
    width: 1em;
    height: 1em;
    border-radius: 50%;
    background: ${({thumbColor}: any) => thumbColor};
    transition: transform 100ms;
    ${({blockFastForward}: any) => (blockFastForward ? 'display: none' : '')};
  }
  input::-moz-range-thumb {
    box-sizing: border-box;
    border: none;
    width: 1em;
    height: 1em;
    border-radius: 50%;
    background: ${({thumbColor}: any) => thumbColor};
    transition: transform 100ms;
  }
  input::-ms-thumb {
    margin-top: 0;
    box-sizing: border-box;
    border: none;
    width: 1em;
    height: 1em;
    border-radius: 50%;
    background: ${({thumbColor}: any) => thumbColor};
    transition: transform 100ms;
  }

  input::-webkit-slider-thumb:hover {
    transform: scale(1.4);
  }

  input::-moz-range-thumb:hover {
    transform: scale(1.4);
  }

  input::-ms-tooltip {
    display: none;
  }
`;

export default class HorizontalRange extends React.PureComponent<HorizontalRangeProps, HorizontalRangeState> {
  destroy$ = new Subject();
  nodeRef: any;
  state = {progressPercentage: 0};
  elementRect: ClientRect | DOMRect;
  constructor(props: any) {
    super(props);
    this.nodeRef = React.createRef();
  }

  componentDidMount() {
    // Tap to seek, only for mobile
    hasTouch &&
      fromEvent(this.nodeRef.current, 'click')
        .pipe(
          filter(() => !this.props.blockFastForward),
          tap(_ => (this.elementRect = (this.nodeRef.current as HTMLInputElement).getBoundingClientRect())),
          map((e: MouseEvent) => (e.screenX - this.elementRect.left) * 100 / (this.elementRect.width + 10)),
          tap((progressPercentage: number) => this.setState({progressPercentage})),
          takeUntil(this.destroy$)
        )
        .subscribe((progressPercentage: number) => this.props.onSeekToRange(progressPercentage));
  }

  componentDidUpdate(prevProps: HorizontalRangeProps) {
    if (!this.props.duration || this.props.currentTime === prevProps.currentTime) return;
    const progressPercentage = this.props.currentTime / this.props.duration * 100;
    this.setState({progressPercentage});
  }

  componentWillUnmount() {
    this.destroy$.next(undefined);
    this.destroy$.complete();
  }

  render() {
    const extraProps = {
      thumbColor: this.props.progressColor,
      progressColor: this.props.progressColor,
      backgroundColor: this.props.progressBackgroundColor,
      textSize: this.props.progressTextSize,
      blockFastForward: this.props.blockFastForward
    };
    return (
      <Container
        {...extraProps}
        children={
          <input
            readOnly={!isIOS}
            step="0.001"
            aria-label="Progress"
            value={this.state.progressPercentage.toFixed(3)}
            style={{'--val': this.state.progressPercentage} as any}
            ref={this.nodeRef}
            onChange={this.handleChange}
            type="range"
          />
        }
      />
    );
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    if (this.props.blockFastForward) {
      return;
    }
    const progressPercentage = parseFloat(event.currentTarget.value);
    this.setState({progressPercentage});
    this.props.onSeekToRange(progressPercentage);
  };
}
