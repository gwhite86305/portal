import React from 'react';
import {storiesOf} from '@storybook/react';
import {withKnobs, object, text, boolean} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import MockRouter from 'react-mock-router';
import FYI from '../dist/components/FYI/FYI';
import PortalComponentWrapper, {mockThemeNeos} from 'component-utils/dist/utils/PortalComponentWrapper';

const messagesNeos = {
  it: {
    'ifc.FYI.header.text': 'Informazioni utilizzo Wi-Fi',
    'ifc.FYI.subHeader.text': [
      'Duerante i voli Neos grali al Wi-Fi bordo potrai rimanere connesso, con i tuoi amici, familiari, colleghi e clienti.',
      'Il servizio Wi-Fi e utiliazzbile in qualiasi momento se le rete e disponible. Dedice tu quando connetterti e quando scollegarti dal Wi-Fi in modo da non perdere i minuti che hai acquistato.',
      'Ti ricordiamo inoltre che la copertura del servizio Wi-Fi e disponible solamente in aclune visione delle aree nella mappa.'
    ],
    'ifc.FYI.button.text': 'ACCETTA',
    'ifc.FYI.altTextForImage.text': 'Coverage Map'
  }
};

const defaultMessagesNeos = {
  it: {
    'ifc.FYI.header.text': 'Title',
    'ifc.FYI.subHeader.text': ['Description'],
    'ifc.FYI.button.text': 'Button',
    'ifc.FYI.altTextForImage.text': 'Alt Text For Image'
  }
};

const noHeaderMessageNeos = {
  it: {
    'ifc.FYI.header.text': '',
    'ifc.FYI.subHeader.text': ['Description'],
    'ifc.FYI.button.text': 'Button',
    'ifc.FYI.altTextForImage.text': 'Alt Text For Image'
  }
};

const noSubHeaderMessageNeos = {
  it: {
    'ifc.FYI.header.text': 'Title',
    'ifc.FYI.subHeader.text': [''],
    'ifc.FYI.button.text': 'Button',
    'ifc.FYI.altTextForImage.text': 'Alt Text For Image'
  }
};

const defaultProps = {
  componentTheme: {
    fontFamily: 'theme.appFontFamily',
    headerTextColor: 'theme.primaryTextColor',
    subHeaderTextColor: 'theme.primaryTextColor'
  },
  feature: {
    image: {
      xs: './images/coverage_map.jpg',
      sm: './images/coverage_map.jpg',
      md: './images/coverage_map.jpg',
      lg: './images/coverage_map.jpg',
      xl: './images/coverage_map.jpg'
    },
    openPageOnClick: 'Checkout_page'
  }
};

const neosProps = {
  componentTheme: {
    backgroundColor: '#ffffff',
    display: {
      xs: true
    }
  },
  feature: {
    image: {
      xs: './images/coverage_map.jpg',
      sm: './images/coverage_map.jpg',
      md: './images/coverage_map.jpg',
      lg: './images/coverage_map.jpg',
      xl: './images/coverage_map.jpg'
    },
    openPageOnClick: 'Checkout_page'
  }
};

const modifiedMockTheme_1 = {
  componentTheme: {
    backgroundColor: '#22313F',
    fontFamily: 'SourceSansPro',
    headerTextColor: 'white',
    subHeaderTextColor: 'white'
  }
};

const stories = storiesOf('[CMOB-20980]-FYI - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('FYI 1 - Default Props', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockLocale="it" mockTheme={mockThemeNeos} mockMessages={defaultMessagesNeos}>
        <FYI {...defaultProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI - Neos Theme', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockLocale="it" mockTheme={mockThemeNeos} mockMessages={messagesNeos}>
        <FYI {...neosProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI - Modified Theme', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockLocale="it" mockTheme={mockThemeNeos} mockMessages={defaultMessagesNeos}>
        <FYI {...neosProps} componentTheme={object('componentTheme', modifiedMockTheme_1.componentTheme)} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI - Test No Header Text Configurability', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockLocale="it" mockTheme={mockThemeNeos} mockMessages={noHeaderMessageNeos}>
        <FYI {...defaultProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI - Test No Sub Header Text Configurability', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper mockLocale="it" mockTheme={mockThemeNeos} mockMessages={noSubHeaderMessageNeos}>
        <FYI {...defaultProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI 2 - Test Text Configurability', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper
        mockLocale="it"
        mockTheme={mockThemeNeos}
        mockMessages={object('Localized messages', messagesNeos)}
      >
        <FYI {...defaultProps} />
      </PortalComponentWrapper>
    </MockRouter>
  );
});

stories.add('FYI - Neos Theme Test All Props', () => {
  return (
    <MockRouter location="/home">
      <PortalComponentWrapper
        mockLocale="it"
        mockTheme={object('theme', mockThemeNeos)}
        mockMessages={object('Localized messages', messagesNeos)}
      >
        <FYI
          componentTheme={object('componentTheme', defaultProps.componentTheme)}
          feature={object('Image Configuration', neosProps.feature)}
          openPageOnSelection={text('Page Selection', neosProps.openPageOnSelection)}
        />
      </PortalComponentWrapper>
    </MockRouter>
  );
});
