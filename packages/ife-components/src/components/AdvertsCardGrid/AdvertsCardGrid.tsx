import * as defaultProps from './defaultProps.json';
import * as React from 'react';
import {
  AdCta,
  AdDescriptionSection,
  AdIcon,
  AdItemBackground,
  AdItemText,
  StyledAdvertsGrid
} from './StyledAdvertsGrid';
import {AdvertItem} from '../../store/adverts/types';
import {AdvertsCardGridProps} from './types';

export default class AdvertsCardGrid extends React.Component<AdvertsCardGridProps> {
  static defaultProps = defaultProps;
  elementRefs: HTMLDivElement[];
  observer: IntersectionObserver;
  containerRef: any;

  constructor(props: AdvertsCardGridProps) {
    super(props);
    this.elementRefs = [];
  }

  componentDidMount() {
    const config = {
      root: this.containerRef.current,
      rootMargin: '0px',
      threshold: 0.5
    };
    this.observer = new IntersectionObserver(this.observeEntries, config);
    this.elementRefs.forEach((entry: Element) => this.observer.observe(entry));
  }

  componentWillUnmount() {
    this.observer && this.observer.disconnect();
  }

  observeEntries = (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => {
    const {onItemInViewPort, items} = this.props;
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        const item = items.find((ad: AdvertItem) => ad.uid === entry.target.id);
        onItemInViewPort && onItemInViewPort(item!);
        observer.unobserve(entry.target);
      }
    });
  };
  getImgUrlForResolution = (props: any) => {
    //TODO: Next task is to implement responsive image detection
    const size = 'original';
    return props && props[0] && props[0][size] && props[0][size].url;
  };

  render() {
    const {items, onClick} = this.props;
    return (
      <StyledAdvertsGrid ref={(ref: any) => (this.containerRef = ref)}>
        {items.map((ad: AdvertItem) => {
          return (
            <div
              id={ad.uid}
              key={ad.uid}
              className="item-container"
              ref={(elementRef: HTMLDivElement) => this.elementRefs.push(elementRef)}
            >
              <AdItemBackground {...{imgUrl: this.getImgUrlForResolution(ad.titleImages)}}>
                <AdDescriptionSection description={ad.description} targetUrl={ad.targetUrl} attributes={ad.attributes}>
                  <AdCta ad={ad} onClick={onClick} />
                </AdDescriptionSection>
                <AdItemText title={ad.title} subtitle={ad.subtitle} />
                <AdIcon {...{imgUrl: this.getImgUrlForResolution(ad.galleryImages)}} />
              </AdItemBackground>
            </div>
          );
        })}
      </StyledAdvertsGrid>
    );
  }
}
