import * as React from 'react';
import Template from '../Template/Template';
import {Page as ExternalInterface} from '../common/ComponentsInterface';
import PortalComponent from 'component-utils/dist/components/PortalComponent';
import {ContainerComponentThemeWrapper} from 'component-utils/dist/utils/ComponentUtils';
import {LayoutContext} from 'component-utils/dist/utils/Contexts';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import {createComponent} from '../../utils/LoaderCreateComponent';
import * as defaults from './defaultProps.json';
import LayoutInterface, {Components} from 'component-utils/dist/types/LayoutInterface';

export interface PageProps {
  children?: string[];
}

/*
    The UX Builder will impose an artificial restriction on component hierarchy, namely:
      * page must container grid container as child
      * grid container must contain grid item (or other component?)
      * details to be worked out later...
 */

const _generateLayout = (children: string[], components: Components): any =>
  children.map((componentUid: string) => createComponent(componentUid, {}, components, null));

const Page: React.FunctionComponent<PageProps & ExternalInterface> = (props, context) => {
  const {feature, componentTheme, children, pageTitleText} = props;
  const {components}: LayoutInterface = React.useContext(LayoutContext) as LayoutInterface;

  // on mount
  React.useEffect(() => {
    document.title = pageTitleText || feature.name;
  }, []);

  let pageComponentHierarchy = (
    <PortalComponent componentTheme={componentTheme}>
      <ContainerComponentThemeWrapper>{_generateLayout(children || [], components)}</ContainerComponentThemeWrapper>
    </PortalComponent>
  );

  return feature.template ? (
    <Template {...components[feature.template].properties} templateBodyComponent={pageComponentHierarchy} />
  ) : (
    pageComponentHierarchy
  );
};

Page.defaultProps = defaults;

export default withLocaleProps(Page, [{pageTitleText: 'system.Page.page.title'}]) as Page<
  PageProps & ExternalInterface & {pageTitleText: string}
>;
