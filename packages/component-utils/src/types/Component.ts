export interface ComponentProperties {
  children?: string[];
  componentTheme?: object;
  config?: any[];
  feature: any;

  [propName: string]: any;
}

export interface Component {
  componentName: string;
  componentLib: string;
  properties: ComponentProperties;
}
