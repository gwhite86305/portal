import {Action, ActionCreators} from './actions';

export type State = {
  readonly errorMessage: string | null;
};

export const initialState: State = {
  errorMessage: null
};

export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State> | undefined;
  switch (action.type) {
    case ActionCreators.purchaseOptionSelected.type:
    case ActionCreators.voucherOptionSelected.type:
    case ActionCreators.subscriptionOptionSelected.type:
    case ActionCreators.loginOptionSelected.type:
    case ActionCreators.voucherValidationStart.type:
    case ActionCreators.voucherValidationSuccess.type:
    case ActionCreators.voucherValidationError.type:
    case ActionCreators.signupWithVoucherStart.type:
    case ActionCreators.signupWithVoucherSuccess.type:
    case ActionCreators.signupWithVoucherError.type:
    case ActionCreators.signupWithPurchaseStart.type:
    case ActionCreators.signupWithPurchaseSuccess.type:
    case ActionCreators.signupWithPurchaseError.type:
    case ActionCreators.deviceSwapStart.type:
    case ActionCreators.deviceSwapSuccess.type:
    case ActionCreators.deviceSwapError.type:
    case ActionCreators.deviceSwapped.type:
    case ActionCreators.orderHistoryStart.type:
    case ActionCreators.orderHistorySuccess.type:
    case ActionCreators.orderHistoryError.type:
    case ActionCreators.resetVoucher.type:
    case ActionCreators.resetSignup.type:
    case ActionCreators.resetSwapDevice.type:
    case ActionCreators.signupFreeStart.type:
    case ActionCreators.signupFreeSuccess.type:
    case ActionCreators.signupFreeError.type:
      partialState = {...action.payload};
      break;
    case ActionCreators.messagingOptionSelected.type:
      partialState = {...action.payload};
      break;
    case ActionCreators.signupWithMessagingStart.type:
    case ActionCreators.signupWithMessagingSuccess.type:
      partialState = {...action.payload};
      break;
    case ActionCreators.signupWithMessagingError.type:
    default:
      return state;
  }

  return {...state, ...partialState};
}
