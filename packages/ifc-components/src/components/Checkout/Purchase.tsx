import * as React from 'react';
import {PortalComponent, ResponsiveImage, sanitize, utils, Currency} from 'component-utils';
import {Button, Spinner} from 'core-components';
import * as defaults from './defaultProps.json';
import {validEmail, confirmEmailMatch} from 'component-utils/dist/utils/Validator';

import styled from 'styled-components';
const {CommonComponentThemeWrapper} = utils.ComponentUtils;
const {applyMediaQueriesForStyle, renderThemeProperty} = utils.styledComponents;

export interface InternalInterface {
  className?: string;
  premiumTiers?: any;
  WifiServiceOptions?: any;
  onPurchaseSubmit?: any;
  componentTheme?: any;
  purchaseBodyText?: string;
  purchaseEmailLabel?: string;
  emailConfirmationLabel?: string;
  purchaseEmailErrorHelpText?: string;
  purchaseNoteText?: string;
  feature?: any;
  purchaseAltTextForNoteImage?: string;
  purchaseTotalLabel?: string;
  purchaseSubmitButtonText?: string;
  errorDict?: any;
  currencyCode?: string;
  currencySuffix?: any;
  locale?: string;
  minimumFractionDigits?: string;
  maximumFractionDigits?: string;
}

const Container = styled(CommonComponentThemeWrapper)`
  form.checkout-purchase-form {
    margin: auto;
    max-width: 688px;
    .header {
      ul {
        padding: 0px;
        padding-left: ${(props: any) => props.theme.gutter};
      }
    }
    h1.header-text {
      margin: 0px;
      ${renderThemeProperty('purchaseOptionHeaderMarginTop', 'margin-top')};
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 2.5);
      ${renderThemeProperty('purchaseOptionHeaderFontFamily', 'font-family')};
      ${renderThemeProperty('purchaseOptionHeaderColor', 'color')};
      ${renderThemeProperty('purchaseOptionHeaderFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.04;
    }
    div.body-text {
      margin: 0px;
      ${renderThemeProperty('purchaseOptionBodyTextColor', 'color')};
      ${renderThemeProperty('purchaseOptionBodyTextFontSize', 'font-size')};
      font-weight: normal;
      line-height: 1.187;
      a {
        color: ${(props: any) => props.theme.secondaryTextColor};
      }
    }
    p.validation-summary {
      color: ${(props: any) => props.theme.formErrorColor};
      margin: 0px;
      margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
      font-weight: bold;
      display: inline-block;
    }
    div.input-wrapper {
      margin-bottom: calc(${(props: any) => props.theme.gutter} * 1.25);
      label {
        display: inline-block;
        word-wrap: break-word;
        ${renderThemeProperty('purchaseOptionInputLabelColor', 'color')};
        ${renderThemeProperty('purchaseOptionInputLabelFontSize', 'font-size')};
        margin: 0px;
        margin-left: calc(${(props: any) => props.theme.gutter} * 0.5);
        line-height: 1.25;
        margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
      }
      input {
        font-family: ${(props: any) => props.theme.appFontFamily};
        width: 100%;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        font-weight: 100;
        margin-top: calc(${(props: any) => props.theme.gutter} * 0.25);
        display: block;
        width: 100%;
        height: calc(${(props: any) => props.theme.gutter} * 2.5625);
        padding: calc(${(props: any) => props.theme.gutter} * 0.375) calc(${(props: any) => props.theme.gutter} * 0.625);
        font-size: ${(props: any) => props.theme.tertiaryFontSize};
        line-height: 1.42857;
        color: ${(props: any) => props.theme.primaryTextColor};
        border: 1px solid;
        border-color: ${(props: any) => props.theme.senaryBackgroundColor};
      }
      input:focus {
        border-color: ${(props: any) => props.theme.secondaryTextColor};
        color: ${(props: any) => props.theme.secondaryTextColor};
        border: 2px solid;
      }
      .validation {
        position: relative;
        padding: calc(${(props: any) => props.theme.gutter} * 0.5) calc(${(props: any) => props.theme.gutter} * 0.625)
          calc(${(props: any) => props.theme.gutter} * 0.5) calc(${(props: any) => props.theme.gutter} * 0.625);
        div.opacity-background {
          background-color: ${(props: any) => props.theme.formErrorColor};
          opacity: 0.14;
          position: absolute;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
        }
        p.help-text {
          margin: 0px;
          line-height: 1.25;
          margin-bottom: calc(${(props: any) => props.theme.gutter} * 0.3125);
        }
        p.message {
          color: ${(props: any) => props.theme.formErrorColor};
          margin: 0px;
          font-weight: bold;
          line-height: 1.25;
        }
        div.circle {
          text-align: right;
          height: 0px;
          span {
            position: relative;
            top: calc(${(props: any) => props.theme.gutter} * -3.75);
            background-color: ${(props: any) => props.theme.formErrorColor};
            width: calc(${(props: any) => props.theme.gutter} * 1.375);
            height: calc(${(props: any) => props.theme.gutter} * 1.375);
            line-height: 1.375;
            display: inline-block;
            text-align: center;
            justify-content: center;
            border-radius: calc(${(props: any) => props.theme.gutter} * 1.375);
            color: ${(props: any) => props.theme.tertiaryTextColor};
          }
        }
      }
    }
    div.button-wrap {
      ${(props: any) =>
        applyMediaQueriesForStyle(
          'margin-top',
          {xs: `calc(${props.theme.gutter}*1.875)`, md: `calc(${props.theme.gutter}*1.875)`},
          props.theme
        )};
      text-align: center;
      button {
        ${renderThemeProperty('purchaseOptionButtonWidth', 'width')};
        ${renderThemeProperty('purchaseOptionButtonHeight', 'height')};
        ${renderThemeProperty('purchaseOptionButtonBorder', 'border')};
        ${renderThemeProperty('purchaseOptionButtonFontSize', 'font-size')};
        ${renderThemeProperty('purchaseOptionButtonFontWeight', 'font-weight')};
        margin: 0 auto;
        white-space: nowrap;
        justify-content: center;
      }
    }
    img {
      margin-top: calc(${(props: any) => props.theme.gutter} * 1.25);
    }
    p.note {
      ${renderThemeProperty('purchaseOptionNoteColor', 'color')};
      ${renderThemeProperty('purchaseOptionNoteFontSize', 'font-size')};
      margin: 0px;
      padding-top: calc(${(props: any) => props.theme.gutter} * 0.625);
    }
    hr {
      height: 0;
      border: 0;
      border-top: 1px solid;
      ${renderThemeProperty('subTotalLineColor', 'border-color')};
      margin-top: calc(${(props: any) => props.theme.gutter} * 1.312);
      padding-bottom: calc(${(props: any) => props.theme.gutter} * 0.875);
    }
    .total {
      display: flex;
      justify-content: space-between;
      .label {
        ${renderThemeProperty('subTotalLabelFontSize', 'font-size')};
        ${renderThemeProperty('subTotalLabelFontWeight', 'font-weight')};
        ${renderThemeProperty('subTotalLabelColor', 'color')};
      }
      .cost {
        ${renderThemeProperty('subTotalCostFontSize', 'font-size')};
        ${renderThemeProperty('subTotalCostFontWeight', 'font-weight')};
        ${renderThemeProperty('subTotalCostColor', 'color')};
      }
    }
  }
`;

class Purchase extends React.Component<InternalInterface, any> {
  public static defaultProps = defaults;

  constructor(props: any) {
    super(props);
    this.state = {
      formErrors: {},
      email: '',
      confirmEmail: ''
    };
    this._handlePurchaseSubmit = this._handlePurchaseSubmit.bind(this);
  }

  _handleFormChange(fieldName: string, fieldValue: string) {
    let stateUpdate = {[fieldName]: fieldValue.trim()};
    this.setState(stateUpdate);
  }

  _handlePurchaseSubmit(event: any) {
    event.preventDefault();
    const {email, confirmEmail} = this.state;
    const {errorDict, feature} = this.props;
    let formErrors: any = {};
    if (!validEmail(email)) {
      formErrors['email'] = errorDict['emailRequired'];
    }
    if (feature.addEmailConfirmationField && !confirmEmailMatch(email, confirmEmail)) {
      formErrors['confirmEmail'] = errorDict['emailMismatch'];
    }
    this.setState({formErrors});
    const selectedTier =
      this.props.premiumTiers.filter((tier: any) => tier.id == this.props.WifiServiceOptions.selected.tierId)[0] || {};
    if (Object.keys(formErrors).length == 0) {
      this.props.onPurchaseSubmit(email, selectedTier.id);
    }
  }

  public render() {
    const {
      premiumTiers,
      WifiServiceOptions,
      errorDict,
      purchaseBodyText,
      purchaseEmailLabel,
      purchaseEmailErrorHelpText,
      purchaseNoteText,
      feature,
      purchaseAltTextForNoteImage,
      purchaseTotalLabel,
      emailConfirmationLabel,
      purchaseSubmitButtonText,
      currencyCode,
      currencySuffix,
      locale,
      minimumFractionDigits,
      maximumFractionDigits
    } = this.props;
    let {formErrors} = this.state;
    const selectedTier = premiumTiers.filter((tier: any) => tier.id == WifiServiceOptions.selected.tierId)[0] || {};
    if (
      WifiServiceOptions.signup &&
      (WifiServiceOptions.signup.error || WifiServiceOptions.signup.action == 'reject')
    ) {
      formErrors['email'] =
        formErrors['email'] ||
        (WifiServiceOptions.signup && errorDict[WifiServiceOptions.signup.code]) ||
        errorDict['generic'];
    }
    const keepSpinner =
      (WifiServiceOptions.signup && WifiServiceOptions.signup.waitingForResponse) ||
      (WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'redirect') ||
      (WifiServiceOptions.signup && WifiServiceOptions.signup.action == 'accept');
    return (
      <PortalComponent componentTheme={this.props.componentTheme}>
        <Container>
          {keepSpinner ? <Spinner /> : null}
          <form className="checkout-purchase-form" onSubmit={this._handlePurchaseSubmit}>
            <div className="header">
              {selectedTier.description ? <h1 className="header-text">{selectedTier.description}</h1> : null}
              {purchaseBodyText ? (
                <div className="body-text" dangerouslySetInnerHTML={sanitize(purchaseBodyText)} />
              ) : null}
            </div>

            {Object.keys(formErrors).length ? (
              <p className="validation-summary">{errorDict['errorHeaderText']}</p>
            ) : null}
            <div className="input-wrapper">
              <label htmlFor="email">{purchaseEmailLabel}</label>
              <input
                className="input-text"
                name="email"
                id="email"
                type="text"
                onChange={e => this._handleFormChange('email', e.target.value)}
              />
              {formErrors.email ? (
                <div className="validation">
                  <div className="opacity-background" />
                  <div className="circle">
                    <span>!</span>
                  </div>

                  {purchaseEmailErrorHelpText ? <p className="help-text">{purchaseEmailErrorHelpText}</p> : null}
                  <p className="message">{formErrors.email}</p>
                </div>
              ) : null}
              {feature.addEmailConfirmationField ? (
                <React.Fragment>
                  <label htmlFor="email">{emailConfirmationLabel}</label>
                  <input
                    className="input-text"
                    name="confirmEmail"
                    id="confirmEmail"
                    type="text"
                    onChange={e => this._handleFormChange('confirmEmail', e.target.value)}
                  />
                  {formErrors.confirmEmail ? (
                    <div className="validation">
                      <div className="opacity-background" />
                      <div className="circle">
                        <span>!</span>
                      </div>
                      <p className="message">{formErrors.confirmEmail}</p>
                    </div>
                  ) : null}
                </React.Fragment>
              ) : null}
            </div>
            {purchaseNoteText ? <p className="note">{purchaseNoteText}</p> : null}
            {feature.noteImage ? (
              <ResponsiveImage altText={purchaseAltTextForNoteImage} image={feature.noteImage} />
            ) : null}
            <hr />
            <div className="total">
              <span className="label">{purchaseTotalLabel}</span>
              <span className="cost">
                <Currency
                  currency={currencyCode}
                  value={selectedTier.cost}
                  locale={locale}
                  minimumFractionDigits={minimumFractionDigits}
                  maximumFractionDigits={maximumFractionDigits}
                  currencySuffix={currencySuffix}
                />
              </span>
            </div>
            <div className="button-wrap">
              <Button isPrimary={true} onClick={this._handlePurchaseSubmit} aria-label={purchaseSubmitButtonText}>
                {purchaseSubmitButtonText}
              </Button>
            </div>
          </form>
        </Container>
      </PortalComponent>
    );
  }
}

export default Purchase;
