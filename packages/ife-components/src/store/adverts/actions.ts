import {ActionCreator} from 'component-utils/dist/store/action-creator';
import {AdvertItem} from './types';
import {axiosWithBaseUrl as axios} from '../../utils/utils';
import {Dispatch} from 'redux';
import {IAdvertsState as State} from './reducer';
export const ActionCreators = {
  getAdvertsStart: new ActionCreator<'getAdvertsStart', any>('getAdvertsStart'),
  getAdvertsSuccess: new ActionCreator<'getAdvertsSuccess', any>('getAdvertsSuccess'),
  getAdvertsError: new ActionCreator<'getAdvertsError', any>('getAdvertsError')
};
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function getAdverts() {
  return (dispatch: Dispatch<any>): void => {
    dispatch(ActionCreators.getAdvertsStart.create({}));
    axios
      .get('/content/ads')
      .then(res => {
        const {
          data: {items}
        } = res;
        dispatch(ActionCreators.getAdvertsSuccess.create(items as AdvertItem[]));
      })
      .catch(_ => {
        dispatch(ActionCreators.getAdvertsError.create({}));
      });
  };
}
