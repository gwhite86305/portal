import React from 'react';
import {storiesOf} from '@storybook/react';
import {boolean, object, text, withKnobs} from '@storybook/addon-knobs/react';
import {checkA11y} from '@storybook/addon-a11y';
import QuickLinks from '../dist/components/QuickLinks/QuickLinks';
import PortalComponentWrapper, {mockTheme} from 'component-utils/dist/utils/PortalComponentWrapper';

const defaultProps = {
  componentTheme: {
    backgroundColor: 'theme.primaryBackgroundColor',
    fontSize: 'theme.defaultFontSize',
    fontWeight: '800',
    textHoverColor: 'theme.tertiaryIconColor',
    textActiveColor: 'theme.primaryButtonActiveColor',
    gutter: '24px'
  },
  feature: {
    itemsPerRow: 5,
    menuItems: [
      {
        iconHeight: '',
        iconUrl: '/images/facebook-square - FontAwesome.svg',
        url: 'http://facebook.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e096',
        labels: {
          en: {
            label: 'Facebook',
            iconAltText: 'Visit Facebook'
          },
          it: {
            label: 'Facebook',
            iconAltText: 'Visita Facebook'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/reddit-square - FontAwesome.svg',
        url: 'http://reddit.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e095',
        labels: {
          en: {
            label: 'Reddit',
            iconAltText: 'Visit Reddit'
          },
          it: {
            label: 'Reddit',
            iconAltText: 'Visita Reddit'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/instagram-square - FontAwesome.svg',
        url: 'http://instagram.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e098',
        labels: {
          en: {
            label: 'Instagram',
            iconAltText: 'Visit Instagram'
          },
          it: {
            label: 'Instagram',
            iconAltText: 'Visita Instagram'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/youtube-square - FontAwesome.svg',
        url: 'http://youtube.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e091',
        labels: {
          en: {
            label: 'YouTube',
            iconAltText: 'Visit YouTube'
          },
          it: {
            label: 'YouTube',
            iconAltText: 'Visita YouTube'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/twitter-square - FontAwesome.svg',
        url: 'http://twitter.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e094',
        labels: {
          en: {
            label: 'Twitter',
            iconAltText: 'Visit Twitter'
          },
          it: {
            label: 'Twitter',
            iconAltText: 'Visita Twitter'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/flickr-square - FontAwesome.svg',
        url: 'http://flickr.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e099',
        labels: {
          en: {
            label: 'Flickr',
            iconAltText: 'Visit Flickr'
          },
          it: {
            label: 'Flickr',
            iconAltText: 'Visita Flickr'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/pinterest-square - FontAwesome.svg',
        url: 'http://pinterest.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e092',
        labels: {
          en: {
            label: 'Pinterest',
            iconAltText: 'Visit Pinterest'
          },
          it: {
            label: 'Pinterest',
            iconAltText: 'Visita Pinterest'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/google-plus-square - FontAwesome.svg',
        url: 'http://google.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e097',
        labels: {
          en: {
            label: 'Google Plus',
            iconAltText: 'Visit Google Plus'
          },
          it: {
            label: 'Google Plus',
            iconAltText: 'Visita Google Plus'
          }
        }
      },
      {
        iconHeight: '',
        iconUrl: '/images/linkedin-square - FontAwesome.svg',
        url: 'http://linkedin.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e093',
        labels: {
          en: {
            label: 'LinkedIn',
            iconAltText: 'Visit LinkedIn'
          },
          it: {
            label: 'LinkedIn',
            iconAltText: 'Visita LinkedIn'
          }
        }
      }
    ]
  }
};

const stories = storiesOf('[CMOB-22377]-QuickLinks - Component', module);
stories.addDecorator(withKnobs);
stories.addDecorator(checkA11y);
stories.add('QuickLinks 1 - Default Props', () => {
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <QuickLinks
        componentTheme={object('componentTheme', defaultProps.componentTheme)}
        feature={object('feature', defaultProps.feature)}
      />
    </PortalComponentWrapper>
  );
});

stories.add('QuickLinks 1 - Modified ItemsPerRow Props', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    itemsPerRow: 9,
    menuItems: defaultProps.feature.menuItems
  };
  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <QuickLinks
        componentTheme={object('componentTheme', testProps.componentTheme)}
        feature={object('feature', testProps.feature)}
      />
    </PortalComponentWrapper>
  );
});

stories.add('QuickLinks 1 - Modified IconHeight Props', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    itemsPerRow: 5,
    menuItems: [
      {
        iconHeight: '50px',
        iconUrl: '/images/facebook-square - FontAwesome.svg',
        url: 'http://facebook.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e096',
        labels: {
          en: {
            label: 'Facebook',
            iconAltText: 'Visit Facebook'
          },
          it: {
            label: 'Facebook',
            iconAltText: 'Visita Facebook'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/reddit-square - FontAwesome.svg',
        url: 'http://reddit.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e095',
        labels: {
          en: {
            label: 'Reddit',
            iconAltText: 'Visit Reddit'
          },
          it: {
            label: 'Reddit',
            iconAltText: 'Visita Reddit'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/instagram-square - FontAwesome.svg',
        url: 'http://instagram.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e098',
        labels: {
          en: {
            label: 'Instagram',
            iconAltText: 'Visit Instagram'
          },
          it: {
            label: 'Instagram',
            iconAltText: 'Visita Instagram'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/youtube-square - FontAwesome.svg',
        url: 'http://youtube.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e091',
        labels: {
          en: {
            label: 'YouTube',
            iconAltText: 'Visit YouTube'
          },
          it: {
            label: 'YouTube',
            iconAltText: 'Visita YouTube'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/twitter-square - FontAwesome.svg',
        url: 'http://twitter.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e094',
        labels: {
          en: {
            label: 'Twitter',
            iconAltText: 'Visit Twitter'
          },
          it: {
            label: 'Twitter',
            iconAltText: 'Visita Twitter'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/flickr-square - FontAwesome.svg',
        url: 'http://flickr.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e099',
        labels: {
          en: {
            label: 'Flickr',
            iconAltText: 'Visit Flickr'
          },
          it: {
            label: 'Flickr',
            iconAltText: 'Visita Flickr'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/pinterest-square - FontAwesome.svg',
        url: 'http://pinterest.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e092',
        labels: {
          en: {
            label: 'Pinterest',
            iconAltText: 'Visit Pinterest'
          },
          it: {
            label: 'Pinterest',
            iconAltText: 'Visita Pinterest'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/google-plus-square - FontAwesome.svg',
        url: 'http://google.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e097',
        labels: {
          en: {
            label: 'Google Plus',
            iconAltText: 'Visit Google Plus'
          },
          it: {
            label: 'Google Plus',
            iconAltText: 'Visita Google Plus'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/linkedin-square - FontAwesome.svg',
        url: 'http://linkedin.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e093',
        labels: {
          en: {
            label: 'LinkedIn',
            iconAltText: 'Visit LinkedIn'
          },
          it: {
            label: 'LinkedIn',
            iconAltText: 'Visita LinkedIn'
          }
        }
      }
    ]
  };

  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <QuickLinks
        componentTheme={object('componentTheme', testProps.componentTheme)}
        feature={object('feature', testProps.feature)}
      />
    </PortalComponentWrapper>
  );
});

stories.add('QuickLinks 1 - Modified Number of Icons', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    itemsPerRow: 3,
    menuItems: [
      {
        iconHeight: '150px',
        iconUrl: '/images/facebook-square - FontAwesome.svg',
        url: 'http://facebook.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e096',
        labels: {
          en: {
            label: 'Facebook',
            iconAltText: 'Visit Facebook'
          },
          it: {
            label: 'Facebook',
            iconAltText: 'Visita Facebook'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/reddit-square - FontAwesome.svg',
        url: 'http://reddit.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e095',
        labels: {
          en: {
            label: 'Reddit',
            iconAltText: 'Visit Reddit'
          },
          it: {
            label: 'Reddit',
            iconAltText: 'Visita Reddit'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/instagram-square - FontAwesome.svg',
        url: 'http://instagram.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e098',
        labels: {
          en: {
            label: 'Instagram',
            iconAltText: 'Visit Instagram'
          },
          it: {
            label: 'Instagram',
            iconAltText: 'Visita Instagram'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/youtube-square - FontAwesome.svg',
        url: 'http://youtube.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e091',
        labels: {
          en: {
            label: 'YouTube',
            iconAltText: 'Visit YouTube'
          },
          it: {
            label: 'YouTube',
            iconAltText: 'Visita YouTube'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/twitter-square - FontAwesome.svg',
        url: 'http://twitter.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e094',
        labels: {
          en: {
            label: 'Twitter',
            iconAltText: 'Visit Twitter'
          },
          it: {
            label: 'Twitter',
            iconAltText: 'Visita Twitter'
          }
        }
      }
    ]
  };

  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <QuickLinks
        componentTheme={object('componentTheme', testProps.componentTheme)}
        feature={object('feature', testProps.feature)}
      />
    </PortalComponentWrapper>
  );
});

stories.add('QuickLinks 1 - Uneven Heighted Icons', () => {
  const testProps = {...defaultProps};
  testProps.feature = {
    itemsPerRow: 3,
    menuItems: [
      {
        iconHeight: '150px',
        iconUrl: '/images/facebook-square - FontAwesome.svg',
        url: 'http://facebook.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e096',
        labels: {
          en: {
            label: 'Facebook',
            iconAltText: 'Visit Facebook'
          },
          it: {
            label: 'Facebook',
            iconAltText: 'Visita Facebook'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/reddit-square - FontAwesome.svg',
        url: 'http://reddit.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e095',
        labels: {
          en: {
            label: 'Reddit',
            iconAltText: 'Visit Reddit'
          },
          it: {
            label: 'Reddit',
            iconAltText: 'Visita Reddit'
          }
        }
      },
      {
        iconHeight: '150px',
        iconUrl: '/images/instagram-square - FontAwesome.svg',
        url: 'http://instagram.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e098',
        labels: {
          en: {
            label: 'Instagram',
            iconAltText: 'Visit Instagram'
          },
          it: {
            label: 'Instagram',
            iconAltText: 'Visita Instagram'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/youtube-square - FontAwesome.svg',
        url: 'http://youtube.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e091',
        labels: {
          en: {
            label: 'YouTube',
            iconAltText: 'Visit YouTube'
          },
          it: {
            label: 'YouTube',
            iconAltText: 'Visita YouTube'
          }
        }
      },
      {
        iconHeight: '50px',
        iconUrl: '/images/twitter-square - FontAwesome.svg',
        url: 'http://twitter.com',
        id: '35111f28-5fa8-4ef2-bdcd-64ba962c9e094',
        labels: {
          en: {
            label: 'Twitter',
            iconAltText: 'Visit Twitter'
          },
          it: {
            label: 'Twitter',
            iconAltText: 'Visita Twitter'
          }
        }
      }
    ]
  };

  return (
    <PortalComponentWrapper defaultComponentProps={defaultProps} mockTheme={mockTheme}>
      <QuickLinks
        componentTheme={object('componentTheme', testProps.componentTheme)}
        feature={object('feature', testProps.feature)}
      />
    </PortalComponentWrapper>
  );
});
