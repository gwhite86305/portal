import { MovieItem } from "../store/movies/types";
import { VideoGroup } from "../store/videos/types";
import { GridItem } from "../components/GridLayout/GridLayout";

// TODO: remove this util file when series & movies are refactored into a sigle component

export const getGridTheme = (width: number, xsFraction: number, smFraction: number, mdFraction: number, imageRatio: number, numItems: number) => {
  const theme = {
    gridGap: {xs: '0.2rem'},
    padding: {xs: '0.2rem'},
    rowHeight: {xs: '', sm: '', md: ''},
    rowHeightMs: {xs: '', sm: '', md: ''},
    horizontalFraction: {xs: '', sm: '', md: ''},
    smallGridItemCol: {xs: '1fr', sm: '1fr', md: '1fr'},
    smallGridItemRow: {xs: '1'}
  };

    // generate horizontal fractions
    theme.horizontalFraction.md = `${1}fr `.repeat(mdFraction);
    theme.horizontalFraction.sm = `${1}fr `.repeat(smFraction);
    theme.horizontalFraction.xs = `${1}fr `.repeat(xsFraction);
  
    // calculate row height based on ratio, width and fractions
    theme.rowHeight.md = `${width / mdFraction / imageRatio}px`;
    theme.rowHeight.sm = `${width / smFraction / imageRatio}px`;
    theme.rowHeight.xs = `${width / xsFraction / imageRatio}px`;
  
    // calculate row height & then repeat for x number of rows
    theme.rowHeightMs.md = `${width / mdFraction / imageRatio}px `.repeat(Math.ceil(numItems / mdFraction));
    theme.rowHeightMs.sm = `${width / smFraction / imageRatio}px `.repeat(Math.ceil(numItems / smFraction));
    theme.rowHeightMs.xs = `${width / xsFraction / imageRatio}px `.repeat(Math.ceil(numItems / xsFraction));
    return theme;
}

export const getRow = (index: number, fraction: number) => Math.ceil(index / fraction);
export const getColumn = (index: number, fraction: number) =>
  index <= fraction ? index : index - (getRow(index, fraction) - 1) * fraction;
  export const getRowPos = (index: number, xsFraction: number, smFraction: number, mdFraction: number) => ({
  xs: `${getRow(index, xsFraction)}`,
  sm: `${getRow(index, smFraction)}`,
  md: `${getRow(index, mdFraction)}`
});

export const getColumPos = (index: number, xsFraction: number, smFraction: number, mdFraction: number) => ({
  xs: `${getColumn(index, xsFraction)}`,
  sm: `${getColumn(index, smFraction)}`,
  md: `${getColumn(index, mdFraction)}`
});