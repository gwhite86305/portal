import * as React from 'react';
import StyledContainer from './StyledContainer';
import {HtmlBundleProps} from './types';
import * as defaults from './defaultProps.json';

const HtmlBundle: React.FC<HtmlBundleProps> = ({htmlBundleUrl}) => (
  <StyledContainer>
    <iframe src={htmlBundleUrl} allowFullScreen />
  </StyledContainer>
);

HtmlBundle.defaultProps = defaults as any;
export default HtmlBundle;
