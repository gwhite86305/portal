# Moving Map

Component which shows position of plane on map, together with bunch information about origin, destination and remaining
flight time.

## Used libraries

Moving map uses (apart from usual stuff)

[pigeon-maps](https://github.com/mariusandra/pigeon-maps)

[pigeon-overlay](https://github.com/mariusandra/pigeon-overlay)

## Used resources

Moving map uses [OpenStreetMap](https://www.openstreetmap.org/about)

Maps are composed of png 'tiles', which can be downloaded from globally available servers, for example:

https://c.tile.openstreetmap.org/4/8/5.png

Where 4 is zoom level, 8 and 5 are variables that allow to get image from certain position.

Those files have to be downloaded and packaged.

## Creating map package for CMS

CMS takes an uploaded zip and extracts the archive, which later is accessible from API. Application expects maps to be
packaged in a certain way (same as V1).

1. In root of archive there are folders, which reflect zoom(z) level of map.
2. Inside zoom folders there are folders, which reflect longitude progression along x axis (x)
3. Inside longitude progression folder named there are png files, which name reflects latitude progression along y axis
   (y).

```
$z/$x/$y.png
```

For example:

```
4/1/1.png
4/1/2.png
4/2/1.png
4/2/2.png
```

## How MovingMap retrieves files

MovingMap components will retrieve archieve url from

```
/api/content/movingmap
```

endpoint. Example response (part that concerns map):

```
"maps": [
    {
      "uid": "5cdc03e2e4b04598114afea7",
      "attributes": {
        "minZoom": 4,
        "maxZoom": 6
      },
      "name": "map-world-zoom-4-5-6",
      "files": [
        "https://global-onboard2-dev-files.cloudstore.aero/vstest/files/5cdd8e81e4b04598114b04e3.zip"
      ]
    }
  ]
```

`.zip` will be stripped from `files[0]` and `$z/$x/$y.png` will be appended at the end, creating an url that points to a
specific tile. The rest is handled by `pigeon-maps`.

Additionally, in response we can see map configuration, `min/maxZoom`.

## Drawing on map

MovingMap will ask endpoint

```
/api/content/movingmap
```

periodically and update state, from which plane, origin and destination lat/lon are taken, which allow then to draw
those on map. Additionally, part of the response is `trackedRoutePoints` which form a path that was travelled so far by
plane.

Line in drawn between those, which is artificially smoothed using bezier curves.
