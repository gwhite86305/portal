import axios from 'axios';
import {utils} from 'component-utils';
import {ActionCreator} from 'component-utils/dist/store/action-creator';

// Action Creators
export const ActionCreators = {
  registerGuestUser: new ActionCreator<'registerGuestUser', any>('registerGuestUser')
};

// Action Types
export type Action = typeof ActionCreators[keyof typeof ActionCreators];

export function registerGuestUser(email: string) {
  return (dispatch: any) => {
    dispatch(ActionCreators.registerGuestUser.create({type:"guest", email: email }));
  };
}

