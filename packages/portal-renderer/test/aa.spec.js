import {screenshot_test, delay, handleFailures} from './utils';
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const request = require('request');
const filePath = require('path');

describe('AA VISUAL REGRESSION TEST', () => {
  let originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 8000000;
  });

  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it(`LAPTOP`, async () => {
    const path = 'test/screenshot/laptop/aa/';
    const results = [];
    const browserConfig = {headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if (process.env.CHROMIUM_PATH) {
      browserConfig['executablePath'] = process.env.CHROMIUM_PATH;
    }
    const browser = await puppeteer.launch(browserConfig);
    const page = await browser.newPage();
    await page.setViewport({width: 1000, height: 1000});
    await page.goto('http://localhost:3001', {waitUntil: 'networkidle0'});
    await page.evaluate(() => {
      document.querySelector("div[data-component-guid='HASH_CAROUSEL_GRID']").style.visibility = 'hidden';
    });
    results.push(await screenshot_test(page, `${path}home`));

    const device = require(filePath.join(__dirname, '..', process.env.DEVICE_URL));
    const mockData = {
      method: 'GET',
      latency: 0,
      return: {
        ...device,
        isServiceAvailable: false
      }
    };
    request.post(
      {
        headers: {'content-type': 'application/json'},
        url: process.env.API_MOCK_URL + '/mock/device',
        body: JSON.stringify(mockData)
      },
      (error, response, body) => {
        console.log(body);
      }
    );
    await delay(6000);
    results.push(await screenshot_test(page, `${path}offline`));

    await browser.close();
    handleFailures('AAL', 'LAPTOP', results);
  });
});
