#! /bin/bash
GIT_VERSION=`git --version | sed -E 's/git version ([1-9]+.[1-9]+).[1-9]+.[1-9]+/\1/'`
echo -n "Found git version: $GIT_VERSION, "
HAS_HOOKS_PATH=`bc <<< "$GIT_VERSION>2.9"`
if [ $HAS_HOOKS_PATH -eq 1 ]; then
	echo \ using .githooks path config...
	git config core.hooksPath .githooks
	echo core.hooksPath set!
else
	
	find .git/hooks -type l -exec rm {} \; && find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;
	echo hooks symlinked!
fi
