import {
  login,
  createOrg,
  airlineIcao,
  airlineName,
  createPortal,
  URLS,
  getComponentList,
  getComponentDefaultProps,
  checkName
} from './utils';
import program from 'commander';
import {execSync} from 'child_process';
import {username, password} from '../jest.config';

export const TASK_NAME = {
  TEST: 'TEST',
  LIST: 'LIST',
  PREVIEW: 'PREVIEW'
};

async function runPreview(params) {
  checkName(program.componentName);
  console.log(` Task Name: ${TASK_NAME.PREVIEW.green}`);
  console.log(` Component Name: ${program.componentName.green}`);
  await login(username, password);
  const org = await createOrg({icaoCode: airlineIcao, name: airlineName});
  const componentList = getComponentList();
  const componentSchema = componentList[program.componentName];
  const defaultProps = getComponentDefaultProps(program.componentName);
  const data = await createPortal(org, program.componentName, defaultProps);
  const {uid} = data;
  console.log(`\nResult:`);
  console.log(' Preview URL:', `${URLS.builderWebUrl}portals/edit/${uid}`.underline.blue);
  console.log(` Press ${'CTRL+C'.bold} to ${'EXIT'.bold} from the inspector preview`);
  process.stdin.resume();
}

async function runTests() {
  const components = Object.keys(getComponentList());
  components.forEach(name => {
    checkName(name);
    // check tests
    console.log(` Task Name: ${TASK_NAME.TEST.green}`);
    console.log(` Component Name: ${name.green}\n`);
    execSync('mkdir -p ./node_modules/portal-builder-inspector/__tests__/', {stdio: [0, 1, 2]});
    execSync('rm -rf ./node_modules/portal-builder-inspector/__tests__/*', {stdio: [0, 1, 2]});
    execSync(
      `cp ./src/components/${name}/__tests__/*.js ./node_modules/portal-builder-inspector/__tests__/ 2>/dev/null || :`,
      {stdio: [0, 1, 2]}
    );
    execSync('cd ./node_modules/portal-builder-inspector/ && npm run e2e', {
      stdio: [0, 1, 2]
    });
  });
}

async function runList(params) {
  console.log(` Task Name: ${'SHOW COMPONENT LIST'.green}`);
  const components = Object.keys(getComponentList());
  console.log(`\nResult:`);
  components.forEach(name => console.log(`  -- ${name.green}`));
  process.exit();
}

export const performTask = (taskName, program) => {
  console.log(`\nTask Summary:`);
  switch (taskName) {
    case TASK_NAME.LIST:
      runList();
      break;
    case TASK_NAME.TEST:
      runTests();
      break;
    case TASK_NAME.PREVIEW:
      runPreview();
      break;
    default:
      break;
  }
};
