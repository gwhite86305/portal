import * as React from 'react';
import {withLocaleProps} from 'component-utils/dist/components/Localize';
import connectWrapper from 'component-utils/dist/utils/connectWrapper';
import {SignupBanner as ExternalInterface} from '../common/ComponentsInterface';
import SignupBanner from './SignupBanner';
import {withRouter} from 'react-router';
import {_signupFreeCall} from '../../store/WifiServiceOptions/actions';
import {getFreeTier} from '../../utils/ConnectionUtils';

export type PageProps = DispatchProps & ExternalInterface & {actions: any};

export interface DispatchProps {
  signupFree(tierId: number): any;
}

@connectWrapper(
  (state: any, ownProps: any) => {
    let props = {};
    if (state.profile && state.device && state.WifiServiceOptions && state.userAgreement) {
      const freeTier = getFreeTier(state.profile);
      let serviceTierId = freeTier ? freeTier.id : null;
      props = {
        serviceTierId: serviceTierId,
        wifiServiceOptions: state.WifiServiceOptions,
        device: state.device,
        userAgreement: state.userAgreement
      };
    }
    return props;
  },
  (dispatch: any, ownProps: PageProps, state: any): DispatchProps => {
    return {
      signupFree: tierId => {
        dispatch(_signupFreeCall(tierId));
      }
    };
  }
)
class SignupBannerContainer extends React.Component<PageProps, any> {
  public render() {
    let onButtonClick;
    // If the user already have freeTier, send the user to the openPageOnClick url
    if (this.props.serviceTierId != this.props.device.serviceTierId) {
      onButtonClick = () => {
        this.props.signupFree(this.props.serviceTierId);
      };
    }
    return <SignupBanner buttonClickHook={onButtonClick} {...this.props} />;
  }
}

export default withRouter(
  withLocaleProps(SignupBannerContainer, [
    {altTextForImage: 'ifc.SignupBanner.image.altText'},
    {subHeaderText: 'ifc.SignupBanner.subHeader.text'},
    {headerText: 'ifc.SignupBanner.header.text'},
    {buttonText: 'ifc.SignupBanner.button.text'}
  ])
) as any;
