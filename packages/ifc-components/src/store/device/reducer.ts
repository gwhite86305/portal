import {Action, ActionCreators} from './actions';
export type State = {
  readonly error?: string | null;
  readonly serviceTierId?: number | null;
  readonly expectingServiceTierChange?: boolean | null | undefined;
};
export const initialState: State = {
  error: null,
  serviceTierId: null,
  expectingServiceTierChange: false
};
export default function reducer(state: State = initialState, action: Action): State {
  let partialState: Partial<State>;
  let clearedState: any = {};
  Object.keys(state).map(key => {
    clearedState[key] = undefined;
  });
  switch (action.type) {
    case ActionCreators.receiveDevice.type:
      partialState = {
        ...clearedState,
        ...action.payload
      };
      break;
    case ActionCreators.updateServiceTier.type:
      partialState = {
        serviceTierId: action.payload
      };
      break;
    case ActionCreators.waitForServiceTier.type:
      partialState = {
        expectingServiceTierChange: true
      };
      break;
    default:
      return state;
  }
  return partialState;
}
